FROM python:3.9
WORKDIR /app
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY ./*.sh /app/
COPY ./discover_xbrl discover_xbrl
COPY ./discover_xbrl/conf/config.api.yaml discover_xbrl/conf/config.yaml
CMD /bin/sh -c "./pre_fill_cache.sh > cache_output & ./api_server.sh"

