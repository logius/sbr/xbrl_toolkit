FROM python:3.9
WORKDIR /app
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY ./*.sh /app/
COPY ./discover_xbrl discover_xbrl
COPY xbrl_parser_cache.sqlite /tmp/
RUN mkdir -p /tmp/reports
RUN mkdir -p /tmp/xbrl-validation-reports/public
RUN mkdir -p /tmp/log
COPY ./discover_xbrl/conf/config.api.yaml discover_xbrl/conf/config.yaml
EXPOSE 8000
CMD ./SP.api_server.sh

