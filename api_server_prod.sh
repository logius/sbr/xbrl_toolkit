#!/bin/bash
# Find out where this script resides on disk,
# that is the project root,
# add that to the PYTHON_PATH
PROJECT_ROOT=$(dirname "$0")
export PYTHONPATH=$PROJECT_ROOT:$PYTHONPATH
export PYTHON_UNBUFFERED=True
echo $PYTHONPATH
# kill any running uvicorn proces
COUNT=$(pgrep -c uvicorn)
if [ "$COUNT" -gt "0" ]
then pkill --signal 9 uvicorn && pkill --signal 9 -f "from multiprocessing." && sleep 3;
fi

nohup uvicorn discover_xbrl.api.api_server:api_server --host 127.0.0.1 --port 8000 --root-path /api --workers 1 > /var/log/toolkit/api_server.log &

