import os.path as path
from pathlib import Path
from unittest import TestCase, skip
from discover_xbrl.sbrnl_xbrl_parser.linkbase.classes import Loc
from discover_xbrl.sbrnl_xbrl_parser.schema.classes import Concept
from discover_xbrl.sbrnl_xbrl_parser.linkbase.functions import (
    follow_local_loc,
    follow_loc_with_element_lists,
)
from discover_xbrl.conf.conf import Config

conf = Config("config.test.nodb.yaml")


class TestLinkbaseFunction(TestCase):
    def test_follow_local_loc(self):
        concepts_dict = {
            "sbr-dim_ValidationTable": Concept(
                "sbr-dim_ValidationTable",
                "ValidationTable",
                "true",
                periodType="duration",
                substitutionGroup="xbrldt:hypercubeItem",
                element_type="xbrli:stringItemType",
                typedDomainRef=None,
                is_abstract="True",
                ns_prefix="sbr-dim",
            ),
            "sbr-dim_ValidationLineItems": Concept(
                "sbr-dim_ValidationLineItems",
                "ValidationLineItems",
                "true",
                periodType="duration",
                substitutionGroup="sbr:primaryDomainItem",
                element_type="xbrli:stringItemType",
                typedDomainRef=None,
                ns_prefix="sbr-dim",
                is_abstract="True",
            ),
        }
        loc = Loc(
            Path(
                f"{conf.test['datadir']}validation/test-minimal-aangifte-hypercube-dimension-def.xml"
            ),
            "sbr-dim_ValidationLineItems",
            "bd_sbr-dim_ValidationLineItems_loc",
            None,
        )

        result = follow_local_loc(concepts_dict, loc)
        self.assertIsInstance(result, Concept)

        loc = Loc(
            Path(
                f"{conf.test['datadir']}validation/test-minimal-aangifte-hypercube-dimension-def.xml"
            ),
            "sbr-dim_ValidationLineItemsXXX",
            "bd_sbr-dim_ValidationLineItems_loc",
            None,
        )
        result = follow_local_loc(concepts_dict, loc)
        self.assertEqual(result, None)

    @skip("this function is never used, so not tested yet")
    def test_follow_loc_with_element_lists(self):
        result = follow_loc_with_element_lists(None, None)
