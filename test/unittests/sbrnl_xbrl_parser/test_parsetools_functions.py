import time
import os.path as path
from unittest import TestCase, skip
from discover_xbrl.sbrnl_xbrl_parser.parsetools.file_handlers import (
    return_file_contents,
)
from discover_xbrl.sbrnl_xbrl_parser.parsetools.helpers import follow_loc, clean_url
from discover_xbrl.sbrnl_xbrl_parser.parsetools.classes import Path, WWWPath
from discover_xbrl.conf.conf import Config

conf = Config("config.test.nodb.yaml")


class TestParsetoolsFunctions(TestCase):
    def test_get_file_contents(self):
        file = Path(f"{conf.test['datadir']}/entrypoints/entrypoint.minimal.xsd")
        discovered = []
        result = return_file_contents(file, discovered)
        self.assertIn('id="entrypoint-minimal"', result)
        self.assertEqual(1, len(discovered))

    def test_get_contents_cached(self):
        file = Path(f"{conf.test['datadir']}/dictionary/test-types.xsd")
        discovered = []
        result = return_file_contents(file, discovered)
        self.assertIn('xs:schema id="bd-types"', result)
        self.assertEqual(1, len(discovered))
        file2 = Path(f"{conf.test['datadir']}/entrypoints/entrypoint.minimal.xsd")
        result = return_file_contents(file2, discovered)
        self.assertIn('id="entrypoint-minimal"', result)
        self.assertEqual(2, len(discovered))
        result = return_file_contents(file, discovered)
        self.assertIn('xs:schema id="bd-types"', result)
        self.assertEqual(2, len(discovered))

    def test_get_file_contents_remote(self):
        file = WWWPath("http://www.xbrl.org/2003/xbrl-instance-2003-12-31.xsd")
        discovered = []
        start = time.time()
        result = return_file_contents(file, discovered)
        uncached = time.time() - start
        self.assertIn(
            "Taxonomy schema for XBRL. This schema defines syntax relating to", result
        )
        start = time.time()
        result = return_file_contents(file, discovered)
        self.assertIn(
            "Taxonomy schema for XBRL. This schema defines syntax relating to", result
        )
        cached = time.time() - start
        self.assertLess(cached, uncached)

    @skip("Todo: test_follow_loc, need example")
    def test_follow_loc(self):
        # see test_parse_linkbase, parse_loc calls this
        pass

    def test_clean_url(self):
        result = clean_url("https://cooljapan.nnl/somexsd.xsd#anchor_id")
        self.assertIsInstance(result, WWWPath)
        result = clean_url(
            f"{conf.test['datadir']}/dictionary/test-types.xsd#bd_hasables"
        )
        self.assertIsInstance(result, Path)
        self.assertNotIn("#", result.as_posix())
