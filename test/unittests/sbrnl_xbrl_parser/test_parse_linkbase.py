import os.path as path
from unittest import TestCase, skip
from pathlib import Path, PosixPath
from lxml import etree

from discover_xbrl.sbrnl_xbrl_parser.parsetools.classes import WWWPath, DTS
from discover_xbrl.sbrnl_xbrl_parser.schema.classes import Concept
from discover_xbrl.sbrnl_xbrl_parser.linkbase.classes import Loc
from discover_xbrl.sbrnl_xbrl_parser.linkbase.parse_linkbase import (
    pre_parse_linkbase,
    parse_linkbase,
    parse_known_linkbase,
)
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.references import (
    discover_linkbaseref,
    get_reference_element,
    discover_locators,
    discover_arcs,
    discover_label_arcs,
)
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.resources import discover_labels
from discover_xbrl.conf.conf import Config

conf = Config("config.test.nodb.yaml")


class testParseLinkbase(TestCase):
    def test_pre_parse_linkbase_locs(self):
        ep = Path(f"{conf.test['datadir']}/dictionary/test-data-lab-nl.xml")
        dts = DTS(ep.name)
        dts.namespaces = {
            "xs": "http://www.w3.org/2001/XMLSchema",
            "hfp": "http://www.w3.org/2001/XMLSchema-hasFacetAndProperty",
            "link": "http://www.xbrl.org/2003/linkbase",
            "xlink": "http://www.w3.org/1999/xlink",
        }

        result = pre_parse_linkbase(ep, dts, ep, [])
        found = [x for x in result]
        self.assertEqual(2, len(found))
        self.assertEqual("test-data.xsd", found[1].name)

    def test_pre_parse_linkbase_roleref(self):
        ep = Path(
            f"{conf.test['datadir']}/validation/test-minimal-aangifte-lineitems-def.xml"
        )
        dts = DTS(ep.name)  # wrong :)
        dts.namespaces = {
            "xs": "http://www.w3.org/2001/XMLSchema",
            "hfp": "http://www.w3.org/2001/XMLSchema-hasFacetAndProperty",
            "link": "http://www.xbrl.org/2003/linkbase",
            "xlink": "http://www.w3.org/1999/xlink",
        }

        result = pre_parse_linkbase(ep, dts, ep, [])
        found = [x for x in result]
        self.assertEqual(5, len(found))
        self.assertEqual("test-minimal-linkroles-tables.xsd", found[1].name)

    @skip("this function needs too much setup for now")
    def test_parse_linkbase(self):
        ep = Path(f"{conf.test['datadir']}/dictionary/test-data-lab-nl.xml")
        dts = DTS(ep.name)
        dts.namespaces = {
            "xs": "http://www.w3.org/2001/XMLSchema",
            "hfp": "http://www.w3.org/2001/XMLSchema-hasFacetAndProperty",
            "link": "http://www.xbrl.org/2003/linkbase",
            "xlink": "http://www.w3.org/1999/xlink",
        }
        parse_linkbase(ep, dts, ep, [], validator=None)

    def test_known_linkbase(self):
        ep = Path(f"{conf.test['datadir']}/dictionary/test-data-lab-nl.xml")
        dts = DTS(ep.name)
        dts.namespaces = {
            "xs": "http://www.w3.org/2001/XMLSchema",
            "hfp": "http://www.w3.org/2001/XMLSchema-hasFacetAndProperty",
            "link": "http://www.xbrl.org/2003/linkbase",
            "xlink": "http://www.w3.org/1999/xlink",
        }
        parse_known_linkbase(ep, dts, ep, [], [])

    def test_discover_linkbaseref(self):
        linkbase_list = []
        doc = etree.parse(
            f"{conf.test['datadir']}/presentation/test-linkroles-pre-generic-lab-nl.xml"
        )
        root = doc.getroot()
        for el in root.xpath(
            "//link:roleRef", namespaces={"link": "http://www.xbrl.org/2003/linkbase"}
        ):
            linkbase_list.append(el)
        linkbases = discover_linkbaseref(
            linkbase_list,
            Path(
                f"{conf.test['datadir']}/presentation/test-linkroles-pre-generic-lab-nl.xml"
            ),
            None,
        )
        linkbase = next(linkbases)
        self.assertEqual("http://www.xbrl.org/2008/role/link", linkbase["uri"])
        linkbase = next(linkbases)
        self.assertEqual("http://www.xbrl.org/2008/role/label", linkbase["uri"])
        self.assertEqual(
            WWWPath("http://www.xbrl.org/2008/generic-label.xsd#standard-label"),
            linkbase["path"],
        )
        # there should be no more labels available
        try:
            _ = next(linkbases)
        except StopIteration:
            pass

    def test_get_reference_element(self):
        nodes = []
        nodes.append(
            etree.fromstring(
                """
            <link:reference id="bd_4500f7e604944a7c8c7f65a35d3424fd" 
            xlink:label="bd_4500f7e604944a7c8c7f65a35d3424fd_ref" 
            xlink:role="http://www.xbrl.org/2003/role/reference" 
            xmlns:link="http://www.xbrl.org/2003/linkbase" 
            xmlns:xlink="http://www.w3.org/1999/xlink" 
            xmlns:ref="http://www.xbrl.org/2006/ref" 
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xlink:type="resource"><ref:Article>215</ref:Article><ref:Name>Richtlijn BTW</ref:Name></link:reference>"""
            )
        )
        nodes.append(
            etree.fromstring(
                """
            <link:reference id="bd_20a99a4ee9b44a58a4970e5ec0d181a7" 
            xmlns:link="http://www.xbrl.org/2003/linkbase" 
            xmlns:xlink="http://www.w3.org/1999/xlink" 
            xmlns:ref="http://www.xbrl.org/2006/ref" 
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xlink:label="bd_20a99a4ee9b44a58a4970e5ec0d181a7_ref" 
            xlink:role="http://www.xbrl.org/2003/role/reference" 
            xlink:type="resource"><ref:Chapter>mda definities</ref:Chapter>
                  <ref:Name>belastingdienst taxonomie definities.pdf</ref:Name>
                  <ref:Paragraph>95</ref:Paragraph>
            </link:reference>"""
            )
        )
        nodes.append(
            etree.fromstring(
                """
            <link:reference id="bd_699c3ba4400e44d4b61c3b7613d457f2" 
            xmlns:link="http://www.xbrl.org/2003/linkbase" 
            xmlns:xlink="http://www.w3.org/1999/xlink" 
            xmlns:ref="http://www.xbrl.org/2006/ref" 
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xlink:label="bd_699c3ba4400e44d4b61c3b7613d457f2_ref" 
            xlink:role="http://www.xbrl.org/2003/role/reference" 
            xlink:type="resource"><ref:Chapter>mda definities</ref:Chapter>
                <ref:Name>belastingdienst taxonomie definities.pdf</ref:Name>
                <ref:Paragraph>93</ref:Paragraph>
            </link:reference>"""
            )
        )
        result_generator = get_reference_element(nodes)
        result = next(result_generator)
        self.assertEqual("bd_4500f7e604944a7c8c7f65a35d3424fd", result.id)
        self.assertEqual("Richtlijn BTW", result.name)
        self.assertEqual("bd_4500f7e604944a7c8c7f65a35d3424fd_ref", result.xlink_label)
        result = next(result_generator)
        self.assertEqual("bd_20a99a4ee9b44a58a4970e5ec0d181a7", result.id)
        self.assertEqual("belastingdienst taxonomie definities.pdf", result.name)
        self.assertEqual("bd_20a99a4ee9b44a58a4970e5ec0d181a7_ref", result.xlink_label)
        result = next(result_generator)
        self.assertEqual("bd_699c3ba4400e44d4b61c3b7613d457f2", result.id)
        self.assertEqual("belastingdienst taxonomie definities.pdf", result.name)
        self.assertEqual("bd_699c3ba4400e44d4b61c3b7613d457f2_ref", result.xlink_label)

    def test_discover_locators(self):
        locator_list = []
        doc = etree.parse(
            f"{conf.test['datadir']}/presentation/test-minimal-aangifte-presentation.xml"
        )
        root = doc.getroot()
        for el in root.xpath(
            "//link:loc", namespaces={"link": "http://www.xbrl.org/2003/linkbase"}
        ):
            locator_list.append(el)
        locators = discover_locators(
            locator_list,
            Path(
                f"{conf.test['datadir']}/presentation/test-minimal-aangifte-presentation.xml"
            ),
        )
        loc = next(locators)  # this is a posix path to self, ignore
        self.assertEqual(type(loc), PosixPath)
        loc = next(locators)
        self.assertEqual(type(loc), Loc)
        self.assertEqual("bd-abstr_ValueAddedTaxDeclarationTitle", loc.href_id)
        loc = next(locators)
        self.assertEqual("bd-abstr_TaxConsultantDataTitle", loc.href_id)
        loc = next(locators)  # this is a posix path to self, ignore
        self.assertEqual(type(loc), PosixPath)
        loc = next(locators)
        self.assertEqual("bd-i_TaxConsultantNumber", loc.href_id)
        self.assertEqual("bd_bd-i_TaxConsultantNumber_loc", loc.label)

    ## @skip("Need new XML first ...")
    def test_discover_arcs(self):
        node1 = etree.fromstring(
            b""" <link:definitionLink xmlns:link="http://www.xbrl.org/2003/linkbase"                
                                      xmlns:xlink="http://www.w3.org/1999/xlink"
                                      xlink:role="urn:bd:linkrole:bd-lr-hd_cube-nodim" 
                                      xlink:type="extended">
                    <link:loc xlink:href="http://www.nltaxonomie.nl/2013/xbrl/sbr-dimensional-concepts.xsd#sbr-dim_ValidationLineItems" 
                              xlink:label="bd_sbr-dim_ValidationLineItems_loc" xlink:type="locator" />
                    <link:loc xlink:href="../dictionary/bd-data.xsd#bd-i_ValueAddedTaxSuppliesServicesReducedTariff" 
                              xlink:label="bd_bd-i_ValueAddedTaxSuppliesServicesReducedTariff_loc" 
                              xlink:type="locator" />
                    <link:definitionArc xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
                                        xlink:from="bd_sbr-dim_ValidationLineItems_loc" 
                                        xlink:to="bd_bd-i_ValueAddedTaxSuppliesServicesReducedTariff_loc" 
                                        xlink:arcrole="http://xbrl.org/int/dim/arcrole/domain-member" 
                                        order="0" xlink:type="arc"/>
                </link:definitionLink>"""
        )
        node2 = etree.fromstring(
            b"""<link:definitionLink xmlns:link="http://www.xbrl.org/2003/linkbase"     
                                     xmlns:xbrldt="http://xbrl.org/2005/xbrldt"            
                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                     xlink:role="urn:bd:linkrole:bd-lr-hd_cube-nodim" 
                                     xlink:type="extended">
                    <link:loc xlink:href="http://www.nltaxonomie.nl/2013/xbrl/sbr-dimensional-concepts.xsd#sbr-dim_ValidationLineItems" 
                              xlink:label="bd_sbr-dim_ValidationLineItems_loc" xlink:type="locator" />
                    <link:loc xlink:href="http://www.nltaxonomie.nl/2013/xbrl/sbr-dimensional-concepts.xsd#sbr-dim_ValidationTable" 
                              xlink:label="bd_sbr-dim_ValidationTable_loc" xlink:type="locator" />
                    <link:definitionArc xlink:from="bd_sbr-dim_ValidationLineItems_loc" 
                                        xlink:to="bd_sbr-dim_ValidationTable_loc" 
                                        xlink:arcrole="http://xbrl.org/int/dim/arcrole/all" xbrldt:contextElement="scenario" 
                                        xbrldt:closed="true" order="0" xlink:type="arc" />
                </link:definitionLink>"""
        )
        dts = DTS("test-suite")
        dts.concepts = {}
        concept = Concept(
            id="sbr-dim_ValidationTable",
            name="ValidationTable",
            nillable="true",
            substitutionGroup="xbrldt:hypercubeItem",
            periodType="duration",
            element_type="xbrli:stringItemType",
            is_abstract="true",
        )
        dts.concepts[concept.id] = concept
        concept = Concept(
            id="sbr-dim_ValidationLineItems",
            name="ValidationLineItems",
            nillable="true",
            substitutionGroup="sbr:primaryDomainItem",
            periodType="duration",
            element_type="xbrli:stringItemType",
            is_abstract="true",
        )
        dts.concepts[concept.id] = concept
        concept = Concept(
            id="bd-i_ValueAddedTaxSuppliesServicesReducedTariff",
            name="ValueAddedTaxSuppliesServicesReducedTariff",
            nillable="true",
            substitutionGroup="sbr:primaryDomainItem",
            periodType="duration",
            element_type="xbrli:stringItemType",
            is_abstract="true",
        )
        dts.concepts[concept.id] = concept

        arclist = node1.xpath(
            "link:definitionArc",
            namespaces={
                "link": "http://www.xbrl.org/2003/linkbase",
                "xlink": "http://www.w3.org/1999/xlink",
                "xbrldt": "http://xbrl.org/2005/xbrldt",
            },
        )

        arclist.extend(
            node2.xpath(
                "link:definitionArc",
                namespaces={
                    "link": "http://www.xbrl.org/2003/linkbase",
                    "xlink": "http://www.w3.org/1999/xlink",
                    "xbrldt": "http://xbrl.org/2005/xbrldt",
                },
            )
        )

        result_generator = discover_arcs(arclist, dts)
        result = next(result_generator)
        self.assertEqual(
            "{http://www.xbrl.org/2003/linkbase}definitionArc", result.arc_type
        )
        self.assertEqual("sbr-dim_ValidationLineItems", result.from_concept)
        self.assertEqual(
            "bd-i_ValueAddedTaxSuppliesServicesReducedTariff", result.to_concept
        )
        result = next(result_generator)
        self.assertEqual(
            "{http://www.xbrl.org/2003/linkbase}definitionArc", result.arc_type
        )
        self.assertEqual("sbr-dim_ValidationLineItems", result.from_concept)
        self.assertEqual("sbr-dim_ValidationTable", result.to_concept)

    def test_discover_labels(self):
        label_list = []
        doc = etree.parse(
            f"{conf.test['datadir']}/presentation/test-abstracts-lab-nl.xml"
        )
        root = doc.getroot()
        for el in root.xpath(
            "//link:label", namespaces={"link": "http://www.xbrl.org/2003/linkbase"}
        ):
            label_list.append(el)

        labels = discover_labels(label_list)
        label = next(labels)
        self.assertEqual(
            "bd_7c6d3400462c409fb28ae6ce506b5219_label_nl", label.xlink_label
        )
        self.assertEqual("Belastingconsulent gegevens", label.text)
        label = next(labels)
        self.assertEqual(
            "bd_08a32264a6464519867e353992c0a029_label_nl", label.xlink_label
        )
        self.assertEqual("Aangifte omzetbelasting", label.text)
        # there should be no more labels available
        try:
            _ = next(labels)
        except StopIteration:
            pass
