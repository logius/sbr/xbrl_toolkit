import os.path as path
from unittest import TestCase, skip
from pathlib import Path
import discover_xbrl.sbrnl_xbrl_parser.parsetools.classes as classes
from discover_xbrl.sbrnl_xbrl_parser.parsetools.loadTaxonomy import Discover
from discover_xbrl.conf.conf import Config

conf = Config("config.test.nodb.yaml")


class TestLoadTaxonomy(TestCase):
    def testLoadDiscover(self):
        discover = Discover(
            ep=f"{conf.test['datadir']}/entrypoints/entrypoint.minimal.xsd"
        )
        self.assertIsInstance(discover, Discover)

    def testGetEPLinkbaseRefs(self):
        discover = Discover(
            ep=Path(f"{conf.test['datadir']}/entrypoints/entrypoint.minimal.xsd")
        )
        result = discover.get_ep_linkbaserefs(
            (discover.ep), discovered_files=discover.file_list
        )
        self.assertEqual(3, len(result))

    @skip("Too much noise on wrong testdata")
    def testFullEP(self):
        discover = Discover(
            ep=Path(f"{conf.test['datadir']}/entrypoints/entrypoint.minimal.xsd")
        )
        ep = Path(
            path.abspath(f"{conf.test['datadir']}/entrypoints/entrypoint.minimal.xsd")
        )
        dts = discover.discover_ep(ep)
        self.assertEqual(13, len(dts.schemas))

    def testDiscoverFile(self):
        discover = Discover(
            ep=Path(f"{conf.test['datadir']}/entrypoints/entrypoint.minimal.xsd")
        )
        result = discover.get_ep_linkbaserefs(
            (discover.ep), discovered_files=discover.file_list
        )
        ep_discovered_files = []
        ep_discoverable_files = []
        ep = Path(
            path.abspath(f"{conf.test['datadir']}/entrypoints/entrypoint.minimal.xsd")
        )
        dts = classes.DTS(ep.name)
        dts.directly_referenced_linkbases_str = result

        # emulate the XMLSchema and xml.xsd namespaces, its' all we need
        dts.namespaces = {
            "xs": "http://www.w3.org/2001/XMLSchema",
            "hfp": "http://www.w3.org/2001/XMLSchema-hasFacetAndProperty",
            "bd-rpt-ob": "http://www.nltaxonomie.nl/nt15/bd/20201209.a/entrypoints/bd-rpt-ob-aangifte-2021",
            "link": "http://www.xbrl.org/2003/linkbase",
            "xlink": "http://www.w3.org/1999/xlink",
        }
        discover.discover_file(
            Path(result[0]),
            ep=ep,
            ep_discoverable_files=ep_discoverable_files,
            ep_discovered_files=ep_discovered_files,
            dts=dts,
            all_files=discover.file_list,
        )
        schemas = dts.schemas
        self.assertEqual(10, len(schemas))
        self.assertEqual(5, len(dts.linkbases))
        self.assertEqual(10, len(dts.concepts))

        datatype_names = [x.name for x in dts.datatypes]
        self.assertIn("dateItemType", datatype_names)
