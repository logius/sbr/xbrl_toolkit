from unittest import TestCase, skip

from discover_xbrl.conf.conf import Config
from discover_xbrl.sbrnl_xbrl_parser.parsetools.loadTaxonomy import Discover
from discover_xbrl.sbrnl_xbrl_parser.parsetools.mem_fs import save_taxonomy_memfs
from discover_xbrl.sbrnl_xbrl_parser.parsetools.taxonomy_package import (
    read_taxonomy_package,
)

conf = Config("config.test.nodb.yaml")


class TestLoadTaxonomyPackage(TestCase):
    def tearDown(self) -> None:
        conf.parser["has_taxonomy_package"] = False

    def testMinimalPackage(self):
        """
        Test a minimal package setup.
        """
        tp = read_taxonomy_package(
            f"{conf.test['datadir']}/taxonomy_packages/nt16_20210728_minimal.zip"
        )

        # metadata
        self.assertEqual(tp["metadata"]["identifier"], "nt16")
        self.assertEqual(tp["metadata"]["version"], "20210728")
        self.assertEqual(tp["metadata"]["publisher"]["nl"], "SBR Programma")
        self.assertEqual(tp["metadata"]["publisherURL"], "http://www.nltaxonomie.nl")
        self.assertEqual(tp["metadata"]["publisherCountry"], "NL")
        self.assertEqual(tp["metadata"]["publicationDate"], "2021-07-28")
        self.assertEqual(tp["metadata"]["name"]["nl"], "Nederlandse Taxonomie 16")
        self.assertEqual(
            tp["metadata"]["description"]["nl"],
            "De Nederlandse Taxonomie bevat de rapportages binnen Standard Business Reporting in Nederland.",
        )

        # Remappings
        self.assertEqual(
            tp["remappings"]["http://www.nltaxonomie.nl/nt16/"],
            "../taxonomie/www.nltaxonomie.nl/nt16/",
        )

        # Entrypoints
        self.assertEqual(tp["entrypoints"][0].names["nl"], "Test Entrypoint (minimal)")
        self.assertEqual(
            tp["entrypoints"][0].descriptions["nl"], "Een minimaal entrypoint"
        )
        self.assertEqual(tp["entrypoints"][0].version, "20210728")
        self.assertEqual(
            tp["entrypoints"][0].href,
            "http://www.nltaxonomie.nl/nt16/test/20210728/entrypoints/entrypoint-minimal.xsd",
        )
        self.assertEqual(
            tp["entrypoints"][0].href_remapped,
            "nt16_20210728_minimal/taxonomie/www.nltaxonomie.nl/nt16/test/20210728/entrypoints/entrypoint-minimal.xsd",
        )

    @skip("Skipped for now due to wrong test-data and too much clutter")
    def testLoadMinimalPackage(self):
        conf.parser["has_taxonomy_package"] = True
        # Load the taxonomy package metadata
        tp_metadata = read_taxonomy_package(
            f"{conf.test['datadir']}/taxonomy_packages/nt16_20210728_minimal.zip"
        )

        # Initialize the discoverer
        discover = Discover(taxonomy_package=tp_metadata)

        # Copy the taxonomy data into memory
        save_taxonomy_memfs(
            taxonomy_package=f"{conf.test['datadir']}/taxonomy_packages/nt16_20210728_minimal.zip"
        )
        # Discover the DTS('s)
        dts = discover.discover_ep(tp_metadata["entrypoints"][0])

        self.assertEqual(len(dts.schemas), 13)
        self.assertEqual(len(dts.locs), 1887)
        self.assertEqual(len(dts.linkbases), 9)
        self.assertEqual(len(dts.labels), 3)
        self.assertEqual(dts.entrypoint_name, "Test Entrypoint (minimal)")
        self.assertEqual(dts.generic_info.version, "20210728")
        self.assertEqual(dts.generic_info.description, "Een minimaal entrypoint")

    def testMultilangualPackage(self):
        """
        Test a minimal package setup.
        """
        conf.parser["has_taxonomy_package"] = True
        tp = read_taxonomy_package(
            f"{conf.test['datadir']}/taxonomy_packages/nt16_20210728_multilang.zip"
        )

        # metadata
        self.assertEqual(tp["metadata"]["identifier"], "nt16")
        self.assertEqual(tp["metadata"]["version"], "20210728")
        self.assertEqual(tp["metadata"]["publisher"]["nl"], "SBR Programma")
        self.assertEqual(tp["metadata"]["publisherURL"], "http://www.nltaxonomie.nl")
        self.assertEqual(tp["metadata"]["publisherCountry"], "NL")
        self.assertEqual(tp["metadata"]["publicationDate"], "2021-07-28")
        self.assertEqual(tp["metadata"]["name"]["nl"], "Nederlandse Taxonomie 16")
        self.assertEqual(tp["metadata"]["name"]["en"], "Dutch Taxonomy 16")
        self.assertEqual(
            tp["metadata"]["description"]["nl"],
            "De Nederlandse Taxonomie bevat de rapportages binnen Standard Business Reporting in Nederland.",
        )
        self.assertEqual(
            tp["metadata"]["description"]["en"],
            "The Dutch Taxonomy contains rapports within the Dutch Standard Business Reporting programme",
        )

        # Remappings
        self.assertEqual(
            tp["remappings"]["http://www.nltaxonomie.nl/nt16/"],
            "../taxonomie/www.nltaxonomie.nl/nt16/",
        )

        # Entrypoints
        self.assertEqual(tp["entrypoints"][0].names["nl"], "Test Entrypoint 1 NL")
        self.assertEqual(tp["entrypoints"][0].names["en"], "Test Entrypoint 1 EN")
        self.assertEqual(
            tp["entrypoints"][0].descriptions["nl"], "Een omschrijving van entrypoint 1"
        )
        self.assertEqual(
            tp["entrypoints"][0].descriptions["en"], "A description of entrypoint 1"
        )
        self.assertEqual(tp["entrypoints"][0].version, "20210728")
        self.assertEqual(
            tp["entrypoints"][0].href,
            "http://www.nltaxonomie.nl/nt16/test/20210728/entrypoints/test_ep1.xsd",
        )
        self.assertEqual(
            tp["entrypoints"][0].href_remapped,
            "nt16_20210728_multilang/taxonomie/www.nltaxonomie.nl/nt16/test/20210728/entrypoints/test_ep1.xsd",
        )
        self.assertEqual(tp["entrypoints"][0].languages, ["nl", "en"])
