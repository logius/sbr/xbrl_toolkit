import os.path as path
from unittest import TestCase, skip
from pathlib import Path, PosixPath
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.resources import (
    discover_definitionlink,
    discover_labels,
)
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.references import (
    discover_linkbaseref,
    discover_locators,
    discover_label_arcs,
    discover_arcs,
)
from discover_xbrl.sbrnl_xbrl_parser.parsetools.classes import WWWPath
from discover_xbrl.sbrnl_xbrl_parser.linkbase.classes import Loc
from lxml import etree
from discover_xbrl.conf.conf import Config

conf = Config("config.test.nodb.yaml")


class TestSubparsers(TestCase):
    def test_discover_arcs(self):
        arcs_list = []
        pass
