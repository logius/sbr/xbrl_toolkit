from unittest import TestCase

from discover_xbrl.sbrnl_taxonomy_builder.linkbase.labelLinkbase import get_labels
from discover_xbrl.sbrnl_xbrl_parser.linkbase.classes import Label
from discover_xbrl.sbrnl_xbrl_parser.schema.classes import Concept


class testLabelLinkbase(TestCase):
    def test_get_labels(self):
        labels = {
            "dmn-i_RandomConcept_label_nl": Label(
                xlink_label="dmn-i_RandomConcept_label_nl",
                linkRole="http://www.xbrl.org/2003/role/label",
                language="nl",
                text="Dit is een willekeurig label",
                id="dmn-iRandomConcept_label_nl",
            ),
            "dmn-i_RandomConcept_label_en": Label(
                xlink_label="dmn-i_RandomConcept_label_en",
                linkRole="http://www.xbrl.org/2003/role/label",
                language="en",
                text="This is a random label",
                id="dmn-iRandomConcept_label_en",
            ),
        }

        concept = Concept(
            id="test_id",
            name="test_name",
            nillable=False,
            is_abstract="false",
            labels=labels,
        )

        labels = get_labels(concept, "/tmp/abs/filepath")

        labels_count = 0
        locs_count = 0
        arcs_count = 0

        for xml_element in labels:
            if xml_element.type == "label":
                labels_count = labels_count + 1
            elif xml_element.type == "labelArc":
                arcs_count = arcs_count + 1
            elif xml_element.type == "locator":
                locs_count = locs_count + 1
            else:
                print(xml_element.type)
                raise

        # Expect 2 items of each type
        self.assertEqual(labels_count, 2)
        self.assertEqual(locs_count, 2)
        self.assertEqual(arcs_count, 2)
