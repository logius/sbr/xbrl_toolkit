from unittest import TestCase

from discover_xbrl.sbrnl_taxonomy_builder.buildtools.classes import (
    Routeable_XML_Element,
    XML_Parent_element,
)
from discover_xbrl.sbrnl_taxonomy_builder.linkbase.xml_elements.locator import (
    loc_creator,
)


class testLocator(TestCase):
    def test_loc_creator(self):

        good_loc = loc_creator(
            concept_id="concept_id",
            file_path_to="/tmp/xbrl/data.xsd",
            file_path="/tmp/xbrl/data.xml",
            xml_parent_element="{http://www.xbrl.org/2003/linkbase}presentationLink",
        )

        # The locator should be a routable XML element
        self.assertEqual(isinstance(good_loc, Routeable_XML_Element), True)

        # The locator should have an XML_parent element as child
        self.assertEqual(isinstance(good_loc.xml_parent_element, str), True)
