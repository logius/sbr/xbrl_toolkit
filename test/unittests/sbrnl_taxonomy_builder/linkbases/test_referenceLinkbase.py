from unittest import TestCase

from discover_xbrl.sbrnl_taxonomy_builder.buildtools.classes import (
    Routeable_XML_Element,
)
from discover_xbrl.sbrnl_taxonomy_builder.linkbase.referenceLinkbase import (
    get_xml_reference,
    get_xml_referenceArc,
)
from discover_xbrl.sbrnl_xbrl_parser.linkbase.classes import Reference


class testReferenceLinkbase(TestCase):
    def test_get_xml_reference(self):
        """
        Input a Reference object and a path. We expect to get a Routable Path

        """
        reference = Reference(
            id="jenv-bw2-i_RJ_2020-01-01_217_308c_ref",
            xlink_label="jenv-bw2-i_RJ_2020-01-01_217_308c_ref",
            chapter="217",
            issuedate="2020-01-01",
            name="Richtlijnen voor de jaarverslaggeving",
            paragraph="308c",
        )
        result = get_xml_reference(
            reference=reference, linkbase_path="/tmp/testpath.xml"
        )

        self.assertEqual(isinstance(result, Routeable_XML_Element), True)

    def test_get_xml_referenceArc(self):
        reference = Reference(
            id="testid",
            xlink_label="testlabel",
            role="http://www.xbrl.org/2003/role/reference",
        )

        result = get_xml_referenceArc(
            reference=reference, concept_id="testid", linkbase_path="/tmp/testpath.xml"
        )
        self.assertEqual(isinstance(result, Routeable_XML_Element), True)
