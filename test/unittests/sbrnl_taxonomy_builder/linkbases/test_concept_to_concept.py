from unittest import TestCase

from discover_xbrl.sbrnl_taxonomy_builder.buildtools.classes import (
    Routeable_XML_Element,
)
from discover_xbrl.sbrnl_taxonomy_builder.linkbase.arcs.concept_to_concept import (
    createItemtype,
)
from discover_xbrl.sbrnl_xbrl_parser.schema.classes import ItemType, EnumerationChoice


class testConcept_to_concept(TestCase):
    def test_createItemtype_integer(self):

        itemtype = createItemtype(
            ItemType(
                base="xbrli:integerItemType",
                id="nl-types_integer20ItemType",
                name="integer20ItemType",
                domain="nl-types",
                totalDigits="20",
            )
        )

        # We expect the object to be a Routable XML Element
        self.assertEqual(isinstance(itemtype, Routeable_XML_Element), True)

        # We expect the itemtype to be written to an XML schema
        self.assertEqual(itemtype.file_path.endswith("xsd"), True)

        ### Tests on the itemType itself
        xml_element = itemtype.element

        xml_element_totalDigits = xml_element.xpath(
            "//xs:complexType/xs:simpleContent/xs:restriction/xs:totalDigits",
            namespaces={"xs": "http://www.w3.org/2001/XMLSchema"},
        )
        # We expect one totalDigits element
        self.assertEqual(len(xml_element_totalDigits), 1)

        # We expect totalDigits to have a value of 20
        self.assertEqual(xml_element_totalDigits[0].get("value"), "20")

        xml_element_base = xml_element.xpath(
            "//xs:complexType/xs:simpleContent/xs:restriction",
            namespaces={"xs": "http://www.w3.org/2001/XMLSchema"},
        )

        # We expect one base
        self.assertEqual(len(xml_element_base), 1)

        # We expect the restriction base to be 'xbrli:integerItemType'
        self.assertEqual(xml_element_base[0].get("base"), "xbrli:integerItemType")

    def test_createItemtype_string(self):

        itemtype = createItemtype(
            ItemType(
                base="xbrli:stringItemType",
                id="nl-types_emailItemType",
                name="emailItemType",
                domain="nl-types",
                restriction_pattern="[^@]+@[^@]+",
                min_length="6",
                max_length="254",
            )
        )

        # We expect the object to be a Routable XML Element
        self.assertEqual(isinstance(itemtype, Routeable_XML_Element), True)

        # We expect the itemtype to be written to an XML schema
        self.assertEqual(itemtype.file_path.endswith("xsd"), True)

        ### Tests on the itemType itself
        xml_element = itemtype.element

        xml_element_minLength = xml_element.xpath(
            "//xs:complexType/xs:simpleContent/xs:restriction/xs:minLength",
            namespaces={"xs": "http://www.w3.org/2001/XMLSchema"},
        )
        # We expect one totalDigits element
        self.assertEqual(len(xml_element_minLength), 1)

        # We expect totalDigits to have a value of 20
        self.assertEqual(xml_element_minLength[0].get("value"), "6")

        xml_element_maxLength = xml_element.xpath(
            "//xs:complexType/xs:simpleContent/xs:restriction/xs:maxLength",
            namespaces={"xs": "http://www.w3.org/2001/XMLSchema"},
        )
        # We expect one totalDigits element
        self.assertEqual(len(xml_element_maxLength), 1)

        # We expect totalDigits to have a value of 20
        self.assertEqual(xml_element_maxLength[0].get("value"), "254")

        xml_element_restriction = xml_element.xpath(
            "//xs:complexType/xs:simpleContent/xs:restriction/*",
            namespaces={"xs": "http://www.w3.org/2001/XMLSchema"},
        )
        # We expect 3 restriction sub elements
        self.assertEqual(len(xml_element_restriction), 3)

        # We expect pattern to have a value of [^@]+@[^@]+
        self.assertEqual(
            xml_element.xpath(
                "//xs:complexType/xs:simpleContent/xs:restriction/xs:pattern",
                namespaces={"xs": "http://www.w3.org/2001/XMLSchema"},
            )[0].get("value"),
            "[^@]+@[^@]+",
        )

        xml_element_base = xml_element.xpath(
            "//xs:complexType/xs:simpleContent/xs:restriction",
            namespaces={"xs": "http://www.w3.org/2001/XMLSchema"},
        )

        # We expect one base
        self.assertEqual(len(xml_element_base), 1)

        # We expect the restriction base to be 'xbrli:integerItemType'
        self.assertEqual(xml_element_base[0].get("base"), "xbrli:stringItemType")

    def test_createItemtype_enumeration(self):
        itemtype = createItemtype(
            ItemType(
                base="xbrli:stringItemType",
                id="nl-codes_personRoleItemType",
                name="personRoleItemType",
                domain="nl-types",
                enumerations=[
                    EnumerationChoice(
                        id="nl-codes_PersonRole_Berichtgever", value="Berichtgever"
                    ),
                    EnumerationChoice(
                        id="nl-codes_PersonRole_Contactpersoon", value="Contactpersoon"
                    ),
                    EnumerationChoice(
                        id="nl-codes_PersonRole_Informatieplichtige",
                        value="Informatieplichtige",
                    ),
                ],
            )
        )

        xml_element_restriction = itemtype.element.xpath(
            "//xs:complexType/xs:simpleContent/xs:restriction/*",
            namespaces={"xs": "http://www.w3.org/2001/XMLSchema"},
        )
        self.assertEqual(len(xml_element_restriction), 3)

        self.assertEqual(
            xml_element_restriction[0].get("id"), "nl-codes_PersonRole_Berichtgever"
        )
        self.assertEqual(
            xml_element_restriction[1].get("id"), "nl-codes_PersonRole_Contactpersoon"
        )
        self.assertEqual(
            xml_element_restriction[2].get("id"),
            "nl-codes_PersonRole_Informatieplichtige",
        )

    def test_createItemtype_maximumcase(self):

        itemtype = createItemtype(
            ItemType(
                base="xbrli:stringItemType",
                id="nl-types_emailItemType",
                name="emailItemType",
                domain="nl-types",
                restriction_pattern="[^@]+@[^@]+",
                min_length="6",
                max_length="254",
                totalDigits="20",
                inheritance_type=None,  # Not used?
                enumeration_type=None,  # Not used?
                enumeration_length=None,  # Not used?
                fraction_digits="2",
                min_inclusive="0",
                labels=None,  # Not used?
            )
        )

        # We expect the object to be a Routable XML Element
        self.assertEqual(isinstance(itemtype, Routeable_XML_Element), True)

        # We expect the itemtype to be written to an XML schema
        self.assertEqual(itemtype.file_path.endswith("xsd"), True)

        ### Tests on the itemType itself
        xml_element = itemtype.element

        xml_element_minLength = xml_element.xpath(
            "//xs:complexType/xs:simpleContent/xs:restriction/xs:minLength",
            namespaces={"xs": "http://www.w3.org/2001/XMLSchema"},
        )
        # We expect one totalDigits element
        self.assertEqual(len(xml_element_minLength), 1)

        # We expect totalDigits to have a value of 20
        self.assertEqual(xml_element_minLength[0].get("value"), "6")

        xml_element_maxLength = xml_element.xpath(
            "//xs:complexType/xs:simpleContent/xs:restriction/xs:maxLength",
            namespaces={"xs": "http://www.w3.org/2001/XMLSchema"},
        )
        # We expect one totalDigits element
        self.assertEqual(len(xml_element_maxLength), 1)

        # We expect totalDigits to have a value of 20
        self.assertEqual(xml_element_maxLength[0].get("value"), "254")

        xml_element_restriction = xml_element.xpath(
            "//xs:complexType/xs:simpleContent/xs:restriction/*",
            namespaces={"xs": "http://www.w3.org/2001/XMLSchema"},
        )
        self.assertEqual(len(xml_element_restriction), 6)

        self.assertEqual(
            xml_element.xpath(
                "//xs:complexType/xs:simpleContent/xs:restriction/xs:pattern",
                namespaces={"xs": "http://www.w3.org/2001/XMLSchema"},
            )[0].get("value"),
            "[^@]+@[^@]+",
        )

        self.assertEqual(
            xml_element.xpath(
                "//xs:complexType/xs:simpleContent/xs:restriction/xs:minInclusive",
                namespaces={"xs": "http://www.w3.org/2001/XMLSchema"},
            )[0].get("value"),
            "0",
        )

        self.assertEqual(
            xml_element.xpath(
                "//xs:complexType/xs:simpleContent/xs:restriction/xs:fractionDigits",
                namespaces={"xs": "http://www.w3.org/2001/XMLSchema"},
            )[0].get("value"),
            "2",
        )

        xml_element_base = xml_element.xpath(
            "//xs:complexType/xs:simpleContent/xs:restriction",
            namespaces={"xs": "http://www.w3.org/2001/XMLSchema"},
        )

        # We expect one base
        self.assertEqual(len(xml_element_base), 1)

        # We expect the restriction base to be 'xbrli:integerItemType'
        self.assertEqual(xml_element_base[0].get("base"), "xbrli:stringItemType")
