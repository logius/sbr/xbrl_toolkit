import os.path as path
import logging
from unittest import TestCase
from pathlib import Path
from lxml import etree
from discover_xbrl.sbrnl_modules.validators.NT16.linkbase.base import (
    nta_2_03_00_01,
    nta_2_03_00_02,
    nta_2_03_00_03,
    nta_2_03_00_04,
    nta_2_03_00_05,
    nta_2_03_00_08,
    nta_2_03_00_12,
)
from discover_xbrl.sbrnl_modules.validators.NT16.linkbase.labels import nta_2_03_08_10
from discover_xbrl.sbrnl_modules.validators.NT16.start import Validator
from discover_xbrl.conf.conf import Config
from discover_xbrl.sbrnl_xbrl_parser.parsetools.classes import DTS
from discover_xbrl.sbrnl_xbrl_parser.linkbase.classes import Label


conf = Config("config.test.nodb.yaml")
logger_linkbase = logging.getLogger("validator.linkbase")


class TestValidationLinkbase(TestCase):
    """test validation rules
    Note: the validation tests return *True* if the rule is violated,this feels the wrong way round :-)
          just ask yourself Does this violate our rules?
    """

    def __init__(self, *args, **kwargs):
        super(TestValidationLinkbase, self).__init__(*args, **kwargs)
        self.logger_linkbase = logging.getLogger("validator.linkbase")
        self.logger_naming_conventions = logging.getLogger(
            "validator.naming_conventions"
        )
        self.validator = None
        self.all_schema_rules = []
        self.all_linkbase_rules = []
        self.all_model_rules = []
        self.setup_validator()

    # 2.03.00.xx Generic rules
    def test_nta_2_03_00_01(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-data-ref.xml")
        contents, linkbase = self.get_xml(url)
        self.assertEqual(False, nta_2_03_00_01(linkbase))
        new_contents = contents.replace('version="1.0"', 'version="1.1"')
        new_linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        self.assertEqual(True, nta_2_03_00_01(new_linkbase))

    def test_nta_2_03_00_02(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-data-ref.xml")
        contents, linkbase = self.get_xml(url)
        self.assertEqual(False, nta_2_03_00_02(linkbase))
        linkbase.attrib[
            "{http://www.w3.org/2001/XMLSchema-instance}schemaLocation"
        ] = " ".join(
            linkbase.attrib[
                "{http://www.w3.org/2001/XMLSchema-instance}schemaLocation"
            ].split(" ")[:3]
        )
        self.assertEqual(True, nta_2_03_00_02(linkbase))

    def test_nta_2_03_00_03(self):
        url = Path(
            f"{conf.test['datadir']}/validation/test-minimal-aangifte-hypercube-dimension-def.xml"
        )
        contents, linkbase = self.get_xml(url)
        outcome = nta_2_03_00_03(contents)
        self.assertEqual(False, outcome)
        contents = (
            '<?xml version="1.1" Encoding="uTF-8" standalone="yes"?>'
        )  # ugly but acceptable
        outcome = nta_2_03_00_03(contents)
        self.assertEqual(False, outcome)
        contents = '<?xml version="1.1" Encoding="uTF-16" standalone="yes"?>'  # No! No!
        outcome = nta_2_03_00_03(contents)
        self.assertEqual(True, outcome)

    def test_nta_2_03_00_04(self):
        url = Path(
            f"{conf.test['datadir']}/validation/test-minimal-aangifte-hypercube-dimension-def.xml"
        )
        contents, linkbase = self.get_xml(url)
        self.assertEqual(False, nta_2_03_00_04(contents))
        parser = etree.XMLParser(remove_comments=True)
        xmldoc = etree.fromstring(bytes(contents, encoding="utf-8"), parser=parser)
        outcome = nta_2_03_00_04(etree.tostring(xmldoc).decode())
        self.assertEqual(True, outcome)

    def test_nts_2_03_00_05(self):
        url = Path(
            f"{conf.test['datadir']}/validation/test-minimal-aangifte-hypercube-dimension-def.xml"
        )
        contents, linkbase = self.get_xml(url)
        self.assertEqual(False, nta_2_03_00_05(contents))
        url = Path(f"{conf.test['datadir']}/validation/file_with_too_many_comments.xml")
        contents, linkbase = self.get_xml(url)
        self.assertEqual(True, nta_2_03_00_05(contents))

    def test_nta_2_03_00_06(self):
        url = Path(f"{conf.test['datadir']}/validation/correct_schema_file.xsd")
        contents, linkbase = self.get_xml(url)
        self.set_rule("2.03.00.06")
        self.assert_xpath_ok(url=url, linkbase=linkbase, contents=contents)
        new_contents = contents.replace(
            "xs:appinfo", "appinfo"
        )  # remove namespace from an element
        linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlogs:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)

    def test_nta_2_03_00_08(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-data-lab-nl.xml")
        contents, linkbase = self.get_xml(url)
        self.set_rule("2.03.00.08")
        self.assertEqual(False, nta_2_03_00_08((linkbase)))
        new_contents = contents.replace(
            "linkbase xmlns:",
            'linkbase xmlns:dcterms="https://purl.org/dc/terms/" xmlns:',
        )
        linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        self.assertEqual(True, nta_2_03_00_08((linkbase)))

    def test_nta_2_03_00_10(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-data-lab-nl.xml")
        contents, linkbase = self.get_xml(url)
        self.set_rule("2.03.00.10")
        self.assert_xpath_ok(url=url, linkbase=linkbase, contents=contents)
        new_contents = contents.replace(
            '"resource"', '"resource" use="prohibit"'
        )  # remove namespace from an element
        linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlogs:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)

    def test_nta_2_03_00_12(self):
        self.set_rule("2.03.00.12")
        url = Path(f"{conf.test['datadir']}/dictionary/test-data-ref.xml")
        contents, linkbase = self.get_xml(url)
        self.assertEqual(False, nta_2_03_00_12((linkbase)))
        for goodarc in linkbase.xpath("//link:referenceArc", namespaces=linkbase.nsmap):
            goodarc.getparent().remove(goodarc)
        self.assertEqual(True, nta_2_03_00_12((linkbase)))

    #
    # 2.03.01.xx
    #
    # Appie: namespace_raadsel hoort bij 2.03.01.01, en ook bij 2.02.00.19
    #
    def test_nta_2_03_02_02(self):
        self.set_rule("2.03.02.02")
        url = Path(f"{conf.test['datadir']}/validation/essence-alias.xml")
        contents, linkbase = self.get_xml(url)
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.02.02: De arcrole ‘essence-alias’ MOET NIET gebruikt worden in 'validation/essence-alias.xml'"
            ],
        )

    def test_nta_2_03_02_03(self):
        self.set_rule("2.03.02.03")
        url = Path(f"{conf.test['datadir']}/validation/similar-tuples.xml")
        contents, linkbase = self.get_xml(url)
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.02.03: De arcrole ‘similar-tuples’ MOET NIET gebruikt worden in 'validation/similar-tuples.xml'"
            ],
        )

    def test_nta_2_03_02_04(self):
        self.set_rule("2.03.02.04")
        url = Path(f"{conf.test['datadir']}/validation/requires-element.xml")
        contents, linkbase = self.get_xml(url)
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.02.04: De arcrole ‘requires-element’ MOET NIET gebruikt worden in 'validation/requires-element.xml'"
            ],
        )

    def test_nta_2_03_02_05(self):
        self.set_rule("2.03.02.05")
        url = Path(
            f"{conf.test['datadir']}/validation/test-minimal-aangifte-lineitems-def.xml"
        )
        contents, linkbase = self.get_xml(url)
        self.assert_xpath_ok(url=url, linkbase=linkbase, contents=contents)
        new_contents = contents.replace(
            'arcroleURI="http://xbrl.org/int/dim/arcrole/domain-member"',
            'xlink:arcrole="./clown.xml#role" arcroleURI="http://xbrl.org/int/dim/arcrole/domain-member"',
        )
        linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.02.05: <link:arcroleRef> @xlink:arcrole MOET NIET gebruikt worden in 'validation/test-minimal-aangifte-lineitems-def.xml'"
            ],
        )

    def test_nta_2_03_02_06(self):
        self.set_rule("2.03.02.06")
        url = Path(
            f"{conf.test['datadir']}/validation/test-minimal-aangifte-lineitems-def.xml"
        )
        contents, linkbase = self.get_xml(url)
        self.assert_xpath_ok(url=url, linkbase=linkbase, contents=contents)
        new_contents = contents.replace(
            'arcroleURI="http://xbrl.org/int/dim/arcrole/domain-member"',
            'xlink:role="./clown-def.xml#rednose" arcroleURI="http://xbrl.org/int/dim/arcrole/domain-member"',
        )
        linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.02.06: <link:arcroleRef> @xlink:role MOET NIET gebruikt worden in 'validation/test-minimal-aangifte-lineitems-def.xml'"
            ],
        )

    def test_nta_2_03_03_02(self):
        self.set_rule("2.03.03.02")
        url = Path(f"{conf.test['datadir']}/dictionary/test-data-ref.xml")
        contents, linkbase = self.get_xml(url)
        self.assert_xpath_ok(url=url, linkbase=linkbase, contents=contents)
        new_contents = contents.replace('id="bd_4500f7e604944a7c8c7f65a35d3424fd"', "")
        linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.03.02: Een reference resource MOET een @id hebben in 'dictionary/test-data-ref.xml'"
            ],
        )

    def test_nta_2_03_03_05(self):
        self.set_rule("2.03.03.05")
        url = Path(f"{conf.test['datadir']}/dictionary/test-data-ref.xml")
        contents, linkbase = self.get_xml(url)
        self.assert_xpath_ok(url=url, linkbase=linkbase, contents=contents)
        new_contents = contents.replace(
            "referenceArc xlink", 'referenceArc order="0" xlink'
        )
        linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.03.05: Een referenceArc MAG NIET @order bevatten in 'dictionary/test-data-ref.xml'"
            ],
        )

    def test_nta_2_03_04_04(self):
        self.set_rule("2.03.04.04")
        url = Path(
            f"{conf.test['datadir']}/presentation/kvk-notes-presentationarc-pre.xml"
        )
        contents, linkbase = self.get_xml(url)
        self.assert_xpath_ok(url=url, linkbase=linkbase, contents=contents)
        new_contents = contents.replace('order="1"', "")
        linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.04.04: <link:presentationArc/@order> MOET gebruikt worden in 'presentation/kvk-notes-presentationarc-pre.xml'"
            ],
        )

    # Test is Deprecated in NT16
    # def test_nta_2_03_04_09(self):
    #     self.set_rule("2.03.04.09")
    #     url = Path(f"{conf.test['datadir']}/presentation/kvk-notes-testing-tab.xml")
    #     contents, linkbase = self.get_xml(url)
    #     self.assert_xpath_ok(url=url, linkbase=linkbase, contents=contents)
    #     new_contents = contents.replace('axis="x"', 'axis="z"')
    #     linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
    #     with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
    #         self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
    #     self.assertEqual(
    #         validatorlog.output,
    #         [
    #             "ERROR:validator.linkbase:2.03.04.09: tableBreakdownArc axis='z' MOET NIET gebruikt worden in 'presentation/kvk-notes-testing-tab.xml'"
    #         ],
    #     )

    def test_nta_2_03_05_04(self):
        self.set_rule("2.03.05.04")
        url = Path(
            f"{conf.test['datadir']}/validation/test-minimal-aangifte-hypercube-dimension-def.xml"
        )
        contents, linkbase = self.get_xml(url)
        self.assert_xpath_ok(url=url, linkbase=linkbase, contents=contents)
        # model-test, need to create objects

    def test_nta_2_03_05_05(self):
        self.set_rule("2.03.05.05")
        url = Path(
            f"{conf.test['datadir']}/validation/test-minimal-aangifte-hypercube-dimension-def.xml"
        )
        contents, linkbase = self.get_xml(url)
        self.assert_xpath_ok(url=url, linkbase=linkbase, contents=contents)
        new_contents = contents.replace("dim/arcrole/all", "dim/arcrole/notAll")
        linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.05.05: Een hypercube – primary relatie MOET de ‘all’ arcrole gebruiken in 'validation/test-minimal-aangifte-hypercube-dimension-def.xml'"
            ],
        )

    def test_nta_2_03_05_06(self):
        self.set_rule("2.03.05.06")
        url = Path(
            f"{conf.test['datadir']}/validation/test-minimal-aangifte-hypercube-dimension-def.xml"
        )
        contents, linkbase = self.get_xml(url)
        self.assert_xpath_ok(url=url, linkbase=linkbase, contents=contents)
        new_contents = contents.replace("scenario", "segment")
        linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.05.06: Een hypercube – primary relatie MOET @xbrldt:contextElement='scenario' gebruiken in 'validation/test-minimal-aangifte-hypercube-dimension-def.xml'"
            ],
        )

    def test_nta_2_03_05_07(self):
        self.set_rule("2.03.05.07")
        url = Path(
            f"{conf.test['datadir']}/validation/test-minimal-aangifte-hypercube-dimension-def.xml"
        )
        contents, linkbase = self.get_xml(url)
        self.assert_xpath_ok(url=url, linkbase=linkbase, contents=contents)
        new_contents = contents.replace(
            '"scenario"', '"scenario" xbrldt:targetRole="./bd_targetrole_false"'
        )
        linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.05.07: De arcrole hypercube-dimension MOET NIET @targetRole gebruiken bij een typed dimensie in 'validation/test-minimal-aangifte-hypercube-dimension-def.xml'"
            ],
        )

    def test_nta_2_03_06_03(self):
        self.set_rule("2.03.06.03")
        url = Path(
            f"{conf.test['datadir']}/validation/ocw-property-plant-equipment-axes-def.xml"
        )
        contents, linkbase = self.get_xml(url)
        self.assert_xpath_ok(url=url, linkbase=linkbase, contents=contents)
        new_contents = contents.replace(
            'xbrldt:targetRole="urn:ocw:linkrole:property-plant-equipment-optional-specification-members-domain"',
            "",
        )
        linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.06.03: Dimension-domain relaties met @usable='false' MOET een @targetRole gebruiken in 'validation/ocw-property-plant-equipment-axes-def.xml'"
            ],
        )

    def test_nta_2_03_06_04(self):
        self.set_rule("2.03.06.04")
        url = Path(
            f"{conf.test['datadir']}/validation/test-minimal-aangifte-hypercube-dimension-def.xml"
        )
        contents, linkbase = self.get_xml(url)
        self.assert_xpath_ok(url=url, linkbase=linkbase, contents=contents)
        new_contents = contents.replace('xbrldt:closed="true"', 'xbrldt:closed="false"')
        linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.06.04: De arcrole all (has hypercube) MOET @xbrldt:closed='true' bevatten in 'validation/test-minimal-aangifte-hypercube-dimension-def.xml'"
            ],
        )

    def test_nta_2_03_06_05(self):
        self.set_rule("2.03.06.05")
        url = Path(
            f"{conf.test['datadir']}/validation/test-minimal-aangifte-hypercube-dimension-def.xml"
        )
        contents, linkbase = self.get_xml(url)
        self.assert_xpath_ok(url=url, linkbase=linkbase, contents=contents)
        new_contents = contents.replace(
            "http://xbrl.org/int/dim/arcrole/all",
            "http://xbrl.org/int/dim/arcrole/dimension-default",
        )
        linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.06.05: De arcrole dimension-default MOET NIET gebruikt worden in 'validation/test-minimal-aangifte-hypercube-dimension-def.xml'"
            ],
        )

    def test_nta_2_03_08_01(self):
        self.set_rule("2.03.08.01")
        url = Path(f"{conf.test['datadir']}/dictionary/test-codes-generic-lab-nl.xml")
        contents, linkbase = self.get_xml(url)
        self.assert_xpath_ok(url=url, linkbase=linkbase, contents=contents)
        new_contents = contents.replace('xml:lang="nl"', 'xml:lang="nl-NL"', 3)
        linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.08.01: De waarde van @xml:lang voor Nederlands MOET ‘nl’ zijn in 'dictionary/test-codes-generic-lab-nl.xml'"
            ],
        )

    def test_nta_2_03_08_02(self):
        self.set_rule("2.03.08.02")
        url = Path(
            f"{conf.test['datadir']}/dictionary/kvk-linkroles-generic-lab-en.xml"
        )
        contents, linkbase = self.get_xml(url)
        self.assert_xpath_ok(url=url, linkbase=linkbase, contents=contents)
        new_contents = contents.replace('xml:lang="en"', 'xml:lang="en-US"', 3)
        linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.08.02: De waarde van @xml:lang voor Engels MOET ‘en’ zijn in 'dictionary/kvk-linkroles-generic-lab-en.xml'"
            ],
        )

    def test_nta_2_03_08_07(self):
        self.set_rule("2.03.08.07")
        url = Path(f"{conf.test['datadir']}/presentation/test-abstracts-lab-nl.xml")
        contents, linkbase = self.get_xml(url)
        self.assert_xpath_ok(url=url, linkbase=linkbase, contents=contents)
        new_contents = contents.replace(
            "Aangifte omzetbelasting", "Aangifte\tomzetbelasting", 1
        )
        linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.08.07: Een <link:label> inhoud MOET behandeld worden als een xs:tokenizedString in 'presentation/test-abstracts-lab-nl.xml'"
            ],
        )

    def test_nta_2_03_08_08(self):
        self.set_rule("2.03.08.08")
        url = Path(f"{conf.test['datadir']}/presentation/test-abstracts-lab-nl.xml")
        contents, linkbase = self.get_xml(url)
        self.assert_xpath_ok(url=url, linkbase=linkbase, contents=contents)
        new_contents = contents.replace(
            'xlink:type="arc"', 'xlink:type="arc" order="0"', 1
        )
        linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.08.08: Een labelArc MAG NIET een @order bevatten. in 'presentation/test-abstracts-lab-nl.xml'"
            ],
        )

    def test_nta_2_03_08_10(self):
        label = Label(
            id="logius_testLabel_nl",
            xlink_label="logius-abst_testLabel_nl",
            linkRole="http://www.xbrl.org/2003/role/label",
            language="nl",
            text="XBRL Taxonomy tool",
        )
        self.assertFalse(nta_2_03_08_10(label))
        label.text = "XBRL <sup>2</sup> Taxonomy tool"
        self.assertTrue(nta_2_03_08_10(label))

    def test_nta_2_03_08_11(self):
        self.set_rule("2.03.08.11")
        url = Path(f"{conf.test['datadir']}/dictionary/test-data-lab-nl.xml")
        contents, linkbase = self.get_xml(url)
        self.assert_xpath_ok(url=url, linkbase=linkbase, contents=contents)
        new_contents = contents.replace(
            "</link:label>",
            "</link:label>"
            '<link:label  xlink:label="bd_54ef9eed52c040f59f7e06b5f397c87d_label_nl" xml:lang="nl" xlink:role="http://www.xbrl.org/2003/role/label" xlink:type="resource">Beconnummerd</link:label>',
        )
        linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.08.11: Een label resource MOET een @id hebben in 'dictionary/test-data-lab-nl.xml'"
            ],
        )

    def test_nta_2_03_08_13(self):
        self.set_rule("2.03.08.13")
        url = Path(
            f"{conf.test['datadir']}/dictionary/kvk-linkroles-generic-lab-de.xml"
        )
        contents, linkbase = self.get_xml(url)
        self.assert_xpath_ok(url=url, linkbase=linkbase, contents=contents)
        new_contents = contents.replace('xml:lang="de"', 'xml:lang="de-DE"', 3)
        linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.08.13: De waarde van @xml:lang voor Duits MOET ‘de’ zijn in 'dictionary/kvk-linkroles-generic-lab-de.xml'"
            ],
        )

    def test_nta_2_03_08_14(self):
        self.set_rule("2.03.08.14")
        url = Path(
            f"{conf.test['datadir']}/dictionary/kvk-linkroles-generic-lab-fr.xml"
        )
        contents, linkbase = self.get_xml(url)
        self.assert_xpath_ok(url=url, linkbase=linkbase, contents=contents)
        new_contents = contents.replace('xml:lang="fr"', 'xml:lang="fr-FR"', 3)
        linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.08.14: De waarde van @xml:lang voor Frans MOET ‘fr’ zijn in 'dictionary/kvk-linkroles-generic-lab-fr.xml'"
            ],
        )

    def test_nta_2_03_09_01(self):
        self.set_rule("2.03.09.01")
        url = Path(f"{conf.test['datadir']}/validation/calculation_linkbase.xml")
        contents, linkbase = self.get_xml(url)
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.09.01: Calculatie linkbases MOETEN NIET gebruikt worden in 'validation/calculation_linkbase.xml'"
            ],
        )

    def test_nta_2_03_09_03(self):
        self.set_rule("2.03.09.03")
        url = Path(f"{conf.test['datadir']}/validation/formula_formula.xml")
        contents, linkbase = self.get_xml(url)
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.09.03: formula:formula MOET NIET gebruikt worden in 'validation/formula_formula.xml'"
            ],
        )

    def test_nta_2_03_09_09(self):
        self.set_rule("2.03.09.09")
        url = Path(f"{conf.test['datadir']}/validation/formula_formula.xml")
        contents, linkbase = self.get_xml(url)
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.09.09: <formula:consistencyAssertion> MOET NIET gebruikt worden in 'validation/formula_formula.xml'"
            ],
        )

    def test_nta_2_03_09_10(self):
        self.set_rule("2.03.09.10")
        url = Path(f"{conf.test['datadir']}/validation/test-kvk-msg-satisfied.xml")
        contents, linkbase = self.get_xml(url)
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.09.10: Een assertion MOET NIET een satisfied message, middels een http://xbrl.org/arcrole/2010/assertion-satisfied-message arc hebben in 'validation/test-kvk-msg-satisfied.xml'"
            ],
        )

    def test_nta_2_03_10_01(self):
        self.set_rule("2.03.10.01")
        url = Path(f"{conf.test['datadir']}/validation/calculation_linkbase.xml")
        contents, linkbase = self.get_xml(url)
        self.assert_xpath_ok(url=url, linkbase=linkbase, contents=contents)
        new_contents = contents.replace(
            'order="1"', 'order="1" xlink:actuate="onRequest"', 1
        )
        linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.10.01: @xlink:actuate MOET NIET gebruikt worden in 'validation/calculation_linkbase.xml'"
            ],
        )

    def test_nta_2_03_10_02(self):
        self.set_rule("2.03.10.02")
        url = Path(f"{conf.test['datadir']}/validation/calculation_linkbase.xml")
        contents, linkbase = self.get_xml(url)
        self.assert_xpath_ok(url=url, linkbase=linkbase, contents=contents)
        new_contents = contents.replace('order="1"', 'order="1" xlink:show="false"', 1)
        linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.10.02: @xlink:show MOET NIET gebruikt worden in 'validation/calculation_linkbase.xml'"
            ],
        )

    def test_nta_2_03_10_03(self):
        self.set_rule("2.03.10.03")
        url = Path(f"{conf.test['datadir']}/validation/calculation_linkbase.xml")
        contents, linkbase = self.get_xml(url)
        self.assert_xpath_ok(url=url, linkbase=linkbase, contents=contents)
        new_contents = contents.replace(
            'order="1"', 'order="1" xlink:title="Forbidden -403-"', 1
        )
        linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.10.03: @xlink:title MOET NIET gebruikt worden in 'validation/calculation_linkbase.xml'"
            ],
        )

    def test_nta_2_03_10_04(self):
        self.set_rule("2.03.10.04")
        url = Path(f"{conf.test['datadir']}/validation/calculation_linkbase.xml")
        contents, linkbase = self.get_xml(url)
        self.assert_xpath_ok(url=url, linkbase=linkbase, contents=contents)
        new_contents = contents.replace(
            'xmlns:xlink="http://www.w3.org/1999/xlink"',
            'xmlns:xlink="http://www.w3.org/1999/xlink" id="dont_do_this"',
            1,
        )
        linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.10.04: link:linkbase/@id MOET NIET gebruikt worden in 'validation/calculation_linkbase.xml'"
            ],
        )

    def test_nta_2_03_10_05(self):
        self.set_rule("2.03.10.05")
        url = Path(f"{conf.test['datadir']}/validation/calculation_linkbase.xml")
        contents, linkbase = self.get_xml(url)
        self.assert_xpath_ok(url=url, linkbase=linkbase, contents=contents)
        new_contents = contents.replace(
            'xmlns:xlink="http://www.w3.org/1999/xlink"',
            'xmlns:xlink="http://www.w3.org/1999/xlink" xsi:nil="true"',
            1,
        )
        linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.10.05: link:linkbase/@xsi:nil MOET NIET gebruikt worden in 'validation/calculation_linkbase.xml'"
            ],
        )

    def test_nta_2_03_10_06(self):
        self.set_rule("2.03.10.06")
        url = Path(f"{conf.test['datadir']}/validation/calculation_linkbase.xml")
        contents, linkbase = self.get_xml(url)
        self.assert_xpath_ok(url=url, linkbase=linkbase, contents=contents)
        new_contents = contents.replace(
            'xmlns:xlink="http://www.w3.org/1999/xlink"',
            'xmlns:xlink="http://www.w3.org/1999/xlink" xsi:noNamespaceSchemaLocation="true"',
            1,
        )
        linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.10.06: link:linkbase/@xsi:noNamespaceSchemaLocation MOET NIET gebruikt worden in 'validation/calculation_linkbase.xml'"
            ],
        )

    def test_nta_2_03_10_07(self):
        self.set_rule("2.03.10.07")
        url = Path(f"{conf.test['datadir']}/validation/calculation_linkbase.xml")
        contents, linkbase = self.get_xml(url)
        self.assert_xpath_ok(url=url, linkbase=linkbase, contents=contents)
        new_contents = contents.replace(
            'xmlns:xlink="http://www.w3.org/1999/xlink"',
            'xmlns:xlink="http://www.w3.org/1999/xlink" xsi:type="string"',
            1,
        )
        linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.10.07: link:linkbase/@xsi:type MOET NIET gebruikt worden in 'validation/calculation_linkbase.xml'"
            ],
        )

    def test_nta_2_03_10_08(self):
        self.set_rule("2.03.10.08")
        url = Path(f"{conf.test['datadir']}/validation/calculation_linkbase.xml")
        contents, linkbase = self.get_xml(url)
        self.assert_xpath_ok(url=url, linkbase=linkbase, contents=contents)
        new_contents = contents.replace(
            'type="locator"',
            'type="locator" xlink:role="http://www.xbrl.org/unspecified"',
            2,
        )
        linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.10.08: link:loc/@xlink:role MOET NIET gebruikt worden in 'validation/calculation_linkbase.xml'"
            ],
        )

    def test_nta_2_03_10_09(self):
        self.set_rule("2.03.10.09")
        url = Path(
            f"{conf.test['datadir']}/validation/ocw-property-plant-equipment-axes-def.xml"
        )
        contents, linkbase = self.get_xml(url)
        self.assert_xpath_ok(url=url, linkbase=linkbase, contents=contents)
        new_contents = contents.replace(
            "link:roleRef",
            'link:roleRef xlink:arcrole="http://www.xbrl.org/unspecified"',
            1,
        )
        linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.10.09: link:roleRef/@xlink:arcrole MOET NIET gebruikt worden in 'validation/ocw-property-plant-equipment-axes-def.xml'"
            ],
        )

    def test_nta_2_03_10_10(self):
        self.set_rule("2.03.10.10")
        url = Path(
            f"{conf.test['datadir']}/validation/ocw-property-plant-equipment-axes-def.xml"
        )
        contents, linkbase = self.get_xml(url)
        self.assert_xpath_ok(url=url, linkbase=linkbase, contents=contents)
        new_contents = contents.replace(
            "link:roleRef",
            'link:roleRef xlink:role="http://www.xbrl.org/role/unspecified"',
            1,
        )
        linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.10.10: link:roleRef/@xlink:role MOET NIET gebruikt worden in 'validation/ocw-property-plant-equipment-axes-def.xml'"
            ],
        )

    def test_nta_2_03_10_11(self):
        self.set_rule("2.03.10.11")
        url = Path(f"{conf.test['datadir']}/presentation/test-linkroles-pre.xsd")
        contents, linkbase = self.get_xml(url)
        self.assert_xpath_ok(url=url, linkbase=linkbase, contents=contents)
        new_contents = contents.replace("link:roleType id=", "link:roleType xid=", 1)
        linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.10.11: link:roleType/@id MOET gebruikt worden in 'presentation/test-linkroles-pre.xsd'"
            ],
        )

    def test_nta_2_03_10_12(self):
        self.set_rule("2.03.10.12")
        url = Path(
            f"{conf.test['datadir']}/validation/ocw-property-plant-equipment-axes-def.xml"
        )
        contents, linkbase = self.get_xml(url)
        self.assert_xpath_ok(url=url, linkbase=linkbase, contents=contents)
        new_contents = contents.replace(
            "</link:definitionLink>",
            "<link:documentation>https://doc.notfound.org/404.html</link:documentation></link:definitionLink>",
            1,
        )
        linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_linkbase, level="ERROR") as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.linkbase:2.03.10.12: link:documentation MOET NIET gebruikt worden in 'validation/ocw-property-plant-equipment-axes-def.xml'"
            ],
        )

    def test_nta_3_02_11_03(self):
        self.set_rule("3.02.11.03")
        url = Path(
            f"{conf.test['datadir']}/validation/test-minimal-aangifte-lineitems-def.xml"
        )
        contents, linkbase = self.get_xml(url)
        self.assert_xpath_ok(url=url, linkbase=linkbase, contents=contents)
        new_contents = contents.replace(
            "ValueAddedTaxPrivateUse", "^V%$alueAddedTaxPrivateUse"
        )
        linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(
            self.logger_naming_conventions, level="ERROR"
        ) as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.naming_conventions:3.02.11.03: Een @xlink:label MOET alleen a-zA-Z0-9_-. tekens gebruiken bd_bd-i_^V%$alueAddedTaxPrivateUse_loc in 'validation/test-minimal-aangifte-lineitems-def.xml'"
            ],
        )

    def setup_validator(self):
        """
        We reuse the validator object as much as possible. Feeding it 1 rule ate a time
        """
        self.validator = Validator()
        self.all_linkbase_rules = self.validator.linkbase_rules
        self.all_model_rules = self.validator.model_rules

    # little helpers
    def assert_xpath_ok(self, url, linkbase, contents):
        with self.assertLogs(self.logger_linkbase, None) as validatorlog:
            self.validator.validate_linkbase(url=url, linkbase=linkbase, file=contents)
            # python 3.10 has assertNoLogs, until then we have to log a dummy message otherwise the test will fail
            self.logger_linkbase.warning("I'm alone")
        self.assertEqual(validatorlog.output, ["WARNING:validator.linkbase:I'm alone"])

    def get_xml(self, url):
        with open(url, "r") as file:
            contents = file.read()
        return contents, etree.fromstring(bytes(contents, encoding="utf-8"))

    def set_rule(self, rule_nr):
        self.validator.linkbase_rules = []
        for rule in self.all_linkbase_rules:
            if rule.rule_nr == rule_nr:
                self.validator.linkbase_rules = [rule]
