from pathlib import Path
from unittest import TestCase, skip
from os.path import join, exists
from os import remove, getenv
from psycopg2 import OperationalError
from discover_xbrl.sbrnl_modules.db.sqlite.db_connection import (
    DBConnection as SqliteConnection,
)
from discover_xbrl.sbrnl_modules.db.postgres.db_connection import (
    DBConnection as PostgresConnection,
)
from discover_xbrl.sbrnl_modules.db.connection import get_connection
from discover_xbrl.sbrnl_modules.db.model.concept import Concept
from discover_xbrl.sbrnl_modules.db.model.dts import DTS
from discover_xbrl.sbrnl_modules.db.model.itemtype import ItemType
from discover_xbrl.sbrnl_modules.db.model.namespace import Namespace
from discover_xbrl.conf.conf import Config

conf = Config("config.test.sqlite.yaml")


class TestSqlite(TestCase):
    testdb_dir = f"{conf.test['datadir']}/db"

    @classmethod
    def setUpClass(cls):
        Path(cls.testdb_dir).mkdir(exist_ok=True)
        cls.db = SqliteConnection(configfile="config.test.sqlite.yaml")
        ddl_file = Path(
            join(conf.project_root, "sbrnl_modules/db/sqlite/create_db.sql")
        )
        if exists(ddl_file):
            with open(ddl_file, "r") as sql:
                ddl = sql.read()
            if ddl:
                cls.cursor = cls.db.connection.cursor()
                cls.cursor.executescript(ddl)
            else:
                raise Exception(f"Could not read {ddl_file}")
        else:
            raise Exception(f"DDL File {ddl_file} not found")

    @classmethod
    def tearDownClass(cls):
        if exists(join(cls.testdb_dir, "taxonomies.db")):
            remove(join(cls.testdb_dir, "taxonomies.db"))

    def test_db_created(self):
        self.cursor.execute("select name from sqlite_master where type = 'table'")
        tables = self.cursor.fetchall()
        self.assertTrue(len(tables) > 0)

    def test_get_connection(self):
        dbconnection = get_connection()
        self.assertEqual(dbconnection.engine, "sqlite")

    def test_table_exists(self):
        self.cursor.execute("select name from sqlite_master where type = 'table'")
        tables = self.cursor.fetchall()
        self.assertEqual(len(tables), 54)

    def test_create_dts(self):
        dts = DTS(db=self.db, name="test_tax", shortname="tst", auto_commit=True)
        dts.find_or_create()
        self.cursor.execute("select name, shortname from dts")
        db_dts = self.cursor.fetchone()
        self.assertEqual(db_dts[0], "test_tax")

    def test_create_namespace(self):
        namespace = Namespace(
            db=self.db,
            prefix="logius",
            uri="http://logius.nl/ns",
            bind_to={"dts": "test_tax"},
        )
        namespace.find_or_create()
        self.cursor.execute("select prefix, uri from namespace")
        db_namespace = self.cursor.fetchone()
        self.assertEqual(db_namespace[0], "logius")
        self.cursor.execute("select count(*) from dts_namespace")
        bound = self.cursor.fetchall()
        self.assertEqual(len(bound), 1)

    def test_bad_nt_version(self):
        self.assertRaises(ValueError, SqliteConnection, **{"nt_version": "nt15"})

    def test_good_nt_version(self):
        con = SqliteConnection(nt_version="nt16")
        self.assertIsInstance(con, SqliteConnection)


class TestPostgres(TestCase):
    conf = Config("config.test.postgres.yaml")
    on_gitlab = getenv("GITLAB_CI", "false")

    @classmethod
    def setUpClass(cls):
        try:
            # hack
            if cls.on_gitlab == "true":
                cls.conf.db["host"] = "postgres"
            # hack
            cls.db = PostgresConnection(conf=cls.conf)
        except:
            print("Go on!")
            print("Got it")
        ddl_file = Path(
            join(cls.conf.project_root, "sbrnl_modules/db/postgres/create_db.sql")
        )
        if exists(ddl_file):
            with open(ddl_file, "r") as sql:
                ddl = sql.read()
            if ddl:
                cls.cursor = cls.db.connection.cursor()
                cls.cursor.execute(ddl)
                cls.db.connection.commit()
            else:
                raise Exception(f"Could not read {ddl_file}")
        else:
            raise Exception(f"DDL File {ddl_file} not found")

    @classmethod
    def tearDownClass(cls):
        cls.db.connection.rollback()  # this will overcome errors
        # connect to the default db, otherwise it won't work
        dbname_remove = cls.conf.db["dbname"]
        cls.db.switch_db("postgres")
        cls.db.connection.set_session(autocommit="on")
        cls.cursor = cls.db.connection.cursor()
        cls.cursor.execute(f"DROP DATABASE IF EXISTS {dbname_remove}")
        cls.db.connection.commit()

    def test_create_concept(self):
        """ Order of tests are almost random,
            so to create required foreign keys, it must be done in the same test """
        namespace = Namespace(
            db=self.db,
            prefix="kctst",
            uri="http://logius.nl/xbrl/test",
            auto_commit=True,
        )
        namespace.find_or_create()
        itemtype = ItemType(
            db=self.db,
            id="stringItemType",
            name="stringItemType",
            domain="",
            base="string",
            auto_commit=True,
        )
        itemtype.find_or_create()
        self.cursor.execute("select id, name, base from itemtype")
        db_itemtype = self.cursor.fetchone()
        self.assertEqual(db_itemtype[2], "string")

        concept = Concept(
            db=self.db,
            id="kctst_conceptOne",
            name="ConceptOne",
            balance=None,
            abstract=False,
            ns_prefix="kctst",
            namespace="http://logius.nl/xbrl/test",
            itemtype_name="stringItemType",
        )
        concept.find_or_create()
        self.cursor.execute(
            "select id, name from concept where name = %s", ("ConceptOne",)
        )
        db_concept = self.cursor.fetchone()
        self.assertEqual(db_concept[0], "kctst_conceptOne")

    def test_create_dts(self):
        dts = DTS(db=self.db, name="test_tax", shortname="tst", auto_commit=True)
        dts.find_or_create()
        self.cursor.execute("select name, shortname from dts")
        db_dts = self.cursor.fetchone()
        self.assertEqual(db_dts[0], "test_tax")

    def test_db_created(self):
        """
        The testdb gets automatically created inside setUpClass,
        Here we check if there are any tables
        """
        print()
        self.cursor.execute(
            "select count(*) from information_schema.tables where table_schema='public'"
        )
        tables = self.cursor.fetchone()
        self.assertTrue(tables[0] > 0)

    def test_bad_nt_version(self):
        self.assertRaises(ValueError, PostgresConnection, **{"nt_version": "nt15"})

    def test_good_nt_version(self):
        con = PostgresConnection(nt_version="nt16")
        self.assertIsInstance(con, PostgresConnection)
