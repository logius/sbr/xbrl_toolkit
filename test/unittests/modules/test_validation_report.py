from datetime import datetime, timedelta
from unittest import TestCase

from discover_xbrl.sbrnl_modules.validators.NT16.classes import ValidationRule
from discover_xbrl.sbrnl_modules.validators.NT16.validation_report import Report
from discover_xbrl.conf.conf import Config

conf = Config("config.test.nodb.yaml")


class TestValidationReport(TestCase):
    def test_create_report(self):
        report = Report("Validation report", "NT16")
        self.assertEqual("NT16", report.nt_version)
        self.assertLessEqual(datetime.now() - report.report_date, timedelta(100))

    def test_report_add_rule(self):
        report = Report("Validation report", "NT16")
        report.add_rule(
            ValidationRule(
                rule_nr="2.02.00.02",
                test_type="schema_tag",
                text="Een schema MOET alleen inhoud hebben gebaseerd op XML 1.0 van de W3C",
                tag="{http://www.w3.org/2001/XMLSchema}schema",
                note="Does check for tag, but not for extra info. Should check for allowed imports as well",
            )
        )
        self.assertEqual(len(report.rules), 1)

    def test_report_add_rule_violation(self):
        report = Report("Validation report", "NT16")
        rule = ValidationRule(
            rule_nr="2.02.00.02",
            test_type="schema_tag",
            text="Een schema MOET alleen inhoud hebben gebaseerd op XML 1.0 van de W3C",
            tag="{http://www.w3.org/2001/XMLSchema}schema",
            note="Does check for tag, but not for extra info. Should check for allowed imports as well",
        )
        report.add_rule(rule)
        report.add_rule_violation(rule, "validation/correct-schema.xsd")
        report.generate_report(writefile=False)
