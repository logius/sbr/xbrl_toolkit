import logging
from unittest import TestCase
from pathlib import Path
from lxml import etree
from discover_xbrl.sbrnl_modules.validators.NT16.namingconventions.files_directories import (
    nta_3_02_01_02,
    nta_3_02_01_03,
    nta_3_02_01_04,
    nta_3_02_01_05,
    nta_3_02_01_09,
    nta_3_02_01_10,
)
from discover_xbrl.sbrnl_modules.validators.NT16.namingconventions.namespace_uri import (
    nta_3_02_03_02,
    nta_3_02_03_04,
    nta_3_02_03_06,
)
from discover_xbrl.sbrnl_modules.validators.NT16.namingconventions.namespace_prefix import (
    nta_3_02_04_01,
    nta_3_02_04_05,
    nta_3_02_04_06,
)
from discover_xbrl.sbrnl_modules.validators.NT16.namingconventions.element_name import (
    nta_3_02_05_02,
    nta_3_02_05_04,
    nta_3_02_05_21,
)
from discover_xbrl.sbrnl_modules.validators.NT16.namingconventions.element_type_name import (
    nta_3_02_08_02,
    # nta_3_02_08_04,
    nta_3_02_08_06,
    nta_3_02_08_09,
)
from discover_xbrl.sbrnl_modules.validators.NT16.namingconventions.element_linkrole import (
    nta_3_02_09_02,
    nta_3_02_09_03,
    nta_3_02_09_04,
    nta_3_02_09_09,
    nta_3_02_10_02,
)
from discover_xbrl.sbrnl_modules.validators.NT16.namingconventions.element_xlink_label import (
    nta_3_02_11_01,
    nta_3_02_11_02,
)
from discover_xbrl.sbrnl_modules.validators.NT16.start import Validator
from discover_xbrl.conf.conf import Config


conf = Config("config.test.nodb.yaml")
logger_linkbase = logging.getLogger("validator.linkbase")


class AnObject(object):
    pass


class TestValidationNamingConventions(TestCase):
    """
    test validation rules
    Note: the validation tests return *True* if the rule is violated,this feels the wrong way round :-)
          just ask yourself Does this violate our rules?
    """

    def __init__(self, *args, **kwargs):
        super(TestValidationNamingConventions, self).__init__(*args, **kwargs)
        self.logger_linkbase = logging.getLogger("validator.linkbase")
        self.logger_schema = logging.getLogger("validator.schema")
        self.logger_naming_conventions = logging.getLogger(
            "validator.naming_conventions"
        )
        self.validator = None
        self.all_schema_rules = []
        self.all_linkbase_rules = []
        self.all_model_rules = []
        self.setup_validator()

    def test_nta_3_02_01_02(self):
        url = Path("/tmp/onlydirs/forthistest/lowercase.xml")
        self.assertEqual(False, nta_3_02_01_02(url))
        url = Path("/tmp/onlydirs/forthistest/UPPERcase.xml")
        self.assertEqual(False, nta_3_02_01_02(url))
        url = Path("/tmp/WrongDir/uppercase.xml")
        self.assertEqual(True, nta_3_02_01_02(url))
        url = Path("/tmp/spatie fout/DIT MAG NOG WEL.xml")
        self.assertEqual(True, nta_3_02_01_02(url))
        url = Path("/OUT_OF_SCOPE/nt14/20190911/kvk/presentation/bestand.xml")
        self.assertEqual(False, nta_3_02_01_02(url))

    def test_nta_3_02_01_03(self):
        url = Path("/tmp/thisisfine/lowercase.xml")
        self.assertEqual(False, nta_3_02_01_03(url))
        url = Path("/tmp/ridiciouslylongfoldername/lowercase.xml")
        self.assertEqual(True, nta_3_02_01_03(url))

    def test_nta_3_02_01_04(self):
        url = Path("/tmp/thisisfine/kvk/dictionary/some-datatypes.xml")
        self.assertEqual(False, nta_3_02_01_04(url))
        url = Path("/tmp/thisisfine/kvk/values/doesnotmatter.xml")
        self.assertEqual(True, nta_3_02_01_04(url))

    def test_nta_3_02_01_05(self):
        url = Path("/tmp/thisisfine/kvk/dictionary/some-datatypes.xml")
        self.assertEqual(False, nta_3_02_01_05(url))
        url = Path("/tmp/thisisfine/kvk/values/DOES matter.xml")
        self.assertEqual(True, nta_3_02_01_05(url))

    def test_nta_3_02_01_09(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-data-lab-nl.xml")
        contents, linkbase = self.get_xml(url)
        self.assertEqual(False, nta_3_02_01_09(url, linkbase))
        url = Path(f"{conf.test['datadir']}/dictionary/test-data-ref.xml")
        self.assertEqual(True, nta_3_02_01_09(url, linkbase))
        url = Path(
            f"{conf.test['datadir']}/validation/ocw-property-plant-equipment-axes-def.xml"
        )
        contents, linkbase = self.get_xml(url)
        self.assertEqual(False, nta_3_02_01_09(url, linkbase))
        url = Path(f"{conf.test['datadir']}/dictionary/test-data-ref.xml")
        self.assertEqual(True, nta_3_02_01_09(url, linkbase))
        contents, linkbase = self.get_xml(url)
        self.assertEqual(False, nta_3_02_01_09(url, linkbase))
        url = Path(f"{conf.test['datadir']}/dictionary/test-data-lab-nl.xml")
        self.assertEqual(True, nta_3_02_01_09(url, linkbase))
        url = Path(
            f"{conf.test['datadir']}/presentation/kvk-notes-presentationarc-pre.xml"
        )
        contents, linkbase = self.get_xml(url)
        self.assertEqual(False, nta_3_02_01_09(url, linkbase))
        url = Path(f"{conf.test['datadir']}/dictionary/test-data-lab-nl.xml")
        self.assertEqual(True, nta_3_02_01_09(url, linkbase))

    def test_nta_3_02_01_10(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-data-lab-nl.xml")
        contents, linkbase = self.get_xml(url)
        self.assertEqual(False, nta_3_02_01_10(url, linkbase))
        # TODO: Veel bestanden neerzetten met gevraagde genereic linkrole's
        # https://www.wikixl.nl/wiki/sbr/index.php/NT_Naamgeving#Tabel_B:_Generic_Linkbase_bestandsnaam_extensies

    def test_nta_3_02_03_02(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-data-lab-nl.xml")
        self.assertEqual(False, nta_3_02_03_02(url))
        url = Path(f"{conf.test['datadir']}/presentation/test-data-lab-nl.xml")
        self.assertEqual(False, nta_3_02_03_02(url))
        url = Path(f"{conf.test['datadir']}/validation/test-data-lab-nl.xml")
        self.assertEqual(True, nta_3_02_03_02(url))
        url = Path(f"{conf.test['datadir']}/static/test-data-lab-nl.xml")
        with self.assertLogs(
            self.logger_naming_conventions, level="ERROR"
        ) as validatorlog:
            self.assertEqual(True, nta_3_02_03_02(url))
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.naming_conventions:During check of NTA rule 3.02.03.02, Unknown directory: static"
            ],
        )

    def test_nta_3_02_03_03(self):
        url = Path(f"{conf.test['datadir']}/validation/correct_schema_file.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("3.02.03.03", "schema")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents, which=schema)
        new_contents = contents.replace(
            "bd-rpt-ob-aangifte-2021",
            "bd-rpt-ob-aangifte-2021-bd-rpt-ob-aangifte-2021-bd-rpt-ob-aangifte-2021-bd-rpt-ob-aangifte-2021-bd-rpt-ob-aangifte-2021-bd-rpt-ob-aangifte-2021-bd-rpt-ob-aangifte-2021bd-rpt-ob-aangifte-2022bd-rpt-ob-aangifte-2023",
        )
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:3.02.03.03: xs:schema/@targetNamespace MOET NIET meer dan 255 tekens zijn in 'validation/correct_schema_file.xsd'"
            ],
        )

    def test_nta_3_02_03_04(self):
        url = Path(f"{conf.test['datadir']}/validation/correct_schema_file.xsd")
        contents, schema = self.get_xml(url)
        self.assertEqual(False, nta_3_02_03_04(schema))
        new_contents = contents.replace(
            "bd-rpt-ob-aangifte-2021", "bd-rpt-ob?@aangifte2021"
        )
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        self.assertEqual(True, nta_3_02_03_04(schema))

    def test_nta_3_02_03_05(self):
        url = Path(f"{conf.test['datadir']}/validation/correct_schema_file.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("3.02.03.05", "schema")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents, which=schema)
        new_contents = contents.replace(
            "http://www.nltaxonomie.nl", "http://www.logius.nl/xbrl/taxonomy"
        )
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:3.02.03.05: xs:schema/@targetNamespace MOET met 'http://www.nltaxonomie.nl' beginnen in 'validation/correct_schema_file.xsd'"
            ],
        )

    def test_nta_3_02_03_06(self):
        url = Path(f"{conf.test['datadir']}/validation/correct_schema_file.xsd")
        contents, schema = self.get_xml(url)
        self.assertEqual(False, nta_3_02_03_06(schema))
        new_contents = contents.replace(
            "http://www.nltaxonomie.nl", "http://www.logius.nl/xbrl/taxonomy"
        )
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        self.assertEqual(True, nta_3_02_03_06(schema))

    def test_nta_3_02_04_01(self):
        url = Path(f"{conf.test['datadir']}/validation/correct_schema_file.xsd")
        contents, schema = self.get_xml(url)
        self.assertEqual(False, nta_3_02_04_01(schema))
        new_contents = contents.replace(
            "http://www.nltaxonomie.nl", "http://www.logius.nl/xbrl/taxonomy", 1
        )
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        self.assertEqual(True, nta_3_02_04_01(schema))

    def test_nta_3_02_04_05(self):
        url = Path(f"{conf.test['datadir']}/validation/correct_schema_file.xsd")
        contents, schema = self.get_xml(url)
        self.assertEqual(False, nta_3_02_04_05(schema))
        new_contents = contents.replace("bd-rpt-ob", "cljpn-rpt-ob")
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        self.assertEqual(True, nta_3_02_04_05(schema))

    def test_nta_3_02_04_06(self):
        url = Path(f"{conf.test['datadir']}/validation/correct_schema_file.xsd")
        contents, schema = self.get_xml(url)
        self.assertEqual(False, nta_3_02_04_06(schema))
        new_contents = contents.replace("bd-rpt-ob", "bd-rpt-ob-te-lang-is-warning")
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        self.assertEqual(True, nta_3_02_04_06(schema))

    def test_nta_3_02_05_02(self):
        c = AnObject()
        c.name = "UpperCamelCase"
        self.assertEqual(False, nta_3_02_05_02(c))
        c.name = "lowerCamelCase"
        self.assertEqual(True, nta_3_02_05_02(c))

    def test_nta_3_02_05_04(self):
        c = AnObject()
        c.name = "UpperCamelCase"
        self.assertEqual(False, nta_3_02_05_04(c))
        c.name = "U%p#perCamelCase"
        self.assertEqual(True, nta_3_02_05_04(c))

    def test_nta_3_02_05_21(self):
        c = AnObject()
        c.name = "UpperCamelCase"
        self.assertEqual(False, nta_3_02_05_21(c))
        c.name = "UpperCamelCaseUpperCamelCaseUpperCamelCaseUpperCamelCaseUpperCamelCaseUpperCamelCaseUpperCamelCaseUpperCamelCaseUpperCamelCaseUpperCamelCaseUpperCamelCaseUpperCamelCaseUpperCamelCaseUpperCamelCaseUpperCamelCase"
        self.assertEqual(True, nta_3_02_05_21(c))

    # validator's little helpers
    def get_xml(self, url):
        with open(url, "r") as file:
            contents = file.read()
        return contents, etree.fromstring(bytes(contents, encoding="utf-8"))

    def test_nta_3_02_08_02(self):
        c = AnObject()
        c.name = "UpperCamelCase"
        self.assertEqual(False, nta_3_02_08_02(c))
        c.name = "this1isn*tall0w3d%"
        self.assertEqual(True, nta_3_02_08_02(c))

    def test_nta_3_02_08_06(self):
        c = AnObject()
        c.name = "very_legit-float_ItemType"
        c.domain = "test_dm"
        self.assertEqual(False, nta_3_02_08_06(c))
        c.name = "not_so_legit_enum_OwnType"
        self.assertEqual(True, nta_3_02_08_06(c))

    def test_nta_3_02_08_09(self):
        c = AnObject()
        c.name = "lowerCamelCase"
        c.domain = "test_dm"
        self.assertEqual(False, nta_3_02_08_09(c))
        c.name = "UpperCamelCase"
        self.assertEqual(True, nta_3_02_08_09(c))

    def test_nta_3_02_09_02(self):
        c = AnObject()
        c.roleURI = "urn:bd:linkrole:bd-hd-lr-hcb-ps"
        self.assertEqual(False, nta_3_02_09_02(c))
        c.roleURI = "urn:bd:linkRole:BD-hd-lr-hcb-ps"
        self.assertEqual(True, nta_3_02_09_02(c))
        c.roleURI = "http://www.xbrl.org/2008/role/verboseLabel"
        self.assertEqual(False, nta_3_02_09_02(c))

    def test_nta_3_02_09_03(self):
        c = AnObject()
        c.roleURI = "urn:bd:linkrole:bd-hd-lr-hcb-ps"
        self.assertEqual(False, nta_3_02_09_03(c))
        c.roleURI = "urn:bd:linkrole:this1isn*tall0w3d%"
        self.assertEqual(True, nta_3_02_09_03(c))

    def test_nta_3_02_09_04(self):
        c = AnObject()
        c.roleURI = "urn:bd:linkrole:bd-hd-lr-hcb-ps"
        self.assertEqual(False, nta_3_02_09_04(c))
        c.roleURI = "urn:bd:linkrole:bd-hd-lr-hcb-psurn:bd:linkrole:bd-hd-lr-hcb-psurn:bd:linkrole:bd-hd-lr-hcb-psurn:bd:linkrole:bd-hd-lr-hcb-psurn:bd:linkrole:bd-hd-lr-hcb-psurn:bd:linkrole:bd-hd-lr-hcb-psurn:bd:linkrole:bd-hd-lr-hcb-psurn:bd:linkrole:bd-hd-lr-hcb-psurn:bd:linkrole:bd-hd-lr-hcb-ps"
        self.assertEqual(True, nta_3_02_09_04(c))

    def test_nta_3_02_09_09(self):
        c = AnObject()
        c.roleURI = "urn:bd:linkrole:bd-hd-lr-hcb-ps"
        self.assertEqual(False, nta_3_02_09_09(c))
        c.roleURI = "urn:cljpn:linkrole:i-m-a-hypercube"
        self.assertEqual(True, nta_3_02_09_09(c))
        c.roleURI = "uri:kvk:linkrole:i-m-a-hypercube"
        self.assertEqual(True, nta_3_02_09_09(c))
        c.roleURI = "urn:kvk:role:i-m-a-hypercube"
        self.assertEqual(True, nta_3_02_09_09(c))

    def test_nta_3_02_10_02(self):
        c = AnObject()
        c.id = "linkrole_bd-hd-lr-hcb-ps"
        self.assertEqual(False, nta_3_02_10_02(c))
        c.id = "linkrole_bd-hd-lr-hcb-pslinkrole_bd-hd-lr-hcb-pslinkrole_bd-hd-lr-hcb-pslinkrole_bd-hd-lr-hcb-pslinkrole_bd-hd-lr-hcb-pslinkrole_bd-hd-lr-hcb-pslinkrole_bd-hd-lr-hcb-pslinkrole_bd-hd-lr-hcb-pslinkrole_bd-hd-lr-hcb-pslinkrole_bd-hd-lr-hcb-pslinkrole_bd-hd-lr-hcb-pslinkrole_bd-hd-lr-hcb-ps"
        self.assertEqual(True, nta_3_02_10_02(c))

    def test_nta_3_02_11_01(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-codes-generic-lab-nl.xml")
        contents, linkbase = self.get_xml(url)
        self.assertEqual(False, nta_3_02_11_01(linkbase))
        new_contents = contents.replace("bd_8fff0", "bd_4444", 1)
        linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        self.assertEqual(True, nta_3_02_11_01(linkbase))

    def test_nta_3_02_11_02(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-data-ref.xml")
        contents, linkbase = self.get_xml(url)
        self.assertEqual(False, nta_3_02_11_02(linkbase))
        new_contents = contents.replace("bd_d2bff930", "bd_4444f930", 1)
        linkbase = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        self.assertEqual(True, nta_3_02_11_02(linkbase))

    def setup_validator(self):
        """
        We reuse the validator object as much as possible. Feeding it 1 rule ate a time
        """
        self.validator = Validator()
        self.all_linkbase_rules = self.validator.linkbase_rules
        self.all_model_rules = self.validator.model_rules
        self.all_schema_rules = self.validator.schema_rules

    def assert_xpath_ok(self, url, schema, contents, which):
        with self.assertLogs(self.logger_naming_conventions, None) as validatorlog:
            if which == "schema":
                self.validator.validate_schema(url=url, schema=schema, file=contents)
            # python 3.10 has assertNoLogs, until then we have to log a dummy message otherwise the test will fail
            self.logger_naming_conventions.warning("I'm alone")
        self.assertEqual(
            validatorlog.output, ["WARNING:validator.naming_conventions:I'm alone"]
        )

    def set_rule(self, rule_nr, which):
        self.validator.linkbase_rules = []
        self.validator.schema_rules = []
        for rule in self.all_linkbase_rules:
            if rule.rule_nr == rule_nr:
                if which in ["schema", "both"]:
                    self.validator.schema_rules = [rule]
                elif which in ["linkbase", "both"]:
                    self.validator.linkbase_rules = [rule]
        for rule in self.all_schema_rules:
            if rule.rule_nr == rule_nr:
                if which in ["schema", "both"]:
                    self.validator.schema_rules = [rule]
                elif which in ["linkbase", "both"]:
                    self.validator.linkbase_rules = [rule]


# niet te testen
# 3.02.01.06
# 3.02.01.07
# 3.02.01.08
# 3.02.01.11
# 3.02.01.12
# 3.02.01.14
# 3.02.01.15
