import os.path as path
import logging
from unittest import TestCase, skip
from unittest.mock import patch
from pathlib import Path
from lxml import etree
from discover_xbrl.sbrnl_modules.validators.NT16.generic.generic import (
    nta_2_01_00_06,
    nta_2_01_00_09,
    nta_2_01_00_11,
)
from discover_xbrl.sbrnl_xbrl_parser.linkbase.classes import Label
from discover_xbrl.sbrnl_xbrl_parser.schema.classes import Concept
from discover_xbrl.sbrnl_modules.validators.NT16.schema.base import (
    nta_2_02_00_03,
    nta_2_02_00_05,
    nta_2_02_00_07,
)
from discover_xbrl.sbrnl_modules.validators.NT16.schema.xlink import (
    nta_2_02_01_03,
    nta_2_02_01_05,
)
from discover_xbrl.sbrnl_modules.validators.NT16.start import Validator
from discover_xbrl.conf.conf import Config

logger_schema = logging.getLogger("validator.schema")
conf = Config("config.test.nodb.yaml")


class TestValidation(TestCase):
    """test validation rules
    Note: the validation tests return *True* if the rule is violated,this feels the wrong way round :-)
          just ask yourself Does this violate our rules?
    """

    def __init__(self, *args, **kwargs):
        super(TestValidation, self).__init__(*args, **kwargs)
        self.logger_schema = logging.getLogger("validator.schema")
        self.validator = None
        self.all_schema_rules = []
        self.all_linkbase_rules = []
        self.all_model_rules = []
        self.setup_validator()

    # Part I
    # 2.01.00.xx Generic rules

    def test_nta_2_01_00_06(self):
        outcome = nta_2_01_00_06("xlink")
        self.assertEqual(outcome, False)
        outcome = nta_2_01_00_06("dcterms")
        self.assertEqual(outcome, True)

    def test_nta_2_01_00_09(self):
        filename = Path(
            f"{conf.test['datadir']}/validation/test-minimal-aangifte-hypercube-dimension-def.xml"
        )
        outcome = nta_2_01_00_09(filename)
        self.assertEqual(outcome, False)
        filename = Path(f"{conf.test['datadir']}/validation/file_with_bom.xml")
        outcome = nta_2_01_00_09(filename)
        self.assertEqual(outcome, True)

    def test_nta_2_01_00_11(self):
        filename = Path(
            f"{conf.test['datadir']}/validation/test-minimal-aangifte-hypercube-dimension-def.xml"
        )
        outcome = nta_2_01_00_11(filename)
        self.assertEqual(outcome, False)
        filename = Path(
            f"{conf.test['datadir']}/i-am-a-file-wth-a-very-very-very-long-filename-which-can-exists-on-your-drive-but-not-in-the-world-famous-dutch-taxonmy-you-cant-do-that-period.xsd"
        )
        outcome = nta_2_01_00_11(filename)
        self.assertEqual(outcome, True)

    # Part II
    # 2.02.00.xx
    def test_nta_2_02_00_02(self):
        url = Path(f"{conf.test['datadir']}/validation/correct_schema_file.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.00.02")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)

        new_contents = contents.replace(
            "http://www.w3.org/2001/XMLSchema", "http://www.w3.org/2011/XMLSchema"
        )
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlogs:
            self.validator.validate_schema(url=url, schema=schema, file=contents)

    def test_nta_2_02_00_03(self):
        with open(
            Path(
                f"{conf.test['datadir']}/validation/test-minimal-aangifte-hypercube-dimension-def.xml"
            ),
            "r",
        ) as file:
            contents = file.read(1024)
            outcome = nta_2_02_00_03(contents)
            self.assertEqual(outcome, False)
        contents = (
            '<?xml version="1.1" Encoding="uTF-8" standalone="yes"?>'
        )  # ugly but acceptable
        outcome = nta_2_02_00_03(contents)
        self.assertEqual(outcome, False)
        contents = '<?xml version="1.1" Encoding="uTF-16" standalone="yes"?>'  # No! No!
        outcome = nta_2_02_00_03(contents)
        self.assertEqual(outcome, True)

    def test_nta_2_02_00_05(self):
        with open(
            Path(
                f"{conf.test['datadir']}/validation/test-minimal-aangifte-hypercube-dimension-def.xml"
            ),
            "r",
        ) as file:
            contents = file.read(2024)
            outcome = nta_2_02_00_05(contents)
            self.assertEqual(outcome, False)
        with open(
            Path(f"{conf.test['datadir']}/validation/file_with_too_many_comments.xml"),
            "r",
        ) as file:
            contents = file.read(2024)
            outcome = nta_2_02_00_05(contents)
            self.assertEqual(outcome, True)

    def test_nta_2_02_00_06(self):
        url = Path(f"{conf.test['datadir']}/validation/correct_schema_file.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.00.06")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)

        new_contents = contents.replace(
            "xs:appinfo", "appinfo"
        )  # remove namespace from an element
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlogs:
            self.validator.validate_schema(url=url, schema=schema, file=contents)

    def test_nta_2_02_00_07(self):
        with open(
            Path(f"{conf.test['datadir']}/validation/correct_schema_file.xsd"), "r"
        ) as file:
            contents = file.read(2024)
            outcome = nta_2_02_00_07(contents)
            self.assertEqual(outcome, False)
        with open(
            Path(f"{conf.test['datadir']}/validation/schema_wrong_xs_location.xsd"), "r"
        ) as file:
            contents = file.read(2024)
            outcome = nta_2_02_00_07(contents)
            self.assertEqual(outcome, True)

    def test_nta_2_02_00_08(self):
        url = Path(f"{conf.test['datadir']}/validation/correct_schema_file.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.00.08")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)

        new_contents = contents.replace(
            'targetNamespace="http://www.nltaxonomie.nl/nt15/bd/20201209.a/entrypoints/bd-rpt-ob-aangifte-2021"',
            "",
        )
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.00.08: Een schema MOET een @targetNamespace hebben in 'validation/correct_schema_file.xsd'"
            ],
        )

    def test_nta_2_02_00_09(self):
        url = Path(f"{conf.test['datadir']}/validation/correct_schema_file.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.00.09")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)

        new_contents = contents.replace(
            'elementFormDefault="qualified"', 'elementFormDefault="unqualified"'
        )
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.00.09: Een schema MOET @attributeFormDefault en @elementFormDefault met de waarden 'unqualified' en 'qualified' respectievelijk bevatten in 'validation/correct_schema_file.xsd'"
            ],
        )

    def test_nta_2_02_00_10(self):
        url = Path(f"{conf.test['datadir']}/validation/correct_schema_file.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.00.10")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)

        new_contents = contents.replace(
            'id="entrypoint-minimal"', 'id="entrypoint-minimal" blockDefault="#all"'
        )
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=new_contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.00.10: Een schema MOET NIET @blockDefault, @finalDefault en @version vermelden in 'validation/correct_schema_file.xsd'"
            ],
        )

    def test_nta_2_02_00_12(self):
        url = Path(f"{conf.test['datadir']}/validation/correct_schema_file.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.00.12")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)

        new_contents = contents.replace(
            "<xs:annotation", "<xs:author>Rembrandt</xs:author>" "<xs:annotation"
        )
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=new_contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.00.12: Een schema waar linkroles of arcroles gedefinieerd worden of linkbases gekoppeld worden, MOET de node waarin dit gebeurd (<xs:annotation><xs:appinfo>) direct achter de root node hebben in 'validation/correct_schema_file.xsd'"
            ],
        )

    def test_nta_2_02_00_14(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-data.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.00.14")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)

        new_contents = contents.replace(
            "</xs:annotation>", "</xs:annotation> <xs:author>Rembrandt</xs:author>"
        )
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=new_contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.00.14: Een schema dat <xs:import> nodes gebruikt MOET deze direct achter de <xs:annotation><xs:appinfo> node opnemen in 'dictionary/test-data.xsd'"
            ],
        )

    def test_nta_2_02_00_16(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-data.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.00.16")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)

        new_contents = contents.replace(
            "http://www.nltaxonomie.nl/nt15/bd/20201209.a/dictionary/bd-types",
            "http://www.schema.org/dictionary/types",
        )
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=new_contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.00.16: xs:schema/xs:import/@schemaLocation MOET gebruik maken van absolute URIs voor bestanden buiten een versie van de NT of buiten de PE-schema in 'dictionary/test-data.xsd'"
            ],
        )

    def test_nta_2_02_00_17(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-data.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.00.17")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)

        new_contents = contents.replace(
            "./test-types.xsd",
            "http://www.nltaxonomie.nl/nt15/bd/20201209.a/dictionary/bd-types/test-types.xsd",
        )
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=new_contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.00.17: xs:schema/xs:import/@schemaLocation MOET gebruik maken van relatieve URIs voor bestanden binnen een versie van de NT of binnen de PE-schema in 'dictionary/test-data.xsd'"
            ],
        )

    def test_nta_2_02_00_18(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-data.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.00.18")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)

        new_contents = contents.replace("xs:import", "xs:include")
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=new_contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.00.18: xs:schema/xs:include MOET NIET gebruikt worden in 'dictionary/test-data.xsd'"
            ],
        )

    def test_nta_2_02_00_19(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-data.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.00.19")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)
        schema.nsmap["xml"] = "http://www.w3.org/XML/1998/namespace"
        elems = schema.xpath("//xs:element", namespaces=schema.nsmap)
        elems[0].attrib[
            "{http://www.w3.org/XML/1998/namespace}base"
        ] = "http://schema.org/"
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.00.19: Een schema MOET NIET namespaceprefixes declareren op element niveau in 'dictionary/test-data.xsd'"
            ],
        )

    def test_nta_2_02_00_22(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-data.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.00.22")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)
        annotation = etree.Element("{http://www.w3.org/2001/XMLSchema}annotation")
        schema.insert(2, annotation)
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.00.22: Er mag slechts één <xs:annotation> node in een schema bestand voorkomen in 'dictionary/test-data.xsd'"
            ],
        )

    def test_nta_2_02_00_23(self):
        url = Path(f"{conf.test['datadir']}/validation/correct_schema_file.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.00.23")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)

        new_contents = contents.replace('id="entrypoint-minimal"', "")
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=new_contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.00.23: Een entrypoint xs:schema MOET een @id hebben. in 'validation/correct_schema_file.xsd'"
            ],
        )

    def test_nta_2_02_00_27(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-codes.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.00.27")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)

        new_contents = contents.replace(
            "</xs:appinfo>",
            '<link:linkbaseRef xlink:href="./test-codes-generic-lab-nl.xml" xlink:role="documentation" xlink:arcrole="http://www.w3.org/1999/xlink/properties/linkbase" xlink:type="simple" /></xs:appinfo>',
        )
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=new_contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.00.27: Een schema MOET unieke linkbaseRefs bevatten. in 'dictionary/test-codes.xsd'"
            ],
        )

    #
    # 2.02.01.xx xlink
    #
    def test_nta_2_02_01_01(self):
        url = Path(f"{conf.test['datadir']}/presentation/test-abstracts.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.01.01")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)
        new_contents = contents.replace('abstract="true"', 'abstract="false"', 1)
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=new_contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.01.01: Een schema MOET één functie ondersteunen: (opsomming) in 'presentation/test-abstracts.xsd'"
            ],
        )
        elem = etree.Element("{http://www.w3.org/2001/XMLSchema}simpleType")
        elem.attrib["name"] = "nonNegativeInteger"
        schema.insert(3, elem)
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=new_contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.01.01: Een schema MOET één functie ondersteunen: (opsomming) in 'presentation/test-abstracts.xsd'"
            ],
        )

    def test_nta_2_02_01_03(self):
        lab_nl = Label(
            id="jenv-bw2-dm_AccidentsAndIllnessesMember_label_nl",
            language="nl",
            xlink_label="jenv-bw2-dm_AccidentsAndIllnessesMember_label_nl",
            text="Ongevallen en ziekte",
            linkRole="http://www.xbrl.org/2003/role/documentation",
        )
        concept = Concept(
            id="jenv-bw2-dm_AccidentsAndIllnessesMember",
            name="AccidentsAndIllnessesMember",
            nillable="false",
            labels={"jenv-bw2-dm_AccidentsAndIllnessesMember_label_nl": lab_nl},
            is_abstract="false",
        )
        self.assertFalse(nta_2_02_01_03(concept=concept))
        lab_nl = Label(
            id="jenv-bw2-dm_AccidentsAndIllnessesMember_label_nl",
            language="nl",
            xlink_label="jenv-bw2-dm_AccidentsAndIllnessesMember_label_nl",
            text="Ongevallen en ziekte",
            linkRole="http://www.xbrl.org/2003/role/label",
        )
        concept.labels = {"jenv-bw2-dm_AccidentsAndIllnessesMember_label_nl": lab_nl}
        self.assertTrue(nta_2_02_01_03(concept=concept))

    def test_nta_02_01_04(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-codesII.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.01.04")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)
        new_contents = contents.replace(
            'xlink:arcrole="http://www.w3.org/1999/xlink/properties/linkbase"',
            'xlink:arcrole="http://www.xbrl.org/2003/role/referenceLinkbaseRef"',
        )
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=new_contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.01.04: Elk schema waarin XML nodes gedefineerd worden MOET een (generic) label linkbase gelinkt hebben in 'dictionary/test-codesII.xsd'"
            ],
        )

    def test_nta_2_02_01_05(self):
        lab_en = Label(
            id="jenv-bw2-dm_AccidentsAndIllnessesMember_label_en",
            language="en",
            xlink_label="jenv-bw2-dm_AccidentsAndIllnessesMember_label_en",
            text="Accidents and illnesses",
            linkRole="http://www.xbrl.org/2003/role/label",
        )
        lab_nl = Label(
            id="jenv-bw2-dm_AccidentsAndIllnessesMember_label_nl",
            language="nl",
            xlink_label="jenv-bw2-dm_AccidentsAndIllnessesMember_label_nl",
            text="Ongevallen en ziekte",
            linkRole="http://www.xbrl.org/2003/role/label",
        )
        concept = Concept(
            id="jenv-bw2-dm_AccidentsAndIllnessesMember",
            name="AccidentsAndIllnessesMember",
            nillable="false",
            labels={
                "jenv-bw2-dm_AccidentsAndIllnessesMember_label_en": lab_en,
                "jenv-bw2-dm_AccidentsAndIllnessesMember_label_nl": lab_nl,
            },
            is_abstract="false",
        )
        self.assertFalse(nta_2_02_01_05(concept=concept))
        concept = Concept(
            id="jenv-bw2-dm_AccidentsAndIllnessesMember",
            name="AccidentsAndIllnessesMember",
            nillable="false",
            labels={
                "jenv-bw2-dm_AccidentsAndIllnessesMember_label_en": lab_en,
                "jenv-bw2-dm_AccidentsAndIllnessesMember_label_nl": lab_nl,
                "jenv-bw2-dm_AccidentsAndIllnessesMember_label_de": lab_en,
            },
            is_abstract="false",
        )
        self.assertTrue(nta_2_02_01_05(concept=concept))

    #
    # 2.02.02.xx Concepts
    #
    def test_nta_2_02_02_01(self):
        url = Path(f"{conf.test['datadir']}/presentation/test-abstracts.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.02.01")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)
        new_contents = contents.replace(
            "</xs:schema>",
            "<xs:wrong>"
            '<xs:element id="bd-abstr_wrong_location" name="AbstractWrongLocation" type="xbrli:stringItemType">'
            "</xs:element></xs:wrong></xs:schema>",
        )
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=new_contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.02.01: Concept definities MOETEN op root level in een schema plaats vinden in 'presentation/test-abstracts.xsd'"
            ],
        )

    def test_nta_2_02_02_03(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-tuples.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.02.03")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)
        new_contents = contents.replace('abstract="false"', 'abstract="true"')
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=new_contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.02.03: Abstracte tuples MOETEN NIET voorkomen in 'dictionary/test-tuples.xsd'"
            ],
        )

    def test_nta_2_02_02_05(self):
        url = Path(f"{conf.test['datadir']}/presentation/test-abstracts.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.02.05")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)
        new_contents = contents.replace(
            "sbr:presentationItem", "kvk:presentationItem", 1
        )
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=new_contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.02.05: <xs:element> MOET NIET gebruikt worden om nieuwe abstract elementen te maken die als substitutionGroup voor andere elementen dienen UITGEZONDERD bij SBR-NT-beheer voor de NT in 'presentation/test-abstracts.xsd'"
            ],
        )
        url = Path(f"{conf.test['datadir']}/dictionary/ocw-wnt-domains.xsd")
        contents, schema = self.get_xml(url)
        new_contents = contents.replace('abstract="false"', 'abstract="true"')
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=new_contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.02.05: <xs:element> MOET NIET gebruikt worden om nieuwe abstract elementen te maken die als substitutionGroup voor andere elementen dienen UITGEZONDERD bij SBR-NT-beheer voor de NT in 'dictionary/ocw-wnt-domains.xsd'"
            ],
        )

    def test_nta_2_02_02_08(self):
        url = Path(f"{conf.test['datadir']}/presentation/test-abstracts.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.02.08")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)
        new_contents = contents.replace('abstract="true"', "")
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=new_contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.02.08: xs:schema/xs:element/@abstract is verplicht in 'presentation/test-abstracts.xsd'"
            ],
        )

    def test_nta_2_02_02_09(self):
        url = Path(f"{conf.test['datadir']}/presentation/test-abstracts.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.02.09")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)
        new_contents = contents.replace(
            'abstract="true"', 'abstract="true" block="extension"'
        )
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=new_contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.02.09: //xs:element/@block is NIET toegestaan in 'presentation/test-abstracts.xsd'"
            ],
        )

    def test_nta_2_02_02_10(self):
        url = Path(f"{conf.test['datadir']}/presentation/test-abstracts.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.02.10")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)
        new_contents = contents.replace(
            'abstract="true"', 'abstract="true" final="#all"'
        )
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=new_contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.02.10: //xs:element/@final is NIET toegestaan in 'presentation/test-abstracts.xsd'"
            ],
        )

    def test_nta_2_02_02_11(self):
        url = Path(f"{conf.test['datadir']}/presentation/test-abstracts.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.02.11")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)
        new_contents = contents.replace(
            'abstract="true"', 'abstract="false" fixed="some value"'
        )
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=new_contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.02.11: //xs:element/@fixed is NIET toegestaan in 'presentation/test-abstracts.xsd'"
            ],
        )

    def test_nta_2_02_02_13(self):
        url = Path(f"{conf.test['datadir']}/presentation/test-abstracts.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.02.13")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)

        new_contents = contents.replace('id="bd-abstr_TaxConsultantDataTitle"', "")
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=new_contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.02.13: //xs:element/@id is verplicht in 'presentation/test-abstracts.xsd'"
            ],
        )

    def test_nta_2_02_02_15(self):
        url = Path(f"{conf.test['datadir']}/presentation/test-abstracts.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.02.15")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)

        new_contents = contents.replace('nillable="false"', "")
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=new_contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.02.15: //xs:element/@nillable is verplicht in 'presentation/test-abstracts.xsd'"
            ],
        )

    def test_nta_2_02_02_16(self):
        url = Path(f"{conf.test['datadir']}/presentation/test-abstracts.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.02.16")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)

        new_contents = contents.replace('nillable="false"', "")
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=new_contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.02.16: xs:schema/xs:element/@nillable=’false’ MOET gebruikt worden als xs:schema/xs:element/@abstract=’true’ in 'presentation/test-abstracts.xsd'"
            ],
        )

    def test_nta_2_02_02_17(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-tuple.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.02.17")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)

        new_contents = contents.replace('nillable="false"', "")
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=new_contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.02.17: xs:schema/xs:element/@nillable=’false’ MOET gebruikt worden als xs:schema/xs:element/@substitutionGroup=’xbrli:tuple’ en zijn afgeleidden in 'dictionary/test-tuple.xsd'"
            ],
        )

    def test_nta_2_02_02_18(self):
        url = Path(f"{conf.test['datadir']}/presentation/test-abstracts.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.02.18")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)

        new_contents = contents.replace('substitutionGroup="sbr:presentationItem"', "")
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=new_contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.02.18: @substitutionGroup MOET gebruikt worden op root <xs:element> die concepten zijn in 'presentation/test-abstracts.xsd'"
            ],
        )

    def test_nta_2_02_02_21(self):
        url = Path(f"{conf.test['datadir']}/presentation/test-abstracts.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.02.21")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)

        new_contents = contents.replace("xbrli:stringItemType", "xbrli:facet")
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=new_contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.02.21: xs:schema/xs:element/@type='xbrli:stringItemType' als xs:schema/xs:element/@abstract='true' in 'presentation/test-abstracts.xsd'"
            ],
        )

    def test_nta_2_02_02_25(self):
        url = Path(
            f"{conf.test['datadir']}/validation/xbrl-syntax-extension-corrected.xsd"
        )
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.02.25")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)

        url = Path(f"{conf.test['datadir']}/validation/xbrl-syntax-extension.xsd")
        new_contents, schema = self.get_xml(url)
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=new_contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.02.25: xs:schema/xs:element/@xbrli:periodType MOET 'duration' zijn voor niet-rapporteerbare items. Voor rapporteerbare items BEHOORT het 'duration' te zijn, UITGEZONDERD rapporteerbare items die op een tijdstip gerapporteerd worden. in 'validation/xbrl-syntax-extension.xsd'"
            ],
        )

    def test_nta_2_02_02_35(self):
        url = Path(f"{conf.test['datadir']}/presentation/test-abstracts.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.02.35")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)

        new_contents = contents.replace(
            "xbrli:stringItemType", "nl-types:imageItemType"
        )
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=new_contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.02.35: Een concept mag niet gebaseerd zijn op het type nl-types:imageItemType. in 'presentation/test-abstracts.xsd'"
            ],
        )

    #
    # 2.02.03.xx
    # schema_linkroles

    def test_nta_2_02_03_01(self):
        # TODO. model-test
        pass

    def test_nta_2_02_03_02(self):
        # TODO. model-test
        pass

    def test_nta_2_02_03_03(self):
        # TODO. model-test
        pass

    #
    # 2.02.04.01
    # ArcRoles

    def test_nta_2_02_04_01(self):
        url = Path(
            f"{conf.test['datadir']}/validation/test-minimal-linkroles-tables.xsd"
        )
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.04.01")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)
        appinfo = schema.xpath("//xs:appinfo", namespaces=schema.nsmap)
        arcroletype = etree.SubElement(
            appinfo[0], "{http://www.xbrl.org/2003/linkbase}arcroleType"
        )
        etree.SubElement(arcroletype, "{http://www.xbrl.org/2003/linkbase}definition")
        etree.SubElement(arcroletype, "{http://www.xbrl.org/2003/linkbase}usedOn")
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.04.01: Arcroles MOETEN NIET aangemaakt worden in 'validation/test-minimal-linkroles-tables.xsd'"
            ],
        )

    #
    # 2.02.05.01
    # Linkparts
    #
    def test_nta_2_02_05_01(self):
        url = Path(f"{conf.test['datadir']}/presentation/test-abstracts.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.05.01")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)

        new_contents = contents.replace(
            'substitutionGroup="sbr:presentationItem"', 'substitutionGroup="link:part"'
        )
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=new_contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.05.01: Reference resource parts MOETEN NIET door NT Partners aangemaakt worden in 'presentation/test-abstracts.xsd'"
            ],
        )

    # 2.02.06.01
    # Context
    # Short message: Just don't
    def test_nta_2_02_06_03(self):
        url = Path(f"{conf.test['datadir']}/presentation/test-abstracts.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.06.03")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)
        root = schema.xpath("//xs:schema", namespaces=schema.nsmap)
        context = etree.SubElement(root[0], "{http://www.w3.org/2001/XMLSchema}element")
        context.attrib["name"] = "context"

        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.06.03: Context elementen MOETEN NIET worden aangemaakt in 'presentation/test-abstracts.xsd'"
            ],
        )

    # 2.02.07.xx
    # type_enumerations
    #
    def test_nta_2_02_07_02(self):
        url = Path(f"{conf.test['datadir']}/validation/xbrl-syntax-extension.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.07.02")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)
        url = Path(f"{conf.test['datadir']}/dictionary/test-types.xsd")
        contents, schema = self.get_xml(url)
        with self.assertLogs(self.logger_schema, level="WARNING") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "WARNING:validator.schema:2.02.07.02: Lengte restricties op types BEHOREN niet gebruikt te worden (gebruik business rules) in 'dictionary/test-types.xsd'"
            ],
        )

    def test_nta_2_02_07_03(self):
        url = Path(f"{conf.test['datadir']}/validation/xbrl-syntax-extension.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.07.03")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)
        url = Path(f"{conf.test['datadir']}/dictionary/test-codes.xsd")
        contents, schema = self.get_xml(url)
        with self.assertLogs(self.logger_schema, level="WARNING") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "WARNING:validator.schema:2.02.07.03: Enumeraties BEHOREN niet gebruikt te worden (gebruik domein) in 'dictionary/test-codes.xsd'"
            ],
        )

    def test_nta_2_02_07_04(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-codes.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.07.04")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)
        new_contents = contents.replace("xbrli:stringItemType", "xbrli:pureItemType", 1)
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_schema, level="WARNING") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.07.04: Enumeraties MOETEN xbrli:stringItemType gebaseerd zijn in 'dictionary/test-codes.xsd'"
            ],
        )

    #
    # 2.02.10.xx
    # Entrypoints
    # veel niet testbaar
    def test_nta_2_02_10_01(self):
        url = Path(f"{conf.test['datadir']}/entrypoints/entrypoint.minimal.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.10.01")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)
        new_contents = contents.replace("/presentation/", "/dictionary/")
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.10.01: Een entrypoint schema in een DTS MOET presentatie linkbase(s) omvatten in 'entrypoints/entrypoint.minimal.xsd'"
            ],
        )

    #
    # 2.02.11.xx
    # Other
    def test_nta_2_02_11_01(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-codes.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.11.01")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)
        root = schema.xpath("//xs:schema", namespaces=schema.nsmap)
        etree.SubElement(root[0], "{http://www.w3.org/2001/XMLSchema}all")
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.11.01: <xs:all> MOET NIET gebruikt worden in 'dictionary/test-codes.xsd'"
            ],
        )

    def test_nta_2_02_11_02(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-codes.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.11.02")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)
        annotation = schema.xpath("//xs:schema/xs:annotation", namespaces=schema.nsmap)
        etree.SubElement(
            annotation[0], "{http://www.w3.org/2001/XMLSchema}documentation"
        )
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.11.02: <xs:annotation><xs:documentation> MOET NIET gebruikt worden in 'dictionary/test-codes.xsd'"
            ],
        )

    def test_nta_2_02_11_03(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-codes.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.11.03")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)
        root = schema.xpath("//xs:schema", namespaces=schema.nsmap)
        etree.SubElement(root[0], "{http://www.w3.org/2001/XMLSchema}any")
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.11.03: <xs:any> MOET NIET gebruikt worden in 'dictionary/test-codes.xsd'"
            ],
        )

    def test_nta_2_02_11_04(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-codes.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.11.04")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)
        root = schema.xpath("//xs:schema", namespaces=schema.nsmap)
        etree.SubElement(root[0], "{http://www.w3.org/2001/XMLSchema}anyAttribute")
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.11.04: <xs:anyAttribute> MOET NIET gebruikt worden in 'dictionary/test-codes.xsd'"
            ],
        )

    def test_nta_2_02_11_05(self):
        url = Path(f"{conf.test['datadir']}/entrypoints/entrypoint.minimal.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.11.05")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)
        new_contents = contents.replace("link:linkbaseRef", "bd-rpt-ob:nonsens", 1)
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.11.05: <xs:appinfo> MOET NIET gebruikt worden voor andere content dan elementen uit de xlink of link namespaces in 'entrypoints/entrypoint.minimal.xsd'"
            ],
        )

    def test_nta_2_02_11_06(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-codes.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.11.06")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)
        root = schema.xpath("//xs:schema", namespaces=schema.nsmap)
        etree.SubElement(root[0], "{http://www.w3.org/2001/XMLSchema}attribute")
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.11.06: <xs:attribute> MOET NIET gebruikt worden in 'dictionary/test-codes.xsd'"
            ],
        )

    def test_nta_2_02_11_07(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-codes.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.11.07")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)
        root = schema.xpath("//xs:schema", namespaces=schema.nsmap)
        etree.SubElement(root[0], "{http://www.w3.org/2001/XMLSchema}attributeGroup")
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.11.07: <xs:attributeGroup> MOET NIET gebruikt worden in 'dictionary/test-codes.xsd'"
            ],
        )

    def test_nta_2_02_11_08(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-tuples.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.11.08")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)
        new_contents = contents.replace("xs:complexType", "xs:simpleType")
        schema = etree.fromstring(bytes(new_contents, encoding="utf-8"))
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.11.08: <xs:choice> MOET NIET gebruikt worden voor simpleType elementen in 'dictionary/test-tuples.xsd'"
            ],
        )

    def test_nta_2_02_11_09(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-tuples.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.11.09")
        with self.assertLogs(self.logger_schema, level="WARNING") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "WARNING:validator.schema:2.02.11.09: <xs:choice> BEHOORT NIET gebruikt worden voor complexType elementen in 'dictionary/test-tuples.xsd'"
            ],
        )

    def test_nta_2_02_11_12(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-codes.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.11.12")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)
        root = schema.xpath("//xs:schema", namespaces=schema.nsmap)
        etree.SubElement(root[0], "{http://www.w3.org/2001/XMLSchema}extension")
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.11.12: <xs:extension> MOET NIET gebruikt worden in 'dictionary/test-codes.xsd'"
            ],
        )

    def test_nta_2_02_11_14(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-codes.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.11.14")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)
        root = schema.xpath("//xs:schema", namespaces=schema.nsmap)
        etree.SubElement(root[0], "{http://www.w3.org/2001/XMLSchema}group")
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.11.14: <xs:group> MOET NIET gebruikt worden in 'dictionary/test-codes.xsd'"
            ],
        )

    def test_nta_2_02_11_15(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-codes.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.11.15")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)
        root = schema.xpath("//xs:schema", namespaces=schema.nsmap)
        etree.SubElement(root[0], "{http://www.w3.org/2001/XMLSchema}key")
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.11.15: <xs:key> MOET NIET gebruikt worden in 'dictionary/test-codes.xsd'"
            ],
        )

    def test_nta_2_02_11_16(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-codes.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.11.16")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)
        root = schema.xpath("//xs:schema", namespaces=schema.nsmap)
        etree.SubElement(root[0], "{http://www.w3.org/2001/XMLSchema}keyref")
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.11.16: <xs:keyref> MOET NIET gebruikt worden in 'dictionary/test-codes.xsd'"
            ],
        )

    def test_nta_2_02_11_17(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-codes.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.11.17")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)
        root = schema.xpath("//xs:schema", namespaces=schema.nsmap)
        etree.SubElement(root[0], "{http://www.w3.org/2001/XMLSchema}list")
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.11.17: <xs:list> MOET NIET gebruikt worden in 'dictionary/test-codes.xsd'"
            ],
        )

    def test_nta_2_02_11_18(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-codes.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.11.18")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)
        root = schema.xpath("//xs:schema", namespaces=schema.nsmap)
        etree.SubElement(root[0], "{http://www.w3.org/2001/XMLSchema}notation")
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.11.18: <xs:notation> MOET NIET gebruikt worden in 'dictionary/test-codes.xsd'"
            ],
        )

    def test_nta_2_02_11_19(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-codes.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.11.19")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)
        root = schema.xpath("//xs:schema", namespaces=schema.nsmap)
        etree.SubElement(root[0], "{http://www.w3.org/2001/XMLSchema}pattern")
        with self.assertLogs(self.logger_schema, level="WARNING") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "WARNING:validator.schema:2.02.11.19: <xs:pattern> BEHOORT NIET gebruikt te worden in 'dictionary/test-codes.xsd'"
            ],
        )

    def test_nta_2_02_11_20(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-codes.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.11.20")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)
        root = schema.xpath("//xs:schema", namespaces=schema.nsmap)
        etree.SubElement(root[0], "{http://www.w3.org/2001/XMLSchema}redefine")
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.11.20: <xs:redefine> MOET NIET gebruikt worden in 'dictionary/test-codes.xsd'"
            ],
        )

    def test_nta_2_02_11_21(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-codesII.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.11.21")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)
        root = schema.xpath("//xs:schema", namespaces=schema.nsmap)
        el = etree.SubElement(root[0], "{http://www.w3.org/2001/XMLSchema}element")
        simplecontent = etree.SubElement(
            el, "{http://www.w3.org/2001/XMLSchema}simpleContent"
        )
        etree.SubElement(simplecontent, "{http://www.w3.org/2001/XMLSchema}restriction")
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.11.21: <xs:restriction> MOET NIET gebruikt worden op xs:element, alleen op xs:simpleType in 'dictionary/test-codesII.xsd'"
            ],
        )

    def test_nta_2_02_11_23(self):
        url = Path(f"{conf.test['datadir']}/dictionary/test-codes.xsd")
        contents, schema = self.get_xml(url)
        self.set_rule("2.02.11.23")
        self.assert_xpath_ok(url=url, schema=schema, contents=contents)
        root = schema.xpath("//xs:schema", namespaces=schema.nsmap)
        etree.SubElement(root[0], "{http://www.w3.org/2001/XMLSchema}unique")
        with self.assertLogs(self.logger_schema, level="ERROR") as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=contents)
        self.assertEqual(
            validatorlog.output,
            [
                "ERROR:validator.schema:2.02.11.23: <xs:unique> MOET NIET gebruikt worden in 'dictionary/test-codes.xsd'"
            ],
        )

    #
    # __init__ helper
    #
    def setup_validator(self):
        """
        We reuse the validator object as much as possible. Feeding it 1 rule ate a time
        """
        self.validator = Validator()
        self.all_linkbase_rules = self.validator.linkbase_rules
        self.all_model_rules = self.validator.model_rules
        self.all_schema_rules = self.validator.schema_rules

    # little helpers
    def assert_xpath_ok(self, url, schema, contents):
        with self.assertLogs(self.logger_schema, None) as validatorlog:
            self.validator.validate_schema(url=url, schema=schema, file=contents)
            # python 3.10 has assertNoLogs, until then we have to log a dummy message otherwise the test will fail
            self.logger_schema.warning("I'm alone")
        self.assertEqual(validatorlog.output, ["WARNING:validator.schema:I'm alone"])

    def get_xml(self, url):
        with open(url, "r") as file:
            contents = file.read()
        return contents, etree.fromstring(bytes(contents, encoding="utf-8"))

    def set_rule(self, rule_nr):
        self.validator.schema_rules = []
        for rule in self.all_schema_rules:
            if rule.rule_nr == rule_nr:
                self.validator.schema_rules = [rule]
