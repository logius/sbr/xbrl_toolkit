from pathlib import Path
from unittest import TestCase
from os.path import join, exists
from os import remove, getenv
from psycopg2 import OperationalError

from discover_xbrl.sbrnl_modules.db.collections.concepts import Concepts
from discover_xbrl.sbrnl_modules.db.collections.hypercubes import HyperCubes
from discover_xbrl.sbrnl_modules.db.connection import get_connection
from discover_xbrl.conf.conf import Config


class TestDbCollections(TestCase):
    conf = Config("config.test.postgres.yaml")
    on_gitlab = getenv("GITLAB_CI", "false")
    dts_name = "bzk-rpt-ti-woningwet.xsd"

    @classmethod
    def setUpClass(cls):
        try:
            # hack
            if cls.on_gitlab == "true":
                cls.conf.db["host"] = "postgres"
            # hack
            cls.db = get_connection(conf=cls.conf)
        except OperationalError:
            exit()
        # data_file = f"{cls.conf.test['datadir']}/db/woningwet_cleaned.sql"
        data_file = f"{cls.conf.test['datadir']}/db/woningwetnt17.sql"
        with open(data_file, "r") as sql:
            data = sql.read()
        if data:
            cls.cursor = cls.db.connection.cursor()
            cls.cursor.execute(data)
            cls.db.connection.commit()

    @classmethod
    def tearDownClass(cls):
        cls.db.connection.rollback()  # this will overcome errors
        # connect to the default db, otherwise it won't work
        dbname_remove = cls.conf.db["dbname"]
        cls.db.switch_db("postgres")
        cls.db.connection.set_session(autocommit="on")
        cls.cursor = cls.db.connection.cursor()
        cls.cursor.execute(f"DROP DATABASE IF EXISTS {dbname_remove}")
        cls.db.connection.commit()

    def test_loaded_data(self):
        cur = self.db.connection.cursor()
        cur.execute(f"select count(*) from concept")
        row = cur.fetchone()
        self.assertEqual(row[0], 27)

    def test_concepts(self):
        concepts = Concepts(db=self.db, full=True)
        concepts.concepts_of_dts(dts_name=self.dts_name, referenced_only=False)
        self.assertEqual(len(concepts.concepts), 22)
        self.assertEqual(len(concepts.concepts[0]["references"]), 1)
        c2 = Concepts(db=self.db, full=False)
        c2.concepts_of_dts(dts_name=self.dts_name, referenced_only=False)
        self.assertIsNone(c2.concepts[0].get("references"))
        concepts.concept_by_id(
            id="bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTripleAndMultiplePerson",
            dts_name="bzk-rpt-ti-woningwet.xsd",
        )
        self.assertEqual(len(concepts.concepts[0]["labels"]), 5)
        self.assertEqual(
            concepts.concepts[0]["labels"][1]["label_text"],
            "Boven de pensioengerechtigde leeftijd, inkomen hoger dan inkomensgrens Wht, drie en meerpersoonshuishoudens",
        )

    def test_concept_by_qname(self):
        concepts = Concepts(db=self.db, full=True)
        concepts.concept_by_qname(
            qname="bzk-ww-i:ActivitiesOverviewDetailPolicyValueMutation",
            dts_name="bzk-rpt-ti-woningwet.xsd",
        )
        self.assertEqual(len(concepts.concepts), 1)
        concepts.concept_by_qname(
            qname="{http://www.nltaxonomie.nl/nt17/rj/20221214/dictionary/rj-data}:SupervisoryBodyReport",
            dts_name="bzk-rpt-ti-woningwet.xsd",
        )
        self.assertEqual(len(concepts.concepts), 1)
        self.assertEqual(
            concepts.concepts[0]["namespace"],
            "http://www.nltaxonomie.nl/nt17/rj/20221214/dictionary/rj-data",
        )

    def test_hypercubes(self):
        hypercubes = HyperCubes(db=self.db, full=False)
        hypercubes.hypercubes_of_dts(dts_name=self.dts_name)
        self.assertEqual(len(hypercubes.hypercubes), 2)
        self.assertIsNone(hypercubes.hypercubes[1].get("line_items"))
        hypercubes = HyperCubes(db=self.db)
        hypercubes.hypercubes_of_dts(dts_name=self.dts_name)
        self.assertEqual(len(hypercubes.hypercubes), 2)
        line_items = [line for h in hypercubes.hypercubes for line in h["line_items"]]
        self.assertEqual(len(line_items), 2)
