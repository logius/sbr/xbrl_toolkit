import json
from unittest import TestCase

from discover_xbrl.conf.conf import Config
from discover_xbrl.sbrnl_modules.db.render.table_grid import TableGrid

conf = Config("config.test.nodb.yaml")


class TestRenderHtmlTable(TestCase):
    def test_table_z_axis(self):
        with open(
            f"{conf.test['datadir']}/json/kvk-lr_SigningFinancialStatements.json"
        ) as jsonfile:
            linkrole = json.load(jsonfile)[0]
        table_renderer = TableGrid()

    def test_table_flatten_balance_sheet(self):
        with open(
            f"{conf.test['datadir']}/json/kvk-lr_ConsolidatedBalanceSheet.json"
        ) as jsonfile:
            linkrole = json.load(jsonfile)[0]
