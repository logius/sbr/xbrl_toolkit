from unittest import TestCase
from discover_xbrl.sbrnl_xbrl_toolbox.widgets.hypercube_widget import (
    insert_newlines,
    HyperCubeWidget,
)
from discover_xbrl.conf.conf import Config

conf = Config("config.test.nodb.yaml")


class TestHypercubeWidget(TestCase):
    def test_insert_newlines(self):
        tekst = (
            "Een heleboel woorden die achter elkaar staan "
            "geschreven zondeer dat er rekening wordt gehouden met de lengte van de zin."
        )
        result = insert_newlines(text=tekst)
        self.assertEqual(3, len(result.split("\n")))
        result = insert_newlines(text=tekst, trailing=False)
        self.assertEqual(2, len(result.split("\n")))

    def test_get_presentation_html(self):
        pass
