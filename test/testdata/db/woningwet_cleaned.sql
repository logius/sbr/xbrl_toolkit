--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1 (Debian 14.1-1.pgdg110+1)
-- Dumped by pg_dump version 14.1

-- Started on 2022-01-24 21:36:35

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;


--
-- Data for Name: dts; Type: TABLE DATA; Schema: public; Owner: postgres
--
INSERT INTO public.dts VALUES ('bzk-rpt-ti-woningwet.xsd', 'woningwet');


--
-- Data for Name: itemtype; Type: TABLE DATA; Schema: public; Owner: postgres
--
INSERT INTO public.itemtype VALUES (NULL, 'stringItemType', 'xbrli', 'string', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('nl-types_formattedExplanationItemType', 'formattedExplanationItemType', 'nl-types', 'nl-types:string100000ItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Data for Name: dts_itemtype; Type: TABLE DATA; Schema: public; Owner: postgres
--
INSERT INTO public.dts_itemtype VALUES ('bzk-rpt-ti-woningwet.xsd', 'stringItemType');
INSERT INTO public.dts_itemtype VALUES ('bzk-rpt-ti-woningwet.xsd', 'formattedExplanationItemType');

--
-- Data for Name: label; Type: TABLE DATA; Schema: public; Owner: postgres
--
INSERT INTO public.label VALUES ('bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersManagement_label_de', 'bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersManagement_label_de', 'http://www.xbrl.org/2003/role/label', 'de', 'Beschreibung der relevanten Nebentätigkeiten von Mitgliedern des Vorstandes');
INSERT INTO public.label VALUES ('bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersManagement_label_fr', 'bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersManagement_label_fr', 'http://www.xbrl.org/2003/role/label', 'fr', 'Description des fonctions annexes importantes des membres de l''administration');
INSERT INTO public.label VALUES ('bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersManagement_documentation_nl', 'bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersManagement_documentation_nl', 'http://www.xbrl.org/2003/role/documentation', 'nl', 'De beschrijving van de relevante nevenfuncties van de leden van het bestuur en de directie in het bestuursverslag.');
INSERT INTO public.label VALUES ('bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersManagement_label_nl', 'bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersManagement_label_nl', 'http://www.xbrl.org/2003/role/label', 'nl', 'Beschrijving van de relevante nevenfuncties van leden van het bestuur');
INSERT INTO public.label VALUES ('bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersManagement_label_en', 'bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersManagement_label_en', 'http://www.xbrl.org/2003/role/label', 'en', 'Description of the additional relevant positions of members of management');
INSERT INTO public.label VALUES ('bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersSupervisoryBody_label_de', 'bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersSupervisoryBody_label_de', 'http://www.xbrl.org/2003/role/label', 'de', 'Beschreibung der relevanten Nebentätigkeiten von Mitgliedern des aufsichtsführenden Organs');
INSERT INTO public.label VALUES ('bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersSupervisoryBody_label_fr', 'bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersSupervisoryBody_label_fr', 'http://www.xbrl.org/2003/role/label', 'fr', 'Description des fonctions annexes importantes des membres de l''organe de surveillance');
INSERT INTO public.label VALUES ('bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersSupervisoryBody_documentation_nl', 'bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersSupervisoryBody_documentation_nl', 'http://www.xbrl.org/2003/role/documentation', 'nl', 'De beschrijving van de relevante nevenfuncties van de leden van het toezichthoudend orgaan in het bestuursverslag.');
INSERT INTO public.label VALUES ('bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersSupervisoryBody_label_nl', 'bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersSupervisoryBody_label_nl', 'http://www.xbrl.org/2003/role/label', 'nl', 'Beschrijving van de relevante nevenfuncties van leden van het toezichthoudend orgaan');
INSERT INTO public.label VALUES ('bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersSupervisoryBody_label_en', 'bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersSupervisoryBody_label_en', 'http://www.xbrl.org/2003/role/label', 'en', 'Description of the additional relevant positions of members of the supervisory body');
INSERT INTO public.label VALUES ('bzk-ww-i_RelevantApprovedInstitutionAssociatedCompaniesDescription_label_de', 'bzk-ww-i_RelevantApprovedInstitutionAssociatedCompaniesDescription_label_de', 'http://www.xbrl.org/2003/role/label', 'de', 'Beschreibung der relevanten zugelassenen Institute assoziierter Unternehmen');
INSERT INTO public.label VALUES ('bzk-ww-i_RelevantApprovedInstitutionAssociatedCompaniesDescription_label_fr', 'bzk-ww-i_RelevantApprovedInstitutionAssociatedCompaniesDescription_label_fr', 'http://www.xbrl.org/2003/role/label', 'fr', 'Description des sociétés associées à un établissement agréé agréé');
INSERT INTO public.label VALUES ('bzk-ww-i_RelevantApprovedInstitutionAssociatedCompaniesDescription_documentation_nl', 'bzk-ww-i_RelevantApprovedInstitutionAssociatedCompaniesDescription_documentation_nl', 'http://www.xbrl.org/2003/role/documentation', 'nl', 'De beschrijving van betrokken toegelaten instelling verbonden ondernemingen, met uitzondering van de samenwerkingsvennootschappen waarin zij vennoten bij wijze van geldschieting is.');
INSERT INTO public.label VALUES ('bzk-ww-i_RelevantApprovedInstitutionAssociatedCompaniesDescription_label_nl', 'bzk-ww-i_RelevantApprovedInstitutionAssociatedCompaniesDescription_label_nl', 'http://www.xbrl.org/2003/role/label', 'nl', 'Beschrijving van betrokken toegelaten instelling verbonden ondernemingen');
INSERT INTO public.label VALUES ('bzk-ww-i_RelevantApprovedInstitutionAssociatedCompaniesDescription_label_en', 'bzk-ww-i_RelevantApprovedInstitutionAssociatedCompaniesDescription_label_en', 'http://www.xbrl.org/2003/role/label', 'en', 'Description of relevant approved institution associated companies');
INSERT INTO public.label VALUES ('bzk-ww-i_ValuationOfImmovableInfrastructuralAppurtenancesDescription_label_de', 'bzk-ww-i_ValuationOfImmovableInfrastructuralAppurtenancesDescription_label_de', 'http://www.xbrl.org/2003/role/label', 'de', 'Beschreibung der Bewertung von unbeweglichen und infrastrukturellen Einrichtungen');
INSERT INTO public.label VALUES ('bzk-ww-i_ValuationOfImmovableInfrastructuralAppurtenancesDescription_label_fr', 'bzk-ww-i_ValuationOfImmovableInfrastructuralAppurtenancesDescription_label_fr', 'http://www.xbrl.org/2003/role/label', 'fr', 'Description de la valorisation des biens immeubles et des infrastructures');
INSERT INTO public.label VALUES ('bzk-ww-i_ValuationOfImmovableInfrastructuralAppurtenancesDescription_documentation_nl', 'bzk-ww-i_ValuationOfImmovableInfrastructuralAppurtenancesDescription_documentation_nl', 'http://www.xbrl.org/2003/role/documentation', 'nl', 'De beschrijving van de waardering onroerende en infrastructurele aanhorigheden op de balans in woningcorporaties.');
INSERT INTO public.label VALUES ('bzk-ww-i_ValuationOfImmovableInfrastructuralAppurtenancesDescription_label_nl', 'bzk-ww-i_ValuationOfImmovableInfrastructuralAppurtenancesDescription_label_nl', 'http://www.xbrl.org/2003/role/label', 'nl', 'Beschrijving van de waardering onroerende en infrastructurele aanhorigheden');
INSERT INTO public.label VALUES ('bzk-ww-i_ValuationOfImmovableInfrastructuralAppurtenancesDescription_label_en', 'bzk-ww-i_ValuationOfImmovableInfrastructuralAppurtenancesDescription_label_en', 'http://www.xbrl.org/2003/role/label', 'en', 'Description of the valuation of immovable and infrastructural appurtenances');
INSERT INTO public.label VALUES ('rj-i_SupervisoryBodyReport_domainSpecificLabel_nl', 'rj-i_SupervisoryBodyReport_domainSpecificLabel_nl', 'http://www.nltaxonomie.nl/2016/labelrole/domainSpecificLabel', 'nl', 'Verslag van de Raad van Toezicht');
INSERT INTO public.label VALUES ('rj-i_SupervisoryBodyReport_documentation_nl', 'rj-i_SupervisoryBodyReport_documentation_nl', 'http://www.xbrl.org/2003/role/documentation', 'nl', 'De rapportage van het toezichthoudend orgaan, waarin zij aangeeft op welke wijze zij het bestuur ondersteunt en/of adviseert over de beleidsvraagstukken en de financiële problematiek.');
INSERT INTO public.label VALUES ('rj-i_SupervisoryBodyReport_label_en', 'rj-i_SupervisoryBodyReport_label_en', 'http://www.xbrl.org/2003/role/label', 'en', 'Report of the supervisory body');
INSERT INTO public.label VALUES ('rj-i_SupervisoryBodyReport_label_nl', 'rj-i_SupervisoryBodyReport_label_nl', 'http://www.xbrl.org/2003/role/label', 'nl', 'Rapportage van het toezichthoudend orgaan');
INSERT INTO public.label VALUES ('rj-i_SupervisoryBodyReport_label_de', 'rj-i_SupervisoryBodyReport_label_de', 'http://www.xbrl.org/2003/role/label', 'de', 'Bericht des aufsichtsführenden Organs');
INSERT INTO public.label VALUES ('rj-i_SupervisoryBodyReport_label_fr', 'rj-i_SupervisoryBodyReport_label_fr', 'http://www.xbrl.org/2003/role/label', 'fr', 'Reporting de l''organe de contrôle');
INSERT INTO public.label VALUES ('rj-i_SupervisoryBodyReport_domainSpecificLabel_en', 'rj-i_SupervisoryBodyReport_domainSpecificLabel_en', 'http://www.nltaxonomie.nl/2016/labelrole/domainSpecificLabel', 'en', 'Report of the supervisory body');
INSERT INTO public.label VALUES ('bzk-ww-abstr_Article35HousingActTitle_label_nl', 'bzk-ww-abstr_Article35HousingActTitle_label_nl', 'http://www.xbrl.org/2003/role/label', 'nl', 'Artikel 35');
INSERT INTO public.label VALUES ('bzk-ww-abstr_Article35HousingActTitle_label_en', 'bzk-ww-abstr_Article35HousingActTitle_label_en', 'http://www.xbrl.org/2003/role/label', 'en', 'Article 35');
INSERT INTO public.label VALUES ('bzk-ww-abstr_Article36HousingActTitle_label_nl', 'bzk-ww-abstr_Article36HousingActTitle_label_nl', 'http://www.xbrl.org/2003/role/label', 'nl', 'Artikel 36');
INSERT INTO public.label VALUES ('bzk-ww-abstr_Article36HousingActTitle_label_en', 'bzk-ww-abstr_Article36HousingActTitle_label_en', 'http://www.xbrl.org/2003/role/label', 'en', 'Article 36');


--
-- Data for Name: namespace; Type: TABLE DATA; Schema: public; Owner: postgres
--
INSERT INTO public.namespace VALUES ('bzk-rpt-ww', 'http://www.nltaxonomie.nl/nt16/bzk/20211208/entrypoints/bzk-rpt-ti-woningwet');
INSERT INTO public.namespace VALUES ('link', 'http://www.xbrl.org/2003/linkbase');
INSERT INTO public.namespace VALUES ('xlink', 'http://www.w3.org/1999/xlink');
INSERT INTO public.namespace VALUES ('xs', 'http://www.w3.org/2001/XMLSchema');
INSERT INTO public.namespace VALUES ('xl', 'http://www.xbrl.org/2003/XLink');
INSERT INTO public.namespace VALUES ('bzk-ww-lr', 'http://www.nltaxonomie.nl/nt16/bzk/20211208/dictionary/bzk-ww-linkroles');
INSERT INTO public.namespace VALUES ('xbrli', 'http://www.xbrl.org/2003/instance');
INSERT INTO public.namespace VALUES ('xbrldt', 'http://xbrl.org/2005/xbrldt');
INSERT INTO public.namespace VALUES ('bzk-ww-codes', 'http://www.nltaxonomie.nl/nt16/bzk/20211208/dictionary/bzk-ww-codes');
INSERT INTO public.namespace VALUES ('bzk-ww-i', 'http://www.nltaxonomie.nl/nt16/bzk/20211208/dictionary/bzk-ww-data');
INSERT INTO public.namespace VALUES ('bzk-ww-types', 'http://www.nltaxonomie.nl/nt16/bzk/20211208/dictionary/bzk-ww-types');
INSERT INTO public.namespace VALUES ('nl-codes', 'http://www.nltaxonomie.nl/nt16/sbr/20210301/dictionary/nl-codes');
INSERT INTO public.namespace VALUES ('nl-types', 'http://www.nltaxonomie.nl/nt16/sbr/20210301/dictionary/nl-types');
INSERT INTO public.namespace VALUES ('num', 'http://www.xbrl.org/dtr/type/numeric');
INSERT INTO public.namespace VALUES ('dtr-types', 'http://www.xbrl.org/dtr/type/2020-01-21');
INSERT INTO public.namespace VALUES ('dtr', 'http://www.xbrl.org/2009/dtr');
INSERT INTO public.namespace VALUES ('sbr', 'http://www.nltaxonomie.nl/2011/xbrl/xbrl-syntax-extension');
INSERT INTO public.namespace VALUES ('sbr-dim', 'http://www.nltaxonomie.nl/2013/xbrl/sbr-dimensional-concepts');
INSERT INTO public.namespace VALUES ('jenv-codes', 'http://www.nltaxonomie.nl/nt16/jenv/20211208/dictionary/jenv-codes');
INSERT INTO public.namespace VALUES ('rj-codes', 'http://www.nltaxonomie.nl/nt16/rj/20211208/dictionary/rj-codes');
INSERT INTO public.namespace VALUES ('rj-i', 'http://www.nltaxonomie.nl/nt16/rj/20211208/dictionary/rj-data');
INSERT INTO public.namespace VALUES ('jenv-bw2-dm', 'http://www.nltaxonomie.nl/nt16/jenv/20211208/dictionary/jenv-bw2-domains');
INSERT INTO public.namespace VALUES ('jenv-bw2-i', 'http://www.nltaxonomie.nl/nt16/jenv/20211208/dictionary/jenv-bw2-data');
INSERT INTO public.namespace VALUES ('bzk-ww-abstr', 'http://www.nltaxonomie.nl/nt16/bzk/20211208/presentation/bzk-ww-abstracts');
INSERT INTO public.namespace VALUES ('gen', 'http://xbrl.org/2008/generic');
INSERT INTO public.namespace VALUES ('xsd', 'http://www.w3.org/2001/XMLSchema');
INSERT INTO public.namespace VALUES ('xsi', 'http://www.w3.org/2001/XMLSchema-instance');
INSERT INTO public.namespace VALUES ('label', 'http://xbrl.org/2008/label');
INSERT INTO public.namespace VALUES ('ref', 'http://www.xbrl.org/2006/ref');
INSERT INTO public.namespace VALUES ('sbr-arc', 'http://www.nltaxonomie.nl/2017/xbrl/sbr-arcroles');
INSERT INTO public.namespace VALUES ('bzk-ww-dm', 'http://www.nltaxonomie.nl/nt16/bzk/20211208/dictionary/bzk-ww-domains');
INSERT INTO public.namespace VALUES ('bzk-ww-sim-codes', 'http://www.nltaxonomie.nl/nt16/bzk/20211208/dictionary/bzk-ww-simple-type-codes');
INSERT INTO public.namespace VALUES ('rj-dm', 'http://www.nltaxonomie.nl/nt16/rj/20211208/dictionary/rj-domains');
INSERT INTO public.namespace VALUES ('reference', 'http://xbrl.org/2008/reference');
INSERT INTO public.namespace VALUES ('sbr-a', 'http://www.nltaxonomie.nl/2011/xbrl/xbrl-syntax-arcroles');
INSERT INTO public.namespace VALUES ('ezk-ncgc-i', 'http://www.nltaxonomie.nl/nt16/ezk/20211208/dictionary/ezk-ncgc-data');


--
-- Data for Name: reference; Type: TABLE DATA; Schema: public; Owner: postgres
--
INSERT INTO public.reference VALUES ('bzk-ww-i_W_2018-01-01_35_2_ref', 'bzk-ww-i_W_2018-01-01_35_2_ref', 'Woningwet', NULL, '2018-01-01', NULL, '35', NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.reference VALUES ('bzk-ww-i_W_2018-01-01_36_2_ref', 'bzk-ww-i_W_2018-01-01_36_2_ref', 'Woningwet', NULL, '2018-01-01', NULL, '36', NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.reference VALUES ('bzk-ww-i_W_2018-01-01_36_4_ref', 'bzk-ww-i_W_2018-01-01_36_4_ref', 'Woningwet', NULL, '2018-01-01', NULL, '36', NULL, NULL, NULL, NULL, '4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.reference VALUES ('bzk-ww-i_RJ_2021-01-01_650_409_ref', 'bzk-ww-i_RJ_2021-01-01_650_409_ref', 'Richtlijnen voor de jaarverslaggeving', NULL, '2021-01-01', '650', NULL, NULL, NULL, NULL, NULL, '409', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.reference VALUES ('bzk-ww-i_RJk_2021-01-01_C2_407_ref', 'bzk-ww-i_RJk_2021-01-01_C2_407_ref', 'Richtlijnen voor de jaarverslaggeving voor kleine rechtspersonen', NULL, '2021-01-01', 'C2', NULL, NULL, NULL, NULL, NULL, '407', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.reference VALUES ('rj-i_W_2018-01-01_36_3_ref', 'rj-i_W_2018-01-01_36_3_ref', 'Woningwet', NULL, '2018-01-01', NULL, '36', NULL, NULL, NULL, NULL, '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.reference VALUES ('rj-i_RJ_2021-01-01_610_504_ref', 'rj-i_RJ_2021-01-01_610_504_ref', 'Richtlijnen voor de jaarverslaggeving', NULL, '2021-01-01', '610', NULL, NULL, NULL, NULL, NULL, '504', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.reference VALUES ('rj-i_RJ_2021-01-01_650_403_ref', 'rj-i_RJ_2021-01-01_650_403_ref', 'Richtlijnen voor de jaarverslaggeving', NULL, '2021-01-01', '650', NULL, NULL, NULL, NULL, NULL, '403', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.reference VALUES ('rj-i_RJ_2021-01-01_660_519_ref', 'rj-i_RJ_2021-01-01_660_519_ref', 'Richtlijnen voor de jaarverslaggeving', NULL, '2021-01-01', '660', NULL, NULL, NULL, NULL, NULL, '519', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);


--
-- Data for Name: dts_namespace; Type: TABLE DATA; Schema: public; Owner: postgres
--
INSERT INTO public.dts_namespace VALUES ('bzk-rpt-ww', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('link', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('xlink', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('xs', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('xl', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('bzk-ww-lr', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('xbrli', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('xbrldt', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('bzk-ww-codes', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('bzk-ww-i', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('bzk-ww-types', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('nl-codes', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('nl-types', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('num', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('dtr-types', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('dtr', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('sbr', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('sbr-dim', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('jenv-codes', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('rj-codes', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('rj-i', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('jenv-bw2-dm', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('jenv-bw2-i', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('bzk-ww-abstr', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('gen', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('xsd', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('xsi', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('label', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('ref', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('sbr-arc', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('bzk-ww-dm', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('bzk-ww-sim-codes', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('rj-dm', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('reference', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('sbr-a', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('ezk-ncgc-i', 'bzk-rpt-ti-woningwet.xsd');


--
-- Data for Name: linkrole; Type: TABLE DATA; Schema: public; Owner: postgres
--
INSERT INTO public.linkrole VALUES (2, 'bzk-ww-lr_Article36HousingAct', 'urn:bzk-ti:linkrole:article36housing-act', 'Artikel 36', 'bzk-rpt-ti-woningwet.xsd', true, 2);
INSERT INTO public.linkrole VALUES (7, 'bzk-ww-lr_Article35HousingAct', 'urn:bzk:linkrole:article35housing-act', 'Artikel 35', 'bzk-rpt-ti-woningwet.xsd', true, 1);
INSERT INTO public.linkrole VALUES (16, 'linkrole-info', 'http://www.nltaxonomie.nl/2011/role/linkrole-info', '', 'bzk-rpt-ti-woningwet.xsd', true, NULL);
INSERT INTO public.linkrole VALUES (17, NULL, 'http://www.nltaxonomie.nl/2016/labelrole/domainSpecificTerseLabel', NULL, 'bzk-rpt-ti-woningwet.xsd', true, NULL);
INSERT INTO public.linkrole VALUES (18, NULL, 'http://www.nltaxonomie.nl/2016/labelrole/domainSpecificLabel', NULL, 'bzk-rpt-ti-woningwet.xsd', true, NULL);
INSERT INTO public.linkrole VALUES (19, NULL, 'http://www.xbrl.org/2008/role/link', NULL, 'bzk-rpt-ti-woningwet.xsd', true, NULL);


--
-- Data for Name: concept; Type: TABLE DATA; Schema: public; Owner: postgres
--
INSERT INTO public.concept VALUES ('bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersManagement', 'DescriptionOfAdditionalRelevantPositionsMembersManagement', NULL, false, false, 'duration', 'bzk-ww-i', 'http://www.nltaxonomie.nl/nt16/bzk/20211208/dictionary/bzk-ww-data', 'xbrli:item', NULL, 'formattedExplanationItemType');
INSERT INTO public.concept VALUES ('bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersSupervisoryBody', 'DescriptionOfAdditionalRelevantPositionsMembersSupervisoryBody', NULL, false, false, 'duration', 'bzk-ww-i', 'http://www.nltaxonomie.nl/nt16/bzk/20211208/dictionary/bzk-ww-data', 'xbrli:item', NULL, 'formattedExplanationItemType');
INSERT INTO public.concept VALUES ('bzk-ww-i_RelevantApprovedInstitutionAssociatedCompaniesDescription', 'RelevantApprovedInstitutionAssociatedCompaniesDescription', NULL, false, false, 'duration', 'bzk-ww-i', 'http://www.nltaxonomie.nl/nt16/bzk/20211208/dictionary/bzk-ww-data', 'xbrli:item', NULL, 'formattedExplanationItemType');
INSERT INTO public.concept VALUES ('bzk-ww-i_ValuationOfImmovableInfrastructuralAppurtenancesDescription', 'ValuationOfImmovableInfrastructuralAppurtenancesDescription', NULL, false, false, 'duration', 'bzk-ww-i', 'http://www.nltaxonomie.nl/nt16/bzk/20211208/dictionary/bzk-ww-data', 'xbrli:item', NULL, 'formattedExplanationItemType');
INSERT INTO public.concept VALUES ('sbr-dim_ValidationTable', 'ValidationTable', NULL, true, true, 'duration', 'sbr-dim', 'http://www.nltaxonomie.nl/2013/xbrl/sbr-dimensional-concepts', 'xbrldt:hypercubeItem', NULL, 'stringItemType');
INSERT INTO public.concept VALUES ('sbr-dim_ValidationLineItems', 'ValidationLineItems', NULL, true, true, 'duration', 'sbr-dim', 'http://www.nltaxonomie.nl/2013/xbrl/sbr-dimensional-concepts', 'sbr:primaryDomainItem', NULL, 'stringItemType');
INSERT INTO public.concept VALUES ('rj-i_SupervisoryBodyReport', 'SupervisoryBodyReport', NULL, false, false, 'duration', 'rj-i', 'http://www.nltaxonomie.nl/nt16/rj/20211208/dictionary/rj-data', 'xbrli:item', NULL, 'formattedExplanationItemType');
INSERT INTO public.concept VALUES ('bzk-ww-abstr_Article35HousingActTitle', 'Article35HousingActTitle', NULL, false, true, 'duration', 'bzk-ww-abstr', 'http://www.nltaxonomie.nl/nt16/bzk/20211208/presentation/bzk-ww-abstracts', 'sbr:presentationItem', NULL, 'stringItemType');
INSERT INTO public.concept VALUES ('bzk-ww-abstr_Article36HousingActTitle', 'Article36HousingActTitle', NULL, false, true, 'duration', 'bzk-ww-abstr', 'http://www.nltaxonomie.nl/nt16/bzk/20211208/presentation/bzk-ww-abstracts', 'sbr:presentationItem', NULL, 'stringItemType');


--
-- Data for Name: concept_label; Type: TABLE DATA; Schema: public; Owner: postgres
--
INSERT INTO public.concept_label VALUES ('bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersManagement', 'bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersManagement_label_de');
INSERT INTO public.concept_label VALUES ('bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersManagement', 'bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersManagement_label_fr');
INSERT INTO public.concept_label VALUES ('bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersManagement', 'bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersManagement_documentation_nl');
INSERT INTO public.concept_label VALUES ('bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersManagement', 'bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersManagement_label_nl');
INSERT INTO public.concept_label VALUES ('bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersManagement', 'bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersManagement_label_en');
INSERT INTO public.concept_label VALUES ('bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersSupervisoryBody', 'bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersSupervisoryBody_label_de');
INSERT INTO public.concept_label VALUES ('bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersSupervisoryBody', 'bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersSupervisoryBody_label_fr');
INSERT INTO public.concept_label VALUES ('bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersSupervisoryBody', 'bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersSupervisoryBody_documentation_nl');
INSERT INTO public.concept_label VALUES ('bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersSupervisoryBody', 'bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersSupervisoryBody_label_nl');
INSERT INTO public.concept_label VALUES ('bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersSupervisoryBody', 'bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersSupervisoryBody_label_en');
INSERT INTO public.concept_label VALUES ('bzk-ww-i_RelevantApprovedInstitutionAssociatedCompaniesDescription', 'bzk-ww-i_RelevantApprovedInstitutionAssociatedCompaniesDescription_label_de');
INSERT INTO public.concept_label VALUES ('bzk-ww-i_RelevantApprovedInstitutionAssociatedCompaniesDescription', 'bzk-ww-i_RelevantApprovedInstitutionAssociatedCompaniesDescription_label_fr');
INSERT INTO public.concept_label VALUES ('bzk-ww-i_RelevantApprovedInstitutionAssociatedCompaniesDescription', 'bzk-ww-i_RelevantApprovedInstitutionAssociatedCompaniesDescription_documentation_nl');
INSERT INTO public.concept_label VALUES ('bzk-ww-i_RelevantApprovedInstitutionAssociatedCompaniesDescription', 'bzk-ww-i_RelevantApprovedInstitutionAssociatedCompaniesDescription_label_nl');
INSERT INTO public.concept_label VALUES ('bzk-ww-i_RelevantApprovedInstitutionAssociatedCompaniesDescription', 'bzk-ww-i_RelevantApprovedInstitutionAssociatedCompaniesDescription_label_en');
INSERT INTO public.concept_label VALUES ('bzk-ww-i_ValuationOfImmovableInfrastructuralAppurtenancesDescription', 'bzk-ww-i_ValuationOfImmovableInfrastructuralAppurtenancesDescription_label_de');
INSERT INTO public.concept_label VALUES ('bzk-ww-i_ValuationOfImmovableInfrastructuralAppurtenancesDescription', 'bzk-ww-i_ValuationOfImmovableInfrastructuralAppurtenancesDescription_label_fr');
INSERT INTO public.concept_label VALUES ('bzk-ww-i_ValuationOfImmovableInfrastructuralAppurtenancesDescription', 'bzk-ww-i_ValuationOfImmovableInfrastructuralAppurtenancesDescription_documentation_nl');
INSERT INTO public.concept_label VALUES ('bzk-ww-i_ValuationOfImmovableInfrastructuralAppurtenancesDescription', 'bzk-ww-i_ValuationOfImmovableInfrastructuralAppurtenancesDescription_label_nl');
INSERT INTO public.concept_label VALUES ('bzk-ww-i_ValuationOfImmovableInfrastructuralAppurtenancesDescription', 'bzk-ww-i_ValuationOfImmovableInfrastructuralAppurtenancesDescription_label_en');
INSERT INTO public.concept_label VALUES ('rj-i_SupervisoryBodyReport', 'rj-i_SupervisoryBodyReport_domainSpecificLabel_nl');
INSERT INTO public.concept_label VALUES ('rj-i_SupervisoryBodyReport', 'rj-i_SupervisoryBodyReport_documentation_nl');
INSERT INTO public.concept_label VALUES ('rj-i_SupervisoryBodyReport', 'rj-i_SupervisoryBodyReport_label_en');
INSERT INTO public.concept_label VALUES ('rj-i_SupervisoryBodyReport', 'rj-i_SupervisoryBodyReport_label_nl');
INSERT INTO public.concept_label VALUES ('rj-i_SupervisoryBodyReport', 'rj-i_SupervisoryBodyReport_label_de');
INSERT INTO public.concept_label VALUES ('rj-i_SupervisoryBodyReport', 'rj-i_SupervisoryBodyReport_label_fr');
INSERT INTO public.concept_label VALUES ('rj-i_SupervisoryBodyReport', 'rj-i_SupervisoryBodyReport_domainSpecificLabel_en');
INSERT INTO public.concept_label VALUES ('bzk-ww-abstr_Article35HousingActTitle', 'bzk-ww-abstr_Article35HousingActTitle_label_nl');
INSERT INTO public.concept_label VALUES ('bzk-ww-abstr_Article35HousingActTitle', 'bzk-ww-abstr_Article35HousingActTitle_label_en');
INSERT INTO public.concept_label VALUES ('bzk-ww-abstr_Article36HousingActTitle', 'bzk-ww-abstr_Article36HousingActTitle_label_nl');
INSERT INTO public.concept_label VALUES ('bzk-ww-abstr_Article36HousingActTitle', 'bzk-ww-abstr_Article36HousingActTitle_label_en');


--
-- Data for Name: concept_reference; Type: TABLE DATA; Schema: public; Owner: postgres
--
INSERT INTO public.concept_reference VALUES ('bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersManagement', 'bzk-ww-i_RJ_2021-01-01_650_409_ref');
INSERT INTO public.concept_reference VALUES ('bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersManagement', 'bzk-ww-i_RJk_2021-01-01_C2_407_ref');
INSERT INTO public.concept_reference VALUES ('bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersManagement', 'bzk-ww-i_W_2018-01-01_36_2_ref');
INSERT INTO public.concept_reference VALUES ('bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersSupervisoryBody', 'bzk-ww-i_RJ_2021-01-01_650_409_ref');
INSERT INTO public.concept_reference VALUES ('bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersSupervisoryBody', 'bzk-ww-i_RJk_2021-01-01_C2_407_ref');
INSERT INTO public.concept_reference VALUES ('bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersSupervisoryBody', 'bzk-ww-i_W_2018-01-01_36_2_ref');
INSERT INTO public.concept_reference VALUES ('bzk-ww-i_RelevantApprovedInstitutionAssociatedCompaniesDescription', 'bzk-ww-i_W_2018-01-01_36_4_ref');
INSERT INTO public.concept_reference VALUES ('bzk-ww-i_ValuationOfImmovableInfrastructuralAppurtenancesDescription', 'bzk-ww-i_W_2018-01-01_35_2_ref');
INSERT INTO public.concept_reference VALUES ('rj-i_SupervisoryBodyReport', 'rj-i_W_2018-01-01_36_3_ref');
INSERT INTO public.concept_reference VALUES ('rj-i_SupervisoryBodyReport', 'rj-i_RJ_2021-01-01_610_504_ref');
INSERT INTO public.concept_reference VALUES ('rj-i_SupervisoryBodyReport', 'rj-i_RJ_2021-01-01_650_403_ref');
INSERT INTO public.concept_reference VALUES ('rj-i_SupervisoryBodyReport', 'rj-i_RJ_2021-01-01_660_519_ref');


--
-- Data for Name: dts_concept; Type: TABLE DATA; Schema: public; Owner: postgres
--
INSERT INTO public.dts_concept VALUES ('bzk-rpt-ti-woningwet.xsd', 'bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersManagement', true, false, false, false);
INSERT INTO public.dts_concept VALUES ('bzk-rpt-ti-woningwet.xsd', 'bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersSupervisoryBody', true, false, false, false);
INSERT INTO public.dts_concept VALUES ('bzk-rpt-ti-woningwet.xsd', 'bzk-ww-i_RelevantApprovedInstitutionAssociatedCompaniesDescription', true, false, false, false);
INSERT INTO public.dts_concept VALUES ('bzk-rpt-ti-woningwet.xsd', 'bzk-ww-i_ValuationOfImmovableInfrastructuralAppurtenancesDescription', true, false, false, false);
INSERT INTO public.dts_concept VALUES ('bzk-rpt-ti-woningwet.xsd', 'sbr-dim_ValidationTable', true, false, false, false);
INSERT INTO public.dts_concept VALUES ('bzk-rpt-ti-woningwet.xsd', 'sbr-dim_ValidationLineItems', true, false, false, false);
INSERT INTO public.dts_concept VALUES ('bzk-rpt-ti-woningwet.xsd', 'rj-i_SupervisoryBodyReport', true, false, false, false);
INSERT INTO public.dts_concept VALUES ('bzk-rpt-ti-woningwet.xsd', 'bzk-ww-abstr_Article35HousingActTitle', true, false, false, false);
INSERT INTO public.dts_concept VALUES ('bzk-rpt-ti-woningwet.xsd', 'bzk-ww-abstr_Article36HousingActTitle', true, false, false, false);


--
-- Data for Name: hypercube; Type: TABLE DATA; Schema: public; Owner: postgres
--
INSERT INTO public.hypercube VALUES (2, 'urn:bzk-ti:linkrole:article36housing-act', 'all', 'scenario', NULL);
INSERT INTO public.hypercube VALUES (7, 'urn:bzk:linkrole:article35housing-act', 'all', 'scenario', NULL);


--
-- Data for Name: line_item; Type: TABLE DATA; Schema: public; Owner: postgres
--
INSERT INTO public.line_item VALUES (3, 2, 'bzk-ww-i_RelevantApprovedInstitutionAssociatedCompaniesDescription');
INSERT INTO public.line_item VALUES (4, 2, 'rj-i_SupervisoryBodyReport');
INSERT INTO public.line_item VALUES (NULL, 2, 'bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersManagement');
INSERT INTO public.line_item VALUES (NULL, 2, 'bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersSupervisoryBody');
INSERT INTO public.line_item VALUES (1, 7, 'bzk-ww-i_ValuationOfImmovableInfrastructuralAppurtenancesDescription');


--
-- Data for Name: presentation_hierarchy_node; Type: TABLE DATA; Schema: public; Owner: postgres
--
INSERT INTO public.presentation_hierarchy_node VALUES (1, 'Article36HousingActTitle', 'concept', 'bzk-ww-abstr_Article36HousingActTitle', NULL, true, 1, NULL, NULL, NULL, 2, false);
INSERT INTO public.presentation_hierarchy_node VALUES (2, 'DescriptionOfAdditionalRelevantPositionsMembersManagement', 'concept', 'bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersManagement', NULL, false, 1, NULL, 1, NULL, NULL, false);
INSERT INTO public.presentation_hierarchy_node VALUES (3, 'DescriptionOfAdditionalRelevantPositionsMembersSupervisoryBody', 'concept', 'bzk-ww-i_DescriptionOfAdditionalRelevantPositionsMembersSupervisoryBody', NULL, false, 2, NULL, 1, NULL, NULL, false);
INSERT INTO public.presentation_hierarchy_node VALUES (4, 'RelevantApprovedInstitutionAssociatedCompaniesDescription', 'concept', 'bzk-ww-i_RelevantApprovedInstitutionAssociatedCompaniesDescription', NULL, false, 3, NULL, 1, NULL, NULL, false);
INSERT INTO public.presentation_hierarchy_node VALUES (5, 'SupervisoryBodyReport', 'concept', 'rj-i_SupervisoryBodyReport', NULL, false, 4, 'http://www.nltaxonomie.nl/2016/labelrole/domainSpecificLabel', 1, NULL, NULL, false);
INSERT INTO public.presentation_hierarchy_node VALUES (6, 'Article35HousingActTitle', 'concept', 'bzk-ww-abstr_Article35HousingActTitle', NULL, true, 1, NULL, NULL, NULL, 7, false);
INSERT INTO public.presentation_hierarchy_node VALUES (7, 'ValuationOfImmovableInfrastructuralAppurtenancesDescription', 'concept', 'bzk-ww-i_ValuationOfImmovableInfrastructuralAppurtenancesDescription', NULL, false, 1, NULL, 6, NULL, NULL, false);
INSERT INTO public.presentation_hierarchy_node VALUES (10, 'urn:bzk:linkrole:article35housing-act', 'linkrole', NULL, 'urn:bzk:linkrole:article35housing-act', false, 1, NULL, 9, NULL, NULL, true);
INSERT INTO public.presentation_hierarchy_node VALUES (12, 'urn:bzk-ti:linkrole:article36housing-act', 'linkrole', NULL, 'urn:bzk-ti:linkrole:article36housing-act', false, 1, NULL, 11, NULL, NULL, true);
INSERT INTO public.presentation_hierarchy_node VALUES (15, 'urn:bzk:linkrole:article35housing-act', 'linkrole', NULL, 'urn:bzk:linkrole:article35housing-act', false, 1, NULL, 14, NULL, NULL, true);
INSERT INTO public.presentation_hierarchy_node VALUES (17, 'urn:bzk-ti:linkrole:article36housing-act', 'linkrole', NULL, 'urn:bzk-ti:linkrole:article36housing-act', false, 1, NULL, 16, NULL, NULL, true);

--
-- Name: dimension_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--
SELECT pg_catalog.setval('public.dimension_rowid_seq', 1, false);

--
-- Name: domain_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--
SELECT pg_catalog.setval('public.domain_rowid_seq', 1, false);

--
-- Name: formula_explicit_dimension_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--
SELECT pg_catalog.setval('public.formula_explicit_dimension_rowid_seq', 1, false);

--
-- Name: formula_period_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--
SELECT pg_catalog.setval('public.formula_period_rowid_seq', 1, false);

--
-- Name: formula_typed_dimension_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--
SELECT pg_catalog.setval('public.formula_typed_dimension_rowid_seq', 1, false);

--
-- Name: linkrole_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--
SELECT pg_catalog.setval('public.linkrole_rowid_seq', 27, true);

--
-- Name: member_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--
SELECT pg_catalog.setval('public.member_rowid_seq', 1, false);

--
-- Name: presentation_hierarchy_node_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--
SELECT pg_catalog.setval('public.presentation_hierarchy_node_rowid_seq', 17, true);

--
-- Name: ruleset_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--
SELECT pg_catalog.setval('public.ruleset_rowid_seq', 1, false);


