--
-- PostgreSQL database dump
--

-- Dumped from database version 11.19 (Debian 11.19-0+deb10u1)
-- Dumped by pg_dump version 15.1

-- Started on 2023-04-13 12:42:45

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;

--
-- TOC entry 3369 (class 0 OID 203212)
-- Dependencies: 196
-- Data for Name: dts; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.dts VALUES ('bzk-rpt-ti-woningwet.xsd', 'woningwet', NULL, NULL);


--
-- TOC entry 3386 (class 0 OID 203443)
-- Dependencies: 213
-- Data for Name: linkrole; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.linkrole VALUES (1, NULL, 'http://www.xbrl.org/2003/role/link', NULL, 'bzk-rpt-ti-woningwet.xsd', false, NULL);
INSERT INTO public.linkrole VALUES (2, 'bzk-ww-lr_Article36HousingAct', 'urn:bzk-ti:linkrole:article36housing-act', 'Artikel 36', 'bzk-rpt-ti-woningwet.xsd', true, 2);
INSERT INTO public.linkrole VALUES (3, 'bzk-ww-lr_BalanceSheetIncomeStatementCashFlow', 'urn:bzk-ti:linkrole:balance-sheet-income-statement-cash-flow', 'Paragraaf 3.1 Balans, Winst-en-verliesrekening en kasstroomoverzicht (Toegelaten instelling)', 'bzk-rpt-ti-woningwet.xsd', false, NULL);
INSERT INTO public.linkrole VALUES (4, 'bzk-ww-lr_NotesBalanceSheetIncomeStatement', 'urn:bzk-ti:linkrole:notes-balance-sheet-income-statement', 'Paragraaf 3.2 Toelichting op de enkelvoudige balans- en Winst-en-verliesrekening', 'bzk-rpt-ti-woningwet.xsd', false, NULL);
INSERT INTO public.linkrole VALUES (5, 'bzk-ww-lr_NotesConsolidatedBalanceSheetIncomeStatement', 'urn:bzk-ti:linkrole:notes-consolidated-balance-sheet-income-statement', 'Paragraaf 3.4 Toelichting op de enkelvoudige of geconsolideerde balans- en Winst-en-verliesrekening', 'bzk-rpt-ti-woningwet.xsd', false, NULL);
INSERT INTO public.linkrole VALUES (6, 'bzk-ww-lr_ActivitiesOverviewHousingDpi', 'urn:bzk:linkrole:activities-overview-housing-dpi', 'Paragraaf 2.1 Activiteitenoverzicht', 'bzk-rpt-ti-woningwet.xsd', false, NULL);
INSERT INTO public.linkrole VALUES (7, 'bzk-ww-lr_Article35HousingAct', 'urn:bzk:linkrole:article35housing-act', 'Artikel 35', 'bzk-rpt-ti-woningwet.xsd', true, 1);
INSERT INTO public.linkrole VALUES (8, 'bzk-ww-lr_CashFlowStatementLibraryHousingDpi', 'urn:bzk:linkrole:cash-flow-statement-library-housing-dpi', 'Paragraaf 3.1 Kasstroomoverzicht enkelvoudig en geconsolideerd', 'bzk-rpt-ti-woningwet.xsd', false, NULL);
INSERT INTO public.linkrole VALUES (9, 'bzk-ww-lr_ForecastBalanceSheetAndIncomeStatementHousingDpi', 'urn:bzk:linkrole:forecast-balance-sheet-and-income-statement-housing-dpi', 'Paragraaf 3.3 Prognose balans en winst-en-verliesrekening enkelvoudig of geconsolideerd', 'bzk-rpt-ti-woningwet.xsd', false, NULL);
INSERT INTO public.linkrole VALUES (10, 'bzk-ww-lr_ForecastDevelopmentEnergyLabelsRentalHousingUnitsHousingDpi', 'urn:bzk:linkrole:forecast-development-energy-labels-rental-housing-units-housing-dpi', 'Paragraaf 2.4 Prognose ontwikkeling energielabel huurwoongelegenheden', 'bzk-rpt-ti-woningwet.xsd', false, NULL);
INSERT INTO public.linkrole VALUES (11, 'bzk-ww-lr_ForecastSpecificationOfBusinessValueHousingDpi', 'urn:bzk:linkrole:forecast-specification-of-business-value-housing-dpi', 'Paragraaf 3.4 Prognose specificatie bedrijfswaarde enkelvoudig of geconsolideerd', 'bzk-rpt-ti-woningwet.xsd', false, NULL);
INSERT INTO public.linkrole VALUES (12, 'bzk-ww-lr_GeneralDataHousingDpi', 'urn:bzk:linkrole:general-data-housing-dpi', 'Paragraaf 1.2 Algemene gegevens', 'bzk-rpt-ti-woningwet.xsd', false, NULL);
INSERT INTO public.linkrole VALUES (13, 'bzk-ww-lr_NotesCashFlowStatementLibraryHousingDpi', 'urn:bzk:linkrole:notes-cash-flow-statement-library-housing-dpi', 'Paragraaf 3.2 Toelichtingen kasstroomoverzicht enkelvoudig en/of geconsolideerd', 'bzk-rpt-ti-woningwet.xsd', false, NULL);
INSERT INTO public.linkrole VALUES (14, 'bzk-ww-lr_NotesToTheGeneralHousingDpi', 'urn:bzk:linkrole:notes-to-the-general-housing-dpi', 'Specifieke elementen prognose informatie en/of verantwoordingsinformatie', 'bzk-rpt-ti-woningwet.xsd', false, NULL);
INSERT INTO public.linkrole VALUES (15, 'bzk-ww-lr_NotesValuationProperty', 'urn:bzk:linkrole:notes-valuation-property', 'Paragraaf 4.2 Toelichting waardering bezit toegelaten instelling', 'bzk-rpt-ti-woningwet.xsd', false, NULL);
INSERT INTO public.linkrole VALUES (16, 'linkrole-info', 'http://www.nltaxonomie.nl/2011/role/linkrole-info', '', 'bzk-rpt-ti-woningwet.xsd', true, NULL);




--
-- TOC entry 3372 (class 0 OID 203247)
-- Dependencies: 199
-- Data for Name: label; Type: TABLE DATA; Schema: public; Owner: postgres
--
INSERT INTO public.label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtSinglePersonHouseholds_documentation_nl', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtSinglePersonHouseholds_documentation_nl', 'http://www.xbrl.org/2003/role/documentation', 'nl', 'Eenpersoonshuishouden boven de pensioengerechtigde leeftijd met een inkomen hoger dan de inkomensgrens. (Wet op de Huurtoeslag: Pensioenleeftijd en huishoudsamenstelling als bepaald in artikel 2. Inkomensgrens (norminkomen) als bepaald in artikel 14.)');
INSERT INTO public.label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtSinglePersonHouseholds_label_nl', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtSinglePersonHouseholds_label_nl', 'http://www.xbrl.org/2003/role/label', 'nl', 'Boven de pensioengerechtigde leeftijd, inkomen hoger dan inkomensgrens Wht, eenpersoonshuishoudens');
INSERT INTO public.label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtSinglePersonHouseholds_terseLabel_nl', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtSinglePersonHouseholds_terseLabel_nl', 'http://www.xbrl.org/2003/role/terseLabel', 'nl', 'Boven de pensioengerechtigde leeftijd, inkomen hoger dan inkomensgrens Wht');
INSERT INTO public.label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtSinglePersonHouseholds_label_en', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtSinglePersonHouseholds_label_en', 'http://www.xbrl.org/2003/role/label', 'en', 'Above the retirement age, income lower higher than income limit Wht, single person households');
INSERT INTO public.label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtSinglePersonHouseholds_terseLabel_en', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtSinglePersonHouseholds_terseLabel_en', 'http://www.xbrl.org/2003/role/terseLabel', 'en', 'Above the retirement age, income lower higher than income limit Wht');
INSERT INTO public.label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTripleAndMultiplePerson_documentation_nl', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTripleAndMultiplePerson_documentation_nl', 'http://www.xbrl.org/2003/role/documentation', 'nl', 'Drie of meerpersoonshuishouden boven de pensioengerechtigde leeftijd met een inkomen hoger dan de inkomensgrens. (Wet op de Huurtoeslag: Pensioenleeftijd en huishoudsamenstelling als bepaald in artikel 2. Inkomensgrens (norminkomen) als bepaald in artikel 14.)');
INSERT INTO public.label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTripleAndMultiplePerson_label_nl', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTripleAndMultiplePerson_label_nl', 'http://www.xbrl.org/2003/role/label', 'nl', 'Boven de pensioengerechtigde leeftijd, inkomen hoger dan inkomensgrens Wht, drie en meerpersoonshuishoudens');
INSERT INTO public.label VALUES ('bzk-ww-i_AttributableFinancingCosts_negatedLabel_en', 'bzk-ww-i_AttributableFinancingCosts_negatedLabel_en', 'http://www.xbrl.org/2009/role/negatedLabel', 'en', 'Attributable financing costs');
INSERT INTO public.label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTripleAndMultiplePerson_terseLabel_nl', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTripleAndMultiplePerson_terseLabel_nl', 'http://www.xbrl.org/2003/role/terseLabel', 'nl', 'Boven de pensioengerechtigde leeftijd, inkomen hoger dan inkomensgrens Wht');
INSERT INTO public.label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTripleAndMultiplePerson_label_en', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTripleAndMultiplePerson_label_en', 'http://www.xbrl.org/2003/role/label', 'en', 'Above the retirement age, income lower higher than income limit Wht, three and multiple person households');
INSERT INTO public.label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTripleAndMultiplePerson_terseLabel_en', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTripleAndMultiplePerson_terseLabel_en', 'http://www.xbrl.org/2003/role/terseLabel', 'en', 'Above the retirement age, income lower higher than income limit Wht');
INSERT INTO public.label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTwoPersonHouseholds_documentation_nl', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTwoPersonHouseholds_documentation_nl', 'http://www.xbrl.org/2003/role/documentation', 'nl', 'Tweepersoonshuishouden boven de pensioengerechtigde leeftijd met een inkomen hoger dan de inkomensgrens. (Wet op de Huurtoeslag: Pensioenleeftijd en huishoudsamenstelling als bepaald in artikel 2. Inkomensgrens (norminkomen) als bepaald in artikel 14.)');
INSERT INTO public.label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTwoPersonHouseholds_label_nl', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTwoPersonHouseholds_label_nl', 'http://www.xbrl.org/2003/role/label', 'nl', 'Boven de pensioengerechtigde leeftijd, inkomen hoger dan inkomensgrens Wht, tweepersoonshuishoudens');
INSERT INTO public.label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTwoPersonHouseholds_terseLabel_nl', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTwoPersonHouseholds_terseLabel_nl', 'http://www.xbrl.org/2003/role/terseLabel', 'nl', 'Boven de pensioengerechtigde leeftijd, inkomen hoger dan inkomensgrens Wht');
INSERT INTO public.label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTwoPersonHouseholds_label_en', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTwoPersonHouseholds_label_en', 'http://www.xbrl.org/2003/role/label', 'en', 'Above the retirement age, income lower higher than income limit Wht, two person households');
INSERT INTO public.label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTwoPersonHouseholds_terseLabel_en', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTwoPersonHouseholds_terseLabel_en', 'http://www.xbrl.org/2003/role/terseLabel', 'en', 'Above the retirement age, income lower higher than income limit Wht');
INSERT INTO public.label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtSinglePersonHouseholds_documentation_nl', 'bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtSinglePersonHouseholds_documentation_nl', 'http://www.xbrl.org/2003/role/documentation', 'nl', 'Eenpersoonshuishouden boven de pensioengerechtigde leeftijd met een inkomen lager of gelijk aan de inkomensgrens. (Wet op de Huurtoeslag: Pensioenleeftijd en huishoudsamenstelling als bepaald in artikel 2. Inkomensgrens (norminkomen) als bepaald in artikel 14.)');
INSERT INTO public.label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtSinglePersonHouseholds_label_nl', 'bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtSinglePersonHouseholds_label_nl', 'http://www.xbrl.org/2003/role/label', 'nl', 'Boven de pensioengerechtigde leeftijd, inkomen lager of gelijk inkomensgrens Wht, eenpersoonshuishoudens');
INSERT INTO public.label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtSinglePersonHouseholds_terseLabel_nl', 'bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtSinglePersonHouseholds_terseLabel_nl', 'http://www.xbrl.org/2003/role/terseLabel', 'nl', 'Boven de pensioengerechtigde leeftijd, inkomen lager of gelijk inkomensgrens Wht');
INSERT INTO public.label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtSinglePersonHouseholds_label_en', 'bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtSinglePersonHouseholds_label_en', 'http://www.xbrl.org/2003/role/label', 'en', 'Above the retirement age, income lower or equal to income limit Wht, single person households');
INSERT INTO public.label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtSinglePersonHouseholds_terseLabel_en', 'bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtSinglePersonHouseholds_terseLabel_en', 'http://www.xbrl.org/2003/role/terseLabel', 'en', 'Above the retirement age, income lower or equal to income limit Wht');
INSERT INTO public.label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtTripleAndMultiplePerson_documentation_nl', 'bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtTripleAndMultiplePerson_documentation_nl', 'http://www.xbrl.org/2003/role/documentation', 'nl', 'Drie of meerpersoonshuishouden boven de pensioengerechtigde leeftijd met een inkomen lager of gelijk aan de inkomensgrens. (Wet op de Huurtoeslag: Pensioenleeftijd en huishoudsamenstelling als bepaald in artikel 2. Inkomensgrens (norminkomen) als bepaald in artikel 14.)');
INSERT INTO public.label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtTripleAndMultiplePerson_label_nl', 'bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtTripleAndMultiplePerson_label_nl', 'http://www.xbrl.org/2003/role/label', 'nl', 'Boven de pensioengerechtigde leeftijd, inkomen lager of gelijk inkomensgrens Wht, drie en meerpersoonshuishoudens');
INSERT INTO public.label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtTripleAndMultiplePerson_terseLabel_nl', 'bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtTripleAndMultiplePerson_terseLabel_nl', 'http://www.xbrl.org/2003/role/terseLabel', 'nl', 'Boven de pensioengerechtigde leeftijd, inkomen lager of gelijk inkomensgrens Wht');
INSERT INTO public.label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtTripleAndMultiplePerson_label_en', 'bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtTripleAndMultiplePerson_label_en', 'http://www.xbrl.org/2003/role/label', 'en', 'Above the retirement age, income lower or equal to income limit Wht, three and multiple person households');
INSERT INTO public.label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtTripleAndMultiplePerson_terseLabel_en', 'bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtTripleAndMultiplePerson_terseLabel_en', 'http://www.xbrl.org/2003/role/terseLabel', 'en', 'Above the retirement age, income lower or equal to income limit Wht');
INSERT INTO public.label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtTwoPersonHouseholds_documentation_nl', 'bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtTwoPersonHouseholds_documentation_nl', 'http://www.xbrl.org/2003/role/documentation', 'nl', 'Tweepersoonshuishouden boven de pensioengerechtigde leeftijd met een inkomen lager of gelijk aan de inkomensgrens. (Wet op de Huurtoeslag: Pensioenleeftijd en huishoudsamenstelling als bepaald in artikel 2. Inkomensgrens (norminkomen) als bepaald in artikel 14.)');
INSERT INTO public.label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtTwoPersonHouseholds_label_nl', 'bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtTwoPersonHouseholds_label_nl', 'http://www.xbrl.org/2003/role/label', 'nl', 'Boven de pensioengerechtigde leeftijd, inkomen lager of gelijk inkomensgrens Wht, tweepersoonshuishoudens');
INSERT INTO public.label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtTwoPersonHouseholds_terseLabel_nl', 'bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtTwoPersonHouseholds_terseLabel_nl', 'http://www.xbrl.org/2003/role/terseLabel', 'nl', 'Boven de pensioengerechtigde leeftijd, inkomen lager of gelijk inkomensgrens Wht');
INSERT INTO public.label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtTwoPersonHouseholds_label_en', 'bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtTwoPersonHouseholds_label_en', 'http://www.xbrl.org/2003/role/label', 'en', 'Above the retirement age, income lower or equal to income limit Wht, two person households');
INSERT INTO public.label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtTwoPersonHouseholds_terseLabel_en', 'bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtTwoPersonHouseholds_terseLabel_en', 'http://www.xbrl.org/2003/role/terseLabel', 'en', 'Above the retirement age, income lower or equal to income limit Wht');

INSERT INTO public.label VALUES ('bzk_ww-codes_BranchType_Daeb_label_nl', 'bzk_ww-codes_BranchType_Daeb_label_nl', 'http://www.xbrl.org/2008/role/label', 'nl', 'DAEB');
INSERT INTO public.label VALUES ('bzk_ww-codes_BranchType_Daeb_label_en', 'bzk_ww-codes_BranchType_Daeb_label_en', 'http://www.xbrl.org/2008/role/label', 'en', 'DAEB');
INSERT INTO public.label VALUES ('bzk_ww-codes_BranchType_NonDaebCompound_label_nl', 'bzk_ww-codes_BranchType_NonDaebCompound_label_nl', 'http://www.xbrl.org/2008/role/label', 'nl', 'Geconsolideerde niet-DAEB verbindingen');
INSERT INTO public.label VALUES ('bzk_ww-codes_BranchType_NonDaebCompound_label_en', 'bzk_ww-codes_BranchType_NonDaebCompound_label_en', 'http://www.xbrl.org/2008/role/label', 'en', 'Consolidated non-DAEB compound');
INSERT INTO public.label VALUES ('bzk_ww-codes_BranchType_NonDaeb_label_nl', 'bzk_ww-codes_BranchType_NonDaeb_label_nl', 'http://www.xbrl.org/2008/role/label', 'nl', 'niet-DAEB');
INSERT INTO public.label VALUES ('bzk_ww-codes_BranchType_NonDaeb_label_en', 'bzk_ww-codes_BranchType_NonDaeb_label_en', 'http://www.xbrl.org/2008/role/label', 'en', 'non-DAEB');
INSERT INTO public.label VALUES ('bzk_ww_codes_ChairmanMember_Member_label_nl', 'bzk_ww_codes_ChairmanMember_Member_label_nl', 'http://www.xbrl.org/2008/role/label', 'nl', 'Lid');
INSERT INTO public.label VALUES ('bzk_ww_codes_ChairmanMember_Member_label_en', 'bzk_ww_codes_ChairmanMember_Member_label_en', 'http://www.xbrl.org/2008/role/label', 'en', 'Member');
INSERT INTO public.label VALUES ('bzk_ww_codes_ChairmanMember_Chairman_label_nl', 'bzk_ww_codes_ChairmanMember_Chairman_label_nl', 'http://www.xbrl.org/2008/role/label', 'nl', 'Voorzitter');
INSERT INTO public.label VALUES ('bzk_ww_codes_ChairmanMember_Chairman_label_en', 'bzk_ww_codes_ChairmanMember_Chairman_label_en', 'http://www.xbrl.org/2008/role/label', 'en', 'Chairman');
INSERT INTO public.label VALUES ('bzk_ww-codes_LoanType_Variable_label_nl', 'bzk_ww-codes_LoanType_Variable_label_nl', 'http://www.xbrl.org/2008/role/label', 'nl', 'Variabel');
INSERT INTO public.label VALUES ('bzk_ww-codes_LoanType_Variable_label_en', 'bzk_ww-codes_LoanType_Variable_label_en', 'http://www.xbrl.org/2008/role/label', 'en', 'Variable');
INSERT INTO public.label VALUES ('bzk_ww-codes_LoanType_Fixed_label_nl', 'bzk_ww-codes_LoanType_Fixed_label_nl', 'http://www.xbrl.org/2008/role/label', 'nl', 'Vast');
INSERT INTO public.label VALUES ('bzk_ww-codes_LoanType_Fixed_label_en', 'bzk_ww-codes_LoanType_Fixed_label_en', 'http://www.xbrl.org/2008/role/label', 'en', 'Fixed');
INSERT INTO public.label VALUES ('rj-codes_relatedPartyMainActivitiesItemType_label_nl', 'rj-codes_relatedPartyMainActivitiesItemType_label_nl', 'http://www.xbrl.org/2008/role/label', 'nl', 'Belangrijkste activiteiten van de verbonden partij');
INSERT INTO public.label VALUES ('rj-codes_financialStatementItemType_label_nl', 'rj-codes_financialStatementItemType_label_nl', 'http://www.xbrl.org/2008/role/label', 'nl', 'Grondslag jaarrekening');
INSERT INTO public.label VALUES ('rj-codes_financingDeficitSurplusToBeSettledInRatesStageOfAdoptionItemType_label_nl', 'rj-codes_financingDeficitSurplusToBeSettledInRatesStageOfAdoptionItemType_label_nl', 'http://www.xbrl.org/2008/role/label', 'nl', 'Stadium van vaststelling van nog in tarieven te verrekenen financieringstekort/-overschot');
INSERT INTO public.label VALUES ('bzk-ww-sim-codes_investmentcategorySimpleType_label_en', 'bzk-ww-sim-codes_investmentcategorySimpleType_label_en', 'http://www.xbrl.org/2008/role/label', 'en', 'Investment category');
INSERT INTO public.label VALUES ('bzk-ww-sim-codes_typeofpropertySimpleType_label_en', 'bzk-ww-sim-codes_typeofpropertySimpleType_label_en', 'http://www.xbrl.org/2008/role/label', 'en', 'Type of property');
INSERT INTO public.label VALUES ('nl-codes_legalItemType_label_fr', 'nl-codes_legalItemType_label_fr', 'http://www.xbrl.org/2008/role/label', 'fr', 'Entité légale');
INSERT INTO public.label VALUES ('nl-codes_yesNoItemType_label_fr', 'nl-codes_yesNoItemType_label_fr', 'http://www.xbrl.org/2008/role/label', 'fr', 'Oui / Non');
INSERT INTO public.label VALUES ('nl-codes_personRoleItemType_label_fr', 'nl-codes_personRoleItemType_label_fr', 'http://www.xbrl.org/2008/role/label', 'fr', 'Rôle de la personne');
INSERT INTO public.label VALUES ('nl-codes_genderItemType_label_fr', 'nl-codes_genderItemType_label_fr', 'http://www.xbrl.org/2008/role/label', 'fr', 'Sexe');
INSERT INTO public.label VALUES ('jenv-codes_basisOfPreparationItemType_label_en', 'jenv-codes_basisOfPreparationItemType_label_en', 'http://www.xbrl.org/2008/role/label', 'en', 'Basis of preparation');
INSERT INTO public.label VALUES ('jenv-codes_directorItemType_label_en', 'jenv-codes_directorItemType_label_en', 'http://www.xbrl.org/2008/role/label', 'en', 'Director');
INSERT INTO public.label VALUES ('jenv-codes_legalFormItemType_label_en', 'jenv-codes_legalFormItemType_label_en', 'http://www.xbrl.org/2008/role/label', 'en', 'Legal form');
INSERT INTO public.label VALUES ('jenv-codes_profitAppropriationItemType_label_en', 'jenv-codes_profitAppropriationItemType_label_en', 'http://www.xbrl.org/2008/role/label', 'en', 'Profit appropriation');
INSERT INTO public.label VALUES ('sbr-arc_ConceptDisclosure_label_en', 'sbr-arc_ConceptDisclosure_label_en', 'http://www.xbrl.org/2008/role/label', 'en', 'Arcrole used for denoting a semantic relationship between monetary concepts and disclosure concepts.');
INSERT INTO public.label VALUES ('sbr-arc_ConceptMemberEquivalence_label_en', 'sbr-arc_ConceptMemberEquivalence_label_en', 'http://www.xbrl.org/2008/role/label', 'en', 'Arcrole used for denoting a semantic relationship between monetary concepts and equivalent domain members.');
INSERT INTO public.label VALUES ('sbr-arc_ConceptPolicy_label_en', 'sbr-arc_ConceptPolicy_label_en', 'http://www.xbrl.org/2008/role/label', 'en', 'Arcrole used for denoting a semantic relationship between monetary concepts and policy concepts.');
INSERT INTO public.label VALUES ('sbr-arc_DynamicLabel_label_en', 'sbr-arc_DynamicLabel_label_en', 'http://www.xbrl.org/2008/role/label', 'en', 'Arcrole used for linking messages to nodes.');
INSERT INTO public.label VALUES ('rj-codes_financialStatementItemType_label_en', 'rj-codes_financialStatementItemType_label_en', 'http://www.xbrl.org/2008/role/label', 'en', 'Basis of preparation financial statement');
INSERT INTO public.label VALUES ('rj-codes_relatedPartyMainActivitiesItemType_label_en', 'rj-codes_relatedPartyMainActivitiesItemType_label_en', 'http://www.xbrl.org/2008/role/label', 'en', 'Main activities of the related party');
INSERT INTO public.label VALUES ('rj-codes_financingDeficitSurplusToBeSettledInRatesStageOfAdoptionItemType_label_en', 'rj-codes_financingDeficitSurplusToBeSettledInRatesStageOfAdoptionItemType_label_en', 'http://www.xbrl.org/2008/role/label', 'en', 'Stage of adoption of financing deficit/surplus to be settled in rates');
INSERT INTO public.label VALUES ('bzk-ww-codes_branchItemType_label_en', 'bzk-ww-codes_branchItemType_label_en', 'http://www.xbrl.org/2008/role/label', 'en', 'Branch');
INSERT INTO public.label VALUES ('bzk-ww-codes_chairmanMemberItemType_label_en', 'bzk-ww-codes_chairmanMemberItemType_label_en', 'http://www.xbrl.org/2008/role/label', 'en', 'Chairman of member');
INSERT INTO public.label VALUES ('bzk-ww-codes_loanItemType_label_en', 'bzk-ww-codes_loanItemType_label_en', 'http://www.xbrl.org/2008/role/label', 'en', 'Loan type');
INSERT INTO public.label VALUES ('bzk-ww-codes_municipality2021ItemType_label_en', 'bzk-ww-codes_municipality2021ItemType_label_en', 'http://www.xbrl.org/2008/role/label', 'en', 'Municipality');
INSERT INTO public.label VALUES ('bzk-ww-codes_redemptionFormItemType_label_en', 'bzk-ww-codes_redemptionFormItemType_label_en', 'http://www.xbrl.org/2008/role/label', 'en', 'Redemption form');
INSERT INTO public.label VALUES ('bzk-ww-codes_valuationMethodItemType_label_en', 'bzk-ww-codes_valuationMethodItemType_label_en', 'http://www.xbrl.org/2008/role/label', 'en', 'Valuation method');
INSERT INTO public.label VALUES ('bzk-ww-codes_yesNoNotApplicableItemType_label_en', 'bzk-ww-codes_yesNoNotApplicableItemType_label_en', 'http://www.xbrl.org/2008/role/label', 'en', 'Yes / No');




--
-- TOC entry 3374 (class 0 OID 203281)
-- Dependencies: 201
-- Data for Name: itemtype; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.itemtype VALUES (NULL, 'nonEmptyURI', 'xl', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'useEnum', 'xl', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'documentationType', 'xl', 'string', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'titleType', 'xl', 'anyType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'locatorType', 'xl', 'anyType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'arcType', 'xl', 'anyType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'resourceType', 'xl', 'anyType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'extendedType', 'xl', 'anyType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'simpleType', 'xl', 'anyType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'contextElementType', 'xbrldt', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'monetary', 'xbrli', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'shares', 'xbrli', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'pure', 'xbrli', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'nonZeroDecimal', 'xbrli', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'precisionType', 'xbrli', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'decimalsType', 'xbrli', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'dateUnion', 'xbrli', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'decimalItemType', 'xbrli', 'decimal', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'floatItemType', 'xbrli', 'float', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'doubleItemType', 'xbrli', 'double', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'monetaryItemType', 'xbrli', 'xbrli:monetary', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'sharesItemType', 'xbrli', 'xbrli:shares', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'pureItemType', 'xbrli', 'xbrli:pure', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'fractionItemType', 'xbrli', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'integerItemType', 'xbrli', 'integer', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'nonPositiveIntegerItemType', 'xbrli', 'nonPositiveInteger', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'negativeIntegerItemType', 'xbrli', 'negativeInteger', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'longItemType', 'xbrli', 'long', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'intItemType', 'xbrli', 'int', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'shortItemType', 'xbrli', 'short', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'byteItemType', 'xbrli', 'byte', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'nonNegativeIntegerItemType', 'xbrli', 'nonNegativeInteger', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'unsignedLongItemType', 'xbrli', 'unsignedLong', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'unsignedIntItemType', 'xbrli', 'unsignedInt', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'unsignedShortItemType', 'xbrli', 'unsignedShort', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'unsignedByteItemType', 'xbrli', 'unsignedByte', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'positiveIntegerItemType', 'xbrli', 'positiveInteger', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'stringItemType', 'xbrli', 'string', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'booleanItemType', 'xbrli', 'boolean', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'hexBinaryItemType', 'xbrli', 'hexBinary', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'base64BinaryItemType', 'xbrli', 'base64Binary', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'anyURIItemType', 'xbrli', 'anyURI', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'QNameItemType', 'xbrli', 'QName', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'durationItemType', 'xbrli', 'duration', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'dateTimeItemType', 'xbrli', 'xbrli:dateUnion', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'timeItemType', 'xbrli', 'time', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'dateItemType', 'xbrli', 'date', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'gYearMonthItemType', 'xbrli', 'gYearMonth', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'gYearItemType', 'xbrli', 'gYear', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'gMonthDayItemType', 'xbrli', 'gMonthDay', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'gDayItemType', 'xbrli', 'gDay', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'gMonthItemType', 'xbrli', 'gMonth', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'normalizedStringItemType', 'xbrli', 'normalizedString', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'tokenItemType', 'xbrli', 'token', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'languageItemType', 'xbrli', 'language', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'NameItemType', 'xbrli', 'Name', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'NCNameItemType', 'xbrli', 'NCName', 'extends', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'contextEntityType', 'xbrli', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'contextPeriodType', 'xbrli', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'contextScenarioType', 'xbrli', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'measuresType', 'xbrli', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('bzk-ww-codes_branchItemType', 'branchItemType', 'bzk-ww-codes', 'xbrli:stringItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('bzk-ww-codes_chairmanMemberItemType', 'chairmanMemberItemType', 'bzk-ww-codes', 'xbrli:stringItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('bzk-ww-codes_loanItemType', 'loanItemType', 'bzk-ww-codes', 'xbrli:stringItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('bzk-ww-codes_municipality2021ItemType', 'municipality2021ItemType', 'bzk-ww-codes', 'xbrli:stringItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('bzk-ww-codes_redemptionFormItemType', 'redemptionFormItemType', 'bzk-ww-codes', 'xbrli:stringItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('bzk-ww-codes_valuationMethodItemType', 'valuationMethodItemType', 'bzk-ww-codes', 'xbrli:stringItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('bzk-ww-codes_yesNoNotApplicableItemType', 'yesNoNotApplicableItemType', 'bzk-ww-codes', 'xbrli:stringItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('bzk-ww-types_gYearRangeItemType', 'gYearRangeItemType', 'bzk-ww-types', 'xbrli:gYearItemType', 'restricts', NULL, NULL, '([1-2]{1}[0-9]{3})', NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('bzk-ww-types_monetary2Decimals20ItemType', 'monetary2Decimals20ItemType', 'bzk-ww-types', 'nl-types:monetary20ItemType', 'restricts', NULL, NULL, '([0-9])*\.[0-9]{2}', NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('bzk-ww-types_nonNegative1DecimalItemType', 'nonNegative1DecimalItemType', 'bzk-ww-types', 'xbrli:decimalItemType', 'restricts', NULL, NULL, '[0-9]*(\.[0-9])?', NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('bzk-ww-types_nonNegativeMax1Percent4DecimalsItemType', 'nonNegativeMax1Percent4DecimalsItemType', 'bzk-ww-types', 'num:percentItemType', 'restricts', NULL, NULL, '[0-1]{1}(\.){0,1}[0-9]{0,4}', NULL, NULL, NULL, NULL, '0');
INSERT INTO public.itemtype VALUES ('bzk-ww-types_percent4DecimalsItemType', 'percent4DecimalsItemType', 'bzk-ww-types', 'nl-types:percent20ItemType', 'restricts', NULL, NULL, '[0-9]{0,16}(\.){0,1}[0-9]{0,4}', NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('bzk-ww-types_percentFourDecimalsItemType', 'percentFourDecimalsItemType', 'bzk-ww-types', 'nl-types:percent20ItemType', 'restricts', NULL, NULL, '-{0,1}[0-9]{0,16}(\.){0,1}[0-9]{0,4}', NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('bzk-ww-types_percentMax1ItemType', 'percentMax1ItemType', 'bzk-ww-types', 'num:percentItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-1');
INSERT INTO public.itemtype VALUES ('nl-types_string255', 'string255', 'nl-types', NULL, NULL, NULL, NULL, NULL, 255, 1, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('nl-types_area20ItemType', 'area20ItemType', 'nl-types', 'dtr-types:areaItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, 20, NULL, NULL);
INSERT INTO public.itemtype VALUES ('nl-types_chamberOfCommerceRegistrationNumberItemType', 'chamberOfCommerceRegistrationNumberItemType', 'nl-types', 'xbrli:stringItemType', 'restricts', NULL, 8, '([1-9][0-9]{7})|([0-9][1-9][0-9]{6})', NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('nl-types_Decimal20ItemType', 'decimal20ItemType', 'nl-types', 'xbrli:decimalItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, 20, NULL, NULL);
INSERT INTO public.itemtype VALUES ('nl-types_emailItemType', 'emailItemType', 'nl-types', 'xbrli:stringItemType', 'restricts', NULL, NULL, '[^@]+@[^@]+', 254, 6, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('nl-types_formattedExplanationItemType', 'formattedExplanationItemType', 'nl-types', 'nl-types:string100000ItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('nl-types_houseLetterNLItemType', 'houseLetterNLItemType', 'nl-types', 'xbrli:stringItemType', 'restricts', NULL, NULL, '[a-zA-Z]{1}', NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('nl-types_houseNumberNLItemType', 'houseNumberNLItemType', 'nl-types', 'xbrli:stringItemType', 'restricts', NULL, NULL, '[0-9]{1,5}', NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('nl-types_imageItemType', 'imageItemType', 'nl-types', 'xbrli:stringItemType', 'restricts', NULL, NULL, NULL, 2000000, 1, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('nl-types_integer20ItemType', 'integer20ItemType', 'nl-types', 'xbrli:integerItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, 20, NULL, NULL);
INSERT INTO public.itemtype VALUES ('nl-types_monetary20ItemType', 'monetary20ItemType', 'nl-types', 'xbrli:monetaryItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, 20, NULL, NULL);
INSERT INTO public.itemtype VALUES ('nl-types_monetaryNoDecimals20ItemType', 'monetaryNoDecimals20ItemType', 'nl-types', 'dtr-types:noDecimalsMonetaryItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, 20, NULL, NULL);
INSERT INTO public.itemtype VALUES ('nl-types_nlzipItemType', 'nlzipItemType', 'nl-types', 'xbrli:stringItemType', 'restricts', NULL, NULL, '([1-9]{1}[0-9]{3}[A-Z]{2})', NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('nl-types_nonNegative20ItemType', 'nonNegative20ItemType', 'nl-types', 'xbrli:stringItemType', 'restricts', NULL, NULL, '([0-9]{1,20})', NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('nl-types_nonNegativeDecimal20ItemType', 'nonNegativeDecimal20ItemType', 'nl-types', 'nl-types:decimal20ItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0');
INSERT INTO public.itemtype VALUES ('nl-types_nonNegativeInteger20ItemType', 'nonNegativeInteger20ItemType', 'nl-types', 'xbrli:nonNegativeIntegerItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, 20, NULL, NULL);
INSERT INTO public.itemtype VALUES ('nl-types_nonNegativeMonetary20ItemType', 'nonNegativeMonetary20ItemType', 'nl-types', 'dtr-types:nonNegativeMonetaryItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, 20, NULL, NULL);
INSERT INTO public.itemtype VALUES ('nl-types_nonNegativeMonetaryNoDecimals20ItemType', 'nonNegativeMonetaryNoDecimals20ItemType', 'nl-types', 'dtr-types:nonNegativeNoDecimalsMonetaryItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, 20, NULL, NULL);
INSERT INTO public.itemtype VALUES ('nl-types_perShare20ItemType', 'perShare20ItemType', 'nl-types', 'dtr-types:perShareItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, 20, NULL, NULL);
INSERT INTO public.itemtype VALUES ('nl-types_percent20ItemType', 'percent20ItemType', 'nl-types', 'dtr-types:percentItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, 20, NULL, NULL);
INSERT INTO public.itemtype VALUES ('nl-types_shares20ItemType', 'shares20ItemType', 'nl-types', 'xbrli:sharesItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, 20, NULL, NULL);
INSERT INTO public.itemtype VALUES ('nl-types_string4ItemType', 'string4ItemType', 'nl-types', 'xbrli:stringItemType', 'restricts', NULL, NULL, NULL, 4, 1, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('nl-types_string24ItemType', 'string24ItemType', 'nl-types', 'xbrli:stringItemType', 'restricts', NULL, NULL, NULL, 24, 1, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('nl-types_string80ItemType', 'string80ItemType', 'nl-types', 'xbrli:stringItemType', 'restricts', NULL, NULL, NULL, 80, 1, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('nl-types_string250ItemType', 'string250ItemType', 'nl-types', 'xbrli:stringItemType', 'restricts', NULL, NULL, NULL, 250, 1, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('nl-types_string500ItemType', 'string500ItemType', 'nl-types', 'xbrli:stringItemType', 'restricts', NULL, NULL, NULL, 500, 1, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('nl-types_string1000ItemType', 'string1000ItemType', 'nl-types', 'xbrli:stringItemType', 'restricts', NULL, NULL, NULL, 1000, 1, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('nl-types_string100000ItemType', 'string100000ItemType', 'nl-types', 'xbrli:stringItemType', 'restricts', NULL, NULL, NULL, 100000, 1, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'prefixedContentType', 'dtr-types', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'SQNameType', 'dtr-types', NULL, NULL, NULL, NULL, '[^:\s0-9][^:\s]*:\S+', NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'SQNamesType', 'dtr-types', NULL, NULL, NULL, NULL, '([^:\s0-9][^:\s]*:\S+)?(\s+[^:\s0-9][^:\s]*:\S+)*', NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('domainItemType', 'domainItemType', 'dtr-types', 'xbrli:stringItemType', 'restricts', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('escapedItemType', 'escapedItemType', 'dtr-types', 'xbrli:stringItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('xmlNodesItemType', 'xmlNodesItemType', 'dtr-types', 'dtr-types:escapedItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('xmlItemType', 'xmlItemType', 'dtr-types', 'dtr-types:xmlNodesItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('textBlockItemType', 'textBlockItemType', 'dtr-types', 'dtr-types:xmlNodesItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'percentItemType', 'dtr-types', 'xbrli:pureItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'perShareItemType', 'dtr-types', 'xbrli:decimalItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'areaItemType', 'dtr-types', 'xbrli:decimalItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'volumeItemType', 'dtr-types', 'xbrli:decimalItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'massItemType', 'dtr-types', 'xbrli:decimalItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'weightItemType', 'dtr-types', 'xbrli:decimalItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'energyItemType', 'dtr-types', 'xbrli:decimalItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'powerItemType', 'dtr-types', 'xbrli:decimalItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'lengthItemType', 'dtr-types', 'xbrli:decimalItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'memoryItemType', 'dtr-types', 'xbrli:decimalItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('dtr_NoDecimalsMonetaryItemType', 'noDecimalsMonetaryItemType', 'dtr-types', 'xbrli:monetaryItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL);
INSERT INTO public.itemtype VALUES ('dtr_nonNegativeMonetaryItemType', 'nonNegativeMonetaryItemType', 'dtr-types', 'xbrli:monetaryItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0');
INSERT INTO public.itemtype VALUES ('dtr_nonNegativeNoDecimalsMonetaryItemType', 'nonNegativeNoDecimalsMonetaryItemType', 'dtr-types', 'xbrli:monetaryItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, 0, '0');
INSERT INTO public.itemtype VALUES (NULL, 'insolationItemType', 'dtr-types', 'xbrli:decimalItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'temperatureItemType', 'dtr-types', 'xbrli:decimalItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'pressureItemType', 'dtr-types', 'xbrli:decimalItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'frequencyItemType', 'dtr-types', 'xbrli:decimalItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'irradianceItemType', 'dtr-types', 'xbrli:decimalItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'speedItemType', 'dtr-types', 'xbrli:decimalItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'planeAngleItemType', 'dtr-types', 'xbrli:decimalItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'voltageItemType', 'dtr-types', 'xbrli:decimalItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'electricCurrentItemType', 'dtr-types', 'xbrli:decimalItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'forceItemType', 'dtr-types', 'xbrli:decimalItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'electricChargeItemType', 'dtr-types', 'xbrli:decimalItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'guidanceItemType', 'dtr-types', 'xbrli:stringItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'noLangTokenItemType', 'dtr-types', 'xbrli:tokenItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'noLangStringItemType', 'dtr-types', 'xbrli:stringItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'prefixedContentItemType', 'dtr-types', 'dtr-types:noLangTokenItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'SQNameItemType', 'dtr-types', 'dtr-types:prefixedContentItemType', 'restricts', NULL, NULL, '[^:\s0-9][^:\s]*:\S+', NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'SQNamesItemType', 'dtr-types', 'dtr-types:prefixedContentItemType', 'restricts', NULL, NULL, '([^:\s0-9][^:\s]*:\S+)?(\s+[^:\s0-9][^:\s]*:\S+)*', NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'gYearListItemType', 'dtr-types', 'xbrli:tokenItemType', 'restricts', NULL, NULL, '\s*(-?[0-9]{4,}(\s+-?[0-9]{4,})*)?\s*', NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('flowItemType', 'flowItemType', 'dtr-types', 'xbrli:decimalItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('massFlowItemType', 'massFlowItemType', 'dtr-types', 'xbrli:decimalItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'monetaryPerLengthItemType', 'dtr-types', 'xbrli:decimalItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'monetaryPerAreaItemType', 'dtr-types', 'xbrli:decimalItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'monetaryPerVolumeItemType', 'dtr-types', 'xbrli:decimalItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'monetaryPerDurationItemType', 'dtr-types', 'xbrli:decimalItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'monetaryPerEnergyItemType', 'dtr-types', 'xbrli:decimalItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'monetaryPerMassItemType', 'dtr-types', 'xbrli:decimalItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('nl-codes_genderItemType', 'genderItemType', 'nl-codes', 'xbrli:stringItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('nl-codes_houseNumberIndicationItemType', 'houseNumberIndicationItemType', 'nl-codes', 'xbrli:stringItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('nl-codes_legalItemType', 'legalItemType', 'nl-codes', 'xbrli:stringItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('nl-codes_personRoleItemType', 'personRoleItemType', 'nl-codes', 'xbrli:stringItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('nl-codes_yesNoItemType', 'yesNoItemType', 'nl-codes', 'xbrli:stringItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'placeholder', 'sbr', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('jenv-codes_basisOfPreparationItemType', 'basisOfPreparationItemType', 'jenv-codes', 'xbrli:stringItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('jenv-codes_directorItemType', 'directorItemType', 'jenv-codes', 'xbrli:stringItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('jenv-codes_legalFormItemType', 'legalFormItemType', 'jenv-codes', 'xbrli:stringItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('jenv-codes_profitAppropriationItemType', 'profitAppropriationItemType', 'jenv-codes', 'xbrli:stringItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('rj-codes_financialStatementItemType', 'financialStatementItemType', 'rj-codes', 'xbrli:stringItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('rj-codes_financingDeficitSurplusToBeSettledInRatesStageOfAdoptionItemType', 'financingDeficitSurplusToBeSettledInRatesStageOfAdoptionItemType', 'rj-codes', 'xbrli:stringItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('rj-codes_relatedPartyMainActivitiesItemType', 'relatedPartyMainActivitiesItemType', 'rj-codes', 'xbrli:stringItemType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'genericArcType', 'gen', 'xl:arcType', 'extents', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'linkType', 'gen', 'xl:extendedType', 'restricts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES (NULL, 'linkTypeWithOpenAttrs', 'gen', 'gen:linkType', 'extents', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('bzk-ww-sim-codes_investmentcategorySimpleType', 'investmentcategorySimpleType', 'bzk-ww-sim-codes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('bzk-ww-sim-codes_typeofpropertySimpleType', 'typeofpropertySimpleType', 'bzk-ww-sim-codes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('string', 'string', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.itemtype VALUES ('anyURI', 'anyURI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);


--
-- TOC entry 3370 (class 0 OID 203219)
-- Dependencies: 197
-- Data for Name: namespace; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.namespace VALUES ('bzk-rpt-ww', 'http://www.nltaxonomie.nl/nt17/bzk/20221214/entrypoints/bzk-rpt-ti-woningwet');
INSERT INTO public.namespace VALUES ('link', 'http://www.xbrl.org/2003/linkbase');
INSERT INTO public.namespace VALUES ('xlink', 'http://www.w3.org/1999/xlink');
INSERT INTO public.namespace VALUES ('xs', 'http://www.w3.org/2001/XMLSchema');
INSERT INTO public.namespace VALUES ('xl', 'http://www.xbrl.org/2003/XLink');
INSERT INTO public.namespace VALUES ('bzk-ww-lr', 'http://www.nltaxonomie.nl/nt17/bzk/20221214/dictionary/bzk-ww-linkroles');
INSERT INTO public.namespace VALUES ('xbrli', 'http://www.xbrl.org/2003/instance');
INSERT INTO public.namespace VALUES ('xbrldt', 'http://xbrl.org/2005/xbrldt');
INSERT INTO public.namespace VALUES ('bzk-ww-codes', 'http://www.nltaxonomie.nl/nt17/bzk/20221214/dictionary/bzk-ww-codes');
INSERT INTO public.namespace VALUES ('bzk-ww-i', 'http://www.nltaxonomie.nl/nt17/bzk/20221214/dictionary/bzk-ww-data');
INSERT INTO public.namespace VALUES ('bzk-ww-types', 'http://www.nltaxonomie.nl/nt17/bzk/20221214/dictionary/bzk-ww-types');
INSERT INTO public.namespace VALUES ('nl-codes', 'http://www.nltaxonomie.nl/nt17/sbr/20220301/dictionary/nl-codes');
INSERT INTO public.namespace VALUES ('nl-types', 'http://www.nltaxonomie.nl/nt17/sbr/20220301/dictionary/nl-types');
INSERT INTO public.namespace VALUES ('num', 'http://www.xbrl.org/dtr/type/numeric');
INSERT INTO public.namespace VALUES ('dtr-types', 'http://www.xbrl.org/dtr/type/2020-01-21');
INSERT INTO public.namespace VALUES ('dtr', 'http://www.xbrl.org/2009/dtr');
INSERT INTO public.namespace VALUES ('sbr', 'http://www.nltaxonomie.nl/2011/xbrl/xbrl-syntax-extension');
INSERT INTO public.namespace VALUES ('sbr-dim', 'http://www.nltaxonomie.nl/2013/xbrl/sbr-dimensional-concepts');
INSERT INTO public.namespace VALUES ('jenv-codes', 'http://www.nltaxonomie.nl/nt17/jenv/20221214/dictionary/jenv-codes');
INSERT INTO public.namespace VALUES ('rj-codes', 'http://www.nltaxonomie.nl/nt17/rj/20221214/dictionary/rj-codes');
INSERT INTO public.namespace VALUES ('rj-i', 'http://www.nltaxonomie.nl/nt17/rj/20221214/dictionary/rj-data');
INSERT INTO public.namespace VALUES ('label', 'http://xbrl.org/2008/label');
INSERT INTO public.namespace VALUES ('sbr-l', 'http://www.nltaxonomie.nl/2016/xbrl/xbrl-syntax-labelroles');
INSERT INTO public.namespace VALUES ('jenv-bw2-dm', 'http://www.nltaxonomie.nl/nt17/jenv/20221214/dictionary/jenv-bw2-domains');
INSERT INTO public.namespace VALUES ('jenv-bw2-i', 'http://www.nltaxonomie.nl/nt17/jenv/20221214/dictionary/jenv-bw2-data');
INSERT INTO public.namespace VALUES ('bzk-ww-abstr', 'http://www.nltaxonomie.nl/nt17/bzk/20221214/presentation/bzk-ww-abstracts');
INSERT INTO public.namespace VALUES ('gen', 'http://xbrl.org/2008/generic');
INSERT INTO public.namespace VALUES ('xsd', 'http://www.w3.org/2001/XMLSchema');
INSERT INTO public.namespace VALUES ('xsi', 'http://www.w3.org/2001/XMLSchema-instance');
INSERT INTO public.namespace VALUES ('negated', 'http://www.xbrl.org/2009/role/negated');
INSERT INTO public.namespace VALUES ('ref', 'http://www.xbrl.org/2006/ref');
INSERT INTO public.namespace VALUES ('sbr-arc', 'http://www.nltaxonomie.nl/2017/xbrl/sbr-arcroles');
INSERT INTO public.namespace VALUES ('bzk-ww-dm', 'http://www.nltaxonomie.nl/nt17/bzk/20221214/dictionary/bzk-ww-domains');
INSERT INTO public.namespace VALUES ('bzk-ww-sim-codes', 'http://www.nltaxonomie.nl/nt17/bzk/20221214/dictionary/bzk-ww-simple-type-codes');
INSERT INTO public.namespace VALUES ('rj-dm', 'http://www.nltaxonomie.nl/nt17/rj/20221214/dictionary/rj-domains');
INSERT INTO public.namespace VALUES ('reference', 'http://xbrl.org/2008/reference');
INSERT INTO public.namespace VALUES ('sbr-a', 'http://www.nltaxonomie.nl/2011/xbrl/xbrl-syntax-arcroles');
INSERT INTO public.namespace VALUES ('ezk-ncgc-i', 'http://www.nltaxonomie.nl/nt17/ezk/20221214/dictionary/ezk-ncgc-data');


--
-- TOC entry 3379 (class 0 OID 203349)
-- Dependencies: 206
-- Data for Name: concept; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.concept VALUES ('bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtSinglePersonHouseholds', 'AboveRetirementAgeIncomeHigherThanIncomeLimitWhtSinglePersonHouseholds', NULL, false, false, 'duration', 'bzk-ww-i', 'http://www.nltaxonomie.nl/nt17/bzk/20221214/dictionary/bzk-ww-data', 'xbrli:item', NULL, 'nonNegativeInteger20ItemType');
INSERT INTO public.concept_label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtSinglePersonHouseholds', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtSinglePersonHouseholds_documentation_nl', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtSinglePersonHouseholds_documentation_nl', 'nl', 'http://www.xbrl.org/2003/role/documentation', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.concept_label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtSinglePersonHouseholds', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtSinglePersonHouseholds_label_nl', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtSinglePersonHouseholds_label_nl', 'nl', 'http://www.xbrl.org/2003/role/label','bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.concept_label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtSinglePersonHouseholds', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtSinglePersonHouseholds_terseLabel_nl', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtSinglePersonHouseholds_terseLabel_nl', 'nl', 'http://www.xbrl.org/2003/role/terseLabel','bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.concept_label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtSinglePersonHouseholds', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtSinglePersonHouseholds_label_en', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtSinglePersonHouseholds_label_en', 'en', 'http://www.xbrl.org/2003/role/label','bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.concept_label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtSinglePersonHouseholds', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtSinglePersonHouseholds_terseLabel_en', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtSinglePersonHouseholds_terseLabel_en', 'en', 'http://www.xbrl.org/2003/role/terseLabel','bzk-rpt-ti-woningwet.xsd');

INSERT INTO public.concept VALUES ('bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTripleAndMultiplePerson', 'AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTripleAndMultiplePerson', NULL, false, false, 'duration', 'bzk-ww-i', 'http://www.nltaxonomie.nl/nt17/bzk/20221214/dictionary/bzk-ww-data', 'xbrli:item', NULL, 'nonNegativeInteger20ItemType');
INSERT INTO public.concept_label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTripleAndMultiplePerson', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTripleAndMultiplePerson_documentation_nl', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTripleAndMultiplePerson_documentation_nl', 'nl', 'http://www.xbrl.org/2003/role/documentation','bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.concept_label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTripleAndMultiplePerson', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTripleAndMultiplePerson_label_nl', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTripleAndMultiplePerson_label_nl', 'nl', 'http://www.xbrl.org/2003/role/label', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.concept_label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTripleAndMultiplePerson', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTripleAndMultiplePerson_terseLabel_nl', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTripleAndMultiplePerson_terseLabel_nl', 'nl', 'http://www.xbrl.org/2003/role/terseLabel', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.concept_label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTripleAndMultiplePerson', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTripleAndMultiplePerson_label_en', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTripleAndMultiplePerson_label_en', 'en', 'http://www.xbrl.org/2003/role/label', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.concept_label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTripleAndMultiplePerson', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTripleAndMultiplePerson_terseLabel_en', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTripleAndMultiplePerson_terseLabel_en', 'en', 'http://www.xbrl.org/2003/role/terseLabel', 'bzk-rpt-ti-woningwet.xsd');

INSERT INTO public.concept VALUES ('bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTwoPersonHouseholds', 'AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTwoPersonHouseholds', NULL, false, false, 'duration', 'bzk-ww-i', 'http://www.nltaxonomie.nl/nt17/bzk/20221214/dictionary/bzk-ww-data', 'xbrli:item', NULL, 'nonNegativeInteger20ItemType');
INSERT INTO public.concept_label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTwoPersonHouseholds', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTwoPersonHouseholds_documentation_nl', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTwoPersonHouseholds_documentation_nl', 'nl', 'http://www.xbrl.org/2003/role/documentation', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.concept_label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTwoPersonHouseholds', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTwoPersonHouseholds_label_nl', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTwoPersonHouseholds_label_nl', 'nl', 'http://www.xbrl.org/2003/role/label', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.concept_label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTwoPersonHouseholds', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTwoPersonHouseholds_terseLabel_nl', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTwoPersonHouseholds_terseLabel_nl', 'nl', 'http://www.xbrl.org/2003/role/terseLabel', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.concept_label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTwoPersonHouseholds', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTwoPersonHouseholds_label_en', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTwoPersonHouseholds_label_en', 'en', 'http://www.xbrl.org/2003/role/label', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.concept_label VALUES ('bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTwoPersonHouseholds', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTwoPersonHouseholds_terseLabel_en', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTwoPersonHouseholds_terseLabel_en', 'en', 'http://www.xbrl.org/2003/role/terseLabel', 'bzk-rpt-ti-woningwet.xsd');

INSERT INTO public.concept VALUES ('bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtSinglePersonHouseholds', 'AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtSinglePersonHouseholds', NULL, false, false, 'duration', 'bzk-ww-i', 'http://www.nltaxonomie.nl/nt17/bzk/20221214/dictionary/bzk-ww-data', 'xbrli:item', NULL, 'nonNegativeInteger20ItemType');
INSERT INTO public.concept VALUES ('bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtTripleAndMultiplePerson', 'AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtTripleAndMultiplePerson', NULL, false, false, 'duration', 'bzk-ww-i', 'http://www.nltaxonomie.nl/nt17/bzk/20221214/dictionary/bzk-ww-data', 'xbrli:item', NULL, 'nonNegativeInteger20ItemType');
INSERT INTO public.concept VALUES ('bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtTwoPersonHouseholds', 'AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtTwoPersonHouseholds', NULL, false, false, 'duration', 'bzk-ww-i', 'http://www.nltaxonomie.nl/nt17/bzk/20221214/dictionary/bzk-ww-data', 'xbrli:item', NULL, 'nonNegativeInteger20ItemType');
INSERT INTO public.concept VALUES ('bzk-ww-i_AccuracyAndCompletenessOfTheHomeAllocation', 'AccuracyAndCompletenessOfTheHomeAllocation', NULL, false, false, 'duration', 'bzk-ww-i', 'http://www.nltaxonomie.nl/nt17/bzk/20221214/dictionary/bzk-ww-data', 'xbrli:item', NULL, 'yesNoItemType');
INSERT INTO public.concept VALUES ('bzk-ww-i_ActivitiesAsReferredToTheHousingAct', 'ActivitiesAsReferredToTheHousingAct', NULL, false, false, 'duration', 'bzk-ww-i', 'http://www.nltaxonomie.nl/nt17/bzk/20221214/dictionary/bzk-ww-data', 'xbrli:item', NULL, 'yesNoItemType');
INSERT INTO public.concept VALUES ('bzk-ww-i_ActivitiesOverviewDetailCashInflows', 'ActivitiesOverviewDetailCashInflows', 'debit', false, false, 'duration', 'bzk-ww-i', 'http://www.nltaxonomie.nl/nt17/bzk/20221214/dictionary/bzk-ww-data', 'xbrli:item', NULL, 'monetaryNoDecimals20ItemType');
INSERT INTO public.concept VALUES ('bzk-ww-i_ActivitiesOverviewDetailCashOutflows', 'ActivitiesOverviewDetailCashOutflows', 'credit', false, false, 'duration', 'bzk-ww-i', 'http://www.nltaxonomie.nl/nt17/bzk/20221214/dictionary/bzk-ww-data', 'xbrli:item', NULL, 'monetaryNoDecimals20ItemType');
INSERT INTO public.concept VALUES ('bzk-ww-i_ActivitiesOverviewDetailDeliveryYear', 'ActivitiesOverviewDetailDeliveryYear', NULL, false, false, 'duration', 'bzk-ww-i', 'http://www.nltaxonomie.nl/nt17/bzk/20221214/dictionary/bzk-ww-data', 'xbrli:item', NULL, 'gYearRangeItemType');
INSERT INTO public.concept VALUES ('bzk-ww-i_ActivitiesOverviewDetailFoundationExpenses', 'ActivitiesOverviewDetailFoundationExpenses', 'debit', false, false, 'duration', 'bzk-ww-i', 'http://www.nltaxonomie.nl/nt17/bzk/20221214/dictionary/bzk-ww-data', 'xbrli:item', NULL, 'monetaryNoDecimals20ItemType');
INSERT INTO public.concept VALUES ('bzk-ww-i_ActivitiesOverviewDetailMarketValueMutation', 'ActivitiesOverviewDetailMarketValueMutation', 'debit', false, false, 'duration', 'bzk-ww-i', 'http://www.nltaxonomie.nl/nt17/bzk/20221214/dictionary/bzk-ww-data', 'xbrli:item', NULL, 'monetaryNoDecimals20ItemType');
INSERT INTO public.concept VALUES ('bzk-ww-i_ActivitiesOverviewDetailNumber', 'ActivitiesOverviewDetailNumber', NULL, false, false, 'duration', 'bzk-ww-i', 'http://www.nltaxonomie.nl/nt17/bzk/20221214/dictionary/bzk-ww-data', 'xbrli:item', NULL, 'nonNegativeInteger20ItemType');
INSERT INTO public.concept VALUES ('bzk-ww-i_ActivitiesOverviewDetailPolicyValueMutation', 'ActivitiesOverviewDetailPolicyValueMutation', 'debit', false, false, 'duration', 'bzk-ww-i', 'http://www.nltaxonomie.nl/nt17/bzk/20221214/dictionary/bzk-ww-data', 'xbrli:item', NULL, 'monetaryNoDecimals20ItemType');
INSERT INTO public.concept VALUES ('bzk-ww-i_ActivitiesWhichBelongToNonDaebActivities', 'ActivitiesWhichBelongToNonDaebActivities', NULL, false, false, 'duration', 'bzk-ww-i', 'http://www.nltaxonomie.nl/nt17/bzk/20221214/dictionary/bzk-ww-data', 'xbrli:item', NULL, 'yesNoItemType');
INSERT INTO public.concept VALUES ('bzk-ww-i_AdditionalRelevantPositionsMembersManagementDescription', 'AdditionalRelevantPositionsMembersManagementDescription', NULL, false, false, 'duration', 'bzk-ww-i', 'http://www.nltaxonomie.nl/nt17/bzk/20221214/dictionary/bzk-ww-data', 'xbrli:item', NULL, 'formattedExplanationItemType');
INSERT INTO public.concept VALUES ('bzk-ww-i_AdditionalRelevantPositionsMembersSupervisoryBodyDescription', 'AdditionalRelevantPositionsMembersSupervisoryBodyDescription', NULL, false, false, 'duration', 'bzk-ww-i', 'http://www.nltaxonomie.nl/nt17/bzk/20221214/dictionary/bzk-ww-data', 'xbrli:item', NULL, 'formattedExplanationItemType');
INSERT INTO public.concept VALUES ('bzk-ww-i_AdditionalRevenueFromHigherRentThanNorm', 'AdditionalRevenueFromHigherRentThanNorm', 'credit', false, false, 'instant', 'bzk-ww-i', 'http://www.nltaxonomie.nl/nt17/bzk/20221214/dictionary/bzk-ww-data', 'xbrli:item', NULL, 'monetaryNoDecimals20ItemType');
INSERT INTO public.concept VALUES ('bzk-ww-i_AffordabilityExitRental', 'AffordabilityExitRental', 'credit', false, false, 'instant', 'bzk-ww-i', 'http://www.nltaxonomie.nl/nt17/bzk/20221214/dictionary/bzk-ww-data', 'xbrli:item', NULL, 'monetaryNoDecimals20ItemType');
INSERT INTO public.concept VALUES ('bzk-ww-i_AgreementsConcludedWithThirdParties', 'AgreementsConcludedWithThirdParties', NULL, false, false, 'duration', 'bzk-ww-i', 'http://www.nltaxonomie.nl/nt17/bzk/20221214/dictionary/bzk-ww-data', 'xbrli:item', NULL, 'yesNoItemType');
INSERT INTO public.concept VALUES ('bzk-ww-i_Allocations', 'Allocations', 'debit', false, false, 'duration', 'bzk-ww-i', 'http://www.nltaxonomie.nl/nt17/bzk/20221214/dictionary/bzk-ww-data', 'xbrli:item', NULL, 'monetaryNoDecimals20ItemType');
INSERT INTO public.concept VALUES ('bzk-ww-i_AmountUndrawnPortionBondLoans', 'AmountUndrawnPortionBondLoans', 'credit', false, false, 'instant', 'bzk-ww-i', 'http://www.nltaxonomie.nl/nt17/bzk/20221214/dictionary/bzk-ww-data', 'xbrli:item', NULL, 'nonNegativeMonetaryNoDecimals20ItemType');
INSERT INTO public.concept VALUES ('bzk-ww-i_AnnualRent', 'AnnualRent', 'debit', false, false, 'duration', 'bzk-ww-i', 'http://www.nltaxonomie.nl/nt17/bzk/20221214/dictionary/bzk-ww-data', 'xbrli:item', NULL, 'monetaryNoDecimals20ItemType');
INSERT INTO public.concept VALUES ('bzk-ww-i_RelevantApprovedInstitutionAssociatedCompaniesDescription', 'RelevantApprovedInstitutionAssociatedCompaniesDescription', NULL, false, false, 'duration', 'bzk-ww-i', 'http://www.nltaxonomie.nl/nt17/bzk/20221214/dictionary/bzk-ww-data', 'xbrli:item', NULL, 'formattedExplanationItemType');
INSERT INTO public.concept VALUES ('rj-i_SupervisoryBodyReport', 'SupervisoryBodyReport', NULL, false, false, 'duration', 'rj-i', 'http://www.nltaxonomie.nl/nt17/rj/20221214/dictionary/rj-data', 'xbrli:item', NULL, 'formattedExplanationItemType');
INSERT INTO public.concept VALUES ('bzk-ww-abstr_Article36HousingActTitle', 'Article36HousingActTitle', NULL, false, true, 'duration', 'bzk-ww-abstr', 'http://www.nltaxonomie.nl/nt17/bzk/20221214/presentation/bzk-ww-abstracts', 'sbr:presentationItem', NULL, 'stringItemType');


--
-- TOC entry 3381 (class 0 OID 203386)
-- Dependencies: 208
-- Data for Name: concept_label; Type: TABLE DATA; Schema: public; Owner: postgres
--


--
-- TOC entry 3382 (class 0 OID 203406)
-- Dependencies: 209
-- Data for Name: reference; Type: TABLE DATA; Schema: public; Owner: postgres
--
INSERT INTO public.reference VALUES ('bzk-ww-i_RTIV2015_2018-01-01_15_1_ref', 'bzk-ww-i_RTIV2015_2018-01-01_15_1_ref', 'Regeling toegelaten instellingen volkshuisvesting 2015', NULL, '2018-01-01', NULL, '15', NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.concept_reference VALUES ('bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtSinglePersonHouseholds', 'bzk-ww-i_RTIV2015_2018-01-01_15_1_ref');
INSERT INTO public.concept_reference VALUES ('bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTripleAndMultiplePerson', 'bzk-ww-i_RTIV2015_2018-01-01_15_1_ref');
INSERT INTO public.concept_reference VALUES ('bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTwoPersonHouseholds', 'bzk-ww-i_RTIV2015_2018-01-01_15_1_ref');
INSERT INTO public.concept_reference VALUES ('bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtSinglePersonHouseholds', 'bzk-ww-i_RTIV2015_2018-01-01_15_1_ref');
INSERT INTO public.concept_reference VALUES ('bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtTripleAndMultiplePerson', 'bzk-ww-i_RTIV2015_2018-01-01_15_1_ref');
INSERT INTO public.concept_reference VALUES ('bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtTwoPersonHouseholds', 'bzk-ww-i_RTIV2015_2018-01-01_15_1_ref');


--
-- TOC entry 3383 (class 0 OID 203414)
-- Dependencies: 210
-- Data for Name: concept_reference; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3407 (class 0 OID 203727)
-- Dependencies: 234
-- Data for Name: concept_relationshipnode; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3389 (class 0 OID 203494)
-- Dependencies: 216
-- Data for Name: hypercube; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.hypercube VALUES (2, 'urn:bzk-ti:linkrole:article36housing-act', 'all', 'scenario', NULL);
INSERT INTO public.hypercube VALUES (7, 'urn:bzk:linkrole:article35housing-act', 'all', 'scenario', NULL);


--
-- TOC entry 3391 (class 0 OID 203504)
-- Dependencies: 218
-- Data for Name: dimension; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3409 (class 0 OID 203743)
-- Dependencies: 236
-- Data for Name: dimension_relationshipnode; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3393 (class 0 OID 203532)
-- Dependencies: 220
-- Data for Name: domain; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3380 (class 0 OID 203367)
-- Dependencies: 207
-- Data for Name: dts_concept; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.dts_concept VALUES ('bzk-rpt-ti-woningwet.xsd', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtSinglePersonHouseholds', false, false, false, false);
INSERT INTO public.dts_concept VALUES ('bzk-rpt-ti-woningwet.xsd', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTripleAndMultiplePerson', false, false, false, false);
INSERT INTO public.dts_concept VALUES ('bzk-rpt-ti-woningwet.xsd', 'bzk-ww-i_AboveRetirementAgeIncomeHigherThanIncomeLimitWhtTwoPersonHouseholds', false, false, false, false);
INSERT INTO public.dts_concept VALUES ('bzk-rpt-ti-woningwet.xsd', 'bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtSinglePersonHouseholds', false, false, false, false);
INSERT INTO public.dts_concept VALUES ('bzk-rpt-ti-woningwet.xsd', 'bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtTripleAndMultiplePerson', false, false, false, false);
INSERT INTO public.dts_concept VALUES ('bzk-rpt-ti-woningwet.xsd', 'bzk-ww-i_AboveRetirementAgeIncomeLowerOrEqualToIncomeLimitWhtTwoPersonHouseholds', false, false, false, false);
INSERT INTO public.dts_concept VALUES ('bzk-rpt-ti-woningwet.xsd', 'bzk-ww-i_AccuracyAndCompletenessOfTheHomeAllocation', false, false, false, false);
INSERT INTO public.dts_concept VALUES ('bzk-rpt-ti-woningwet.xsd', 'bzk-ww-i_ActivitiesAsReferredToTheHousingAct', false, false, false, false);
INSERT INTO public.dts_concept VALUES ('bzk-rpt-ti-woningwet.xsd', 'bzk-ww-i_ActivitiesOverviewDetailCashInflows', false, false, false, false);
INSERT INTO public.dts_concept VALUES ('bzk-rpt-ti-woningwet.xsd', 'bzk-ww-i_ActivitiesOverviewDetailCashOutflows', false, false, false, false);
INSERT INTO public.dts_concept VALUES ('bzk-rpt-ti-woningwet.xsd', 'bzk-ww-i_ActivitiesOverviewDetailDeliveryYear', false, false, false, false);
INSERT INTO public.dts_concept VALUES ('bzk-rpt-ti-woningwet.xsd', 'bzk-ww-i_ActivitiesOverviewDetailFoundationExpenses', false, false, false, false);
INSERT INTO public.dts_concept VALUES ('bzk-rpt-ti-woningwet.xsd', 'bzk-ww-i_ActivitiesOverviewDetailMarketValueMutation', false, false, false, false);
INSERT INTO public.dts_concept VALUES ('bzk-rpt-ti-woningwet.xsd', 'bzk-ww-i_ActivitiesOverviewDetailNumber', false, false, false, false);
INSERT INTO public.dts_concept VALUES ('bzk-rpt-ti-woningwet.xsd', 'bzk-ww-i_ActivitiesOverviewDetailPolicyValueMutation', false, false, false, false);
INSERT INTO public.dts_concept VALUES ('bzk-rpt-ti-woningwet.xsd', 'bzk-ww-i_ActivitiesWhichBelongToNonDaebActivities', false, false, false, false);
INSERT INTO public.dts_concept VALUES ('bzk-rpt-ti-woningwet.xsd', 'bzk-ww-i_AdditionalRevenueFromHigherRentThanNorm', false, false, false, false);
INSERT INTO public.dts_concept VALUES ('bzk-rpt-ti-woningwet.xsd', 'bzk-ww-i_AffordabilityExitRental', false, false, false, false);
INSERT INTO public.dts_concept VALUES ('bzk-rpt-ti-woningwet.xsd', 'bzk-ww-i_AgreementsConcludedWithThirdParties', false, false, false, false);
INSERT INTO public.dts_concept VALUES ('bzk-rpt-ti-woningwet.xsd', 'bzk-ww-i_Allocations', false, false, false, false);
INSERT INTO public.dts_concept VALUES ('bzk-rpt-ti-woningwet.xsd', 'bzk-ww-i_AmountUndrawnPortionBondLoans', false, false, false, false);
INSERT INTO public.dts_concept VALUES ('bzk-rpt-ti-woningwet.xsd', 'bzk-ww-i_AnnualRent', false, false, false, false);


--
-- TOC entry 3378 (class 0 OID 203331)
-- Dependencies: 205
-- Data for Name: dts_itemtype; Type: TABLE DATA; Schema: public; Owner: postgres
--

--
-- TOC entry 3373 (class 0 OID 203262)
-- Dependencies: 200
-- Data for Name: dts_label; Type: TABLE DATA; Schema: public; Owner: postgres
--
INSERT INTO public.label VALUES ('bzk-ww-lr_Article35HousingAct_label_nl', 'bzk-ww-lr_Article35HousingAct_label_nl', 'http://www.xbrl.org/2008/role/label', 'nl', 'Artikel 35');
INSERT INTO public.label VALUES ('bzk-ww-lr_Article35HousingAct_label_en', 'bzk-ww-lr_Article35HousingAct_label_en', 'http://www.xbrl.org/2008/role/label', 'en', 'Article 35');

INSERT INTO public.dts_label VALUES ('bzk-ww-lr_Article35HousingAct_label_nl', 'bzk-ww-lr_Article35HousingAct_label_nl', 'nl', 'http://www.xbrl.org/2008/role/label', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_label VALUES ('bzk-ww-lr_Article35HousingAct_label_en', 'bzk-ww-lr_Article35HousingAct_label_en', 'en', 'http://www.xbrl.org/2008/role/label', 'bzk-rpt-ti-woningwet.xsd');

--
-- TOC entry 3371 (class 0 OID 203229)
-- Dependencies: 198
-- Data for Name: dts_namespace; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.dts_namespace VALUES ('bzk-rpt-ww', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('link', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('xlink', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('xs', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('xl', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('bzk-ww-lr', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('xbrli', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('xbrldt', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('bzk-ww-codes', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('bzk-ww-i', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('bzk-ww-types', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('nl-codes', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('nl-types', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('num', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('dtr-types', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('dtr', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('sbr', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('sbr-dim', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('jenv-codes', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('rj-codes', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('rj-i', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('label', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('sbr-l', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('jenv-bw2-dm', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('jenv-bw2-i', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('bzk-ww-abstr', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('gen', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('xsd', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('xsi', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('negated', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('ref', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('sbr-arc', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('bzk-ww-dm', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('bzk-ww-sim-codes', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('rj-dm', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('reference', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('sbr-a', 'bzk-rpt-ti-woningwet.xsd');
INSERT INTO public.dts_namespace VALUES ('ezk-ncgc-i', 'bzk-rpt-ti-woningwet.xsd');


--
-- TOC entry 3375 (class 0 OID 203288)
-- Dependencies: 202
-- Data for Name: enumeration_option; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.enumeration_option VALUES (NULL, 'optional');
INSERT INTO public.enumeration_option VALUES (NULL, 'prohibited');
INSERT INTO public.enumeration_option VALUES (NULL, 'segment');
INSERT INTO public.enumeration_option VALUES (NULL, 'scenario');


--
-- TOC entry 3376 (class 0 OID 203295)
-- Dependencies: 203
-- Data for Name: enumeration_option_label; Type: TABLE DATA; Schema: public; Owner: postgres
--
INSERT INTO public.enumeration_option VALUES ('bzk_ww-codes_BranchType_Daeb', 'DAEB');


INSERT INTO public.enumeration_option_label VALUES ('bzk_ww-codes_BranchType_Daeb', NULL, 'bzk_ww-codes_BranchType_Daeb_label_nl', 'nl');
INSERT INTO public.enumeration_option_label VALUES ('bzk_ww-codes_BranchType_Daeb', NULL, 'bzk_ww-codes_BranchType_Daeb_label_en', 'en');
--
-- TOC entry 3417 (class 0 OID 203793)
-- Dependencies: 244
-- Data for Name: formula_typed_dimension; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3377 (class 0 OID 203313)
-- Dependencies: 204
-- Data for Name: itemtype_enumeration_option; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.itemtype_enumeration_option VALUES ('branchItemType', 'bzk_ww-codes_BranchType_Daeb');
--
-- TOC entry 3396 (class 0 OID 203568)
-- Dependencies: 223
-- Data for Name: line_item; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.line_item VALUES (3, 2, 'bzk-ww-i_RelevantApprovedInstitutionAssociatedCompaniesDescription');
INSERT INTO public.line_item VALUES (4, 2, 'rj-i_SupervisoryBodyReport');



--
-- TOC entry 3387 (class 0 OID 203458)
-- Dependencies: 214
-- Data for Name: linkrole_label; Type: TABLE DATA; Schema: public; Owner: postgres
--
INSERT INTO public.label VALUES ('bzk-ww-lr_Article36HousingAct_label_nl', 'bzk-ww-lr_Article36HousingAct_label_nl', 'http://www.xbrl.org/2008/role/label', 'nl', 'Artikel 36');
INSERT INTO public.label VALUES ('bzk-ww-lr_Article36HousingAct_label_en', 'bzk-ww-lr_Article36HousingAct_label_en', 'http://www.xbrl.org/2008/role/label', 'en', 'Article 36');

INSERT INTO public.linkrole_label VALUES ('bzk-ww-lr_Article36HousingAct_label_nl', 'bzk-ww-lr_Article36HousingAct_label_nl', 'nl', 'http://www.xbrl.org/2008/role/label', 2);
INSERT INTO public.linkrole_label VALUES ('bzk-ww-lr_Article36HousingAct_label_en', 'bzk-ww-lr_Article36HousingAct_label_en', 'en', 'http://www.xbrl.org/2008/role/label', 2);

--
-- TOC entry 3397 (class 0 OID 203581)
-- Dependencies: 224
-- Data for Name: linkrole_reference; Type: TABLE DATA; Schema: public; Owner: postgres
--




--
-- TOC entry 3384 (class 0 OID 203432)
-- Dependencies: 211
-- Data for Name: used_on; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.used_on VALUES ('link:definitionLink');
INSERT INTO public.used_on VALUES ('link:presentationLink');
INSERT INTO public.used_on VALUES ('sbr:linkroleOrder');
INSERT INTO public.used_on VALUES ('label:label');
INSERT INTO public.used_on VALUES ('link:label');
INSERT INTO public.used_on VALUES ('gen:link');


--
-- TOC entry 3388 (class 0 OID 203476)
-- Dependencies: 215
-- Data for Name: linkrole_used_on; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.linkrole_used_on VALUES (2, 'link:definitionLink');
INSERT INTO public.linkrole_used_on VALUES (2, 'link:presentationLink');
INSERT INTO public.linkrole_used_on VALUES (7, 'link:definitionLink');
INSERT INTO public.linkrole_used_on VALUES (7, 'link:presentationLink');
INSERT INTO public.linkrole_used_on VALUES (16, 'sbr:linkroleOrder');


--
-- TOC entry 3418 (class 0 OID 203806)
-- Dependencies: 245
-- Data for Name: variable; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3419 (class 0 OID 203814)
-- Dependencies: 246
-- Data for Name: linkrole_variable; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3395 (class 0 OID 203553)
-- Dependencies: 222
-- Data for Name: member; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3433 (class 0 OID 203991)
-- Dependencies: 260
-- Data for Name: mutation; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3420 (class 0 OID 203832)
-- Dependencies: 247
-- Data for Name: parameter; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3423 (class 0 OID 203859)
-- Dependencies: 250
-- Data for Name: presentation_hierarchy_node; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.presentation_hierarchy_node VALUES (1, 'Article36HousingActTitle', 'concept', 'bzk-ww-abstr_Article36HousingActTitle', NULL, true, 1, NULL, NULL, NULL, 2, false);
INSERT INTO public.presentation_hierarchy_node VALUES (2, 'AdditionalRelevantPositionsMembersManagementDescription', 'concept', 'bzk-ww-i_AdditionalRelevantPositionsMembersManagementDescription', NULL, false, 1, NULL, 1, NULL, NULL, false);


--
-- TOC entry 3404 (class 0 OID 203687)
-- Dependencies: 231
-- Data for Name: table_breakdown; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3429 (class 0 OID 203947)
-- Dependencies: 256
-- Data for Name: table_filter; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3421 (class 0 OID 203839)
-- Dependencies: 248
-- Data for Name: table_parameter; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3406 (class 0 OID 203708)
-- Dependencies: 233
-- Data for Name: table_rulenode_label; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3425 (class 0 OID 203886)
-- Dependencies: 252
-- Data for Name: variable_filter; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3427 (class 0 OID 203911)
-- Dependencies: 254
-- Data for Name: variableset_variable; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3403 (class 0 OID 203668)
-- Dependencies: 230
-- Data for Name: xbrl_table_label; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3439 (class 0 OID 0)
-- Dependencies: 257
-- Name: arc_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.arc_rowid_seq', 3544, true);


--
-- TOC entry 3440 (class 0 OID 0)
-- Dependencies: 217
-- Name: dimension_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.dimension_rowid_seq', 1, false);


--
-- TOC entry 3441 (class 0 OID 0)
-- Dependencies: 219
-- Name: domain_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.domain_rowid_seq', 1, false);


--
-- TOC entry 3442 (class 0 OID 0)
-- Dependencies: 241
-- Name: formula_explicit_dimension_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.formula_explicit_dimension_rowid_seq', 1, false);


--
-- TOC entry 3443 (class 0 OID 0)
-- Dependencies: 239
-- Name: formula_period_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.formula_period_rowid_seq', 1, false);


--
-- TOC entry 3444 (class 0 OID 0)
-- Dependencies: 243
-- Name: formula_typed_dimension_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.formula_typed_dimension_rowid_seq', 1, false);


--
-- TOC entry 3445 (class 0 OID 0)
-- Dependencies: 212
-- Name: linkrole_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.linkrole_rowid_seq', 49, true);


--
-- TOC entry 3446 (class 0 OID 0)
-- Dependencies: 221
-- Name: member_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.member_rowid_seq', 1, false);


--
-- TOC entry 3447 (class 0 OID 0)
-- Dependencies: 259
-- Name: mutation_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.mutation_rowid_seq', 1, false);


--
-- TOC entry 3448 (class 0 OID 0)
-- Dependencies: 249
-- Name: presentation_hierarchy_node_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.presentation_hierarchy_node_rowid_seq', 17, true);


--
-- TOC entry 3449 (class 0 OID 0)
-- Dependencies: 237
-- Name: ruleset_rowid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ruleset_rowid_seq', 1, false);


-- Completed on 2023-04-13 12:42:48

--
-- PostgreSQL database dump complete
--

