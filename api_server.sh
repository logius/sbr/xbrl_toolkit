#!/bin/bash
# Find out where this script resides on disk,
# that is the project root,
# add that to the PYTHON_PATH
PROJECT_ROOT=$(dirname "$0")
export PYTHONPATH=$PROJECT_ROOT:/home/xiffy/develop/kenniscentrumxbrl/discover_xbrl:$PYTHONPATH
export PYTHON_UNBUFFERED=True
echo $PYTHONPATH
#cd ${PROJECT_ROOT}/discover_xbrl/api
uvicorn discover_xbrl.api.api_server:api_server --reload-dir ./discover_xbrl --reload --host 127.0.0.1 --root-path /api

