#!/bin/bash
# Find out where this script resides on disk,
# that is the project root,
# add that to the PYTHON_PATH
PROJECT_ROOT=$(dirname "$0")
export PYTHONPATH=$PROJECT_ROOT:$PYTHON_PATH
export PYTHON_UNBUFFERED=True
echo $PYTHON_PATH
python ${PROJECT_ROOT}/discover_xbrl/sbrnl_xbrl_toolbox/xbrl_cli.py "$@"

