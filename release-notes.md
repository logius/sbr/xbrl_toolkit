2023-06-21

Hypercube rendering.
- van de definitionlink combineren we de line-items (y-axis) en de eventuele dimensies. De dimensies worden behandeld als in een dimensional-relationship node uit een table-linkbase. Eventuele vervolg dimensies worden 'geprojecteerd' op eerdere dimensies 

Members kunnen een preferred label hebben.
- In de presentation linkbase onderkennen we preferred labels en  gebruiken die in de rendering van een tabel. Members in een dimensie kunnen ook een preferred label hebben, en die worden nu ook gebruikt bij het rendren van een tabel, met de aanduiding om welk type preferred label het gaat.

Line-items kunnen een hierarchy zijn. 
- Op de definition-link zijn deze nu zichtbaar. (ESEF/IFRS)

Dimensies kunnen bestaan zoner dat er een hpercube in de linkrole is gedefinieerd. 
- Deze slaan we nu rechtstreeks aan de linkrole gekoppeld op. (ESEF/IFRS)

Version Compare
- Houdt rekening met de aanpassingen in de manier waarop we de taxonomie parsen en opslaan. 

Onder de motorkap
- Betere afhandeling van Locators die niet per se uniek zijn binnen een taxonomie. De parser ging hier wel van uit.
- Koppeling van linkrole naar dimensie zonder een hyercube er tussen. 
- Labels kunnen of op xlink-label, of op id uniek zijn binnen een dts. Labels hebben nu een unieke index op de combinatie van deze velden.
- 
