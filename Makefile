all: toolkit

toolkit:
	# cd into running directory, make sure we are on the master, and pull from gitlab
	# docker-less, after the pull, the service will reload itsel
	cd /app/kenniscentrumxbrl
	git checkout master
	git pull
	@cp /var/log/toolkit/api_server.log /var/log/toolkit/api_server.log.1
	# restart the AP-Server
	. venv/bin/activate && ./api_server_prod.sh
	@echo waiting for the server to restart
	@sleep 5
	@cat /var/log/toolkit/api_server.log
	@echo "--"
	@echo "The server should be running now. Testing ..."
	@curl http://localhost:8000/nt-versions
	@echo ""
	@echo "You should see a couple of NT versions"

restart:
	cd /app/kenniscentrumxbrl
	@cp /var/log/toolkit/api_server.log /var/log/toolkit/api_server.log.1
	. venv/bin/activate && ./api_server_prod.sh
	@echo waiting for the server to restart
	@sleep 5
	@tail /var/log/toolkit/api_server.log

rebuild_cache:
	sudo rm -rf /var/cache/nginx/*
	cd /app/kenniscentrumxbrl
	. venv/bin/activate && nohup ./pre_fill_cache.sh > /tmp/cache_rebuild.out &

sitemap:
	cd /app/kenniscentrumxbrl
	. venv/bin/activate && export PYTHONPATH=.:$$PYTHONPATH && python discover_xbrl/api/sitemap.py

.PHONY: list
list:
	@LC_ALL=C $(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | sort | egrep -v -e '^[^[:alnum:]]' -e '^$@$$'
