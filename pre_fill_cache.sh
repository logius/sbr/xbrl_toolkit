#!/bin/bash
# Find out where this script resides on disk,
# that is the project root,
# add that to the PYTHON_PATH
PROJECT_ROOT=$(dirname "$0")
export PYTHONPATH=$PROJECT_ROOT:$PYTHONPATH
export PYTHON_UNBUFFERED=True
echo Cacherunner: $PYTHONPATH
python discover_xbrl/api/pre_fill_cache.py

