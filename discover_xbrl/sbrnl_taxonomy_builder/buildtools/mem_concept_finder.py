import logging

from lxml import etree
import fs

from .mem_fs import mem_fs
from .mem_file_reader import get_relative_path

logger = logging.getLogger("builder.buildtools")
from discover_xbrl.sbrnl_taxonomy_builder.config.conf import Config

conf = Config()


def file_exists(path):
    if mem_fs.exists(path):
        return True
    return False


def find_namespace(namespace, rel_path):
    """
    Attempts to find a local representation of the given namespace by matching it to its target namespace

    :param namespace:
    :param relative_path:
    :return:
    """
    for (
        subdir,
        dirs,
        files,
    ) in mem_fs.walk():  # Only walk through the memFS dirs of the new taxonomy
        for filename in files:
            if filename.suffix == ".xsd":
                filepath = subdir + "/" + filename.name
                parser = etree.XMLParser(remove_blank_text=True, encoding="utf-8")
                tree = etree.parse(mem_fs.open(filepath), parser)
                root = tree.getroot()
                schema = tree.xpath(
                    f"//xs:schema[@targetNamespace = '{namespace}']",
                    namespaces=root.nsmap,
                )

                if schema:
                    rel_path_new = fs.path.relativefrom(rel_path, subdir)

                    return rel_path_new

    logger.warning(f"Could not find namespace locally: '{namespace}'")
    return None


def find_schemalocation(namespace, rel_path):
    """
    Attempts to find a local representation of the given namespace by matching it to its target namespace

    :param namespace:
    :param relative_path:
    :return:
    """
    for (
        subdir,
        dirs,
        files,
    ) in mem_fs.walk():  # Only walk through the memFS dirs of the new taxonomy
        for filename in files:
            if filename.suffix == ".xsd":
                filepath = subdir + "/" + filename.name
                parser = etree.XMLParser(remove_blank_text=True, encoding="utf-8")
                tree = etree.parse(mem_fs.open(filepath), parser)
                root = tree.getroot()
                schema = tree.xpath(
                    f"//xs:schema[@targetNamespace = '{namespace}']",
                    namespaces=root.nsmap,
                )
                if schema:
                    rel_path_new = fs.path.relativefrom(rel_path, filepath)

                    if not rel_path_new.startswith("."):
                        rel_path_new = "./" + rel_path_new

                    # rel_path = mem_fs.dir
                    return rel_path_new

    logger.warning(f"Could not find schemalocation locally: '{namespace}'")
    return None


def find_itemtype(itemType_name, path=None):
    """
    Find the given itemType and returns it. If path is given, do not loop through files.

    :param itemtype_name:
    :param path:
    :return:
    """
    if path:
        parser = etree.XMLParser(remove_blank_text=True, encoding="utf-8")
        tree = etree.parse(mem_fs.open(path), parser)

        element = tree.xpath(
            f"//xs:complexType[@name = '{itemType_name}']",
            namespaces={"xs": "http://www.w3.org/2001/XMLSchema"},
        )
        if element:
            return element

    else:
        for (subdir, dirs, files,) in mem_fs.walk(
            filter=["*.xsd"]
        ):  # Only walk through the memFS dirs of the new taxonomy
            for filename in files:
                filepath = subdir + "/" + filename.name
                parser = etree.XMLParser(remove_blank_text=True, encoding="utf-8")
                tree = etree.parse(mem_fs.open(filepath), parser)

                element = tree.xpath(
                    f"//xs:complexType[@name = '{itemType_name}']",
                    namespaces={"xs": "http://www.w3.org/2001/XMLSchema"},
                )
                if element:
                    return element
    return None


def find_element_by_id(concept_id, reference_filepath, xpath=None, filenames=None):
    """
    Searches in-memory filesystem in order to find (external) concepts.
    :param conceptname:
    :param prefix:
    :return:
    """
    if not filenames:
        filenames = ["*.*"]
    for subdir, dirs, files in mem_fs.walk(
        filter=filenames
    ):  # Only walk through the memFS dirs of the new taxonomy
        for filename in files:
            filepath = subdir + "/" + filename.name
            parser = etree.XMLParser(remove_blank_text=True, encoding="utf-8")
            tree = etree.parse(mem_fs.open(filepath), parser)
            root = tree.getroot()

            if not xpath:
                element = tree.xpath(f"//*[@id='{concept_id}']")
            else:
                element = root.xpath(
                    f"//{xpath}[@id='{concept_id}']", namespaces=conf.nsmap
                )

            if element:
                return get_relative_path(reference_filepath, filepath)

    return None


def find_element_by_name(elem_name, prefix, reference_filepath):
    """
    Searches in-memory filesystem in order to find (external) itemTypes.
    :param conceptname:
    :param prefix:
    :return:
    """
    for (
        subdir,
        dirs,
        files,
    ) in mem_fs.walk():  # Only walk through the memFS dirs of the new taxonomy
        for filename in files:
            filepath = subdir + "/" + filename.name
            parser = etree.XMLParser(remove_blank_text=True, encoding="utf-8")
            tree = etree.parse(mem_fs.open(filepath), parser)

            element = tree.xpath(f"//*[@name = '{elem_name}']")

            if element:
                # Found element with same name
                if prefix in element[0].nsmap.keys():
                    # Prefix of concept is also used in nsmap of element.
                    schema = tree.xpath("//xs:schema", namespaces=tree.getroot().nsmap)
                    if schema:
                        namespace = schema[0].get("targetNamespace")
                    else:
                        print("")  # Schema does not have a target namespace!

                    rel_path = fs.path.relativefrom(reference_filepath, filepath)
                    return {"namespace": namespace, "schemaLocation": rel_path}
                else:
                    logger.warning(
                        f"Element found by ID, but namespace differs"
                    )  # While the ID is the same, element does not have the same namespace and is probably not
                    # the one we are looking for.
    return None


def find_id_by_roleURI(
    roleuri, reference_filepath, expected_file=None, expected_filter=None
):
    """
    Searches in-memory filesystem for the @id the corespondents with the given roleURI
    :param roleuri: The roleURI to look for
    :param reference_filepath: The filepath which the roleURI should reference to
    :pram expected_file: An optional string. This file is checked first if given
    :param expected_filter: An optional string, this filter is checked first if given.
    :return:
    """
    if not expected_filter:
        # Set expected filter if not explicitly given. (can't do that in declaration because list is a mutable object)
        expected_filter = ["*.xsd"]

    parser = etree.XMLParser(remove_blank_text=True, encoding="utf-8")

    # first try the file where the URI is expected to be found
    if expected_file:

        # And if the existed file exists
        if mem_fs.exists(expected_file):
            tree = etree.parse(mem_fs.open(expected_file), parser)
            element = tree.xpath(f"//*[@roleURI='{roleuri}']")

            if element:
                rel_path = fs.path.relativefrom(reference_filepath, expected_file)
                return {"id": element[0].get("id"), "rel_path": rel_path}
        else:
            logger.warning(
                f"Expected file '{expected_file}' does not exist when looking for roleURI"
            )

    # If the expected file is not given, or the element is not found, go though all schemas

    for subdir, dirs, files in mem_fs.walk(
        filter=expected_filter
    ):  # Only walk through the memFS dirs of the new taxonomy
        for filename in files:
            filepath = subdir + "/" + filename.name

            tree = etree.parse(mem_fs.open(filepath), parser)

            element = tree.xpath(
                f"//link:roleType[@roleURI='{roleuri}']", namespaces=conf.nsmap
            )

            if element:
                # Found element with same name
                rel_path = fs.path.relativefrom(reference_filepath, filepath)
                return {"id": element[0].get("id"), "rel_path": rel_path}

    return None
