import fs

from .mem_fs import mem_fs
from lxml import etree


def file_exists(file):
    if mem_fs.exists(file):
        return True
    return False


def get_relative_path(base, relative):
    base_without_file = str(base).rsplit("/", 1)[0]
    return fs.path.relativefrom(base_without_file, relative)


def get_uri_list(uri_list=None):
    """
    Loops though imported files and builds up a list of uri

    :param: qname_list: An optional list of qnames to be appended to

    :return:
    """

    if uri_list is None:
        uri_list = []

    for subdir, dirs, files in mem_fs.walk(filter=["*.xsd"]):
        for filename in files:
            filepath = subdir + "/" + filename.name
            parser = etree.XMLParser(remove_blank_text=True, encoding="utf-8")
            tree = etree.parse(mem_fs.open(filepath), parser)
            root = tree.getroot()

            for element in root.xpath():
                pass


def get_imported_namespaces():
    """
    Loops though imported files and builds up a list of namespaces

    :param: qname_list: An optional list of qnames to be appended to

    :return:
    """
    imported_namespaces = {}

    for subdir, dirs, files in mem_fs.walk(filter=["*.xsd"]):
        for filename in files:
            filepath = subdir + "/" + filename.name
            parser = etree.XMLParser(remove_blank_text=True, encoding="utf-8")
            tree = etree.parse(mem_fs.open(filepath), parser)
            root = tree.getroot()

            for schema in root.xpath(
                "//xs:schema", namespaces={"xs": "http://www.w3.org/2001/XMLSchema"}
            ):
                targetNamespace = schema.get("targetNamespace")

                for prefix, namespace in schema.nsmap.items():
                    if namespace == targetNamespace:
                        imported_namespaces[prefix] = {
                            "namespace": targetNamespace,
                            "localpath": filepath,
                        }

    return imported_namespaces
