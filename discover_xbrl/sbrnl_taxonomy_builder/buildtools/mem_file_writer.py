import datetime
import os
import re
import copy

import fs
from fs.osfs import OSFS
from lxml import etree

from .classes import XML_Namespace, XML_Parent_element
from .mem_file_reader import get_relative_path

from ..linkbase.create_linkbase_stub import create_linkbase_stub
from ..linkbase.file_router import xml_linkbase_router
from ..schema.create_schema_stub import create_schema_stub
from ..schema.file_router import xml_schema_router
from .mem_concept_finder import find_namespace, find_schemalocation

from .mem_fs import mem_fs

import logging

logger = logging.getLogger("builder.buildtools.filewriter")

from discover_xbrl.sbrnl_taxonomy_builder.config.conf import Config

conf = Config()


def import_taxonomies(absolute_path, domainlist, www_domain, nt_version):
    importable_paths = copy.deepcopy(domainlist)
    for prefix, filepaths in domainlist.items():
        for filepath in filepaths:
            # Check if the directory exists and if not, create it.
            if os.path.exists(path=f"{absolute_path}/{prefix}/{filepath}"):

                with OSFS(
                    f"{absolute_path}/{prefix}/{filepath}",
                    "r",
                ) as os_fs:
                    fs.copy.copy_dir(
                        src_fs=os_fs,
                        src_path=".",
                        dst_fs=mem_fs,
                        dst_path=f"{www_domain}/{nt_version}/{prefix}/{filepath}",
                    )
                # Remove filepath from prefix if it is found
                i = importable_paths[prefix].index(filepath)
                importable_paths[prefix].pop(i)

            else:
                raise FileNotFoundError(
                    f"Importable dir does not exist: '{absolute_path}/{prefix}/{filepath}'"
                )

        if len(importable_paths[prefix]) == 0:
            importable_paths.pop(prefix)

    if len(importable_paths) > 0:
        raise FileNotFoundError(f"Didn't find all directories that had to be imported")


def create_directory(directory):
    mem_fs.makedirs(f"{directory}")


def create_directory_structure(relative_root):
    """
    Creates directory structure in-memory filesystem

    :param abs_location:
    :return:
    """

    logger.info(
        f"Creating taxonomy structure for domain {conf.generic_info.get('domain')}, version: {conf.generic_info.get('date_version')}"
    )

    # www_domain = configuration['generic_info']['www_domain']
    # nt_version = configuration['generic_info']['nt_version']
    # domain = configuration['generic_info']['domain']
    # date_version = configuration['generic_info']['date_version']
    #
    # relative_root = f"{www_domain}/{nt_version}/{domain}/{date_version}"

    if not mem_fs.exists(relative_root):
        mem_fs.makedirs(relative_root)

        if not mem_fs.exists(f"{relative_root}/entrypoints"):
            mem_fs.makedir(f"{relative_root}/entrypoints")
            mem_fs.makedir(f"{relative_root}/dictionary")
            mem_fs.makedir(f"{relative_root}/presentation")
            mem_fs.makedir(f"{relative_root}/validation")


def enrich_routable_file(routable_file):

    # Add information to the routable file depending on type
    if routable_file.xml_namespace == XML_Namespace.SCHEMA:
        # Destination is an XML Schema (.xsd)
        return xml_schema_router(routable_file)

    elif routable_file.xml_namespace == XML_Namespace.LINKBASE:
        # Destination is an XML linkbase (.xml)
        return xml_linkbase_router(routable_file)

    else:
        # file destination not known!
        """
        IF ROUTABLE FILE IS NONE, IT IS PROBABLY NOT RETURNED CORRECTLY BY ROUTER!
        """

        raise Exception


def get_file_root(routable_file):
    """
     Get the file destination to write to.

    :param routable_file:
    :return:
    """
    routable_file = enrich_routable_file(routable_file=routable_file)

    # Open the file
    if mem_fs.exists(f"{routable_file.file_path}"):
        # Make sure the output is reformatted.
        # Note: Could remove wanted white space! Need to check if this is happening within tags
        parser = etree.XMLParser(remove_blank_text=False)
        tree = etree.parse(mem_fs.open(f"{routable_file.file_path}"), parser)

        # Check if element already exists. If not, return None so the file is not writen

        # todo: check if this could be sped up with an xpath expression

        for element in tree.findall(f"//{routable_file.element.tag}"):

            if element.attrib == routable_file.element.attrib:

                # logger.debug(
                #     f"{element.tag}^{element.attrib}^{routable_file.file_path}^{routable_file.type}"
                # )

                return None  # Element is already present in the file. Do not set root

        # If the element itself is not found, and
        return tree.getroot()  # Root is the existing file.

    # If the file is empty, first create a stub.
    else:
        # File needs to created
        if routable_file.xml_namespace.value == 0:
            # schema stub needs to be created before the element can be written to the file

            return create_schema_stub(
                filepath=routable_file.file_path,
                id=routable_file.id,
            )  # Root is a new schema

        elif routable_file.xml_namespace.value == 1:

            return create_linkbase_stub()  # Root is a new linkbase
        else:
            logger.error("file extension not known!")
            raise Exception


def file_writer(routable_file):
    """
    Expects an Routeable_XML_Element object. This is saved depending on the place
    the schema or linkbase router sends it to.
    :param routable_file:
    :return:
    """
    # First try to the the root element, either by creating a new file to write to or by parsing an existing file.
    if routable_file is None:
        logger.error(f"file writer received an empty or invalid routable file")
        raise

    root = get_file_root(routable_file=routable_file)

    if root is not None:  # If root has been set, write. Otherwise, don't.

        # Get the element that the file should be appended to
        if routable_file.xml_parent_element is not None:

            debug = False
            if debug is True:
                # I thought I already did check if there were duplicates, but apparently not?
                for e in root:
                    if e.get("{http://www.w3.org/1999/xlink}from"):
                        if e.tag == routable_file.element.tag:

                            if e.get(
                                "{http://www.w3.org/1999/xlink}from"
                            ) == routable_file.element.get(
                                "{http://www.w3.org/1999/xlink}from"
                            ) and e.get(
                                "{http://www.w3.org/1999/xlink}to"
                            ) == routable_file.element.get(
                                "{http://www.w3.org/1999/xlink}to"
                            ):
                                print("duplicate found!")

            if isinstance(routable_file.xml_parent_element, str):
                xml_parent_element = root.find(".//" + routable_file.xml_parent_element)

            elif isinstance(routable_file.xml_parent_element, XML_Parent_element):

                # Parent elements are not always selected on xlink@role
                if routable_file.xml_parent_element.role is None:
                    xpath_expression = f".//{routable_file.xml_parent_element.ns}:{routable_file.xml_parent_element.localname}"

                else:
                    xpath_expression = f'.//{routable_file.xml_parent_element.ns}:{routable_file.xml_parent_element.localname}[@xlink:role="{routable_file.xml_parent_element.role}"]'

                xml_parent_element = root.find(
                    xpath_expression,
                    namespaces=conf.nsmap,
                )

            else:
                xml_parent_element = None

            if xml_parent_element is None:

                # if the root is the element we want to find, it will not be found.
                # In that case, check if we are indeed looking for the root and assign accordingly
                if root.tag == routable_file.xml_parent_element:
                    xml_parent_element = root

            # Add element to parent
            if xml_parent_element is None:
                logger.warning(
                    f"Parent element for {routable_file.type} in file '{routable_file.file_path}' not found. This should have been created earlier."
                )
            elif routable_file.element in xml_parent_element.getchildren():
                pass
            else:
                if routable_file.xml_parent_position:
                    # If the element should be placed in a particular place, use insert
                    xml_parent_element.insert(
                        routable_file.xml_parent_position, routable_file.element
                    )
                else:
                    # Otherwise append to list
                    xml_parent_element.append(routable_file.element)

        else:
            # If the routable element does not have a parent, assume the root is the parent itself.
            pass

        # Write file back to virtual disk
        mem_fs.open(f"{routable_file.file_path}", "wb").write(
            etree.tostring(
                root,
                pretty_print=True,
                xml_declaration=True,
                encoding="UTF-8",
                standalone=True,
            )
        )


def add_imports(tree, ns_schemaLocations, rel_path=None):
    """
    Loops though used namespaces and yields accommodating <import /> element.

    :param tree:
    :param ns_schemaLocations:
    :return:
    """

    root = tree.getroot()
    # annotation_index = root.find("{http://www.w3.org/2001/XMLSchema}annotation")
    for namespace in root.nsmap:

        if namespace in ["xs", "xlink"]:
            pass  # Default namespaces

        else:
            if namespace in ns_schemaLocations.keys():

                ns_import = etree.Element("{http://www.w3.org/2001/XMLSchema}import")
                static_ns = ns_schemaLocations[namespace]

                # Only look for a relative schemalocation if the location is not already (statically) given
                if "schemaLocation" not in static_ns.keys():

                    ns_import.attrib["namespace"] = static_ns["namespace"]

                    schemaLocation = find_schemalocation(
                        namespace=static_ns["namespace"], rel_path=rel_path
                    )

                    if schemaLocation:
                        ns_import.attrib["schemaLocation"] = schemaLocation

                    elif "schemaLocation" not in static_ns.keys():
                        logger.error(
                            f"schemaLocation for NS '{namespace}' not given in configuration"
                        )

                    elif "xsd" not in static_ns["schemaLocation"]:
                        logger.error("schemaLocation does not point to a Schema")

                    else:
                        ns_import.attrib["schemaLocation"] = static_ns["schemaLocation"]

                    root.insert(1, ns_import)

            else:
                ns_loc = find_namespace(namespace=namespace, rel_path=tree.docinfo.URL)
                logger.error(f"Namespace '{namespace}' not recognized")
    return tree


def add_values_namespace_linkbase(tree, elem_prefix, namespace):
    root_str = etree.tostring(
        tree,
        pretty_print=True,
        xml_declaration=True,
        encoding="UTF-8",
        standalone=True,
    ).decode()
    root_str = root_str.replace(
        "<link:linkbase",
        f"<link:linkbase xmlns:{elem_prefix}='{namespace}'",
    )

    return etree.ElementTree(etree.fromstring(bytes(root_str, encoding="utf-8")))


def add_schemaLocation_linkbase(tree, schemalocation_string):

    # Add xsd namespace
    tree = add_values_namespace_linkbase(
        tree=tree,
        elem_prefix="xsi",
        namespace="http://www.w3.org/2001/XMLSchema-instance",
    )

    # Get root
    root_str = etree.tostring(
        tree,
        pretty_print=True,
        xml_declaration=True,
        encoding="UTF-8",
        standalone=True,
    ).decode()

    # Add schemalocation string to linkbase element
    root_str = root_str.replace(
        "<link:linkbase",
        f"<link:linkbase xsi:schemaLocation='{schemalocation_string}'",
    )

    return etree.ElementTree(etree.fromstring(bytes(root_str, encoding="utf-8")))


def add_values_namespace(tree, prefix, ns, filepath=None):
    """
    Takes a tree prefix and static namespace information and adds an <import> element + namespace declaration.
    This is used for XBRL specific cases, such as prefixes in attribute values or linkrole prefixes

    :param tree:
    :param prefix:
    :param ns:
    :param filepath:
    :return:
    """
    root = tree.getroot()
    ns_import = etree.Element("{http://www.w3.org/2001/XMLSchema}import")
    ns_import.attrib["namespace"] = ns["namespace"]
    add_import = False  # Default do not add an <import> element

    if filepath is not None:
        path, filename = str(filepath).rsplit("/", 1)
        if "localSchemaLocation" in ns.keys():
            # (abs) Local filepath has been given. We do not need to look for the file
            rel_path = fs.path.relativefrom(base=path, path=ns["localSchemaLocation"])
            ns_import.attrib["schemaLocation"] = rel_path
            add_import = True  # add import as it is a valid pointer

        elif "schemaLocation" not in ns.keys():
            # schemaLocation is not found within the static configuration

            # Look for a schema with @targetNamespace that matches schema location
            schemaLocation = find_schemalocation(ns["namespace"], rel_path=path)

            if schemaLocation == filename:
                pass  # namespace points to this document itself, import is not needed

            elif schemaLocation:
                ns_import.attrib["schemaLocation"] = schemaLocation
                add_import = True  # add import as it is a valid pointer

            else:
                logger.error(
                    f"namespace '{ns['namespace']}' cannot be found by looking for @targetNamespace"
                )
    else:
        # The path is a remote path, add it based on the static location given in configuration
        ns_import.attrib["schemaLocation"] = ns["schemaLocation"]
        add_import = True  # Import is statically set

    if add_import:

        if "xsd" not in ns_import.attrib["schemaLocation"]:
            logger.error("schemaLocation does not point to a Schema")

        root.insert(1, ns_import)

    return add_namespace_prefix(tree=tree, prefix=prefix, namespace=ns["namespace"])


def add_namespace_prefix(tree, prefix, namespace):
    root_str = etree.tostring(
        tree,
        pretty_print=True,
        xml_declaration=True,
        encoding="UTF-8",
        standalone=True,
    ).decode()

    # namespace is added manually as etree.cleanup_namespaces does not add a namespace for an attribute value.
    root_str = root_str.replace(
        "<xs:schema", f'<xs:schema xmlns:{prefix}="{namespace}"'
    )

    return etree.ElementTree(etree.fromstring(bytes(root_str, encoding="utf-8")))


def prettify_linkbases(created_taxo_path):
    ns_schemaLocations = conf.namespaces

    for subdir, dirs, files in mem_fs.walk(
        created_taxo_path, filter=["*.xml"]
    ):  # Only walk through the memFS dirs of the new taxonomy
        for filename in files:
            filepath = subdir + "/" + filename.name
            parser = etree.XMLParser(remove_blank_text=True, encoding="utf-8")
            tree = etree.parse(mem_fs.open(filepath), parser)
            root = tree.getroot()

            tree = add_imports(
                tree=tree, ns_schemaLocations=ns_schemaLocations, rel_path=subdir
            )

            etree.cleanup_namespaces(
                tree, top_nsmap=conf.nsmap
            )  # Take out all unused namespace declarations

            # Get the new list of namespaces so we can check whether or not ns prefixes in values are missing.
            # If we do not check this, we might end up with double declarations
            used_namespaces = list(root.nsmap.keys())

            for elem in tree.xpath("//*", namespaces=conf.nsmap):

                ### Add namespaces for element text
                if elem.tag in [
                    "{http://xbrl.org/2008/formula}qname",
                    "{http://xbrl.org/2008/filter/concept}qname",
                    "{http://xbrl.org/2008/filter/dimension}qname",
                    "{http://xbrl.org/2010/filter/concept-relation}qname",
                    "{http://xbrl.org/2014/table}relationshipSource",
                    "{http://xbrl.org/2014/table}dimensionAspect",
                    "{http://xbrl.org/2014/table}entityIdentifierAspect",
                    "{http://xbrl.org/2014/table}periodAspect",
                    "{http://xbrl.org/2014/table}conceptAspect",
                    "{http://xbrl.org/2014/table}dimension",
                ]:
                    if ":" in elem.text:
                        elem_prefix = str(elem.text).split(":")[0]
                        # check if the namespace is already known
                        if elem_prefix not in used_namespaces:

                            # Check if the namespace is described manually
                            namespace = conf.get_namespace(elem_prefix)
                            if namespace:
                                tree = add_values_namespace_linkbase(
                                    tree=tree,
                                    elem_prefix=elem_prefix,
                                    namespace=namespace["namespace"],
                                )

                                used_namespaces.append(elem_prefix)
                            else:
                                logger.error(
                                    f"namespace type prefix {elem_prefix} not in configuration"
                                )

                # Add references from <formula:*> dimension
                if elem.tag in [
                    "{http://xbrl.org/2008/formula}explicitDimension",
                    "{http://xbrl.org/2008/formula}typedDimension",
                ]:
                    if ":" in elem.get("dimension"):
                        elem_prefix = str(elem.get("dimension")).split(":")[0]
                        # check if the namespace is already known
                        if elem_prefix not in used_namespaces:
                            # Check if the namespace is described manually
                            if elem_prefix in ns_schemaLocations.keys():
                                tree = add_values_namespace_linkbase(
                                    tree=tree,
                                    elem_prefix=elem_prefix,
                                    namespace=f"{ns_schemaLocations[elem_prefix]['namespace']}",
                                )

                                used_namespaces.append(elem_prefix)
                            else:
                                logger.error(
                                    f"namespace type prefix {elem_prefix} not in configuration"
                                )
                # Add references from <variable:*> dimension
                if elem.tag in ["{http://xbrl.org/2008/variable}parameter"]:
                    for attr in [elem.get("as"), elem.get("select")]:
                        if ":" in attr:
                            elem_prefixes = re.findall("([-0-9a-zA-Z]+)\:", attr)

                            # check if the namespace is already known
                            for elem_prefix in elem_prefixes:
                                if elem_prefix not in used_namespaces:
                                    # Check if the namespace is described manually
                                    if elem_prefix in ns_schemaLocations.keys():
                                        tree = add_values_namespace_linkbase(
                                            tree=tree,
                                            elem_prefix=elem_prefix,
                                            namespace=f"{ns_schemaLocations[elem_prefix]['namespace']}",
                                        )

                                        used_namespaces.append(elem_prefix)
                                    else:
                                        logger.error(
                                            f"namespace type prefix {elem_prefix} not in configuration"
                                        )

            ### ADD xsi:schemaLocation IF NEEDED
            schemaLocation_string = ""

            for namespace in used_namespaces:

                if namespace in conf.namespaces.keys() and namespace not in [
                    "xs",
                    "link",
                    "xlink",
                    "xbrldt",
                ]:
                    # If we gave as schemaLocation for this namespace, add an explicit schemalocation entry

                    ns = conf.namespaces[namespace]["namespace"]
                    if "schemaLocation" in conf.namespaces[namespace].keys():

                        sc_loc = conf.namespaces[namespace]["schemaLocation"]

                    else:
                        # Find relative schemaLocation if local
                        if "http://" in conf.nsmap[namespace]:
                            ns_rel = conf.nsmap[namespace].replace("http:/", "")
                        else:
                            ns_rel = conf.nsmap[namespace]

                        sc_loc = get_relative_path(filepath, ns_rel)
                        if not sc_loc.startswith("."):
                            sc_loc = "./" + sc_loc
                        if not ".xsd" in sc_loc:
                            sc_loc = sc_loc + ".xsd"

                    schemaLocation_string = schemaLocation_string + f"{ns} {sc_loc} "

            if schemaLocation_string:

                tree = add_schemaLocation_linkbase(
                    tree=tree,
                    schemalocation_string=schemaLocation_string,
                )

            string_tree = prettify_namespace(tree=tree)
            mem_fs.open(filepath, "wb").write(string_tree)


def prettify_schemas(created_taxo_path):
    """
    Before saving, loop through all files and add namespaces for prefixed attribute values.

    :param namespaces:
    :return:
    """
    ns_schemaLocations = conf.namespaces

    for subdir, dirs, files in mem_fs.walk(
        created_taxo_path, filter=["*.xsd"]
    ):  # Only walk through the memFS dirs of the new taxonomy
        for filename in files:
            filepath = subdir + "/" + filename.name
            parser = etree.XMLParser(remove_blank_text=True, encoding="utf-8")
            tree = etree.parse(mem_fs.open(filepath), parser)
            root = tree.getroot()

            tree = add_imports(
                tree=tree, ns_schemaLocations=ns_schemaLocations, rel_path=subdir
            )

            etree.cleanup_namespaces(
                tree, top_nsmap=conf.nsmap
            )  # Take out all unused namespace declarations

            # Get the new list of namespaces so we can check whether or not ns prefixes in values are missing.
            # If we do not check this, we might end up with double declarations
            used_namespaces = list(root.nsmap.keys())

            ## add @targetNamespace  prefix to tree
            ## 3.02.04.01
            ## "xs:schema/@targetNamespace MOET een prefix hebben"
            if "targetNamespace" in root.attrib:
                # Look for a schema with @targetNamespace that matches schema location

                schema_namespace = root.get("targetNamespace")
                for schema_prefix, schema_dict in ns_schemaLocations.items():
                    if schema_dict["namespace"] == schema_namespace:

                        if schema_prefix:
                            tree = add_namespace_prefix(
                                tree, schema_prefix, schema_namespace
                            )
                            used_namespaces.append(schema_prefix)

            # Look for all elements
            if tree.xpath("//xs:element", namespaces=conf.nsmap):
                for elem in tree.xpath("//xs:element", namespaces=conf.nsmap):

                    if ":" in elem.get("substitutionGroup", ""):

                        elem_prefix = str(elem.get("substitutionGroup")).split(":")[0]

                        # check if the namespace is already known
                        if elem_prefix not in used_namespaces:
                            # Check if the namespace is described manually
                            if elem_prefix in ns_schemaLocations.keys():
                                tree = add_values_namespace(
                                    tree=tree,
                                    prefix=elem_prefix,
                                    ns=ns_schemaLocations[elem_prefix],
                                )
                                used_namespaces.append(elem_prefix)
                            else:
                                logger.error(
                                    f"namespace substitutionGroup prefix {elem_prefix} not in configuration"
                                )

                    # Add references from itemTypes
                    if ":" in elem.get("type"):
                        elem_prefix = str(elem.get("type")).split(":")[0]
                        # check if the namespace is already known
                        if elem_prefix not in used_namespaces:

                            # Check if the namespace is described manually
                            if elem_prefix in ns_schemaLocations.keys():
                                tree = add_values_namespace(
                                    tree=tree,
                                    prefix=elem_prefix,
                                    ns=ns_schemaLocations[elem_prefix],
                                    filepath=filepath,
                                )
                                used_namespaces.append(elem_prefix)
                            else:
                                logger.error(
                                    f"namespace type prefix {elem_prefix} not in configuration"
                                )

            """
            Schema's that define elements also need to have an explicit namespace that is not used as a normal prefix
            """
            for elem in tree.xpath("//*", namespaces=conf.nsmap):

                if elem.tag == "{http://www.w3.org/2001/XMLSchema}restriction":
                    if ":" in elem.get("base"):
                        elem_prefix = str(elem.get("base")).split(":")[0]
                        if elem_prefix not in used_namespaces:
                            tree = add_values_namespace(
                                tree=tree,
                                prefix=elem_prefix,
                                ns=ns_schemaLocations[elem_prefix],
                                filepath=filepath,
                            )
                            used_namespaces.append(elem_prefix)

                if elem.tag == "{http://www.xbrl.org/2003/linkbase}usedOn":
                    elem_prefix = str(elem.text).split(":")[0]
                    if elem_prefix not in used_namespaces:
                        tree = add_values_namespace(
                            tree=tree,
                            prefix=elem_prefix,
                            ns=ns_schemaLocations[elem_prefix],
                            filepath=filepath,
                        )
                        used_namespaces.append(elem_prefix)

            # Need to explicitly add xbrli and xbrldt import as this is expected for XBRL2.1 but not for XML
            for elem_prefix in ["xbrldt", "xbrli"]:
                if elem_prefix in used_namespaces:
                    if not elem.xpath(
                        f"//xs:import[@schemaLocation='{ns_schemaLocations[elem_prefix]['schemaLocation']}']",
                        namespaces=elem.nsmap,
                    ):
                        xml_import = etree.Element(
                            "{http://www.w3.org/2001/XMLSchema}import"
                        )
                        xml_import.attrib["namespace"] = ns_schemaLocations[
                            elem_prefix
                        ]["namespace"]
                        xml_import.attrib["schemaLocation"] = ns_schemaLocations[
                            elem_prefix
                        ]["schemaLocation"]

                        root = tree.getroot()
                        root.insert(1, xml_import)

            string_tree = prettify_namespace(tree=tree)
            mem_fs.open(filepath, "wb").write(string_tree)


def prettify_namespace(tree):
    ns_schemaLocations = conf.namespaces

    """
    Add comments to the files.
    This has to be done manually as LXML does not like adding anything next to the root node
    """
    release_data = datetime.datetime.strptime(
        str(conf.generic_info.get("date_version")), "%Y%m%d"
    )

    etree_str = etree.tostring(
        tree,
        pretty_print=True,
        xml_declaration=True,
        encoding="UTF-8",
        standalone=True,
    )

    comment = bytes(
        f"\n<!-- This file is part of the Dutch Taxonomy (Nederlandse Taxonomie; NT) \n"
        f"Intellectual Property of the State of the Netherlands \n"
        f"Architecture: {conf.generic_info.get('nt_version')} \n"
        f"Version: {conf.generic_info.get('date_version')} \n"
        f"Release date: {release_data.strftime('%a %b %m 09:00:00 %Y')} -->",
        encoding="UTF-8",
    )
    index = etree_str.find(bytes("\n", encoding="UTF-8"))
    new_etree_str = etree_str[:index] + comment + etree_str[index:]

    ### also make sure elements get placed on new lines. This is sometimes necessary for dealing with .insert() on lxml

    # new_etree_str = new_etree_str.replace(b"/><", b"/>\n<  ")

    return new_etree_str


def write_virtual_filesystem(write_directory):
    """
    Write the virtual filesystem to disk.

    :param write_directory:
    :return:
    """
    # Check if the directory exists and if not, create it.

    if not os.path.isdir(write_directory):
        os.makedirs(write_directory)

    with fs.open_fs(write_directory.__str__()) as os_fs:
        fs.copy.copy_fs(mem_fs, os_fs)
