from discover_xbrl.sbrnl_taxonomy_builder.buildtools.classes import (
    Routeable_XML_Element,
)
from discover_xbrl.sbrnl_taxonomy_builder.buildtools.mem_file_reader import file_exists
from discover_xbrl.sbrnl_taxonomy_builder.config.conf import Config

from discover_xbrl.sbrnl_taxonomy_builder.linkbase.xml_elements.arcrole import (
    get_arcrole_element,
)

from discover_xbrl.sbrnl_taxonomy_builder.linkbase.xml_elements.roleref import (
    get_xml_roleref,
)
from discover_xbrl.sbrnl_taxonomy_builder.linkbase.xml_elements.roleref_labels import (
    create_generic_labelLink,
)


conf = Config()


def linkrole_generic_labels(linkbase_path, entrypoint_path=None):
    # get  <gen:link xlink:role="http://www.xbrl.org/2008/role/link" xlink:type="extended">
    yield create_generic_labelLink(linkbase_path)

    # <link:roleRef roleURI="http://www.xbrl.org/2008/role/label" xlink:href="http://www.xbrl.org/2008/generic-label.xsd#standard-label" xlink:type="simple"/>
    yield Routeable_XML_Element(
        xml_namespace="linkbase",
        type="roleRef",
        element=get_xml_roleref(
            roleURI=f"http://www.xbrl.org/2008/role/label",
            ref_url="http://www.xbrl.org/2008/generic-label.xsd",
            ref_id="standard-label",
        ),
        file_path=linkbase_path,
        xml_parent_element="{http://www.xbrl.org/2003/linkbase}linkbase",
    )

    # <link:roleRef roleURI="http://www.xbrl.org/2008/role/link" xlink:href="http://www.xbrl.org/2008/generic-link.xsd#standard-link-role" xlink:type="simple"/>
    yield Routeable_XML_Element(
        xml_namespace="linkbase",
        type="roleRef",
        element=get_xml_roleref(
            roleURI=f"http://www.xbrl.org/2008/role/link",
            ref_url="http://www.xbrl.org/2008/generic-link.xsd",
            ref_id="standard-link-role",
        ),
        file_path=linkbase_path,
        xml_parent_element="{http://www.xbrl.org/2003/linkbase}linkbase",
    )

    # <link:arcroleRef arcroleURI="http://xbrl.org/arcrole/2008/element-label" xlink:href="http://www.xbrl.org/2008/generic-label.xsd#element-label" xlink:type="simple"/>
    yield Routeable_XML_Element(
        xml_namespace="linkbase",
        type="arcroleRef",
        element=get_arcrole_element("http://xbrl.org/arcrole/2008/element-label"),
        file_path=linkbase_path,
    )


def linkrole_label_files(dts):
    generic_labels = []
    generic_messages = []

    for roletype in dts.directly_referenced_linkbases:

        for label in roletype.labels.values():
            if (
                label.language not in generic_labels
                and label.linkRole == "http://www.xbrl.org/2008/role/label"
            ):
                generic_labels.append(label.language)
            elif (
                label.language not in generic_messages
                and label.linkRole == "http://www.xbrl.org/2010/role/message"
            ):
                # Messages and labels for specific linkroles are not handled over here.
                # Other than the linkrole-labels, they are not written to a generic file
                generic_messages.append(label.language)

    for label_language in generic_labels:

        if conf.generic_info.get("prefix") is not None:
            suffix = f'-{conf.generic_info.get("prefix")}'
        else:
            suffix = ""

        filename = f"{conf.generic_info.get('domain')}-linkroles-generic-lab-{label_language}{suffix}.xml"
        linkbase_path = f"{conf.relative_root}/dictionary/{filename}"

        # Could have been created for previous DTS
        if not file_exists(linkbase_path):
            for xml_element in linkrole_generic_labels(
                linkbase_path,
                entrypoint_path=f"{conf.relative_root}/entrypoints/{dts.entrypoint_name}",
            ):
                yield xml_element


def get_empty_files(dts):
    """
    Create some empty files so we do not need to check for their existence down the line

    :return:
    """
    for xml_element in linkrole_label_files(dts):
        yield xml_element

    # todo: normal (concept) labels
