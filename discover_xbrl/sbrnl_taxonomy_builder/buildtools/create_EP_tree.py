import logging

from datetime import datetime
from .empty_files import get_empty_files
from ..buildtools.mem_file_writer import create_directory_structure, file_writer
from ..linkbase.table_of_contents import build_table_of_contents
from ..schema.linkrole_definition import (
    get_entrypoint_stub,
)

from discover_xbrl.sbrnl_taxonomy_builder.config.conf import Config
from discover_xbrl.sbrnl_taxonomy_builder.specs.tables.start import build_tables
from ..specs.dimension.start_dim import build_hypercubes
from ..specs.formula.start import build_formulas

config = Config()

logger = logging.getLogger("builder.buildtools")


def get_extra_arcs(currently_found_arcs, linkbase_arcs):

    newly_found_arcs = []
    for arc in linkbase_arcs:

        if not arc in currently_found_arcs:
            newly_found_arcs.append(newly_found_arcs)


def start_processing_ep(dts):
    logger.info(f"Starting to process DTS {dts.entrypoint_name}")

    # Call all the builder parts
    call_file_loop(dts)

    logger.info(f"Finished processing DTS {dts.entrypoint_name}")


"""
Note to self:

For now, the loop is setup in such a way that we call different functions from call_file_loop.
However, maybe it turns out that it's better to call every function recursively from get_linkbaserefs.
"""

xml_element_generators = [
    get_entrypoint_stub,
    get_empty_files,
    build_formulas,
    build_hypercubes,
    build_tables,
    build_table_of_contents,
    # get_labelLinkbase # todo: call the lable linkbase builder directly, or though adding conceptwriter?
]


def call_file_loop(dts):

    for generator in xml_element_generators:
        """
        NOTE TO SELF:
        get_linkbaserefs should not take DTS arg, but LR + applicable DTS name.

        LR can be 'big' main LR or subset of LR.

        In get_linkbaserefs:
        make loop for each mentioned EP.

        """

        routable_element = generator(dts)
        start_time = datetime.now()
        logger.info(f"{str(start_time)} - starting generator: '{generator.__name__}'")

        for file in routable_element:
            if file is not None:
                file_writer(file)
        logger.info(f"done in {str(datetime.now()-start_time)}")
