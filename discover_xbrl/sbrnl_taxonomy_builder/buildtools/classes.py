import logging
from enum import Enum

logger = logging.getLogger("builder.buildtools")

from discover_xbrl.sbrnl_taxonomy_builder.config.conf import Config

conf = Config()


class XML_Namespace(Enum):
    SCHEMA = 0
    LINKBASE = 1


class Routeable_XML_Element:
    def __init__(
        self,
        xml_namespace,
        type,
        element,
        filename=None,
        file_path=None,
        xml_parent_element=None,
        xml_parent_position=None,
        id=None,
    ):
        """

        :param xml_namespace: Type of XML element, used to choose the correct router
        :param type: Type of XBRL element, used by the relevant router to choose file location
        :param element: The XML element that needs to be writen to file
        :param xml_parent_position: Optional int, used to indicate placement as child position
        :param filename: Optional sting, used when the element should be written to a specific file.

        The following attributes are likely added by the filerouter:
        :param file_path: Optional sting, relative path the file needs to be writen to
        :param xml_parent_element: Optional attribute to indicate to which XML element this element should be appended to
        """
        if (
            xml_namespace == "schema"
            or xml_namespace == "Schema"
            or xml_namespace == "SCHEMA"
            or xml_namespace == 0
        ):
            self.xml_namespace = XML_Namespace.SCHEMA
        elif (
            xml_namespace == "linkbase"
            or xml_namespace == "Linkbase"
            or xml_namespace == "LINKBASE"
            or xml_namespace == 1
        ):
            self.xml_namespace = XML_Namespace.LINKBASE
        else:
            logger.error(f"xml namespace of '{type}' not found")

        self.type = type
        self.element = element
        self.filename = filename
        self.file_path = file_path
        self.xml_parent_element = xml_parent_element
        self.xml_parent_position = xml_parent_position
        self.id = id  # ID of the root element. Used for Entrypoints


class XML_Parent_element:
    def __init__(self, ns, localname, tag=None, role=None):
        self.ns = ns
        self.localname = localname
        if tag:
            self.tag = tag
        else:
            self.tag = "{" + conf.nsmap[ns] + "}" + self.localname

        self.role = role
