class QNameStore:

    _instance = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self):

        self.arcStore = {}
        self.conceptStore = []
        self.parameterStore = []
        self.domainmemberStore = []

    def linkrole_exists(self, linkroleURI):
        if linkroleURI in self.linkrole.keys():
            return True
        return False

    def concept_exists(self, conceptName):
        if conceptName in self.conceptStore:
            return True
        return False

    def domainmember_exists(self, domainmemberName):
        if domainmemberName in self.domainmemberStore:
            return True
        return False

    def parameter_exists(self, parameterLabel):
        if parameterLabel in self.parameterStore:
            return True
        return False

    def arc_exists_in_linkrole(self, linkrole, arc):
        """
        Function to check if an arc already exists for a given linkrole.
        This is especially handy for handling multiple entrypoints.

        :param linkrole:
        :param from_obj:
        :param to_obj:
        :return:
        """
        # If linkrole is not know, there are also no arcs to be found
        if linkrole in self.arcStore.keys():

            # If the exact arc is found, we do not need to look further.
            if arc in self.arcStore.get(linkrole, False):
                return True

        return False

    def add_arc_to_linkrole_store(self, linkrole, arc):

        if linkrole not in self.arcStore.keys():
            self.arcStore[linkrole] = []

            if arc not in self.arcStore[linkrole]:
                self.arcStore[linkrole].append(arc)
