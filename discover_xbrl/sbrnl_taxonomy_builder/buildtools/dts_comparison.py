import logging

logger = logging.getLogger("builder.buildtools")


class DTS_comparator:

    _instance = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self, dts_list=None):

        if dts_list:
            self.dts_list = dts_list

    def presentation_hierarchy_different_in_other_dts(self, dts):

        differing_dts_list = []
        nr_of_nodes = len(dts.presentation_hierarchy.descendants)

        for other_dts in self.dts_list:
            if len(other_dts.presentation_hierarchy.descendants) != nr_of_nodes:
                differing_dts_list.append(other_dts.shortname)

        if len(differing_dts_list) > 0:
            return differing_dts_list
        else:
            return False

    def presentationlink_different_in_other_dts(self, linkrole):

        differing_dts_list = []
        nr_of_nodes = len(linkrole.presentation_hierarchy.descendants)

        for other_dts in self.dts_list:
            if linkrole in other_dts.directly_referenced_linkbases:
                if (
                    other_dts.roletypes[linkrole.roleURI].presentation_hierarchy
                    is not None
                ):
                    if (
                        len(
                            other_dts.roletypes[
                                linkrole.roleURI
                            ].presentation_hierarchy.descendants
                        )
                        != nr_of_nodes
                    ):
                        differing_dts_list.append(other_dts.shortname)

        if len(differing_dts_list) > 0:
            return differing_dts_list
        else:
            return False

    def lineitems_different_in_other_dts(self, linkrole, current_ep_suffix):
        """
        Checks whether or not the given lineitems are different in other DTSses for the given linkrole

        :return:
        """

        # First find the linkrole in the current DTS. We could also pass the linkrole as an object but this is probably
        #  a wise double check.
        differing_dts_list = []

        for dts in self.dts_list:

            # Check if the linkrole exists for the other DTS
            if dts.roletypes.get(linkrole.roleURI, False):
                # Check if the linkrole actually has a hypercube
                if dts.roletypes[linkrole.roleURI].hypercube is not None:
                    # Check if the lineitems match the given linkrole
                    if (
                        dts.roletypes[linkrole.roleURI].hypercube.lineItems
                        is not linkrole.hypercube.lineItems
                    ):
                        differing_dts_list.append(dts.shortname)

        if len(differing_dts_list) > 0:
            # add current ep to list
            differing_dts_list.append(current_ep_suffix)
            differing_dts_list.sort()

            return differing_dts_list

        return False

    def _dimension_in_eps(self, linkrole, dimension):

        ep_list = {}
        for dts in self.dts_list:
            if dts.roletypes.get(linkrole.roleURI, False):
                for other_linkrole in dts.roletypes.values():

                    # Only compare the same linkrole
                    if linkrole.roleURI == other_linkrole.roleURI:

                        # Only check other linkrole if it has a Hypercube
                        if other_linkrole.hypercube:
                            for other_dimension in other_linkrole.hypercube.dimensions:
                                if (
                                    dimension.target_linkrole
                                    == other_dimension.target_linkrole
                                ):
                                    dimension_role_string = f"{dimension.concept.name}@{dimension.target_linkrole}"
                                    if not dimension_role_string in ep_list:

                                        ep_list[dimension_role_string] = [dts.shortname]
                                    else:
                                        ep_list[dimension_role_string].append(
                                            dts.shortname
                                        )

        return ep_list

    def dimensions_different_in_other_dts(self, linkrole, current_ep_suffix):
        """
        Checks whether or not the given lineitems are different in other DTSses for the given linkrole

        :return:
        """

        # First find the linkrole in the current DTS. We could also pass the linkrole as an object but this is probably
        #  a wise double check.
        differing_dimensions = {}

        # TODO: Simplify check if we continue to not use the differences to combine files!

        for dts in self.dts_list:

            # Check if the linkrole exists for the other DTS
            if dts.roletypes.get(linkrole.roleURI, False):
                # Check if the linkrole actually has a hypercube

                if dts.roletypes[linkrole.roleURI].hypercube is not None:
                    # Check if the dimensions match the given linkrole

                    if (
                        dts.roletypes[linkrole.roleURI].hypercube.dimensions
                        is not linkrole.hypercube.dimensions
                    ):
                        for dimension in linkrole.hypercube.dimensions:

                            differing_dimensions[
                                f"{dimension.concept.name}@{dimension.target_linkrole}"
                            ] = self._dimension_in_eps(
                                linkrole=linkrole, dimension=dimension
                            )

        if len(differing_dimensions) > 0:

            return differing_dimensions

        return False

    def lineitems_same_as_other_dts(self, linkrole):
        """
        Checks whether or not the given lineitems are different in other DTSses for the given linkrole

        :return:
        """

        # First find the linkrole in the current DTS. We could also pass the linkrole as an object but this is probably
        #  a wise double check.
        same_dts_list = []

        for dts in self.dts_list:

            # Check if the linkrole exists for the other DTS
            if dts.roletypes.get(linkrole.roleURI, False):
                # Check if the linkrole actually has a hypercube
                if dts.roletypes[linkrole.roleURI].hypercube is not None:
                    # Check if the lineitems match the given linkrole
                    if (
                        dts.roletypes[linkrole.roleURI].hypercube.lineItems
                        is linkrole.hypercube.lineItems
                    ):
                        same_dts_list.append(dts.shortname)

        if len(same_dts_list) > 0:

            return same_dts_list

        return False
