import logging

from discover_xbrl.conf.conf import Config


def setup_logger():
    """
    # Set all loggers for the builder
    #"""

    formatter = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    )
    conf = Config()

    # Setup builder logger
    logger_builder = logging.getLogger("builder")
    logger_builder.propagate = False
    logger_builder.setLevel(logging.INFO)
    logger_builder.addHandler(
        logging.FileHandler(f"{conf.logging['maindir']}/builder.log", "a+")
    )
    logger_builder.addHandler(logging.StreamHandler())
    # logger_builder.setLevel(logging.WARNING)   # container for parser loggers. Do not log info and debug

    # Setup postprocessing logger
    logger_postprocessing = logging.getLogger("builder.postprocessing")
    logger_postprocessing.setLevel(logging.INFO)
    logger_postprocessing.parent = False
    logger_postprocessing.addHandler(
        logging.FileHandler(
            f"{conf.logging['maindir']}/builder_postprocessing.log", "a+"
        )
    )
    logger_postprocessing.addHandler(logging.StreamHandler())

    # Setup linkbase logger
    logger_linkbase = logging.getLogger("builder.linkbase")
    logger_linkbase.setLevel(logging.INFO)
    logger_linkbase.propagate = False
    logger_linkbase.addHandler(
        logging.FileHandler(f"{conf.logging['maindir']}/builder_linkbase.log", "a+")
    )
    logger_linkbase.addHandler(logging.StreamHandler())

    # Setup schema logger
    logger_schema = logging.getLogger("builder.schema")
    logger_schema.propagate = False
    logger_schema.setLevel(logging.INFO)
    logger_schema.addHandler(
        logging.FileHandler(f"{conf.logging['maindir']}/builder_schema.log", "a+")
    )
    logger_schema.addHandler(logging.StreamHandler())

    # Setup buildtools logger
    logger_buildtools = logging.getLogger("builder.buildtools")
    logger_buildtools.propagate = False
    logger_buildtools.addHandler(
        logging.FileHandler(f"{conf.logging['maindir']}/builder_buildtools.log", "a+")
    )
    logger_buildtools.addHandler(logging.StreamHandler())

    # Setup buildtools logger
    logger_buildtools_filewriter = logging.getLogger("builder.buildtools.filewriter")
    logger_buildtools_filewriter.propagate = False
    logger_buildtools_filewriter.setLevel(logging.DEBUG)
    logger_buildtools_filewriter.addHandler(
        logging.FileHandler(f"{conf.logging['maindir']}/builder_filewriter.log", "a+")
    )
    logger_buildtools_filewriter.addHandler(logging.StreamHandler())
