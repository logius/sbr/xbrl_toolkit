import logging
import os
from pathlib import Path

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

from PySide2.QtCore import QThread
from PySide2.QtWidgets import QGroupBox, QVBoxLayout, QPushButton, QFileDialog, QWidget

from .buildtools.create_EP_tree import start_processing_ep
from .buildtools.mem_file_writer import (
    write_virtual_filesystem,
    import_taxonomies,
    prettify_linkbases,
    prettify_schemas,
    create_directory_structure,
)
from .buildtools.mem_file_reader import get_imported_namespaces
from discover_xbrl.sbrnl_modules.db.sql_to_dts import SqlToDts
from discover_xbrl.sbrnl_taxonomy_builder.config.conf import Config as BuilderConfig


logger = logging.getLogger("builder")

builderconf = BuilderConfig()

from .buildtools.dts_comparison import DTS_comparator

fh = logging.FileHandler(
    builderconf.generic_info.get("logfile", "/tmp/xbrl_builder.log"), "a+"
)

fh.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
fh.setFormatter(formatter)
logger.addHandler(fh)

# fh = logging.FileHandler(f'builder/buildlog.log', 'a+')
# fh.setLevel(logging.INFO)
# formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
# fh.setFormatter(formatter)
# logger_build_gui.addHandler(fh)

from discover_xbrl.sbrnl_taxonomy_builder.buildtools.qname_store import QNameStore

"""
GUI for taxonomy building module. 
"""


class TaxonomyBuildingWorker(QThread):
    def __init__(self, parent=None, dts_list=None, directory=None):

        self.dts_list = dts_list
        self.export_directory = directory

        QThread.__init__(self, parent)

    def run(self):
        """
        Exports the DTS as a taxonomy.

        :return: None
        """

        # Get the config file with information on namespaces, versioning and domain prefixes.

        # self.get_config()
        """
        NOTE TO SELF:   
        Before this runs, we need to check for:
        -   discrepancies between EPs presentation hierarchy
        -   EPs that reference the same LR more than once?
        
        """

        ###
        ### This is stuff that comes from builder_metadata.yaml
        ###

        # Create a virtual filesystem. The taxonomy lives in memory during building
        logger.info("Creating in memory directory structure")
        create_directory_structure(relative_root=builderconf.relative_root)

        # add imported namespaces to known namespaces map

        # Import referenced taxonomies. Right now this is done via support script.
        if builderconf.generic_info.get("import_taxonomies"):
            import_taxonomies(
                # absolute_path=os.path.abspath(__file__).rsplit("/", 1)[0],
                absolute_path=builderconf.generic_info.get("import_dir"),
                domainlist=builderconf.generic_info.get("import_taxonomies"),
                www_domain=builderconf.generic_info.get("www_domain"),
                nt_version=builderconf.generic_info.get("nt_version"),
            )
        imported_namespaces = get_imported_namespaces()
        builderconf.namespaces.update(imported_namespaces)

        # Initialize the Qname Store
        qname_store = QNameStore()

        # Load the DTS comparator to allow for comparisons during runtime.
        DTS_comparator(dts_list=self.dts_list)

        # Loop though all DTS/EP's loaded by the program.
        for dts in self.dts_list:
            #
            # Add extra info to the builder if this is available in the DTS object
            # Some of this information overwrites the builder_metadata.
            # We expect the data in the DTS object to be more relevant.
            #
            if hasattr(dts, "generic_info") and dts.generic_info is not None:
                for prefix, v in dts.generic_info.new_namespaces.items():
                    if prefix not in builderconf.namespaces:
                        builderconf.namespaces[prefix] = {
                            "namespace": v["namespace"],
                            "localpath": v["localpath"],
                        }
                    elif builderconf.namespaces[prefix]["namespace"] != v["namespace"]:
                        print(
                            f"External namespace '{v['namespace']}' does not match '{builderconf.namespaces[prefix]['namespace']}' "
                            f"but shares prefix {prefix}."
                            f"Either the value in builder config or (generated) taxonomy is wrong."
                        )

                # Overwrite info from the builder config file in favor of taxonomy data
                if dts.generic_info.domain:
                    builderconf.generic_info["domain"] = dts.generic_info.domain
                if dts.generic_info.prefix:
                    builderconf.generic_info["prefix"] = dts.generic_info.prefix
                if dts.generic_info.date:
                    builderconf.generic_info["date_version"] = dts.generic_info.date
                if dts.generic_info.nt_version:
                    builderconf.generic_info["nt_version"] = dts.generic_info.nt_version
                if dts.generic_info.new_namespaces:
                    # These are namespaces that are newly defined in this DTS
                    builderconf.generic_info["new_namespaces"] = [
                        ns["namespace"]
                        for ns in dts.generic_info.new_namespaces.values()
                    ]

                # Set the absolute and relative root again
                relative_root = (
                    f"{builderconf.generic_info.get('www_domain')}/"
                    f"{builderconf.generic_info.get('nt_version')}/"
                    f"{builderconf.generic_info.get('domain')}/"
                    f"{builderconf.generic_info.get('date_version')}"
                )
                setattr(builderconf, "relative_root", relative_root)
                # Absolute root, used for target namespace and other absolute indicators
                absolute_root = (
                    f"{builderconf.generic_info.get('www_protocol')}/"
                    f"{builderconf.generic_info.get('www_domain')}/"
                    f"{builderconf.generic_info.get('nt_version')}/"
                    f"{builderconf.generic_info.get('domain')}/"
                    f"{builderconf.generic_info.get('date_version')}"
                )
                setattr(builderconf, "absolute_root", absolute_root)

            # Import referenced taxonomies. Right now this is done via support script.
            if builderconf.generic_info.get("import_taxonomies"):
                import_taxonomies(
                    # absolute_path=os.path.abspath(__file__).rsplit("/", 1)[0],
                    absolute_path=builderconf.generic_info.get("import_dir"),
                    domainlist=builderconf.generic_info.get("import_taxonomies"),
                    www_domain=builderconf.generic_info.get("www_domain"),
                    nt_version=builderconf.generic_info.get("nt_version"),
                )

            logger.info(f"Starting to work on EP: {dts.entrypoint_name}")

            # Start the processing loop
            start_processing_ep(dts=dts)

        # Write to filesystem after all EP's are writen to memory

        # Post-loop functions
        logger.info(f"Doing post-loop additions before writing")
        prettify_linkbases(created_taxo_path=builderconf.relative_root)
        prettify_schemas(created_taxo_path=builderconf.relative_root)

        logger.info(f"Starting to write taxonomy to disk")

        write_virtual_filesystem(self.export_directory)
        logger.info(f"All done! (wrote taxonomy to disk)")


class TaxonomyBuilderWidget(QGroupBox):
    """
    GUI Widget for building a taxonomy. This is the central control panel for taxonomy building tools.
    """

    def __init__(self, parent=None):
        super(TaxonomyBuilderWidget, self).__init__(parent)

        self.title = "Taxonomy Builder"
        self.parent = parent
        self.dts_worker = None

        # Create widgets
        buildTaxonomyButton = QPushButton("&Generate taxonomy")

        # Connect functions to buttons
        buildTaxonomyButton.clicked.connect(self.selectTaxonomySheet)

        # Set widgets to layout
        layout = QVBoxLayout()
        layout.addWidget(buildTaxonomyButton)

        # Set layout to class attribute
        self.setLayout(layout)

    def selectTaxonomySheet(self):
        """
        Popup for selecting the sheet used to build the taxonomy from
        """

        testing = True  # temporary check so I don't get asked for an export dir the whole time.
        if not testing:
            popup = QWidget()
            target_taxonomy_directory = Path(
                QFileDialog.getExistingDirectory(popup, "Select output directory")
            )
        else:
            if not Path(builderconf.generic_info.get("export_dir")).exists():
                os.makedirs(builderconf.generic_info.get("export_dir"))
            target_taxonomy_directory = Path(builderconf.generic_info.get("export_dir"))

        if not len(self.parent.discovered_dts_list):
            print(f"Fetching from db")
            dts_maker = SqlToDts(
                dts_name="kvk-rpt-jaarverantwoording-2021-nlgaap-middelgroot-publicatiestukken.xsd"
            )
            self.parent.discovered_dts_list.append(dts_maker.DTS)

        self.callBuilder(target_taxonomy_directory)

    def callBuilder(self, directory):
        """
        Sets GUI elements before calling the discovery routine.
        """

        self.dts_worker = TaxonomyBuildingWorker(
            self, dts_list=self.parent.discovered_dts_list, directory=directory
        )

        # Call actual building routine
        # dts_list = self.dts_worker.run()
        self.dts_worker.run()
