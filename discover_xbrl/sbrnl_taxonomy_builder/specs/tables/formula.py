import logging
from lxml import etree
from discover_xbrl.sbrnl_taxonomy_builder.buildtools.qname_store import QNameStore
from discover_xbrl.sbrnl_xbrl_parser.schema.classes import Concept

qname_store = QNameStore()
from discover_xbrl.sbrnl_taxonomy_builder.config.conf import Config

conf = Config()
logger = logging.getLogger("builder.linkbase")


def get_xml_formula_explicit_dimension(ruleset):
    xml_explicit_dimension = etree.Element(
        "{http://xbrl.org/2008/formula}explicitDimension"
    )
    xml_explicit_dimension.set("dimension", ruleset.qname_dimension)

    xml_formula_member = etree.Element("{http://xbrl.org/2008/formula}member")
    if ruleset.qname_member:
        if isinstance(ruleset.qname_member, str):
            if isinstance(ruleset.qname_member_obj, Concept):
                etree.SubElement(
                    xml_formula_member, "{http://xbrl.org/2008/formula}qname"
                ).text = ruleset.qname_member
            elif (
                hasattr(ruleset, "qname_member_expression")
                and ruleset.qname_member_expression
            ):
                etree.SubElement(
                    xml_formula_member, "{http://xbrl.org/2008/formula}qnameExpression"
                ).text = ruleset.qname_member_expression
    elif (
        hasattr(ruleset, "qname_member_expression") and ruleset.qname_member_expression
    ):
        etree.SubElement(
            xml_formula_member, "{http://xbrl.org/2008/formula}qnameExpression"
        ).text = ruleset.qname_member_expression

        # Add <formula:member> to <formula:explicitDimension>

    else:
        logger.warning(
            f"ruleset '{ruleset.qname_dimension}' does not have a qname or qname expression"
        )
    xml_explicit_dimension.append(xml_formula_member)
    return xml_explicit_dimension


def get_xml_formula_typed_dimension(ruleset):
    xml_typed_dimension = etree.Element("{http://xbrl.org/2008/formula}typedDimension")
    xml_typed_dimension.set("dimension", ruleset.qname_dimension)

    return xml_typed_dimension


def get_xml_ruleset(ruleset):
    # Add <table:ruleSet tag="">
    xml_ruleset_element = etree.Element("{http://xbrl.org/2014/table}ruleSet")
    xml_ruleset_element.set("tag", ruleset.tag)

    for rule in ruleset.formulas:
        # Add the ruleSet
        xml_ruleset_element.append(get_xml_formula_period(ruleset=rule))

    return xml_ruleset_element


def get_xml_formula_period(ruleset):
    xml_formula_period = etree.Element("{http://xbrl.org/2008/formula}period")

    if ruleset.periodtype == "duration":
        xml_formula_type = etree.Element("{http://xbrl.org/2008/formula}duration")
        xml_formula_type.set("start", ruleset.start)
        xml_formula_type.set("end", ruleset.end)
    elif ruleset.periodtype == "instant":
        xml_formula_type = etree.Element("{http://xbrl.org/2008/formula}instant")
        xml_formula_type.set("value", ruleset.value)
    elif ruleset.periodtype == "forever":
        xml_formula_type = etree.Element("{http://xbrl.org/2008/formula}forever")
    else:
        xml_formula_type = None

    if xml_formula_type is not None:
        xml_formula_period.append(xml_formula_type)

    return xml_formula_period
