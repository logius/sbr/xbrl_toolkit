from lxml import etree

from discover_xbrl.sbrnl_taxonomy_builder.buildtools.classes import (
    Routeable_XML_Element,
    XML_Parent_element,
)
from discover_xbrl.sbrnl_taxonomy_builder.linkbase.genericLinkbase import (
    get_generic_labels,
)
from discover_xbrl.sbrnl_taxonomy_builder.linkbase.xml_elements.arcrole import (
    get_arcrole_element,
)
from discover_xbrl.sbrnl_taxonomy_builder.specs.tables.breakdown_tree import (
    loop_through_definition_node_subtree,
)
from discover_xbrl.sbrnl_taxonomy_builder.specs.tables.parameter import (
    get_table_parameter,
)


def get_xml_table_element(xml_parent_element, linkbase_path, table):
    xml_table_element = etree.Element("{http://xbrl.org/2014/table}table")

    xml_table_element.set("id", table.xlink_label)
    xml_table_element.set("parentChildOrder", table.parentChildOrder)
    xml_table_element.set("{http://www.w3.org/1999/xlink}label", table.xlink_label)
    xml_table_element.set("{http://www.w3.org/1999/xlink}type", "resource")
    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type="table",
        element=xml_table_element,
        file_path=linkbase_path,
        xml_parent_element=xml_parent_element,
    )


def get_xml_table_breakdown(
    xml_parent_element, linkbase_path, table_label, breakdown, linkrole, ep_path
):
    ### <table:breakdown>.labe
    xml_table_breakdown_element = etree.Element("{http://xbrl.org/2014/table}breakdown")
    xml_table_breakdown_element.set("id", breakdown.xlink_label)
    xml_table_breakdown_element.set("parentChildOrder", breakdown.parentChildOrder)
    xml_table_breakdown_element.set(
        "{http://www.w3.org/1999/xlink}label", breakdown.xlink_label
    )
    xml_table_breakdown_element.set("{http://www.w3.org/1999/xlink}type", "resource")
    yield Routeable_XML_Element(
        xml_namespace="linkbase",
        type="table_breakdown",
        element=xml_table_breakdown_element,
        file_path=linkbase_path,
        xml_parent_element=xml_parent_element,
    )

    ### <table:tableBreakdownArc>
    xml_table_breakdownArc_element = etree.Element(
        "{http://xbrl.org/2014/table}tableBreakdownArc"
    )

    xml_table_breakdownArc_element.set("axis", breakdown.axis)
    xml_table_breakdownArc_element.set("order", str(breakdown.order))
    xml_table_breakdownArc_element.set(
        "{http://www.w3.org/1999/xlink}arcrole",
        "http://xbrl.org/arcrole/2014/table-breakdown",
    )
    xml_table_breakdownArc_element.set(
        "{http://www.w3.org/1999/xlink}type",
        "arc",
    )
    xml_table_breakdownArc_element.set(
        "{http://www.w3.org/1999/xlink}from", table_label
    )
    xml_table_breakdownArc_element.set(
        "{http://www.w3.org/1999/xlink}to", breakdown.xlink_label
    )
    yield Routeable_XML_Element(
        xml_namespace="linkbase",
        type="table_breakdown_arc",
        element=xml_table_breakdownArc_element,
        file_path=linkbase_path,
        xml_parent_element=xml_parent_element,
    )

    for xml_element in loop_through_definition_node_subtree(
        breakdown=breakdown,
        linkbase_path=linkbase_path,
        xml_parent_element=xml_parent_element,
        linkrole=linkrole,
        ep_path=ep_path,
    ):
        yield xml_element


def get_arcroles(linkbase_path):
    yield Routeable_XML_Element(
        xml_namespace="linkbase",
        type="arcroleRef",
        element=get_arcrole_element(
            arcrole_uri="http://xbrl.org/arcrole/2014/breakdown-tree",
            href="http://www.xbrl.org/2014/table.xsd#breakdown-tree",
        ),
        file_path=linkbase_path,
    )

    yield Routeable_XML_Element(
        xml_namespace="linkbase",
        type="arcroleRef",
        element=get_arcrole_element(
            arcrole_uri="http://xbrl.org/arcrole/2014/definition-node-subtree",
            href="http://www.xbrl.org/2014/table.xsd#definition-node-subtree",
        ),
        file_path=linkbase_path,
    )

    yield Routeable_XML_Element(
        xml_namespace="linkbase",
        type="arcroleRef",
        element=get_arcrole_element(
            arcrole_uri="http://xbrl.org/arcrole/2014/table-breakdown",
            href="http://www.xbrl.org/2014/table.xsd#table-breakdown",
        ),
        file_path=linkbase_path,
    )


def built_table(linkbase_path, table, linkrole, ep_path):
    parent_gen_link = XML_Parent_element(
        ns="gen",
        localname="link",
        role=linkrole.roleURI,
    )
    for xml_arcrole_element in get_arcroles(linkbase_path=linkbase_path):
        yield xml_arcrole_element

    yield Routeable_XML_Element(
        xml_namespace="linkbase",
        type="arcroleRef",
        element=get_arcrole_element(
            arcrole_uri="http://xbrl.org/arcrole/2014/table-parameter",
            href="http://www.xbrl.org/2014/table.xsd#table-parameter",
        ),
        file_path=linkbase_path,
    )

    # Go though the table breakdowns and build/yield each axis
    for table_breakdown in table.table_breakdowns:

        # multiple elements are yielded through this recursive loop
        for xml_element in get_xml_table_breakdown(
            xml_parent_element=parent_gen_link,
            linkbase_path=linkbase_path,
            table_label=table.xlink_label,
            breakdown=table_breakdown,
            linkrole=linkrole,
            ep_path=ep_path,
        ):
            yield xml_element

    # Yield  the <table:table> element
    yield get_xml_table_element(
        xml_parent_element=parent_gen_link, linkbase_path=linkbase_path, table=table
    )

    for label in get_generic_labels(
        labels=table.labels, linkrole=linkrole, ep_path=ep_path
    ):
        yield label

    # Add table parameters
    for i, parameter in enumerate(table.parameters):

        for xml_parameter_element in get_table_parameter(
            parameter=parameter,
            table_id=table.xlink_label,
            file_path=linkbase_path,
            parent_link=parent_gen_link,
            # order=i + len(table.table_breakdowns) + 1,
        ):
            yield xml_parameter_element

    for table_filter in table.filters:
        print()
