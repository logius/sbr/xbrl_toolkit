import logging

import anytree
from lxml import etree

from discover_xbrl.sbrnl_taxonomy_builder.buildtools.classes import (
    Routeable_XML_Element,
    XML_Parent_element,
)
from discover_xbrl.sbrnl_taxonomy_builder.buildtools.empty_files import (
    linkrole_generic_labels,
)
from discover_xbrl.sbrnl_taxonomy_builder.buildtools.mem_file_reader import (
    get_relative_path,
)
from discover_xbrl.sbrnl_taxonomy_builder.linkbase.genericLinkbase import (
    get_generic_link,
)
from discover_xbrl.sbrnl_taxonomy_builder.linkbase.xml_elements.arcrole import (
    get_arcrole_element,
)
from discover_xbrl.sbrnl_taxonomy_builder.linkbase.xml_elements.locator import (
    loc_creator,
)
from discover_xbrl.sbrnl_taxonomy_builder.linkbase.xml_elements.roleref import (
    get_xml_roleref,
)

from discover_xbrl.sbrnl_taxonomy_builder.linkbase.xml_elements.roleref_labels import (
    get_generic_label,
    get_generic_labelarc,
    get_dynamic_labelarc,
)
from discover_xbrl.sbrnl_taxonomy_builder.linkbase.xml_elements.roleref_msg import (
    get_generic_message,
)

from discover_xbrl.sbrnl_taxonomy_builder.specs.tables.formula import (
    get_xml_formula_explicit_dimension,
    get_xml_formula_typed_dimension,
    get_xml_formula_period,
    get_xml_ruleset,
)
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.formulas.classes.formula import (
    FormulaExplicitDimension,
    FormulaTypedDimension,
    FormulaPeriod,
)
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.tables.classes import (
    RuleNode,
    Breakdown,
    ConceptRelationshipNode,
    AspectNode,
    DimensionRelationshipNode,
    RuleSet,
)

from discover_xbrl.sbrnl_taxonomy_builder.config.conf import Config

conf = Config()


def get_xml_table_breakdown_tree_arc(
    node, linkbase_path, xml_parent_element, parent_id
):
    ### <table:tableBreakdownArc>
    xml_table_breakdown_tree_Arc_element = etree.Element(
        "{http://xbrl.org/2014/table}breakdownTreeArc"
    )

    if node.order:
        xml_table_breakdown_tree_Arc_element.set("order", str(node.order))

    xml_table_breakdown_tree_Arc_element.set(
        "{http://www.w3.org/1999/xlink}arcrole",
        "http://xbrl.org/arcrole/2014/breakdown-tree",
    )
    xml_table_breakdown_tree_Arc_element.set(
        "{http://www.w3.org/1999/xlink}from", parent_id
    )
    xml_table_breakdown_tree_Arc_element.set(
        "{http://www.w3.org/1999/xlink}to", node.xlink_label
    )
    xml_table_breakdown_tree_Arc_element.set(
        "{http://www.w3.org/1999/xlink}type", "arc"
    )
    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type="table_breakdown_arc",
        element=xml_table_breakdown_tree_Arc_element,
        file_path=linkbase_path,
        xml_parent_element=xml_parent_element,
    )


def get_xml_table_definition_node_subtree_arc(
    node, linkbase_path, xml_parent_element, parent_id
):

    xml_table_definition_subtree_Arc_element = etree.Element(
        "{http://xbrl.org/2014/table}definitionNodeSubtreeArc"
    )

    if node.order:
        xml_table_definition_subtree_Arc_element.set("order", str(node.order))

    xml_table_definition_subtree_Arc_element.set(
        "{http://www.w3.org/1999/xlink}arcrole",
        "http://xbrl.org/arcrole/2014/definition-node-subtree",
    )
    xml_table_definition_subtree_Arc_element.set(
        "{http://www.w3.org/1999/xlink}from", parent_id
    )
    xml_table_definition_subtree_Arc_element.set(
        "{http://www.w3.org/1999/xlink}to", node.xlink_label
    )
    xml_table_definition_subtree_Arc_element.set(
        "{http://www.w3.org/1999/xlink}type", "arc"
    )
    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type="table_subtree_arc",
        element=xml_table_definition_subtree_Arc_element,
        file_path=linkbase_path,
        xml_parent_element=xml_parent_element,
    )


def get_table_tree_arc(node, linkbase_path, xml_parent_element):
    """
    Returns a breakdown tree arc or definition node subtree arc, depending on the depth

    :param node:
    :param linkbase_path:
    :param xml_parent_element:
    :return:
    """
    # if node.depth <= 1:
    if isinstance(node.parent, Breakdown):
        return get_xml_table_breakdown_tree_arc(
            node=node,
            linkbase_path=linkbase_path,
            xml_parent_element=xml_parent_element,
            parent_id=node.parent.xlink_label,
        )
    else:

        return get_xml_table_definition_node_subtree_arc(
            node=node,
            linkbase_path=linkbase_path,
            xml_parent_element=xml_parent_element,
            parent_id=node.parent.xlink_label,
        )


def get_xml_rule_node(node, linkbase_path, xml_parent_element):

    # Get node
    xml_table_node_element = etree.Element("{http://xbrl.org/2014/table}ruleNode")

    xml_table_node_element.set(
        "id", node.xlink_label
    )  # Do not use table.id but table.label?
    xml_table_node_element.set("merge", str(node.merge).lower())
    xml_table_node_element.set("parentChildOrder", node.parentChildOrder)

    if node.tagselector:
        xml_table_node_element.set("tagSelector", node.tagselector)

    if node.is_abstract:
        xml_table_node_element.set("abstract", str(node.is_abstract).lower())

    xml_table_node_element.set("{http://www.w3.org/1999/xlink}label", node.xlink_label)
    xml_table_node_element.set("{http://www.w3.org/1999/xlink}type", "resource")

    # Add rulesets to node element
    for ruleset in node.rulesets:

        if isinstance(ruleset, FormulaExplicitDimension):
            # Add<formula:explicitDimension> to <table:ruleNode>
            xml_table_node_element.append(
                get_xml_formula_explicit_dimension(ruleset=ruleset)
            )
        elif isinstance(ruleset, FormulaTypedDimension):
            # Add<formula:typedDimension> to <table:ruleNode>
            xml_table_node_element.append(
                get_xml_formula_typed_dimension(ruleset=ruleset)
            )
        elif isinstance(ruleset, FormulaPeriod):
            # Add<table:ruleSet> to <table:ruleNode>
            xml_table_node_element.append(get_xml_formula_period(ruleset=ruleset))

        elif isinstance(ruleset, RuleSet):
            xml_table_node_element.append(get_xml_ruleset(ruleset=ruleset))
        else:
            print("")

    yield Routeable_XML_Element(
        xml_namespace="linkbase",
        type="table_node",
        element=xml_table_node_element,
        file_path=linkbase_path,
        xml_parent_element=xml_parent_element,
    )

    ## Get Arc
    yield get_table_tree_arc(
        node=node, linkbase_path=linkbase_path, xml_parent_element=xml_parent_element
    )


def get_xml_relationship_node(node, linkbase_path, xml_parent_element):

    # First get the main element.

    xml_table_node_element = etree.Element(
        "{http://xbrl.org/2014/table}conceptRelationshipNode"
    )

    xml_table_node_element.set(
        "id", node.xlink_label
    )  # Do not use table.id but table.label?
    xml_table_node_element.set(
        "parentChildOrder",
        node.parentChildOrder if node.parentChildOrder else "parent-first",
    )

    if node.tagselector:
        xml_table_node_element.set("tagSelector", node.tagselector)

    xml_table_node_element.set("{http://www.w3.org/1999/xlink}label", node.xlink_label)
    xml_table_node_element.set("{http://www.w3.org/1999/xlink}type", "resource")

    # Add the relationship source
    etree.SubElement(
        xml_table_node_element, "{http://xbrl.org/2014/table}relationshipSource"
    ).text = node.relationship_source

    # Add the linkrole
    etree.SubElement(
        xml_table_node_element, "{http://xbrl.org/2014/table}linkrole"
    ).text = str(node.linkrole)

    # Add the arcrole
    etree.SubElement(
        xml_table_node_element, "{http://xbrl.org/2014/table}arcrole"
    ).text = node.arcrole

    # Add the formulaAxis
    if node.formula_axis is not None:
        etree.SubElement(
            xml_table_node_element, "{http://xbrl.org/2014/table}formulaAxis"
        ).text = node.formula_axis

    yield Routeable_XML_Element(
        xml_namespace="linkbase",
        type="table_node",
        element=xml_table_node_element,
        file_path=linkbase_path,
        xml_parent_element=xml_parent_element,
    )

    ## Get Arc
    yield get_table_tree_arc(
        node=node, linkbase_path=linkbase_path, xml_parent_element=xml_parent_element
    )


def get_xml_aspect_node(node, linkbase_path, xml_parent_element):

    # Get node

    xml_table_node_element = etree.Element("{http://xbrl.org/2014/table}aspectNode")

    xml_table_node_element.set(
        "id", node.xlink_label
    )  # Do not use table.id but table.label?

    if node.tagselector:
        xml_table_node_element.set("tagSelector", node.tagselector)

    xml_table_node_element.set("{http://www.w3.org/1999/xlink}label", node.xlink_label)
    xml_table_node_element.set("{http://www.w3.org/1999/xlink}type", "resource")

    ## add aspect to node
    if node.aspect.aspect_type == "dimension":
        xml_table_node_aspect_element = etree.Element(
            "{http://xbrl.org/2014/table}dimensionAspect"
        )
        xml_table_node_aspect_element.set(
            "includeUnreportedValue", str(node.aspect.includeUnreportedValue).lower()
        )
    elif node.aspect.aspect_type == "identity_identifier":
        xml_table_node_aspect_element = etree.Element(
            "{http://xbrl.org/2014/table}entityIdentifierAspect"
        )
    elif node.aspect.aspect_type == "period":
        xml_table_node_aspect_element = etree.Element(
            "{http://xbrl.org/2014/table}periodAspect"
        )
    elif node.aspect.aspect_type == "concept":
        xml_table_node_aspect_element = etree.Element(
            "{http://xbrl.org/2014/table}conceptAspect"
        )

    xml_table_node_aspect_element.text = node.aspect.value
    xml_table_node_element.append(xml_table_node_aspect_element)

    yield Routeable_XML_Element(
        xml_namespace="linkbase",
        type="table_node",
        element=xml_table_node_element,
        file_path=linkbase_path,
        xml_parent_element=xml_parent_element,
    )

    ## Get Arc
    yield get_table_tree_arc(
        node=node, linkbase_path=linkbase_path, xml_parent_element=xml_parent_element
    )


def get_xml_dimensionrelationship_node(node, linkbase_path, xml_parent_element):

    # Get node

    xml_table_node_element = etree.Element(
        "{http://xbrl.org/2014/table}dimensionRelationshipNode"
    )

    xml_table_node_element.set(
        "id", node.xlink_label
    )  # Do not use table.id but table.label?
    if node.parentChildOrder:
        xml_table_node_element.set("parentChildOrder", node.parentChildOrder)

    if node.tagselector:
        xml_table_node_element.set("tagSelector", node.tagselector)

    xml_table_node_element.set("{http://www.w3.org/1999/xlink}label", node.xlink_label)
    xml_table_node_element.set("{http://www.w3.org/1999/xlink}type", "resource")

    # Add the linkrole
    etree.SubElement(
        xml_table_node_element, "{http://xbrl.org/2014/table}linkrole"
    ).text = (
        node.linkrole if isinstance(node.linkrole, str) else node.linkrole.roleURI
    )

    # Add the relationship source
    etree.SubElement(
        xml_table_node_element, "{http://xbrl.org/2014/table}dimension"
    ).text = (
        node.dimension
        if isinstance(node.dimension, str)
        else f"{node.dimension.concept.ns_prefix}:{node.dimension.concept.name}"
    )

    # Add the formulaAxis
    if node.formula_axis is not None:
        etree.SubElement(
            xml_table_node_element, "{http://xbrl.org/2014/table}formulaAxis"
        ).text = node.formula_axis

    yield Routeable_XML_Element(
        xml_namespace="linkbase",
        type="table_node",
        element=xml_table_node_element,
        file_path=linkbase_path,
        xml_parent_element=xml_parent_element,
    )

    ## Get Arc
    yield get_table_tree_arc(
        node=node, linkbase_path=linkbase_path, xml_parent_element=xml_parent_element
    )


class DimenensionRelationshipNode(object):
    pass


def loop_through_definition_node_subtree(
    breakdown, linkbase_path, xml_parent_element, linkrole, ep_path
):

    for any_node in [node for node in anytree.PreOrderIter(breakdown)]:

        if isinstance(any_node, RuleNode):
            for xml_element in get_xml_rule_node(
                any_node,
                linkbase_path=linkbase_path,
                xml_parent_element=xml_parent_element,
            ):
                yield xml_element

        elif isinstance(any_node, ConceptRelationshipNode):
            for xml_element in get_xml_relationship_node(
                any_node,
                linkbase_path=linkbase_path,
                xml_parent_element=xml_parent_element,
            ):
                yield xml_element

        elif isinstance(any_node, AspectNode):
            for xml_element in get_xml_aspect_node(
                any_node,
                linkbase_path=linkbase_path,
                xml_parent_element=xml_parent_element,
            ):
                yield xml_element

        elif isinstance(any_node, DimensionRelationshipNode):
            for xml_element in get_xml_dimensionrelationship_node(
                any_node,
                linkbase_path=linkbase_path,
                xml_parent_element=xml_parent_element,
            ):
                yield xml_element

        elif isinstance(any_node, Breakdown):
            pass  # Ignore the breakdown, this has already been written and depends on table info.
        else:
            print("")

        if hasattr(any_node, "labels") and any_node.xlink_label is not None:
            written_label_filepaths = []

            for label in any_node.labels.values():
                # todo: This should probably not be placed here in breakdown_tree, but in more generic place
                #  for writing generic labels/messages
                if label.linkRole in [
                    "http://www.xbrl.org/2010/role/message",
                    "http://www.xbrl.org/2010/role/verboseMessage",
                    "http://www.xbrl.org/2010/role/terseMessage",
                ]:
                    # todo: Not currently handling arcRoles like DynamicLabel. Do we even have this info in our model?
                    # todo: largely the same process as for generic labels; should be generalized.
                    msg_filepath = f"{conf.relative_root}/presentation/{conf.generic_info.get('domain')}-{linkrole.roletype_name}-generic-msg-{label.language}.xml"
                    if msg_filepath not in written_label_filepaths:
                        """First create the file itself"""
                        for xml_gen in get_generic_link(
                            roletype=linkrole,
                            ep_path=ep_path,
                            linkbase_path=msg_filepath,
                        ):
                            yield xml_gen

                        # <link:arcroleRef arcroleURI="http://www.nltaxonomie.nl/2017/arcrole/DynamicLabel" xlink:href="http://www.nltaxonomie.nl/2017/xbrl/sbr-arcroles.xsd#sbr-arc_DynamicLabel" xlink:type="simple"/>
                        yield Routeable_XML_Element(
                            xml_namespace="linkbase",
                            type="arcroleRef",
                            element=get_arcrole_element(
                                arcrole_uri="http://www.nltaxonomie.nl/2017/arcrole/DynamicLabel",
                                href="http://www.nltaxonomie.nl/2017/xbrl/sbr-arcroles.xsd#sbr-arc_DynamicLabel",
                            ),
                            file_path=msg_filepath,
                            xml_parent_element="{http://www.xbrl.org/2003/linkbase}linkbase",
                        )

                        # <link:roleRef roleURI="http://www.xbrl.org/2010/role/message" xlink:href="http://www.xbrl.org/2010/generic-message.xsd#standard-message" xlink:type="simple"/>
                        yield Routeable_XML_Element(
                            xml_namespace="linkbase",
                            type="roleRef",
                            element=get_xml_roleref(
                                roleURI=f"http://www.xbrl.org/2010/role/message",
                                ref_url="http://www.xbrl.org/2010/generic-message.xsd",
                                ref_id="standard-message",
                            ),
                            file_path=msg_filepath,
                            xml_parent_element="{http://www.xbrl.org/2003/linkbase}linkbase",
                        )
                        written_label_filepaths.append(msg_filepath)

                    xml_message = get_generic_message(
                        message=label,
                        filepath=msg_filepath,
                        linkrole_uri=linkrole.roleURI,
                    )
                    yield xml_message

                    yield get_dynamic_labelarc(
                        node_id=any_node.xlink_label,
                        message_id=label.xlink_label,
                        label_filepath=xml_message.file_path,
                        label_parent_element=xml_message.xml_parent_element,
                    )

                    rel_path = get_relative_path(
                        base=msg_filepath, relative=linkbase_path
                    )

                    yield loc_creator(
                        concept_id=any_node.xlink_label,
                        file_path_to=rel_path,
                        file_path=xml_message.file_path,
                        xml_parent_element=XML_Parent_element(
                            ns="gen", localname="link", role=linkrole.roleURI
                        ),
                    )

                elif label.linkRole in [
                    "http://www.xbrl.org/2008/role/label",
                    "http://www.xbrl.org/2008/role/verboseLabel",
                    "http://www.xbrl.org/2008/role/terseLabel",
                    "http://www.xbrl.org/2008/role/documentation",
                ]:

                    label_filepath = f"{conf.relative_root}/presentation/{conf.generic_info.get('domain')}-{linkrole.roletype_name}-generic-lab-{label.language}.xml"

                    if label_filepath not in written_label_filepaths:
                        """First create the file itself"""
                        for xml_gen in get_generic_link(
                            roletype=linkrole,
                            ep_path=ep_path,
                            linkbase_path=label_filepath,
                        ):
                            yield xml_gen

                        for xml_gen in linkrole_generic_labels(
                            linkbase_path=label_filepath
                        ):
                            yield xml_gen
                        written_label_filepaths.append(label_filepath)

                    xml_label = get_generic_label(
                        label=label,
                        filepath=label_filepath,
                        linkrole_uri=linkrole.roleURI,
                    )
                    yield xml_label

                    yield get_generic_labelarc(
                        concept_id=any_node.xlink_label,
                        routable_label_id=label.xlink_label,
                        label_filepath=xml_label.file_path,
                        label_parent_element=xml_label.xml_parent_element,
                    )

                    rel_path = get_relative_path(
                        base=label_filepath, relative=linkbase_path
                    )
                    yield loc_creator(
                        concept_id=any_node.xlink_label,
                        file_path_to=rel_path,
                        file_path=xml_label.file_path,
                        xml_parent_element=XML_Parent_element(
                            ns="gen", localname="link", role=linkrole.roleURI
                        ),
                    )
                else:
                    print("type of label/message unknown")
