from discover_xbrl.sbrnl_taxonomy_builder.buildtools.dts_comparison import (
    DTS_comparator,
)
from discover_xbrl.sbrnl_taxonomy_builder.config.conf import Config

from discover_xbrl.sbrnl_taxonomy_builder.linkbase.genericLinkbase import (
    get_generic_link,
)


from discover_xbrl.sbrnl_taxonomy_builder.specs.tables.build_table import built_table
from discover_xbrl.sbrnl_taxonomy_builder.specs.tables.presentation_hierarchy import (
    get_xml_presentation_hierarchy,
)

conf = Config()
dts_comparator = DTS_comparator()


def build_tables(dts):

    # Get static params
    relative_root = conf.relative_root

    for roletype in dts.directly_referenced_linkbases:
        tab_file_path = f"{conf.relative_root}/presentation/{conf.generic_info.get('domain')}-{roletype.roletype_name}-tab.xml"

        if (
            roletype.presentation_hierarchy is not None
            and roletype.roleURI != "http://www.xbrl.org/2008/role/link"
        ):

            differing_dts_list = dts_comparator.presentationlink_different_in_other_dts(
                linkrole=roletype
            )
            if differing_dts_list:
                # there are differences in the presentationlink hierarchy for this linkrole.
                # Use a different filename for this linkbase.
                pre_file_path = f"{conf.relative_root}/presentation/{conf.generic_info.get('domain')}-{roletype.roletype_name}-{dts.shortname}-pre.xml"
                tab_file_path = f"{conf.relative_root}/presentation/{conf.generic_info.get('domain')}-{roletype.roletype_name}-{dts.shortname}-tab.xml"

            else:
                pre_file_path = f"{conf.relative_root}/presentation/{conf.generic_info.get('domain')}-{roletype.roletype_name}-pre.xml"

            # Get presentation linkbase / hierarchy
            for xml_presentation_element in get_xml_presentation_hierarchy(
                ep_path=f"{relative_root}/entrypoints/{dts.entrypoint_name}",
                roletype=roletype,
                pre_file_path=pre_file_path,
            ):
                yield xml_presentation_element

        # Only attempt if there are actually tables present
        if len(roletype.tables) > 0 and tab_file_path is not None:

            # Create the <gen:link> for parameters.
            # This should probably be done somewhere else as we are not sure we are going to need this, but yeah.
            for xml_element in get_generic_link(
                roletype=dts.roletypes["http://www.xbrl.org/2008/role/link"],
                linkbase_path=f"{conf.relative_root}/presentation/{conf.generic_info.get('domain')}-generic-parameter.xml",
                ep_path=f"{relative_root}/entrypoints/{dts.entrypoint_name}",
            ):
                yield xml_element

            # Create the file itself by creating a generic link parent.
            for xml_presentation_link in get_generic_link(
                roletype=roletype,
                linkbase_path=tab_file_path,
                ep_path=f"{relative_root}/entrypoints/{dts.entrypoint_name}",
            ):
                yield xml_presentation_link

            ### Write a -tab.xml file per linkrole
            for table in roletype.tables:
                for xml_table in built_table(
                    linkbase_path=tab_file_path,
                    table=table,
                    linkrole=roletype,
                    ep_path=f"{relative_root}/entrypoints/{dts.entrypoint_name}",
                ):
                    yield xml_table
