import re

from lxml import etree

from discover_xbrl.sbrnl_taxonomy_builder.buildtools.classes import (
    Routeable_XML_Element,
)
from discover_xbrl.sbrnl_taxonomy_builder.buildtools.mem_concept_finder import (
    find_element_by_id,
)
from discover_xbrl.sbrnl_taxonomy_builder.buildtools.qname_store import QNameStore
from discover_xbrl.sbrnl_taxonomy_builder.config.conf import Config
from discover_xbrl.sbrnl_taxonomy_builder.linkbase.arcs.concept_to_concept import (
    loc_creator,
)

conf = Config()
qname_store = QNameStore()


def get_xml_table_parameter_arc(
    parameter_name,
    linkbase_path,
    xml_parent_element,
    from_table,
    to_loc_label,
    order=None,
):

    xml_table_parameter_arc = etree.Element(
        "{http://xbrl.org/2014/table}tableParameterArc"
    )

    xml_table_parameter_arc.set("name", parameter_name)
    if order is not None:
        xml_table_parameter_arc.set("order", order)
    xml_table_parameter_arc.set(
        "{http://www.w3.org/1999/xlink}arcrole",
        "http://xbrl.org/arcrole/2014/table-parameter",
    )
    xml_table_parameter_arc.set(
        "{http://www.w3.org/1999/xlink}type",
        "arc",
    )
    xml_table_parameter_arc.set(
        "{http://www.w3.org/1999/xlink}from",
        from_table,
    )
    xml_table_parameter_arc.set(
        "{http://www.w3.org/1999/xlink}to",
        to_loc_label,
    )

    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type="tableArc",
        xml_parent_element=xml_parent_element,
        file_path=linkbase_path,
        element=xml_table_parameter_arc,
    )


def get_xml_parameter_element(parameter):

    xml_parameter = etree.Element("{http://xbrl.org/2008/variable}parameter")
    xml_parameter.set("as", parameter.as_datatype)
    xml_parameter.set("id", parameter.xlink_label)
    xml_parameter.set("name", parameter.name)
    xml_parameter.set("{http://www.w3.org/1999/xlink}label", parameter.xlink_label)
    xml_parameter.set("required", str(parameter.required).lower())
    xml_parameter.set("select", parameter.select)
    xml_parameter.set(
        "{http://www.w3.org/1999/xlink}type",
        "resource",
    )
    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type="parameter",
        element=xml_parameter,
    )


def get_table_parameter(file_path, table_id, parameter, parent_link, order=None):

    # todo: This is an issue. Parameters with the same ID sometimes exist in other parts/domains of the taxonomy.
    #  in those cases, we want to create our own.
    #  for now this is 'solved' by looking for a filename with the domain in there. That is not a fool-proof way at all.
    rel_path = find_element_by_id(
        concept_id=parameter.id,
        reference_filepath=file_path,
        filenames=[f"*{conf.generic_info['domain']}*parameter*.xml"],
    )

    if not rel_path:

        # Parameter needs to be saved to its own file
        xml_parameter = get_xml_parameter_element(parameter=parameter)
        yield xml_parameter
        rel_path = ".." + re.sub(conf.relative_root, "", xml_parameter.file_path)

    to_loc = loc_creator(
        concept_id=parameter.xlink_label,
        file_path_to=rel_path,
        file_path=file_path,
        xml_parent_element=parent_link,
    )
    yield to_loc

    # Get arc
    yield get_xml_table_parameter_arc(
        parameter_name=parameter.name,
        from_table=table_id,
        linkbase_path=file_path,
        xml_parent_element=parent_link,
        to_loc_label=to_loc.element.get("{http://www.w3.org/1999/xlink}label"),
        order=order,
    )
