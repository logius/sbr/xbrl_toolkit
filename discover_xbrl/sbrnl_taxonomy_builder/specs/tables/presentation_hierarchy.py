import anytree

from discover_xbrl.sbrnl_taxonomy_builder.buildtools.qname_store import QNameStore
from discover_xbrl.sbrnl_taxonomy_builder.config.conf import Config
from discover_xbrl.sbrnl_taxonomy_builder.linkbase.arcs.concept_to_concept import (
    create_presentation_arc,
)
from discover_xbrl.sbrnl_taxonomy_builder.linkbase.presentationLinkbase import (
    get_presentation_link,
    parse_presentation_arc,
)

conf = Config()

qname_store = QNameStore()


def get_presentation_hierarchy(linkrole, file_path):

    for node in [
        node for node in anytree.PreOrderIter(linkrole.presentation_hierarchy)
    ]:
        if node.depth > 0:

            arc = create_presentation_arc(
                from_obj=node.parent.object,
                to_obj=node.object,
                arcrole="http://www.xbrl.org/2003/arcrole/parent-child",
                order=node.order,
            )

            if not qname_store.arc_exists_in_linkrole(
                linkrole=linkrole.roleURI,
                arc=arc,
            ):
                # add arc to qstore
                qname_store.add_arc_to_linkrole_store(
                    linkrole=linkrole.roleURI, arc=arc
                )

                for XML_element in parse_presentation_arc(
                    arc=arc,
                    linkbase_path=file_path,
                ):
                    yield XML_element


def get_xml_presentation_hierarchy(ep_path, roletype, pre_file_path):

    #### PRESENTATION HIERARCHY ####

    for presentation_link in get_presentation_link(
        roletype=roletype, linkbase_path=pre_file_path, ep_path=ep_path
    ):

        yield presentation_link

    for xml_element in get_presentation_hierarchy(
        linkrole=roletype, file_path=pre_file_path
    ):

        yield xml_element
