from lxml import etree
from discover_xbrl.sbrnl_taxonomy_builder.buildtools.classes import (
    Routeable_XML_Element,
)


def xml_variable_filter_arc(variable_id, filter, linkbase_path, xml_parent_element):
    xml_filter_element = etree.Element(
        "{http://xbrl.org/2008/variable}variableFilterArc"
    )

    xml_filter_element.set("complement", filter.arc_complement)
    xml_filter_element.set("cover", filter.arc_cover)
    xml_filter_element.set("order", str(filter.arc_order))
    xml_filter_element.set("priority", filter.arc_priority)
    xml_filter_element.set(
        "{http://www.w3.org/1999/xlink}arcrole",
        "http://xbrl.org/arcrole/2008/variable-filter",
    )
    xml_filter_element.set("{http://www.w3.org/1999/xlink}from", variable_id)
    xml_filter_element.set("{http://www.w3.org/1999/xlink}to", filter.xlink_label)
    xml_filter_element.set("{http://www.w3.org/1999/xlink}type", "arc")

    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type="arc",
        element=xml_filter_element,
        file_path=linkbase_path,
        xml_parent_element=xml_parent_element,
    )


def xml_variable_filter_set_arc(variable_id, filter, linkbase_path, xml_parent_element):
    xml_filter_element = etree.Element(
        "{http://xbrl.org/2008/variable}variableSetFilterArc"
    )

    xml_filter_element.set("complement", filter.arc_complement)
    xml_filter_element.set("order", filter.arc_order)
    xml_filter_element.set("priority", filter.arc_priority)
    xml_filter_element.set(
        "{http://www.w3.org/1999/xlink}arcrole",
        "http://xbrl.org/arcrole/2008/variable-set-filter",
    )
    xml_filter_element.set("{http://www.w3.org/1999/xlink}from", variable_id)
    xml_filter_element.set("{http://www.w3.org/1999/xlink}to", filter.xlink_label)
    xml_filter_element.set("{http://www.w3.org/1999/xlink}type", "arc")

    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type="arc",
        element=xml_filter_element,
        file_path=linkbase_path,
        xml_parent_element=xml_parent_element,
    )
