from lxml import etree

from discover_xbrl.sbrnl_taxonomy_builder.buildtools.classes import (
    Routeable_XML_Element,
)

from discover_xbrl.sbrnl_taxonomy_builder.linkbase.xml_elements.arcrole import (
    get_arcrole_element,
)
from discover_xbrl.sbrnl_taxonomy_builder.specs.formula.arcs import (
    xml_variable_filter_arc,
    xml_variable_filter_set_arc,
)
from discover_xbrl.sbrnl_taxonomy_builder.specs.formula.filter import (
    xml_fact_filter,
    xml_fact_filter_generator,
)

from discover_xbrl.sbrnl_taxonomy_builder.config.conf import Config


conf = Config()


def get_variable_arcroles(linkrole, linkbase_path):
    # Add an arcRole for domain member to lineitems file

    yield Routeable_XML_Element(
        xml_namespace="linkbase",
        type="arcroleRef",
        element=get_arcrole_element("http://xbrl.org/arcrole/2008/variable-filter"),
        file_path=linkbase_path,
    )

    yield Routeable_XML_Element(
        xml_namespace="linkbase",
        type="arcroleRef",
        element=get_arcrole_element("http://xbrl.org/arcrole/2008/variable-set-filter"),
        file_path=linkbase_path,
    )

    yield Routeable_XML_Element(
        xml_namespace="linkbase",
        type="arcroleRef",
        element=get_arcrole_element("http://xbrl.org/arcrole/2008/boolean-filter"),
        file_path=linkbase_path,
    )

    yield Routeable_XML_Element(
        xml_namespace="linkbase",
        type="arcroleRef",
        element=get_arcrole_element("http://xbrl.org/arcrole/2008/variable-set"),
        file_path=linkbase_path,
    )
    yield Routeable_XML_Element(
        xml_namespace="linkbase",
        type="arcroleRef",
        element=get_arcrole_element("http://xbrl.org/arcrole/2008/variable-filter"),
        file_path=linkbase_path,
    )


def xml_fact_variable(variable, linkbase_path, xml_parent_element, type_of_arc=None):
    xml_fact_variable_element = etree.Element(
        "{http://xbrl.org/2008/variable}factVariable"
    )

    variable = variable["variable"]  # unpack the actual variable object

    xml_fact_variable_element.set("bindAsSequence", variable.bind_as_sequence)
    if variable.fallback_value:
        xml_fact_variable_element.set("fallbackValue", variable.bind_as_sequence)
    # else:
    #     xml_fact_variable_element.set("fallbackValue", "()")
    xml_fact_variable_element.set("id", variable.xlink_label)
    xml_fact_variable_element.set("matches", variable.matches)
    xml_fact_variable_element.set("nils", variable.nils)
    xml_fact_variable_element.set("bindAsSequence", variable.bind_as_sequence)
    xml_fact_variable_element.set(
        "{http://www.w3.org/1999/xlink}label", variable.xlink_label
    )
    xml_fact_variable_element.set("{http://www.w3.org/1999/xlink}type", "resource")

    yield Routeable_XML_Element(
        xml_namespace="linkbase",
        type="variable",
        element=xml_fact_variable_element,
        file_path=linkbase_path,
        xml_parent_element=xml_parent_element,
    )

    for filter in variable.filters:

        yield xml_fact_filter(
            filter=filter,
            linkbase_path=linkbase_path,
            xml_parent_element=xml_parent_element,
        )

        for xml_element in xml_fact_filter_generator(
            filter=filter,
            linkbase_path=linkbase_path,
            xml_parent_element=xml_parent_element,
        ):
            yield xml_element

        if type_of_arc == "variable_filter":
            yield xml_variable_filter_arc(
                variable_id=variable.id,
                filter=filter,
                linkbase_path=linkbase_path,
                xml_parent_element=xml_parent_element,
            )

        elif type_of_arc == "variable_set_filter":
            yield xml_variable_filter_set_arc(
                variable_id=variable.id,
                filter=filter,
                linkbase_path=linkbase_path,
                xml_parent_element=xml_parent_element,
            )
