import logging
from lxml import etree

from discover_xbrl.sbrnl_taxonomy_builder.buildtools.classes import (
    Routeable_XML_Element,
)

from discover_xbrl.sbrnl_taxonomy_builder.config.conf import Config
from discover_xbrl.sbrnl_taxonomy_builder.linkbase.genericLinkbase import (
    get_generic_labels,
)

from discover_xbrl.sbrnl_taxonomy_builder.specs.formula.variable import (
    xml_fact_variable,
)
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.formulas.classes.assertion import (
    ExistenceAssertion,
    ValueAssertion,
)
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.formulas.classes.variables import (
    FactVariable,
)

conf = Config()
logger = logging.getLogger("builder.linkbase")


def build_assertions(linkrole, linkbase_path, parent_gen_link, ep_path):

    if linkrole.resources["variables"] and linkrole.roleURI not in [
        "http://www.xbrl.org/2008/role/link"
    ]:
        for assertion in linkrole.resources["assertions"]:

            if isinstance(assertion, ExistenceAssertion):
                for xml_assertion_element in xml_existence_assertion(
                    assertion=assertion,
                    linkbase_path=linkbase_path,
                    xml_parent_element=parent_gen_link,
                ):
                    yield xml_assertion_element
            elif isinstance(assertion, ValueAssertion):
                for xml_assertion_element in xml_value_assertion(
                    assertion=assertion,
                    linkbase_path=linkbase_path,
                    xml_parent_element=parent_gen_link,
                ):
                    yield xml_assertion_element

            if assertion.labels is not None and len(assertion.labels) > 0:
                for xml_elem in get_generic_labels(assertion.labels, linkrole, ep_path):
                    yield xml_elem


def xml_variable_arc(
    assertion_id,
    variable_id,
    variable_name,
    order,
    priority,
    linkbase_path,
    xml_parent_element,
):
    # yield the arc
    xml_assertion_arc_element = etree.Element(
        "{http://xbrl.org/2008/variable}variableArc"
    )
    xml_assertion_arc_element.set("name", f"varArc_{variable_name}")
    xml_assertion_arc_element.set("order", str(order))
    xml_assertion_arc_element.set("priority", str(priority))
    xml_assertion_arc_element.set(
        "{http://www.w3.org/1999/xlink}arcrole",
        "http://xbrl.org/arcrole/2008/variable-set",
    )
    xml_assertion_arc_element.set("{http://www.w3.org/1999/xlink}from", assertion_id)
    xml_assertion_arc_element.set("{http://www.w3.org/1999/xlink}to", variable_id)

    xml_assertion_arc_element.set("{http://www.w3.org/1999/xlink}type", "arc")

    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type="variable",
        element=xml_assertion_arc_element,
        file_path=linkbase_path,
        xml_parent_element=xml_parent_element,
    )


def xml_value_assertion(assertion, linkbase_path, xml_parent_element):
    xml_assertion_element = etree.Element(
        "{http://xbrl.org/2008/assertion/value}valueAssertion"
    )
    xml_assertion_element.set("aspectModel", assertion.aspectModel)
    xml_assertion_element.set("id", assertion.xlink_label)
    xml_assertion_element.set("implicitFiltering", assertion.implicitFiltering)
    xml_assertion_element.set("test", assertion.test)
    xml_assertion_element.set(
        "{http://www.w3.org/1999/xlink}label", assertion.xlink_label
    )
    xml_assertion_element.set("{http://www.w3.org/1999/xlink}type", "resource")

    for variableSet in assertion.variableSet:

        for variable in variableSet.variables:

            if isinstance(variable["variable"], FactVariable):
                for xml_fact_variable_element in xml_fact_variable(
                    variable=variable,
                    linkbase_path=linkbase_path,
                    xml_parent_element=xml_parent_element,
                    type_of_arc="variable_filter",
                ):

                    yield xml_fact_variable_element

                    if variable["variable"].id is None:
                        var_id = variable["variable"].xlink_label
                    else:
                        var_id = variable["variable"].id

                    yield xml_variable_arc(
                        assertion_id=assertion.id,
                        variable_id=var_id,
                        variable_name=variable["variable"].xlink_label.replace(
                            "factVariable_", ""
                        ),
                        order=variable["order"],
                        priority=variable["priority"],
                        linkbase_path=linkbase_path,
                        xml_parent_element=xml_parent_element,
                    )
            else:
                pass

    yield Routeable_XML_Element(
        xml_namespace="linkbase",
        type="assertion",
        element=xml_assertion_element,
        file_path=linkbase_path,
        xml_parent_element=xml_parent_element,
    )


def xml_existence_assertion(assertion, linkbase_path, xml_parent_element):
    xml_assertion_element = etree.Element(
        "{http://xbrl.org/2008/assertion/existence}existenceAssertion"
    )
    xml_assertion_element.set("aspectModel", assertion.aspectModel)
    xml_assertion_element.set("id", assertion.xlink_label)
    xml_assertion_element.set("implicitFiltering", assertion.implicitFiltering)
    if assertion.test:
        xml_assertion_element.set("test", assertion.test)
    xml_assertion_element.set(
        "{http://www.w3.org/1999/xlink}label", assertion.xlink_label
    )
    xml_assertion_element.set("{http://www.w3.org/1999/xlink}type", "resource")

    for variableSet in assertion.variableSet:

        for variable in variableSet.variables:
            if isinstance(variable["variable"], FactVariable):

                for xml_fact_variable_element in xml_fact_variable(
                    variable=variable,
                    linkbase_path=linkbase_path,
                    xml_parent_element=xml_parent_element,
                    type_of_arc="variable_filter",
                ):
                    yield xml_fact_variable_element

                    yield xml_variable_arc(
                        assertion_id=assertion.id,
                        variable_id=variable["variable"].id,
                        variable_name=variable["variable"].id.replace(
                            "factVariable_", ""
                        ),
                        order=variable["order"],
                        priority=variable["priority"],
                        linkbase_path=linkbase_path,
                        xml_parent_element=xml_parent_element,
                    )
            else:
                pass

    yield Routeable_XML_Element(
        xml_namespace="linkbase",
        type="assertion",
        element=xml_assertion_element,
        file_path=linkbase_path,
        xml_parent_element=xml_parent_element,
    )
