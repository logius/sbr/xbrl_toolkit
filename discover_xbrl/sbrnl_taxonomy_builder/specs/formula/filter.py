from lxml import etree
import logging
from discover_xbrl.sbrnl_taxonomy_builder.buildtools.classes import (
    Routeable_XML_Element,
)
from discover_xbrl.sbrnl_taxonomy_builder.linkbase.arcs.concept_to_concept import (
    write_concept,
)

from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.formulas.classes.filter_aspect_cover import (
    FilterAspectCover,
)
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.formulas.classes.filter_boolean import (
    FilterBooleanOr,
)
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.formulas.classes.filter_concept import (
    FilterConceptName,
    FilterConceptBalance,
    FilterConceptDatatype,
    FilterConceptSubstitutionGroup,
)

from discover_xbrl.sbrnl_taxonomy_builder.config.conf import Config
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.formulas.classes.filter_concept_relation import (
    FilterConceptRelation,
)
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.formulas.classes.filter_dimension import (
    FilterDimensionExplicit,
    FilterDimensionTyped,
)
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.formulas.classes.filter_period import (
    FilterPeriodInstantDuration,
    FilterPeriodStart,
    FilterPeriodEnd,
    FilterPeriodInstant,
)
from discover_xbrl.sbrnl_taxonomy_builder.buildtools.qname_store import QNameStore
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.formulas.classes.variables import (
    VariableSet,
)

qname_store = QNameStore()

conf = Config()
logger = logging.getLogger("builder.linkbase")


def xml_filter_concept_name(filter, linkbase_path, xml_parent_element):
    xml_filter_element = etree.Element(
        "{http://xbrl.org/2008/filter/concept}conceptName"
    )

    xml_filter_element.set("id", filter.xlink_label)
    xml_filter_element.set("{http://www.w3.org/1999/xlink}label", filter.xlink_label)
    xml_filter_element.set("{http://www.w3.org/1999/xlink}type", "resource")

    if filter.qname:
        xml_concept = etree.SubElement(
            xml_filter_element, "{http://xbrl.org/2008/filter/concept}concept"
        )
        # if isinstance(filter.qname, Concept):
        #     if filter.qname.ns_prefix is None:
        #         print("Qname prefix is None")
        #     etree.SubElement(
        #         xml_concept, "{http://xbrl.org/2008/filter/concept}qname"
        #     ).text = f"{filter.qname.ns_prefix}:{filter.qname.name}"

        # elif isinstance(filter.qname, str):
        # if filter.qname is None:
        #     print("Qname is None")
        etree.SubElement(
            xml_concept, "{http://xbrl.org/2008/filter/concept}qname"
        ).text = filter.qname

        # elif isinstance(filter.qname, list):
        #     for qname_concept in filter.qname:
        #
        #         etree.SubElement(
        #             xml_concept, "{http://xbrl.org/2008/filter/concept}qname"
        #         ).text = f"{qname_concept.ns_prefix}:{qname_concept.name}"

    if filter.qname_expression:
        print("TODO")

    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type="filter",
        element=xml_filter_element,
        file_path=linkbase_path,
        xml_parent_element=xml_parent_element,
    )


def xml_filter_concept_datatype(filter, linkbase_path, xml_parent_element):
    xml_filter_element = etree.Element(
        "{http://xbrl.org/2008/filter/concept}conceptDataType"
    )

    xml_filter_element.set("id", filter.xlink_label)
    xml_filter_element.set("strict", filter.strict)
    xml_filter_element.set("{http://www.w3.org/1999/xlink}label", filter.xlink_label)
    xml_filter_element.set("{http://www.w3.org/1999/xlink}type", "resource")

    xml_concept = etree.SubElement(
        xml_filter_element, "{http://xbrl.org/2008/filter/concept}type"
    )

    # for qname in filter.type_qname:
    etree.SubElement(
        xml_concept, "{http://xbrl.org/2008/filter/concept}qname"
    ).text = filter.type_qname

    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type="filter",
        element=xml_filter_element,
        file_path=linkbase_path,
        xml_parent_element=xml_parent_element,
    )


def xml_filter_concept_substitutiongroup(filter, linkbase_path, xml_parent_element):
    xml_filter_element = etree.Element(
        "{http://xbrl.org/2008/filter/concept}conceptSubstitutionGroup"
    )

    xml_filter_element.set("id", filter.xlink_label)
    xml_filter_element.set("strict", filter.strict)
    xml_filter_element.set("{http://www.w3.org/1999/xlink}label", filter.xlink_label)
    xml_filter_element.set("{http://www.w3.org/1999/xlink}type", "resource")

    if filter.type_qname:
        xml_concept = etree.SubElement(
            xml_filter_element, "{http://xbrl.org/2008/filter/concept}substitutionGroup"
        )

        # for qname_concept in filter.type_qname:

        member_qname = etree.SubElement(
            xml_concept, "{http://xbrl.org/2008/filter/concept}qname"
        )

        if isinstance(filter.type_qname, str):
            member_qname.text = filter.type_qname
        else:
            member_qname.text = f"{filter.type_qname.ns_prefix}:{filter.type_qname.id}"
    else:
        # TODO: check in parser how this could occur.
        logger.error(
            f"Missing Qname for concept substitutionGroupFilter: {filter.xlink_label}"
        )
        return None

    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type="filter",
        element=xml_filter_element,
        file_path=linkbase_path,
        xml_parent_element=xml_parent_element,
    )


def xml_filter_concept_relation(filter, linkbase_path, xml_parent_element):
    xml_filter_element = etree.Element(
        "{http://xbrl.org/2010/filter/concept-relation}conceptRelation"
    )

    xml_filter_element.set("id", filter.xlink_label)
    xml_filter_element.set("{http://www.w3.org/1999/xlink}label", filter.xlink_label)
    xml_filter_element.set("{http://www.w3.org/1999/xlink}type", "resource")

    etree.SubElement(
        xml_filter_element, "{http://xbrl.org/2010/filter/concept-relation}qname"
    ).text = (
        filter.qname
        if isinstance(filter.qname, str)
        else f"{filter.qname.ns_prefix}:{filter.qname.name}"
    )
    etree.SubElement(
        xml_filter_element, "{http://xbrl.org/2010/filter/concept-relation}linkrole"
    ).text = filter.linkrole.roleURI
    etree.SubElement(
        xml_filter_element, "{http://xbrl.org/2010/filter/concept-relation}arcrole"
    ).text = filter.arcrole
    etree.SubElement(
        xml_filter_element, "{http://xbrl.org/2010/filter/concept-relation}axis"
    ).text = str(filter.axis).lower()

    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type="filter",
        element=xml_filter_element,
        file_path=linkbase_path,
        xml_parent_element=xml_parent_element,
    )


def xml_filter_aspect_cover(filter, linkbase_path, xml_parent_element):
    xml_filter_element = etree.Element(
        "{http://xbrl.org/2010/filter/aspect-cover}aspectCover"
    )

    xml_filter_element.set("id", filter.xlink_label)
    xml_filter_element.set("{http://www.w3.org/1999/xlink}label", filter.xlink_label)
    xml_filter_element.set("{http://www.w3.org/1999/xlink}type", "resource")

    etree.SubElement(
        xml_filter_element, "{http://xbrl.org/2010/filter/aspect-cover}aspect"
    ).text = filter.aspect

    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type="filter",
        element=xml_filter_element,
        file_path=linkbase_path,
        xml_parent_element=xml_parent_element,
    )


def xml_filter_dimension_explicit(filter, linkbase_path, xml_parent_element):
    xml_filter_element = etree.Element(
        "{http://xbrl.org/2008/filter/dimension}explicitDimension"
    )

    xml_filter_element.set("id", filter.xlink_label)
    xml_filter_element.set("{http://www.w3.org/1999/xlink}label", filter.xlink_label)
    xml_filter_element.set("{http://www.w3.org/1999/xlink}type", "resource")

    # Add Dimension
    xml_dimension = etree.SubElement(
        xml_filter_element, "{http://xbrl.org/2008/filter/dimension}dimension"
    )
    # for dimension in filter.qname_dimension:
    #
    #     if isinstance(filter.qname_dimension, str):
    #         ns_prefix, concept_name = filter.qname_dimension.split(":", 1)
    #     else:
    #         ns_prefix = filter.qname_dimension.ns_prefix
    #         concept_name = filter.qname_dimension.name

    if filter.qname_dimension_obj.ns_prefix.startswith(conf.generic_info.get("domain")):
        # If the concept is in this domain, we need to also write it.
        if not qname_store.concept_exists(conceptName=filter.qname_dimension_obj.name):

            for xml_element in write_concept(
                concept=filter.qname_dimension_obj, linkbase_path=None, create_loc=True
            ):
                yield xml_element

    etree.SubElement(
        xml_dimension, "{http://xbrl.org/2008/filter/dimension}qname"
    ).text = filter.qname_dimension

    # Add Member
    if filter.qname_member:
        xml_member = etree.SubElement(
            xml_filter_element, "{http://xbrl.org/2008/filter/dimension}member"
        )
        # for member in filter.qname_member:
        #     if isinstance(member, str):
        #         ns_prefix, concept_name = member.split(":", 1)
        #     else:
        #         ns_prefix = member.ns_prefix
        #         concept_name = member.name

        if filter.qname_dimension_obj.ns_prefix.startswith(
            conf.generic_info.get("domain")
        ):
            if not qname_store.concept_exists(filter.qname_dimension_obj.name):

                for xml_element in write_concept(
                    concept=filter.qname_dimension_obj,
                    linkbase_path=None,
                    create_loc=True,
                ):
                    yield xml_element

        member_qname = etree.SubElement(
            xml_member, "{http://xbrl.org/2008/filter/dimension}qname"
        )

        member_qname.text = filter.qname_dimension

    yield Routeable_XML_Element(
        xml_namespace="linkbase",
        type="filter",
        element=xml_filter_element,
        file_path=linkbase_path,
        xml_parent_element=xml_parent_element,
    )


def xml_filter_dimension_typed(filter, linkbase_path, xml_parent_element):
    xml_filter_element = etree.Element(
        "{http://xbrl.org/2008/filter/dimension}typedDimension"
    )

    xml_filter_element.set("id", filter.xlink_label)
    xml_filter_element.set("{http://www.w3.org/1999/xlink}label", filter.xlink_label)
    xml_filter_element.set("{http://www.w3.org/1999/xlink}type", "resource")

    # Add Dimension
    xml_dimension = etree.SubElement(
        xml_filter_element, "{http://xbrl.org/2008/filter/dimension}dimension"
    )

    ns_prefix = filter.qname_dimension_obj.ns_prefix
    concept_name = filter.qname_dimension_obj.name

    if ns_prefix.startswith(conf.generic_info.get("domain")):
        # If the concept is in this domain, we need to also write it.
        if not qname_store.concept_exists(conceptName=concept_name):

            for xml_element in write_concept(
                concept=filter.qname_dimension_obj, linkbase_path=None, create_loc=True
            ):
                yield xml_element

    etree.SubElement(
        xml_dimension, "{http://xbrl.org/2008/filter/dimension}qname"
    ).text = filter.qname_dimension

    # Add Member if applicable
    if hasattr(filter, "qname_member"):
        xml_member = etree.SubElement(
            xml_filter_element, "{http://xbrl.org/2008/filter/dimension}member"
        )

        # for member in filter.qname_member:
        if filter.qname_member.ns_prefix.startswith(conf.generic_info.get("domain")):
            if not qname_store.concept_exists(filter.qname_member.name):

                for xml_element in write_concept(
                    concept=filter.qname_member_obj, linkbase_path=None, create_loc=True
                ):
                    yield xml_element

        etree.SubElement(
            xml_member, "{http://xbrl.org/2008/filter/dimension}qname"
        ).text = f"{filter.qname_member.ns_prefix}:{filter.qname_member.id}"

    yield Routeable_XML_Element(
        xml_namespace="linkbase",
        type="filter",
        element=xml_filter_element,
        file_path=linkbase_path,
        xml_parent_element=xml_parent_element,
    )


def xml_filter_concept_balance(filter, linkbase_path, xml_parent_element):
    xml_filter_element = etree.Element(
        "{http://xbrl.org/2008/filter/concept}conceptBalance"
    )

    xml_filter_element.set("balance", filter.balance_type)
    xml_filter_element.set("id", filter.xlink_label)
    xml_filter_element.set("{http://www.w3.org/1999/xlink}label", filter.xlink_label)
    xml_filter_element.set("{http://www.w3.org/1999/xlink}type", "resource")

    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type="filter",
        element=xml_filter_element,
        file_path=linkbase_path,
        xml_parent_element=xml_parent_element,
    )


def xml_filter_boolean(filter, linkbase_path, xml_parent_element):
    xml_filter_element = etree.Element("{http://xbrl.org/2008/filter/boolean}orFilter")

    xml_filter_element.set("id", filter.xlink_label)
    xml_filter_element.set("{http://www.w3.org/1999/xlink}label", filter.xlink_label)
    xml_filter_element.set("{http://www.w3.org/1999/xlink}type", "resource")

    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type="filter",
        element=xml_filter_element,
        file_path=linkbase_path,
        xml_parent_element=xml_parent_element,
    )


def xml_filter_period_instant_duration(filter, linkbase_path, xml_parent_element):
    xml_filter_element = etree.Element(
        "{http://xbrl.org/2008/filter/period}instantDuration"
    )

    xml_filter_element.set("boundary", filter.boundary)
    if isinstance(filter.variable, VariableSet):
        # If the variable is actually a set, refer to this set instead of a separate variable.
        variable = filter.variable.name
    else:
        variable = filter.variable
    xml_filter_element.set("variable", variable)

    xml_filter_element.set("id", filter.xlink_label)
    xml_filter_element.set("{http://www.w3.org/1999/xlink}label", filter.xlink_label)
    xml_filter_element.set("{http://www.w3.org/1999/xlink}type", "resource")

    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type="filter",
        element=xml_filter_element,
        file_path=linkbase_path,
        xml_parent_element=xml_parent_element,
    )


def xml_filter_period_start(filter, linkbase_path, xml_parent_element):
    xml_filter_element = etree.Element(
        "{http://xbrl.org/2008/filter/period}periodStart"
    )

    xml_filter_element.set("date", filter.date)

    xml_filter_element.set("id", filter.xlink_label)
    xml_filter_element.set("{http://www.w3.org/1999/xlink}label", filter.xlink_label)
    xml_filter_element.set("{http://www.w3.org/1999/xlink}type", "resource")

    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type="filter",
        element=xml_filter_element,
        file_path=linkbase_path,
        xml_parent_element=xml_parent_element,
    )


def xml_filter_period_end(filter, linkbase_path, xml_parent_element):
    xml_filter_element = etree.Element("{http://xbrl.org/2008/filter/period}periodEnd")

    xml_filter_element.set("date", filter.date)

    xml_filter_element.set("id", filter.xlink_label)
    xml_filter_element.set("{http://www.w3.org/1999/xlink}label", filter.xlink_label)
    xml_filter_element.set("{http://www.w3.org/1999/xlink}type", "resource")

    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type="filter",
        element=xml_filter_element,
        file_path=linkbase_path,
        xml_parent_element=xml_parent_element,
    )


def xml_filter_period_instant(filter, linkbase_path, xml_parent_element):
    xml_filter_element = etree.Element(
        "{http://xbrl.org/2008/filter/period}periodInstant"
    )

    xml_filter_element.set("date", filter.date)

    xml_filter_element.set("id", filter.xlink_label)
    xml_filter_element.set("{http://www.w3.org/1999/xlink}label", filter.xlink_label)
    xml_filter_element.set("{http://www.w3.org/1999/xlink}type", "resource")

    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type="filter",
        element=xml_filter_element,
        file_path=linkbase_path,
        xml_parent_element=xml_parent_element,
    )


def xml_fact_filter(filter, linkbase_path, xml_parent_element):

    if isinstance(filter, FilterConceptName):
        return xml_filter_concept_name(
            filter=filter,
            linkbase_path=linkbase_path,
            xml_parent_element=xml_parent_element,
        )

    elif isinstance(filter, FilterConceptDatatype):
        return xml_filter_concept_datatype(
            filter=filter,
            linkbase_path=linkbase_path,
            xml_parent_element=xml_parent_element,
        )
    elif isinstance(filter, FilterConceptSubstitutionGroup):
        return xml_filter_concept_substitutiongroup(
            filter=filter,
            linkbase_path=linkbase_path,
            xml_parent_element=xml_parent_element,
        )

    elif isinstance(filter, FilterBooleanOr):
        return xml_filter_boolean(
            filter=filter,
            linkbase_path=linkbase_path,
            xml_parent_element=xml_parent_element,
        )
    elif isinstance(filter, FilterConceptBalance):
        return xml_filter_concept_balance(
            filter=filter,
            linkbase_path=linkbase_path,
            xml_parent_element=xml_parent_element,
        )

    elif isinstance(filter, FilterConceptRelation):
        return xml_filter_concept_relation(
            filter=filter,
            linkbase_path=linkbase_path,
            xml_parent_element=xml_parent_element,
        )

    elif isinstance(filter, FilterDimensionExplicit):
        pass  # done on xml_fact_filter_generator() because additional Concepts can be yielded

    elif isinstance(filter, FilterDimensionTyped):
        pass  # done on xml_fact_filter_generator() because additional Concepts can be yielded.

    elif isinstance(filter, FilterAspectCover):
        return xml_filter_aspect_cover(
            filter=filter,
            linkbase_path=linkbase_path,
            xml_parent_element=xml_parent_element,
        )

    elif isinstance(filter, FilterPeriodInstantDuration):
        return xml_filter_period_instant_duration(
            filter=filter,
            linkbase_path=linkbase_path,
            xml_parent_element=xml_parent_element,
        )
    elif isinstance(filter, FilterPeriodStart):
        return xml_filter_period_start(
            filter=filter,
            linkbase_path=linkbase_path,
            xml_parent_element=xml_parent_element,
        )
    elif isinstance(filter, FilterPeriodEnd):
        return xml_filter_period_start(
            filter=filter,
            linkbase_path=linkbase_path,
            xml_parent_element=xml_parent_element,
        )
    elif isinstance(filter, FilterPeriodInstant):
        return xml_filter_period_instant(
            filter=filter,
            linkbase_path=linkbase_path,
            xml_parent_element=xml_parent_element,
        )

    else:
        print("")


def xml_fact_filter_generator(filter, linkbase_path, xml_parent_element):

    if isinstance(filter, FilterDimensionExplicit):
        for xml_element in xml_filter_dimension_explicit(
            filter=filter,
            linkbase_path=linkbase_path,
            xml_parent_element=xml_parent_element,
        ):
            yield xml_element

    elif isinstance(filter, FilterDimensionTyped):
        for xml_element in xml_filter_dimension_typed(
            filter=filter,
            linkbase_path=linkbase_path,
            xml_parent_element=xml_parent_element,
        ):
            yield xml_element
