from discover_xbrl.sbrnl_taxonomy_builder.buildtools.classes import XML_Parent_element
from discover_xbrl.sbrnl_taxonomy_builder.config.conf import Config
from discover_xbrl.sbrnl_taxonomy_builder.linkbase.genericLinkbase import (
    get_generic_link,
)
from discover_xbrl.sbrnl_taxonomy_builder.specs.formula.assertion import (
    build_assertions,
)
from discover_xbrl.sbrnl_taxonomy_builder.specs.formula.variable import (
    get_variable_arcroles,
)

conf = Config()


def build_formulas(dts):
    for linkrole in dts.directly_referenced_linkbases:

        if linkrole.resources["variables"] and linkrole.roleURI not in [
            "http://www.xbrl.org/2008/role/link"
        ]:

            linkbase_path = f"{conf.relative_root}/validation/{conf.generic_info.get('domain')}-{linkrole.roletype_name}-for.xml"

            # Create the Generic Link to put everything into
            for xml_element in get_generic_link(
                roletype=linkrole,
                linkbase_path=linkbase_path,
                ep_path=f"{conf.relative_root}/entrypoints/{dts.entrypoint_name}",
            ):
                yield xml_element

            # Create an XML parent element to be passed around, referencing the just created generic link.
            parent_gen_link = XML_Parent_element(
                ns="gen",
                localname="link",
                role=linkrole.roleURI,
            )

            # get arcroles
            for xml_arcrole in get_variable_arcroles(
                linkrole=linkrole, linkbase_path=linkbase_path
            ):
                yield xml_arcrole

            for xml_assertion_element in build_assertions(
                linkrole=linkrole,
                linkbase_path=linkbase_path,
                parent_gen_link=parent_gen_link,
                ep_path=f"{conf.relative_root}/entrypoints/{dts.entrypoint_name}",
            ):
                yield xml_assertion_element
