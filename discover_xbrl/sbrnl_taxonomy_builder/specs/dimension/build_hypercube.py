import logging

from discover_xbrl.sbrnl_taxonomy_builder.buildtools.classes import (
    Routeable_XML_Element,
    XML_Parent_element,
)
from discover_xbrl.sbrnl_taxonomy_builder.config.conf import Config
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.dimensions.classes import (
    DimensionalArc,
)
from ...buildtools.dts_comparison import DTS_comparator
from ...buildtools.mem_file_reader import file_exists
from ...linkbase.arcs.concept_to_concept import (
    arcWriter_concept_to_concept,
    arcWriter,
    write_concept,
)
from ...linkbase.definitionLinkbase import get_definition_link
from discover_xbrl.sbrnl_taxonomy_builder.linkbase.xml_elements.roleref import (
    get_linkrole_roleref,
)
from ...linkbase.xml_elements.arcrole import get_arcrole_element

conf = Config()


from discover_xbrl.sbrnl_taxonomy_builder.buildtools.qname_store import QNameStore

qname_store = QNameStore()


def create_dimensional_arc(
    from_obj, to_obj, arcrole, order=None, usable=None, target_linkrole=None
):
    """
    Creates an arc of the given arcrole

    :return:
    """

    arc = DimensionalArc(
        tag="{http://www.xbrl.org/2003/linkbase}definitionArc",
        target_role=target_linkrole,
        arcrole=arcrole,
        from_obj=from_obj,
        to_obj=to_obj,
    )
    if order is not None:
        arc.order = order

    if usable is not None:
        if isinstance(usable, str):
            arc.usable = str
        elif usable == True:
            arc.usable = "true"
        elif usable == False:
            arc.usable = "false"
        else:
            # Default option?
            arc.usable = "true"

    if target_linkrole is not None:

        arc.target_role = target_linkrole

    return arc


def create_lineitem_table_arc(
    from_obj, to_obj, hc_type, context_element, closed=None, order=None, targetrole=None
):
    """
    Creates an arc for lineitem-table

    :return:
    """

    attrs = {
        "{http://www.w3.org/1999/xlink}from": from_obj.id,
        "{http://www.w3.org/1999/xlink}to": to_obj.id,
    }

    if hc_type == "all":
        arcrole = "http://xbrl.org/int/dim/arcrole/all"
    elif hc_type == "notAll":
        arcrole = "http://xbrl.org/int/dim/arcrole/notAll"

    arc = DimensionalArc(
        tag="{http://www.xbrl.org/2003/linkbase}definitionArc",
        from_obj=from_obj,
        to_obj=to_obj,
        arcrole=arcrole,
        context_element=context_element,
    )

    if closed is not None:
        # If the optional attribute @closed is set, add this to the arc.
        arc.closed = closed
    else:
        # 2.03.06.04: De arcrole all (has hypercube) MOET @xbrldt:closed='true' bevatten
        arc.closed = "true"

    return arc


def lineitems(
    linkrole,
    linkrole_name,
    entrypoint_name,
    ep_shortname,
    validation_lineitems_obj,
    ep_path,
):
    # First make sure there is a parent for the lineitems to be added to.
    # Get static params
    domain = conf.generic_info.get("domain")

    dts_comparator = DTS_comparator()
    if dts_comparator.lineitems_different_in_other_dts(
        linkrole=linkrole, current_ep_suffix=ep_shortname
    ):
        # Create a separate file if there are conflicting lineitem orders/lists.
        linkbase_lineitems_path = f"{conf.relative_root}/validation/{domain}-{linkrole_name}-{ep_shortname}-lineitems-def.xml"
    else:
        linkbase_lineitems_path = f"{conf.relative_root}/validation/{domain}-{linkrole_name}-lineitems-def.xml"

    # If the file does not exist, we need to create it first
    # We do that here by yielding a definition link, which automatically uses a stub to create a new file.
    # It also creates a reference automatically from the EP.

    for XML_element in get_definition_link(
        roletype=linkrole,
        linkbase_path=linkbase_lineitems_path,
        ep_path=ep_path,
    ):
        yield XML_element

    lineitems = linkrole.hypercube.lineItems

    if lineitems is not None:

        # Add an arcRole for domain member to lineitems file
        yield Routeable_XML_Element(
            xml_namespace="linkbase",
            type="arcroleRef",
            element=get_arcrole_element(
                "http://xbrl.org/int/dim/arcrole/domain-member"
            ),
            file_path=linkbase_lineitems_path,
        )

        written_concepts = []

        # check if there are any duplicates in lineitems
        found_lineitems = []
        for lineitem in lineitems:
            if lineitem[1] not in found_lineitems:
                found_lineitems.append(lineitem[1])
            else:
                logging.warning(
                    f"duplicate lineitem: '{lineitem[1]}' in linkrole: '{linkrole_name}'"
                )

        # Start processing all lineitems
        for lineitem in lineitems:

            arc_domain_member = create_dimensional_arc(
                from_obj=validation_lineitems_obj,
                to_obj=lineitem[1],
                arcrole="http://xbrl.org/int/dim/arcrole/domain-member",
                order=lineitem[0],
            )

            if not qname_store.arc_exists_in_linkrole(
                linkrole=linkrole.roleURI,
                arc=arc_domain_member,
            ):
                # add arc to qstore
                qname_store.add_arc_to_linkrole_store(
                    linkrole=linkrole, arc=arc_domain_member
                )

                yield arcWriter(
                    arc=arc_domain_member,
                    linkbase_path=linkbase_lineitems_path,
                    xml_parent_element=XML_Parent_element(
                        ns="link",
                        localname="definitionLink",
                        role=linkrole.roleURI,
                    ),
                    arc_type="definitionArc",
                )
                for concept in [
                    arc_domain_member.from_concept_obj,
                    arc_domain_member.to_concept_obj,
                ]:

                    if concept.id not in written_concepts:
                        written_concepts.append(concept.id)
                        # todo: check if concept has already been written?
                        # if not qname_store.concept_exists(concept.name):
                        #     # Put the concept in the Qstore so we do not need to make it in the future
                        #     qname_store.conceptStore.append(concept.name)

                        for xml_concept in write_concept(
                            concept=concept,
                            xml_parent_element=XML_Parent_element(
                                ns="link",
                                localname="definitionLink",
                                role=linkrole.roleURI,
                            ),
                            # xml_parent_element="{http://www.xbrl.org/2003/linkbase}definitionLink",
                            linkbase_path=linkbase_lineitems_path,
                        ):
                            yield xml_concept

                # for xml_element in arcWriter_concept_to_concept(
                #     arc=arc_domain_member,
                #     linkbase_path=linkbase_lineitems_path,
                #     linkbase_type="{http://www.xbrl.org/2003/linkbase}definitionLink",
                # ):
                #     yield xml_element


def loop_domain_members(
    members: list, parent, target_linkrole_uri: str, linkbase_path: str
):

    for i, domain_member in enumerate(members, start=1):

        # Write the 'to' object
        for xml_concept in write_concept(
            concept=parent,
            xml_parent_element=XML_Parent_element(
                ns="link",
                localname="definitionLink",
                role=target_linkrole_uri,
            ),
            linkbase_path=linkbase_path,
        ):
            yield xml_concept

        arc = create_dimensional_arc(
            from_obj=parent.concept,
            to_obj=domain_member.concept,
            order=domain_member.order,
            arcrole="http://xbrl.org/int/dim/arcrole/domain-member",
            usable=domain_member.usable,
        )

        yield arcWriter(
            arc=arc,
            linkbase_path=linkbase_path,
            xml_parent_element=XML_Parent_element(
                ns="link",
                localname="definitionLink",
                role=target_linkrole_uri,
            ),
        )

        yield Routeable_XML_Element(
            xml_namespace="linkbase",
            type="arcroleRef",
            element=get_arcrole_element(
                "http://xbrl.org/int/dim/arcrole/domain-member"
            ),
            file_path=linkbase_path,
        )
        loop_domain_members(
            members=domain_member.children,
            target_linkrole_uri=target_linkrole_uri,
            linkbase_path=linkbase_path,
        )


def dimension_domain(dimension, relative_root, ep_path):

    # The linkrole URI which describes the dimension/domain.

    # Dimension has no typedDomainRef so it is an explicit member
    if (
        dimension.concept.typedDomainRef_obj is None
        and dimension.concept.typedDomainRef is None
    ):
        linkbase_path = f"{relative_root}/validation/{conf.generic_info.get('domain')}-{dimension.target_linkrole.roletype_name}-def.xml"

        # Create a definition link for the linkrole
        for XML_element in get_definition_link(
            roletype=dimension.target_linkrole,
            linkbase_path=linkbase_path,
            ep_path=ep_path,
        ):
            yield XML_element

        # Yield an arcrole to describe what we are trying to do in this linkrole
        yield Routeable_XML_Element(
            xml_namespace="linkbase",
            type="arcroleRef",
            element=get_arcrole_element(
                "http://xbrl.org/int/dim/arcrole/dimension-domain"
            ),
            file_path=linkbase_path,
        )

        # Add a roleRef per Dimension.
        for XML_linkroleref in get_linkrole_roleref(
            roletype=dimension.target_linkrole,
            linkbase_path=linkbase_path,
        ):
            yield XML_linkroleref

        written_members = []

        for i, member in enumerate(dimension.children, start=1):

            dimension_domain_arc = create_dimensional_arc(
                from_obj=dimension.concept,
                to_obj=member.concept,
                arcrole="http://xbrl.org/int/dim/arcrole/dimension-domain",
                usable=member.usable,
                #  target_linkrole=dimension.target_linkrole,
            )
            if hasattr(member, "order"):
                dimension_domain_arc.order = member.order
            else:
                dimension_domain_arc.order = i

            # Make Arc itself.
            yield arcWriter(
                dimension_domain_arc,
                linkbase_path,
                XML_Parent_element(
                    ns="link",
                    localname="definitionLink",
                    role=dimension.target_linkrole.roleURI,
                ),
            )

            # Make Concept and Locators for both the 'from' and 'to' side of the Arc
            for concept in [
                dimension_domain_arc.from_concept_obj,
                dimension_domain_arc.to_concept_obj,
            ]:
                if concept.id not in written_members:
                    written_members.append(concept.id)

                    # todo: check if concept has already been written in other ELR?

                    for xml_concept in write_concept(
                        concept=concept,
                        xml_parent_element=XML_Parent_element(
                            ns="link",
                            localname="definitionLink",
                            role=dimension.target_linkrole.roleURI,
                        ),
                        linkbase_path=linkbase_path,
                    ):
                        yield xml_concept

            # get domain-members
            loop_domain_members(
                members=dimension.children,
                target_linkrole_uri=dimension.target_linkrole.roleURI,
                linkbase_path=linkbase_path,
                parent=dimension,
            )

    else:
        # Element is a typed member

        # we still need to write the Typed Member and dimension
        for xml_concept in write_concept(
            concept=dimension.concept.typedDomainRef_obj,
            create_loc=False,
        ):
            yield xml_concept


def build_hypercube(
    linkrole,
    relative_root,
    entrypoint_name,
    namespaces,
    ep_shortname,
    ep_path,
    validation_lineitems_obj=None,
    validation_lineitems_table_obj=None,
):

    dts_comparator = DTS_comparator()

    differences = dts_comparator.dimensions_different_in_other_dts(
        linkrole=linkrole, current_ep_suffix=ep_shortname
    )
    if differences:

        # We need to do something to create separate files. See:
        #  https://gitlab.com/logius/kenniscentrumxbrl/-/issues/24

        linkbase_path = f"{relative_root}/validation/{conf.generic_info.get('domain')}-{linkrole.roletype_name}-{ep_shortname}-def.xml"
    else:
        linkbase_path = f"{relative_root}/validation/{conf.generic_info.get('domain')}-{linkrole.roletype_name}-def.xml"

    linkrole_name = linkrole.roletype_name
    hypercube_type = linkrole.hypercube.hc_type

    ###
    # LINEITEMS
    ###

    # If the file does not exist, we need to create it first
    # We do that here by yielding a definition link, which automatically uses a stub to create a new file.
    # It also creates a reference automatically from the EP.

    if not file_exists(linkbase_path):
        for definition_link in get_definition_link(
            roletype=linkrole,
            linkbase_path=linkbase_path,
            ep_path=f"{relative_root}/entrypoints/{entrypoint_name}",
        ):
            yield definition_link

    for XML_element in lineitems(
        linkrole=linkrole,
        linkrole_name=linkrole_name,
        ep_shortname=ep_shortname,
        entrypoint_name=entrypoint_name,
        validation_lineitems_obj=validation_lineitems_obj,
        ep_path=ep_path,
    ):
        yield XML_element

    ###
    # DIMENSIONS
    ###

    # Loop through all dimensions
    for i, dimension in enumerate(linkrole.hypercube.dimensions):

        """
        Write the concept to concept arc to the taxonomy.

        Locators are automatically created.

        If the @from and @to objects do not exist, these are automatically written to the taxonomy as well.
        """

        ###
        # DOMAIN-MEMBER
        #
        # Writing these before Dimensions, because for typedDim's we expect members to be available
        # when creating the Dim Concept and its typedDomainRef attr
        ###

        for xml_domain_member_element in dimension_domain(
            dimension=dimension,
            relative_root=relative_root,
            ep_path=ep_path,
        ):
            yield xml_domain_member_element

        arc = create_dimensional_arc(
            from_obj=validation_lineitems_table_obj,
            to_obj=dimension.concept,
            arcrole="http://xbrl.org/int/dim/arcrole/hypercube-dimension",
            target_linkrole=dimension.target_linkrole
            # order=i,
        )

        # Only add the arc if it does not exist
        if not qname_store.arc_exists_in_linkrole(
            linkrole=linkrole.roleURI,
            arc=arc,
        ):
            # add arc to qstore
            qname_store.add_arc_to_linkrole_store(linkrole=linkrole, arc=arc)

            for xml_element in arcWriter_concept_to_concept(
                arc=arc,
                linkbase_path=linkbase_path,
                xml_parent_element=XML_Parent_element(
                    ns="link", localname="definitionLink", role=linkrole.roleURI
                )
                # linkbase_type="{http://www.xbrl.org/2003/linkbase}definitionLink",
            ):

                yield xml_element

    ###
    # HYPERCUBE
    ###

    # Add arcRoles
    if hypercube_type == "all":

        yield Routeable_XML_Element(
            xml_namespace="linkbase",
            type="arcroleRef",
            element=get_arcrole_element("http://xbrl.org/int/dim/arcrole/all"),
            file_path=linkbase_path,
        )

    elif hypercube_type == "notAll":
        yield Routeable_XML_Element(
            xml_namespace="linkbase",
            type="arcroleRef",
            element=get_arcrole_element("http://xbrl.org/int/dim/arcrole/notAll"),
            file_path=linkbase_path,
        )

    yield Routeable_XML_Element(
        xml_namespace="linkbase",
        type="arcroleRef",
        element=get_arcrole_element(
            "http://xbrl.org/int/dim/arcrole/hypercube-dimension"
        ),
        file_path=linkbase_path,
    )

    # Add rolerefs

    # Yield a roleRef to the linkrole if it exists, or create it if it doesn't
    for XML_linkroleref in get_linkrole_roleref(
        roletype=linkrole,
        linkbase_path=linkbase_path,
    ):
        yield XML_linkroleref

    for dimension in linkrole.hypercube.dimensions:
        # Add a roleref for the dimension
        if dimension.target_linkrole:
            for XML_linkroleref in get_linkrole_roleref(
                roletype=dimension.target_linkrole,
                linkbase_path=linkbase_path,
            ):
                yield XML_linkroleref

    # Add the link from lineitems to validation table as well
    # This arc gets the order of dimensions + 1

    arc_lineitem_table = create_lineitem_table_arc(
        from_obj=validation_lineitems_obj,
        to_obj=validation_lineitems_table_obj,
        hc_type=hypercube_type,
        context_element=linkrole.hypercube.context_element,
    )

    if not qname_store.arc_exists_in_linkrole(
        linkrole=linkrole.roleURI,
        arc=arc_lineitem_table,
    ):
        # add arc to qstore
        qname_store.add_arc_to_linkrole_store(
            linkrole=linkrole,
            arc=arc_lineitem_table,
        )
        for xml_element in arcWriter_concept_to_concept(
            arc=arc_lineitem_table,
            linkbase_path=linkbase_path,
            xml_parent_element=XML_Parent_element(
                ns="link",
                localname="definitionLink",
                role=linkrole.roleURI,
            ),
        ):
            yield xml_element
