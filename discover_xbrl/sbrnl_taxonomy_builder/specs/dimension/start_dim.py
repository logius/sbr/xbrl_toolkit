from discover_xbrl.sbrnl_taxonomy_builder.config.conf import Config
from discover_xbrl.sbrnl_taxonomy_builder.specs.dimension.build_hypercube import (
    build_hypercube,
)

conf = Config()


def build_hypercubes(dts):

    # Get static params

    relative_root = conf.relative_root
    namespaces = conf.namespaces

    for linkrole in dts.directly_referenced_linkbases:
        # get hypercubes per linkrole
        if linkrole.hypercube:
            for hc in build_hypercube(
                linkrole=linkrole,
                relative_root=relative_root,
                namespaces=namespaces,
                entrypoint_name=dts.entrypoint_name,
                ep_shortname=dts.shortname,
                validation_lineitems_obj=dts.concepts["sbr-dim_ValidationLineItems"],
                validation_lineitems_table_obj=dts.concepts["sbr-dim_ValidationTable"],
                ep_path=f"{relative_root}/entrypoints/{dts.entrypoint_name}",
            ):
                yield hc
