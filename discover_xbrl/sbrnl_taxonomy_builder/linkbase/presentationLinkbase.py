import logging

from lxml import etree

from .arcs.concept_to_concept import arcWriter_concept_to_concept
from ..schema.create_linkrole import get_linkroleref
from ..buildtools.classes import Routeable_XML_Element, XML_Parent_element

from discover_xbrl.sbrnl_taxonomy_builder.linkbase.xml_elements.roleref import (
    get_linkrole_roleref,
)

logger = logging.getLogger("builder.linkbase")

from discover_xbrl.sbrnl_taxonomy_builder.config.conf import Config

conf = Config()


def create_presentation_linkbase(hierarchy):

    pass


def get_presentation_link(roletype, ep_path, linkbase_path):

    for xml_roleref in get_linkrole_roleref(
        roletype=roletype,
        linkbase_path=linkbase_path,
    ):
        yield xml_roleref

    # Yield a linkbase reference into the EP
    yield Routeable_XML_Element(
        xml_namespace="schema",
        type="linkbaseref",
        element=get_linkroleref(
            role="http://www.xbrl.org/2003/role/presentationLinkbaseRef",
            path=linkbase_path,
            relative_root=conf.relative_root,
        ),
        file_path=ep_path,
    )

    # Continue by adding the presentationLink itself
    xml_presentationLink = etree.Element(
        "{http://www.xbrl.org/2003/linkbase}presentationLink"
    )

    xml_presentationLink.set("{http://www.w3.org/1999/xlink}role", roletype.roleURI)
    xml_presentationLink.set("{http://www.w3.org/1999/xlink}type", "extended")

    # Yield the definitionLink Element
    yield Routeable_XML_Element(
        xml_namespace="linkbase",
        type="presentationLink",
        element=xml_presentationLink,
        file_path=linkbase_path,
        xml_parent_element="{http://www.xbrl.org/2003/linkbase}linkbase",
    )


def parse_presentation_arc(arc, linkbase_path):
    """
    Handles parsing a presentation arc. Yields any kind of XML element that has to be written to a file.

    :return:
    """

    if arc.from_concept_obj and arc.to_concept_obj:

        if arc.arcrole in [
            "http://www.xbrl.org/2003/arcrole/parent-child",
        ]:  # Concept to Concept

            for xml_concept in arcWriter_concept_to_concept(
                arc=arc,
                linkbase_path=linkbase_path,
                xml_parent_element=XML_Parent_element(
                    ns="link", localname="presentationLink"
                ),
                # linkbase_type="{http://www.xbrl.org/2003/linkbase}presentationLink",
            ):
                yield xml_concept
        else:
            print(f"Arcrole {arc.arcrole} not understood in parse_presentation_arc()")
