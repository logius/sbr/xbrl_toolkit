import logging
import re
from lxml import etree

from discover_xbrl.sbrnl_xbrl_parser.linkbase.classes import Arc
from discover_xbrl.sbrnl_xbrl_parser.schema.classes import Concept
from ..labelLinkbase import get_labels, create_labelLink
from discover_xbrl.sbrnl_taxonomy_builder.linkbase.xml_elements.locator import (
    loc_creator,
)
from ..referenceLinkbase import get_xml_references

from ...buildtools.classes import Routeable_XML_Element
from ...buildtools.mem_concept_finder import (
    find_element_by_id,
    find_element_by_name,
    file_exists,
    find_itemtype,
)
from ...buildtools.mem_file_reader import get_relative_path
from ...buildtools.mem_file_writer import create_directory
from ...buildtools.qname_store import QNameStore
from ...schema.create_linkrole import get_linkroleref

logger = logging.getLogger("builder.linkbase")

from discover_xbrl.sbrnl_taxonomy_builder.config.conf import Config

conf = Config()
qname_store = QNameStore()

"""
Functions to write concepts, along with arc's and loc's to the taxonomy.
"""


def createItemtype(itemType):
    xml_complexType = etree.Element("{http://www.w3.org/2001/XMLSchema}complexType")
    xml_complexType.set("id", f"{itemType.id}")
    xml_complexType.set("name", f"{itemType.name}")

    xml_simpleContent = etree.Element("{http://www.w3.org/2001/XMLSchema}simpleContent")
    xml_restriction = etree.Element("{http://www.w3.org/2001/XMLSchema}restriction")

    xml_restriction.set("base", itemType.base)

    if itemType.restriction_length:
        xml_restriction.append(
            etree.Element(
                "{http://www.w3.org/2001/XMLSchema}length",
                value=str(itemType.restriction_length),
            )
        )

    if itemType.restriction_min_length:
        xml_restriction.append(
            etree.Element(
                "{http://www.w3.org/2001/XMLSchema}minLength",
                value=str(itemType.restriction_min_length),
            )
        )

    if itemType.restriction_max_length:
        xml_restriction.append(
            etree.Element(
                "{http://www.w3.org/2001/XMLSchema}maxLength",
                value=str(itemType.restriction_max_length),
            )
        )

    if itemType.totalDigits:
        xml_restriction.append(
            etree.Element(
                "{http://www.w3.org/2001/XMLSchema}totalDigits",
                value=str(itemType.totalDigits),
            )
        )

    if itemType.restriction_pattern:
        xml_restriction.append(
            etree.Element(
                "{http://www.w3.org/2001/XMLSchema}pattern",
                value=itemType.restriction_pattern,
            )
        )

    if itemType.fraction_digits:
        xml_restriction.append(
            etree.Element(
                "{http://www.w3.org/2001/XMLSchema}fractionDigits",
                value=str(itemType.fraction_digits),
            )
        )

    if itemType.min_inclusive:
        xml_restriction.append(
            etree.Element(
                "{http://www.w3.org/2001/XMLSchema}minInclusive",
                value=str(itemType.min_inclusive),
            )
        )

    if itemType.enumerations:
        variant = "codes"  # Set item variant to enumeration (codes)

        # Add enumerations
        for enum in itemType.enumerations:
            xml_enum = etree.Element("{http://www.w3.org/2001/XMLSchema}enumeration")
            xml_enum.set("id", enum.id)
            xml_enum.set("value", enum.value)
            xml_restriction.append(xml_enum)

    else:
        variant = "types"  # Set item variant to types

    xml_simpleContent.append(xml_restriction)
    xml_complexType.append(xml_simpleContent)

    XML_element = Routeable_XML_Element(
        xml_namespace="schema",
        type="itemtype",
        element=xml_complexType,
        file_path=f"{conf.relative_root}/dictionary/{conf.generic_info['domain']}-{variant}.xsd",
    )

    return XML_element


def xml_concept_and_labels(concept_object, file_path):
    """
    Writes a concept to a taxonomy.

    :param concept_object: Concept, object that is going to be written
    :param file_path: string, path where file should be written to.

    :return:
    """

    # Get static params
    domain = conf.generic_info.get("domain")
    namespaces = conf.namespaces

    # Set XML element attributes
    xml_concept = etree.Element("{http://www.w3.org/2001/XMLSchema}element")

    # If @id has been given, use that. Otherwise, use the concept prefix that is passed on

    xml_concept.set("id", f"{concept_object.id}")

    xml_concept.set("name", f"{concept_object.name}")

    if isinstance(concept_object.element_type, str):
        itemType_prefix, itemType_name = str(concept_object.element_type).split(
            ":"
        )  # Split itemType into name and prefix
        if not itemType_prefix in ["xbrli"]:
            print(f"Should not be string: '{itemType_name}'")
        xml_concept.set("type", f"{concept_object.element_type}")
    else:
        itemType_prefix = concept_object.element_type.domain
        itemType_name = concept_object.element_type.name
        xml_concept.set("type", f"{itemType_prefix}:{itemType_name}")

    # If itemtype prefix is given in configuration, assume the NS and itemType exist. Otherwise, try to find it
    # if itemType_prefix not in namespaces.keys():

    # todo: allow for external elements to be discovered rather than expected to be available.
    if not itemType_prefix in ["xbrli"]:
        itemType_xml = find_itemtype(itemType_name=itemType_name)
        if not itemType_xml:
            if domain in itemType_prefix:
                itemType = createItemtype(itemType=concept_object.element_type)

                yield itemType
        else:
            # add found namespace to namespaces dict if it does not exist yet.
            if itemType_prefix not in namespaces.keys():
                path = itemType_xml[0].base
                if path.startswith("/"):
                    path = path.strip("/")
                namespaces[itemType_prefix] = {
                    "namespace": "http://" + path.replace(".xsd", ""),
                }
                logger.info(f"Added namespace. prefix: {itemType_prefix}, ns: {path}")

    if concept_object.substitutionGroup is not None:
        xml_concept.set("substitutionGroup", concept_object.substitutionGroup)

    if concept_object.periodType is not None:
        xml_concept.set(
            "{http://www.xbrl.org/2003/instance}periodType",
            f"{concept_object.periodType}",
        )

    if concept_object.typedDomainRef is not None:
        xml_concept.set(
            "{http://xbrl.org/2005/xbrldt}typedDomainRef",
            f"{concept_object.typedDomainRef}",
        )

    elif isinstance(concept_object.typedDomainRef_obj, Concept):
        typed_element_path = find_element_by_id(
            concept_id=concept_object.typedDomainRef_obj.id,
            reference_filepath=file_path,
            filenames=["*.xsd"],
        )
        if typed_element_path:
            xml_concept.set(
                "{http://xbrl.org/2005/xbrldt}typedDomainRef",
                f"{typed_element_path}#{concept_object.typedDomainRef_obj.id}",
            )
        else:
            logger.warning(
                f"typedDomainRef for '{concept_object.name}' is not found: '{concept_object.typedDomainRef.name}."
                f"This elements should have been written earlier."
            )

    if hasattr(concept_object, "is_abstract"):
        if concept_object.is_abstract is True:
            xml_concept.set("abstract", "true")
        elif concept_object.is_abstract is False:
            xml_concept.set("abstract", "false")

    if not concept_object.is_abstract and hasattr(concept_object, "balance"):
        xml_concept.set(
            "{http://www.xbrl.org/2003/instance}balance", f"{concept_object.balance}"
        )

    xml_concept.set("nillable", f"{concept_object.nillable}".lower())

    # Yield the concept element

    yield Routeable_XML_Element(
        xml_namespace="schema",
        type="concept",
        element=xml_concept,
        file_path=file_path,
    )

    label_linkbases = []
    ### LABELS ###
    for label in get_labels(concept_object, concept_filepath=file_path):

        # If the label linkbase does not exist, this needs to be created
        if not file_exists(label.file_path):

            # Need to create a linkbase stub
            yield create_labelLink(label.file_path)

            # Yield a linkbase reference into the EP
            yield Routeable_XML_Element(
                xml_namespace="schema",
                type="linkbaseref",
                element=get_linkroleref(
                    role="http://www.xbrl.org/2003/role/labelLinkbaseRef",
                    path=label.file_path,
                    relative_root=conf.relative_root,
                ),
                file_path=file_path,
            )
        yield label

    ### REFERENCES ###

    for reference in concept_object.references:
        for xml_object in get_xml_references(
            reference=reference, concept_id=concept_object.id, concept_path=file_path
        ):
            yield xml_object


def create_presentation_arc(from_obj, to_obj, arcrole, order):
    """
    Creates an arc of the given arcrole

    :return:
    """

    arc = Arc(
        tag="{http://www.xbrl.org/2003/linkbase}presentationArc",
        arcrole=arcrole,
        from_obj=from_obj,
        to_obj=to_obj,
        order=order,
    )
    return arc


def get_new_concept_href(concept):

    prefix = conf.generic_info.get("prefix", conf.generic_info.get("domain"))

    # Only use a suffix if a prefix is set
    if conf.generic_info.get("prefix"):
        suffix = f"-{prefix}"
    else:
        suffix = ""

    domain = conf.generic_info.get("domain")
    new_namespaces = conf.generic_info.get("new_namespaces")

    # concept is local to the domain we are exporting
    if concept.namespace in new_namespaces:
        # todo :EK: this shouldn't be based on ns_prefix. too unreliable.
        #  ex, bzk-ww-i would not work because of 'ww'
        #  Perhaps this should be completely dependent on the given namespace.

        """
        Chose where to save the element. This is based on the type and/or substitutionGroup
        """
        if concept.namespace not in conf.nsmap.values():

            found_namespace = conf.get_namespace(concept.ns_prefix)
            if not found_namespace:
                print(f"Namespace is not known for prefix: '{concept.ns_prefix}'")

            if concept.ns_prefix in conf.nsmap.keys():
                logger.info(
                    f"namespace for prefix '{concept.ns_prefix}' differs: '{concept.namespace}' "
                    f"instead of {found_namespace['namespace']}."
                )
            else:
                conf.nsmap[concept.ns_prefix] = concept.namespace

        if concept.substitutionGroup == "xbrli:item":
            href = f"{conf.relative_root}/dictionary/{domain}-data{suffix}.xsd"
            concept_prefix = prefix + "-i"

        elif concept.substitutionGroup == "sbr:domainMemberItem":
            href = f"{conf.relative_root}/dictionary/{domain}-domains{suffix}.xsd"
            concept_prefix = prefix + "-dm"

        elif concept.substitutionGroup == "xbrldt:dimensionItem":

            href = f"{conf.relative_root}/dictionary/{domain}-axes{suffix}.xsd"
            concept_prefix = prefix + "-dim"

        elif concept.substitutionGroup == "sbr:presentationItem":
            # elif concept.ns_prefix == f"{prefix}-abstr":
            href = f"{conf.relative_root}/presentation/{domain}-abstr{suffix}.xsd"
            concept_prefix = prefix + "-abstr"

        elif concept.is_abstract == False and concept.substitutionGroup is None:
            # Assume it is a typed member
            href = f"{conf.relative_root}/dictionary/{domain}-domains{suffix}.xsd"
            concept_prefix = prefix + "-dm"

        else:
            logger.warning(
                f"Builder does not know where to write concept '{concept.ns_prefix}' to. "
                f"This is normally based on substitutionGroup"
            )
    else:
        print("Prefix of concept is not the same as the thing we are writing now.")
    return (href, concept_prefix)


def write_concept(
    concept,
    xml_parent_element=None,
    linkbase_path=None,
    create_loc=True,
):
    """


    :param concept:
    :param arcrole:
    :param xml_parent_element: Optional, but necessary if creating locs
    :param linkbase_path: Optional, but necessary if creating locs
    :param create_loc: Boolean. Yields a locator if True
    :return:
    """

    relative_concept_path = ""
    absolute_concept_path = ""

    # todo: do I want to even do this check? It is probably better to assume that everything that
    #  does not exist is new. But then we need to search through all files to get the concepts
    #  or at the start, build a list of already existing concepts.
    if concept.namespace in conf.generic_info.get("new_namespaces"):
        # We need to write this concept, as the namespace does not yet exist

        # Get the concept href and prefix
        absolute_concept_path, concept_prefix = get_new_concept_href(concept=concept)

        # Add an @id attribute to concept if this does not exist.
        if not hasattr(concept, "id"):
            concept.id = concept_prefix + concept.name

        """
        Get the element, add a loc to it as well.
        """

        if not qname_store.concept_exists(concept.name):

            for xml_concept in xml_concept_and_labels(
                concept_object=concept,
                file_path=absolute_concept_path,
            ):
                yield xml_concept

                # Put the concept in the Qstore so we do not need to make it in the future
                qname_store.conceptStore.append(concept.name)

    # namespace is known and not new. It needs to be found, and referred to by a locator.
    elif concept.namespace in conf.nsmap.values():

        # Get the namespace
        ns = conf.get_namespace_info(namespace=concept.namespace)
        # ns = conf.namespaces[concept.ns_prefix]

        # If the schemaLocation is known (is absolute) we can add it here. In that case we assume the concept exists.
        if ns.get("schemaLocation"):
            relative_concept_path = ns["schemaLocation"]

        elif ns.get(
            "localpath"
        ):  # If a localpath is given, attempt to get the relative path that way

            #  Get the relative path based upon schemalocation's local path
            relative_concept_path = get_relative_path(
                linkbase_path, ns.get("localpath")
            )
            if not relative_concept_path.endswith(".xsd"):
                raise ValueError(
                    f"Path does not end in 'xsd'. Likely 'localpath' has not been set correctly in metadata YAML "
                )

    else:
        # Try and find the element in a local, external file. This is the most reliable, but also
        # time consuming option, so it is used as a last resort.

        relative_concept_path = find_element_by_id(
            concept_id=concept.id,
            reference_filepath=linkbase_path,
            xpath="xs:element",
            filenames=["*.xsd"],
        )

        if relative_concept_path is None:
            # todo: this will likely be the first moment we notice am imported taxonomy is missing.
            #  we should check this before running the builder, maybe by checking prefixes of elements beforehand.
            logger.error(f"Could not find element '{concept.ns_prefix}:{concept.name}")
            raise Exception(
                f"Could not find element '{concept.name}'. \n "
                f"This should not happen and needs to be adressed before taxonomy can be exported. \n"
                f"Things to check in config:"
                f"- imported domains (Could be the concept should actually be written)"
                f"- domain of exported taxonomy."
            )

    # Create a loc to the concept
    if create_loc:
        if relative_concept_path == "":
            if absolute_concept_path == "":

                print(
                    "Haven't been able to find an absolute path, so we can't make it into a relative one either"
                )
            else:
                relative_concept_path = ".." + re.sub(
                    conf.relative_root, "", absolute_concept_path
                )

        yield loc_creator(
            concept_id=concept.id,
            file_path_to=relative_concept_path,
            file_path=linkbase_path,
            xml_parent_element=xml_parent_element,
        )  # Create the locator needed to link to a concept

    # Check if the namespace needs to be added to the list
    # We do this so the namespace can be reused later.
    # Also, if the prefix already exists, but without the right namespace, something is going wrong.
    if concept.namespace not in conf.nsmap.values():
        # Concept namespace is not known in the file.

        found_namespace = conf.get_namespace(concept.ns_prefix)
        if not found_namespace:
            found_prefix = conf.get_prefix(concept.namespace)
            if not found_prefix:
                conf.namespaces[concept_prefix] = {
                    "namespace": concept.namespace,
                    "LocalSchemaLocation": absolute_concept_path,
                }


def arcWriter(arc, linkbase_path, xml_parent_element, arc_type="arc"):

    xml_arc = etree.Element(f"{arc.arc_type}")

    xml_arc.set(
        "{http://www.w3.org/1999/xlink}from", f"{arc.from_concept_obj.id}_loc"
    )  # coulc use arc.from_concept as well, but I decide not to as it is easier to just use the concept object when creating a taxonomy from scretch
    xml_arc.set("{http://www.w3.org/1999/xlink}to", f"{arc.to_concept_obj.id}_loc")
    xml_arc.set("{http://www.w3.org/1999/xlink}arcrole", arc.arcrole)

    if "order" in vars(arc):
        if arc.order is not None:
            # xml_arc.set("{http://www.w3.org/1999/xlink}order", str(arc.order))
            xml_arc.set("order", str(arc.order))

    if "closed" in vars(arc):
        if arc.closed is not None:
            xml_arc.set("{http://xbrl.org/2005/xbrldt}closed", str(arc.closed).lower())

    if "usable" in vars(arc):
        if arc.usable is not None:
            xml_arc.set("{http://xbrl.org/2005/xbrldt}usable", str(arc.usable).lower())

    if "context_element" in vars(arc):
        if arc.context_element is not None:
            xml_arc.set(
                "{http://xbrl.org/2005/xbrldt}contextElement", str(arc.context_element)
            )

    if "target_role" in vars(arc):
        if arc.target_role is not None:
            xml_arc.set(
                "{http://xbrl.org/2005/xbrldt}targetRole", str(arc.target_role.roleURI)
            )

    xml_arc.set(
        "{http://www.w3.org/1999/xlink}type", "arc"
    )  # Could use arc.type, but I think this is static?

    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type=arc_type,
        element=xml_arc,
        xml_parent_element=xml_parent_element,
        file_path=linkbase_path,
    )


def arcWriter_concept_to_concept(arc, linkbase_path, xml_parent_element):
    """
    Writes the XML element information of an arc

    :param arc:
    :return:
    """
    # Set path depending on the type of linkbase

    # Make Arc itself.
    yield arcWriter(arc, linkbase_path, xml_parent_element)

    # Make Concept and Locators for both the 'from' and 'to' side of the Arc
    for concept in [arc.from_concept_obj, arc.to_concept_obj]:

        # todo: check if concept has already been written?

        for xml_concept in write_concept(
            concept=concept,
            xml_parent_element=xml_parent_element,
            linkbase_path=linkbase_path,
        ):
            yield xml_concept
