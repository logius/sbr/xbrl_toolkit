import logging
from lxml import etree

from discover_xbrl.sbrnl_xbrl_parser.linkbase.classes import GenericArc
from discover_xbrl.sbrnl_xbrl_parser.schema.classes import Concept, RoleType
from .concept_to_concept import write_concept
from discover_xbrl.sbrnl_taxonomy_builder.linkbase.xml_elements.locator import (
    loc_creator,
)

from ..xml_elements.roleref import get_xml_linkrole_def
from ..xml_elements.roleref_labels import xml_roleref_labels

from ...buildtools.classes import Routeable_XML_Element, XML_Parent_element
from ...buildtools.mem_concept_finder import find_element_by_id
from ...buildtools.qname_store import QNameStore

logger = logging.getLogger("builder.linkbase")

from discover_xbrl.sbrnl_taxonomy_builder.config.conf import Config

conf = Config()
qname_store = QNameStore()

"""
Functions to write concepts, along with arc's and loc's to the taxonomy.
"""


def get_generic_arc(from_obj, to_obj, order=None):
    arc = GenericArc(
        tag="{http://xbrl.org/2008/generic}arc",
        arcrole="http://www.xbrl.org/2013/arcrole/parent-child",
        from_obj=from_obj,
        to_obj=to_obj,
        order=order,
    )
    return arc


def xml_generic_arc(arc, roleuri, linkbase_path):
    xml_arc = etree.Element(f"{arc.arc_type}")

    xml_arc.set(
        "{http://www.w3.org/1999/xlink}from", f"{arc.from_concept_obj.object.id}_loc"
    )  # coulc use arc.from_concept as well, but I decide not to as it is easier to just use the concept object when creating a taxonomy from scretch
    xml_arc.set(
        "{http://www.w3.org/1999/xlink}to", f"{arc.to_concept_obj.object.id}_loc"
    )
    xml_arc.set("{http://www.w3.org/1999/xlink}arcrole", arc.arcrole)

    if "order" in vars(arc):
        if arc.order is not None:
            xml_arc.set("{http://www.w3.org/1999/xlink}order", str(arc.order))

    xml_arc.set(
        "{http://www.w3.org/1999/xlink}type", "arc"
    )  # Could use arc.type, but I think this is static?

    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type="arc",
        element=xml_arc,
        xml_parent_element=XML_Parent_element(
            ns="gen",
            localname="link",
            role=roleuri,
        ),
        file_path=linkbase_path,
    )


def toc_arcWriter(arc, linkbase_path):
    """
    Writes the arc of the DTS presentation hierarchy

    :param arc:
    :return:
    """
    # Set path depending on the type of linkbase

    # Make Arc itself.
    yield xml_generic_arc(
        arc=arc,
        roleuri="http://www.xbrl.org/2008/role/link",
        linkbase_path=linkbase_path,
    )
    for xml_element in toc_element(
        obj=arc.to_concept_obj.object, arcrole=arc.arcrole, linkbase_path=linkbase_path
    ):
        yield xml_element


def toc_element(obj, arcrole, linkbase_path):
    # Make Concept and Locators the given object

    xml_parent_element = XML_Parent_element(
        ns="gen",
        localname="link",
        role="http://www.xbrl.org/2008/role/link",
    )

    # for obj in [arc.from_concept_obj, arc.to_concept_obj]:
    if isinstance(obj, Concept):

        for xml_element in write_concept(
            concept=obj,
            xml_parent_element=XML_Parent_element(
                ns="gen",
                localname="link",
            ),
            linkbase_path=linkbase_path,
        ):
            yield xml_element

    elif isinstance(obj, RoleType):

        # lr = find_id_by_roleURI(
        #     roleuri=roletype.roleURI,
        #     reference_filepath=str(linkbase_path).rsplit("/", 1)[0],
        #     expected_filter=["*linkroles*.xsd"],
        # )
        element_path = find_element_by_id(
            concept_id=obj.id,
            reference_filepath=linkbase_path,
            xpath="link:roleType",
            filenames=["*linkroles*.xsd"],
        )

        if element_path:
            yield loc_creator(
                concept_id=obj.id,
                file_path_to=element_path,
                file_path=linkbase_path,
                xml_parent_element=xml_parent_element,
            )
        else:
            # First create the linkrole

            xml_linkrole_element = get_xml_linkrole_def(linkrole=obj)
            yield xml_linkrole_element
            # get labels of linkrole
            for xml_labels_linkrole in xml_roleref_labels(
                roletype=obj, rel_schema_path=xml_linkrole_element.file_path
            ):
                yield xml_labels_linkrole

            if conf.generic_info.get("prefix") is not None:
                suffix = f'-{conf.generic_info.get("prefix")}'
            else:
                suffix = ""

            yield loc_creator(
                concept_id=obj.id,
                file_path_to=f"../dictionary/{conf.generic_info.get('domain')}-linkroles{suffix}.xsd",
                file_path=linkbase_path,
                xml_parent_element=xml_parent_element,
            )
