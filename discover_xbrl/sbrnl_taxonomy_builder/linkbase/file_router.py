import logging

from discover_xbrl.sbrnl_taxonomy_builder.buildtools.classes import XML_Parent_element

logger = logging.getLogger("builder.linkbase")
from discover_xbrl.sbrnl_taxonomy_builder.config.conf import Config

conf = Config()

"""
Routes the XML element to the correct linkbase file.
"""


def xml_linkbase_router(routable_element):
    """

    :param routable_element:
    :return:
    """

    if routable_element.type == "locator":

        # note to self:
        # file_path already given because this is dependent on the arc this loc belongs to

        return routable_element

    elif routable_element.type == "arc":

        # note to self:
        # file_path already given because this is dependent on the arc this loc belongs to

        return routable_element

    elif routable_element.type == "labelArc":
        return routable_element
    elif routable_element.type == "definitionArc":
        return routable_element
    elif routable_element.type == "presentationArc":
        return routable_element
    elif routable_element.type == "referenceArc":
        return routable_element
    elif routable_element.type == "genericArc":
        return routable_element
    elif routable_element.type == "tableArc":
        return routable_element

    elif routable_element.type == "loc":
        return routable_element

    elif routable_element.type == "label":
        return routable_element

    elif routable_element.type == "message":
        return routable_element

    elif routable_element.type == "reference":
        # routable_element.xml_parent_element = (
        #     "{http://www.xbrl.org/2003/linkbase}referenceLink",
        # )
        return routable_element

    elif routable_element.type == "roleRef":
        routable_element.xml_parent_position = 0
        return routable_element

    elif routable_element.type == "arcroleRef":
        """
        <link:arcroleRef
            arcroleURI="http://xbrl.org/int/dim/arcrole/all"
            xlink:href="http://www.xbrl.org/2005/xbrldt-2005.xsd#all"
            xlink:type="simple"/>
        """
        routable_element.xml_parent_position = 1
        routable_element.xml_parent_element = (
            "{http://www.xbrl.org/2003/linkbase}linkbase"
        )
        return routable_element

    elif routable_element.type == "presentationLink":
        """
        <link:presentationLink
            xlink:role="urn:uri_of_roletype"
            xlink:type="extended"
            >
        """
        routable_element.xml_parent_element = (
            "{http://www.xbrl.org/2003/linkbase}linkbase"
        )
        return routable_element

    elif routable_element.type == "labelLink":
        """
        <link:labelLink
            xlink:type="extended"
            >
        """
        routable_element.xml_parent_element = (
            "{http://www.xbrl.org/2003/linkbase}linkbase"
        )
        return routable_element

    elif routable_element.type == "variable":
        return routable_element

    elif routable_element.type == "assertion":
        return routable_element

    elif routable_element.type == "filter":
        return routable_element

    elif routable_element.type == "genericLabelLink":
        routable_element.xml_parent_element = (
            "{http://www.xbrl.org/2003/linkbase}linkbase"
        )
        return routable_element

    elif routable_element.type == "definitionLink":
        """
        <link:definitionLink
            xlink:role="urn:uri_of_roletype"
            xlink:type="extended"
            >
        """
        routable_element.xml_parent_element = (
            "{http://www.xbrl.org/2003/linkbase}linkbase"
        )
        return routable_element

    elif routable_element.type == "referenceLink":

        routable_element.xml_parent_element = (
            "{http://www.xbrl.org/2003/linkbase}linkbase"
        )
        return routable_element

    elif routable_element.type == "genericLink":
        # Do not set an xml_parent_element here as it depends on an URI. This should be set dynamically.
        routable_element.xml_parent_element = (
            "{http://www.xbrl.org/2003/linkbase}linkbase"
        )
        return routable_element

    elif routable_element.type in [
        "table",
        "table_breakdown",
        "table_breakdown_arc",
        "table_node",
        "table_subtree_arc",
    ]:

        # We could also just list all of these as tables, but this makes debugging easier.
        return routable_element

    elif routable_element.type == "parameter":
        routable_element.xml_parent_element = XML_Parent_element(
            ns="gen",
            localname="link",
            role="http://www.xbrl.org/2008/role/link",
        )
        routable_element.file_path = f"{conf.relative_root}/presentation/{conf.generic_info.get('domain')}-generic-parameter.xml"
        return routable_element
    else:
        logger.warning(f"Linkbase routable element type not know")
        return routable_element
