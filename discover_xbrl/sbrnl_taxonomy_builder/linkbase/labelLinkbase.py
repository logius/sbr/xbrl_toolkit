import logging
from lxml import etree

from discover_xbrl.sbrnl_taxonomy_builder.linkbase.xml_elements.locator import (
    loc_creator,
)
from discover_xbrl.sbrnl_taxonomy_builder.linkbase.xml_elements.roleref import (
    get_xml_roleref,
)

from ..buildtools.classes import Routeable_XML_Element, XML_Parent_element

from ..buildtools.mem_file_reader import get_relative_path

logger = logging.getLogger("builder.linkbase")

from discover_xbrl.sbrnl_taxonomy_builder.config.conf import Config

conf = Config()


def create_labelLink(file_path):
    xml_labelLink = etree.Element("{http://www.xbrl.org/2003/linkbase}labelLink")

    xml_labelLink.set(
        "{http://www.w3.org/1999/xlink}role", "http://www.xbrl.org/2003/role/link"
    )
    xml_labelLink.set("{http://www.w3.org/1999/xlink}type", "extended")

    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type="labelLink",
        element=xml_labelLink,
        file_path=file_path,
    )


def get_labels(concept, concept_filepath):

    # Loop though all labels connected to the given concept

    concept_labels = concept.labels.values()

    for label in concept_labels:

        # Yield the <link:label>
        for xml_label in get_label(label, concept_is_abstract=concept.is_abstract):
            yield xml_label

        # Get the relative path from XML to XSD
        rel_path = get_relative_path(xml_label.file_path, concept_filepath)

        yield loc_creator(
            concept_id=concept.id,
            file_path_to=rel_path,
            file_path=xml_label.file_path,
            xml_parent_element=XML_Parent_element(ns="link", localname="labelLink"),
        )

        yield get_labelarc(
            concept_id=concept.id,
            routable_label_id=label.id if label.id is not None else label.xlink_label,
            label_filepath=xml_label.file_path,
            label_parent_element=xml_label.xml_parent_element,
        )


def get_labelarc(concept_id, routable_label_id, label_filepath, label_parent_element):
    """Create a <labelArc> between Loc and Label"""
    xml_arc = etree.Element("{http://www.xbrl.org/2003/linkbase}labelArc")
    xml_arc.set(
        "{http://www.w3.org/1999/xlink}arcrole",
        "http://www.xbrl.org/2003/arcrole/concept-label",
    )  # todo: need to be set dynamically
    xml_arc.set("{http://www.w3.org/1999/xlink}from", f"{concept_id}_loc")
    xml_arc.set("{http://www.w3.org/1999/xlink}type", "arc")
    xml_arc.set("{http://www.w3.org/1999/xlink}to", routable_label_id)

    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type="labelArc",
        element=xml_arc,
        xml_parent_element=label_parent_element,  # same as label's parent
        file_path=label_filepath,  # same as labels's parent
    )


def get_label(label, concept_is_abstract):

    if concept_is_abstract:
        concept_type = "abstr"
        path = "presentation"
    else:
        concept_type = "data"
        path = "dictionary"

    if label.linkRole == "http://www.xbrl.org/2003/role/label":
        label_type = "lab"
        xml_parent = "{http://www.xbrl.org/2003/linkbase}labelLink"

    elif label.linkRole == "http://www.xbrl.org/2003/role/documentation":
        label_type = "documentation"
        xml_parent = "{http://www.xbrl.org/2003/linkbase}labelLink"

    elif label.linkRole == "http://www.xbrl.org/2003/role/verboseLabel":
        label_type = "verbose-lab"
        xml_parent = "{http://www.xbrl.org/2003/linkbase}labelLink"

    elif label.linkRole == "http://www.xbrl.org/2003/role/terseLabel":
        label_type = "terse-lab"
        xml_parent = "{http://www.xbrl.org/2003/linkbase}labelLink"

    elif label.linkRole == "http://www.xbrl.org/2003/role/totalLabel":
        label_type = "total-lab"
        xml_parent = "{http://www.xbrl.org/2003/linkbase}labelLink"

    elif label.linkRole == "http://www.xbrl.org/2003/role/periodStartLabel":
        label_type = "periodstart-lab"
        xml_parent = "{http://www.xbrl.org/2003/linkbase}labelLink"

    elif label.linkRole == "http://www.xbrl.org/2003/role/periodEndLabel":
        label_type = "periodend-lab"
        xml_parent = "{http://www.xbrl.org/2003/linkbase}labelLink"

    elif label.linkRole == "http://www.xbrl.org/2003/role/commentaryGuidance":
        label_type = "commentaryguidance-lab"
        xml_parent = "{http://www.xbrl.org/2003/linkbase}labelLink"

    elif label.linkRole == "http://www.xbrl.org/2003/role/definitionGuidance":
        label_type = "definitionguidance-lab"
        xml_parent = "{http://www.xbrl.org/2003/linkbase}labelLink"

    elif label.linkRole == "http://www.xbrl.org/2009/role/negatedTotalLabel":

        label_type = "negatedtotal-lab"
        roleref = get_negated_label_roleref(
            ref_type="negatedTotalLabel",
            label_type=label_type,
            path=path,
            concept_type=concept_type,
            label_language=label.language,
        )
        yield roleref
        xml_parent = "{http://www.xbrl.org/2003/linkbase}labelLink"

    elif label.linkRole == "http://www.xbrl.org/2009/role/negatedTerseLabel":

        label_type = "negatedterse-lab"
        roleref = get_negated_label_roleref(
            ref_type="negatedTerseLabel",
            label_type=label_type,
            path=path,
            concept_type=concept_type,
            label_language=label.language,
        )
        yield roleref
        xml_parent = "{http://www.xbrl.org/2003/linkbase}labelLink"

    elif label.linkRole == "http://www.xbrl.org/2009/role/negatedLabel":

        label_type = "negated-lab"
        roleref = get_negated_label_roleref(
            ref_type="negatedLabel",
            label_type=label_type,
            path=path,
            concept_type=concept_type,
            label_language=label.language,
        )
        yield roleref
        xml_parent = "{http://www.xbrl.org/2003/linkbase}labelLink"

    elif label.linkRole == "http://www.xbrl.org/2008/role/label":

        label_type = "generic-lab"
        xml_parent = {
            "tag": "{http://xbrl.org/2008/generic}link",
            "roleuri": "http://www.xbrl.org/2008/role/link",
        }

    elif label.linkRole == "http://www.xbrl.org/2008/role/verboseLabel":

        label_type = "generic-verbose-lab"
        xml_parent = {
            "tag": "{http://xbrl.org/2008/generic}link",
            "roleuri": "http://www.xbrl.org/2008/role/link",
        }

    elif (
        label.linkRole == "http://www.nltaxonomie.nl/2016/labelrole/domainSpecificLabel"
    ):

        label_type = "domainSpecificLabel-lab"
        roleref = get_domain_label_roleref(
            ref_type="domainSpecificLabel",
            label_type=label_type,
            path=path,
            concept_type=concept_type,
            label_language=label.language,
        )
        yield roleref
        xml_parent = "{http://www.xbrl.org/2003/linkbase}labelLink"

    elif (
        label.linkRole
        == "http://www.nltaxonomie.nl/2016/labelrole/domainSpecificTerseLabel"
    ):

        label_type = "domainSpecificTerseLabel-lab"
        roleref = get_domain_label_roleref(
            ref_type="domainSpecificTerseLabel",
            label_type=label_type,
            path=path,
            concept_type=concept_type,
            label_language=label.language,
        )
        yield roleref
        xml_parent = "{http://www.xbrl.org/2003/linkbase}labelLink"

    elif (
        label.linkRole
        == "http://www.nltaxonomie.nl/2016/labelrole/domainSpecificVerboseLabel"
    ):

        label_type = "domainSpecificVerboseLabel-lab"
        roleref = get_domain_label_roleref(
            ref_type="domainSpecificVerboseLabel",
            label_type=label_type,
            path=path,
            concept_type=concept_type,
            label_language=label.language,
        )
        yield roleref
        xml_parent = "{http://www.xbrl.org/2003/linkbase}labelLink"

    elif (
        label.linkRole
        == "http://www.nltaxonomie.nl/2016/labelrole/domainSpecificTotalLabel"
    ):

        label_type = "domainSpecificTotalLabel-lab"
        roleref = get_domain_label_roleref(
            ref_type="domainSpecificTotalLabel",
            label_type=label_type,
            path=path,
            concept_type=concept_type,
            label_language=label.language,
        )
        yield roleref
        xml_parent = "{http://www.xbrl.org/2003/linkbase}labelLink"

    else:
        print(f"Unknown label linkrole: {label.linkRole}")

    if conf.generic_info.get("prefix") is not None:
        prefix = f"-{conf.generic_info.get('prefix')}"
    else:
        prefix = ""
    filename = f"{conf.generic_info.get('domain')}-{concept_type}-{label_type}-{label.language}{prefix}.xml"

    xml_label = etree.Element("{http://www.xbrl.org/2003/linkbase}label")

    # add an @id to Label if this does not exist
    if not hasattr(label, "id"):
        label.id = label.xlink_label

    xml_label.set("id", f"{label.id if label.id is not None else label.xlink_label}")
    xml_label.set("{http://www.w3.org/1999/xlink}label", label.xlink_label)
    xml_label.set("{http://www.w3.org/1999/xlink}role", label.linkRole)
    xml_label.set("{http://www.w3.org/1999/xlink}type", "resource")
    xml_label.set("{http://www.w3.org/XML/1998/namespace}lang", label.language)
    xml_label.text = label.text

    yield Routeable_XML_Element(
        xml_namespace="linkbase",
        type="label",
        element=xml_label,
        xml_parent_element=xml_parent,  # depends on arc (gen or presentation?)
        file_path=f"{conf.relative_root}/{path}/{filename}",
    )


def get_negated_label_roleref(ref_type, label_type, path, concept_type, label_language):

    file_path = f"{conf.relative_root}/{path}/{conf.generic_info.get('domain')}-{concept_type}-{label_type}-{label_language}.xml"
    if ":" in file_path:
        print("':' in linkbase path when writing label specific roleRefs")

    xml_roleRef = get_xml_roleref(
        roleURI=f"http://www.xbrl.org/2009/role/{ref_type}",
        ref_url="http://www.xbrl.org/lrr/role/negated-2009-12-16.xsd",
        ref_id=ref_type,
    )

    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type="roleRef",
        element=xml_roleRef,
        file_path=file_path,
        xml_parent_position=0,
        xml_parent_element="{http://www.xbrl.org/2003/linkbase}linkbase",
    )


def get_domain_label_roleref(ref_type, label_type, path, concept_type, label_language):

    xml_roleRef = get_xml_roleref(
        roleURI=f"http://www.xbrl.org/2009/role/{ref_type}",
        ref_url="http://www.nltaxonomie.nl/2016/xbrl/sbr-labelroles.xsd",
        ref_id=ref_type,
    )
    file_path = f"{conf.relative_root}/{path}/{conf.generic_info.get('domain')}-{concept_type}-{label_type}-{label_language}.xml"
    if ":" in file_path:
        print("':' in linkbase path when writing label specific roleRefs")

    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type="roleRef",
        element=xml_roleRef,
        file_path=file_path,
        xml_parent_position=0,
        xml_parent_element="{http://www.xbrl.org/2003/linkbase}linkbase",
    )
