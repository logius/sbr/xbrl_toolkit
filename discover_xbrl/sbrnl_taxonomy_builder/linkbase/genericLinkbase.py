import logging

from lxml import etree

from .xml_elements.roleref_labels import get_generic_label
from ..buildtools.empty_files import linkrole_generic_labels
from ..schema.create_linkrole import get_linkroleref
from ..buildtools.classes import Routeable_XML_Element
from discover_xbrl.sbrnl_taxonomy_builder.linkbase.xml_elements.roleref import (
    get_linkrole_roleref,
)

logger = logging.getLogger("builder.linkbase")

from discover_xbrl.sbrnl_taxonomy_builder.config.conf import Config

conf = Config()


def create_generic_linkbase(hierarchy):

    pass


def get_generic_link(roletype, ep_path, linkbase_path):

    # for xml_elem in linkrole_generic_labels(
    #     linkbase_path=linkbase_path, entrypoint_path=ep_path
    # ):
    #     yield xml_elem

    for xml_roleref in get_linkrole_roleref(
        roletype=roletype,
        linkbase_path=linkbase_path,
    ):
        yield xml_roleref

    # Yield a linkbase reference into the EP
    # This should be a generic link ("http://www.xbrl.org/2008/role/link")
    yield Routeable_XML_Element(
        xml_namespace="schema",
        type="linkbaseref",
        element=get_linkroleref(
            role="http://www.xbrl.org/2008/role/link",
            path=linkbase_path,
            relative_root=conf.relative_root,
        ),
        file_path=ep_path,
    )

    # Continue by adding the genericLink itself
    xml_genericLink = etree.Element("{http://xbrl.org/2008/generic}link")

    xml_genericLink.set("{http://www.w3.org/1999/xlink}role", roletype.roleURI)
    xml_genericLink.set("{http://www.w3.org/1999/xlink}type", "extended")

    # Yield the definitionLink Element
    yield Routeable_XML_Element(
        xml_namespace="linkbase",
        type="genericLink",
        element=xml_genericLink,
        file_path=linkbase_path,
        xml_parent_element="{http://www.xbrl.org/2003/linkbase}linkbase",
    )


def get_generic_labels(labels, linkrole, ep_path):
    """
    Yields XML elements that are used to write labels to a generic linkbase.

    :param labels: List or dict of Label objects
    :param linkrole: Linkrole object
    :param ep_path: Absolute path of the entrypoint schema.
    :return:
    """



    # Make a list of languages. For each one, we might need to create a file
    langs = {}
    if isinstance(labels, dict):
        for label in labels.values():
            if label.language not in langs.keys():
                langs[label.language] = []
            langs[label.language].append(label)

    elif isinstance(labels, list):
        for label in labels:
            if label.language not in langs.keys():
                langs[label.language] = []
            langs[label.language].append(label)

    written_files = []
    for lang, labels in langs.items():
        for label in labels:
            # First create the linkbases and link to them from the EP
            label_filepath = f"{conf.relative_root}/validation/{conf.generic_info.get('domain')}-{linkrole.roletype_name}-for-gen-lab-{lang}.xml"

            # Get the <gen:link> parent element for the linkbase, if the file hasn't been created earlier

            if label_filepath not in written_files:

                for xml_element in get_generic_link(
                    roletype=linkrole,
                    linkbase_path=label_filepath,
                    ep_path=ep_path,
                ):
                    yield xml_element

                # Write extra generic link stuff.
                for xml_elem in linkrole_generic_labels(
                        linkbase_path=label_filepath, entrypoint_path=ep_path
                ):
                    yield xml_elem

                written_files.append(label_filepath)

            yield get_generic_label(
                label=label,
                filepath=label_filepath,
                linkrole_uri=linkrole.roleURI,
            )
