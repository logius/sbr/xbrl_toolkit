import logging

from lxml import etree

from ..schema.create_linkrole import get_linkroleref
from ..buildtools.classes import Routeable_XML_Element
from discover_xbrl.sbrnl_taxonomy_builder.linkbase.xml_elements.roleref import (
    get_linkrole_roleref,
)

logger = logging.getLogger("builder.linkbase")

from discover_xbrl.sbrnl_taxonomy_builder.config.conf import Config

conf = Config()


def create_definition_linkbase(hierarchy):

    pass


def get_definition_link(roletype, ep_path, linkrole_name=None, linkbase_path=None):

    if not linkbase_path:
        linkbase_path = f"{conf.relative_root}/validation/{linkrole_name}-def.xml"

    for roleref in get_linkrole_roleref(
        roletype=roletype,
        linkbase_path=linkbase_path,
    ):
        yield roleref

    # Yield a linkbase reference into the EP
    yield Routeable_XML_Element(
        xml_namespace="schema",
        type="linkbaseref",
        element=get_linkroleref(
            role="http://www.xbrl.org/2003/role/definitionLinkbaseRef",
            path=linkbase_path,
            relative_root=conf.relative_root,
        ),
        file_path=ep_path,
    )

    # Continue by adding the definitionLink itself
    xml_definitionLink = etree.Element(
        "{http://www.xbrl.org/2003/linkbase}definitionLink"
    )
    xml_definitionLink.set("{http://www.w3.org/1999/xlink}role", roletype.roleURI)
    xml_definitionLink.set("{http://www.w3.org/1999/xlink}type", "extended")

    # Yield the definitionLink Element
    yield Routeable_XML_Element(
        xml_namespace="linkbase",
        type="definitionLink",
        element=xml_definitionLink,
        file_path=linkbase_path,
    )
