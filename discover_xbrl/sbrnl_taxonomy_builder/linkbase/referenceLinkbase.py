import re

from lxml import etree

from discover_xbrl.sbrnl_taxonomy_builder.buildtools.classes import (
    Routeable_XML_Element,
    XML_Parent_element,
)
from discover_xbrl.sbrnl_taxonomy_builder.linkbase.xml_elements.locator import (
    loc_creator,
)

from discover_xbrl.sbrnl_taxonomy_builder.config.conf import Config
from discover_xbrl.sbrnl_taxonomy_builder.schema.create_linkrole import (
    get_linkroleref,
)

conf = Config()


def get_xml_reference(reference, linkbase_path):
    """
    "{http://www.xbrl.org/2003/linkbase}reference"
        <link:reference
            id="kvk-i_BWB2_2015-01-01_396_1_ref"
            xlink:label="kvk-i_BWB2_2015-01-01_396_1_ref"
            xlink:role="http://www.xbrl.org/2003/role/reference"
            xlink:type="resource"
            >
                <ref:Article>
                    396
                </ref:Article>
                <ref:IssueDate>
                    2015-01-01
                </ref:IssueDate>
                <ref:Name>
                    Burgerlijk wetboek boek 2
                </ref:Name>
                <ref:Paragraph>
                    1<
                /ref:Paragraph>
        </link:reference>
    """
    xml_reference = etree.Element("{http://www.xbrl.org/2003/linkbase}reference")
    xml_reference.set("id", reference.xlink_label)
    xml_reference.set(
        "{http://www.w3.org/1999/xlink}role", "http://www.xbrl.org/2003/role/reference"
    )
    xml_reference.set("{http://www.w3.org/1999/xlink}label", reference.xlink_label)
    xml_reference.set("{http://www.w3.org/1999/xlink}type", "resource")

    if reference.appendix is not None:
        xml_appendix = etree.Element("{http://www.xbrl.org/2006/ref}appendix")
        xml_appendix.text = reference.appendix

        xml_reference.append(xml_appendix)

    if reference.article is not None:
        xml_article = etree.Element("{http://www.xbrl.org/2006/ref}article")
        xml_article.text = reference.article
        xml_reference.append(xml_article)

    if reference.chapter is not None:
        xml_chapter = etree.Element("{http://www.xbrl.org/2006/ref}chapter")
        xml_chapter.text = reference.chapter
        xml_reference.append(xml_chapter)

    if reference.clause is not None:
        xml_clause = etree.Element("{http://www.xbrl.org/2006/ref}clause")
        xml_clause.text = reference.clause
        xml_reference.append(xml_clause)

    if reference.example is not None:
        xml_example = etree.Element("{http://www.xbrl.org/2006/ref}example")
        xml_example.text = reference.example
        xml_reference.append(xml_example)

    if reference.exhibit is not None:
        xml_exhibit = etree.Element("{http://www.xbrl.org/2006/ref}exhibit")
        xml_exhibit.text = reference.exhibit
        xml_reference.append(xml_exhibit)

    if reference.footnote is not None:
        xml_footnote = etree.Element("{http://www.xbrl.org/2006/ref}footnote")
        xml_footnote.text = reference.footnote
        xml_reference.append(xml_footnote)

    if reference.issuedate is not None:
        xml_issuedate = etree.Element("{http://www.xbrl.org/2006/ref}issuedate")
        xml_issuedate.text = reference.issuedate
        xml_reference.append(xml_issuedate)

    if reference.name is not None:
        xml_name = etree.Element("{http://www.xbrl.org/2006/ref}name")
        xml_name.text = reference.name
        xml_reference.append(xml_name)

    if reference.note is not None:
        xml_note = etree.Element("{http://www.xbrl.org/2006/ref}note")
        xml_note.text = reference.note
        xml_reference.append(xml_note)

    if reference.number is not None:
        xml_number = etree.Element("{http://www.xbrl.org/2006/ref}number")
        xml_number.text = reference.number
        xml_reference.append(xml_number)

    if reference.page is not None:
        xml_page = etree.Element("{http://www.xbrl.org/2006/ref}page")
        xml_page.text = reference.page
        xml_reference.append(xml_page)

    if reference.paragraph is not None:
        xml_paragraph = etree.Element("{http://www.xbrl.org/2006/ref}paragraph")
        xml_paragraph.text = reference.paragraph
        xml_reference.append(xml_paragraph)

    if reference.publisher is not None:
        xml_publisher = etree.Element("{http://www.xbrl.org/2006/ref}publisher")
        xml_publisher.text = reference.publisher
        xml_reference.append(xml_publisher)

    if reference.section is not None:
        xml_section = etree.Element("{http://www.xbrl.org/2006/ref}section")
        xml_section.text = reference.section
        xml_reference.append(xml_section)

    if reference.sentence is not None:
        xml_sentence = etree.Element("{http://www.xbrl.org/2006/ref}sentence")
        xml_sentence.text = reference.sentence
        xml_reference.append(xml_sentence)

    if reference.subclause is not None:
        xml_subclause = etree.Element("{http://www.xbrl.org/2006/ref}subclause")
        xml_subclause.text = reference.subclause
        xml_reference.append(xml_subclause)

    if reference.subparagraph is not None:
        xml_subparagraph = etree.Element("{http://www.xbrl.org/2006/ref}subparagraph")
        xml_subparagraph.text = reference.subparagraph
        xml_reference.append(xml_subparagraph)

    if reference.subsection is not None:
        xml_subsection = etree.Element("{http://www.xbrl.org/2006/ref}subsection")
        xml_subsection.text = reference.subsection
        xml_reference.append(xml_subsection)

    if reference.uri is not None:
        xml_uri = etree.Element("{http://www.xbrl.org/2006/ref}uri")
        xml_uri.text = reference.uri
        xml_reference.append(xml_uri)

    if reference.uridate is not None:
        xml_uridate = etree.Element("{http://www.xbrl.org/2006/ref}uridate")
        xml_uridate.text = reference.uridate
        xml_reference.append(xml_uridate)

    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type="reference",
        element=xml_reference,
        file_path=linkbase_path,
        xml_parent_element="{http://www.xbrl.org/2003/linkbase}referenceLink",
    )


def get_xml_referencelink(linkbase_path):

    """
    1. write reference link
       "{http://www.xbrl.org/2003/linkbase}referenceLink"
           <link:referenceLink
               xlink:role="http://www.xbrl.org/2003/role/link"
               xlink:type="extended"
           >
    """

    xml_referenceLink = etree.Element(
        "{http://www.xbrl.org/2003/linkbase}referenceLink"
    )

    xml_referenceLink.set(
        "{http://www.w3.org/1999/xlink}role", "http://www.xbrl.org/2003/role/link"
    )
    xml_referenceLink.set("{http://www.w3.org/1999/xlink}type", "extended")

    # return the definitionLink Element
    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type="referenceLink",
        element=xml_referenceLink,
        file_path=linkbase_path,
    )


def get_xml_referenceArc(reference, concept_id, linkbase_path):
    """
    "{http://www.xbrl.org/2003/linkbase}referenceArc"

        <link:referenceArc
            xlink:arcrole="http://www.xbrl.org/2003/arcrole/concept-reference"
            xlink:from="kvk-i_LegalSizeCriteriaClassification_loc"
            xlink:to="kvk-i_BWB2_2015-01-01_396_1_ref"
            xlink:type="arc"
        />
    """
    xml_reference_arc = etree.Element("{http://www.xbrl.org/2003/linkbase}referenceArc")
    xml_reference_arc.set(
        "{http://www.w3.org/1999/xlink}arcrole",
        "http://www.xbrl.org/2003/arcrole/concept-reference",
    )

    xml_reference_arc.set("{http://www.w3.org/1999/xlink}from", concept_id + "_loc")
    xml_reference_arc.set("{http://www.w3.org/1999/xlink}to", reference.xlink_label)

    xml_reference_arc.set("{http://www.w3.org/1999/xlink}type", "arc")

    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type="referenceArc",
        element=xml_reference_arc,
        file_path=linkbase_path,
        xml_parent_element=("{http://www.xbrl.org/2003/linkbase}referenceLink"),
    )


def get_xml_references(reference, concept_id, concept_path):
    linkbase_path = f"{conf.relative_root}/dictionary/{conf.generic_info.get('domain')}-data-ref.xml"

    # 1. write reference link
    yield get_xml_referencelink(linkbase_path=linkbase_path)

    # 2. Write reference
    yield get_xml_reference(reference=reference, linkbase_path=linkbase_path)

    # 3.  Write arc
    yield get_xml_referenceArc(
        reference=reference, linkbase_path=linkbase_path, concept_id=concept_id
    )

    # 4. Get locator to concept
    rel_path = ".." + re.sub(conf.relative_root, "", concept_path)
    yield loc_creator(
        concept_id=concept_id,
        file_path_to=rel_path,
        file_path=linkbase_path,
        xml_parent_element=XML_Parent_element(ns="link", localname="referenceLink"),
    )

    """
    5. Put reference in concept's schema
    """

    yield Routeable_XML_Element(
        xml_namespace="schema",
        type="linkbaseref",
        element=get_linkroleref(
            role="http://www.xbrl.org/2003/role/referenceLinkbaseRef",
            path=linkbase_path,
            relative_root=conf.relative_root,
        ),
        file_path=concept_path,
    )
