import logging

from lxml import etree
from discover_xbrl.sbrnl_taxonomy_builder.config.conf import Config

conf = Config()

logger = logging.getLogger("builder.linkbase")


def create_linkbase_stub():
    """
    Returns an empty linkbase tree

    :return:
    """
    linkbase_ns = {
        "link": "http://www.xbrl.org/2003/linkbase",
        "xlink": "http://www.w3.org/1999/xlink",
    }

    """
    Build the tree:
    """
    xmltree_linkbase = etree.Element(
        "{http://www.xbrl.org/2003/linkbase}linkbase", nsmap=linkbase_ns
    )
    return xmltree_linkbase
