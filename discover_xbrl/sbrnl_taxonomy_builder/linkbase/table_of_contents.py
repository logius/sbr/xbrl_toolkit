import anytree

from discover_xbrl.sbrnl_taxonomy_builder.buildtools.classes import (
    Routeable_XML_Element,
)
from discover_xbrl.sbrnl_taxonomy_builder.buildtools.dts_comparison import (
    DTS_comparator,
)
from discover_xbrl.sbrnl_taxonomy_builder.linkbase.arcs.toc_arcs import (
    toc_arcWriter,
    get_generic_arc,
    toc_element,
)
from discover_xbrl.sbrnl_taxonomy_builder.linkbase.genericLinkbase import (
    get_generic_link,
)

from discover_xbrl.sbrnl_taxonomy_builder.config.conf import Config
from discover_xbrl.sbrnl_taxonomy_builder.linkbase.xml_elements.roleref import (
    get_xml_roleref,
)
from discover_xbrl.sbrnl_taxonomy_builder.linkbase.xml_elements.arcrole import (
    get_arcrole_element,
)

from discover_xbrl.sbrnl_xbrl_parser.schema.classes import RoleType
from discover_xbrl.sbrnl_taxonomy_builder.buildtools.qname_store import QNameStore

qname_store = QNameStore()
conf = Config()


def xml_roleref_toc(linkbase_path):
    """
    Get the roleref specific for the presentation hierarchy

    :return:
    """

    xml_roleRef = get_xml_roleref(
        roleURI="http://www.xbrl.org/2008/role/link",
        ref_url="http://www.xbrl.org/2008/generic-link.xsd",
        ref_id="standard-link-role",
    )

    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type="roleRef",
        element=xml_roleRef,
        file_path=linkbase_path,
        xml_parent_element="{http://www.xbrl.org/2003/linkbase}linkbase",
    )


def build_table_of_contents(dts):
    """
    Builds the  table of contents, with parent/child relations between (abstract) Concepts and Linkroles.

    :param dts:
    :return:
    """

    # Check if the presentation hierarchy differs between DTSses
    dts_comparator = DTS_comparator()

    differences = dts_comparator.presentation_hierarchy_different_in_other_dts(dts)

    if differences:

        ep_shortname = dts.shortname
        linkbase_path = f"{conf.relative_root}/presentation/{conf.generic_info.get('domain')}-presentation-hierarchy-{ep_shortname}.xml"

    else:
        linkbase_path = f"{conf.relative_root}/presentation/{conf.generic_info.get('domain')}-presentation-hierarchy.xml"

    # Get the <link:roleRef>
    # I think this should be a specific one, rather than get_generic_link as we do not want to add 2008/role/link to
    # linkroles.xsd

    yield xml_roleref_toc(linkbase_path=linkbase_path)

    # Get arcroleRef
    yield Routeable_XML_Element(
        xml_namespace="linkbase",
        type="arcroleRef",
        element=get_arcrole_element(
            arcrole_uri="http://www.xbrl.org/2013/arcrole/parent-child",
            href="http://www.xbrl.org/lrr/arcrole/parent-child-2013-09-19.xsd#parent-child",
        ),
        file_path=linkbase_path,
    )

    # Get the <gen:link> element
    for xml_element in get_generic_link(
        roletype=RoleType(roleURI="http://www.xbrl.org/2008/role/link"),
        ep_path=f"{conf.relative_root}/entrypoints/{dts.entrypoint_name}",
        linkbase_path=linkbase_path,
    ):
        yield xml_element

    # # Linkbase type is actually generic, but passing presentationLink also makes the concept get
    # linkbase_type = (
    #     XML_Parent_element(
    #         ns="gen",
    #         localname="link",
    #         role="http://www.xbrl.org/2008/role/link",
    #     ),
    # )

    # Get generic arc's to link to (abstract) concepts and roletypes
    written_ids = []
    for node in [node for node in anytree.PreOrderIter(dts.presentation_hierarchy)]:

        # Cannot write an arc with 'from' to root, as nothing comes before the root
        if node.depth == 0:
            if node.object.id not in written_ids:
                written_ids.append(node.object.id)
                for xml_element in toc_element(
                    obj=node.object,
                    arcrole="http://www.xbrl.org/2013/arcrole/parent-child",
                    linkbase_path=linkbase_path,
                ):

                    yield xml_element

        else:
            arc = get_generic_arc(
                from_obj=node.parent,
                to_obj=node,
                order=node.order,
            )

            if not qname_store.arc_exists_in_linkrole(
                linkrole="http://www.xbrl.org/2008/role/link",
                arc=arc,
            ):
                # add arc to qstore
                qname_store.add_arc_to_linkrole_store(
                    linkrole="http://www.xbrl.org/2008/role/link", arc=arc
                )

                for XML_element in toc_arcWriter(
                    arc=arc,
                    linkbase_path=linkbase_path,
                ):
                    yield XML_element
