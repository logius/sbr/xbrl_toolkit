import logging

from lxml import etree

from discover_xbrl.sbrnl_taxonomy_builder.buildtools.classes import (
    Routeable_XML_Element,
)
from discover_xbrl.sbrnl_taxonomy_builder.buildtools.mem_file_reader import (
    get_relative_path,
    file_exists,
)
from discover_xbrl.sbrnl_taxonomy_builder.buildtools.qname_store import QNameStore
from discover_xbrl.sbrnl_taxonomy_builder.config.conf import Config
from discover_xbrl.sbrnl_taxonomy_builder.buildtools.mem_concept_finder import (
    find_id_by_roleURI,
)
from discover_xbrl.sbrnl_taxonomy_builder.linkbase.xml_elements.roleref_labels import (
    xml_roleref_labels,
)

conf = Config()

qname_store = QNameStore()


def get_xml_roleref(roleURI, ref_url, ref_id):
    xml_roleRef = etree.Element("{http://www.xbrl.org/2003/linkbase}roleRef")
    xml_roleRef.set("roleURI", roleURI)
    xml_roleRef.set(
        "{http://www.w3.org/1999/xlink}href",
        f"{ref_url}#{ref_id}",
    )
    xml_roleRef.set("{http://www.w3.org/1999/xlink}type", "simple")
    return xml_roleRef


def get_xml_linkrole_def(linkrole, referring_linkbase_type=None):

    roletype = etree.Element("{http://www.xbrl.org/2003/linkbase}roleType")

    if linkrole.id:
        roletype.set("id", linkrole.id)
    roletype.set("roleURI", linkrole.roleURI.__str__())

    xml_role_definition = etree.Element("{http://www.xbrl.org/2003/linkbase}definition")
    xml_role_definition.text = linkrole.definition
    roletype.append(xml_role_definition)

    # If the referring linkbase type is given, use this.
    if referring_linkbase_type is not None:
        xml_role_usedon = etree.Element("{http://www.xbrl.org/2003/linkbase}usedOn")
        xml_role_usedon.text = referring_linkbase_type
        roletype.append(xml_role_definition)
    elif linkrole.used_on is not None:

        # Otherwise, use the values given by the linkrole model.
        for used_on in linkrole.used_on:
            xml_role_usedon = etree.Element("{http://www.xbrl.org/2003/linkbase}usedOn")
            xml_role_usedon.text = used_on
            roletype.append(xml_role_usedon)

    return Routeable_XML_Element(
        xml_namespace="schema", type="linkrole", element=roletype
    )


def get_linkrole_roleref(roletype, linkbase_path, referring_linkbase_type=None):
    """
    Get <roleref> and linkrole definition if needed.

    :param roletype: RoleType object
    :param linkbase_path: Str path that needs to be included in the EP schema.
    :param refering_linkbase_type: Str role the linkbase has.
    :return:
    """

    if roletype.roleURI == "http://www.xbrl.org/2008/role/link":
        # Set relative path and ID hardcoded
        lr = {}
        lr["rel_path"] = "http://www.xbrl.org/2008/generic-link.xsd"
        lr["id"] = "standard-link-role"

    else:
        # Check if the Roletype already exists (in -linkroles.xsd probably)
        lr = find_id_by_roleURI(
            roleuri=roletype.roleURI,
            reference_filepath=str(linkbase_path).rsplit("/", 1)[0],
            expected_filter=["*linkroles*.xsd"],
        )

    if lr is None:
        # Roletype has not been created yet.
        logging.debug(
            f"No linkrole has been found for {roletype.roleURI}. Trying to write it now"
        )
        # Create it first.
        xml_linkrole = get_xml_linkrole_def(
            linkrole=roletype, referring_linkbase_type=referring_linkbase_type
        )
        yield xml_linkrole

        # get labels of linkrole
        for xml_labels_linkrole in xml_roleref_labels(
            roletype=roletype, rel_schema_path=xml_linkrole.file_path
        ):
            yield xml_labels_linkrole

        # As we have just created the linkrole, we can expect it to exist without looking for it.
        xml_path = str(linkbase_path).rsplit("/", 1)[0]

        lr = {}
        lr["rel_path"] = "../" + get_relative_path(xml_path, xml_linkrole.file_path)
        lr["id"] = xml_linkrole.element.get("id")

        if lr is None:
            logging.error(
                f"Linkrole still not found, even after trying to create it: '{roletype.roleURI}'"
            )

    # Yield roleref for linkroleURI
    yield Routeable_XML_Element(
        xml_namespace="linkbase",
        type="roleRef",
        element=get_xml_roleref(
            roleURI=roletype.roleURI, ref_url=lr["rel_path"], ref_id=lr["id"]
        ),
        file_path=linkbase_path,
        xml_parent_element="{http://www.xbrl.org/2003/linkbase}linkbase",
    )
