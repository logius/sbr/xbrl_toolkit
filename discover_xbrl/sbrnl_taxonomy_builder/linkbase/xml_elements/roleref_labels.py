from lxml import etree

from discover_xbrl.sbrnl_taxonomy_builder.config.conf import Config
from discover_xbrl.sbrnl_taxonomy_builder.buildtools.classes import (
    Routeable_XML_Element,
    XML_Parent_element,
)

from discover_xbrl.sbrnl_taxonomy_builder.buildtools.mem_file_reader import (
    get_relative_path,
    file_exists,
)

from discover_xbrl.sbrnl_taxonomy_builder.linkbase.xml_elements.locator import (
    loc_creator,
)
from discover_xbrl.sbrnl_taxonomy_builder.schema.create_linkrole import get_linkroleref

conf = Config()


def create_generic_labelLink(file_path):
    xml_labelLink = etree.Element("{http://xbrl.org/2008/generic}link")

    xml_labelLink.set(
        "{http://www.w3.org/1999/xlink}role", "http://www.xbrl.org/2008/role/link"
    )
    xml_labelLink.set("{http://www.w3.org/1999/xlink}type", "extended")

    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type="genericLabelLink",
        element=xml_labelLink,
        file_path=file_path,
    )


def get_dynamic_labelarc(node_id, message_id, label_filepath, label_parent_element):
    """Create a <labelArc> between node loc and message"""
    xml_arc = etree.Element("{http://xbrl.org/2008/generic}arc")
    xml_arc.set(
        "{http://www.w3.org/1999/xlink}arcrole",
        "http://www.nltaxonomie.nl/2017/arcrole/DynamicLabel",
    )
    xml_arc.set("{http://www.w3.org/1999/xlink}from", f"{node_id}_loc")
    xml_arc.set("{http://www.w3.org/1999/xlink}type", "arc")
    xml_arc.set("{http://www.w3.org/1999/xlink}to", message_id)

    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type="labelArc",
        element=xml_arc,
        xml_parent_element=label_parent_element,  # same as label's parent
        file_path=label_filepath,  # same as labels's parent
    )


def get_generic_labelarc(
    concept_id, routable_label_id, label_filepath, label_parent_element
):
    """Create a <labelArc> between Loc and Label"""
    xml_arc = etree.Element("{http://xbrl.org/2008/generic}arc")
    xml_arc.set(
        "{http://www.w3.org/1999/xlink}arcrole",
        "http://xbrl.org/arcrole/2008/element-label",
    )
    xml_arc.set("{http://www.w3.org/1999/xlink}from", f"{concept_id}_loc")
    xml_arc.set("{http://www.w3.org/1999/xlink}type", "arc")
    xml_arc.set("{http://www.w3.org/1999/xlink}to", routable_label_id)

    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type="labelArc",
        element=xml_arc,
        xml_parent_element=label_parent_element,  # same as label's parent
        file_path=label_filepath,  # same as labels's parent
    )


def get_generic_label(label, filepath, linkrole_uri=None):
    xml_label = etree.Element("{http://xbrl.org/2008/label}label")

    # add an @id to Label if this does not exist
    if not hasattr(label, "id"):
        label.id = label.xlink_label

    xml_label.set("id", f"{label.id}")
    xml_label.set("{http://www.w3.org/1999/xlink}label", label.xlink_label)
    xml_label.set(
        "{http://www.w3.org/1999/xlink}role", "http://www.xbrl.org/2008/role/label"
    )
    xml_label.set("{http://www.w3.org/1999/xlink}type", "resource")
    xml_label.set("{http://www.w3.org/XML/1998/namespace}lang", label.language)
    xml_label.text = label.text

    if linkrole_uri is None:
        parent_element = XML_Parent_element(
            ns="gen", localname="link", role="http://www.xbrl.org/2008/role/link"
        )
    else:
        parent_element = XML_Parent_element(
            ns="gen", localname="link", role=linkrole_uri
        )

    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type="label",
        element=xml_label,
        xml_parent_element=parent_element,
        file_path=filepath,
    )


def get_labels_linkrole(concept, concept_filepath):

    # Loop though all labels connected to the given concept

    if isinstance(concept.labels, dict):
        concept_labels = concept.labels.values()
    elif isinstance(concept.labels, list):
        concept_labels = concept.labels
    else:
        print(f"Labels of {concept.name} are not in dict or list")

    for label in concept_labels:
        if label.linkRole == "http://www.xbrl.org/2008/role/label":
            if conf.generic_info.get("prefix") is not None:
                suffix = f'-{conf.generic_info.get("prefix")}'
            else:
                suffix = ""

            filename = f"{conf.relative_root}/dictionary/{conf.generic_info.get('domain')}-linkroles-generic-lab-{label.language}{suffix}.xml"
            # Yield the <link:label>?

            xml_label = get_generic_label(label, filepath=filename)
            yield xml_label
            # Get the relative path from XML to XSD
            rel_path = get_relative_path(xml_label.file_path, concept_filepath)

            yield loc_creator(
                concept_id=concept.id,
                file_path_to=rel_path,
                file_path=xml_label.file_path,
                xml_parent_element=XML_Parent_element(
                    ns="gen",
                    localname="link",
                    role="http://www.xbrl.org/2008/role/link",
                ),
            )

            yield get_generic_labelarc(
                concept_id=concept.id,
                routable_label_id=label.id,
                label_filepath=xml_label.file_path,
                label_parent_element=xml_label.xml_parent_element,
            )


def xml_roleref_labels(roletype, rel_schema_path):

    completed_filepaths = []

    for label in get_labels_linkrole(roletype, concept_filepath=rel_schema_path):

        # If the label linkbase does not exist, this needs to be created
        if not file_exists(label.file_path):
            # Need to create a linkbase stub
            yield create_generic_labelLink(label.file_path)

        if label.file_path not in completed_filepaths:
            completed_filepaths.append(label.file_path)
            # Add a <link:linkbaseRef> pointing towards the linkbase file

            # Yield a linkbase reference into the EP
            yield Routeable_XML_Element(
                xml_namespace="schema",
                type="linkbaseref",
                element=get_linkroleref(
                    role="http://www.xbrl.org/2008/role/link",
                    path=label.file_path,
                    relative_root=conf.relative_root,
                ),
                file_path=rel_schema_path,
            )

        yield label
