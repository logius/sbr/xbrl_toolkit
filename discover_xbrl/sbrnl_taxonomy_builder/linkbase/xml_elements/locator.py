import logging
from lxml import etree

from discover_xbrl.sbrnl_taxonomy_builder.buildtools.classes import (
    Routeable_XML_Element,
)

logger = logging.getLogger("builder.linkbase")


def get_routable_loc(xml_loc, xml_parent_element, file_path):
    """

    :param xml_loc: LXML object, locator
    :param xml_parent_element: string or XML parent element
    :param file_path: file location of the locator
    :return:
    """
    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type="locator",
        element=xml_loc,
        xml_parent_element=xml_parent_element,
        file_path=file_path,
    )


def loc_creator(concept_id, file_path_to, file_path, xml_parent_element):
    """
    Creates a locator between arc and concept

    :param concept_id: string, the if of the linkable concept
    :file_path_to: string, the relative path of the linkable concept
    :file_path: file location of the locator itself
    :xml_parent_object: string or XML_Parent_element, describes the XML parent element locator should be appended to
    :return: Routeable_XML_Element, type=locator
    """

    if file_path_to is None or file_path_to == "":
        logger.warning(f"loc_creator got an invalid path")

    xml_loc = etree.Element("{http://www.xbrl.org/2003/linkbase}loc")

    xml_loc.set("{http://www.w3.org/1999/xlink}href", f"{file_path_to}#{concept_id}")
    xml_loc.set("{http://www.w3.org/1999/xlink}label", f"{concept_id}_loc")
    xml_loc.set("{http://www.w3.org/1999/xlink}type", "locator")

    return get_routable_loc(
        xml_loc=xml_loc, xml_parent_element=xml_parent_element, file_path=file_path
    )
