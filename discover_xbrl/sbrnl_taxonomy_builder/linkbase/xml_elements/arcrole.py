import logging

from lxml import etree

logger = logging.getLogger("builder.linkrole")
from discover_xbrl.sbrnl_taxonomy_builder.config.conf import Config

conf = Config()


def get_arcrole_element(arcrole_uri, href=None):
    xml_arcrole = etree.Element("{http://www.xbrl.org/2003/linkbase}arcroleRef")

    xml_arcrole.set("arcroleURI", arcrole_uri)
    xml_arcrole.set("{http://www.w3.org/1999/xlink}type", "simple")

    if href is not None:
        xml_arcrole.set(
            "{http://www.w3.org/1999/xlink}href",
            href,
        )
    else:

        if arcrole_uri == "http://xbrl.org/int/dim/arcrole/hypercube-dimension":
            xml_arcrole.set(
                "{http://www.w3.org/1999/xlink}href",
                "http://www.xbrl.org/2005/xbrldt-2005.xsd#hypercube-dimension",
            )

        elif arcrole_uri == "http://xbrl.org/int/dim/arcrole/all":
            xml_arcrole.set(
                "{http://www.w3.org/1999/xlink}href",
                "http://www.xbrl.org/2005/xbrldt-2005.xsd#all",
            )

        elif arcrole_uri == "http://xbrl.org/int/dim/arcrole/notAll":
            xml_arcrole.set(
                "{http://www.w3.org/1999/xlink}href",
                "http://www.xbrl.org/2005/xbrldt-2005.xsd#notAll",
            )

        elif arcrole_uri == "http://xbrl.org/int/dim/arcrole/domain-member":
            xml_arcrole.set(
                "{http://www.w3.org/1999/xlink}href",
                "http://www.xbrl.org/2005/xbrldt-2005.xsd#domain-member",
            )

        elif arcrole_uri == "http://xbrl.org/int/dim/arcrole/dimension-domain":
            xml_arcrole.set(
                "{http://www.w3.org/1999/xlink}href",
                "http://www.xbrl.org/2005/xbrldt-2005.xsd#dimension-domain",
            )

        elif arcrole_uri == "http://xbrl.org/arcrole/2008/element-label":
            xml_arcrole.set(
                "{http://www.w3.org/1999/xlink}href",
                "http://www.xbrl.org/2008/generic-label.xsd#element-label",
            )
        elif arcrole_uri == "http://xbrl.org/arcrole/2008/variable-filter":
            xml_arcrole.set(
                "{http://www.w3.org/1999/xlink}href",
                "http://www.xbrl.org/2008/variable.xsd#variable-filter",
            )
        elif arcrole_uri == "http://xbrl.org/arcrole/2008/variable-set-filter":
            xml_arcrole.set(
                "{http://www.w3.org/1999/xlink}href",
                "http://www.xbrl.org/2008/variable.xsd#variable-set-filter",
            )
        elif arcrole_uri == "http://xbrl.org/arcrole/2008/boolean-filter":
            xml_arcrole.set(
                "{http://www.w3.org/1999/xlink}href",
                "http://www.xbrl.org/2008/boolean-filter.xsd#boolean-filter",
            )
        elif arcrole_uri == "http://xbrl.org/arcrole/2008/variable-set-precondition":
            xml_arcrole.set(
                "{http://www.w3.org/1999/xlink}href",
                "http://www.xbrl.org/2008/variable.xsd#variable-set-precondition",
            )
        elif arcrole_uri == "http://xbrl.org/arcrole/2008/variable-set":
            xml_arcrole.set(
                "{http://www.w3.org/1999/xlink}href",
                "http://www.xbrl.org/2008/variable.xsd#variable-set",
            )
        elif (
            arcrole_uri == "http://xbrl.org/arcrole/2016/assertion-unsatisfied-severity"
        ):
            xml_arcrole.set(
                "{http://www.w3.org/1999/xlink}href",
                "http://www.xbrl.org/2016/assertion-severity.xsd#assertion-unsatisfied-severity",
            )
        elif arcrole_uri == "http://www.xbrl.org/2013/arcrole/parent-child":
            xml_arcrole.set(
                "{http://www.w3.org/1999/xlink}href",
                "http://www.xbrl.org/lrr/arcrole/parent-child-2013-09-19.xsd#parent-child",
            )

        else:
            logger.warning(f"arcrole {arcrole_uri} not supported")

    return xml_arcrole
