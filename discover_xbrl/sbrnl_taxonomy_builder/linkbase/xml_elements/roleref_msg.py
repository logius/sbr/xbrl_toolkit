from lxml import etree

from discover_xbrl.sbrnl_taxonomy_builder.config.conf import Config
from discover_xbrl.sbrnl_taxonomy_builder.buildtools.classes import (
    Routeable_XML_Element,
    XML_Parent_element,
)

from discover_xbrl.sbrnl_taxonomy_builder.buildtools.mem_file_reader import (
    get_relative_path,
    file_exists,
)

from discover_xbrl.sbrnl_taxonomy_builder.linkbase.xml_elements.locator import (
    loc_creator,
)
from discover_xbrl.sbrnl_taxonomy_builder.schema.create_linkrole import get_linkroleref

conf = Config()


def create_generic_labelLink(file_path):
    xml_labelLink = etree.Element("{http://xbrl.org/2008/generic}link")

    xml_labelLink.set(
        "{http://www.w3.org/1999/xlink}role", "http://www.xbrl.org/2008/role/link"
    )
    xml_labelLink.set("{http://www.w3.org/1999/xlink}type", "extended")

    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type="genericLabelLink",
        element=xml_labelLink,
        file_path=file_path,
    )


def get_generic_labelarc(
    concept_id, routable_label_id, label_filepath, label_parent_element
):
    """Create a <labelArc> between Loc and Label"""
    xml_arc = etree.Element("{http://xbrl.org/2008/generic}arc")
    xml_arc.set(
        "{http://www.w3.org/1999/xlink}arcrole",
        "http://xbrl.org/arcrole/2008/element-label",
    )
    xml_arc.set("{http://www.w3.org/1999/xlink}from", f"{concept_id}_loc")
    xml_arc.set("{http://www.w3.org/1999/xlink}type", "arc")
    xml_arc.set("{http://www.w3.org/1999/xlink}to", routable_label_id)

    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type="labelArc",
        element=xml_arc,
        xml_parent_element=label_parent_element,  # same as label's parent
        file_path=label_filepath,  # same as labels's parent
    )


def get_generic_message(message, filepath, linkrole_uri=None):
    xml_message = etree.Element("{http://xbrl.org/2010/message}message")

    # add an @id to Label if this does not exist
    if not hasattr(message, "id"):
        message.id = message.xlink_label

    xml_message.set("id", f"{message.id}")
    xml_message.set("{http://www.w3.org/1999/xlink}label", message.xlink_label)
    xml_message.set(
        "{http://www.w3.org/1999/xlink}role", "http://www.xbrl.org/2010/role/message"
    )
    xml_message.set("{http://www.w3.org/1999/xlink}type", "resource")
    xml_message.set("{http://www.w3.org/XML/1998/namespace}lang", message.language)
    xml_message.text = message.text

    if linkrole_uri is None:
        parent_element = XML_Parent_element(
            ns="gen", localname="link", role="http://www.xbrl.org/2008/role/link"
        )
    else:
        parent_element = XML_Parent_element(
            ns="gen", localname="link", role=linkrole_uri
        )

    return Routeable_XML_Element(
        xml_namespace="linkbase",
        type="message",
        element=xml_message,
        xml_parent_element=parent_element,
        file_path=filepath,
    )

