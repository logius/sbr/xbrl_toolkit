'''
File that loads all relevant module parts.
'''
from .widget import TaxonomyBuilderWidget
from .app_logger import setup_logger

def load_module(module_object={}):

    setup_logger()
    module_object['name'] = "taxonomy_builder"
    module_object['type'] = "export"
    module_object['gui'] = TaxonomyBuilderWidget
    module_object['description'] = """
    Module that allows for taxonomies to be generated. Together with the XBRL taxonomy parser this module is the most
    ambitious part of the toolkit. However, taxonomies generated with this tooling are not to be used in production
    but are used within small pilot projects.
    """


    return module_object

