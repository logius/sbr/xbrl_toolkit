import logging

from ..buildtools.classes import Routeable_XML_Element

logger = logging.getLogger("builder.schema")

from discover_xbrl.sbrnl_taxonomy_builder.config.conf import Config

conf = Config()


def get_entrypoint_stub(dts):
    """
    Technically, the schema stub is enough for XBRL 2.1 to be used as an entrypoint,
    but we also need to add @id

    This is done by adding the id attribute to Routable_XML_Element. This makes sure it gets added when getting the stub.

    2.02.00.23: Een entrypoint xs:schema MOET een @id hebben.

    :return:
    """

    yield Routeable_XML_Element(
        xml_namespace="schema",
        type="entrypoint",
        element=None,
        filename=dts.entrypoint_name,
        id=conf.generic_info.get("domain") + "-" + dts.shortname,
    )
