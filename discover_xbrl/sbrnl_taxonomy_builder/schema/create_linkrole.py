import logging

from lxml import etree

logger = logging.getLogger("builder.schema")


def get_linkroleref(role, path, relative_root):
    """

    :param linkbaseref:
    :return:
    """

    rel_path = str(path).replace(relative_root, "")

    if str(path).endswith(".xsd"):
        logger.error(f"Reference points to '.xsd'. Linkbase (.xml) expected!")

    xml_linkbaseref = etree.Element("{http://www.xbrl.org/2003/linkbase}linkbaseRef")

    xml_linkbaseref.set(
        "{http://www.w3.org/1999/xlink}arcrole",
        "http://www.w3.org/1999/xlink/properties/linkbase",
    )  # coulc use arc.from_concept as well, but I decide not to as it is easier to just use the concept object when creating a taxonomy from scretch
    xml_linkbaseref.set("{http://www.w3.org/1999/xlink}href", f"..{rel_path}")
    xml_linkbaseref.set("{http://www.w3.org/1999/xlink}role", f"{role}")
    xml_linkbaseref.set("{http://www.w3.org/1999/xlink}type", "simple")
    return xml_linkbaseref
