import logging

from lxml import etree

from discover_xbrl.sbrnl_taxonomy_builder.config.conf import Config

conf = Config()

logger = logging.getLogger("builder.schema")


def create_schema_stub(filepath, id=None):
    """
    Returns an empty schema tree

    :return:
    """

    # namespaces = {}

    """
    Build the tree:
    <xs:schema
        targetNamespace="{absolute_url_of_this_file_without_extension}" 
        xmlns:bd-rpt-zvw-sba="{absolute_url_of_this_file_without_extension}" 
        xmlns:link="http://www.xbrl.org/2003/linkbase" 
        xmlns:xlink="http://www.w3.org/1999/xlink" 
        elementFormDefault="qualified" 
        attributeFormDefault="unqualified"
        xmlns:xs="http://www.w3.org/2001/XMLSchema">
    >
        <xs:annotation>
            <xs:appinfo>
            </xs:appinfo>
        </xs:annotation>
    </xs:schema>
    """
    schema_ns = {"xs": "http://www.w3.org/2001/XMLSchema"}

    xmltree_schema = etree.Element(
        "{http://www.w3.org/2001/XMLSchema}schema", nsmap=schema_ns
    )
    # nsmap=nsmap)  # We could add some namespaces if we want, however, it is better to explicitly name these over here
    xmltree_schema.attrib["attributeFormDefault"] = "unqualified"
    xmltree_schema.attrib["elementFormDefault"] = "qualified"

    if id:
        xmltree_schema.attrib["id"] = id

    namespace = conf.generic_info['www_protocol'] + str(filepath).replace(".xsd", "")
    new_prefix = namespace.rsplit("/", 1)[1]

    targetNamespace = None
    for ns_prefix, ns in conf.namespaces.items():
        if filepath in ns["namespace"]:
            targetNamespace = ns["namespace"]

    if not targetNamespace:

        # Set local schema location. We cannot set schemaLocation here because that needs to be relative
        conf.namespaces[new_prefix] = {"namespace": namespace, "localSchemaLocation": filepath}
        logger.info(
            f"Adding Namespace: prefix: '{new_prefix}', ns: '{namespace}', location: {filepath} is not known!"
        )

        xmltree_schema.attrib["targetNamespace"] = namespace

    # xmltree_schema.attrib['targetNamespace'] = f"{absolute_root}/{filepath}"
    else:

        # Set local schema location. We cannot set schemaLocation here because that needs to be relative
        known_namespace = conf.get_namespace(new_prefix)
        known_namespace['localSchemaLocation'] = filepath

        xmltree_schema.attrib["targetNamespace"] = targetNamespace

    xmltree_annotation = etree.Element("{http://www.w3.org/2001/XMLSchema}annotation")
    xmltree_schema.append(xmltree_annotation)  # Add notation to schema

    xmltree_appinfo = etree.Element("{http://www.w3.org/2001/XMLSchema}appinfo")
    xmltree_annotation.append(xmltree_appinfo)  # Add app info to annotation

    # filestring = etree.tostring(
    #     xmltree_schema,
    #     pretty_print=True,
    #     xml_declaration=True,
    #     encoding='UTF-8',
    #     standalone=True
    # )

    return xmltree_schema
