import logging

logger = logging.getLogger("builder.schema")

from discover_xbrl.sbrnl_taxonomy_builder.config.conf import Config

conf = Config()

"""
Routes the XML element to the correct schema file.
"""


def xml_schema_router(routable_element):
    """

    :param routable_element:
    :param domain: Domain prefix
    :param relative_root: The path in which the new taxonomy is written
    :return:
    """
    if routable_element.type == "linkrole":
        """
        Linkrole Definition

        <link:roleType id="" roleURI="">
            <link:definition>Balans</link:definition>
            <link:usedOn>gen:link</link:usedOn>
            <link:usedOn>link:definitionLink</link:usedOn>
            <link:usedOn>link:presentationLink</link:usedOn>
        </link:roleType>
        """

        routable_element.xml_parent_element = (
            "{http://www.w3.org/2001/XMLSchema}appinfo"
        )
        if routable_element.file_path is None:
            if conf.generic_info.get("prefix") is not None:
                suffix = f'-{conf.generic_info.get("prefix")}'
            else:
                suffix = ""
            routable_element.file_path = f"{conf.relative_root}/dictionary/{conf.generic_info.get('domain')}-linkroles{suffix}.xsd"

        return routable_element

    elif routable_element.type == "linkbaseref":
        """
        Linkrole reference (in entrypoint)

           <link:linkbaseRef
            xlink:arcrole="http://www.w3.org/1999/xlink/properties/linkbase"
            xlink:href=""
            xlink:role=""
            xlink:type="simple"
          />
        """

        routable_element.xml_parent_element = (
            "{http://www.w3.org/2001/XMLSchema}appinfo"
        )
        return routable_element

    elif routable_element.type == "concept":

        routable_element.xml_parent_element = "{http://www.w3.org/2001/XMLSchema}schema"
        # note to self:
        # file_path already given because this is needed to generate a loc
        # Either keep it like this and define it right away, or add the path here and
        # generate the loc over here as well.

        return routable_element

    elif routable_element.type == "itemtype":
        # file path is set dynamically (codes or types)
        routable_element.xml_parent_element = "{http://www.w3.org/2001/XMLSchema}schema"

        return routable_element

    elif routable_element.type == "import":
        routable_element.xml_parent_element = "{http://www.w3.org/2001/XMLSchema}schema"
        routable_element.xml_parent_position = (
            1  # needs to be inserted after the <xs:annotation> tag
        )
        # <xs:import>
        return routable_element

    elif routable_element.type == "entrypoint":
        routable_element.file_path = (
            f"{conf.relative_root}/entrypoints/{routable_element.filename}"
        )
        routable_element.xml_parent_element = None
        return routable_element

    else:
        return routable_element
