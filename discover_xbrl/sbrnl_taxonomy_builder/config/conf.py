import yaml
import os.path


class Config:
    _instance = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self, file=None):
        """
        Initialize the configuration
        any first-level variable in the yanl configuration will become
        an attribute of this object.
        so
        db:
          user: 'postgres'
          host: 'localhost'
        becomes
        self.db = {'user': 'postgres', 'host': 'localhost'}

        :param file: str, optional. When given the config will be (re-)read from this file
                                    Otherwise the default config will be loaded once
                                    and returned to all requests
        some doctesst
        >>> tst = Config('builder_metadata_example.yaml')
        >>> tst.generic_info['date_version']
        '20201209'
        >>> tst.get_config('no.yaml')
        Problem with opening configuration 'no.yaml'
        """
        if file or not hasattr(self, "namespaces"):
            self.get_config(file=file)

    def get_config(self, file=None):
        """
        reads config.yaml, or if provided any yaml configuration

        :param file: filename of config to read, if None config.yaml will be used
        :return config dictionary
        """
        if file is None:
            file = "builder_metadata.yaml"
        try:
            with open(os.path.join(os.path.dirname(__file__), file), "r") as configfile:
                config = yaml.load(configfile, Loader=yaml.Loader)
        except (IOError, FileNotFoundError) as e:
            print(f"Problem with opening configuration '{os.path.basename(file)}'")
            return None

        for key, value in config.items():
            setattr(self, key, value)

        ### Add custom, dynamic attributes

        # Relative root, used to refer to the files relative to each other (in memory)
        rel_root_str = f"{self.generic_info.get('www_domain')}/{self.generic_info.get('nt_version')}/{self.generic_info.get('domain')}/{self.generic_info.get('date_version')}"
        setattr(self, "relative_root", rel_root_str)

        # Absolute root, used for target namespace and other absolute indicators
        abs_root_str = f"{self.generic_info.get('www_protocol')}/{self.generic_info.get('www_domain')}/{self.generic_info.get('nt_version')}/{self.generic_info.get('domain')}/{self.generic_info.get('date_version')}"
        setattr(self, "absolute_root", abs_root_str)

    @property
    def nsmap(self):
        """
        Used to pass to lxml as a namespace mapping.
        In order to do this, the  namespace value of each entry is kept.

        :return: Dict with prefix:uri
        """
        nsmap = {}
        for k, v in self.namespaces.items():
            nsmap[k] = v["namespace"]

        return nsmap

    def get_namespace(self, prefix):
        """
        Takes a prefix and returns the corresponding information if it exists

        """
        for k, v in self.namespaces.items():
            if k == prefix:
                v["prefix"] = k
                return v
        return None

    def get_namespace_info(self, namespace):
        """
        Takes a prefix and returns the corresponding information if it exists

        """
        for k, v in self.namespaces.items():
            if v["namespace"] == namespace:
                return v
        return None

    def get_prefix(self, namespace):
        """
        Takes a namespace and returns the corresponding information if it exists

        """
        for k, v in self.namespaces.items():
            if v["namespace"] == namespace:

                return {k: v}
        return None


if __name__ == "__main__":
    import doctest

    doctest.testmod()
    c = Config()
