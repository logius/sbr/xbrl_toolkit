from os.path import join, isdir
from pathlib import Path


class EPList:
    def __init__(self, directory=None, nt_version=None, sub_version=None):
        self.local_dir = directory
        self.nt_version = nt_version
        self.sub_version = sub_version if sub_version else ""
        self.top_dir = self.create_start()
        self.ep_list = []

    def create_list(self):
        """ gathers all entrypoints from 1 NT-version
            optionally the alfa or beta 'sub-version'
            It will start at '<local_taxonomy_dir>/<nt_version>
            then it will traverse 'bzk', 'bd', 'jenv', ... """
        if self.top_dir and isdir(self.top_dir):
            for file in Path(self.top_dir).iterdir():
                if not file.is_file():
                    self.traverse_domain(file)
        else:
            self.ep_list.append(f"{self.top_dir} is not a directory!")

    def traverse_domain(self, domain):
        """ We have three options,
        alfa, beta, or production. Alfa directories end with '.a' by convention
        Beta versions are contained in directories ending with '.b'. If neither .a or .b is present
        We have the final version
        """
        for dir in domain.iterdir():
            if not dir.is_dir():
                continue
            if self.sub_version == "alfa" and dir.suffix == ".a":
                self.get_eps(dir)
            elif self.sub_version == "beta" and dir.suffix == ".b":
                self.get_eps(dir)
            elif self.sub_version == "" and dir.suffix not in [".a", ".b"]:
                self.get_eps(dir)

    def get_eps(self, dir):
        """The finish is futile; find the entrypoints directory and all files inside
        need to be parsed. """
        for subdir in dir.iterdir():
            if str(subdir).endswith("entrypoints"):
                for file in subdir.iterdir():
                    if file.is_file() and file.suffix == ".xsd":
                        self.ep_list.append(str(file))

    def create_start(self):
        return join(self.local_dir, self.nt_version)


class NTVersionList:
    def __init__(self, local_taxonomy_dir=None):
        """
        NTVersionList
        @param: local_taxonomy_dir: str
        property:
        nt_versions: returns all available NT-versions withoon the loal_taxonomy_dir
        """
        self.local_taxonomy_dir = local_taxonomy_dir

    @property
    def nt_versions(self):
        # we look at the disk, not at the Database here
        nt_versions = []
        if self.local_taxonomy_dir and isdir(self.local_taxonomy_dir):
            for version in Path(self.local_taxonomy_dir).iterdir():
                if version.is_dir():
                    nt_versions.append(str(version).split("/")[-1])
        return sorted(nt_versions)


def main():
    versions = NTVersionList(local_taxonomy_dir="/tmp/xbrl/www.nltaxonomie.nl")
    print(versions.nt_versions)
    # ep_lister = EPList(
    #    directory="/tmp/xbrl/www.nltaxonomie.nl", nt_version="nt16", sub_version=""
    # )
    # ep_lister.create_list()


if __name__ == "__main__":
    main()
