import argparse
import smtplib
import sys
from email.message import EmailMessage
from email.utils import formatdate
from os.path import join

from discover_xbrl.conf.conf import Config
from discover_xbrl.sbrnl_modules.db.dts_to_sql import DtsToSql
from discover_xbrl.sbrnl_modules.validators.NT16.module import (
    load_module as NT16_Validator_module,
)
from discover_xbrl.sbrnl_modules.validators.NT16.validation_report import Report
from discover_xbrl.sbrnl_xbrl_parser.parsetools.helpers import version_from_identifier
from discover_xbrl.sbrnl_xbrl_parser.parsetools.loadTaxonomy import Discover
from discover_xbrl.sbrnl_xbrl_parser.parsetools.mem_fs import save_taxonomy_memfs
from discover_xbrl.sbrnl_xbrl_parser.parsetools.taxonomy_package import (
    read_taxonomy_package,
)
from discover_xbrl.sbrnl_xbrl_toolbox.build_ep_list import EPList
from discover_xbrl.sbrnl_modules.db.version_compare.version_compare import (
    VersionCompare,
)


class CLI:
    """
    CLI is a command line utility for theXBRL Toolkit.
    Currently it's main function is to ease and automate tasks
    - create ep_lists
    - parse entrypoints
    - store entrypoints
    - validate NTA rules
    - store Python objects
    """

    def __init__(self, configfile=None):
        if not configfile:
            self._conf = Config()
        else:
            self._conf = Config(configfile)
        self.local_taxonomy_dir = None
        self.parse = None
        self.validate_nt = None
        self.store = None
        self.compare = None
        self.cache = None
        self.run_id = None
        self.build_ep_list = None
        self.nt_version = None
        self.alfa = None
        self.beta = None
        self.ep_list = []
        self.taxonomy_package = None
        self.taxonomy_package_metadata = None
        self.validator = None
        self.notify = None
        self.discovered_dts_list = []

    @property
    def sub_version(self):
        sub_version = ""
        if self.alfa:
            sub_version = "alfa"
        elif self.beta:
            sub_version = "beta"
        return sub_version

    def create_ep_list(self):
        if not self.local_taxonomy_dir:
            self.local_taxonomy_dir = self._conf.parser.get("local_taxonomy_dir")
        ep_list = EPList(
            directory=self.local_taxonomy_dir,
            nt_version=self.nt_version,
            sub_version=self.sub_version,
        )
        ep_list.create_list()
        self.ep_list = ep_list.ep_list

    def nt_versions_from_local_dir(self, local_taxonomy_dir=None):
        local_taxonomy_dir = (
            local_taxonomy_dir
            if local_taxonomy_dir
            else self._conf.parser.get("local_taxonomy_dir")
        )

    def parse_eps(self, ep):
        validator_module = None
        if self.validate_nt:
            if self.validator is None:
                self.validator = NT16_Validator_module()
            validator_module = self.validator["setup_validator"]

        with open(
            join(self._conf.logging["maindir"], self._conf.logging["xbrl_cli"]), "a+", 1
        ) as outfile:
            print(f"\n Parsing: {ep}")
            sys.stdout = (
                outfile
            )  # the logfile now takes standard output, thus it can be followed by the browser.
            print(f" Parsing: {ep}")
            discoverer = Discover(
                ep=ep,
                discover_all_ep=False,
                validator=validator_module() if validator_module else None,
                taxonomy_package=self.taxonomy_package_metadata,
            )
            discoverer.start_discovering()
            sys.stdout = sys.__stdout__  # restore standard output.
            # sys.stderr = sys.__stderr__
            self.discovered_dts_list.extend(discoverer.dts_list)

    def store_eps(self):
        if self._conf.db["type"] == "postgres":
            self._conf.db["dbname"] = self.db_name
        else:
            self._conf.db["file"] = self.db_name
        sqlwriter = DtsToSql()
        for dts in self.discovered_dts_list:
            print(f" Saving to the database: {dts.entrypoint_name}")
            with open(
                join(self._conf.logging["maindir"], self._conf.logging["xbrl_cli"]),
                "a+",
                1,
            ) as outfile:
                sys.stdout = outfile
                print(f" Saving to the database: {dts.entrypoint_name}")
                sqlwriter.set_dts(dts)
                sys.stdout = sys.__stdout__

    def refresh_cache(self):
        from discover_xbrl.api.pre_fill_cache import ApiCacher

        update_cache = self._conf.api.get("update_cache", True)
        if update_cache or self.cache:
            api_cacher = ApiCacher(ep_list=self.ep_list, nt_version=self.db_name)
            api_cacher.cache_api()
        else:
            print("Not updating HTML-Cache.")

    @property
    def db_name(self):
        if not self.nt_version:
            if self._conf.db["type"] == "postgres":
                return self._conf.db["name"]
            else:
                return self._conf.db["file"]
        if self.sub_version > "":
            _db_name = f"{self.nt_version}_{self.sub_version}"
        else:
            _db_name = self.nt_version.replace("-", "_")
        if self._conf.db["type"] == "postgres":
            return _db_name.lower()
        else:
            return f"db/{_db_name}.db"

    def handle_taxonomy_package(self):
        with open(
            join(self._conf.logging["maindir"], self._conf.logging["xbrl_cli"]), "a+"
        ) as outfile:
            sys.stdout = outfile

            print("Handling taxonomy package.\n")
            names = self.ep_list.copy()
            self.ep_list = []
            self.taxonomy_package_metadata = read_taxonomy_package(
                self.taxonomy_package
            )
            ep_list = self.taxonomy_package_metadata["entrypoints"]
            # probably leave this out, now that we can alter this on the client?
            # or better, make this a function and only call if no version is sent.
            self.nt_version = version_from_identifier(
                self.taxonomy_package_metadata["metadata"].get("identifier")
            )
            print(f"Using NT Version: {self.nt_version}")
            sys.stdout = sys.__stdout__

        # filter ep_list op eventueel lijstje van de client. Matcht op entrypoint.name
        # filter ep_list op, alfa, beta and final. Those can be in the same package
        for ep in ep_list:
            if len(names) and ep.name not in names:
                continue
            if ep.version:
                if self.sub_version == "alfa" and not ep.version.endswith(".a"):
                    continue
                if self.sub_version == "beta" and not ep.version.endswith(".b"):
                    continue
                if self.sub_version == "" and (
                    ep.version.endswith(".a") or ep.version.endswith(".b")
                ):
                    continue

            self.ep_list.append(ep)

        save_taxonomy_memfs(taxonomy_package=self.taxonomy_package)
        self._conf.parser["has_taxonomy_package"] = True

    def update_db_list(self):
        if self.store:
            if self._conf.db["type"] == "postgres":
                db_names = self._conf.db.get("dbnames")
                if db_names and self.db_name not in db_names.keys():
                    self._conf.db["dbnames"][self.db_name] = self.db_name
                    self._conf.save()
            else:
                db_files = self._conf.db["files"]
                db_key = self.nt_version
                if self.sub_version:
                    db_key += f".{self.sub_version[0]}"
                if db_key not in db_files.keys():
                    self._conf.db["files"][self.db_name] = f"db/{self.db_name}.db"
                    self._conf.save()

    def send_notification(self):
        with open(
            join(self._conf.logging["maindir"], self._conf.logging["xbrl_cli"]), "a+"
        ) as outfile:
            if self._conf.admin.get("send_notification"):
                try:
                    server = smtplib.SMTP(
                        self._conf.admin.get("smtp_server"),
                        port=self._conf.admin.get("smtp_port", 25),
                    )
                    message = self.format_message()
                    if self._conf.admin.get("smtp_user") and self._conf.admin.get(
                        "smtp_password"
                    ):
                        server.login(
                            self._conf.admin["smtp_user"],
                            self._conf.admin["smtp_password"],
                        )
                    email = EmailMessage()
                    email["Subject"] = "XBRL Toolkit. Batchjob done."
                    email[
                        "From"
                    ] = "Kenniscentrum XBRL tooling <kc-xbrl.tooling@cooljapan.nl>"
                    email["To"] = self.notify
                    email["Date"] = formatdate(localtime=True)
                    email.set_content(message)
                    server.send_message(email)
                    outfile.write("Fetching the pigeons\n")
                except Exception as error:
                    print("Sending email failed")
                    print(error)
            else:
                outfile.write(f"Not notifying {self.notify} because of config")

    def format_message(self):
        message = "Job done!\n\nWe've parsed"
        if self.store:
            message += " stored"
        if self.validate_nt:
            message += " validated"
        if self.compare:
            message += f" compared domain {self.run_id}"
        message += ":\n"
        for ep in self.ep_list:
            message += f"{ep}\n"
        if self.validate_nt:
            message += "--- validation reports ---\n"
            for ep in self.ep_list:
                url = Report.get_reportname_from_name(ep)
                url = "https://" + (
                    self._conf.validator.get("servername", "kc-xbrl.cooljapan.nl")
                    + "/nta/validation-reports"
                    + url
                )
                message += f"{url}\n"
        if self._conf.api.get("update_cache", True):
            message += "\ncache update has been started."
        return message

    def run(self):
        with open(
            join(self._conf.logging["maindir"], self._conf.logging["xbrl_cli"]), "w"
        ) as outfile:  # this trruncate previous runs
            outfile.write("XBRL Tooling started\n")

            if self.build_ep_list:
                self.create_ep_list()
            elif self.taxonomy_package:
                if not self.ep_list:
                    self.ep_list = []
                self.handle_taxonomy_package()
            else:
                self.ep_list = (
                    self._conf.parser.get("default_ep_list")
                    if not self.ep_list
                    else self.ep_list
                )
            # Reduce memory consumption by parsing and storing and then deleting a parsed ep
            print(f"Start parsing \nvalidating: {self.validate_nt}\n")
        for ep in sorted(self.ep_list):
            if self.parse:
                self.parse_eps(ep)
            if self.store:
                self.store_eps()
            self.discovered_dts_list = []
        self._conf.parser["has_taxonomy_package"] = False
        self.update_db_list()

        if self.compare and self.run_id:
            self.compare_versions()

        if self.notify:
            self.send_notification()
        with open(
            join(self._conf.logging["maindir"], self._conf.logging["xbrl_cli"]), "a+"
        ) as outfile:
            outfile.write(f"all done\n")

        if self.store or self.cache:
            self.refresh_cache()

    def compare_versions(self):
        compare_versions = self.compare_list()
        print(f"Start batch comparing {self.db_name} with:", end="")

        for old_db in compare_versions:
            print(f'{old_db["value"]}')
            compare = VersionCompare(
                old_db=old_db["value"], new_db=self.db_name, run_id=self.run_id
            )
            compare.create_report()
            compare.create_domeinen_reports()
            compare.create_ep_reports()

    def compare_list(self):
        # Builds a list of older NT-Versions up and until the last production release
        # so if db_name is nt17 it will return nt16, nt17_alfa and nt17_beta,
        #                  nt17_alfa will return nt16
        from discover_xbrl.api.api_server import get_nt_versions  # otherwise circulair

        available_versions = get_nt_versions()
        compare_with = []
        if self.db_name:
            for _index, obj in enumerate(available_versions["nt_versions"]):
                if obj["value"] == self.db_name:
                    _index += 1
                    break
            else:
                _index = 0  # Danger, danger :-) You wanted this, You get this!
                # IF you give 'NT18_alfa' and the list only goes to 'NT17' you'll get just that!
            if _index is not None:
                for obj in available_versions["nt_versions"][_index:]:
                    compare_with.append(obj)
                    if "_alfa" not in obj["value"] and "_beta" not in obj["value"]:
                        break
        compare_with.reverse()
        return compare_with


def main():
    """
    Reads the command line arguments or displays some help
    Running without any arguments will start the default
    Parse: True
    Validate: False
    Store: True
    Compare: False
        Run_Id: ""
    local_taxonomy_dir: from config
    ep_list: from config
    notify: from config
    """
    cli = CLI()
    default_local_taxonomy_dir = cli._conf.parser.get("local_taxonomy_dir")
    arg_parser = argparse.ArgumentParser(
        description="CLI for XBRL Toolkit. "
        "This tool will parse, validate and store the taxonomies found in: \n"
        f"{cli._conf.parser['default_ep_list']} \n"
        f"Writing to {cli._conf.db['type']}."
        "this can be overridden by flags and options."
    )
    arg_parser.add_argument(
        "-l", "--local_taxonomy_dir", default=default_local_taxonomy_dir
    )
    arg_parser.add_argument(
        "--parse",
        type=bool,
        help="Parse the taxonomies",
        default=True,
        action=argparse.BooleanOptionalAction,
    )
    arg_parser.add_argument(
        "--cache",
        action=argparse.BooleanOptionalAction,
        type=bool,
        help="Cachen zonder parsen",
        default=False,
    )
    arg_parser.add_argument(
        "--validate",
        action=argparse.BooleanOptionalAction,
        type=bool,
        help="Validate the taxonomies (NTA Validation)",
        default=False,
    )
    arg_parser.add_argument(
        "--store",
        type=bool,
        help="Store to DB",
        default=True,
        action=argparse.BooleanOptionalAction,
    )
    arg_parser.add_argument(
        "--compare",
        type=bool,
        help="Compare with previous version(s)",
        default=False,
        action=argparse.BooleanOptionalAction,
    ),
    arg_parser.add_argument(
        "--run_id",
        choices=["SBR", "BD", "BZK", "KVK", "OCW", None],
        default=None,
        help="Which domein should be compared? -only used if --compare is present.",
    )
    arg_parser.add_argument(
        "--build_ep_list",
        type=bool,
        action=argparse.BooleanOptionalAction,
        help="Build a fresh EP list, based on local_taxonomy_dir and nt_version. "
        "If False; config.yaml.default_ep_list will be used",
        default=False,
    )
    arg_parser.add_argument(
        "--ep_list", help="Space separated list of taxonomies to parse", nargs="*"
    )
    arg_parser.add_argument(
        "--alfa",
        type=bool,
        help="Build an Alfa EP-list. (only if --build_ep_list)",
        action=argparse.BooleanOptionalAction,
        default=False,
    )
    arg_parser.add_argument(
        "--beta",
        type=bool,
        help="Build a Beta EP-list. (only if --build_ep_list)",
        action=argparse.BooleanOptionalAction,
        default=False,
    )
    arg_parser.add_argument(
        "--taxonomy_package",
        type=str,
        help="Full pathname to taxonomy package. (instead of entrypoint list)",
        default="",
    )
    arg_parser.add_argument(
        "--notify", default="", help="Valid email address to notify if batch is ready "
    )
    arg_parser.add_argument(
        "-n", "--nt_version", default="nt17", help="NT Version to build the EP list "
    )
    parsed = arg_parser.parse_args()
    cli.local_taxonomy_dir = parsed.local_taxonomy_dir
    cli.parse = parsed.parse
    cli.validate_nt = parsed.validate
    cli.store = parsed.store
    cli.compare = parsed.compare
    cli.cache = parsed.cache
    cli.run_id = parsed.run_id
    cli.alfa = parsed.alfa
    cli.beta = parsed.beta
    cli.build_ep_list = parsed.build_ep_list
    cli.nt_version = parsed.nt_version
    cli.notify = parsed.notify
    cli.taxonomy_package = parsed.taxonomy_package
    cli.ep_list = parsed.ep_list

    print(f"Version {cli.nt_version}", end=" ")
    if cli.alfa:
        print("alfa", end="")
    if cli.beta:
        print("beta", end="")
    print("")
    print(f"Voor opslag wordt gebruikt: {cli._conf.db['type']} {cli.db_name}")
    print(f"Parse: {cli.parse}")
    print(f"Store: {cli.store}")
    print(f"Validate: {cli.validate_nt}")
    if cli.taxonomy_package:
        print(f"Using taxonomy-package: {cli.taxonomy_package}")
    elif cli.build_ep_list:
        print(f"Building EP List from {cli.local_taxonomy_dir}")
    else:
        print(f"Entrypoints: {cli._conf.parser.get('default_ep_list')}")
    if cli.notify:
        print(f"When finished will email: {cli.notify}")

    cli.run()
    cli.update_db_list()


if __name__ == "__main__":
    main()
