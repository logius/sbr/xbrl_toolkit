from PySide2.QtWebEngineWidgets import QWebEngineView
from PySide2.QtWidgets import (
    QDockWidget,
    QLabel,
    QLineEdit,
    QPushButton,
    QTableWidget,
    QTableWidgetItem,
    QGroupBox,
    QHBoxLayout,
    QVBoxLayout,
    QTreeWidget,
    QTreeWidgetItem,
)
from PySide2.QtCore import Qt, QUrl


class HyperCubeWidget:
    def __init__(self, parent=None, showdetail=True):
        self.viewbox = QGroupBox()
        self.showdetail = showdetail
        self.parent = parent
        self.table = None
        self.search_filter = None
        self.table_groupbox = self.get_table_groupbox()
        layout = QHBoxLayout()
        layout.addWidget(self.table_groupbox)
        if self.showdetail:
            self.detail_view = QDockWidget()
            layout.addWidget(self.detail_view)
        self.viewbox.setLayout(layout)
        self.lineitems_table = None
        self.browser = None
        self.dts = None

    def get_table_groupbox(self):
        table_groupbox = QGroupBox()
        search_groupbox = QGroupBox()
        searchfield = QLineEdit()
        searchfield.textChanged.connect(self.set_search)
        searchbutton = QPushButton("Filter")
        searchbutton.clicked.connect(self.filter_table)
        searchfield.returnPressed.connect(self.filter_table)
        self.table = QTableWidget()
        self.table.setColumnCount(3)
        self.table.setHorizontalHeaderLabels(["definition", "type", "roleURI"])
        layout = QHBoxLayout()
        layout.addWidget(searchfield)
        layout.addWidget(searchbutton)
        search_groupbox.setLayout(layout)
        layout = QVBoxLayout()
        layout.addWidget(search_groupbox)
        layout.addWidget(self.table)
        table_groupbox.setLayout(layout)
        return table_groupbox

    def set_search(self, data):
        self.search_filter = data

    def filter_table(self):
        self.table.setRowCount(0)
        self.fill_listview(dts=self.dts)

    def fill_listview(self, dts=None):
        if not dts:
            return False
        self.dts = dts
        self.table.setSortingEnabled(False)
        for hypercube in dts.hypercubes:
            row_pos = self.table.rowCount()
            if self.search_filter:
                if (
                    self.search_filter.lower()
                    not in hypercube.linkrole.definition.lower()
                ):
                    continue
            self.table.insertRow(row_pos)
            self.table.setItem(
                (row_pos), 0, QTableWidgetItem(str(hypercube.linkrole.definition))
            )
            self.table.setItem((row_pos), 1, QTableWidgetItem(str(hypercube.hc_type)))
            self.table.setItem(
                (row_pos), 2, QTableWidgetItem(str(hypercube.linkrole.roleURI))
            )
        if self.showdetail:
            self.table.currentItemChanged.connect(self.detail_row_view)
        self.table.resizeColumnToContents(0)
        self.table.verticalHeader().hide()
        self.table.setSortingEnabled(True)
        status = f"{self.table.rowCount()} hypercubes"
        if self.search_filter:
            status = f"{status} (filter: {self.search_filter})"
        self.parent.set_status(status)

    def detail_row_view(self, cell_item):
        if not hasattr(cell_item, "text") or not cell_item.text():
            return False
        found = False
        for hypercube in self.dts.hypercubes:
            if hypercube.linkrole.definition == cell_item.text():
                found = True
                break
        if not found:
            return False
        groupbox = QGroupBox()
        layout = QVBoxLayout()
        lineitems_title = QLabel("Line items")
        layout.addWidget(lineitems_title)
        self.lineitems_table = self.get_lineitems(hypercube)
        layout.addWidget(self.lineitems_table)
        if hypercube.dimensions:
            dimensionTitle = QLabel("Dimensions")
            layout.addWidget(dimensionTitle)
            dimension_treeview = self.get_dimensions(hypercube)
            layout.addWidget(dimension_treeview)
        presentation_layout = self.get_presentation(hypercube=hypercube, lang="nl")
        layout.addLayout(presentation_layout)
        groupbox.setLayout(layout)
        self.detail_view.setWidget(groupbox)
        self.detail_view.show()

    def get_lineitems(self, hypercube):
        detailTable = QTableWidget()
        detailTable.setColumnCount(5)
        detailTable.setHorizontalHeaderLabels(
            ["Order", "Name", "Type", "Required", "typedDomain"]
        )
        detailTable.verticalHeader().hide()

        for order, lineitem in hypercube.lineItems:
            row_pos = detailTable.rowCount()
            detailTable.insertRow(row_pos)
            q_order = QTableWidgetItem()
            q_order.setData(Qt.EditRole, order)
            q_elementtype = QTableWidgetItem()
            required = "*" if lineitem.nillable == "false" else lineitem.nillable
            q_elementtype.setData(Qt.EditRole, lineitem.element_type.name)
            domainref = lineitem.typedDomainRef
            detailTable.setItem(row_pos, 0, q_order)
            detailTable.setItem(row_pos, 1, QTableWidgetItem(lineitem.name))
            detailTable.setItem(row_pos, 2, q_elementtype)
            detailTable.setItem(row_pos, 3, QTableWidgetItem(required))
            detailTable.setItem(row_pos, 4, QTableWidgetItem(domainref))
        detailTable.resizeColumnToContents(0)
        detailTable.resizeColumnToContents(1)
        detailTable.setSortingEnabled(True)
        detailTable.currentItemChanged.connect(self.scroll_to_concept)
        return detailTable

    def scroll_to_concept(self, item):
        print(
            f"row: {item.row()} text: {self.lineitems_table.item(item.row(), 1).text()}"
        )
        name = self.lineitems_table.item(item.row(), 1).text()
        # self.browser.setUrl(QUrl(f"#{name}"))
        print("Qt maakt scroll to anchor lastig, helaas,dit is eenvoudig op t web")

    @staticmethod
    def get_dimensions(hypercube):
        dimension_treeview = QTreeWidget()
        dimension_treeview.setColumnCount(1)
        dimension_treeview.setHeaderLabels(["Dimensions"])

        for dimension in hypercube.dimensions:
            row_pos = dimension_treeview.topLevelItemCount()
            widget_item = QTreeWidgetItem([str(dimension)])
            dimension_treeview.insertTopLevelItem(row_pos, widget_item)
            if dimension.members:
                for domain_member in dimension.members:
                    sub_item = QTreeWidgetItem([str(domain_member)])
                    widget_item.addChild(sub_item)
            elif dimension.domains:
                for domain in dimension.domains:
                    sub_item = QTreeWidgetItem([str(domain)])
                    for member in domain.members:
                        memb_item = QTreeWidgetItem([str(member)])
                        sub_item.addChild(memb_item)
                    widget_item.addChild(sub_item)
        dimension_treeview.expandAll()
        return dimension_treeview

    @staticmethod
    def get_dimensions_html(hypercube=None, lang=None):
        if not hypercube or not hypercube.dimensions:
            return ""
        retval = "<details><summary>Dimensions (click for details)</summary>"
        retval += "<table>"
        for dimension in hypercube.dimensions:
            retval += "<tr>\n"
            retval += (
                f"<td colspan='2' class='dimension'>"
                f"<h6 class='dimension'>{get_label(dimension.concept, lang=lang)}</h6></td>"
            )
            retval += "</tr>"
            doc = get_documentation(dimension.concept, lang=lang)
            if doc:
                retval += "<tr>\n"
                retval += f"<td colspan='2' class='dimension_documentation'>{doc}</td>"
                retval += "</tr>"

            for domain_member in dimension.members:
                if domain_member.usable:
                    retval += "<tr>\n"
                    doc = get_documentation(domain_member.concept, lang=lang)
                    if doc:
                        retval += (
                            f"<td class='domain_member'>"
                            f"{get_label(domain_member.concept, lang=lang)}</td>"
                        )
                        retval += f"<td class='domain_member_documentation'>{doc}</td>"
                    else:
                        retval += (
                            f"<td colspan='2' class='domain_member'>"
                            f"{get_label(domain_member.concept, lang=lang)}</td>"
                        )
                    retval += "</tr>"
            for domain in dimension.domains:
                retval += "<tr>\n"
                doc = get_documentation(domain.concept, lang=lang)
                if doc:
                    retval += (
                        f"<td class='domain'>"
                        f"{get_label(domain.concept, lang=lang)}</td>"
                    )
                    retval += f"<td class='domain_documentation'>{doc}</td>"
                else:
                    retval += (
                        f"<td colspan='2' class='domain'>"
                        f"{get_label(domain.concept, lang=lang)}</td>"
                    )
                retval += "</tr>"
                for member in domain.members:
                    retval += "<tr>\n"
                    doc = get_documentation(member.concept, lang=lang)
                    if doc:
                        retval += (
                            f"<td class='domain_member'>"
                            f"{get_label(member.concept, lang=lang)}</td>"
                        )
                        retval += f"<td class='domain_member_documentation'>{doc}</td>"
                    else:
                        retval += (
                            f"<td colspan='2' class='domain_member'>"
                            f"{get_label(member.concept, lang=lang)}</td>"
                        )

                    retval += "</tr>"
        retval += "</table></details>"
        return retval

    def get_presentation(self, hypercube, lang=None):
        if lang is None:
            lang = "nl"
        self.browser = QWebEngineView()
        self.browser.setHtml(HyperCubeWidget().get_presentation_html(hypercube, lang))
        layout = QVBoxLayout()
        layout.addWidget(self.browser)
        print(self.browser.url())
        return layout

    @staticmethod
    def get_presentation_html(hypercube, lang=None, full_page=True):
        if lang is None:
            lang = "nl"
        if full_page:
            output = (
                f"<!DOCTYPE html><html lang='{lang}'><head>"
                "<meta charset='utf-8' />"
                f"<title>hypercube</title>{add_css()}</head><body>"
            )
        else:
            output = ""
        output += "<div class='dimensions'>"
        output += HyperCubeWidget.get_dimensions_html(hypercube, lang=lang)
        output += "</div>\n"
        output += "<form>\n"
        zebra = 0
        for order, lineitem in hypercube.lineItems:
            zebra += 1
            zebraclass = "odd" if zebra % 2 == 0 else "even"
            output += f"<div class='{zebraclass} lineitem'>\n"
            output += f"<div class='label_input'>"
            output += (
                f"<label for='{lineitem.name}'>{get_label(concept=lineitem, lang=lang)}"
            )
            if lineitem.nillable == "false":
                output += "<span class='required_field'> * </span>"
            output += "</label>\n"
            if lineitem.element_type.enumerations and len(
                lineitem.element_type.enumerations
            ):
                output += f"<select id='{lineitem.name}'>"
                for choice in lineitem.element_type.enumerations:
                    output += f"<option>{get_label(choice, lang=lang)}</option>"
                output += "</select>"
            else:
                output += f"<input type='text' name='{lineitem.name}' id='{lineitem.name}' value='{lineitem.element_type.name}'>"
            output += "\n</div>\n"
            documentation = get_documentation(lineitem, lang=lang)
            if documentation:
                output += f"<div class='documentation'>{documentation}</div>"
            output += "</div>"
        output += "</form>\n"
        if full_page:
            output += "</body></html>"
        return output


def add_css():
    return """<style>
    h1, h5 {text-align: center;}
    h1 {font-size: 26px;}
    h2 {font-size: 24px;}
    h3 {border-left: 22px solid #333; font-size: 22px; padding-left: 10px;}
    h4 {font-size: 20px;}
    h5 {font-size: 18px;  margin: 0; padding: 5px 10px 10px;}
    h6 {font-size: 16px; border-left: 20px solid #38466F; margin: 10px 0 0 5px; padding-left: 10px;}
    h6.definition, h6.dimension {border-left: None;}
    h6.dimension {text-align: center;}
    h1, h2, h3, h4, h5, h6, table, summary, details, section, p, div {font-family: "Liberation Sans", verdana, "Deja Vu Sans", Helvetica;}
    h5, h6 {color: #38466F;}
    td {vertical-align: top;}
    
    form {font-family: "Liberation Sans", verdana, "Deja Vu Sans",  Helvetica; 
          margin: 5px 10px 22px 22px;}
    label {width: 250px; display: inline-block; margin-right: 5px;}
    input {border: 1px solid #444; min-width: 300px; height: 25px;}
    select {height: 2rem; min-width: 300px}
    .documentation {clear: both; font-size: smaller; padding-left: 255px; color: #38466F; max-width: 600px;}
    .odd {background-color: #eee;}
    .lineitem, .reportable_concept {padding: 10px 5px;}
    .label_input {display: flex; }
    .todo {background: yellow; text-align: center; display: none}
    .dimensions {margin: 5px 10px 25px 10px; background-color: aliceblue;} 
    .definition, dimension {text-align: right;}
    .domain_table {max-width: 800px;}
    .domain_member {font-weight: bold; min-width: 260px;}
    .dimension_reference {font-size: smaller; color: #38466F;}
    .hypercube {border: 1px dotted #38466F; margin-bottom: 22px; padding-top: 10px; padding-right: 17px;}
    </style> 
    """


def get_documentation(concept, lang=None):
    if not concept.labels:
        return False
    if lang is None:
        lang = "nl"
    documentation = False
    for label in concept.labels.values():
        if (
            label.linkRole == "http://www.xbrl.org/2003/role/documentation"
            and label.language == lang
        ):
            documentation = label.text
            break
    if documentation:
        documentation = insert_newlines(documentation)
    return documentation


def get_label(concept, lang=None):
    if not concept.labels:
        return False
    if lang is None:
        lang = "nl"
    found_label = False
    if type(concept.labels) == dict:
        labellist = concept.labels.values()
    else:
        labellist = concept.labels
    for label in labellist:
        if (
            label.linkRole == "http://www.xbrl.org/2003/role/label"
            or label.linkRole == "http://www.xbrl.org/2008/role/label"
        ) and label.language == lang:
            found_label = label.text
            break
    return found_label


def format_references(concept, lang=None):
    reportable_fields = [
        "article",
        "chapter",
        "publisher",
        "number",
        "page",
        "paragraph",
        "section",
        "subsection",
        "subparagraph",
        "clause",
        "subclause",
        "appendix",
        "issuedate",
        "example",
        "exhibit",
        "footnote",
        "sentence",
        "uri",
        "uridate",
    ]
    if not concept.references:
        return ""
    retval = ""
    for reference in concept.references:
        retval += f"<tr><td colspan='2' class='dimension_reference'>{reference.name}: "
        for key in reportable_fields:
            if getattr(reference, key) is None:
                continue
            retval += f"{key}: {getattr(reference, key)} "
        retval += "</td></tr>"
    return retval
    pass


def insert_newlines(text, trailing=True):
    words = text.split(" ")
    output = ""
    for count, word in enumerate(words):
        if count > 0 and count % 12 == 0:
            output += "\n"
        output += f"{word} "
    if trailing:
        output += "\n"
    return output
