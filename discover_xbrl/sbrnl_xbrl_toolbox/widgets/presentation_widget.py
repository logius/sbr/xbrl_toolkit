from PySide2.QtWidgets import (
    QDockWidget,
    QTreeWidget,
    QTreeWidgetItem,
    QTableWidget,
    QTableWidgetItem,
    QGroupBox,
    QHBoxLayout,
    QVBoxLayout,
    QLineEdit,
    QPushButton,
    QFormLayout,
    QScrollArea,
    QMainWindow,
    QTextBrowser,
)
from PySide2.QtWebEngineWidgets import QWebEngineView
from .concepts_widget import ConceptsWidget
from .roletype_widget import RoleTypeDetail
from .presentation_as_html import PresentationAsHTML


class PresentationWidget:
    def __init__(self, parent=None, showdetail=True):
        self.viewbox = QGroupBox()
        self.treeview = None
        self.treeview_filter = None
        self.showdetail = showdetail
        self.parent = parent
        self.treeview_groupbox = self.get_presentation_groupbox()
        self.lang = "nl"
        self.parent = parent

        layout = QHBoxLayout()
        layout.addWidget(self.treeview_groupbox)
        if self.showdetail:
            self.detailWidget = QDockWidget()
            layout.addWidget(self.detailWidget)
        self.viewbox.setLayout(layout)
        self.dts = None

    def get_presentation_groupbox(self):
        presentation_groupbox = QGroupBox()
        search_groupbox = QGroupBox()
        searchfield = QLineEdit()
        searchfield.textChanged.connect(self.set_search)
        searchbutton = QPushButton("Filter")
        searchbutton.clicked.connect(self.filter_table)
        searchfield.returnPressed.connect(self.filter_table)
        view_as_html_btn = QPushButton("View as html")
        view_as_html_btn.clicked.connect(self.view_as_html)
        switch_lang_btn = QPushButton("Switch language")
        switch_lang_btn.clicked.connect(self.set_lang)
        btn_layout = QHBoxLayout()
        btn_layout.addWidget(view_as_html_btn)
        btn_layout.addWidget(switch_lang_btn)
        self.treeview = QTreeWidget()
        self.treeview.setColumnCount(4)
        self.treeview.setHeaderLabels(["Presentation", "Label", "Order", ""])
        layout = QHBoxLayout()
        layout.addWidget(searchfield)
        layout.addWidget(searchbutton)
        search_groupbox.setLayout(layout)
        layout = QVBoxLayout()
        layout.addWidget(search_groupbox)
        layout.addWidget(self.treeview)
        layout.addLayout(btn_layout)
        presentation_groupbox.setLayout(layout)
        return presentation_groupbox

    def set_lang(self, lang=None):
        if lang and lang in ["de", "en", "fr", "nl"]:
            self.lang = lang
        else:
            if self.lang == "nl":
                self.lang = "en"
            else:
                self.lang = "nl"
        self.treeview.clear()
        self.fill_listview(dts=self.dts)

    def set_search(self, data):
        self.treeview_filter = data

    def filter_table(self):
        self.treeview.clear()
        self.fill_listview(dts=self.dts)

    def fill_listview(self, dts=None):
        if not dts:
            return False
        self.dts = dts
        self.treeview.setSortingEnabled(False)
        if self.dts.presentation_hierarchy:
            row_pos = self.treeview.topLevelItemCount()
            ident = self.dts.presentation_hierarchy.object.id
            nodetype = "roletype"
            if (
                hasattr(self.dts.presentation_hierarchy.object, "name")
                and self.dts.presentation_hierarchy.object.name
            ):
                ident = self.dts.presentation_hierarchy.object.name
                nodetype = "concept"

            widget_item = QTreeWidgetItem(
                [
                    ident,
                    self.get_label(self.dts.presentation_hierarchy),
                    self.dts.presentation_hierarchy.order,
                    nodetype,
                ]
            )
            self.treeview.insertTopLevelItem(row_pos, widget_item)
            for child in self.dts.presentation_hierarchy.children:
                subtree = self.get_subtree_items(child)
                if subtree:
                    widget_item.addChild(subtree)
                    subtree.setExpanded(True)
            widget_item.setExpanded(True)

        if self.showdetail:
            self.treeview.currentItemChanged.connect(self.presentation_detail_view)
        self.treeview.resizeColumnToContents(2)
        self.treeview.resizeColumnToContents(0)
        self.treeview.resizeColumnToContents(1)

        status = f"{self.treeview.topLevelItemCount()} presentation hierarchy"
        if self.treeview_filter:
            status = f"{status} (filter: {self.treeview_filter})"
        self.parent.set_status(status)

    def get_subtree_items(self, subtree):
        ident = subtree.object.id
        nodetype = "roletype"
        if hasattr(subtree.object, "name"):
            ident = subtree.object.name
            nodetype = "concept"

        widget_item = QTreeWidgetItem(
            [ident, self.get_label(subtree), subtree.order, nodetype]
        )
        if subtree.children:
            for child in subtree.children:
                sub_item = self.get_subtree_items(child)
                if sub_item:
                    widget_item.addChild(sub_item)
                    sub_item.setExpanded(True)
        if self.treeview_filter:
            if (
                self.treeview_filter.lower() not in ident.lower()
                and widget_item.childCount() == 0
            ):
                return None
        return widget_item

    def presentation_detail_view(self, row):
        type, ident = row.data(3, 0), row.data(0, 0)
        cell = FakeItem(ident)
        if type == "concept":
            concept_widget = ConceptsWidget()
            concept_widget.set_dts(self.dts)
            detail = concept_widget.concept_detail_view(cell, return_widget=True)
            self.detailWidget.setWidget(detail)
            self.detailWidget.show()
            return True
        elif type == "roletype":
            found = False
            for roletype in self.dts.roletypes.values():
                if roletype.id == ident:
                    found = True
                    break
            if not found:
                return False
            roletype_view = RoleTypeDetail(roletype, dts=self.dts, lang=self.lang)
            self.detailWidget.setWidget(roletype_view.groupbox)
            self.detailWidget.show()

    def get_label(self, object):
        if hasattr(object, "labels"):
            for label in object.labels.values():
                if (
                    label.language == self.lang
                    and label.linkRole == "http://www.xbrl.org/2003/role/label"
                ):
                    return label.text
        else:
            print(object)

    def view_as_html(self):
        html_engine = PresentationAsHTML(dts=self.dts, lang=self.lang)
        _ = html_engine.output_html()  # we'll use thefile instead
        HTMLBrowser(parent=self.parent, html=None, file=html_engine.cache_filename)


class HTMLBrowser(QMainWindow):
    def __init__(self, parent=None, html=None, file=None):
        super(HTMLBrowser, self).__init__(parent)
        self.html = html
        self.file = file
        self.setup_ui()
        self.show()

    def setup_ui(self):
        presentation = QGroupBox("Taxonomy presentation")
        canvas = QWebEngineView()
        if self.file:
            canvas.load(f"file:///{self.file}")
        else:
            canvas.setHtml(self.html)
        layout = QVBoxLayout()
        layout.addWidget(canvas)
        presentation.setLayout(layout)
        self.setGeometry(80, 180, 900, 900)
        self.setWindowTitle("Presentation hierarchy")
        self.setCentralWidget(presentation)


class FakeItem:
    def __init__(self, data):
        self.data = data

    def text(self):
        return self.data
