from PySide2.QtWidgets import (
    QComboBox,
    QDockWidget,
    QTableWidget,
    QTableWidgetItem,
    QGroupBox,
    QHBoxLayout,
    QVBoxLayout,
    QLineEdit,
    QTextEdit,
    QPushButton,
    QFormLayout,
    QLabel,
)


class LabelsWidget:
    def __init__(self, parent=None, showdetail=True):
        self.viewbox = QGroupBox()
        self.table = None
        self.table_filter = None
        self.language_filter = None
        self.role_filter = None
        self.language_cb = None
        self.role_cb = None
        self.showdetail = showdetail
        self.parent = parent
        self.label_types = []
        self.languages = []
        self.table_groupbox = self.get_table_groupbox()

        layout = QVBoxLayout()
        layout.addWidget(self.table_groupbox)
        layout.addLayout(self.get_static_filters_layout())
        if self.showdetail:
            self.detailWidget = QDockWidget()
            layout.addWidget(self.detailWidget)
        self.viewbox.setLayout(layout)
        self.dts = None

    def get_table_groupbox(self):
        table_groupbox = QGroupBox()
        search_groupbox = QGroupBox()
        searchfield = QLineEdit()
        searchfield.textChanged.connect(self.set_search)
        searchbutton = QPushButton("Filter")
        searchbutton.clicked.connect(self.filter_table)
        searchfield.returnPressed.connect(self.filter_table)
        self.table = QTableWidget()
        self.table.setColumnCount(4)
        self.table.setHorizontalHeaderLabels(["Text", "lang", "role", "id"])
        self.table.verticalHeader().hide()
        layout = QHBoxLayout()
        layout.addWidget(searchfield)
        layout.addWidget(searchbutton)
        search_groupbox.setLayout(layout)
        layout = QVBoxLayout()
        layout.addWidget(search_groupbox)
        layout.addWidget(self.table)
        table_groupbox.setLayout(layout)
        return table_groupbox

    def set_dts(self, dts=None):
        if dts:
            self.dts = dts

    def set_search(self, data):
        self.table_filter = data

    def filter_table(self):
        self.table.setRowCount(0)
        self.fill_listview(dts=self.dts)

    def fill_listview(self, dts=None):
        if not dts:
            return False
        self.dts = dts
        self.table.setSortingEnabled(False)
        for id, label in self.dts.all_labels.items():
            if self.table_filter:
                if self.table_filter.lower() not in label.text.lower():
                    continue
            if self.language_filter:
                if not label.language == self.language_filter:
                    continue
            role = label.linkRole.split("/")[-1]
            if self.role_filter:
                if not role == self.role_filter:
                    continue
            row_pos = self.table.rowCount()
            self.table.insertRow(row_pos)
            text = label.text[:80] if label.text else ""  # truncate text
            if role not in self.label_types:
                self.label_types.append(role)
            if label.language not in self.languages:
                self.languages.append(label.language)
            self.table.setItem(row_pos, 0, QTableWidgetItem(str(text)))
            self.table.setItem(row_pos, 1, QTableWidgetItem(str(label.language)))
            self.table.setItem(row_pos, 2, QTableWidgetItem(str(role)))
            self.table.setItem(row_pos, 3, QTableWidgetItem(str(id)))

        if self.showdetail:
            self.table.currentItemChanged.connect(self.label_detail_view)
        self.table.resizeColumnToContents(0)
        self.table.resizeColumnToContents(1)
        self.table.resizeColumnToContents(2)
        self.table.setSortingEnabled(True)
        self.fill_static_filters()
        status = f"{self.table.rowCount()} labels"
        if self.table_filter:
            status = f"{status} (filter: {self.table_filter})"
        if self.language_filter:
            status = f"{status} taal: {self.language_filter}"
        if self.role_filter:
            status = f"{status} role: {self.role_filter}"
        self.parent.set_status(status)

    def label_detail_view(self, cell_item, previous_item=None, return_widget=False):
        found = False
        if not cell_item or not cell_item.text():
            return False
        search_id = self.table.item(cell_item.row(), 3).text()
        for label in self.dts.all_labels.values():
            if search_id == label.id:
                found = True
                break
        if not found:
            return False
        groupbox = QGroupBox("Label")
        label_detail_form = QFormLayout()
        for var in label.__dict__.items():
            label = var[0]
            if len(var[1]) < 150:
                field = QLineEdit()
            else:
                field = QTextEdit()
            field.setText(str(var[1]))
            label_detail_form.addRow(label, field)
        layout = QVBoxLayout()
        layout.addLayout(label_detail_form)
        groupbox.setLayout(layout)
        if return_widget:
            return groupbox
        self.detailWidget.setWidget(groupbox)
        self.detailWidget.show()

    def set_language_filter(self, index):
        if index == 0:
            self.language_filter = None
        else:
            self.language_filter = self.language_cb.currentData()
        self.table.setRowCount(0)
        self.fill_listview(dts=self.dts)

    def set_role_filter(self, index):
        if index == 0:
            self.role_filter = None
        else:
            self.role_filter = self.role_cb.currentData()
        self.table.setRowCount(0)
        self.fill_listview(dts=self.dts)

    def fill_static_filters(self):
        if self.language_cb.count() == 0:
            self.language_cb.addItem("", None)
            for lang in self.languages:
                self.language_cb.addItem(lang, lang)
            self.language_cb.currentIndexChanged.connect(self.set_language_filter)
        if self.role_cb.count() == 0:
            self.role_cb.addItem("", None)
            for role in self.label_types:
                self.role_cb.addItem(role, role)
            self.role_cb.currentIndexChanged.connect(self.set_role_filter)

    def get_static_filters_layout(self):
        layout = QHBoxLayout()
        layout.addWidget(QLabel("Filter op taal"))
        self.language_cb = QComboBox()
        layout.addWidget(self.language_cb)
        layout.addWidget(QLabel("Filter op rol"))
        self.role_cb = QComboBox()
        layout.addWidget(self.role_cb)
        return layout
