from PySide2.QtWebEngineWidgets import QWebEngineView
from PySide2.QtWidgets import (
    QDockWidget,
    QTreeWidget,
    QTreeWidgetItem,
    QTableWidget,
    QTableWidgetItem,
    QGroupBox,
    QHBoxLayout,
    QVBoxLayout,
    QLineEdit,
    QPushButton,
    QFormLayout,
)
from discover_xbrl.sbrnl_xbrl_toolbox.widgets.presentation_as_html import (
    PresentationAsHTML,
)


class RoleTypeDetail:
    def __init__(self, roletype=None, dts=None, lang=None):
        self.roletype = roletype
        self.groupbox = QGroupBox("roleType content")
        self.dts = dts
        self.lang = lang
        if roletype:
            self.roletype_detail_form()

    def roletype_detail_form(self):
        layout = QVBoxLayout()
        if self.roletype.tables:
            layout.addLayout(self.get_tables_layout(self.roletype.tables))
        if self.roletype.hypercube:
            layout.addLayout(self.get_hypercube_layout(self.roletype.hypercube))
        if self.roletype.variables:
            layout.addLayout(self.get_variables_layout(self.roletype.variables))
        if self.roletype.presentation_hierarchy:
            layout.addLayout(
                self.get_presentation_layout(self.roletype.presentation_hierarchy)
            )
        for reference in self.roletype.references:
            print(f"reference: {reference.id}")
        self.groupbox.setLayout(layout)

    def get_tables_layout(self, tables):
        layout = QFormLayout()
        for table in tables:
            layout.addRow("Table", QLineEdit(table.label))
        return layout

    def get_hypercube_layout(self, hypercube):
        layout = QFormLayout()
        layout.addRow("Hypercube", QLineEdit(hypercube.linkrole.definition))
        return layout

    def get_variables_layout(self, variables):
        layout = QVBoxLayout()
        var_table = QTableWidget()
        var_table.setColumnCount(2)
        var_table.setHorizontalHeaderLabels(["Id", "Fallback"])
        var_table.verticalHeader().hide()
        for variable in variables:
            row_pos = var_table.rowCount()
            var_table.insertRow(row_pos)
            var_table.setItem(row_pos, 0, QTableWidgetItem(str(variable.id)))
            if hasattr(variable, "fallback_value"):
                var_table.setItem(
                    row_pos, 1, QTableWidgetItem(str(variable.fallback_value))
                )
            else:
                var_table.setItem(row_pos, 1, QTableWidgetItem("-"))

        var_table.resizeColumnToContents(0)
        layout.addWidget(var_table)
        return layout

    def get_presentation_layout(self, presentation_hierarchy):
        html = self.get_presentation_hierarchy(presentation_hierarchy)
        canvas = QWebEngineView()
        canvas.setHtml(html)
        layout = QVBoxLayout()
        layout.addWidget(canvas)
        return layout

    def get_presentation_hierarchy(self, presentation_hierarchy):
        html_engine = PresentationAsHTML(
            dts=self.dts, lang=self.lang, presentation_hierarchy=presentation_hierarchy
        )
        return html_engine.output_html()
