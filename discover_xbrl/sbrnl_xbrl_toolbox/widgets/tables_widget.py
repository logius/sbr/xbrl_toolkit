from PySide2.QtWebEngineWidgets import QWebEngineView
from PySide2.QtWidgets import (
    QDockWidget,
    QTreeWidget,
    QTreeWidgetItem,
    QTableWidget,
    QTableWidgetItem,
    QGroupBox,
    QHBoxLayout,
    QVBoxLayout,
    QLineEdit,
    QPushButton,
)
from discover_xbrl.sbrnl_xbrl_toolbox.widgets.hypercube_widget import (
    add_css,
    get_label,
    get_documentation,
)
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.tables.classes import (
    ConceptRelationshipNode,
    DimensionRelationshipNode,
    Aspect,
    AspectNode,
    RuleNode,
)


class TablesWidget:
    def __init__(self, parent=None, showdetail=True):
        self.viewbox = QGroupBox()
        self.treeview = None
        self.treeview_filter = None
        self.showdetail = showdetail
        self.parent = parent
        self.treeview_groupbox = self.get_table_groupbox()
        self.lang = "nl"

        layout = QHBoxLayout()
        layout.addWidget(self.treeview_groupbox)
        if self.showdetail:
            self.detailWidget = QDockWidget()
            layout.addWidget(self.detailWidget)
        self.viewbox.setLayout(layout)
        self.dts = None

    def get_table_groupbox(self):
        table_groupbox = QGroupBox()
        search_groupbox = QGroupBox()
        searchfield = QLineEdit()
        searchfield.textChanged.connect(self.set_search)
        searchbutton = QPushButton("Filter")
        searchbutton.clicked.connect(self.filter_table)
        searchfield.returnPressed.connect(self.filter_table)
        self.treeview = QTreeWidget()
        self.treeview.setColumnCount(3)
        self.treeview.setHeaderLabels(["Table", "Order", "Axis"])
        layout = QHBoxLayout()
        layout.addWidget(searchfield)
        layout.addWidget(searchbutton)
        search_groupbox.setLayout(layout)
        layout = QVBoxLayout()
        layout.addWidget(search_groupbox)
        layout.addWidget(self.treeview)
        table_groupbox.setLayout(layout)
        return table_groupbox

    def set_search(self, data):
        self.treeview_filter = data

    def filter_table(self):
        self.treeview.clear()
        self.fill_listview(dts=self.dts)

    def fill_listview(self, dts=None):
        if not dts:
            return False
        self.dts = dts
        self.treeview.setSortingEnabled(False)
        for table in self.dts.tables:
            widget_item = QTreeWidgetItem([table.id, table.parentChildOrder, None])
            for breakdown_arc in table.table_breakdowns:
                order = str(int(breakdown_arc.order)) if breakdown_arc.order else ""
                sub_item = QTreeWidgetItem(
                    [breakdown_arc.id, order, breakdown_arc.axis]
                )
                if breakdown_arc.children and len(breakdown_arc.children) > 0:
                    for subtree_item in self.get_subtree_items(breakdown_arc.children):
                        sub_item.addChild(subtree_item)
                if self.treeview_filter:
                    if (
                        self.treeview_filter.lower() not in breakdown_arc.id.lower()
                        and sub_item.childCount() == 0
                    ):
                        continue
                widget_item.addChild(sub_item)
            if self.treeview_filter:
                if (
                    self.treeview_filter.lower() not in table.id.lower()
                    and widget_item.childCount() == 0
                ):
                    continue

            row_pos = self.treeview.topLevelItemCount()
            self.treeview.insertTopLevelItem(row_pos, widget_item)

        self.treeview.resizeColumnToContents(0)
        self.treeview.setSortingEnabled(True)
        if self.showdetail:
            self.treeview.currentItemChanged.connect(self.table_detail_view)

        status = f"{self.treeview.topLevelItemCount()} tabellen"
        if self.treeview_filter:
            status = f"{status} (filter: {self.treeview_filter})"
        self.parent.set_status(status)

    def get_subtree_items(self, subtree):
        subtree_items = []
        for node in subtree:
            subtree_item = QTreeWidgetItem([node.id, node.parentChildOrder, ""])
            if node.children:
                for child in node.children:
                    child_item = self.get_child_tree(child)
                    if child_item:
                        subtree_item.addChild(child_item)
            if self.treeview_filter:
                if (
                    self.treeview_filter.lower() not in node.id.lower()
                    and subtree_item.childCount() == 0
                ):
                    continue
            subtree_items.append(subtree_item)
        return subtree_items

    def get_child_tree(self, node):
        child_item = QTreeWidgetItem([node.id])
        if node.children:
            for child in node.children:
                grandchild_item = self.get_child_tree(child)
                if grandchild_item:
                    child_item.addChild(grandchild_item)
        if self.treeview_filter:
            if (
                self.treeview_filter.lower() not in node.id.lower()
                and child_item.childCount() == 0
            ):
                return None
        return child_item

    def table_detail_view(self, cell_item):
        top_item = cell_item
        if not top_item:
            return None
        while top_item.parent():  # traverse the tree up to the top node
            top_item = top_item.parent()
        found = False
        for table in self.dts.tables:
            if top_item.text(0) == table.id:
                found = True
                break
        if not found:
            return False
        groupbox = QGroupBox(f"")
        layout = QVBoxLayout()
        canvas = QWebEngineView()
        canvas.setHtml(
            self.get_table_html(table, dts=self.dts, lang=self.lang, full_page=True)
        )
        layout.addWidget(canvas)
        groupbox.setLayout(layout)
        self.detailWidget.setWidget(groupbox)
        self.detailWidget.show()

    @staticmethod
    def get_table_html(table, dts=None, lang=None, full_page=True):
        if lang is None:
            lang = "nl"
        if full_page:
            output = (
                f"<!DOCTYPE html><html lang='{lang}'><head>"
                "<meta charset='utf-8' />"
                f"<title>Table</title>{add_css()}</head><body>"
            )
        else:
            output = ""
        output += "<table border='1px'>"
        # I need X and Y in order, neither are compulsory
        # if x is absent, we get 1 column,
        # if y is absent, we get 1 row.
        columns = 1
        rows = 1
        has_x = False
        has_y = False
        cells = []
        for table_breakdown in table.table_breakdowns:
            if table_breakdown.axis == "x":
                has_x = True
                t_heading = TablesWidget().get_table_heading(
                    table_breakdown, dts=dts, lang=lang
                )

        # old loop
        for table_breakdown in table.table_breakdowns:
            output += "<tr>\n"
            output += f"<td>{table_breakdown.axis}</td>\n"
            if table_breakdown.children:
                for child in table_breakdown.children:
                    output += TablesWidget().get_table_child_html(
                        child, table_breakdown.axis, dts, lang
                    )
            output += "</tr>"
        output += "</table></body></html>"
        return output

    @staticmethod
    def get_table_child_html(node, axis, dts, lang):
        html = ""
        if node.children:
            for child in node.children:
                html += TablesWidget().get_table_child_html(child, axis, dts, lang)
        else:
            print(axis)
            html += "<td>"
            if isinstance(node, ConceptRelationshipNode):
                html += TablesWidget().get_prefixed_concept(
                    node.relationship_source, dts, lang
                )
                if hasattr(node.parent, "rulesets"):
                    html += "<br/>"
                    html += TablesWidget().get_ruleset_html(
                        node.parent.rulesets, dts, lang
                    )
            elif isinstance(node, DimensionRelationshipNode):
                html += TablesWidget().get_prefixed_concept(node.dimension, dts, lang)
                if hasattr(node.parent, "rulesets"):
                    html += "<br/>"
                    html += TablesWidget().get_ruleset_html(
                        node.parent.rulesets, dts, lang
                    )
            elif isinstance(node, Aspect) or isinstance(node, AspectNode):
                if hasattr(node.aspect, "value"):
                    html += TablesWidget().get_prefixed_concept(
                        node.aspect.value, dts, lang
                    )
                else:
                    print("What's wrong with this aspect?")
                    print(node.aspect.__dict__)
            elif hasattr(node, "rulesets"):
                if hasattr(node.parent, "rulesets"):
                    html += "<br/>"
                    html += TablesWidget().get_ruleset_html(
                        node.parent.rulesets, dts, lang
                    )
                html += TablesWidget().get_ruleset_html(node.rulesets, dts, lang)
            else:
                print(f"blijft over: {type(node)}")
            # html += f"<td>{axis} {type(node)}</td>\n"
            html += "</td>\n"

        return html

    @staticmethod
    def get_ruleset_html(rulesets, dts, lang):
        html = ""
        for formula in rulesets:
            if hasattr(formula, "periodtype"):
                html += f"{formula.periodtype}"
                if not hasattr(formula, "value"):
                    if hasattr(formula, "start"):
                        html += f" start: {formula.start} end: {formula.end}"
                else:
                    html += f" value:{formula.value}"
            elif hasattr(formula, "dimension"):
                dimension = TablesWidget().get_prefixed_concept(
                    formula.dimension, dts, lang
                )
                member = TablesWidget().get_prefixed_concept(formula.member, dts, lang)
                html += f"Dimension: {dimension}<br>member: {member}<br>"
            elif hasattr(formula, "formulas"):
                print(TablesWidget().get_ruleset_html(formula.formulas, dts, lang))
                html += TablesWidget().get_ruleset_html(formula.formulas, dts, lang)
            else:
                print(type(formula))
                print(formula.__dict__)
        return html

    @staticmethod
    def get_prefixed_concept(prefixed_name, dts, lang):
        prefix, name = prefixed_name.split(":")
        content = ""
        for concept in dts.concepts.values():
            if concept.ns_prefix == prefix and concept.name == name:
                label = get_label(concept=concept, lang=lang)
                if label:
                    content += label
                else:
                    content += f"{concept.name} (no label)"
        return content

    @staticmethod
    def get_table_heading(table_breakdown, dts, lang):
        """We know the width in columns (leaves),
        for each level in the axis we need to add a heading row,
        """
        output = "<thead>\n"
        columns = len(table_breakdown.leaves)
        for node in table_breakdown.children:
            output += TablesWidget().get_heading_row(node, dts=dts, lang=lang)
        output += "</thead>\n"
        print(output)
        return output

    @staticmethod
    def get_heading_row(node, dts, lang):
        output = "<tr>"
        output += f"<th colspan='{len(node.leaves)}'></th>"
        output += "</tr>\n"
        for child in node.children:
            output += TablesWidget().get_heading_row(child, dts=dts, lang=lang)
        return output
