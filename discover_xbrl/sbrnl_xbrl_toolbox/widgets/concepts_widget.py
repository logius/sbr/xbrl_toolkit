from PySide2.QtWidgets import (
    QDockWidget,
    QTableWidget,
    QTableWidgetItem,
    QGroupBox,
    QHBoxLayout,
    QVBoxLayout,
    QLineEdit,
    QPushButton,
    QFormLayout,
    QLabel,
)
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.tables.classes import (
    ConceptRelationshipNode,
    DimensionRelationshipNode,
    Aspect,
    AspectNode,
    RuleNode,
)


class ConceptsWidget:
    def __init__(self, parent=None, showdetail=True):
        self.viewbox = QGroupBox()
        self.table = None
        self.table_filter = None
        self.showdetail = showdetail
        self.parent = parent
        self.table_groupbox = self.get_table_groupbox()

        layout = QHBoxLayout()
        layout.addWidget(self.table_groupbox)
        if self.showdetail:
            self.detailWidget = QDockWidget()
            layout.addWidget(self.detailWidget)
        self.viewbox.setLayout(layout)
        self.dts = None

    def get_table_groupbox(self):
        table_groupbox = QGroupBox()
        search_groupbox = QGroupBox()
        searchfield = QLineEdit()
        searchfield.textChanged.connect(self.set_search)
        searchbutton = QPushButton("Filter")
        searchbutton.clicked.connect(self.filter_table)
        searchfield.returnPressed.connect(self.filter_table)
        self.table = QTableWidget()
        self.table.setColumnCount(3)
        self.table.setHorizontalHeaderLabels(["Name", "Abstract", "Type"])
        self.table.verticalHeader().hide()
        layout = QHBoxLayout()
        layout.addWidget(searchfield)
        layout.addWidget(searchbutton)
        search_groupbox.setLayout(layout)
        layout = QVBoxLayout()
        layout.addWidget(search_groupbox)
        layout.addWidget(self.table)
        table_groupbox.setLayout(layout)
        return table_groupbox

    def set_dts(self, dts=None):
        if dts:
            self.dts = dts

    def set_search(self, data):
        self.table_filter = data

    def filter_table(self):
        self.table.setRowCount(0)
        self.fill_listview(dts=self.dts)

    def fill_listview(self, dts=None):
        if not dts:
            return False
        self.dts = dts
        self.table.setSortingEnabled(False)
        for name, concept in self.dts.concepts.items():
            if self.table_filter:
                if self.table_filter.lower() not in concept.name.lower():
                    continue
            row_pos = self.table.rowCount()
            self.table.insertRow(row_pos)
            self.table.setItem(row_pos, 0, QTableWidgetItem(str(concept.name)))
            self.table.setItem(row_pos, 1, QTableWidgetItem(str(concept.is_abstract)))
            self.table.setItem(
                (row_pos), 2, QTableWidgetItem(str(concept.element_type))
            )

        if self.showdetail:
            self.table.currentItemChanged.connect(self.concept_detail_view)
        self.table.resizeColumnToContents(0)
        self.table.setSortingEnabled(True)
        status = f"{self.table.rowCount()} concepten"
        if self.table_filter:
            status = f"{status} (filter: {self.table_filter})"
        self.parent.set_status(status)

    def get_labels_table(self, dict=None):
        dictListTable = QTableWidget()
        dictListTable.setColumnCount(5)
        dictListTable.setHorizontalHeaderLabels(
            ["Text", "Label", "lang", "linkRole", "xlink_label"]
        )
        dictListTable.verticalHeader().hide()
        for k, v in dict.items():
            row_pos = dictListTable.rowCount()
            dictListTable.insertRow(row_pos)
            dictListTable.setItem(row_pos, 0, QTableWidgetItem(str(v.text)))
            dictListTable.setItem(row_pos, 1, QTableWidgetItem(str(k)))
            dictListTable.setItem(row_pos, 2, QTableWidgetItem(str(v.language)))
            dictListTable.setItem(row_pos, 3, QTableWidgetItem(str(v.linkRole)))
            dictListTable.setItem(row_pos, 4, QTableWidgetItem(str(v.xlink_label)))
        dictListTable.resizeColumnToContents(2)
        dictListTable.resizeColumnToContents(0)
        return dictListTable

    def get_reference_table(self, references=None):
        reference_table = QTableWidget()
        reference_table.setColumnCount(5)
        reference_table.setHorizontalHeaderLabels(
            ["Label", "Naam", "Chapter", "§", "¶"]
        )
        reference_table.verticalHeader().hide()
        for reference in references:
            row_pos = reference_table.rowCount()
            reference_table.insertRow(row_pos)
            reference_table.setItem(row_pos, 0, QTableWidgetItem(reference.label))
            reference_table.setItem(row_pos, 1, QTableWidgetItem(reference.name))
            reference_table.setItem(row_pos, 2, QTableWidgetItem(reference.chapter))
            reference_table.setItem(row_pos, 3, QTableWidgetItem(reference.section))
            reference_table.setItem(row_pos, 4, QTableWidgetItem(reference.paragraph))
        reference_table.resizeColumnToContents(1)
        return reference_table

    def get_occurrences_table(self, concept=None, dts=None):
        if not concept or not dts:
            return None
        occurrences_table = QTableWidget()
        occurrences_table.setColumnCount(2)
        occurrences_table.setHorizontalHeaderLabels(["Type", "Naam"])
        occurrences_table.verticalHeader().hide()

        # print(f"{concept.name} {concept.ns_prefix} {concept.id}")
        for hypercube in dts.hypercubes:
            for _, lineitem in hypercube.lineItems:
                if lineitem.name == concept.name:
                    row_pos = occurrences_table.rowCount()
                    occurrences_table.insertRow(row_pos)
                    occurrences_table.setItem(row_pos, 0, QTableWidgetItem("Hypercube"))
                    occurrences_table.setItem(
                        row_pos, 1, QTableWidgetItem(hypercube.linkrole.definition)
                    )
                    break
        for table in dts.tables:
            for breakdown in table.table_breakdowns:
                for descendant in breakdown.descendants:
                    if isinstance(descendant, RuleNode):
                        for rule in descendant.rulesets:
                            if (
                                hasattr(rule, "member")
                                and rule.member == f"{concept.ns_prefix}:{concept.name}"
                            ):
                                row_pos = occurrences_table.rowCount()
                                occurrences_table.insertRow(row_pos)
                                occurrences_table.setItem(
                                    row_pos, 0, QTableWidgetItem("Table")
                                )
                                occurrences_table.setItem(
                                    row_pos, 1, QTableWidgetItem(table.label)
                                )
                                break
                    elif isinstance(descendant, Aspect) or isinstance(
                        descendant, AspectNode
                    ):
                        if (
                            hasattr(descendant, "value")
                            and descendant.value
                            == f"{concept.ns_prefix}:{concept.name}"
                        ):
                            row_pos = occurrences_table.rowCount()
                            occurrences_table.insertRow(row_pos)
                            occurrences_table.setItem(
                                row_pos, 0, QTableWidgetItem("Table")
                            )
                            occurrences_table.setItem(
                                row_pos, 1, QTableWidgetItem(table.label)
                            )
                            print("Aspect!")
                            break
                    elif isinstance(descendant, ConceptRelationshipNode):
                        if (
                            descendant.relationship_source
                            == f"{concept.ns_prefix}:{concept.name}"
                        ):
                            row_pos = occurrences_table.rowCount()
                            occurrences_table.insertRow(row_pos)
                            occurrences_table.setItem(
                                row_pos, 0, QTableWidgetItem("Table")
                            )
                            occurrences_table.setItem(
                                row_pos, 1, QTableWidgetItem(table.label)
                            )
                    else:
                        if descendant.id == concept.id:
                            print(f"catch me: {descendant.id} - {type(descendant)}")
        if occurrences_table.rowCount() > 0:
            occurrences_table.resizeColumnToContents(0)
            occurrences_table.resizeColumnToContents(1)
            return occurrences_table
        return None

    def concept_detail_view(self, cell_item, previous_item=None, return_widget=False):
        found = False
        if not cell_item or not cell_item.text():
            return False
        for concept in self.dts.concepts.values():
            if hasattr(concept, "name") and cell_item.text() == concept.name:
                found = True
                break
        if not found:
            return False
        groupbox = QGroupBox("Concept")
        concept_detail_form = QFormLayout()
        label_tables = {"labels": None, "references": None}
        for var in concept.__dict__.items():
            if var[0] == "labels":
                if len(var[1]):
                    label_tables["labels"] = self.get_labels_table(var[1])
            elif var[0] == "references":
                label_tables["references"] = self.get_reference_table(var[1])
            else:
                label = var[0]
                field = QLineEdit()
                field.setText(str(var[1]))
                concept_detail_form.addRow(label, field)

        layout = QVBoxLayout()
        layout.addLayout(concept_detail_form)
        for kind, label_table in label_tables.items():
            if label_table:
                header = QLabel(kind)
                layout.addWidget(header)
                layout.addWidget(label_table)
        occurrences = self.get_occurrences_table(dts=self.dts, concept=concept)
        if occurrences:
            layout.addWidget(QLabel("Gebruik"))
            layout.addWidget(occurrences)
        groupbox.setLayout(layout)
        if return_widget:
            return groupbox
        self.detailWidget.setWidget(groupbox)
        self.detailWidget.show()
