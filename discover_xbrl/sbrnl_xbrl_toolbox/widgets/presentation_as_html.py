from os.path import join, exists
from os import mkdir
from discover_xbrl.conf.conf import Config
from discover_xbrl.sbrnl_xbrl_toolbox.widgets.hypercube_widget import (
    add_css,
    HyperCubeWidget,
    get_documentation,
    get_label,
)


class PresentationAsHTML:
    def __init__(self, dts=None, lang=None, presentation_hierarchy=None):
        self.dts = dts
        self.lang = lang if lang else "nl"
        self.presentation_hierarchy = (
            presentation_hierarchy
            if presentation_hierarchy
            else dts.presentation_hierarchy
        )
        self._zebras = 0
        self.conf = Config()

    def output_html(self):
        if not self.presentation_hierarchy:
            return False
        content = self.get_label(self.presentation_hierarchy)
        output = (
            f"<!DOCTYPE html><html lang='{self.lang}'>\n<head>"
            "<meta charset='utf-8'/>"
            f"<title>Presentation Hierarchy: {content}</title>{add_css()}</head><body>\n"
        )
        output += f"<h1>{content}</h1>\n"
        if self.presentation_hierarchy.children:
            for child in self.presentation_hierarchy.children:
                output += self.get_child_output(child)
        output += "</body>\n</html>\n"
        filename = self.cache_filename
        # cache this file, there is a 2MB limit on what we can send the browser,
        # Most DTS files will exceed this limit

        with open(filename, "w+") as htmlfile:
            htmlfile.write(output)
        return output

    def get_child_output(self, node, level=2):
        if level > 6:
            level = 6
        output = f"<div class='level{level}'>"
        if hasattr(node, "name") and node.name:  # Concept
            if node.is_abstract:
                output += f"<h{level}>{self.get_label(node)}</h{level}>\n"
                doc = get_documentation(node)
                if doc:
                    print(doc)
            else:
                self._zebras += 1
                zebraclass = "odd" if self._zebras % 2 == 0 else "even"
                output += f"<div class='{zebraclass} reportable_concept'>\n"
                output += f"<div class='label_input'>"
                output += f"<label for='{node.name}'>{get_label(concept=node, lang=self.lang)}"
                if node.nillable == "false":
                    output += "<span class='required_field'> * </span>"
                output += "</label>\n"
                if node.element_type.enumerations and len(
                    node.element_type.enumerations
                ):
                    output += f"<select id='{node.name}'>"
                    for choice in node.element_type.enumerations:
                        output += (
                            f"<option>{get_label(choice, lang=self.lang)}</option>"
                        )
                    output += "</select>"
                else:
                    output += f"<input type='text' name='{node.name}' id='{node.name}' value='{node.element_type.name}'>"
                output += "\n</div>\n"
                documentation = get_documentation(node, lang=self.lang)
                if documentation:
                    output += f"<div class='documentation'>{documentation}</div>"
                output += "</div>"
        else:
            output += f"<div class='todo'>roletype: {node.definition}</div>\n"
            if node.hypercube:
                output += "<div class='hypercube'>"
                output += f"<h6 class='definition'>{node.definition}</h6>\n"
                output += HyperCubeWidget().get_presentation_html(
                    hypercube=node.hypercube, lang=self.lang, full_page=False
                )
                output += "</div>"
        if node.children:
            for child in node.children:
                output += self.get_child_output(child, level=level + 1)
        output += "</div>"
        return output

    def get_label(self, obj):
        for label in obj.labels.values():
            if (
                label.language == self.lang
                and label.linkRole == "http://www.xbrl.org/2003/role/label"
            ):
                return label.text

    @property
    def cache_filename(self):
        output_dir = "/tmp/html"

        if not exists(output_dir):
            mkdir(output_dir)

        if hasattr(self.conf, "editor"):
            output_dir = self.conf.editor.get("html_cache", output_dir)
        if hasattr(self.presentation_hierarchy, "name"):
            name = self.presentation_hierarchy.name
        else:
            name = self.presentation_hierarchy.id
        name = f"{name}_{self.lang}.html"
        return join(output_dir, name)
