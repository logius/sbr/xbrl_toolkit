import sys
import pickle
from pathlib import Path
from zipfile import ZipFile
from PySide2.QtWidgets import (
    QApplication,
    QVBoxLayout,
    QTabWidget,
    QMainWindow,
    QDockWidget,
)
from discover_xbrl.sbrnl_xbrl_toolbox.widgets.hypercube_widget import HyperCubeWidget
from discover_xbrl.sbrnl_xbrl_toolbox.widgets.concepts_widget import ConceptsWidget
from discover_xbrl.sbrnl_xbrl_toolbox.widgets.tables_widget import TablesWidget
from discover_xbrl.sbrnl_xbrl_toolbox.widgets.labels_widget import LabelsWidget
from discover_xbrl.sbrnl_xbrl_toolbox.widgets.presentation_widget import (
    PresentationWidget,
)
from discover_xbrl.conf.conf import Config


class XBRLEditorGui(QMainWindow):
    def __init__(self, parent=None):
        super(XBRLEditorGui, self).__init__(parent)
        self.conf = Config()
        self.tabs = QTabWidget()
        self.hypercube_widget = HyperCubeWidget(parent=self)
        self.concepts_widget = ConceptsWidget(parent=self)
        self.tables_widget = TablesWidget(parent=self)
        self.labels_widget = LabelsWidget(parent=self)
        self.presentation_widget = PresentationWidget(parent=self)
        self.tabs.addTab(self.concepts_widget.viewbox, "Concepten")
        self.tabs.addTab(self.labels_widget.viewbox, "Labels")
        self.tabs.addTab(self.hypercube_widget.viewbox, "hypercubes")
        self.tabs.addTab(self.tables_widget.viewbox, "Tables")
        self.tabs.addTab(self.presentation_widget.viewbox, "Presentation")
        self.default_status = "SBRNL - XBRL Taxonomy editor"
        if hasattr(self.conf, "editor"):
            if self.conf.editor.get("geometry"):
                self.setGeometry(*self.conf.editor["geometry"])
            else:
                self.setGeometry(50, 150, 1200, 800)
            if self.conf.editor.get("default_status"):
                self.default_status = self.conf.editor["default_status"]
        else:
            self.setGeometry(50, 150, 1200, 800)
        self.setCentralWidget(self.tabs)

        self.set_status()
        self.setWindowTitle("Taxonomy editor")
        self.dts = None

    def set_dts(self, dts=None):
        self.dts = dts
        self.setWindowTitle(f"Taxonomy editor - {dts.entrypoint_name}")
        self.hypercube_widget.fill_listview(dts)
        self.concepts_widget.fill_listview(dts)
        self.tables_widget.fill_listview(dts)
        self.labels_widget.fill_listview(dts)
        self.presentation_widget.fill_listview(dts)

    def set_status(self, message=None):
        if message:
            message = f" - {message}"
        else:
            message = ""
        self.statusBar().showMessage(f"{self.default_status} {message}")


if __name__ == "__main__":
    app = QApplication(sys.argv)
    conf = Config()
    xbrl_editor = XBRLEditorGui()
    if hasattr(conf, "editor"):
        zip_url = Path(conf.editor["load_default_dts"])
        dts_list = []
        if zip_url.name:  # If a file is selected, else pass
            with ZipFile(zip_url, "r") as dts_package_zip:
                for file in dts_package_zip.namelist():
                    if str(file).startswith("dts/"):
                        dts = dts_package_zip.read(file)
                        dts_unpickled = pickle.loads(dts)
                        xbrl_editor.set_dts(dts_unpickled)
    xbrl_editor.show()
    sys.exit(app.exec_())
