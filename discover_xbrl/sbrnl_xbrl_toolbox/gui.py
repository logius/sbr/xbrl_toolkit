import logging

from PySide2.QtCore import Qt, QThreadPool
from PySide2.QtWidgets import (
    QApplication,
    QDockWidget,
    QFileDialog,
    QMainWindow,
    QTextEdit,
)

from discover_xbrl.sbrnl_modules.dts_manager import module as dts_manager_module
from discover_xbrl.sbrnl_modules.exports import module as export_module
from discover_xbrl.sbrnl_modules.preparer_extensions import (
    module as preparer_extensions_module,
)

from discover_xbrl.sbrnl_modules.semantics_checker import (
    module as semantics_check_module,
)
from discover_xbrl.sbrnl_modules.instance import module as instance_module
from discover_xbrl.sbrnl_modules.validators import module as validator_module
from discover_xbrl.sbrnl_taxonomy_builder import module as taxonomy_building_module
from discover_xbrl.sbrnl_xbrl_parser import module as xbrl_parser_module
from discover_xbrl.sbrnl_xbrl_toolbox.widgets.hypercube_widget import HyperCubeWidget
from discover_xbrl.sbrnl_xbrl_toolbox.widgets.concepts_widget import ConceptsWidget
from discover_xbrl.conf.conf import Config


"""
This file is used to start the gui version of the toolbox.
"""


windows = []

# Module dictionary used to add modules to the gui.
module_dict = {}
module_dict["name"] = None
module_dict["gui"] = None


class XBRLProcessorGui(QMainWindow):
    def __init__(self, parent=None):

        super(XBRLProcessorGui, self).__init__(parent)

        self.discovered_dts = None
        self.discovered_dts_list = []
        self.dispatch_list = []
        self.validator = None
        self.concepts_widget = ConceptsWidget(parent=self, showdetail=False)
        self.hypercube_widget = HyperCubeWidget(parent=self, showdetail=False)

        # Set central widget
        conceptWindow = QDockWidget()
        conceptWindow.setWidget(self.concepts_widget.viewbox)
        conceptWindow.setAttribute(Qt.WA_DeleteOnClose)
        hypercubeWindow = QDockWidget()
        hypercubeWindow.setWidget(self.hypercube_widget.viewbox)
        hypercubeWindow.setAttribute(Qt.WA_DeleteOnClose)
        self.setCentralWidget(conceptWindow)

        self.default_status = "SBRNL - XBRL Taxonomy toolbox"
        if hasattr(conf, "gui"):
            if conf.gui.get("default_status"):
                self.default_status = conf.gui["default_status"]
        self.set_status()

        self.modules = self.load_modules()

        if self.dispatch_list:  # some events that might have gone off too early
            for event in self.dispatch_list:
                self.dispatch(action=event["action"], payload=event["payload"])
            self.dispatch_list = []

        self.setWindowTitle("XBRL Processor")

        self.info = QTextEdit(self)
        self.info.setReadOnly(True)
        self.info.ensureCursorVisible()  # auto-scroll, should be an option ;-)
        info_dock = QDockWidget()
        info_dock.setWidget(self.info)
        self.addDockWidget(Qt.BottomDockWidgetArea, info_dock)

        self.threadpool = QThreadPool()

        thread_count = conf.parser.get("thread_count", 1)
        self.threadpool.setMaxThreadCount(thread_count)
        self.info.append(
            "Multithreading with maximum %d threads" % self.threadpool.maxThreadCount()
        )
        self.app = app

    def load_modules(self):
        """
        Load modules.
        First, the load_module functions of each module are added to a list.
        These functions consist of the following information:

        'name': Name of the module
        'type': Type of the module, such as 'input' or 'export'
        'gui': function that is added to the GUI, typically this is an QGroupBox object.
        """
        modules = [
            xbrl_parser_module.load_module(),
            # sbr_wonen_module.load_module(),
            dts_manager_module.load_module(),
            export_module.load_module(),
            taxonomy_building_module.load_module(),
            semantics_check_module.load_module(),
            preparer_extensions_module.load_module(),
            validator_module.load_module(),
            instance_module.load_module(),
        ]

        # Each added module is inported via this loop
        for module in modules:
            moduleDock = (
                QDockWidget()
            )  # Dock widget is made for the module. Allows for moving within the GUI
            importable_gui_element = module[
                "gui"
            ]  # Get the function that is to be imported
            module["instance"] = importable_gui_element(parent=self)
            moduleDock.setWidget(
                module["instance"]
            )  # Set and call the widget, with self as an agument to set it as parent.
            moduleDock.setAttribute(Qt.WA_DeleteOnClose)

            # All 'input' modules should be added to the left of the GUI
            if module["type"] == "input":
                self.addDockWidget(Qt.LeftDockWidgetArea, moduleDock)

            # All 'export' modules should be added to the right of the GUI
            if module["type"] == "export":
                self.addDockWidget(Qt.RightDockWidgetArea, moduleDock)

            if module["type"] == "validation":
                # If a validator is loaded, it should be added to the left side of the GUI
                # And a variable holding the validator function should be added
                self.addDockWidget(Qt.LeftDockWidgetArea, moduleDock)
        return modules

    def setDTS(self, dts):
        # deprecated. Should use setDTS_list
        self.discovered_dts = dts
        self.concepts_widget.fill_listview(dts)
        # self.hypercube_widget.fill_listview(dts)

    def setDTS_list(self, dts_list):
        self.discovered_dts_list = dts_list
        self.dispatch(action="enable_widget", payload="dts_list_updated")
        # feed older functions the first DTS
        if len(self.discovered_dts_list) and not self.discovered_dts:
            self.setDTS(dts_list[0])

    def add_dts_list(self, dts_list):
        """
        method to extend the current dts_list, in contrast to replacing the list
        :params dts_list: list containing dts-objects
        """
        self.discovered_dts_list.extend(dts_list)
        if len(self.discovered_dts_list) == len(dts_list) and not self.discovered_dts:
            self.setDTS(dts_list[0])
        self.dispatch(action="enable_widget", payload="dts_list_updated")

    def selectFolderPopup(self):
        export_dir = QFileDialog(None, "Select export folder", "", "")
        export_dir.setFileMode(QFileDialog.DirectoryOnly)
        return export_dir.getExistingDirectory()

    def set_status(self, message=None):
        if message:
            message = f" - {message}"
        else:
            message = ""
        self.statusBar().showMessage(f"{self.default_status} {message}")

    def dispatch(self, action=None, payload=None):
        """
        event dispatcher for the gui components.
        components could implement 'action' as a method and update their viewws
            :param: action -> str
            :param: payload -> any
        """
        if action and getattr(self, "modules", None):
            for module in self.modules:
                dispatch_call = getattr(module["instance"], action, None)
                if dispatch_call:
                    dispatch_call(payload)
        else:
            # save it for the morning after
            print(f"Saving action: {action}")
            self.dispatch_list.append({"action": action, "payload": payload})


def start_gui():
    import sys

    logging.basicConfig()

    # Setup the logger
    rootlogger = logging.getLogger()

    app = QApplication(sys.argv)
    XBRLProcessor = XBRLProcessorGui()
    XBRLProcessor.show()
    sys.exit(app.exec_())


if __name__ == "__main__":

    import sys

    logging.basicConfig()

    # Setup the logger
    rootlogger = logging.getLogger()
    conf = Config()
    app = QApplication(sys.argv)
    XBRLProcessor = XBRLProcessorGui()
    XBRLProcessor.show()
    sys.exit(app.exec_())
