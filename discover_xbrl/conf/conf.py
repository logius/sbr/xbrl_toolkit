import yaml
import os.path


class Config:
    _instance = None
    loaded_file = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self, file=None):
        """
        Initialize the configuration
        any first-level variable in the yanl configuration will become
        an attribute of this object.
        so
        db:
          user: 'postgres'
          host: 'localhost'
        becomes
        self.db = {'user': 'postgres', 'host': 'localhost'}

        :param file: str, optional. When given the config will be (re-)read from this file
                                    Otherwise the default config will be loaded once
                                    and returned to all requests
        some doctesst
        >>> tst = Config('config.sample.yaml')
        >>> tst.parser['ep_outputdir']
        '/tmp/xbrl'
        >>> tst.get_config('no.yaml')
        Problem with opening configuration 'no.yaml'
        """
        if file or not hasattr(self, "project_root"):
            self.file = file
            self.get_config(file=file)

    def get_config(self, file=None):
        """
        reads config.yaml, or if provided any yaml configuration

        :param file: filename of config to read, if None config.yaml will be used
        :return config dictionary
        """
        if file is None:
            file = "config.yaml"
            self.file = file
        try:
            # print(f"Loading {file}")
            with open(os.path.join(os.path.dirname(__file__), file), "r") as configfile:
                config = yaml.load(configfile, Loader=yaml.Loader)
        except (IOError, FileNotFoundError) as e:
            print(f"Problem with opening configuration '{os.path.basename(file)}'")
            return None

        for key, value in config.items():
            setattr(self, key, value)

        if hasattr(self, "project_root") and self.project_root == "..":
            self.project_root = os.path.abspath(
                os.path.join(os.path.dirname(__file__), self.project_root)
            )

        if hasattr(self, "test"):
            self.test["datadir"] = os.path.join(self.project_root, self.test["datadir"])

        setattr(self, "loaded_file", file)

        # Set has_taxonomy_package to False
        if not self.parser.get("has_taxonomy_package"):
            self.parser["has_taxonomy_package"] = False

    def save(self):
        if self.loaded_file:
            with open(
                os.path.join(os.path.dirname(__file__), self.loaded_file), "w"
            ) as configfile:
                configfile.write(
                    yaml.dump(self.__dict__)
                )  # No fancy object annotations please.


if __name__ == "__main__":
    import doctest

    doctest.testmod()
    c = Config()
