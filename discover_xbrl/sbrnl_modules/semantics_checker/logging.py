import logging

from discover_xbrl.conf.conf import Config


def setup_logger():
    """
    Setting up logger
    """
    formatter = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    )

    # Setup exports logger
    conf = Config()
    logger_builder = logging.getLogger("modules.semantics_checker")
    logger_builder.addHandler(
        logging.FileHandler(f"{conf.logging['maindir']}/semantics.log", "a+")
    )
    logger_builder.addHandler(logging.StreamHandler())
