import os
from pathlib import Path

from PySide2.QtWidgets import QGroupBox, QVBoxLayout, QPushButton

from .semantics_checker import generate_report

from discover_xbrl.conf.conf import Config

conf = Config()


class SemanticsChecker(QGroupBox):
    def __init__(self, parent=None):
        super(SemanticsChecker, self).__init__(parent)
        self.title = "Exports"
        self.parent = parent

        # reportingActionsBox = QGroupBox("Reporting")

        # Create widgets
        generateSemanticReportButton = QPushButton("Generate semantic report")

        # Connect functions to buttons
        generateSemanticReportButton.clicked.connect(self.generateSemanticReport)

        # Set widgets to layout
        layout = QVBoxLayout()
        layout.addWidget(generateSemanticReportButton)

        # Set layout to class attribute
        self.setDisabled(False)
        self.setLayout(layout)

    def generateSemanticReport(self):
        # todo EK: Right now we get the exports_dir value from an import, there is probably a more elegant way.
        # Create the path if it does not exist
        path = conf.semantics_checker.get("export_path").rsplit("/", 1)[0]
        Path(path).mkdir(parents=True, exist_ok=True)

        save_path = conf.semantics_checker.get("export_path")
        generate_report(save_path, self.parent.discovered_dts_list)
        # todo: call semantic check function
