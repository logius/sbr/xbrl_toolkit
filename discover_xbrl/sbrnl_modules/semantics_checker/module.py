'''
File that loads all relevant module parts.
'''
from .logging import setup_logger
from .widget import SemanticsChecker

def load_module(module_object={}):

    setup_logger()
    module_object['name'] = "Semantics checker"
    module_object['type'] = "export"
    module_object['gui'] = SemanticsChecker
    module_object['description'] = """
    Analytical tools to help with semantic challenges within the NT. Also known as 'Poortwachter' tooling.
    """

    return module_object
