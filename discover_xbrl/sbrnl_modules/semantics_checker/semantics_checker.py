import logging

from openpyxl import Workbook
from ..exports.export import TaxonomyExcelExporter


logger = logging.getLogger('modules.semantics_checker')

stopwords = ['of', 'in', 'and', 'the', 'from', 'for', 'or']
ignored_domains = ['ifrs-smes', 'ifrs-full', 'ifrs-mc']
special_parts = ['title', 'disclosure', 'policy', 'member']


def generate_main_overview(ws, dts_list):
    distinct_concepts = {}
#     header = ["Concept", "Indeling", "Type", "label", "documentation", "terseLabel", "totalLabel", "verboseLabel",
#               "periodStartLabel"	periodEndLabel	domainSpecificLabel	domainSpecificTerseLabel	domainSpecificVerboseLabel	commentaryGuidance	definitionGuidance	domainSpecificCommentaryGuidance	domainSpecificPeriodStartLabel	domainSpecificPeriodEndLabel	domainSpecificTotalLabel
# ]
    # create a distinct list of concepts
    for dts in dts_list:
        for concept_name, concept in dts.concepts.items():

            if concept_name not in distinct_concepts:
                distinct_concepts[concept_name] = concept

    for concept_name, concept in distinct_concepts.items():
        row = [concept_name, "", concept.element_type]
        ws.append(row)

    return ws



def generate_report(save_path, dts_list):

    '''
    Loop through DTS files.
    Create an overview per EP, with duplicates based on ID and label names.
    Create a comparison between EP's, with duplicates between them, based on ID and label names

    :param save_path:
    :param dts_list:
    :return:
    '''
    workbook = Workbook()

    # ws = workbook.active
    #
    # workbook['overview'] = generate_main_overview(ws, dts_list)

    # counters and wordlists persistant over dts/ep's
    concept_partslist = {}
    itemtype_partlist = {}

    word_counter = {}
    itemtype_counter = {}

    itemtypes_usage_counter = {}
    substitutiongroup_counter = {}

    # Temporary booleans to set/unset test output
    output_itemtype = False
    output_parts_per_domain = False

    for dts in dts_list:
        # todo: Do not make a list per DTS
        # todo: Or have separate exports for one or multiple EP's

        for conceptname, concept in dts.concepts.items():

            # Only parse concepts that do not appear in an international/ignored domain
            if concept._taxonomy_domain not in ignored_domains:

                '''
                Create a dictionary with count per word
                '''

                for part in concept._name_parts:

                    # Do not count stopwords
                    if part not in stopwords:


                        if part not in word_counter.keys():
                            word_counter[part] = 1
                        else:
                            word_counter[part] = word_counter[part] + 1

                '''
                Create a list of parts per domain
                '''
                if concept._taxonomy_domain not in concept_partslist:
                    concept_partslist[concept._taxonomy_domain] = [concept._name_parts]
                else:
                    concept_partslist[concept._taxonomy_domain].append(concept._name_parts)

                '''
                Count usage of itemtypes
                '''
                if concept.element_type not in itemtypes_usage_counter:
                    itemtypes_usage_counter[concept.element_type] = 1
                else:
                    itemtypes_usage_counter[concept.element_type] = itemtypes_usage_counter[concept.element_type] + 1

                '''
                Count substitution groups
                '''
                tokenized_substitutiongroup = str(concept.substitutionGroup).replace(":", "_")
                if tokenized_substitutiongroup not in substitutiongroup_counter:
                    substitutiongroup_counter[tokenized_substitutiongroup] = 1
                else:
                    substitutiongroup_counter[tokenized_substitutiongroup] = substitutiongroup_counter[tokenized_substitutiongroup] + 1


        for itemtype in dts.datatypes:

            # @id is not required, and the name is often not distinctive enough, so we need to add our own info.
            # to identify the itemtypes.
            if itemtype.domain:
                combined_domain_name = itemtype.domain + "_" + itemtype.name
            elif itemtype.id:
                # a bit hacky, but some itemtypes do not get a domain, so we need to try and infer it from the id instead.
                combined_domain_name = itemtype.id

            '''
            Process itemtypes
            '''



            '''
               Create a dictionary with count per itemType name
               '''

            for part in itemtype._name_parts:

                # Do not count stopwords
                if part not in stopwords:

                    if part not in itemtype_counter.keys():
                        itemtypes_usage_counter[part] = 1
                    else:
                        itemtypes_usage_counter[part] = itemtypes_usage_counter[part] + 1

                '''
                Count usage of itemtypes
                '''
                if combined_domain_name not in itemtypes_usage_counter:
                    itemtypes_usage_counter[combined_domain_name] = 1
                else:
                    itemtypes_usage_counter[combined_domain_name] = itemtypes_usage_counter[
                                                                        combined_domain_name] + 1

        '''
        Outputs
        
        not working with multiple DTS:
        Itemtypes (no rows)
        Concepts (Empty rows and repeating headers)
        Labels (Repeating, maximim rows)
        Creates a sheet per DTS
        
        '''
        # for countdict in [word_counter, itemtypes_counter, substitutiongroup_counter]:
        workbook = create_sheet_count(workbook, count_dict=word_counter, count_type="concept_parts")
        workbook = create_sheet_count(workbook, count_dict=itemtype_counter, count_type="itemtype")
        workbook = create_sheet_count(workbook, count_dict=itemtypes_usage_counter, count_type="itemtype_usage_concepts")
        workbook = create_sheet_count(workbook, count_dict=substitutiongroup_counter, count_type="substitutiongroup")

        exporter = TaxonomyExcelExporter(dts_list=dts_list, dir=save_path)

        exporter.save_DTS_list_excel(workbook)
        # call the generic export function to add more information and write the Excel to disk.

        # # create an overview of id's and labels, in parts.
        #
        # for part, count in {k: v for k, v in sorted(word_counter.items(), key=lambda item: item[1])}.items():
        #     print(f"{part}: {count} times")
        #
        # # Total parts per domain
        # if output_parts_per_domain:
        #     for domain, partlist in {k: v for k, v in sorted(partslist.items(), key=lambda item: item[1])}.items():
        #         print(f"Domain '{domain}' has {len(partlist)} concepts")
        #
        # # create an overview of id's and labels, in parts.
        # if output_itemtype:
        #     for itemtype, count in {k: v for k, v in sorted(itemtypes_counter.items(), key=lambda item: item[1])}.items():
        #         print(f"Itemtype: {itemtype} is used {count} times")

def create_sheet_count(workbook, count_dict, count_type):
    '''
    Takes a workbook, a dictionary with counting information (key: countable object name, value: amount of occurences)
    and the name of the countable object.


    :param workbook: openpyxl workbook
    :param count_dict:
    :param count_type:
    :return: openpyxl workbook with counting sheet
    '''
    max_column_size = 0

    sheet = workbook.create_sheet(title=f"counts_{count_type}")
    sheet.append([count_type, "occurrences"])
    for element, count in {k: v for k, v in sorted(count_dict.items(), key=lambda item: item[1], reverse=True)}.items():

        # Need to make sure only strings are used. If the  element is an object, we need to get the objects name.
        if not isinstance(element, str):
            if hasattr(element, 'name'):
                element = element.name
            else:
                logger.error("Semantics checker is tring to report on an element that has no name!")

        if element: # element could be None, but still have a count. In that case, do not check for length.
            if len(element) > max_column_size:
                max_column_size = len(element)

        sheet.append([element, count])


    # Adjust the width of the first column
    sheet.column_dimensions["A"].width = max_column_size
    return workbook
