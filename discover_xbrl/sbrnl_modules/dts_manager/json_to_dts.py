import json
from anytree import AnyNode

from discover_xbrl.sbrnl_xbrl_parser.linkbase.classes import Label, Reference
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.dimensions.classes import (
    Hypercube,
    Dimension,
    Member,
)
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.formulas.classes.formula import (
    FormulaPeriod,
    FormulaTypedDimension,
)
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.formulas.classes.variables import (
    Parameter,
    Variable,
    FactVariable,
    GeneralVariable,
)
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.tables.classes import (
    Table,
    Breakdown,
    RuleNode,
    RuleSet,
    ConceptRelationshipNode,
    DimensionRelationshipNode,
    AspectNode,
)
from discover_xbrl.sbrnl_xbrl_parser.parsetools.classes import DTS
from discover_xbrl.sbrnl_xbrl_parser.schema.classes import Concept, ItemType, RoleType


def get_concept_from_json(dict, itemtype=None):
    if itemtype is None:
        itemtype = dict["itemtype_name"]

    c = Concept(
        id=dict["id"],
        name=dict["name"],
        balance=dict["balance"],
        nillable=dict["nillable"],
        is_abstract=dict["abstract"],
        periodType=dict["periodtype"],
        ns_prefix=dict["ns_prefix"],
        namespace=dict["namespace"],
        substitutionGroup=dict["substitution_group"],
        typedDomainRef=dict["typed_domainref"],
        element_type=itemtype,
    )
    for dict_label in dict["labels"]:
        l = get_label(dict=dict_label)
        c.labels[l.xlink_label] = l

    return c


def get_label(dict):
    l = Label(
        # id=dict["id"],
        xlink_label=dict["link_label"],
        linkRole=dict["link_role"],
        language=dict["lang"],
        text=dict["label_text"],
    )
    return l


def get_variable(dict):
    if dict["type"] == "FactVariable":
        variable = FactVariable(
            id=json_variable["id"],
            xlink_label=json_variable["xlink_label"],
            variable_arc_name=json_variable["id"],
            matches=json_variable["matches"],
            bind_as_sequence=json_variable["bind_as_sequence"],
            nils=json_variable["nils"],
            fallback_value=json_variable["fallback_value"],
            filters=json_variable["filters"],
        )
    elif dict["type"] == "GeneralVariable":
        variable = GeneralVariable(
            id=json_variable["id"],
            xlink_label=json_variable["label"],
            bind_as_sequence=json_variable["bind_as_sequence"],
            select=json_variable["select"],
        )
    else:
        variable = Variable(
            id=json_variable["id"],
            xlink_label=json_variable["label"],
            bind_as_sequence=json_variable["bind_as_sequence"],
        )

    return variable


def get_dimension_members(dict, parent):
    members = []
    for json_member in dict:
        # todo: think we are missing Member.target_linkrole. Although, this is probably only used to build the hierarchy
        member = Member(
            concept=get_concept_from_json(json_member["concept"]),
            parent=parent,
            order=json_member["order"],
            usable=json_member["usable"],
        )
        if json_member["members"]:
            get_dimension_members(json_member["members"], parent=member)

        members.append(member)

    return members


def get_tableRuleNodes(dict, parent):

    for json_node in dict:

        node = RuleNode(
            id=json_node["id"],
            xlink_label=json_node["label"],
            parent=parent,
            order=json_node["order"],
            parentChildOrder=json_node["parent_child_order"],
            is_abstract=None,  # todo: not availabe, but probably also wrongly added to object?
            merge=json_node["merge"],
            tagselector=json_node["tagselector"],
            rulesets=[],
        )
        if json_node["children"]:
            get_tableRuleNodes(json_node["children"], parent=node)

        if json_node["formula_periods"]:
            for json_formula_period in json_node["formula_periods"]:

                if json_formula_period["period_type"] == "duration":
                    filter = FormulaPeriod(
                        periodtype=json_formula_period["period_type"],
                        value=json_formula_period["value"],
                        start=json_formula_period["start"],
                        end=json_formula_period["end"],
                    )

                    if json_formula_period["parent_ruleset"] is not None:
                        print("Recursion is needed")

                    # todo: Not sure if this should be added here directly. We are doing thins
                    #  in the parser, but need to make sure if this is actually the idea.
                    node.rulesets.append(filter)

        if json_node["formula_typed_dimensions"]:
            print("todo: formula_typed_dimensions")

        if json_node["rulesets"]:
            rulesets = []
            for json_ruleset in json_node["rulesets"]:
                formulas = []
                for json_formula_period in json_ruleset["formula_periods"]:
                    formulas.append(
                        FormulaPeriod(
                            periodtype=json_formula_period["period_type"],
                            value=json_formula_period["value"],
                            start=json_formula_period["start"],
                            end=json_formula_period["end"],
                        )
                    )
                for json_typed_dimension in json_ruleset["formula_typed_dimensions"]:

                    typed_dimension = FormulaTypedDimension(
                        qname_dimension=json_typed_dimension["qname_dimension"],
                        value=json_typed_dimension["value"],
                        xpath=json_typed_dimension["xpath"],
                    )
                    rulesets.append(typed_dimension)

                # todo: child_elements is not implemented correcly in parser. It should be more generic.
                #  for now, we just manually add the FormulaPeriod's
                ruleset = RuleSet(
                    tag=json_ruleset["tag"], formulas=formulas, child_elements=[]
                )
                rulesets.append(ruleset)

        if json_node["aspect_nodes"]:
            for json_aspect_node in json_node["aspect_nodes"]:
                aspect_node = AspectNode(
                    id=json_aspect_node["id"],
                    xlink_label=json_aspect_node["label"],
                    order=json_aspect_node["order"],
                    merge=json_aspect_node["merge"],
                    is_abstract=json_aspect_node["is_abstract"],
                    parent=parent,
                    tagselector=json_aspect_node["tagselector"],
                )

                for json_label in json_aspect_node["labels"]:
                    l = get_label(json_label)
                    aspect_node.labels[l.linkLabel] = l

        if json_node["concept_relationshipnodes"]:
            for json_concept_relationshipnode in json_node["concept_relationshipnodes"]:
                crn = ConceptRelationshipNode(
                    id=json_concept_relationshipnode["id"],
                    xlink_label=json_concept_relationshipnode["label"],
                    order=json_concept_relationshipnode["order"],
                    parent=parent,
                    parentChildOrder=json_concept_relationshipnode[
                        "parent_child_order"
                    ],
                    relationship_source=None,  # todo: not available
                    linkrole=None,  # todo: not available
                    formula_axis=None,  # todo: not available,
                    generations=None,  # todo: not available,
                    arcrole=None,  # todo: not available,
                    linkname=None,  # todo: not available,
                    arcname=None,  # todo: not available,
                    tagselector=json_concept_relationshipnode["tagselector"],
                )
                for json_label in json_concept_relationshipnode["labels"]:
                    l = get_label(json_label)
                    crn.labels[l.linkLabel] = l

        if json_node["dimension_relationshipnodes"]:
            for json_dimension_relationshipnode in json_node[
                "dimension_relationshipnodes"
            ]:
                dim_node = DimensionRelationshipNode(
                    id=json_dimension_relationshipnode["id"],
                    xlink_label=json_dimension_relationshipnode["label"],
                    order=json_dimension_relationshipnode["order"],
                    parent=parent,
                    parentChildOrder=json_dimension_relationshipnode[
                        "parent_child_order"
                    ],
                    relationship_source=json_dimension_relationshipnode[
                        "relationship_source"
                    ],
                    linkrole=json_dimension_relationshipnode[
                        "linkrole_role_uri"
                    ],  # todo: Need to get this from DTS (halp!)
                    formula_axis=json_dimension_relationshipnode["formula_axis"],
                    generations=json_dimension_relationshipnode["generations"],
                    tagselector=json_dimension_relationshipnode["tagselector"],
                )
                for json_label in json_dimension_relationshipnode["labels"]:
                    l = get_label(json_label)
                    dim_node.labels[l.linkLabel] = l


def get_itemtype_from_json(json_itemtype):
    itemType = ItemType(
        id=json_itemtype["id"],
        name=json_itemtype["name"],
        domain=json_itemtype["domain"],
        base=json_itemtype["base"],
        inheritance_type=json_itemtype["inheritance_type"],
        #   restriction_type=json_itemtype["restriction_type"],
        #   restriction_length=json_itemtype["restriction_length"],
        restriction_pattern=json_itemtype["restriction_pattern"],
        min_length=json_itemtype["restriction_max_length"],
        max_length=json_itemtype["restriction_min_length"],
        totalDigits=json_itemtype["total_digits"],
        fraction_digits=json_itemtype["fraction_digits"],
        min_inclusive=json_itemtype["min_inclusive"],
        #   max_inclusive=json_itemtype["max_inclusive"],
    )
    return itemType


if __name__ == "__main__":
    with open("../../../tmp/middelgroot_16.json") as file:
        json_file = json.loads(file.read())

        print(f"Importing {json_file['name']}")
        dts = DTS(name=json_file["name"], shortname=json_file["shortname"])

        print(f"Importing {len(json_file['concepts'])} concepts.")
        for json_concept in json_file["concepts"]:
            concept = get_concept_from_json(json_concept)

            # Get labels
            for json_label in json_concept["labels"]:
                label = get_label(json_label)
                concept.labels[label.linkLabel] = label

            # Get references
            # todo: reference handling in DTS is based on linkbases
            #  most should be "http://www.xbrl.org/2003/role/link"
            #  but can also be generic: "http://www.xbrl.org/2008/role/link"
            #  I suppose this does not matter for the model; a reference to a concept will be the former
            #  while to e.g., a linkrole would be generic.
            #  Here we could just choose to put them in the 'standard' linkbase because we are dealing with concepts.
            #  but do we want to reply on that info on this layer of abstraction?
            for json_reference in json_concept["references"]:

                reference = Reference(
                    id=json_reference["id"],
                    xlink_label=json_reference["label"],
                    name=json_reference["name"],
                    number=json_reference["number"],
                    issuedate=json_reference["issuedate"],
                    article=json_reference["article"],
                    chapter=json_reference["chapter"],
                    note=json_reference["note"],
                    section=json_reference["section"],
                    subsection=json_reference["subsection"],
                    publisher=json_reference["publisher"],
                    paragraph=json_reference["paragraph"],
                    subparagraph=json_reference["subparagraph"],
                    clause=json_reference["clause"],
                    subclause=json_reference["subclause"],
                    appendix=json_reference["appendix"],
                    example=json_reference["example"],
                    page=json_reference["page"],
                    exhibit=json_reference["exhibit"],
                    footnote=json_reference["footnote"],
                    sentence=json_reference["sentence"],
                    uri=json_reference["uri"],
                    uridate=json_reference["uridate"],
                )
                if reference not in concept.references:
                    concept.references.append(reference)

            # Get itemType
            json_itemtype = json_concept["itemtype"]
            dts_itemtype = [
                i
                for i in dts.datatypes
                if i.name == json_itemtype["name"]
                and i.domain == json_itemtype["domain"]
            ]

            if dts_itemtype:
                concept.element_type = dts_itemtype[0]
            else:
                # todo: extra/missing values?

                itemType = get_itemtype_from_json(json_itemtype=json_itemtype)
                dts.datatypes.append(itemType)
                concept.element_type = itemType

            # Add the concept to DTS
            # todo: should use {namespace}_{name} as key, probably?
            dts.concepts[json_concept["id"]] = concept

        for json_linkrole in json_file["linkroles"]:

            if json_linkrole["directly_referenced"] == 0:
                # todo: Decide to skip these?
                print(
                    f"Skipping linkrole {json_linkrole['role_uri']}. Not directly referenced"
                )
            else:

                linkrole = RoleType(
                    id=json_linkrole["id"],
                    roleURI=json_linkrole["role_uri"],
                    #        hypercube=None,
                    definition=json_linkrole["definition"],
                    used_on=None,  # todo: Missing. Technically we can refer this but still
                    parent_dts=dts,
                )

                if json_linkrole["hypercube"] is not None:

                    hypercube = Hypercube(
                        hc_type=json_linkrole["hypercube"]["hc_type"],
                        linkrole=json_linkrole["hypercube"]["hc_role_uri"],
                        context_element=json_linkrole["hypercube"]["context_element"],
                        closed=json_linkrole["hypercube"]["closed"],
                    )
                    lineitems = []
                    for json_lineitem in json_linkrole["hypercube"]["line_items"]:
                        lineitem_concept = get_concept_from_json(
                            json_lineitem["concept"]
                        )

                        lineitems.append((json_lineitem["order"], lineitem_concept))
                        hypercube.lineItems = lineitems

                    for json_dimension in json_linkrole["hypercube"]["dimensions"]:
                        dimension = Dimension(
                            concept=get_concept_from_json(json_dimension["concept"]),
                            target_linkrole=json_dimension["target_linkrole"],
                        )
                        members = get_dimension_members(
                            json_dimension["members"], parent=dimension
                        )

                if len(json_linkrole["tables"]) > 0:
                    for json_table in json_linkrole["tables"]:

                        table = Table(
                            id=json_table["id"],
                            parentChildOrder=json_table["parent_child_order"],
                            xlink_label=json_table["label"],
                        )

                        labels = {}
                        for json_label in json_table["labels"]:
                            l = get_label(json_label)
                            labels[l.linkLabel] = l
                        table.labels = labels

                        for json_parameter in json_table["parameters"]:
                            parameter = Parameter(
                                as_datatype=json_parameter["as_datatype"],
                                xlink_label=json_parameter["label"],
                                name=json_parameter["name"],
                                required=json_parameter["required"],
                                select=json_parameter["select"],
                                id=json_parameter["id"],
                            )
                            if parameter not in table.parameters:
                                table.parameters.append(parameter)

                        for json_variable in json_table["variables"]:

                            variable = get_variable(json_variable)

                            # todo: Check if this should indeed be added to linkrole, rather than table
                            if variable not in linkrole.variables:
                                linkrole.variables.append(variable)

                        for json_table_breakdown in json_table["table_breakdowns"]:
                            table_breakdown = Breakdown(
                                parentChildOrder=json_table_breakdown[
                                    "parent_child_order"
                                ],
                                label=json_table_breakdown["label"],
                                axis=json_table_breakdown["axis"],
                                order=json_table_breakdown["order"],
                                id=json_table_breakdown["id"],
                            )
                            if table_breakdown not in table.table_breakdowns:
                                table.table_breakdowns.append(table_breakdown)

                            # Add rulenodes to breakdown's AnyTtee
                            get_tableRuleNodes(
                                json_table_breakdown["table_rulenodes"],
                                parent=table_breakdown,
                            )
                        linkrole.tables.append(table)

                    # todo: kinda double to write to both places. Should probably be a property
                    dts.directly_referenced_linkbases.append(linkrole)
                    if linkrole.roleURI not in dts.roletypes.keys():
                        dts.roletypes[linkrole.roleURI] = linkrole

        for namespace in json_file["namespaces"]:
            dts.namespaces[namespace["uri"]] = namespace["prefix"]
        print(
            f"{len(dts.references)} references (todo: DTS expects references in linkroles)"
        )
        print(f"{len(dts.namespaces)} namespaces")
        print(f"{len(dts.concepts)} concepts")
        print(
            f"{len(dts.labels)} labels (I think dts.labels is deprecated? We now also have all_labels)"
        )
        print(f"{len(dts.datatypes)} itemtypes")
        print(f"{len(dts.tables)} tables")
