import pickle

from lxml import etree
from pathlib import Path
from zipfile import ZipFile
from PySide2.QtWidgets import QFileDialog, QGroupBox, QVBoxLayout, QPushButton
from discover_xbrl.conf.conf import Config

conf = Config()


def iterate_though_object(obj):

    for attr in obj.__dict__.values():

        if isinstance(attr, list):

            for list_item in attr:
                if isinstance(list_item, (etree._Element, etree._ElementTree)):
                    print(f"Found the lxml element: {list_item}")

        elif isinstance(attr, dict):

            for k, v in attr.items():
                if isinstance(k, (etree._Element, etree._ElementTree)):
                    print(f"Found the lxml element")
                if isinstance(v, (etree._Element, etree._ElementTree)):
                    print(f"Found the lxml element")

                elif not isinstance(v, (str, float, int)):
                    # Go one layer deeper if the value may hold other objects
                    iterate_though_object(v)


def minimize_dts(dts):
    """
    Removes information from the DTS that are added during parsing, but are not necessary for building the taxonomy.

    :param dts: DTS object
    :return: DTS object
    """

    # Flip boolean to be more aggressive when removing.
    remove_uncertain = True

    ### DTS ###

    # Linkbases and schema's are only found by parsing. These can be removed for sure.
    delattr(dts, "linkbases")
    delattr(dts, "schemas")

    if remove_uncertain:
        delattr(dts, "namespaces")  # Not sure if namespaces should be removed.

        for label in dts.labels.values():
            # ID should probably be derived from the xlink_label.
            delattr(label, "id")

        for concept in dts.concepts.values():
            # ID should probably be derived from the concept name.
            delattr(concept, "id")

    ### ROLETYPES ###
    for roletype in dts.roletypes.values():

        # We probably should assume arcs are not available to use. If we do need them,
        # we need to create them when going from DTS -> LDM
        delattr(roletype, "arcs")

        if remove_uncertain:
            # ID should probably be derived from the name.
            delattr(roletype, "id")

            # Go through the labels that are tied to this specific linkrole

            """ # These labels are also already added to the dts.labels dict.
            for label in roletype.labels.values():
                # ID should probably be derived from the xlink_label.
                delattr(label, "id")
            """

            for assertion in roletype.assertions:
                delattr(assertion, "id")

    return dts


class TaxonomyManagerWidget(QGroupBox):
    def __init__(self, parent=None):
        super(TaxonomyManagerWidget, self).__init__(parent)

        self.title = "Taxonomy Actions"
        self.maximumHeight = 400
        self.parent = parent

        # Create widgets
        saveDTSButton = QPushButton("Save DTS")
        saveLDMButton = QPushButton("Save LDM")
        loadDTSButton = QPushButton("Load DTS")

        # If the 'overwrite_load_dts attribute exist in vars, load this DTS on load
        if hasattr(conf, "dts_manager"):

            if conf.dts_manager is not None:
                if conf.dts_manager.get("load_default_dts", False):

                    self.callloadDTSbundle(
                        source_taxonomy=conf.dts_manager.get("load_default_dts")
                    )

        # Connect functions to buttons

        saveDTSButton.clicked.connect(self.callsaveDTSbundle)
        saveLDMButton.clicked.connect(self.callsaveLDMbundle)
        loadDTSButton.clicked.connect(self.callloadDTSbundle)

        # Set widgets to layout
        layout = QVBoxLayout()

        layout.addWidget(saveDTSButton)
        layout.addWidget(saveLDMButton)
        layout.addWidget(loadDTSButton)

        # Set layout to class attribute
        # saveTaxonomyJSONButton.setDisabled(False)

        self.setLayout(layout)

    def callsaveDTSbundle(self):
        """
        Saves the discovered DTS to a zip file

        ZIP structure:
        .
        dts\                Folder with DTS files
        dts\example.json    pickled DTS file

        :return:
        """
        source_taxonomy = QFileDialog.getSaveFileName(
            self, "Select target DTS)", "", "Entrypoint zip (*.zip)"
        )
        zip_url = Path(source_taxonomy[0])

        if zip_url:
            with ZipFile(zip_url, "w") as dts_package_zip:
                for dts in self.parent.discovered_dts_list:

                    try:
                        pickled_dts = pickle.dumps(dts)
                    except:
                        print("Cannot pickle. Now trying to find the lxml element")
                        iterate_though_object(dts)

                    else:

                        dts_name = str(dts.entrypoint_name).replace(".xsd", ".json")
                        dts_package_zip.writestr("dts/" + dts_name, pickled_dts)

    def callsaveLDMbundle(self):
        """
        Saves the discovered DTS to a zip file

        ZIP structure:
        .
        dts\                Folder with DTS files
        dts\example.json    pickled DTS file

        :return:
        """
        source_taxonomy = QFileDialog.getSaveFileName(
            self, "Select target LDM)", "", "Entrypoint zip (*.zip)"
        )
        zip_url = Path(source_taxonomy[0])

        if zip_url:
            with ZipFile(zip_url, "w") as dts_package_zip:
                for dts in self.parent.discovered_dts_list:

                    dts = minimize_dts(dts)  # minimize DTS before saving

                    try:
                        pickled_dts = pickle.dumps(dts)
                    except:
                        print("Cannot pickle. Now trying to find the lxml element")
                        iterate_though_object(dts)

                    else:

                        dts_name = str(dts.entrypoint_name).replace(".xsd", ".json")
                        dts_package_zip.writestr("dts/" + dts_name, pickled_dts)

    def callloadDTSbundle(self, source_taxonomy=None):

        if not source_taxonomy:
            source_taxonomy = QFileDialog.getOpenFileName(
                self, "Select saved DTS)", None, "DTS (*.zip)"
            )[0]
        else:
            print(source_taxonomy)
        zip_url = Path(source_taxonomy)
        dts_list = []
        if zip_url.name:  # If a file is selected, else pass
            with ZipFile(zip_url, "r") as dts_package_zip:
                for file in dts_package_zip.namelist():
                    if str(file).startswith("dts/"):
                        dts = dts_package_zip.read(file)
                        dts_unpickled = pickle.loads(dts)
                        dts_list.append(dts_unpickled)

            self.parent.setDTS_list(dts_list)
            self.parent.dispatch("update_entrypointlistview", [str(zip_url)])
