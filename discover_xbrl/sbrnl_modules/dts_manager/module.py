"""
File that loads all relevant module parts.
"""
from .widget import TaxonomyManagerWidget


def load_module(module_object={}):

    module_object["name"] = "DTS Manager"
    module_object["type"] = "input"
    module_object["gui"] = TaxonomyManagerWidget
    module_object[
        "description"
    ] = """
    This module is used to load or save DTS 'bundles'. These are zipfiles of in memory object dumps of the loaded DTS. 
    While not always backwards compatible between version of the toolbox, this module allows for saving an entire
    loaded taxonomy. As discovering can take multiple hours, being able to save a copy of the in-memory representation
    is helpful when working on parser or analytical functions.
    """

    return module_object
