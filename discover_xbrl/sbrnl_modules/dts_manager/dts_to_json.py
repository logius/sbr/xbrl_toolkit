import pickle
import json
import copy
from pathlib import Path
from zipfile import ZipFile

from discover_xbrl.conf.conf import Config

conf = Config()


class DtsToJson:
    def __init__(self, dts=None):
        self.dts = dts
        self.as_json = None

    def set_dts(self, dts):
        self.dts = dts
        self.toJSON()

    def toJSON(self):
        dts_as_json = {
            "concepts": [],
            "labels": [],
            "tables": [],
            "hypercubes": [],
            "assertions": [],
            "references": [],
            "variables": [],
            "datatypes": [],
            "roletypes": [],
            "presentation_hierarchy": None,
        }

        for label in self.dts.labels.values():
            dts_as_json["labels"].append(label.__dict__)
        for assertion in self.dts.assertions:
            dts_as_json["assertions"].append(assertion.__dict__)
        for reference in self.dts.references:
            dts_as_json["references"].append(reference.__dict__)
        for variable in self.dts.variables:
            dts_as_json["variables"].append(variable.__dict__)
        for datatype in self.dts.datatypes:
            if hasattr(datatype, "_source"):
                delattr(datatype, "_source")
            dts_as_json["datatypes"].append(datatype.__dict__)
        for concept in self.dts.concepts.values():
            if hasattr(concept, "_source"):
                delattr(concept, "_source")
            concept_copy = concept.__dict__.copy()
            concept_copy["labels"] = [key for key in concept_copy["labels"].keys()]
            dts_as_json["concepts"].append(concept_copy)

        presentation = []
        if self.dts.presentation_hierarchy:
            for child in self.dts.presentation_hierarchy.children:
                child_node = build_presentation_child(child)
                presentation.append(child_node)
        dts_as_json["presentation_hierarchy"] = presentation

        for table in self.dts.tables:
            _breakdowns = []
            for breakdown in table.table_breakdowns:
                d = breakdown.__dict__
                d["children"] = []
                if breakdown.children and len(breakdown.children) > 0:
                    for child in breakdown.children:
                        subtree = get_breakdown(child)
                        d["children"].extend(subtree)
                del d["_NodeMixin__children"]
                if "_NodeMixin__parent" in d:
                    del d["_NodeMixin__parent"]
                _breakdowns.append(d)
            table.table_breakdowns = _breakdowns
            dts_as_json["tables"].append(table.__dict__)

        for hypercube in self.dts.hypercubes:
            h = hypercube.__dict__
            h["linkrole"] = h["linkrole"].id
            dimensions = []
            for dimension in hypercube.dimensions:
                dim = dimension.__dict__
                dim["concept"] = dim["concept"].id
                if dim["target_linkrole"]:
                    dim["target_linkrole"] = dim["target_linkrole"].id
                members = []
                for member in dim["domain_members"]:
                    memb = member.__dict__
                    memb["concept"] = memb["concept"].id
                    members.append(memb)
                dim["domain_members"] = members
                dimensions.append(dim)
                domains = []
                for domain in dim["domains"]:
                    dom = domain.__dict__
                    dom["concept"] = dom["concept"].id
                    dom["target_linkrole"] = dom["target_linkrole"].id
                    members = []
                    for member in dom["members"]:
                        memb = member.__dict__
                        memb["concept"] = memb["concept"].id
                        members.append(memb)
                    dom["members"] = members
                    domains.append(dom)
                dim["domains"] = domains
            h["dimensions"] = dimensions
            if hasattr(hypercube, "lineItems"):
                lineitems = []
                for order, lineitem in hypercube.lineItems:
                    li = lineitem.__dict__["id"]
                    li = (order, {"concept": li})
                    lineitems.append(li)
                h["lineItems"] = lineitems
            dts_as_json["hypercubes"].append(h)

        for roletype in self.dts.roletypes.values():
            roletype_copy = roletype.__dict__.copy()
            roletype_copy["labels"] = [key for key in roletype_copy["labels"].keys()]
            del roletype_copy["parent_dts"]
            del roletype_copy["arcs"]
            if roletype_copy["hypercube"] is not None:
                roletype_copy["hypercube"] = roletype.definition
            del roletype_copy["hypercube"]
            if roletype.tables:
                roletype_copy["tables"] = []
                for table in roletype.tables:
                    roletype_copy["tables"].append(table.id)
            else:
                del roletype_copy["tables"]
            if roletype_copy["assertions"]:
                roletype_copy["assertions"] = []
                for assertion in roletype.assertions:
                    roletype_copy["assertions"].append(assertion.id)
            else:
                del roletype_copy["assertions"]
            if roletype_copy["variables"]:
                roletype_copy["variables"] = []
                for variable in roletype.variables:
                    roletype_copy["variables"].append(variable.id)
            else:
                del roletype_copy["variables"]
            if roletype_copy["references"]:
                roletype_copy["references"] = []
                for reference in roletype.references:
                    roletype_copy["references"].append(reference.id)
            else:
                del roletype_copy["references"]
            del roletype_copy["xml_table_elems"]
            del roletype_copy["presentation_hierarchy"]
            del roletype_copy["definition_hierarchy"]

            dts_as_json["roletypes"].append(roletype_copy)
        self.as_json = json.dumps(dts_as_json, default=lambda o: o.__dict__, indent=4)


def get_breakdown(subtree):
    node = subtree
    subtree_items = []
    subtree_item = node.__dict__
    if node.children:
        subtree_item["children"] = []
        for child in node.children:
            child_items = get_breakdown(child)
            subtree_item["children"].extend(child_items)
    if "_NodeMixin__children" in subtree_item:
        del subtree_item["_NodeMixin__children"]
    if "_NodeMixin__parent" in subtree_item:
        del subtree_item["_NodeMixin__parent"]
    subtree_items.append(subtree_item)
    return subtree_items


def build_presentation_child(presentation_child):
    node = presentation_child
    key = type(node).__name__.lower()
    presentation_child_item = {"order": node.order, key: node.id}
    if node.children:
        presentation_child_item["children"] = []
        for child in node.children:
            child_item = build_presentation_child(child)
            presentation_child_item["children"].append(child_item)
    return presentation_child_item


def main():
    jsonalizer = DtsToJson()
    if hasattr(conf, "editor"):
        zip_url = Path(conf.editor["load_default_dts"])
        dts_list = []
        if zip_url.name:  # If a file is selected, else pass
            with ZipFile(zip_url, "r") as dts_package_zip:
                for file in dts_package_zip.namelist():
                    if str(file).startswith("dts/"):
                        dts = dts_package_zip.read(file)
                        dts_unpickled = pickle.loads(dts)
                        jsonalizer.set_dts(dts_unpickled)
        jsonalizer.toJSON()
        with open("/tmp/ocw-po-vo2020.json", "w+") as file:
            file.write(jsonalizer.as_json)


if __name__ == "__main__":
    main()
