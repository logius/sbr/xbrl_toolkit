import requests
import yaml
from anytree import AnyNode
from dataclasses import dataclass

from discover_xbrl.sbrnl_modules.db.dts_to_sql import DtsToSql
from discover_xbrl.sbrnl_modules.dts_manager.json_to_dts import (
    get_itemtype_from_json,
    get_concept_from_json,
)
from discover_xbrl.sbrnl_taxonomy_builder.widget import TaxonomyBuildingWorker
from discover_xbrl.sbrnl_xbrl_parser.linkbase.classes import Label
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.dimensions.classes import (
    Hypercube,
    Dimension,
    Member,
)
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.formulas.classes.formula import (
    FormulaPeriod,
    FormulaExplicitDimension,
    FormulaTypedDimension,
)
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.formulas.classes.variables import (
    Parameter,
)
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.tables.classes import (
    Breakdown,
    Table,
    RuleNode,
    ConceptRelationshipNode,
    RuleSet,
    AspectNode,
    Aspect,
)
from discover_xbrl.sbrnl_xbrl_parser.parsetools.classes import DTS
from discover_xbrl.sbrnl_xbrl_parser.schema.classes import Concept, RoleType, ItemType


yaml_file_path = "../../../input_files/yaml_to_dts/ihz.yaml"


#############################################
###
###     Rewrite of the dts_from_yaml
###     This script is used as an intermediate step for taxonomy building using the XBRL Toolkit
###
###     It uses A YAML file as its primary input.
###     It allows for new declarations, as well as reuse via the API.
###
###     Linkroles contain parts of a report. They are self contained and can be reused as such.
###     Central to this are the two types of hierarchy:
###     Presentation Hierarchy is used to create a Table of Contents, consisting of Linkroles and abstract Concepts
###     Linkrole Hierarchies describe the separate linkrole contents.
###
##############################################

# todo: move the dataclass to another file. Probably a good idea to include this with all DTS object, also from parser
@dataclass
class MetaInformation:
    name: str
    parent_ep: str  # Entrypoint that is uses in API queries as long as DTS is needed in some API entrypoints
    date: str
    nt_version: str
    domain: str
    new_namespaces: list
    api_uri: str
    prefix: str


### Methods to get information either from the API, or from the DTS if it already exists.
def get_itemtype(dts: DTS, qname: str = None, id: str = None):

    for itemtype in dts.datatypes:
        if (
            f"{itemtype.domain}:{itemtype.name}" == f"{qname}ItemType"
            or f"{itemtype.domain}:{itemtype.name}" == qname
        ):
            return itemtype

        elif itemtype.name == qname:
            # Could be without prefix
            return itemtype

    # print(f"Itemtype not found in DTS: '{qname}'")
    # Need to get it from the API
    if ":" in qname:
        # todo: need to cut the qname because the API only does localname at the moment
        stripped_qname = str(qname).split(":")[1]
    else:
        stripped_qname = qname

    r = requests.get(
        f"{MetaInformation.api_uri}/dts/{MetaInformation.parent_ep}/itemtypes/{stripped_qname}?nt_version={MetaInformation.nt_version}"
    )
    if r.status_code == 200:
        if r.text is not None and r.text != "[]":
            json_repr = r.json()
            if len(json_repr) > 0:

                itemType = get_itemtype_from_json(r.json()[0])
                if itemType not in dts.datatypes:
                    dts.datatypes.append(itemType)

                return itemType
            else:
                print("empty response")
        else:

            raise LookupError("API returns empty result")
    else:
        raise LookupError("ItemType not found")


### Methods that build new object from YAML
def build_concept(dts: DTS, yaml: dict, substitutionGroup: str = None):

    is_abstract = bool(yaml.get("abstract", True))
    if MetaInformation.prefix:
        suffix = f"-{MetaInformation.prefix}"
    else:
        suffix = ""

    if is_abstract is True and substitutionGroup is None:
        elem_type_flag = "abstr"
        namespace_string = (
            f"http://www.nltaxonomie.nl/{MetaInformation.nt_version}/{MetaInformation.domain}/"
            f"{MetaInformation.date}/presentation/{MetaInformation.domain}-abstr{suffix}"
        )
        substitutionGroup = "sbr:presentationItem"

    elif is_abstract is False and substitutionGroup is None:
        elem_type_flag = "i"
        namespace_string = (
            f"http://www.nltaxonomie.nl/{MetaInformation.nt_version}/{MetaInformation.domain}/"
            f"{MetaInformation.date}/dictionary/{MetaInformation.domain}-data{suffix}"
        )
        substitutionGroup = "xbrli:item"

    # Get the concept name
    concept_name = yaml.get("name")
    if concept_name is None:
        # If the concept name is not given, extract it from the qname
        concept_name = yaml.get("qname")
        if ":" in concept_name:
            # if it has a prefix, cut that part off
            concept_name = str(concept_name).rsplit(":", 1)[-1]

        if "}" in concept_name:
            # If it is in clark notation, cut that part off
            concept_name = str(concept_name).rsplit("}", 1)[-1]

    concept = Concept(
        id=f"{MetaInformation.domain}-{elem_type_flag}_{concept_name}",
        is_abstract=is_abstract,
        name=concept_name,
        periodType=yaml.get("periodtype", "duration"),
        nillable=yaml.get("nillable", "false"),
        namespace=namespace_string,
        substitutionGroup=substitutionGroup,
        element_type=get_itemtype(dts, "xbrli:stringItemType"),
    )
    # todo: normalize this more
    if yaml.get("labels") is not None:
        for language, label in yaml["labels"].items():
            label_id = f"{concept.id}_label_{language}"
            concept.labels[label_id] = Label(
                id=label_id,
                xlink_label=label_id,
                linkRole="http://www.xbrl.org/2003/role/label",
                language=language,
                text=label,
            )

    if yaml.get("terse_labels") is not None:
        for language, label in yaml["labels"].items():
            label_id = f"{concept.id}_terselabel_{language}"
            concept.labels[label_id] = Label(
                id=label_id,
                xlink_label=label_id,
                linkRole="http://www.xbrl.org/2003/role/terseLabel",
                language=language,
                text=label,
            )

    if yaml.get("documentation") is not None:
        for language, label in yaml["labels"].items():
            label_id = f"{concept.id}_documentation_{language}"
            concept.labels[label_id] = Label(
                id=label_id,
                xlink_label=label_id,
                linkRole="http://www.xbrl.org/2003/role/documentation",
                language=language,
                text=label,
            )

    return concept


def get_concept(
    dts: DTS,
    qname: str = None,
    id: str = None,
    yaml: dict = None,
    create_new: bool = True,
):

    if qname is not None:
        if ":" in qname and "}" not in qname:
            concept_prefix, concept_id = qname.split(":", 1)
            for dts_concept in dts.concepts.values():
                if (
                    dts_concept.id == concept_id
                    and dts_concept.ns_prefix == concept_prefix
                ):
                    return dts_concept
        else:

            for dts_concept in dts.concepts.values():
                if dts_concept.id == qname or dts_concept.name == qname:
                    return dts_concept

    if qname is not None:
        if isinstance(qname, dict):
            print("Trying to feed a dict instead of str")

        # workaround for implementation of clark notation in API
        if "}" in qname:
            qname = str(qname).replace("}", "}:")

        r = requests.get(
            f"{MetaInformation.api_uri}/qname?qname={qname}&full=true&nt_version={MetaInformation.nt_version}"
        )
    elif id is not None:
        r = requests.get(f"{MetaInformation.api_uri}/concepts/{id}?full=true")
    else:
        raise ValueError(
            "Need to query the API, but did not get an @id or QName for this lookup"
        )

    if r.status_code == 200:
        concept_json = r.json()
        itemtype = get_itemtype(dts=dts, qname=concept_json["itemtype_name"])
        concept = get_concept_from_json(concept_json, itemtype=itemtype)
        return concept

    elif r.status_code == 404:
        pass
        # print(f"could not find concept: '{qname if qname else id}'")
    else:
        print("Something went wrong when quering the API")

    # qname is not given, so we assume this is a new concept that needs to be build
    if yaml is not None and create_new is True:
        # if yaml is not None and yaml.get("labels") is not None and create_new is True:
        return build_concept(dts=dts, yaml=yaml)


def loop_presentation_hierarchy(dts: DTS, children: list, parent: AnyNode):
    for order, current_object in enumerate(children):

        current_node = AnyNode()
        for k, v in current_object.items():

            if k == "Concept":
                if v.get("qname"):
                    concept = get_concept(dts=dts, qname=v.get("qname"), yaml=v)
                elif v.get("id"):
                    concept = get_concept(dts=dts, id=v.get("id"), yaml=v)
                elif v.get("name"):
                    concept = get_concept(dts=dts, qname=v.get("name"), yaml=v)

                else:
                    concept = build_concept(dts=dts, yaml=v)

                # Add concept to DTS
                if concept.id not in dts.concepts.keys():
                    dts.concepts[concept.id] = concept

                # Add current node to parent
                current_node.order = order
                current_node.object = concept
                current_node.parent = parent

            elif k == "Linkrole":
                if isinstance(v, list):
                    raise ValueError(f"Expecting a dictionary, got a list: '{v}'")
                if not dts.roletypes.get(v.get("URI")):
                    if not v.get("id"):
                        raise Exception(
                            f"Linkrole '{v.get('URI')}' does not have an ID!"
                        )

                    if not v.get("URI"):
                        raise Exception(
                            f"Linkrole '{v.get('id')}' does not have an URI!"
                        )

                    roletype = RoleType(
                        roleURI=v.get("URI"),
                        definition=v.get("definition"),
                        id=v.get("id"),
                    )

                    # Check for wrongly indented yaml
                    for k in v.keys():
                        if "urn:" in k:
                            raise ValueError(
                                f"elr has a wrongly indented value: {roletype.roleURI}"
                            )
                    dts.directly_referenced_linkbases.append(roletype)
                    dts.roletypes[roletype.roleURI] = roletype

                else:
                    roletype = dts.roletypes.get(v.get("URI"))

                current_node.order = order
                current_node.object = roletype
                current_node.parent = parent

            else:
                print(f"Element type not recognized in linkrole hierarchy: '{k}'")

            # Loop though children if available.
            if v.get("children"):
                loop_presentation_hierarchy(
                    dts=dts, children=v.get("children"), parent=current_node
                )


def build_presentation_hierarchy(dts: DTS, presentation_hierarchy: str):
    if len(presentation_hierarchy) != 1:
        print("DTS hierarchy should have one root object. None or > 1 found!")
        dts.presentation_hierarchy = None

    else:
        if not "Concept" in presentation_hierarchy[0]:
            print("DTS hierarchy does not have a root Concept defined!")
            dts.presentation_hierarchy = None

        else:
            root_concept = presentation_hierarchy[0]["Concept"]

            if root_concept.get("qname"):
                concept = get_concept(
                    dts=dts, qname=root_concept.get("qname"), yaml=root_concept
                )
            elif root_concept.get("id"):
                concept = get_concept(
                    dts=dts, id=root_concept.get("id"), yaml=root_concept
                )
            elif root_concept.get("name"):
                concept = get_concept(
                    dts=dts, qname=root_concept.get("name"), yaml=root_concept
                )

            else:
                concept = build_concept(dts=dts, yaml=root_concept)

            root = AnyNode(object=concept)

            # Start to loop though the presentation hierarchy
            loop_presentation_hierarchy(
                dts=dts, children=root_concept.get("children"), parent=root
            )

            dts.presentation_hierarchy = root


def loop_presentation_link(dts: DTS, parent: AnyNode, yaml=dict):
    for order, yaml_element in enumerate(yaml):
        current_node = AnyNode()
        yaml_concept = yaml_element.get("Concept")

        if yaml_concept is None:
            raise ValueError(
                f"Concept has no name. Probably an YAML identation issue: {yaml_concept.get('qname')}"
            )
        elif isinstance(yaml_concept, list):
            raise ValueError(
                f"Probably an YAML identation issue: {yaml_concept} is a list, not a dict"
            )
        if yaml_concept.get("name"):
            concept = get_concept(
                dts=dts,
                qname=yaml_concept.get("name"),
                yaml=yaml_concept,
            )
        elif yaml_concept.get("qname"):
            concept = get_concept(
                dts=dts,
                qname=yaml_concept.get("qname"),
                yaml=yaml_concept,
            )
        elif yaml_concept.get("id"):
            concept = get_concept(dts=dts, id=yaml_concept.get("id"), yaml=yaml_concept)

        # Add concept to DTS
        if concept.id not in dts.concepts.keys():
            dts.concepts[concept.id] = concept

        # Add current node to parent
        current_node.order = order
        current_node.object = concept
        current_node.parent = parent

        if yaml_concept.get("children"):
            loop_presentation_link(
                dts=dts, parent=current_node, yaml=yaml_concept.get("children")
            )


def loop_lineitems(dts: DTS, yaml=dict):
    """
    Yields non-abstract items from a hierarchy.

    :param dts:
    :param yaml:
    :return:
    """
    for order, yaml_element in enumerate(yaml):
        yaml_concept = yaml_element.get("Concept")

        if yaml_concept.get("name"):
            concept = get_concept(
                dts=dts,
                qname=yaml_concept.get("name"),
                yaml=yaml_concept,
            )
        elif yaml_concept.get("qname"):
            concept = get_concept(
                dts=dts,
                qname=yaml_concept.get("qname"),
                yaml=yaml_concept,
            )
        elif yaml_concept.get("id"):
            concept = get_concept(dts=dts, id=yaml_concept.get("id"), yaml=yaml_concept)

        # Only do something if it isn't an abstract concept
        if concept.is_abstract is False:
            yield concept

            # Add concept to DTS
            if concept.id not in dts.concepts.keys():
                dts.concepts[concept.id] = concept

        if yaml_concept.get("children"):
            for child in loop_lineitems(dts=dts, yaml=yaml_concept.get("children")):
                yield child


def get_lineitems(dts: DTS, yaml: dict):
    lineItems = []

    if yaml.get("name"):
        root_concept = get_concept(
            dts=dts,
            qname=yaml.get("name"),
            yaml=yaml,
        )
        if root_concept.is_abstract is False:
            lineItems.append((1, root_concept))
    elif yaml.get("qname"):
        root_concept = get_concept(
            dts=dts,
            qname=yaml.get("qname"),
            yaml=yaml,
        )
        if root_concept.is_abstract is False:
            lineItems.append((1, root_concept))

    if yaml.get("children"):
        for i, lineitem in enumerate(
            loop_lineitems(
                dts=dts,
                yaml=yaml.get("children"),
            ),
            len(lineItems) + 1,
        ):
            # Add the lineitem to the list of lineitems
            lineItems.append((i, lineitem))

    return lineItems


def get_dimensions(dts: DTS, linkrole: RoleType, yaml_dimensions: dict):

    dimensions = []
    for yaml_dim in yaml_dimensions:
        for yaml_dim_name, yaml_dim_info in yaml_dim.items():
            dimensions.append(
                get_dimension(
                    dts=dts,
                    linkrole=linkrole,
                    yaml_dim_name=yaml_dim_name,
                    yaml_dim_info=yaml_dim_info,
                )
            )

    return dimensions


def loop_explicit_members(
    dts: DTS, yaml_members: dict, ns_prefix: str, suffix: str, parent: AnyNode
):
    for yaml_member in yaml_members:
        member_concept = Concept(
            id=f"{ns_prefix}-dm_{yaml_member.get('name')}",
            is_abstract=False,
            name=f"{yaml_member.get('name')}",
            nillable="false",
            periodType=yaml_member.get("periodtype", "duration"),
            namespace=f"http://www.nltaxonomie.nl/{MetaInformation.nt_version}/"
            f"{MetaInformation.domain}/{MetaInformation.date}/dictionary/"
            f"{MetaInformation.domain}-domains{suffix}",
            element_type=get_itemtype(
                dts=dts,
                qname=yaml_member.get("itemType", "xbrli:stringItemType"),
            ),
            substitutionGroup="sbr:domainMemberItem",
        )
        if member_concept.id not in dts.concepts.keys():
            dts.concepts[member_concept.id] = member_concept

        Member(
            concept=member_concept,
            parent=parent,
            target_linkrole=None,
            order=None,
            usable=yaml_member.get("usable", True),
        )

        if yaml_member.get("children"):
            loop_explicit_members(
                dts=dts,
                yaml_members=yaml_members["children"],
                ns_prefix=ns_prefix,
                suffix=suffix,
                parent=parent,
            )


def get_dimension(
    dts: DTS, linkrole: RoleType, yaml_dim_name: str, yaml_dim_info: dict
):

    dim_concept = get_concept(dts=dts, qname=yaml_dim_name)
    ns_prefix = f"{MetaInformation.prefix if MetaInformation.prefix else MetaInformation.domain}"
    if MetaInformation.prefix:
        suffix = f"-{MetaInformation.prefix}"
    else:
        suffix = ""
    if not dim_concept:

        if yaml_dim_name is None:
            raise ValueError(f"Concept has no name")
        dim_concept = Concept(
            id=f"{ns_prefix}-dim_{yaml_dim_name}",
            name=yaml_dim_name,
            nillable=False,
            is_abstract=True,
            substitutionGroup="xbrldt:dimensionItem",
            element_type="xbrli:stringItemType",
            periodType="duration",
            namespace=f"http://www.nltaxonomie.nl/{MetaInformation.nt_version}/"
            f"{MetaInformation.domain}/{MetaInformation.date}/dictionary/"
            f"{MetaInformation.domain}-axes{suffix}",
        )
        if dim_concept.id not in dts.concepts:
            dts.concepts[dim_concept.id] = dim_concept

    dim_obj = Dimension(
        concept=dim_concept,
    )

    if yaml_dim_info["dimensiontype"] == "typed":
        # If the dimension is typed, we need to get or create the Concept that is used for reporting
        # this value
        yaml_member = yaml_dim_info["typedMember"]
        if isinstance(yaml_member, dict):
            membername = yaml_member.get("name")
        else:
            membername = yaml_member

        if membername is None:
            raise ValueError(f"Dimension member has no name: '{yaml_member}'")
        typed_concept = get_concept(
            dts=dts, qname=membername, yaml=yaml_member, create_new=False
        )

        if not typed_concept:
            # need to create a Concept for the typed dimension, as it does not exist

            typed_concept = Concept(
                id=f"{ns_prefix}-dm_{yaml_member.get('name')}",
                is_abstract=False,
                namespace=f"http://www.nltaxonomie.nl/{MetaInformation.nt_version}/"
                f"{MetaInformation.domain}/{MetaInformation.date}/dictionary/"
                f"{MetaInformation.domain}-domains{suffix}",
                name=f"{yaml_member.get('name')}",
                nillable="false",
                element_type=get_itemtype(
                    dts=dts,
                    qname=yaml_member.get("itemType", "nl-types:string255"),
                ),
            )

        # Reference the typed member
        dim_obj.concept.typedDomainRef_obj = typed_concept

        if typed_concept.id not in dts.concepts.keys():
            dts.concepts[typed_concept.id] = typed_concept

        Member(
            concept=typed_concept,
            parent=dim_obj,
            target_linkrole=None,
            order=None,
            usable=yaml_member.get("usable", True),
        )

    elif yaml_dim_info["dimensiontype"] == "explicit":

        # Set the target linkrole for the dimension if it is explicit.

        if yaml_dim_info.get("targetrole"):
            # create a new linkrole for putting the dim/member relations into
            dim_obj.target_linkrole = RoleType(roleURI=yaml_dim_info.get("targetrole"))
            dts.directly_referenced_linkbases.append(dim_obj.target_linkrole)
            # todo: although, if a linkrole is given, we need to write those dim/mem relations
            #  to another ELR
            pass
        else:
            pass
            # set the current linkrole as the target
            dim_obj.target_linkrole = linkrole

        if yaml_dim_info.get("members"):
            loop_explicit_members(
                dts=dts,
                yaml_members=yaml_dim_info["members"],
                ns_prefix=ns_prefix,
                suffix=suffix,
                parent=dim_obj,
            )
    else:
        raise Exception(
            f"Dimensiontype '{yaml_dim_info['dimensiontype']}' not understood"
        )

    return dim_obj


def get_yaml_table_node(
    dts: DTS,
    yaml_node: dict,
    order: int,
    parent: AnyNode,
    parent_string: str,
    linkrole: RoleType = None,
):
    # Get rulesets if applicable
    if "rulesets" in yaml_node.keys():
        rulesets = []
        for yaml_ruleset in yaml_node["rulesets"]:

            if yaml_ruleset["ruleset"] == "formula-period":

                if yaml_ruleset.get("start") and yaml_ruleset.get("end"):
                    rulesets.append(
                        FormulaPeriod(
                            periodtype="duration",
                            value=None,
                            start=yaml_ruleset.get("start"),
                            end=yaml_ruleset.get("end"),
                            elem=None,
                        )
                    )
                elif yaml_ruleset.get("value"):
                    rulesets.append(
                        FormulaPeriod(
                            periodtype="instant", value=yaml_ruleset.get("value")
                        )
                    )
                else:
                    print(
                        f"formula-period rule should either have a 'start' and 'end' for duration, "
                        f"or a 'value' for an instant."
                    )
            elif yaml_ruleset["ruleset"] == "formula-explicit-dimension":

                rulesets.append(
                    FormulaExplicitDimension(
                        qname_dimension=yaml_ruleset.get("qname_dimension"),
                        qname_member=yaml_ruleset.get("qname_member", None),
                    )
                )
            elif yaml_ruleset["ruleset"] == "formula-typed-dimension":

                rulesets.append(
                    FormulaTypedDimension(
                        qname_dimension=yaml_ruleset.get("qname_dimension")
                    )
                )
            elif yaml_ruleset["ruleset"] == "ruleset":

                rulesets.append(RuleSet(tag=yaml_ruleset.get("tag")))
    else:
        rulesets = []

    node_string = f"{parent_string}.{order}"
    if yaml_node["node_type"] == "RuleNode":

        node = RuleNode(
            id=node_string,
            xlink_label=node_string,
            parent=parent,
            order=order,
            parentChildOrder=yaml_node.get("parentChildOrder", "parent-first"),
            is_abstract=yaml_node.get("abstract", "true"),
            merge=yaml_node.get("merge", "false"),
            rulesets=rulesets,
        )
    elif yaml_node["node_type"] == "ConceptRelationshipNode":

        # If the linkrole is explicitly given in the YAML file, use that instead of the LR in which
        #  the table lives.
        if yaml_node.get("linkrole"):
            linkrole = yaml_node.get("linkrole")

            linkrole_obj = [
                lr for lr in dts.directly_referenced_linkbases if lr.roleURI == linkrole
            ][0]
        else:
            linkrole_obj = linkrole

        if yaml_node.get("concept"):
            relationship_source = yaml_node.get("concept")
        else:
            # If no relationship source is given,
            # we automatically assume we want the root of this linkrole's presentation hierarchy.
            # This will be the case when using a template for example
            if hasattr(linkrole.presentation_hierarchy.object, "ns_prefix"):
                c_rel_source = linkrole.presentation_hierarchy.object.ns_prefix
            else:
                c_rel_source = [
                    k
                    for k, v in dts.namespaces.items()
                    if v == linkrole.presentation_hierarchy.object.namespace
                ][0]
            relationship_source = (
                f"{c_rel_source}:{linkrole.presentation_hierarchy.object.name}"
            )

        node = ConceptRelationshipNode(
            id=node_string,
            xlink_label=node_string,
            parent=parent,
            order=order,
            relationship_source=relationship_source,
            parentChildOrder=yaml_node.get("parentChildOrder", "parent-first"),
            linkrole=linkrole,
            arcrole=yaml_node.get(
                "arcrole", "http://www.xbrl.org/2003/arcrole/parent-child"
            ),
            formula_axis=yaml_node.get("formulaAxis", "descendant"),
        )

        # Check if linkrole is allowed to be linked to from a generic linkbase
        if "gen:link" not in linkrole_obj.used_on:
            linkrole_obj.used_on.append("gen:link")

    elif yaml_node["node_type"] == "DimensionAspectNode":
        node = AspectNode(
            id=node_string,
            xlink_label=node_string,
            parent=parent,
            order=order,
            is_abstract=yaml_node.get("abstract", "false"),
            merge=yaml_node.get("merge", "false"),
        )

        node.aspect = Aspect(aspect_type="dimension", value=yaml_node["dimension"])

    else:
        print("Rulenode type not understood")
        return None

    # Get nested children of node
    if "children" in yaml_node.keys():
        for i, child_node in enumerate(yaml_node["children"], 1):
            get_yaml_table_node(
                dts=dts,
                yaml_node=child_node,
                order=i,
                parent=node,
                parent_string=node_string,
            )

    node.parent = parent


def yaml_table_builder(dts: DTS, linkrole: RoleType, yaml_table: dict):
    # Table name is used to name breakdown and nodes as well

    table_name = yaml_table.get("name", f"{linkrole.roletype_name}Table")

    table = Table(
        yaml_table.get("parentChildOrder", "parent-first"),
        xlink_label=table_name,
        id=table_name,
    )
    if yaml_table.get("labels") is not None:
        for language, label in yaml_table["labels"].items():
            label_id = f"{table.id}_label_{language}"
            table.labels[label_id] = Label(
                id=label_id,
                xlink_label=label_id,
                linkRole="http://www.xbrl.org/2008/role/label",
                language=language,
                text=label,
            )
    else:
        label_id = f"{table.id}_label_en"
        table.labels[label_id] = Label(
            id=label_id,
            xlink_label=label_id,
            linkRole="http://www.xbrl.org/2008/role/label",
            language="en",
            text=f"{linkrole.definition} [table]",
        )

    # Get the axes of the table
    for breakdown_axis, yaml_breakdown in yaml_table["Breakdowns"].items():
        breakdown = Breakdown(
            parentChildOrder=yaml_breakdown.get("parentChildOrder", "parent-first"),
            xlink_label=f"{table_name}-{breakdown_axis}",
            id=f"{table_name}-{breakdown_axis}",
            axis=breakdown_axis,
            order=yaml_breakdown.get("parentChildOrder", "1"),
        )

        # Get the (nested) nodes under the breakdown
        for i, yaml_node in enumerate(yaml_breakdown["Nodes"], 1):
            get_yaml_table_node(
                dts=dts,
                yaml_node=yaml_node,
                order=i,
                parent=breakdown,
                parent_string=f"{table_name}-{breakdown_axis}",
                linkrole=linkrole,
            )

        table.table_breakdowns.append(breakdown)

        # Add the parameters to the table
        for yaml_param in yaml_table["Parameters"]:
            param = [p for p in dts.get_resources["parameters"] if p.name == yaml_param]
            if len(param) > 0:
                table.parameters.append(param[0])
            else:
                print(
                    f"Parameter '{yaml_param}' for Table '{table.xlink_label}' not found in DTS"
                )

    return table


def build_linkrole_hierarchy(dts: DTS, linkrole_hierarchy: str):

    print(f"Starting to loop though {len(linkrole_hierarchy.items())} linkroles")
    for linkrole_uri, linkrole_information in linkrole_hierarchy.items():
        if linkrole_uri in ["Table", "Parameter", "Hypercube", "Concept"]:
            raise ValueError(
                f"Wrong name for a linkrole. Probably an issue with yaml indentation"
            )

        print(f"Parsing information for '{linkrole_uri}'")

        # Get the linkrole if it is found in the hierarchy earlier on
        linkrole = [
            lr for lr in dts.directly_referenced_linkbases if lr.roleURI == linkrole_uri
        ]
        if len(linkrole) > 0:
            linkrole = linkrole[0]
        else:
            # Was not defined in hierarchy, added it anyway.
            linkrole = RoleType(
                roleURI=linkrole_uri,
                definition=linkrole_information.get("definition"),
                id=linkrole_information.get("id"),
            )
            dts.directly_referenced_linkbases.append(linkrole)
            dts.roletypes[linkrole_uri] = linkrole

        if not linkrole_information.get("Concept"):
            print(f"Linkrole {linkrole_uri} does not have an abstract title concept")
            lineItems = []
        else:
            yaml_concept = linkrole_information.get("Concept")

            # Check if there is not a wrongly indented information on tables or hypercubes in the Concept dict
            if yaml_concept.get("Table") or yaml_concept.get("Hypercube"):
                raise ValueError(
                    f"Am not expecting Table or Hypercube as a value of Concept in lr '{linkrole_uri}'"
                )

            # Build presentationLink
            if yaml_concept.get("qname"):
                concept = get_concept(
                    dts=dts, qname=yaml_concept.get("qname"), yaml=yaml_concept
                )
            elif yaml_concept.get("id"):
                concept = get_concept(
                    dts=dts, id=yaml_concept.get("id"), yaml=yaml_concept
                )
            elif yaml_concept.get("name"):
                concept = get_concept(
                    dts=dts, qname=yaml_concept.get("name"), yaml=yaml_concept
                )

            else:
                concept = build_concept(dts=dts, yaml=yaml_concept)
            presentationLink_hierarchy = AnyNode(object=concept)

            if concept.id not in dts.concepts.keys():
                dts.concepts[concept.id] = concept

            if yaml_concept.get("children"):
                loop_presentation_link(
                    dts=dts,
                    parent=presentationLink_hierarchy,
                    yaml=yaml_concept.get("children"),
                )

            linkrole.presentation_hierarchy = presentationLink_hierarchy
            linkrole.used_on.append("link:presentationLink")

            lineItems = get_lineitems(dts=dts, yaml=yaml_concept)

        dimensions = []
        if linkrole_information.get("Hypercube"):

            # Check for wrongly indented yaml
            for k in linkrole_information["Hypercube"].keys():
                if "urn:" in k:
                    raise ValueError(
                        f"Table has a wrongly indented value: {linkrole.roleURI}"
                    )

            # We add dimensions if they are available in the YAML
            if linkrole_information["Hypercube"].get("Dimensions"):

                dimensions = get_dimensions(
                    dts=dts,
                    linkrole=linkrole,
                    yaml_dimensions=linkrole_information["Hypercube"].get("Dimensions"),
                )

            if linkrole_information["Hypercube"].get("lineitems"):
                lineitem_linkrole = linkrole_information["Hypercube"].get("lineitems")
                lineitem_lr = dts.roletypes[lineitem_linkrole]
                if lineitem_linkrole in dts.roletypes.keys() and hasattr(
                    lineitem_lr, "hypercube"
                ):
                    if lineitem_lr.hypercube is not None:
                        lineItems.extend(
                            dts.roletypes[lineitem_linkrole].hypercube.lineItems
                        )
                    else:
                        print(
                            f"Linkrole supposed to have lineitems, but does not have a hypercube'{lineitem_linkrole}'"
                        )
                else:
                    print(
                        f"Linkrole with lineitems not known in DTS: '{lineitem_linkrole}'"
                    )

        if len(lineItems) > 0 or len(dimensions) > 0:
            # Add hypercube to linkhole if it has at least lineitems or dimensions
            if "link:definitionLink" not in linkrole.used_on:
                linkrole.used_on.append("link:definitionLink")

            linkrole.hypercube = Hypercube(
                hc_type="all",
                linkrole=linkrole,
                lineitems=lineItems,
                context_element="scenario",
                closed=True,
                dimensions=dimensions,
            )
        else:
            print(f"{linkrole_uri} has no hypercube")

        #####
        # Build tables
        #####
        if linkrole_information.get("Table"):
            yaml_table = linkrole_information["Table"]
            linkrole.tables.append(
                yaml_table_builder(dts=dts, linkrole=linkrole, yaml_table=yaml_table)
            )

            # Check for wrongly indented yaml
            for k in yaml_table.keys():
                if "urn:" in k:
                    raise ValueError(
                        f"Table has a wrongly indented value: {linkrole.roleURI}"
                    )

        elif linkrole.hypercube is not None and len(linkrole.hypercube.lineItems) > 0:
            # try and get the YAML template for tables

            if len(linkrole.hypercube.dimensions) == 0:
                # Get the template for non-dimensional tables
                yaml_table_template = yaml.load(
                    open(
                        f"{yaml_file_path.rsplit('/',1)[0]}/table_nondimensional.yaml"
                    ).read(),
                    Loader=yaml.FullLoader,
                )

            else:
                yaml_table_template = None

            if yaml_table_template is not None:
                linkrole.tables.append(
                    yaml_table_builder(
                        dts=dts, linkrole=linkrole, yaml_table=yaml_table_template
                    )
                )

            # Check if linkrole is allowed to be linked to from a generic linkbase
            if "gen:link" not in linkrole.used_on:
                linkrole.used_on.append("gen:link")


def parse_yaml(yaml):
    """
    Parses a YAML file that contains instructions on how to build a taxonomy.

    For now, it ends in writing a taxonomy to disk. In the future it should return the DTS object instead.

    :param yaml: Yaml string containing the taxonomy (meta)data
    :return: None
    """
    MetaInformation.name = yaml.get("dts")["name"]
    MetaInformation.domain = yaml.get("dts")["domain"]
    MetaInformation.prefix = yaml.get("dts")["prefix"]
    MetaInformation.date = yaml.get("dts")["date"]
    MetaInformation.nt_version = str.lower(yaml.get("dts")["nt_version"])
    MetaInformation.parent_ep = yaml.get("dts")["parent_ep"]
    MetaInformation.new_namespaces = yaml.get("dts").get("new_namespaces", [])
    MetaInformation.api_uri = yaml.get("dts").get("api_uri")

    # Start with an empty DTS
    if "dts" in yaml.keys():
        dts = DTS(name=MetaInformation.name, generic_info=MetaInformation)

        for k, v in MetaInformation.new_namespaces.items():
            dts.namespaces[k] = v["namespace"]

    else:
        raise KeyError("DTS metadata not included in YAML file")

    # ADD HARDCODED STUFF
    lineItems_concept = get_concept(
        dts=dts,
        qname="{http://www.nltaxonomie.nl/2013/xbrl/sbr-dimensional-concepts}ValidationLineItems",
    )
    if lineItems_concept and lineItems_concept.id not in dts.concepts:
        dts.concepts[lineItems_concept.id] = lineItems_concept
    hc_concept = get_concept(
        dts=dts,
        qname="{http://www.nltaxonomie.nl/2013/xbrl/sbr-dimensional-concepts}ValidationTable",
    )
    if hc_concept and hc_concept.id not in dts.concepts:
        dts.concepts[hc_concept.id] = hc_concept

    # Get datatypes, if abailable
    if "datatypes" in yaml.keys():

        # bd-types-ext3
        suffix = "" if MetaInformation.prefix is None else f"-{MetaInformation.prefix}"

        for yaml_datatype in yaml["datatypes"]:
            datatype = ItemType(
                id=yaml_datatype.get("id"),
                name=yaml_datatype.get("id"),
                domain=f"{MetaInformation.domain}-types{suffix}",
                base=yaml_datatype.get("base"),
                totalDigits=yaml_datatype.get("totalDigits"),
                restriction_pattern=yaml_datatype.get("pattern"),
                fraction_digits=yaml_datatype.get("fractionDigits"),
                min_inclusive=yaml_datatype.get("minInclusive"),
                max_inclusive=yaml_datatype.get("maxInclusive"),
                min_length=yaml_datatype.get("minLength"),
                max_length=yaml_datatype.get("maxLength"),
            )
            dts.datatypes.append(datatype)

    # Get parameters, if available
    if "parameters" in yaml.keys():
        lr_role_link = [
            lr
            for lr in dts.linkbases
            if lr.role_uri == "http://www.xbrl.org/2008/role/link"
        ]
        if len(lr_role_link) < 1:
            lr_role_link = RoleType(roleURI="http://www.xbrl.org/2008/role/link")
            dts.roletypes[lr_role_link.roleURI] = lr_role_link
        else:
            lr_role_link = lr_role_link[0]

        for yaml_param in yaml["parameters"]:
            param = Parameter(
                as_datatype=yaml_param.get("as"),
                xlink_label=yaml_param.get("name"),
                name=yaml_param.get("name"),
                id=yaml_param.get("name"),
                required=yaml_param.get("required"),
                select=yaml_param.get("select"),
            )
            lr_role_link.resources["parameters"].append(param)

    print("Starting to loop through linkrole hierarchies")
    # Loop though linkrole hierarchies.
    build_linkrole_hierarchy(
        dts=dts, linkrole_hierarchy=yaml.get("linkrole_hierarchies")
    )

    print(f"Starting to build presentation hierarchy")
    # Build the presentation hierarchy.
    build_presentation_hierarchy(
        dts=dts, presentation_hierarchy=yaml.get("presentation_hierarchy")
    )

    # # Write the DTS to SQL
    # sqlwriter = DtsToSql()
    # print(f" Saving to the database: {dts.entrypoint_name}")
    # sqlwriter.set_dts(dts)

    # Start writing the EP
    print("starting to export the taxonomy")
    taxonomy_builder = TaxonomyBuildingWorker(dts_list=[dts], directory="/tmp/xbrl")
    taxonomy_builder.run()


file = open(yaml_file_path, "r")

parse_yaml(yaml.load(file.read(), Loader=yaml.FullLoader))
