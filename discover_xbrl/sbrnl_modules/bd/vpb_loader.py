from tokenize import tokenize

from lxml import etree

from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.dimensions.classes import (
    Dimension,
    Member,
)
from discover_xbrl.sbrnl_xbrl_parser.schema.classes import ItemType, Concept
from discover_xbrl.sbrnl_xbrl_parser.schema.subparser.resources import discover_itemtype

file_gsp = "/mnt/data/work2/tuples/bd_ihz/docs/GSP_IHZ-v2-VPB_DA-2021_4.xml"

file_hierarchy = "/mnt/data/work2/tuples/bd_ihz/docs/Relatie_BMG-IHZ-VPB_DA-2021_20210610_7.1.1_v2.0.xsd"

itemTypes = []
domeinen = []
rapportages = []
groepen = []

debug = True

with open(file_hierarchy, encoding="utf-8") as hierarchy:
    tree_hierarchy = etree.fromstring(bytes(hierarchy.read(), encoding="utf=8"))

    for tree_elem in tree_hierarchy:
        """
        Running through the tree hierarchy explicitly.
        Here we could also use XPath to more specifically get the information we'd need,
        but this way we can also check if we missed anything.
        """
        if tree_elem.tag == "{http://www.w3.org/2001/XMLSchema}element":
            # This is the start of the hierarchy
            pass
        elif tree_elem.tag == "{http://www.w3.org/2001/XMLSchema}simpleType":
            # Should become itemTypes

            itemtype_name = tree_elem.get("name")
            itemtype = ItemType(id=itemtype_name, name=itemtype_name, domain="bd-types")
            for itemtype_elem in tree_elem:
                if itemtype_elem.tag == "{http://www.w3.org/2001/XMLSchema}restriction":
                    itemtype.base = itemtype_elem.get("base")
                    enums = []
                    for res_e in itemtype_elem:
                        if res_e.tag == "{http://www.w3.org/2001/XMLSchema}minLength":
                            itemtype.restriction_min_length = res_e.get("value")
                        elif res_e.tag == "{http://www.w3.org/2001/XMLSchema}maxLength":
                            itemtype.restriction_max_length = res_e.get("value")
                        elif (
                            res_e.tag
                            == "{http://www.w3.org/2001/XMLSchema}maxInclusive"
                        ):
                            itemtype.max_inclusive = res_e.get("value")
                        elif (
                            res_e.tag
                            == "{http://www.w3.org/2001/XMLSchema}minInclusive"
                        ):
                            itemtype.min_inclusive = res_e.get("value")
                        elif (
                            res_e.tag
                            == "{http://www.w3.org/2001/XMLSchema}minInclusive"
                        ):
                            itemtype.min_inclusive = res_e.get("value")
                        elif res_e.tag == "{http://www.w3.org/2001/XMLSchema}length":
                            itemtype.restriction_length = res_e.get("value")
                        elif (
                            res_e.tag == "{http://www.w3.org/2001/XMLSchema}totalDigits"
                        ):
                            itemtype.totalDigits = res_e.get("value")
                        elif res_e.tag == "{http://www.w3.org/2001/XMLSchema}pattern":
                            itemtype.restriction_pattern = res_e.get("value")
                        elif (
                            res_e.tag
                            == "{http://www.w3.org/2001/XMLSchema}fractionDigits"
                        ):
                            itemtype.fraction_digits = res_e.get("value")
                        elif (
                            res_e.tag == "{http://www.w3.org/2001/XMLSchema}enumeration"
                        ):
                            enums.append(res_e.get("value"))
                        else:
                            # todo: union
                            print(f"restriction unknown: {res_e.tag}")
                    pass
                else:
                    print(f"Unknown itemtype attr: {itemtype_elem.tag}")
            itemtype.enumerations = enums
            itemTypes.append(itemtype)


class BD_Domein:
    def __init__(self, naam, elem=None):
        self.naam = naam

        if elem is not None:
            for dom_subelem in dom_elem:
                for attr in dom_subelem.attrib:
                    # check if there are attributes here
                    print(f"unknown attr: {attr}")
                for subsubelem in dom_subelem:
                    print(f"Unused subelement: {subsubelem.tag}")

                if dom_subelem.tag == "Formaat":
                    """
                    BD specificatie (7. Indeling gegevensspecificatie):
                    Weergave van het door de Belastingdienst toegekende attribuuttype.
                    Het formaat en de domeinen worden hieronder weergegeven.

                    7.1 Toelichting op formaat
                    an = alfanummeriek
                    a1 == min/max 1 int
                    an..30 == max 30 str
                    """
                    if "an" in dom_subelem.text:
                        self.format_type = "string"
                    elif "a" in dom_subelem.text:
                        self.format_type = "int"
                    elif "n" in dom_subelem.text:
                        self.format_type = "int"
                    else:
                        print(f"format unknown! '{dom_subelem.text}'")

                    self.formaat = dom_subelem.text
                elif dom_subelem.tag == "Waardebereik":
                    # todo: EK: Dit lijkt een uitgeschreven uitleg te zijn. Parsen?

                    self.waardebereik = dom_subelem.text
                elif dom_subelem.tag == "Masker":
                    self.masker = dom_subelem.text

                else:
                    print(f"Domeintype subeleemenr: {dom_subelem.tag}")


def get_domainmember_hiearchy(elem, parent, root=False):
    """
    Attempt to build a domainMember hierarchy from a dimension elem.
    """
    domainMember_name = elem.get("naam")
    domain_member = None
    # Abstract concept should be saved somewhere as well.
    # Shouldn't this just be a linkrole?

    for domainMember in elem:

        if domainMember.tag == "Groep":

            # todo: these seem to just be abstract headers in the table.
            #  should I try to add all elements under this group to the dimension?
            #  and separately build some kind of table structure?
            pass
            """
            Arghh wacht eens, waar moet dit ding aan vast hangen?
            """

        elif domainMember.tag == "Element":
            member_name = domainMember.get("naam")
            tokenized_member_name = str(member_name).rstrip().replace(" ", "")

            member_concept = Concept(
                id=tokenized_member_name,
                name=tokenized_member_name,
                nillable=False,
                substitutionGroup="sbr:domainMemberItem",
                element_type="xbrli:stringItemType",
                periodType="",
            )

            domain_member = Member(
                concept=member_concept,
                parent=parent,
                target_linkrole=None,
                order=None,
                usable=None,
            )
    if domain_member:
        return domain_member
    else:
        pass
        # I guess this can happen if this layer is only an abstract concept acting as a lable


def create_dimension(elem, typed_domainref=None):
    """
    Dimension for DomainMembers to be nested under.
    Level 3 of the specifications
    """
    dimension_name = elem.get("naam")

    for attr in elem.attrib:
        if attr != "naam":
            print("Has attrib other than name!")

    tokenized_dimension_name = str(dimension_name).rstrip().replace(" ", "")
    dimension_concept = Concept(
        id=tokenized_dimension_name,
        name=tokenized_dimension_name,
        is_abstract=True,
        nillable=False,
        substitutionGroup="xbrldt:dimensionItem",
        element_type="xbrli:stringItemType",
        periodType="",
    )
    dimension = Dimension(concept=dimension_concept)
    if typed_domainref is not None:
        dimension_concept.typedDomainRef = typed_domainref
        # todo: add target_linkrole to Dimension() as well

    dimension.domainMember_hiearchy = get_domainmember_hiearchy(
        elem=elem, parent=dimension, root=True
    )

    return dimension


class Chapter:
    """
    Chapter of a report. Level 2 of the specifications
    For us, this level will be the linkroles
    """

    def __init__(self, name=None, elem=None):
        self.name = name

        self.dimensions = []
        # Elements are non abstract, but should be placed a level deeper.
        # Perhaps we can create a generic dimension for these.
        self.xml_orphaned_elements = []

        for attr in elem.attrib:
            if attr != "naam":
                print("Has attrib other than name!")

        for dimension in elem:
            if dimension.tag == "Groep":
                # todo: get dimensions here and put them in self.dimensions
                #  nicest would be if we could already add a concept to go with it and directly use the datamodel
                #  of the parser
                self.dimensions.append(create_dimension(dimension))
                # get_subgroup(group=dimension)
            elif dimension.tag == "Element":
                # We do not expect reportable elements on this level. We need to create a generic dimension for these.
                self.xml_orphaned_elements.append(dimension)

            else:
                print("Atts for linkrole?")

    def __repr__(self):
        return self.name


class Report:
    """
    A (separate?) report. Level 1 of the specifications
    """

    def __init__(self, name, chapters=None, elem=None):
        self.name = name
        self.chapters = chapters if chapters else []

        self.xml_orphaned_elements = []

        if elem is not None:
            # build the report if an (xml) element is given:
            for group in elem:
                if group.tag == "Groep":
                    self.chapters.append(Chapter(name=group.get("naam"), elem=group))
                elif group.tag == "Element":
                    # On the chapter level we have ways to place these elements
                    # but here, on the report level we cannot place them in a generic dimension.
                    # If there are a lot of elements here, this plan is not going to work.
                    self.xml_orphaned_elements.append(group)
                else:
                    print("Should not have anything not-abstract on this level")

    def __repr__(self):
        return self.name


class BD_Condition:
    def __init__(self, elem):
        self.name = elem.get("naam")

        specification = elem.find("Specificatie")
        self.specification = specification.text if specification is not None else None

        explanation = elem.find("Toelichting")
        self.explanation = explanation.text if explanation is not None else None

    def __repr__(self):
        return self.name


class BD_Rule:
    """
    Een regel heeft 1 of meerdere conditie, en elke conditie heeft een specificatie en optioneel een toelichting.
    """

    def __init__(self, elem):
        self.conditions = []
        for element_attribute in elem:
            if element_attribute.tag == "Conditie":
                self.conditions.append(BD_Condition(elem=element_attribute))


class BD_Element:
    def __init__(self, name, elem=None):
        self.name = name

        for element_attribute in elem:
            if element_attribute.tag == "Bron":
                self.source = element_attribute.text
            elif element_attribute.tag == "Domein":
                self.domain = element_attribute.text
            elif element_attribute.tag == "Definitie":
                self.definition = element_attribute.text
            elif element_attribute.tag == "Formaat":
                self.format = element_attribute.text
            elif element_attribute.tag == "Gegevensregel":
                if not hasattr(self, "rule"):
                    self.rule = BD_Rule(element_attribute)
                else:
                    print("Already has a rule")
            elif element_attribute.tag == "Heeftrelatiemet":
                self.hasrelation = element_attribute.text
            elif element_attribute.tag == "Identificatienummer":
                self.id = element_attribute.text
            elif element_attribute.tag == "Middel":
                self.means = element_attribute.text
            elif element_attribute.tag == "Toelichting":
                self.explanation = element_attribute.text
            else:
                print("Element attribute unknown")

    def __repr__(self):
        return self.name


def get_subgroup(group, indent=0):
    """Niet benutte jaarruimten (7)
    Main test method for working through the hiearchy.

    Observaties tot zover:
    -   Elements zijn nooit genest, wat ik wel zou verwachten bij geneste, rapporteerbare elementen
    -   Er zijn veel abstracte 'groepen' met < 25 kinderen. Dit zijn goede kandidaten om te bundelen tot 1 table/LR
        -   Sommige van deze groepen lijken meer op dimensies (Niet benutte jaarruimten) waar over 7 jaren de niet benutte jaarruimte wordt uitgevraagd
    -   Er zijn hier en daar wat totalen. Deze zouden goed in een 'rollup' passen.


    """
    indent_size = ""
    for i in range(indent):
        indent_size = "  " + indent_size

    for elem in group:
        elem_type = None
        if elem.tag == "Groep" and indent == 0:
            elem_type = "Linkrole: "

        elif elem.tag == "Groep" and indent >= 1:
            elem_type = "Abstract: "
            # todo: when abstract (label) and when dimension? (probably if it has an element as a child?
        elif elem.tag == "Element":
            elem_type = "E: "
            # Observatie: Elements zitten nooit genest.
            #   De (XBRL) tuples lijken dus niet uit geneste niet-abstracte constructies voort te komen.
            e = BD_Element(name=elem.get("naam"), elem=elem)

        elif elem.tag == "DomeinType":
            pass  # todo: some really weird way of trying to explain what kind of itemType needs to be used, maybe?
        else:
            print("Unknown tag?")

        if elem_type is not None:

            # counter to easier see how 'big' an abstract part is.
            if elem.tag == "Groep":
                amount_descendants = 0
                for a in elem.iterdescendants(tag=["Groep", "Element"]):
                    amount_descendants += 1
                if amount_descendants > 25:
                    print(f"=======Level: {indent}")

                amount_descendants = f"({amount_descendants})"
            # elif elem.tag == "Element":
            #     # todo: iets doen met deze elementen
            #     if hasattr(e, "rule"):
            #         amount_descendants = f"conditions: {len(e.rule.conditions)}"
            #     else:
            #         amount_descendants = "No Business rule"
            else:
                amount_descendants = ""

            print(
                f"{indent_size}{elem_type} {elem.get('naam', None)} {amount_descendants}"
            )

            # Only gp for subgroups for, well groups
            if elem.tag == "Groep":
                get_subgroup(elem, indent + 1)


with open(file_gsp, encoding="utf-8") as gsp:
    tree_gsp = etree.fromstring(bytes(gsp.read(), encoding="utf=8"))

    modelnaam = tree_gsp.get("type")
    modelversie = tree_gsp.get("versie")

    for elem in tree_gsp:
        # todo: iets doen met de GSP
        if elem.tag == "DomeinDefinities":
            for dom_elem in elem:

                if dom_elem.tag == "DomeinType":
                    domein_naam = dom_elem.get("naam")
                    domeinen.append(BD_Domein(naam=domein_naam, elem=dom_elem))

                else:
                    print(f"Unknown DomDef: {dom_elem.tag}")

        elif elem.tag == "Groep":
            for elem_reports in elem:
                # de eerste laag lijken 'losse' formulieren te zijn. Dit zou een EP kunnen zijn of een deel ervan
                rapportages.append(
                    Report(name=elem_reports.get("naam"), elem=elem_reports)
                )
            get_subgroup(elem)
            # # elif elem.tag == "Groep":
            # #     report_name = elem.get("naam")
            # #     print(report_name)
            # #     # for grp_elem in elem:
            #     #     print(grp_elem.tag)
            #
            #     pass  # todo
            # else:
            #     print(elem.tag)
        else:
            print(f"document part not understood: {elem.tag}")
