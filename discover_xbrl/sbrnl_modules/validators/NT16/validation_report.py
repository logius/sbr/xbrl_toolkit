import operator
import subprocess
import os
import time
from datetime import datetime
from jinja2 import Environment, FileSystemLoader, select_autoescape
from discover_xbrl.conf.conf import Config

conf = Config()
env = Environment(
    loader=FileSystemLoader(f"{conf.project_root}/templates"),
    autoescape=select_autoescape(["html", "xml"]),
)


class ReportRule:
    def __init__(self, parent, rule):
        self.report = parent
        self.rule = rule
        self.violations = []
        self.level = "GOOD"
        self.times = 0

    def add_violation(self, message):
        for violation in self.violations:
            if message == violation.message:
                return
        self.times += 1
        self.level = self.rule.severity
        self.violations.append(ValidationViolation(parent=self, message=message))

    def __lt__(self, other):
        return self.rule.rule_nr < other.rule.rule_nr


class ValidationViolation:
    def __init__(self, parent, message):
        self.rule = parent
        self.message = message


class Report:
    def __init__(self, name, version, subversion=None):
        self.report_name = name
        self.nt_version = version
        self.subversion = subversion
        self.report_date = datetime.now()
        self.rules = []
        self.levels = ["GOOD"]  # because good ones don't report themselves.

    def add_rule(self, rule):
        already_here = False
        for known_rule in self.rules:
            if rule.rule_nr == known_rule.rule.rule_nr:
                already_here = True
        if not already_here:
            self.rules.append(ReportRule(self, rule))

    def add_rule_violation(self, rule, message):
        for r in self.rules:
            if r.rule == rule:
                if rule.severity not in self.levels:
                    self.levels.append(rule.severity)
                r.add_violation(message)

    def update_name_subversion(self, ep):
        parts = ep.split("/")
        parts.reverse()
        if parts[2] and parts[4]:
            self.nt_version = parts[4].lower()
            if "." in parts[2]:
                self.subversion = parts[2][-1]

    def generate_report(self, entrypoint_name=None, writefile=True):
        """Methode die het huidige report naar htlm omzet"""
        if entrypoint_name:
            self.report_name = entrypoint_name
        template = env.get_template("validation_report.html")
        output = template.render(report=self)
        indextemplate = env.get_template("index.html")
        topindextemplate = env.get_template("top_index.html")

        if writefile:
            filename = f"{self.report_name.replace('/', '_')}.html"
            top_report_dir = conf.validator.get("report_dir", "/tmp/xbrl")
            subdir = f"{self.nt_version}"
            if self.subversion:
                subdir += f".{self.subversion}"
            report_dir = f"{top_report_dir}/{subdir}"
            os.makedirs(report_dir, exist_ok=True)
            new_report = True if os.path.isfile(f"{report_dir}/{filename}") else False
            if conf.validator.get(
                "autocommit_reports", False
            ):  # als we commiten, eerst pullen!
                subprocess.run(["git", "pull", "origin", "master"], cwd=report_dir)

            with open(f"{report_dir}/{filename}", "+w") as outfile:
                outfile.write(output)
            with open(f"{report_dir}/index.html", "+w") as outfile:
                filelist = [
                    {"name": xsd, "mtime": ""}
                    for xsd in sorted(os.listdir(report_dir))
                    if xsd.endswith(".xsd.html")
                ]
                for file in filelist:
                    file["mtime"] = datetime.strptime(
                        time.ctime(
                            os.path.getmtime(os.path.join(report_dir, file["name"]))
                        ),
                        "%a %b %d %H:%M:%S %Y",
                    )
                output = indextemplate.render(filelist=filelist)
                outfile.write(output)

            with open(f"{top_report_dir}/index.html", "+w") as outfile:
                files = os.scandir(top_report_dir)
                filelist = [dir for dir in files if dir.is_dir()]
                filelist.sort(key=lambda x: x.name)
                output = topindextemplate.render(filelist=filelist)
                outfile.write(output)

            if conf.validator.get("autocommit_reports", False):
                subprocess.run(["git", "add", filename], cwd=report_dir)
                subprocess.run(["git", "add", "index.html"], cwd=report_dir)
                subprocess.run(["git", "add", "index.html"], cwd=top_report_dir)
                if new_report:
                    commit_message = f"Validatierapport: {filename} opnieuw gemaakt. De index is ververst"
                else:
                    commit_message = f"Validatierapport: {filename} toegevoegd. Opgenomen in de index"
                subprocess.run(["git", "commit", "-m", commit_message], cwd=report_dir)
                subprocess.run(["git", "push", "origin", "master"], cwd=report_dir)

    @staticmethod
    def get_reportname_from_name(name):
        parts = name.split("/")
        parts.reverse()
        name = parts[0]
        ep_nt_version = parts[4]
        ep_sub_version = parts[2].split(".")
        filename = f"/{ep_nt_version}"
        if len(ep_sub_version) > 1:
            filename += f".{ep_sub_version[1]}"
        filename += f"/{name.replace('/', '_')}.html"
        return filename
