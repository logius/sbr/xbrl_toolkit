import logging
import codecs
import os
from pathlib import PosixPath
from ..classes import ValidationRule
from ..parameters import get_all_allowed_namespaces

logger = logging.getLogger("validator")


def nta_2_01_00_06(namespace):
    # only known namespaces are allowed
    return namespace not in get_all_allowed_namespaces()


def nta_2_01_00_09(filename):
    # no BOM allowed
    if type(filename) == PosixPath:
        with open(filename, "rb") as file:
            contents = file.read(4)
            return contents.startswith((codecs.BOM_UTF8, codecs.BOM_UTF16))
    else:
        return False


def nta_2_01_00_11(filename):
    # only remote, Iseem tomiss the file cache ...
    return len(os.path.basename(filename)) > 140


def generic_base():

    rules = [
        ValidationRule(
            rule_nr="2.01.00.06",
            test_type="model_namespaces",
            text="De NT MOET NIET refereren aan externe DTS'n die niet geautoriseerd zijn door SBR-NT-beheer",
            hardcoded_test=nta_2_01_00_06,
        ),
        ValidationRule(
            rule_nr="2.01.00.09",
            test_type="both_path_hardcoded",
            text="BOM karakters MOETEN NIET voorkomen in bestanden die de een NT of PE taxonomie vormen",
            hardcoded_test=nta_2_01_00_09,
        ),
        ValidationRule(
            rule_nr="2.01.00.11",
            test_type="both_path_hardcoded",
            text="De maximale lengte van bestandsnamen is 140 karakters.",
            hardcoded_test=nta_2_01_00_11,
        ),
    ]
    return rules
