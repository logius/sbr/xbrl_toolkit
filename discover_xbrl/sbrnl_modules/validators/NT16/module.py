from .logging import setup_logger
from .start import Validator

"""
File that loads all relevant module parts.
"""


def load_module(module_object={}):

    setup_logger()

    module_object["name"] = "NT16"
    module_object["type"] = "validation"
    module_object["setup_validator"] = Validator

    return module_object
