import logging

logger = logging.getLogger("validator")


class ValidationRule:
    def __init__(
        self,
        rule_nr,
        text,
        test_type,
        hardcoded_test=None,
        sourceline=None,
        xpath=None,
        tag=None,
        note=None,
        severity=None,
        subset_xpath=None,
        offender=None,
    ):
        # Generic information
        self.rule_nr = rule_nr
        self.text = text
        # Subject information
        self.sourceline = sourceline
        # Text expressions
        self.xpath = xpath
        self.tag = tag
        self.hardcoded_test = hardcoded_test
        self.subset_xpath = subset_xpath
        self.offender = offender

        # Developer information
        self.note = note
        self.severity = (
            severity if severity else "ERROR"
        )  # unless stated other wise, this is an ERROR
        self.test_type = test_type
        self.input_type = self.set_input_type()

    def set_input_type(self):
        test_types = [
            "note",
            "both_hardcoded",
            "both_path_hardcoded",
            "schema_xpath",
            "schema_tag",
            "schema_hardcoded",
            "schema_file_hardcoded",
            "schema_hardcoded_url_etree",
            "linkbase_xpath",
            "linkbase_tag",
            "linkbase_hardcoded",
            "linkbase_file_hardcoded",
            "linkbase_hardcoded_url_etree",
            "linkbase_hardcoded_subset",
            "model_element",
            "model_datatype",
            "model_linkrole",
            "model_hypercube",
            "model_referenced_linkrole",
            "model_namespaces",
            "model_label",
        ]
        if self.test_type in test_types:

            if self.test_type.startswith("model"):
                # test should be run after the model is build
                return "model"
            elif self.test_type.startswith("schema"):
                # test is run during schema discovery
                return "schema"
            elif self.test_type.startswith("linkbase"):
                # test is run during schema discovery
                return "linkbase"
            elif self.test_type.startswith("both"):
                # test is run during schema and linkbase discovery
                return "both"
            elif self.test_type == "note":
                # non-testable rule / placeholder
                return "note"
            else:
                return None

        else:
            logging.error(f"validationrule {self.rule_nr} has an unknown type")

    @property
    def wikipage(self):
        return f"https://www.wikixl.nl/wiki/sbr/index.php/{self.rule_nr}"

    def __str__(self):
        return f"{self.rule_nr}: {self.text}"
