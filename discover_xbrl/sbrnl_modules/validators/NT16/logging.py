import logging
import os
from datetime import datetime

from discover_xbrl.conf.conf import Config


def setup_logger():
    """
    # Set all loggers for the validator
    #"""

    """
    TODO:
    issue: these loggers are initalized after loading the files where they are used. For example: start.py
    """
    conf = Config()

    if os.path.exists(f"{conf.logging['maindir']}/validator_report.tmp"):

        os.rename(
            f"{conf.logging['maindir']}/validator_report.tmp",
            f"{conf.logging['maindir']}/validator_report_{datetime.now().strftime('%Y-%m-%dT%H_%M_%S.%f%z')}.tmp",
        )

    # Setup validator logger
    logger_validator = logging.getLogger("validator")
    logger_validator.propagate = False  # Do not send error lines to parent logger
    logger_validator.setLevel(
        logging.INFO
    )  # container for parser loggers. Do not log info and debug
    logger_validator.addHandler(
        logging.FileHandler(f"{conf.logging['maindir']}/validator.log", "a+")
    )
    logger_validator.addHandler(logging.StreamHandler())
    logger_validator.addHandler(
        logging.FileHandler(f"{conf.logging['maindir']}/validator_report.tmp", "a+")
    )

    # Setup linkbase logger
    logger_validator_linkbase = logging.getLogger("validator.linkbase")
    logger_validator_linkbase.propagate = (
        False  # Do not send error lines to parent logger
    )
    logger_validator_linkbase.setLevel(logging.DEBUG)
    logger_validator_linkbase.addHandler(
        logging.FileHandler(f"{conf.logging['maindir']}/validator_linkbase.log", "a+")
    )
    logger_validator_linkbase.addHandler(logging.StreamHandler())
    logger_validator_linkbase.addHandler(
        logging.FileHandler(f"{conf.logging['maindir']}/validator_report.tmp", "a+")
    )

    # Setup schema logger
    logger_validator_schema = logging.getLogger("validator.schema")
    logger_validator_schema.propagate = (
        False  # Do not send error lines to parent logger
    )
    logger_validator_schema.setLevel(logging.DEBUG)
    logger_validator_schema.addHandler(
        logging.FileHandler(f"{conf.logging['maindir']}/validator_schema.log", "a+")
    )
    logger_validator_schema.addHandler(logging.StreamHandler())
    logger_validator_schema.addHandler(
        logging.FileHandler(f"{conf.logging['maindir']}/validator_report.tmp", "a+")
    )

    # Setup naming_conventions logger
    logger_validator_naming_conventions = logging.getLogger(
        "validator.naming_conventions"
    )
    logger_validator_naming_conventions.propagate = (
        False  # Do not send error lines to parent logger
    )
    logger_validator_naming_conventions.setLevel(logging.DEBUG)
    logger_validator_naming_conventions.addHandler(
        logging.FileHandler(
            f"{conf.logging['maindir']}/validator_naming_conventions.log", "a+"
        )
    )
    logger_validator_naming_conventions.addHandler(logging.StreamHandler())
    logger_validator_naming_conventions.addHandler(
        logging.FileHandler(f"{conf.logging['maindir']}/validator_report.tmp", "a+")
    )

    # Setup naming_conventions logger
    logger_validator_exceptions = logging.getLogger("validator.exceptions")
    logger_validator_exceptions.propagate = (
        False  # Do not send error lines to parent logger
    )
    logger_validator_exceptions.setLevel(logging.DEBUG)
    logger_validator_exceptions.addHandler(
        logging.FileHandler(f"{conf.logging['maindir']}/validator_exceptions.log", "a+")
    )
    logger_validator_exceptions.addHandler(logging.StreamHandler())
    logger_validator_exceptions.addHandler(
        logging.FileHandler(f"{conf.logging['maindir']}/validator_report.tmp", "a+")
    )
