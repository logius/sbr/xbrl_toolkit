import re
import logging

from ..classes import ValidationRule
from .namespace_prefix import nt_domains

logger = logging.getLogger("validator.naming_conventions")
ignore_list = ["http://www.xbrl.org", "www.nltaxonomie.nl"]


def nta_3_02_11_01(linkbase):
    xlink_labels = linkbase.xpath(
        "//link:loc[@xlink:label and @xlink:type='locator']", namespaces=linkbase.nsmap
    )
    for xlink_label in xlink_labels:
        id = xlink_label.attrib.get("{http://www.w3.org/1999/xlink}href")
        if "#" in id:
            id = id.split("#")[1]
        label = xlink_label.attrib.get("{http://www.w3.org/1999/xlink}label")
        if label != f"{id}_loc":
            # this wil report only the first offense, but I think that's OK
            # if you offend once you probably did it on all occasions
            return True
    return False


def nta_3_02_11_02(linkbase):
    xlink_labels = linkbase.xpath(
        "//link:reference[@xlink:label and @xlink:type='resource']",
        namespaces=linkbase.nsmap,
    )
    for xlink_label in xlink_labels:
        id = xlink_label.attrib.get("id")
        label = xlink_label.attrib.get("{http://www.w3.org/1999/xlink}label")
        parts = label.split("_")
        if parts[0] not in nt_domains:
            return True
        if parts[-1] != "ref":
            return True
        if id not in label:
            return True
    return False


def nta_3_02_11_03(xlink_label):
    label = xlink_label.attrib.get("{http://www.w3.org/1999/xlink}label")
    for url in ignore_list:
        if url in label:
            return False
    if not re.match("^[\.A-Za-z0-9_-]*$", label):
        return True
    return False


def element_xlink_label():
    rules = [
        ValidationRule(
            rule_nr="3.02.11.01",
            test_type="linkbase_hardcoded",
            text="Een @xlink:label voor een locator BEHOORT te bestaan uit de @id van de gerefereerde XML node, een underscore gevolgd door de tekens ‘loc’",
            hardcoded_test=nta_3_02_11_01,
            severity="WARNING",
        ),
        ValidationRule(
            rule_nr="3.02.11.02",
            test_type="linkbase_hardcoded",
            text="Een @xlink:label voor een reference resource MOET bestaan uit de namespace prefix van het schema waarin de linkrole URI is gedefinieerd, underscore, unieke string van de tekens, underscore, unieke deel van de role, underscore, ‘ref’",
            hardcoded_test=nta_3_02_11_02,
            severity="WARNING",
        ),
        ValidationRule(
            rule_nr="3.02.11.03",
            test_type="linkbase_hardcoded_subset",
            subset_xpath="//*[@xlink:label]",
            offender="{http://www.w3.org/1999/xlink}label",
            text="Een @xlink:label MOET alleen a-zA-Z0-9_-. tekens gebruiken",
            hardcoded_test=nta_3_02_11_03,
        ),
    ]
    return rules
