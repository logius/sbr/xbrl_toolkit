import logging

from ..classes import ValidationRule

logger = logging.getLogger("validator.naming_conventions")


def nta_3_02_01_02(path):
    # all but the filename, that's tested in rue #5
    start = max(0, len(path.parts) - 5)
    for part in path.parts[start:-1]:
        if not part.lower() == part or " " in part:
            return True
    return False


def nta_3_02_01_03(path):
    start = max(0, len(path.parts) - 5)
    for part in path.parts[start:-1]:
        if len(part) > 15:
            return True
    return False


def nta_3_02_01_04(path):
    """
    domain
        eejjmmdd
            dictionary
            entrypoints
            presentation
            validation
    """
    main_folders = ["dictionary", "entrypoints", "presentation", "validation"]
    if path.parts[-2] not in main_folders:
        return True
    return False


def nta_3_02_01_05(path):
    if not path.parts[-1].islower():
        return True
    return False


def nta_3_02_01_09(path, linkbase):
    if linkbase.tag == "{http://www.w3.org/2001/XMLSchema}schema":
        # If the given element is actually a schema, do not check further
        return False

    if (
        linkbase.xpath("//link:labelLink", namespaces=linkbase.nsmap)
        and not "-lab" in path.parts[-1]
    ):
        return True

    if (
        linkbase.xpath("//link:definitionLink", namespaces=linkbase.nsmap)
        and not "-def" in path.parts[-1]
    ):
        return True

    if (
        linkbase.xpath("//link:referenceLink", namespaces=linkbase.nsmap)
        and not "-ref" in path.parts[-1]
    ):
        return True

    if (
        linkbase.xpath("//link:presentationLink", namespaces=linkbase.nsmap)
        and not "-pre" in path.parts[-1]
    ):
        return True

    return False


def nta_3_02_01_10(path, linkbase):
    roleRefs = linkbase.xpath("//link:linkbase/link:roleref", namespaces=linkbase.nsmap)
    for roleref in roleRefs:
        # Generic label link
        if (
            roleref.get("roleURI") == "http://www.xbrl.org/2008/role/label"
            and not "-generic-lab" in path.parts[-1]
        ):
            return True

        if (
            roleref.get("roleURI") == "http://www.xbrl.org/2008/role/reference"
            and not "-generic-ref.xml" in path.parts[-1]
        ):
            return True

        if (
            roleref.get("roleURI") == "http://www.xbrl.org/2010/role/message"
            and not "-generic-msg" in path.parts[-1]
        ):
            return True

        if (
            roleref.get("roleURI") == "http://xbrl.org/2008/formula"
            and not "-generic-for.xml" in path.parts[-1]
        ):
            return True

        if (
            roleref.get("roleURI")
            == "http://www.nltaxonomie.nl/2011/role/linkrole-info"
            and not "-generic-linkrole-order.xml" in path.parts[-1]
        ):
            return True

        if (
            roleref.get("roleURI")
            == "http://www.nltaxonomie.nl/2017/arcrole/ConceptDisclosure"
            and not "-generic-conceptdisclosure.xml" in path.parts[-1]
        ):
            return True

        if (
            roleref.get("roleURI")
            == "http://www.nltaxonomie.nl/2017/arcrole/ConceptMemberEquivalence"
            and not "-generic-conceptmemberequivalence.xml" in path.parts[-1]
        ):
            return True

        if (
            roleref.get("roleURI")
            == "http://www.nltaxonomie.nl/2017/arcrole/ConceptPolicy"
            and not "-generic-conceptpolicy.xml" in path.parts[-1]
        ):
            return True

        if (
            "http://xbrl.org/arcrole/2008/variable" in roleref.get("roleURI")
            and not "-for" in path.parts[-1]
        ):
            return True

    return False


def files_directories():
    rules = [
        ValidationRule(
            rule_nr="3.02.01.02",
            test_type="both_path_hardcoded",
            text="De map naam MOET in lowercase, en mag geen spaties bevatten.",
            hardcoded_test=nta_3_02_01_02,
            note="This test generates a false positive if the absolute path contains an uppercase char or space. Consider feeding relative path, although that is not available for parser at present",
        ),
        ValidationRule(
            rule_nr="3.02.01.03",
            test_type="both_path_hardcoded",
            text="De naam van een map MOET minder dan 15 karakters zijn.",
            hardcoded_test=nta_3_02_01_03,
            note="This test generates a false positive if the absolute path contains a long folder",
        ),
        ValidationRule(
            rule_nr="3.02.01.04",
            test_type="both_path_hardcoded",
            text="Mappen die deel uitmaken van de NT MOETEN in de structuur van tabel F passen",
            hardcoded_test=nta_3_02_01_04,
            note="Currently does not check for date/version indicator or domain",
        ),
        ValidationRule(
            rule_nr="3.02.01.05",
            test_type="both_path_hardcoded",
            text="Bestandsnamen MOETEN in lowercase",
            hardcoded_test=nta_3_02_01_05,
        ),
        ValidationRule(
            rule_nr="3.02.01.09",
            test_type="linkbase_hardcoded_url_etree",
            text="Linkbase inhoud, zoals gedefinieerd door XBRL 2.1 specificatie (label, reference, presentation, definition, calculation)en modules (formula en tables), MOET tot uitdrukking gebracht worden in de bestandsnaam conform tabel A",
            hardcoded_test=nta_3_02_01_09,
        ),
        ValidationRule(
            rule_nr="3.02.01.10",
            test_type="linkbase_hardcoded_url_etree",
            text="Linkbase inhoud, zoals gedefinieerd door de Generic Link 1.0 specificatie, MOET tot uitdrukking gebracht worden in de bestandsnaam conform tabel B",
            hardcoded_test=nta_3_02_01_10,
        ),
    ]
    return rules
