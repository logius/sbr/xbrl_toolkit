import re

import logging

from ..classes import ValidationRule

logger = logging.getLogger("validator.naming_conventions")


def nta_3_02_05_02(elem):
    if not re.match("^(([A-Z]|[0-9])([a-z]|[0-9]|[0-9]_)*)*$", elem.name):
        return True
    return False


def nta_3_02_05_04(elem):
    if not re.match("^([A-Z]|[0-9]|[a-z]|-|_)*$", elem.name):
        return True
    return False


def nta_3_02_05_21(elem):
    if len(elem.name) > 200:
        return True
    return False


def element_name():
    rules = [
        ValidationRule(
            rule_nr="3.02.05.02",
            test_type="model_element",
            text="Conceptnamen MOETEN upper camelcase gebruiken",
            hardcoded_test=nta_3_02_05_02,
        ),
        ValidationRule(
            rule_nr="3.02.05.04",
            test_type="model_element",
            text="Een conceptnaam MOET alleen de tekens a-zA-Z0-9_- gebruiken",
            hardcoded_test=nta_3_02_05_04,
        ),
        ValidationRule(
            rule_nr="3.02.05.21",
            test_type="model_element",
            text="Een conceptnaam MOET niet langer zijn dan 200 tekens",
            hardcoded_test=nta_3_02_05_21,
        ),
    ]
    return rules
