import re

import logging

from ..classes import ValidationRule

logger = logging.getLogger("validator.naming_conventions")


def nta_3_02_03_02(path):
    """
    Checks if the correct path is used per file type.

    Dictionary
        *-lab-??.xml
        *-codes.xsd
        *-axes.xsd
        *-linkroles-*.xsd

    Validation
        *-def.xml*


    :param path:
    :return:
    """

    dictionary_regexes = [
        "[a-zA-Z0-9_-]*-lab-[a-z]{2}.xml",
        "[a-zA-Z0-9_-]*-ref.xml",
        "[a-zA-Z0-9_-]*-def.xml",
        "[a-zA-Z0-9_-]*-codes.xsd",
        "[a-zA-Z0-9_-]*-axes.xsd",
        "[a-zA-Z0-9_-]*-data.xsd",
        "[a-zA-Z0-9_-]*-types.xsd",
        "[a-zA-Z0-9_-]*-tuples.xsd",
        "[a-zA-Z0-9_-]*-domains.xsd",
        "[a-zA-Z0-9_-]*-linkroles[a-zA-Z0-9_-]*.xsd",
    ]
    presentation_regexes = [
        "[a-zA-Z0-9_-]*-abstracts[a-zA-Z0-9_-]*.xsd",
        "[a-zA-Z0-9_-]*-tab.xml",
        "[a-zA-Z0-9_-]*-pre.xml",
        "[a-zA-Z0-9_-]*-def.xml",
        "[a-zA-Z0-9_-]*-ref.xml",
        "[a-zA-Z0-9_-]*tab-generic-lab-[a-z]{2}.xml",
        "[a-zA-Z0-9_-]*tab-generic-[a-zA-Z0-9_-]*-lab-[a-z]{2}.xml",
        "[a-zA-Z0-9_-]*tab-generic-msg-[a-z]{2}.xml",
        "[a-zA-Z0-9_-]*-presentation-hierarchy[a-zA-Z0-9_-]*.xml",
        "[a-zA-Z0-9_-]*-generic-parameter.xml",
        "[a-zA-Z0-9_-]*-generic-lab-nl.xml",
        "[a-zA-Z0-9_-]*-lab-[a-z]{2}.xml",
        "[a-zA-Z0-9_-]*-generic-linkrole-order[a-zA-Z0-9_-]*.xml",
    ]
    validation_regexes = [
        "[a-zA-Z0-9_-]*-def.xml",
        "[a-zA-Z0-9_-]*-for-[a-zA-Z0-9_-]*-[a-z]{2}.xml",
        "[a-zA-Z0-9_-]*-for.xml",
        "[a-zA-Z0-9_-]*-parentchildsummation.xml",
    ]

    if path.parts[-2] == "presentation":
        for presentation_regex_regex in presentation_regexes:
            if re.match(presentation_regex_regex, path.name):
                return False

    elif path.parts[-2] == "validation":
        for validation_regex in validation_regexes:
            if re.match(validation_regex, path.name):
                return False

    elif path.parts[-2] == "dictionary":
        for dictionary_regex in dictionary_regexes:
            if re.match(dictionary_regex, path.name):
                return False

    elif path.parts[-2] == "entrypoints":
        for regex_list in [
            dictionary_regexes,
            presentation_regexes,
            validation_regexes,
        ]:
            for regex in regex_list:
                if re.match(regex, path.name):
                    return True

        return False  # EP's should not match any of the regexes

    else:
        logger.error(
            f"During check of NTA rule 3.02.03.02, Unknown directory: {path.parts[-2]}"
        )
    return True


def nta_3_02_03_04(schema):
    targetnamespace = schema.xpath(
        "/xs:schema[@targetNamespace]", namespaces=schema.nsmap
    )
    if targetnamespace:
        ns_text = targetnamespace[0].get("targetNamespace")
        if not re.match("^[a-z0-9_\-\/.:]*$", ns_text):
            return True
    return False


def nta_3_02_03_06(schema):
    targetnamespace = schema.xpath(
        "/xs:schema[@targetNamespace]", namespaces=schema.nsmap
    )
    if targetnamespace:
        ns_text = targetnamespace[0].get("targetNamespace")
        if not re.match(
            "^http:\/\/www\.nltaxonomie\.nl\/nt[0-9]{2}\/[a-z]{2,}\/[0-9]{8}.{0,2}\/[a-z]{5,}\/",
            ns_text,
        ):
            return True

    return False


def namespace_uri():
    rules = [
        ValidationRule(
            rule_nr="3.02.03.02",
            test_type="both_path_hardcoded",
            text="xs:schema/@targetNamespace MOET zijn componenten door middel van een slash (/) scheiden",
            hardcoded_test=nta_3_02_03_02,
            note="This NTA rule is mislabled in other tools, and instead checks if files are placed in the correct "
            "directory. Our tooling now asserts the same",
        ),
        ValidationRule(
            rule_nr="3.02.03.03",
            test_type="schema_xpath",
            text="xs:schema/@targetNamespace MOET NIET meer dan 255 tekens zijn",
            xpath="//xs:schema[@targetNamespace and string-length(@targetNamespace) > 255]",
        ),
        ValidationRule(
            rule_nr="3.02.03.04",
            test_type="schema_hardcoded",
            text="xs:schema/@targetNamespace MOET alleen a-z0-9_-/.: tekens gebruiken",
            hardcoded_test=nta_3_02_03_04,
        ),
        ValidationRule(
            rule_nr="3.02.03.05",
            test_type="schema_xpath",
            text="xs:schema/@targetNamespace MOET met 'http://www.nltaxonomie.nl' beginnen",
            xpath="//xs:schema[@targetNamespace and not(starts-with(@targetNamespace, 'http://www.nltaxonomie.nl'))]",
        ),
        ValidationRule(
            rule_nr="3.02.03.06",
            test_type="schema_hardcoded",
            text="	xs:schema/@targetNamespace MOET de volgende componenten bevatten: http://www.nltaxonomie.nl / {ntversie} / {domein prefix} / {publicatiedatum}{optioneel: versie} / {mapnaam}",
            hardcoded_test=nta_3_02_03_06,
        ),
    ]
    return rules
