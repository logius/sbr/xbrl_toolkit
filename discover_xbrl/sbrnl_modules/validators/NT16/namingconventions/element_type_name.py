import re

import logging

from ..classes import ValidationRule

logger = logging.getLogger("validator.naming_conventions")


def nta_3_02_08_02(datatype):
    if not re.match("^[A-Za-z0-9_-]*$", datatype.name):
        return True
    return False


def nta_3_02_08_06(datatype):
    # Don't check external namespaces
    if datatype.domain in ["dtr-types"]:
        return False

    if "ItemType" in datatype.name:
        return False
    if datatype.name == "string255":
        # EK: This is an exception and is only used for TypedMembers
        return False

    elif hasattr(datatype, "enumerations") and len(datatype.enumerations) > 0:
        # EK: enumerations are also an exception
        return False
    return True


def nta_3_02_08_09(elem):

    # Don't check external namespaces
    if elem.domain in ["dtr-types"]:
        return False

    if not re.match("^[a-z][a-zA-Z0-9_]+$", elem.name):
        return True
    return False


def element_type_name():
    rules = [
        ValidationRule(
            rule_nr="3.02.08.02",
            test_type="model_datatype",
            text="Een datatype naam MAG alleen de tekens a-zA-Z0-9_- gebruiken",
            hardcoded_test=nta_3_02_08_02,
        ),
        ValidationRule(
            rule_nr="3.02.08.06",
            test_type="model_datatype",
            text="De naam van een datatype voor generieke inzet, MOET het achtervoegsel 'ItemType' krijgen",
            hardcoded_test=nta_3_02_08_06,
        ),
        ValidationRule(
            rule_nr="3.02.08.09",
            test_type="model_datatype",
            text="Typenamen MOETEN lower camelcase gebruiken en MOGEN nummers en underscore gebruiken",
            hardcoded_test=nta_3_02_08_09,
        ),
    ]
    return rules


xml_simple_types = [
    "anyURI",
    "base64Binary",
    "boolean",
    "byte",
    "date",
    "dateTime",
    "decimal",
    "derivationControl",
    "double",
    "duration",
    "float",
    "gDay",
    "gMonth",
    "gMonthDay",
    "gYear",
    "gYearMonth",
    "hexBinary",
    "int",
    "integer",
    "language",
    "long",
    "Name",
    "negativeInteger",
    "nonNegativeInteger",
    "nonPositiveInteger",
    "normalizedString",
    "positiveInteger",
    "QName",
    "short",
    "simpleDerivationSet",
    "string",
    "time",
    "token",
    "unsignedByte",
    "unsignedInt",
    "unsignedLong",
    "unsignedShort",
]
