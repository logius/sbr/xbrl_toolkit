import re

import logging

from ..classes import ValidationRule
from discover_xbrl.sbrnl_modules.validators.NT16.parameters import (
    get_all_allowed_namespaces,
)

logger = logging.getLogger("validator.naming_conventions")


def nta_3_02_04_01(schema):
    targetnamespace = schema.get("targetNamespace")
    for k, v in schema.nsmap.items():
        if v == targetnamespace:
            return False

    return True


def nta_3_02_04_05(schema):
    allowed_namespaces = get_all_allowed_namespaces()
    for k, v in schema.nsmap.items():
        if k not in allowed_namespaces.keys():
            domain = k.split("-")
            if domain[0] not in nt_domains:
                return True
    return False


def nta_3_02_04_06(schema):
    for k, v in schema.nsmap.items():
        if len(k) > 20:
            return True
    return False


def namespace_prefix():
    rules = [
        ValidationRule(
            rule_nr="3.02.04.01",
            test_type="schema_hardcoded",
            text="xs:schema/@targetNamespace MOET een prefix hebben",
            hardcoded_test=nta_3_02_04_01,
        ),
        ValidationRule(
            rule_nr="3.02.04.05",
            test_type="both_hardcoded",
            text="Prefixes MOETEN alleen a-z0-9- tekens bevatten",
            hardcoded_test=nta_3_02_04_05,
        ),
        ValidationRule(
            rule_nr="3.02.04.06",
            test_type="both_hardcoded",
            text="Namespace prefixes BEHOREN niet langer te zijn dan 20 tekens",
            hardcoded_test=nta_3_02_04_06,
            severity="WARNING",
        ),
    ]
    return rules


nt_domains = [
    "abm",
    "bd",
    "bzk",
    "cbs",
    "ez",
    "ezk",
    "iasb",
    "kvk",
    "nl",
    "rj",
    "sbr",
    "venj",
    "jenv",
    "vwa",
    "ocw",
    "lnv",
    "lei",
]
