import re
import logging

from ..classes import ValidationRule
from .namespace_prefix import nt_domains

logger = logging.getLogger("validator.naming_conventions")
ignore_list = ["http://www.xbrl.org", "www.nltaxonomie.nl"]


def nta_3_02_09_02(linkrole):
    for url in ignore_list:
        if url in linkrole.roleURI:
            return False
    if not linkrole.roleURI.lower() == linkrole.roleURI:
        return True
    return False


def nta_3_02_09_03(linkrole):
    for url in ignore_list:
        if url in linkrole.roleURI:
            return False
    if not re.match("^[\:A-Za-z0-9_-]*$", linkrole.roleURI):
        return True
    return False


def nta_3_02_09_04(linkrole):
    for url in ignore_list:
        if url in linkrole.roleURI:
            return False
    if len(linkrole.roleURI) > 255:
        return True
    return False


def nta_3_02_09_09(linkrole):
    for url in ignore_list:
        if url in linkrole.roleURI:
            return False
    parts = linkrole.roleURI.split(":")
    if len(parts) < 4:
        return True
    if parts[0] != "urn":
        return True
    if parts[2] != "linkrole":
        return True
    if parts[1] not in nt_domains:
        return True
    return False


def nta_3_02_10_02(linkrole):
    if linkrole.id is None:
        return False
    for url in ignore_list:
        if url in linkrole.id:
            return False
    if len(linkrole.id) > 255:
        return True
    return False


def element_linkrole():
    rules = [
        ValidationRule(
            rule_nr="3.02.09.02",
            test_type="model_linkrole",
            text="Een linkrole URI MOET in lowercase.",
            hardcoded_test=nta_3_02_09_02,
        ),
        ValidationRule(
            rule_nr="3.02.09.03",
            test_type="model_linkrole",
            text="Een linkrole URI MAG alleen de tekens a-z0-9_-: gebruiken",
            hardcoded_test=nta_3_02_09_03,
        ),
        ValidationRule(
            rule_nr="3.02.09.04",
            test_type="model_linkrole",
            text="Een linkrole URI MOET NIET meer dan 255 tekens zijn",
            hardcoded_test=nta_3_02_09_04,
        ),
        ValidationRule(
            rule_nr="3.02.09.09",
            test_type="model_linkrole",
            text="Een NT linkrole URI MOET de volgende structuur volgen: urn:{NT partner prefix}:linkrole:{functionele naam}",
            hardcoded_test=nta_3_02_09_09,
        ),
        ValidationRule(
            rule_nr="3.02.10.02",
            test_type="model_linkrole",
            text="Een linkrole @id MOET NIET meer dan 255 tekens zijn",
            hardcoded_test=nta_3_02_10_02,
        ),
    ]
    return rules
