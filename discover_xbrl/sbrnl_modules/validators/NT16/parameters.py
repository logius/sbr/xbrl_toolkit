allowed_namespaces_xbrl = {
    "acf": "http://xbrl.org/2010/filter/aspect-cover",
    "bf": "http://xbrl.org/2008/filter/boolean",
    "cf": "http://xbrl.org/2008/filter/concept",
    "crf": "http://xbrl.org/2010/filter/concept-relation",
    "df": "http://xbrl.org/2008/filter/dimension",
    "dtr": "http://www.xbrl.org/2009/dtr",
    "dtr-types": "http://www.xbrl.org/dtr/type/2020-01-21",
    "ea": "http://xbrl.org/2008/assertion/existence",
    "formula": "http://xbrl.org/2008/formula",
    "ifrs-full": "https://xbrl.ifrs.org/taxonomy/2022-03-24/ifrs-full",
    "ifrs-smes": "https://xbrl.ifrs.org/taxonomy/2022-03-24/ifrs-smes",
    "ifrs-mc": "http://xbrl.ifrs.org/taxonomy/2020-03-16/ifrs-mc",
    "gen": "http://xbrl.org/2008/generic",
    "gpl": "http://xbrl.org/2013/preferred-label",
    "label": "http://xbrl.org/2008/label",
    "link": "http://www.xbrl.org/2003/linkbase",
    "msg": "http://xbrl.org/2010/message",
    "negated": "http://www.xbrl.org/2009/role/negated",
    "num": "http://www.xbrl.org/dtr/type/numeric",
    "pf": "http://xbrl.org/2008/filter/period",
    "ref": "http://www.xbrl.org/2006/ref",
    "reference": "http://xbrl.org/2008/reference",
    "sev": "http://xbrl.org/2016/assertion-severity",
    "table": "http://xbrl.org/2014/table",
    "tf": "http://xbrl.org/2008/filter/tuple",
    "va": "http://xbrl.org/2008/assertion/value",
    "variable": "http://xbrl.org/2008/variable",
    "validation": "http://xbrl.org/2008/validation",
    "valm": "http://xbrl.org/2010/message/validation",
    "xbrldt": "http://xbrl.org/2005/xbrldt",
    "xbrldi": "http://xbrl.org/2006/xbrldi",
    "xbrli": "http://www.xbrl.org/2003/instance",
    "xff": "http://www.xbrl.org/2010/function/formula",  # Deprecated? https://specifications.xbrl.org/release-history-formula-1.0-xf.html
    "xfi": "http://www.xbrl.org/2008/function/instance",  # Deprecated? https://specifications.xbrl.org/release-history-formula-1.0-xf.html
    "xl": "http://www.xbrl.org/2003/XLink",
}

allowed_namespaces_xml = {
    "xlink": "http://www.w3.org/1999/xlink",
    "xs": "http://www.w3.org/2001/XMLSchema",
    "xsd": "http://www.w3.org/2001/XMLSchema",  # 'xsd' gets used in W3c files.
    "xsi": "http://www.w3.org/2001/XMLSchema-instance",
}

allowed_namespaces_sbr = {
    "iso3166-enum-sbr": "http://www.nltaxonomie.nl/nt17/sbr/20220301/dictionary/iso3166-countrycodes-2022-03-03",
    "iso4217-enum-sbr": "http://www.nltaxonomie.nl/nt17/sbr/20220301/dictionary/iso4217-currencycodes-2018-08-02",
    "sbi": "http://www.nltaxonomie.nl/nt17/sbr/20200301/dictionary/sbi-businesscodes-2018",
    "sbr": "http://www.nltaxonomie.nl/2011/xbrl/xbrl-syntax-extension",
    "sbr-dim": "http://www.nltaxonomie.nl/2013/xbrl/sbr-dimensional-concepts",
    "nl-cd": "http://www.nltaxonomie.nl/nt18/sbr/20230301/dictionary/nl-common-data",
    "nl-codes": "http://www.nltaxonomie.nl/nt18/sbr/20230301/dictionary/nl-codes",
    "nl-types": "http://www.nltaxonomie.nl/nt18/sbr/20230301/dictionary/nl-types",
}


def get_all_allowed_namespaces():
    namespaces = {}
    for namespace in [
        allowed_namespaces_xml,
        allowed_namespaces_sbr,
        allowed_namespaces_xbrl,
    ]:
        namespaces.update(namespace)
    return namespaces
