import logging
import lxml

from .parameters import (
    allowed_namespaces_sbr,
    allowed_namespaces_xbrl,
    allowed_namespaces_xml,
)
from discover_xbrl.sbrnl_modules.validators.NT16.validation_report import Report

from .generic.generic import generic_base

from .linkbase.arcroles import linkbase_arcroles
from .linkbase.base import linkbase_base
from .linkbase.dimensions import linkbase_dimensions
from .linkbase.elements import linkbase_elements
from .linkbase.formulas import linkbase_formulas
from .linkbase.hypercube import linkbase_hypercube
from .linkbase.labels import linkbase_labels
from .linkbase.presentation import linkbase_presentation
from .linkbase.references import linkbase_references
from .linkbase.other import linkbase_other

from .schema.arcroles import schema_arcroles
from .schema.base import schema_base
from .schema.concepts import schema_concepts
from .schema.context import schema_context
from .schema.dimensions import schema_dimensions
from .schema.entrypoints import schema_entrypoints
from .schema.link_parts import schema_link_parts
from .schema.linkroles import schema_linkroles
from .schema.other import schema_other
from .schema.type_enumerations import schema_type_enums
from .schema.xlink import schema_xlink

from .namingconventions.files_directories import files_directories
from .namingconventions.namespace_uri import namespace_uri
from .namingconventions.namespace_prefix import namespace_prefix
from .namingconventions.element_name import element_name
from .namingconventions.element_type_name import element_type_name
from .namingconventions.element_xlink_label import element_xlink_label
from discover_xbrl.sbrnl_modules.validators.NT16.namingconventions.element_enumeration import (
    element_enumeration,
)
from discover_xbrl.sbrnl_modules.validators.NT16.namingconventions.element_linkrole import (
    element_linkrole,
)

logger = logging.getLogger("validator")
logger_exceptions = logging.getLogger("validator.exceptions")
logger_linkbase = logging.getLogger("validator.linkbase")
logger_schema = logging.getLogger("validator.schema")
logger_naming_conventions = logging.getLogger("validator.naming_conventions")


ignore_list = ["http://www.xbrl.org", "http://www.nltaxonomie.nl"]


def url_in_ignore_list(url):
    for ignored_url in ignore_list:
        if url.__str__().startswith(ignored_url):
            return True
    return False


class Validator:
    def __init__(self):
        """
        This class is initialized once after which each validation rule is routed through.

        :return:
        """
        self.linkbase_rules = []
        self.schema_rules = []
        self.model_rules = []
        self.report = Report("Validation report NTA-Rules", "NT16")
        self.namespaces = self.get_namespaces()
        self.get_rules()

    def get_namespaces(self):
        namespaces = {}
        namespaces.update(allowed_namespaces_xml)
        namespaces.update(allowed_namespaces_xbrl)
        namespaces.update(allowed_namespaces_sbr)
        return namespaces

    def get_rules(self):
        """
        Gathers all rules and adds them to the correct attributes, depending on when these need to run.

        :return:
        """
        rule_lists = [
            generic_base(),  # General rules    - 2.01.00.x
            schema_base(),  # XML Schema - Base         - 2.02.00.x
            schema_xlink(),  # XML Schema - XLink        - 2.02.01.x
            schema_concepts(),  # XML Schema - Concepts     - 2.02.02.x
            schema_linkroles(),  # XML Schema - Linkroles    - 2.02.03.x
            schema_arcroles(),  # XML Schema - Arcroles     - 2.03.04.x
            schema_link_parts(),  # XML Schema - Link-parts   - 2.02.05.x
            schema_context(),  # XML Schema - Context      - 2.02.06.x
            schema_type_enums(),  # XML Schema - type&enums   - 2.02.07.x
            schema_dimensions(),  # XML Schema - Dimensions   - 2.02.08.x
            schema_entrypoints(),  # XML Schela - Entrpoints  - 2.02.10.x
            schema_other(),  # XML Schema - other        - 2.02.11.x
            # Linkbase validations
            linkbase_base(),  # XML Linkbase - Basis          - 2.03.00.x
            linkbase_elements(),  # XML Linkbase - Elements       - 2.03.01.x
            linkbase_arcroles(),  # XML Linkbase - Arcroles       - 2.03.02.x
            linkbase_references(),  # XML Linkbase - References     - 2.03.03.x
            linkbase_presentation(),  # XML Linkbase - Presentation   - 2.03.04.x
            linkbase_hypercube(),  # XML Linkbase - Hypercube      - 2.03.05.x
            linkbase_dimensions(),  # XML Linkbase - Dimensions     - 2.03.06.x
            linkbase_labels(),  # XML Linkbase - Labels         - 2.03.08.x
            linkbase_formulas(),  # XML Linkbase - Formulas         - 2.03.09.x
            linkbase_other(),  # XML Linkbase - Other - 2.03.10.x
            files_directories(),  # Files and Directories     3.02.01.x
            namespace_uri(),  # Namespace URI             3.02.03.x
            namespace_prefix(),  # Namespace Prefix          3.02.04.x
            # DTS/model checks        Element name              3.02.05.x
            # N/A                     Element id                3.02.06.x
            # Not testable            Element id                3.02.07.x
            element_name(),  # Element name              3.02.05.x
            element_type_name(),  # Element type name         3.02.08.x
            element_linkrole(),  # Linkrole names and attributes 3.02.09.x en 3.02.10.x
            element_xlink_label(),  # xlink:label conventies 3.02.11.x
            element_enumeration(),  # enumerations 3.02.13.x
        ]

        for rule_list in rule_lists:
            for rule in rule_list:
                self.report.add_rule(rule=rule)
                if rule.input_type == "linkbase":
                    self.linkbase_rules.append(rule)
                    logger.info(f"Loaded NTA linkbase rule: {rule.rule_nr}")
                elif rule.input_type == "schema":
                    self.schema_rules.append(rule)
                    logger.info(f"Loaded NTA schema rule: {rule.rule_nr}")
                elif rule.input_type == "both":
                    self.schema_rules.append(rule)
                    self.linkbase_rules.append(rule)
                    logger.info(f"Loaded NTA schema rule: {rule.rule_nr}")
                elif rule.input_type == "model":
                    self.model_rules.append(rule)
                    logger.info(f"Loaded NTA model rule: {rule.rule_nr}")
                elif rule.input_type == "note":
                    pass  # placeholder rule.
                else:
                    logger.warning(f"could not load rule {rule.rule_nr}")

    def validate_linkbase(self, url, linkbase, file):
        """
        Presently gets called before/while parsing each linkbase file.

        :return:
        """

        # Use the built-in list of allowed namespaces. Validity of namespaces in DTS should be test with a model test
        namespaces = self.namespaces

        if not url_in_ignore_list(url):

            for rule in self.linkbase_rules:

                if rule.test_type == "linkbase_xpath":
                    # Test is based upon a xpath expression.

                    try:
                        linkbase.xpath(rule.xpath, namespaces=namespaces)
                    except lxml.etree.XPathEvalError as e:
                        if e.args[0] == "Undefined namespace prefix":
                            logger_exceptions.debug(
                                f"rule:{rule.rule_nr} is not evaluated for file: {url.name}, predicate/prefix not available"
                            )
                        else:
                            self.report.add_rule_violation(
                                rule=rule, message=f" on rule {rule.rule_nr}"
                            )
                            if hasattr(rule, "severity") and rule.severity == "WARNING":
                                logger_exceptions.warning(
                                    f"{rule.rule_nr}: {rule.text} in '{filename}'"
                                )
                            else:
                                logger_exceptions.error(
                                    f"XPath evaluation error in {url.name} on rule {rule.rule_nr}"
                                )
                    else:
                        if linkbase.xpath(rule.xpath, namespaces=namespaces):
                            filename = f"{url.parts[-2]}/{url.name}"
                            self.report.add_rule_violation(
                                rule=rule, message=f"in '{filename}'"
                            )
                            if hasattr(rule, "severity") and rule.severity == "WARNING":
                                logger_linkbase.warning(
                                    f"{rule.rule_nr}: {rule.text} in '{filename}'"
                                )
                            else:
                                logger_linkbase.error(
                                    f"{rule.rule_nr}: {rule.text} in '{filename}'"
                                )
                elif rule.test_type in ["linkbase_hardcoded_subset"]:
                    nodes = linkbase.xpath(rule.subset_xpath, namespaces=linkbase.nsmap)
                    for node in nodes:
                        if rule.hardcoded_test(node):
                            offender = node.get(rule.offender)
                            filename = f"{url.parts[-2]}/{url.name}"
                            self.report.add_rule_violation(
                                rule=rule, message=f"{offender} in '{filename}'"
                            )
                            if hasattr(rule, "severity") and rule.severity == "WARNING":
                                logger_naming_conventions.warning(
                                    f"{rule.rule_nr}: {rule.text} {offender} in '{filename}'"
                                )
                            else:
                                logger_naming_conventions.error(
                                    f"{rule.rule_nr}: {rule.text} {offender} in '{filename}'"
                                )

                elif rule.test_type in ["linkbase_hardcoded", "both_hardcoded"]:
                    # Hardcoded schema tests based on etree

                    if rule.hardcoded_test(linkbase):
                        filename = f"{url.parts[-2]}/{url.name}"
                        self.report.add_rule_violation(
                            rule=rule, message=f"in '{filename}'"
                        )
                        if hasattr(rule, "severity") and rule.severity == "WARNING":
                            logger_naming_conventions.warning(
                                f"{rule.rule_nr}: {rule.text} in '{filename}'"
                            )
                        else:
                            logger_naming_conventions.error(
                                f"{rule.rule_nr}: {rule.text} in '{filename}'"
                            )

                elif rule.test_type == "linkbase_tag":
                    # Test is based upon a tag comparison
                    if not linkbase.tag == rule.tag:
                        filename = f"{url.parts[-2]}/{url.name}"
                        self.report.add_rule_violation(
                            rule=rule, message=f"in '{filename}'"
                        )
                        if hasattr(rule, "severity") and rule.severity == "WARNING":
                            logger_linkbase.warning(
                                f"{rule.rule_nr}: {rule.text} in '{filename}'"
                            )
                        else:
                            logger_linkbase.error(
                                f"{rule.rule_nr}: {rule.text} in '{filename}'"
                            )

                elif rule.test_type == "linkbase_file_hardcoded":
                    # Test based upon the raw string data of a file
                    if rule.hardcoded_test(file):
                        filename = f"{url.parts[-2]}/{url.name}"
                        self.report.add_rule_violation(
                            rule=rule, message=f"in '{filename}'"
                        )
                        if hasattr(rule, "severity") and rule.severity == "WARNING":
                            logger_linkbase.warning(
                                f"{rule.rule_nr}: {rule.text} in '{filename}'"
                            )
                        else:
                            logger_linkbase.error(
                                f"{rule.rule_nr}: {rule.text} in '{filename}'"
                            )

                elif rule.test_type in [
                    "linkbase_path_hardcoded",
                    "both_path_hardcoded",
                ]:
                    # Hardcoded tests based on path variable
                    if rule.hardcoded_test(url):
                        filename = f"{url.parts[-2]}/{url.name}"
                        self.report.add_rule_violation(rule=rule, message=filename)
                        if hasattr(rule, "severity") and rule.severity == "WARNING":
                            logger_naming_conventions.warning(
                                f"{rule.rule_nr}: {rule.text} in '{filename}'"
                            )
                        else:
                            logger_naming_conventions.error(
                                f"{rule.rule_nr}: {rule.text} in '{filename}'"
                            )

                elif rule.test_type == "linkbase_hardcoded_url_etree":

                    # Hardcoded linkbase tests based on url and etree
                    if rule.hardcoded_test(url, linkbase):
                        filename = f"{url.parts[-2]}/{url.name}"
                        self.report.add_rule_violation(
                            rule=rule, message=f"in '{filename}'"
                        )
                        if hasattr(rule, "severity") and rule.severity == "WARNING":
                            logger_naming_conventions.warning(
                                f"{rule.rule_nr}: {rule.text} in '{filename}'"
                            )
                        else:
                            logger_naming_conventions.error(
                                f"{rule.rule_nr}: {rule.text} in '{filename}'"
                            )

    def validate_schema(self, url, schema, file):

        # Use the built-in list of allowed namespaces. Validity of namespaces in DTS should be test with a model test
        namespaces = self.namespaces

        if not url_in_ignore_list(url):

            for rule in self.schema_rules:

                if rule.test_type == "schema_xpath":
                    if schema.xpath(rule.xpath, namespaces=namespaces):
                        filename = f"{url.parts[-2]}/{url.name}"
                        self.report.add_rule_violation(
                            rule=rule, message=f"in '{filename}'"
                        )
                        if hasattr(rule, "severity") and rule.severity == "WARNING":
                            logger_schema.warning(
                                f"{rule.rule_nr}: {rule.text} in '{filename}'"
                            )
                        else:
                            logger_schema.error(
                                f"{rule.rule_nr}: {rule.text} in '{filename}'"
                            )

                elif rule.test_type in ["schema_path_hardcoded", "both_path_hardcoded"]:
                    # Hardcoded tests based on path variable
                    if rule.hardcoded_test(url):
                        filename = f"{url.parts[-2]}/{url.name}"
                        self.report.add_rule_violation(
                            rule=rule, message=f"in '{filename}'"
                        )
                        if hasattr(rule, "severity") and rule.severity == "WARNING":
                            logger_schema.warning(
                                f"{rule.rule_nr}: {rule.text} in '{filename}'"
                            )
                        else:
                            logger_schema.error(
                                f"{rule.rule_nr}: {rule.text} in '{filename}'"
                            )

                elif rule.test_type == "schema_tag":
                    # Test is based upon a tag comparison
                    if not schema.tag == rule.tag:
                        filename = f"{url.parts[-2]}/{url.name}"
                        self.report.add_rule_violation(
                            rule=rule, message=f"in '{filename}'"
                        )
                        if hasattr(rule, "severity") and rule.severity == "WARNING":
                            logger_schema.warning(
                                f"{rule.rule_nr}: {rule.text} in '{filename}'"
                            )
                        else:
                            logger_schema.error(
                                f"{rule.rule_nr}: {rule.text} in '{filename}'"
                            )

                elif rule.test_type == "schema_file_hardcoded":
                    # Test based upon the raw string data of a file
                    if rule.hardcoded_test(file):
                        filename = f"{url.parts[-2]}/{url.name}"
                        self.report.add_rule_violation(
                            rule=rule, message=f"in '{filename}'"
                        )
                        if hasattr(rule, "severity") and rule.severity == "WARNING":
                            logger_schema.warning(
                                f"{rule.rule_nr}: {rule.text} in '{filename}'"
                            )
                        else:
                            logger_schema.error(
                                f"{rule.rule_nr}: {rule.text} in '{filename}'"
                            )

                elif rule.test_type in ["schema_hardcoded", "both_hardcoded"]:
                    # Hardcoded schema tests based on etree
                    if rule.hardcoded_test(schema):
                        filename = f"{url.parts[-2]}/{url.name}"
                        self.report.add_rule_violation(
                            rule=rule, message=f"in '{filename}'"
                        )
                        if hasattr(rule, "severity") and rule.severity == "WARNING":
                            logger_schema.warning(
                                f"{rule.rule_nr}: {rule.text} in '{filename}'"
                            )
                        else:
                            logger_schema.error(
                                f"{rule.rule_nr}: {rule.text} in '{filename}'"
                            )

                elif rule.test_type == "schema_hardcoded_url_etree":
                    # Hardcoded linkbase tests based on url and etree

                    if rule.hardcoded_test(url, schema):
                        filename = f"{url.parts[-2]}/{url.name}"
                        self.report.add_rule_violation(
                            rule=rule, message=f"in '{filename}'"
                        )
                        if hasattr(rule, "severity") and rule.severity == "WARNING":
                            logger_schema.warning(
                                f"{rule.rule_nr}: {rule.text} in '{filename}'"
                            )
                        else:
                            logger_schema.error(
                                f"{rule.rule_nr}: {rule.text} in '{filename}'"
                            )

    def validate_dts(self, dts):

        for rule in self.model_rules:
            if rule.test_type == "model_element":

                for name, concept in dts.concepts.items():
                    if url_in_ignore_list(concept._source):
                        continue
                    if rule.hardcoded_test(concept):
                        filename = f"{concept._source.parts[-2]}/{concept._source.name}"
                        self.report.add_rule_violation(
                            rule=rule, message=f"for element '{name}' in {filename}"
                        )
                        if hasattr(rule, "severity") and rule.severity == "WARNING":
                            logger_schema.warning(
                                f"{rule.rule_nr}: {rule.text} for element '{name}' in {filename}"
                            )
                        else:
                            logger_schema.error(
                                f"{rule.rule_nr}: {rule.text} for element '{name}' in {filename}"
                            )

            elif rule.test_type == "model_datatype":
                for concept in dts.datatypes:
                    if url_in_ignore_list(concept._source):
                        continue
                    if rule.hardcoded_test(concept):
                        filename = f"{concept._source.parts[-2]}/{concept._source.name}"
                        self.report.add_rule_violation(
                            rule=rule,
                            message=f"for datatype {concept.name} in '{filename}' ",
                        )
                        if hasattr(rule, "severity") and rule.severity == "WARNING":
                            logger_schema.warning(
                                f"{rule.rule_nr}: {rule.text} for datatype {concept.name} in '{filename}'"
                            )
                        else:
                            logger_schema.error(
                                f"{rule.rule_nr}: {rule.text} for datatype {concept.name} in '{filename}'"
                            )

            elif rule.test_type == "model_linkrole":

                for name, linkrole in dts.roletypes.items():
                    if rule.hardcoded_test(linkrole):
                        self.report.add_rule_violation(
                            rule=rule,
                            message=f"for roletype {linkrole.roleURI} in EP '{dts.entrypoint_name}'",
                        )
                        if hasattr(rule, "severity") and rule.severity == "WARNING":
                            logger_schema.warning(
                                f"{rule.rule_nr}: {rule.text} for roletype {linkrole.roleURI} in EP '{dts.entrypoint_name}'"
                            )
                        else:
                            logger_schema.error(
                                f"{rule.rule_nr}: {rule.text} for roletype {linkrole.roleURI} in EP '{dts.entrypoint_name}'"
                            )

            elif rule.test_type == "model_referenced_linkrole":

                for linkrole in dts.directly_referenced_linkbases:

                    if rule.hardcoded_test(linkrole):
                        self.report.add_rule_violation(
                            rule=rule,
                            message=f"for roletype {linkrole.roleURI} in EP '{dts.entrypoint_name}'",
                        )
                        if hasattr(rule, "severity") and rule.severity == "WARNING":
                            logger_schema.warning(
                                f"{rule.rule_nr}: {rule.text} for roletype {linkrole.roleURI} in EP '{dts.entrypoint_name}'"
                            )
                        else:
                            logger_schema.error(
                                f"{rule.rule_nr}: {rule.text} for roletype {linkrole.roleURI} in EP '{dts.entrypoint_name}'"
                            )

            elif rule.test_type == "model_namespaces":

                for name, concept in dts.namespaces.items():
                    if not concept.startswith(
                        "http://www.nltaxonomie.nl/"
                    ) and rule.hardcoded_test(name):
                        self.report.add_rule_violation(
                            rule=rule,
                            message=f"for namespace {name} in EP '{dts.entrypoint_name}'",
                        )
                        if hasattr(rule, "severity") and rule.severity == "WARNING":
                            logger_schema.warning(
                                f"{rule.rule_nr}: {rule.text} for namespace {name} in EP '{dts.entrypoint_name}'"
                            )
                        else:
                            logger_schema.error(
                                f"{rule.rule_nr}: {rule.text} for namespace {name} in EP '{dts.entrypoint_name}'"
                            )

            elif rule.test_type == "model_hypercube":

                for hypercube in dts.hypercubes:
                    if rule.hardcoded_test(hypercube):
                        self.report.add_rule_violation(
                            rule=rule,
                            message=f"for hypercube {hypercube} in EP '{dts.entrypoint_name}'",
                        )
                        if hasattr(rule, "severity") and rule.severity == "WARNING":
                            logger_schema.warning(
                                f"{rule.rule_nr}: {rule.text} for hypercube {hypercube} in EP '{dts.entrypoint_name}'"
                            )
                        else:
                            logger_schema.error(
                                f"{rule.rule_nr}: {rule.text} for hypercube {hypercube} in EP '{dts.entrypoint_name}'"
                            )
            elif rule.test_type == "model_label":
                for label in dts.labels.values():
                    if rule.hardcoded_test(label):
                        self.report.add_rule_violation(
                            rule=rule,
                            message=f"for label {label.text} in EP '{dts.entrypoint_name}'",
                        )
                        if hasattr(rule, "severity") and rule.severity == "WARNING":
                            logger_schema.warning(
                                f"{rule.rule_nr}: {rule.text} for label {label.text} in EP '{dts.entrypoint_name}'"
                            )
                        else:
                            logger_schema.error(
                                f"{rule.rule_nr}: {rule.text} for label {label.text} in EP '{dts.entrypoint_name}'"
                            )
