import logging

from ..classes import ValidationRule

logger = logging.getLogger("validator.schema")


def technicalreference_schema():

    rules = [
        ValidationRule(
            rule_nr="4.01.001",
            test_type="schema_xpath",
            text="<xs:all> MOET NIET gebruikt worden",
            xpath="//xs:all",
        )
    ]
    return rules
