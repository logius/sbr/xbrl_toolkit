import logging

from ..classes import ValidationRule

logger = logging.getLogger("validator.schema")


"""
All NL 2.02.04.* validations
"""


def schema_arcroles():

    rules = [
        ValidationRule(
            rule_nr="2.02.04.01",
            test_type="schema_xpath",
            text="Arcroles MOETEN NIET aangemaakt worden",
            xpath="//link:arcroleType",
        )
    ]
    return rules
