import logging

from ..classes import ValidationRule

logger = logging.getLogger("validator.schema")


"""
All NL 2.02.02.* validations
"""


def nta_2_02_02_26(elem):
    for label in elem.labels.values():
        if label.language == "nl" and label.role in [
            "http://www.xbrl.org/2003/role/label",
            "http://www.xbrl.org/2008/role/label",
        ]:
            return False
    return True


def schema_concepts():

    rules = [
        ValidationRule(
            rule_nr="2.02.02.01",
            test_type="schema_xpath",
            text="Concept definities MOETEN op root level in een schema plaats vinden",
            xpath="//xs:element and //xs:schema//*/xs:element",
        ),
        ValidationRule(
            rule_nr="2.02.02.03",
            test_type="schema_xpath",
            text="Abstracte tuples MOETEN NIET voorkomen",
            xpath="//xs:element[@abstract='true']/xs:complexType",
        ),
        ValidationRule(
            rule_nr="2.02.02.05",
            test_type="schema_xpath",
            text="<xs:element> MOET NIET gebruikt worden om nieuwe abstract elementen te maken die als substitutionGroup voor andere elementen dienen UITGEZONDERD bij SBR-NT-beheer voor de NT",
            xpath="//xs:element[@abstract='true'] and (//xs:element[@abstract='true' and not (contains(@substitutionGroup,'sbr:') or contains(@substitutionGroup,'xbrli:') or contains(@substitutionGroup,'xbrldt:'))])",
        ),
        ValidationRule(
            rule_nr="2.02.02.08",
            test_type="schema_xpath",
            text="xs:schema/xs:element/@abstract is verplicht",
            xpath="//xs:element and not(//xs:schema/xs:element[@abstract])",
        ),
        ValidationRule(
            rule_nr="2.02.02.09",
            test_type="schema_xpath",
            text="//xs:element/@block is NIET toegestaan",
            xpath="//xs:element[@block]",
        ),
        ValidationRule(
            rule_nr="2.02.02.10",
            test_type="schema_xpath",
            text="//xs:element/@final is NIET toegestaan",
            xpath="//xs:element[@final]",
        ),
        ValidationRule(
            rule_nr="2.02.02.11",
            test_type="schema_xpath",
            text="//xs:element/@fixed is NIET toegestaan",
            xpath="//xs:element[@fixed]",
        ),
        ValidationRule(
            rule_nr="2.02.02.13",
            test_type="schema_xpath",
            text="//xs:element/@id is verplicht",
            xpath="//xs:element and //xs:element[not(@id)]",
        ),
        ValidationRule(
            rule_nr="2.02.02.15",
            test_type="schema_xpath",
            text="//xs:element/@nillable is verplicht",
            xpath="//xs:element and //xs:element[not(@nillable)]",
        ),
        ValidationRule(
            rule_nr="2.02.02.16",
            test_type="schema_xpath",
            text="xs:schema/xs:element/@nillable=’false’ MOET gebruikt worden als xs:schema/xs:element/@abstract=’true’",
            xpath="(//xs:schema/xs:element[@abstract='true' and not(@nillable='false')])",
        ),
        ValidationRule(
            rule_nr="2.02.02.17",
            test_type="schema_xpath",
            text="xs:schema/xs:element/@nillable=’false’ MOET gebruikt worden als xs:schema/xs:element/@substitutionGroup=’xbrli:tuple’ en zijn afgeleidden",
            xpath="//xs:schema/xs:element/@substitutionGroup='xbrli:tuple' and not(//xs:schema/xs:element[@nillable='false'])",
        ),
        ValidationRule(
            rule_nr="2.02.02.18",
            test_type="schema_xpath",
            text="@substitutionGroup MOET gebruikt worden op root <xs:element> die concepten zijn",
            xpath="/xs:schema/xs:element[@abstract='true' and not(@substitutionGroup)]",
            note="Is this just a more specified version of 2.02.02.17? Now probably leads to false positives.",
        ),
        ValidationRule(
            rule_nr="2.02.02.21",
            test_type="schema_xpath",
            text="xs:schema/xs:element/@type='xbrli:stringItemType' als xs:schema/xs:element/@abstract='true'",
            xpath="//xs:schema/xs:element[@abstract='true'] and not(//xs:schema/xs:element[@abstract='true' and @type='xbrli:stringItemType'])",
        ),
        ValidationRule(
            rule_nr="2.02.02.25",
            test_type="schema_xpath",
            text="xs:schema/xs:element/@xbrli:periodType MOET 'duration' zijn voor niet-rapporteerbare items. Voor rapporteerbare items BEHOORT het 'duration' te zijn, UITGEZONDERD rapporteerbare items die op een tijdstip gerapporteerd worden.",
            xpath="//xs:schema/xs:element[@abstract='true' and not(@xbrli:periodType='duration')]",
            note="changed _adv; this rule works, andontheplus side, www.nltaxonomie.nl is ignored, so no false positives on sbr elements"
            # hardcoded_test=nta_2_02_02_25,
        ),
        ValidationRule(
            rule_nr="2.02.02.26",
            test_type="model_element",
            text=" 	Een concept MOET een standaard label in de lokale taal hebben",
            hardcoded_test=nta_2_02_02_26,
        ),
        ValidationRule(
            rule_nr="2.02.02.35",
            test_type="schema_xpath",
            text="Een concept mag niet gebaseerd zijn op het type nl-types:imageItemType.",
            xpath="//xs:schema/xs:element[@type='nl-types:imageItemType']",
            note="changed @typeS to @type in thexpath-expression",
        ),
    ]
    return rules
