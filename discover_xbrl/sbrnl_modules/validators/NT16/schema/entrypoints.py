import logging

from ..classes import ValidationRule

logger = logging.getLogger("validator.schema")


"""
All NL 2.02.10.* validations
"""


def schema_entrypoints():

    rules = [
        ValidationRule(
            rule_nr="2.02.10.01",
            test_type="schema_xpath",
            text="Een entrypoint schema in een DTS MOET presentatie linkbase(s) omvatten",
            xpath="//xs:schema[contains(@targetNamespace, '/entrypoints/')] and count(/xs:schema/xs:annotation/xs:appinfo/link:linkbaseRef[contains(@xlink:href,'/presentation/')]) < 1",
        ),
        # ValidationRule(
        #     rule_nr="2.02.10.02",
        #     test_type="schema_xpath",
        #     text="Een entrypoint schema in een dimensionele DTS MOET minstens één presentatie linkbase(s) voor niet abstracte items bevatten.",
        #     xpath=""
        # ),
        # ValidationRule(
        #     rule_nr="2.02.10.03",
        #     test_type="schema_xpath",
        #     text="Een entrypoint schema in een dimensionele DTS MOET presentatie linkbase(s) voor dimensie, domein en domein leden bevatten",
        #     xpath=""
        # ),
    ]
    return rules
