import logging

from ..classes import ValidationRule

logger = logging.getLogger("validator.schema")


"""
All NL 2.02.06.* validations
Di zijn instance regels ?? de taxonomie kent geen context-elements
"""


def schema_context():

    rules = [
        ValidationRule(
            rule_nr="2.02.06.03",
            test_type="schema_xpath",
            text="Context elementen MOETEN NIET worden aangemaakt",
            xpath="//xs:element[@name='context']",
            note="new _adv",
        )
    ]
    return rules
