import logging

from ..classes import ValidationRule

logger = logging.getLogger("validator.schema")


"""
All NL 2.02.11.* validations
"""


def schema_other():

    rules = [
        ValidationRule(
            rule_nr="2.02.11.01",
            test_type="schema_xpath",
            text="<xs:all> MOET NIET gebruikt worden",
            xpath="//xs:all",
        ),
        ValidationRule(
            rule_nr="2.02.11.02",
            test_type="schema_xpath",
            text="<xs:annotation><xs:documentation> MOET NIET gebruikt worden",
            xpath="//xs:annotation/xs:documentation",
        ),
        ValidationRule(
            rule_nr="2.02.11.03",
            test_type="schema_xpath",
            text="<xs:any> MOET NIET gebruikt worden",
            xpath="//xs:any",
        ),
        ValidationRule(
            rule_nr="2.02.11.04",
            test_type="schema_xpath",
            text="<xs:anyAttribute> MOET NIET gebruikt worden",
            xpath="//xs:anyAttribute",
        ),
        ValidationRule(
            rule_nr="2.02.11.05",
            test_type="schema_xpath",
            text="<xs:appinfo> MOET NIET gebruikt worden voor andere content dan elementen uit de xlink of link namespaces",
            # hardcoded_test=nta_2_02_11_05,
            xpath="//xs:appinfo/*[not(namespace-uri()='http://www.xbrl.org/2003/linkbase' or namespace-uri()='http://www.w3.org/1999/xlink')]",
            note="False positive because gen link is not allowed according to the rule, but is used sometimes in linkrole/usedon",
        ),
        ValidationRule(
            rule_nr="2.02.11.06",
            test_type="schema_xpath",
            text="<xs:attribute> MOET NIET gebruikt worden",
            xpath="//xs:attribute",
        ),
        ValidationRule(
            rule_nr="2.02.11.07",
            test_type="schema_xpath",
            text="<xs:attributeGroup> MOET NIET gebruikt worden",
            xpath="//xs:attributeGroup",
        ),
        ValidationRule(
            rule_nr="2.02.11.08",
            test_type="schema_xpath",
            text="<xs:choice> MOET NIET gebruikt worden voor simpleType elementen",
            xpath="//xs:simpleType//xs:choice",
        ),
        ValidationRule(
            rule_nr="2.02.11.09",
            test_type="schema_xpath",
            text="<xs:choice> BEHOORT NIET gebruikt worden voor complexType elementen",
            xpath="//xs:complexType//xs:choice",
            severity="WARNING",
        ),
        ValidationRule(
            rule_nr="2.02.11.12",
            test_type="schema_xpath",
            text="<xs:extension> MOET NIET gebruikt worden",
            xpath="//xs:extension",
        ),
        ValidationRule(
            rule_nr="2.02.11.14",
            test_type="schema_xpath",
            text="<xs:group> MOET NIET gebruikt worden",
            xpath="//xs:group",
        ),
        ValidationRule(
            rule_nr="2.02.11.15",
            test_type="schema_xpath",
            text="<xs:key> MOET NIET gebruikt worden",
            xpath="//xs:key",
        ),
        ValidationRule(
            rule_nr="2.02.11.16",
            test_type="schema_xpath",
            text="<xs:keyref> MOET NIET gebruikt worden",
            xpath="//xs:keyref",
        ),
        ValidationRule(
            rule_nr="2.02.11.17",
            test_type="schema_xpath",
            text="<xs:list> MOET NIET gebruikt worden",
            xpath="//xs:list",
        ),
        ValidationRule(
            rule_nr="2.02.11.18",
            test_type="schema_xpath",
            text="<xs:notation> MOET NIET gebruikt worden",
            xpath="//xs:notation",
        ),
        ValidationRule(
            rule_nr="2.02.11.19",
            test_type="schema_xpath",
            text="<xs:pattern> BEHOORT NIET gebruikt te worden",
            xpath="//xs:pattern",
            severity="WARNING",
        ),
        ValidationRule(
            rule_nr="2.02.11.20",
            test_type="schema_xpath",
            text="<xs:redefine> MOET NIET gebruikt worden",
            xpath="//xs:redefine",
        ),
        ValidationRule(
            rule_nr="2.02.11.21",
            test_type="schema_xpath",
            text="<xs:restriction> MOET NIET gebruikt worden op xs:element, alleen op xs:simpleType",
            xpath="//xs:restriction/ancestor::xs:element",
            note="Dit klopt niet, de bijzin moet of simpleContent schrijven, of helemaal weg.",
        ),
        ValidationRule(
            rule_nr="2.02.11.23",
            test_type="schema_xpath",
            text="<xs:unique> MOET NIET gebruikt worden",
            xpath="//xs:unique",
        ),
    ]
    return rules
