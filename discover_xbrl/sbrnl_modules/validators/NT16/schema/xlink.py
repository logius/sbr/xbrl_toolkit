import logging

from ..classes import ValidationRule

logger = logging.getLogger("validator.schema")


"""
All NL 2.02.01.* validations
"""


def nta_2_02_01_03(concept):
    if concept.is_abstract:
        return False

    elif len(concept.references):
        return False

    elif "TypedMember" in concept.name:
        # EK: TypedMembers are exempt from this rule.
        # I couldn't think of a better way to test this
        return False

    elif concept.substitutionGroup in [
        "sbr:presentationTuple",
        "sbr:specificationTuple",
    ]:
        # Tuples are exempt from the rule
        return False

    for label in concept.labels.values():
        if label.linkRole.endswith("documentation") or label.linkRole.endswith(
            "commentaryGuidance"
        ):
            return False

    return True


def nta_2_02_01_05(concept):
    combinations = []
    for label in concept.labels.values():
        if label.linkRole.endswith("documentation") or label.linkRole.endswith(
            "Guidance"
        ):
            continue  # de BD voegt meerdere labels van deze typen toe, dat mag nu 20230208
        combination = (label.language, label.linkRole)
        if combination not in combinations:
            combinations.append(combination)
        else:
            return True

    return False


def schema_xlink():

    rules = [
        ValidationRule(
            rule_nr="2.02.01.01",
            test_type="schema_xpath",
            text="Een schema MOET één functie ondersteunen: (opsomming)",
            xpath="count(//xs:element[@abstract='false' and not(contains(@substitutionGroup, 'Tuple'))][1] | "
            "//xs:element[@abstract='true' and not(contains(@substitutionGroup, 'sbr:domain'))][1] | "
            "//*[local-name()='roleType'][1] | "
            "(//xs:complexType)[1]) > 1"
            "or count(//xs:element[@abstract='false' and not(contains(@substitutionGroup, 'Tuple'))][1]|"
            "//xs:element[@abstract='true'and not(contains(@substitutionGroup, 'sbr:domain'))][1] | "
            "//*[local-name()='roleType'][1] | "
            "(//xs:simpleType)[1]) > 1",
            note="Still todo: dimension, hypercube(table)"
            "abstract True/False mogen in een domain-bestand samen als de substitutiegroep "
            "sbr:domain bevat."
            "de dubbeling is omdat complexType en simpleType samen mogen voorkomen, "
            "maar niet in combinatie met anderen",
        ),
        ValidationRule(
            rule_nr="2.02.01.03",
            test_type="model_element",
            text="Een schema dat niet abstracte items definieert MOET een (2.1) reference linkbase gelinkt hebben EN/OF een label linkbase met @xlink:role=documentation",
            hardcoded_test=nta_2_02_01_03,
        ),
        ValidationRule(
            rule_nr="2.02.01.04",
            test_type="schema_xpath",
            text="Elk schema waarin XML nodes gedefineerd worden MOET een (generic) label linkbase gelinkt hebben",
            xpath="(//xs:enumeration or //xs:complexType or //xs:simpleType or "
            "//*[local-name()='roleType'])"
            "and not(//link:linkbaseRef[@xlink:arcrole='http://www.w3.org/1999/xlink/properties/linkbase'])",
        ),
        ValidationRule(
            rule_nr="2.02.01.05",
            test_type="model_element",
            text="Een element MOET NIET meer dan één label resource in één taal, role, arcrole en linkrole hebben",
            hardcoded_test=nta_2_02_01_05,
        ),
    ]
    return rules
