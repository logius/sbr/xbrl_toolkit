import logging

from ..classes import ValidationRule

logger = logging.getLogger("validator.schema")


"""
All NL 2.02.07.* validations
Deze regels worde nmet voeten getreden (en de meeste zijn warnings)
"""


def schema_type_enums():

    rules = [
        ValidationRule(
            rule_nr="2.02.07.02",
            test_type="schema_xpath",
            text="Lengte restricties op types BEHOREN niet gebruikt te worden (gebruik business rules)",
            xpath="//xs:complexType//xs:restriction/xs:maxLength or //xs:simpleType/xs:restriction/xs:maxLength",
            note="new _adv. Deze zal dus ALTIJD afgaan, de uitzonderingen die we toestaan zijn niet gepubliceerd",
            severity="WARNING",
        ),
        ValidationRule(
            rule_nr="2.02.07.03",
            test_type="schema_xpath",
            text="Enumeraties BEHOREN niet gebruikt te worden (gebruik domein)",
            xpath="//xs:complexType//xs:restriction/xs:enumeration or //xs:complexType//xs:restriction/xs:enumeration",
            severity="WARNING",
        ),
        ValidationRule(
            rule_nr="2.02.07.04",
            test_type="schema_xpath",
            text="Enumeraties MOETEN xbrli:stringItemType gebaseerd zijn",
            xpath="//xs:complexType//xs:restriction[@base!='xbrli:stringItemType']/xs:enumeration or //xs:complexType//xs:restriction[@base!='xbrli:stringItemType']/xs:enumeration",
        ),
    ]
    return rules
