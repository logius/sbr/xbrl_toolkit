import logging

from ..classes import ValidationRule

logger = logging.getLogger("validator.schema")


"""
All NL 2.02.05.* validations
"""


def schema_link_parts():

    rules = [
        ValidationRule(
            rule_nr="2.02.05.01",
            test_type="schema_xpath",
            text="Reference resource parts MOETEN NIET door NT Partners aangemaakt worden",
            xpath="//xs:element[@substitutionGroup='link:part']",
        )
    ]
    return rules
