import logging
import re

from ..classes import ValidationRule

logger = logging.getLogger("validator.schema")


"""
All NL 2.02.00.* validations
"""


def nta_2_02_00_03(file):
    lines = file.split("\n")
    # If the string is found, the test returns false
    pattern = """encoding=['|"]utf-8['|"]"""
    if re.search(pattern, lines[0], flags=re.IGNORECASE):
        return False
    return True


def nta_2_02_00_04(file):
    # Een schema MOET een XML commentaarsectie op regel twee (en verder) bevatten met daarin de IP rechten, releasedatum en versie.
    lines = file.split("\n")

    if not "<!--" in lines[1]:  # Line 2 should  start with a comment
        return True
    return False


def nta_2_02_00_05(file):
    # Een schema MOET NIET meer dan 1 XML comment node bevatten
    lines = file.split("\n")
    for line in lines[2:]:
        # If a start comment tag is found after line 2, there are either more comment sections, or the first one starts
        # after line 2, also triggering 2.02.00.04
        if "<!--" in line:

            return True
    return False


def nta_2_02_00_07(file):
    # Een schema MOET de root node (xs:schema) direct achter de IP rechten hebben
    lines = file.split("\n")
    comments_end_index = 0
    schema_start_index = 0
    for line in lines:
        if "-->" in line:
            comments_end_index = lines.index(line)
        if "<xs:schema" in line:
            schema_start_index = lines.index(line)

    # If the start schema tag is not on the next row, throw error.
    if schema_start_index - comments_end_index != 1:
        return True
    return False


def schema_base():

    rules = [
        ValidationRule(
            rule_nr="2.02.00.02",
            test_type="schema_tag",
            text="Een schema MOET alleen inhoud hebben gebaseerd op XML 1.0 van de W3C",
            tag="{http://www.w3.org/2001/XMLSchema}schema",
            note="Does check for tag, but not for extra info. Should check for allowed imports as well",
        ),
        ValidationRule(
            rule_nr="2.02.00.03",
            test_type="schema_file_hardcoded",
            text="Een schema MOET UTF-8 karakterset voor de inhoud ondersteunen",
            hardcoded_test=nta_2_02_00_03,
        ),
        ValidationRule(
            rule_nr="2.02.00.04",
            test_type="schema_file_hardcoded",
            text="Een schema MOET een XML commentaarsectie op regel twee (en verder) bevatten met daarin de IP rechten, releasedatum en versie.",
            hardcoded_test=nta_2_02_00_04,
        ),
        ValidationRule(
            rule_nr="2.02.00.05",
            test_type="schema_file_hardcoded",
            text="Een schema MOET NIET meer dan 1 XML comment node bevatten",
            hardcoded_test=nta_2_02_00_05,
        ),
        ValidationRule(
            rule_nr="2.02.00.06",
            test_type="schema_xpath",
            text="Een schema MOET alleen prefixed element nodes bevatten",
            xpath="//*[namespace-uri()='']",
            note="Revised _av This is meant for all XML elements",
        ),
        ValidationRule(
            rule_nr="2.02.00.07",
            test_type="schema_file_hardcoded",
            text="Een schema MOET de root node (xs:schema) direct achter de IP rechten hebben",
            hardcoded_test=nta_2_02_00_07,
        ),
        ValidationRule(
            rule_nr="2.02.00.08",
            test_type="schema_xpath",
            text="Een schema MOET een @targetNamespace hebben",
            xpath="not(//xs:schema[@targetNamespace])",
        ),
        ValidationRule(
            rule_nr="2.02.00.09",
            test_type="schema_xpath",
            text="Een schema MOET @attributeFormDefault en @elementFormDefault met de waarden 'unqualified' en 'qualified' respectievelijk bevatten",
            xpath='not(//*[@attributeFormDefault="unqualified"] and //*[@elementFormDefault="qualified"])',
        ),
        ValidationRule(
            rule_nr="2.02.00.10",
            test_type="schema_xpath",
            text="Een schema MOET NIET @blockDefault, @finalDefault en @version vermelden",
            xpath="(//*[@blockDefault] or //*[@finalDefault] or //*[@version])",
        ),
        ValidationRule(
            rule_nr="2.02.00.12",
            test_type="schema_xpath",
            text="Een schema waar linkroles of arcroles gedefinieerd worden of linkbases gekoppeld worden, MOET de node waarin dit gebeurd (<xs:annotation><xs:appinfo>) direct achter de root node hebben",
            xpath="//xs:annotation and not(//xs:schema/*[1]=xs:annotation)",
            note="Revised _av",
        ),
        ValidationRule(
            rule_nr="2.02.00.14",
            test_type="schema_xpath",
            text="Een schema dat <xs:import> nodes gebruikt MOET deze direct achter de <xs:annotation><xs:appinfo> node opnemen",
            xpath="//xs:import and //xs:appinfo and not (//xs:import//preceding-sibling::*[1]=xs:annotation)",
            note="Revised _av (nog niet correct, many false positives)",
        ),
        ValidationRule(
            rule_nr="2.02.00.16",
            test_type="schema_xpath",
            text="xs:schema/xs:import/@schemaLocation MOET gebruik maken van absolute URIs voor bestanden buiten een versie van de NT of buiten de PE-schema",
            xpath='//xs:import[not(starts-with(@schemaLocation, "http")) and not(starts-with(@namespace, "http://www.nltaxonomie.nl/nt"))]',
            note="Revised _av Opposite of 2.02.00.17",
        ),
        ValidationRule(
            rule_nr="2.02.00.17",
            test_type="schema_xpath",
            text="xs:schema/xs:import/@schemaLocation MOET gebruik maken van relatieve URIs voor bestanden binnen een versie van de NT of binnen de PE-schema",
            xpath='//xs:import[starts-with(@schemaLocation, "http://www.nltaxonomie.nl/nt") and starts-with(@namespace, "http://www.nltaxonomie.nl/nt")]',
            note="Added _av Opposite of 2.02.00.16 ",
        ),
        ValidationRule(
            rule_nr="2.02.00.18",
            test_type="schema_xpath",
            text="xs:schema/xs:include MOET NIET gebruikt worden",
            xpath="//xs:schema/xs:include",
        ),
        ValidationRule(
            rule_nr="2.02.00.19",
            test_type="schema_xpath",
            text="Een schema MOET NIET namespaceprefixes declareren op element niveau",
            xpath="//*[@xml:base]",
            note="Revised _av",
        ),
        ValidationRule(
            rule_nr="2.02.00.22",
            test_type="schema_xpath",
            text="Er mag slechts één <xs:annotation> node in een schema bestand voorkomen",
            xpath="count(//xs:annotation) > 1",
        ),
        ValidationRule(
            rule_nr="2.02.00.23",
            test_type="schema_xpath",
            text="Een entrypoint xs:schema MOET een @id hebben.",
            xpath='//xs:schema[contains(@targetNamespace,"entrypoints") and not(@id)]',
            note="Revised _av",
        ),
        ValidationRule(
            rule_nr="2.02.00.27",
            test_type="schema_xpath",
            text="Een schema MOET unieke linkbaseRefs bevatten.",
            xpath="not(count(//*[local-name()='linkbaseRef']/@xlink:href) = count(//*[local-name()='linkbaseRef'][not(@xlink:href = following-sibling::*[local-name()='linkbaseRef']/@xlink:href)]/@xlink:href))",
            note="Working with localnames for the tag 'linkbaseRef' because the namespace might be abssent in the schema. "
            "The same should probably be done for the attribute-name 'xlink:href'",
        ),
        ValidationRule(
            rule_nr="2.02.00.29",
            test_type="schema_file_hardcoded",
            hardcoded_test=nta_2_02_00_07,
            text="Een schema MOET de root node direct na het enige commentaar hebben of direct aan het begin van het document hebben als er geen commentaar aanwezig is.",
            note="What is this different form 2.02.00.07? ",
        ),
    ]
    return rules
