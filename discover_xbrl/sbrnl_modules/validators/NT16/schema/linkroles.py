import logging

from ..classes import ValidationRule

logger = logging.getLogger("validator.schema")


def nta_2_02_03_01(linkrole):
    if linkrole.used_on:
        if "link:calculationLink" in linkrole.used_on:
            return True

    return False


def nta_2_02_03_02(linkrole):
    # 	Een linkrole MOET GEEN kind element <link:usedOn> hebben waarvan de waarde niet geadresseerd wordt
    if not linkrole.used_on:
        return False  # No used on values present
    arc_types = []

    for arc in linkrole.arcs:
        if arc.arc_type not in arc_types:
            arc_types.append(arc.arc_type)

    if (
        "link:definitionLink" in linkrole.used_on
        and "{http://www.xbrl.org/2003/linkbase}definitionArc" not in arc_types
    ):
        return True

    elif (
        "link:presentationLink" in linkrole.used_on
        and "{http://www.xbrl.org/2003/linkbase}presentationArc" not in arc_types
    ):
        return True

    return False


def nta_2_02_03_03(linkrole):
    # Rule states:Linkroles zijn de root van een set aan relaties en kunnen daarom getoond worden in een GUI.
    # So we can ignore built-in roles because those are not used for presentation
    if linkrole.roleURI in [
        "http://www.nltaxonomie.nl/2011/role/linkrole-info",
        "http://www.nltaxonomie.nl/2016/labelrole/domainSpecificTerseLabel",
        "http://www.nltaxonomie.nl/2016/labelrole/domainSpecificVerboseLabel",
        "http://www.nltaxonomie.nl/2016/labelrole/domainSpecificLabel",
        "http://www.nltaxonomie.nl/2016/labelrole/domainSpecificPeriodEndLabel",
        "http://www.nltaxonomie.nl/2016/labelrole/domainSpecificPeriodStartLabel",
        "http://www.nltaxonomie.nl/2016/labelrole/domainSpecificTotalLabel",
        "http://www.nltaxonomie.nl/2016/labelrole/industrySpecificTerseLabel",
        "http://www.nltaxonomie.nl/2016/labelrole/industrySpecificVerboseLabel",
        "http://www.nltaxonomie.nl/2016/labelrole/industrySpecificLabel",
        "http://www.nltaxonomie.nl/2016/labelrole/industrySpecificPeriodEndLabel",
        "http://www.nltaxonomie.nl/2016/labelrole/industrySpecificPeriodStartLabel",
        "http://www.nltaxonomie.nl/2016/labelrole/industrySpecificTotalLabel",
        "http://www.nltaxonomie.nl/2016/labelrole/entitySpecificTerseLabel",
        "http://www.nltaxonomie.nl/2016/labelrole/entitySpecificVerboseLabel",
        "http://www.nltaxonomie.nl/2016/labelrole/entitySpecificLabel",
        "http://www.nltaxonomie.nl/2016/labelrole/entitySpecificPeriodEndLabel",
        "http://www.nltaxonomie.nl/2016/labelrole/entitySpecificPeriodStartLabel",
        "http://www.nltaxonomie.nl/2016/labelrole/entitySpecificTotalLabel",
    ]:
        return False
    elif "http://www.xbrl.org/" in linkrole.roleURI:
        return False

    # Een linkrole URI MOET een Generic Label hebben
    for label_name, label in linkrole.labels.items():

        # If a generic label has been found, return False.
        if label.linkRole == "http://www.xbrl.org/2008/role/label":
            return False
    return True


"""
All NL 2.02.03.* validations
"""


def schema_linkroles():

    rules = [
        ValidationRule(
            rule_nr="2.02.03.01",
            test_type="model_linkrole",
            text="//link:usedOn='link:calculationLink' MOET NIET gebruikt worden",
            # xpath="//link:usedOn[text()='link:calculationLink']"
            hardcoded_test=nta_2_02_03_01,
        ),
        ValidationRule(
            rule_nr="2.02.03.02",
            test_type="model_referenced_linkrole",
            text="Een linkrole MOET GEEN kind element <link:usedOn> hebben waarvan de waarde niet geadresseerd wordt",
            hardcoded_test=nta_2_02_03_02,
            note="Test checks if a presentation and/or definition hierarchy is available if relevant. This test may also fail if the taxonomy does contain information on these roles.",
        ),
        ValidationRule(
            rule_nr="2.02.03.03",
            test_type="model_linkrole",
            text="Een linkrole URI MOET een Generic Label hebben",
            hardcoded_test=nta_2_02_03_03,
            severity="WARNING",
        ),
    ]
    return rules
