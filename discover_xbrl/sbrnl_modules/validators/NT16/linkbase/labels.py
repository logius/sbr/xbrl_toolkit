import logging
import lxml.html

from ..classes import ValidationRule

logger = logging.getLogger("validator.linkbase")


def nta_2_03_08_10(label):
    return (
        lxml.html.fromstring(bytes(label.text, encoding="utf-8")).find(".//*")
        is not None
    )


def linkbase_labels():

    rules = [
        ValidationRule(
            rule_nr="2.03.08.01",
            test_type="linkbase_xpath",
            text="De waarde van @xml:lang voor Nederlands MOET ‘nl’ zijn",
            xpath="//*[contains(@xml:lang,'nl-')]",
        ),
        ValidationRule(
            rule_nr="2.03.08.02",
            test_type="linkbase_xpath",
            text="De waarde van @xml:lang voor Engels MOET ‘en’ zijn",
            xpath="//*[contains(@xml:lang,'en-')]",
        ),
        # ValidationRule(
        #     rule_nr="2.03.08.04",
        #     test_type="linkbase_xpath",
        #     text="NT Partners die labelrollen met semantische betekenis gebruiken MOETEN een waarschuwing in de 'Business Rules' documentatie opnemen",
        #     xpath="//*[contains(@xml:lang,'en-')]"
        # ),
        # ValidationRule(
        #     rule_nr="2.03.08.05",
        #     test_type="linkbase_xpath",
        #     text="Van alle gebruikte labelrolen in de NT MOET een voorkomen in de lokale taal bestaan",
        #     xpath="not(//*[@xml:lang='nl')]",
        #     note="Needs to be checked in model as languages exist in different files"
        # ),
        # ValidationRule(
        #     rule_nr="2.03.08.06",
        #     test_type="linkbase_xpath",
        #     text="Label resources in de @xlink:role documentation MOGEN NIET tegenstrijdig zijn aan eventuele standaard referenties naar het gekoppelde concept en v.v.",
        #     xpath=""
        # ),
        ValidationRule(
            rule_nr="2.03.08.07",
            test_type="linkbase_xpath",
            text="Een <link:label> inhoud MOET behandeld worden als een xs:tokenizedString",
            xpath="//link:label[contains(text(), '\t') or contains(text(), '\n') or contains(text(), '\r')]",
        ),
        ValidationRule(
            rule_nr="2.03.08.08",
            test_type="linkbase_xpath",
            text="Een labelArc MAG NIET een @order bevatten.",
            xpath="//link:labelArc[@order]",
        ),
        ValidationRule(
            rule_nr="2.03.08.10",
            test_type="model_label",
            text="<link:label> inhoud MOET GEEN opmaak bevatten",
            xpath="//link:label[contains(text(), '<')]",
            hardcoded_test=nta_2_03_08_10,
        ),
        ValidationRule(
            rule_nr="2.03.08.11",
            test_type="linkbase_xpath",
            text="Een label resource MOET een @id hebben",
            xpath="//link:label[not(@id)]",
        ),
        ValidationRule(
            rule_nr="2.03.08.13",
            test_type="linkbase_xpath",
            text="De waarde van @xml:lang voor Duits MOET ‘de’ zijn",
            xpath="//*[contains(@xml:lang,'de-')]",
        ),
        ValidationRule(
            rule_nr="2.03.08.14",
            test_type="linkbase_xpath",
            text="De waarde van @xml:lang voor Frans MOET ‘fr’ zijn",
            xpath="//*[contains(@xml:lang,'fr-')]",
        ),
    ]
    return rules
