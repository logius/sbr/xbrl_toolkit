import logging

from ..classes import ValidationRule

logger = logging.getLogger("validator.linkbase")


def linkbase_references():

    rules = [
        # ValidationRule(
        #     rule_nr="2.03.03.01",
        #     test_type="linkbase_xpath",
        #     text="Een reference resource gebaseerd op door XBRL gedefinieerde link:parts MOET het ref-2006-02-27.xsd schema gebruiken",
        #     xpath="//link:reference and //link:reference/*[not(self::ref:*)]",
        #     note="xpath looks for all children that are not of prefix 'ref'. "
        #          "In theory, the taxonomy builder could refer to an entire different schema which is not checked."
        # ),
        ValidationRule(
            rule_nr="2.03.03.02",
            test_type="linkbase_xpath",
            text="Een reference resource MOET een @id hebben",
            xpath="//link:reference and //link:reference[not(@id)]",
        ),
        # 2.03.03.03 Modeltesting
        # 2.03.03.04 Kansrekening doen? split on space ...
        ValidationRule(
            rule_nr="2.03.03.05",
            test_type="linkbase_xpath",
            text="Een referenceArc MAG NIET @order bevatten",
            xpath="//link:referenceArc and //link:referenceArc[@order]",
        ),
    ]
    return rules
