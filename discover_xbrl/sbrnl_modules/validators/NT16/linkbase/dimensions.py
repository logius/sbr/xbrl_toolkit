import logging

from ..classes import ValidationRule

logger = logging.getLogger("validator.linkbase")


def linkbase_dimensions():

    rules = [
        ValidationRule(
            rule_nr="2.03.06.01",
            test_type="note",
            text="De relaties die de dimensie vorm geven BEHOREN NIET gezien te worden als de presentatie",
            note="This rule is just a human readable note without testable elements",
        ),
        # ValidationRule(
        #     rule_nr="2.03.06.02",
        #     test_type="linkbase_xpath",
        #     text="Een dimension MOET een unieke set van domeinleden hebben",
        #     xpath='',
        # ),
        ValidationRule(
            rule_nr="2.03.06.03",
            test_type="linkbase_xpath",
            text="Dimension-domain relaties met @usable='false' MOET een @targetRole gebruiken",
            xpath="//link:definitionArc[@xbrldt:usable='false' and not(@xbrldt:targetRole)]",
        ),
        ValidationRule(
            rule_nr="2.03.06.04",
            test_type="linkbase_xpath",
            text="De arcrole all (has hypercube) MOET @xbrldt:closed='true' bevatten",
            xpath="//link:definitionArc[@xlink:arcrole='http://xbrl.org/int/dim/arcrole/all' and not(@xbrldt:closed='true')]",
        ),
        ValidationRule(
            rule_nr="2.03.06.05",
            test_type="linkbase_xpath",
            text="De arcrole dimension-default MOET NIET gebruikt worden",
            xpath="//link:definitionArc[@xlink:arcrole='http://xbrl.org/int/dim/arcrole/dimension-default']",
        ),
    ]
    return rules
