import logging

from ..classes import ValidationRule

logger = logging.getLogger("validator.linkbase")


def linkbase_formulas():

    rules = [
        ValidationRule(
            rule_nr="2.03.09.01",
            test_type="linkbase_xpath",
            text="Calculatie linkbases MOETEN NIET gebruikt worden",
            xpath="//link:calculationArc",
        ),
        ValidationRule(
            rule_nr="2.03.09.03",
            test_type="linkbase_xpath",
            text="formula:formula MOET NIET gebruikt worden",
            xpath="//formula:formula",
        ),
        ValidationRule(
            rule_nr="2.03.09.09",
            test_type="linkbase_xpath",
            text="<formula:consistencyAssertion> MOET NIET gebruikt worden",
            xpath="//formula:consistencyAssertion",
        ),
        ValidationRule(
            rule_nr="2.03.09.10",
            test_type="linkbase_xpath",
            text="Een assertion MOET NIET een satisfied message, middels een http://xbrl.org/arcrole/2010/assertion-satisfied-message arc hebben",
            xpath="//gen:arc[@xlink:arcrole='http://xbrl.org/arcrole/2010/assertion-satisfied-message']",
        ),
    ]
    return rules


# model-tests
# 2.03.09.07
# 2.03.09.08
# 2.03.09.11
# 2.03.09.18
# 2.03.09.19
# 2.03.09.20
# 2.03.09.21
# 2.03.09.22
# 2.03.09.23

# niet te testen?
# 2.03.09.04 !
# 2.03.09.05 !
# 2.03.09.06 ??
# 2.03.09.12
# 2.03.09.13
# 2.03.09.14
# 2.03.09.15 ??
# 2.03.09.16 ??
# 2.03.09.17 !
