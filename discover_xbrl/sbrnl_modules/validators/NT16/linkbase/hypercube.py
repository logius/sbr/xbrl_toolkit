import logging

from ..classes import ValidationRule

logger = logging.getLogger("validator.linkbase")


def nta_2_03_05_01(hypercube):

    for dimension in hypercube.dimensions:
        if not dimension:
            return True
    return False


def nta_2_03_05_04(hypercube):

    for dimension in hypercube.dimensions:
        if not dimension.target_linkrole:
            for member in dimension.members:
                # If there is no typedDomainRef, but there is a substitutionGroup (indicating a TypedMember), return true
                if (
                    not member.concept.typedDomainRef
                    and member.concept.substitutionGroup
                ):
                    return True

    return False


def linkbase_hypercube():

    rules = [
        # ValidationRule(
        #     rule_nr="2.03.05.01",
        #     test_type="model_hypercube",
        #     text="Een hypercube MOET NIET meer dan één sbr:primaryDomain als kind in een 'a1l' arc per linkrole hebben",
        #     hardcoded_test=nta_2_03_05_01
        # ),
        ValidationRule(
            rule_nr="2.03.05.04",
            test_type="model_hypercube",
            text="De arcrole hypercube-dimension MOET @targetRole gebruiken om de expliciete dimensie inhoud te adresseren",
            hardcoded_test=nta_2_03_05_04,
        ),
        ValidationRule(
            rule_nr="2.03.05.05",
            test_type="linkbase_xpath",
            text="Een hypercube – primary relatie MOET de ‘all’ arcrole gebruiken",
            xpath="//link:definitionArc[@xlink:arcrole='http://xbrl.org/int/dim/arcrole/notAll']",
        ),
        ValidationRule(
            rule_nr="2.03.05.06",
            test_type="linkbase_xpath",
            text="Een hypercube – primary relatie MOET @xbrldt:contextElement='scenario' gebruiken",
            xpath="//link:definitionArc[@xlink:arcrole='http://xbrl.org/int/dim/arcrole/all'] and not(//link:definitionArc[@xlink:arcrole='http://xbrl.org/int/dim/arcrole/all' and @xbrldt:contextElement='scenario'])",
            note="Expression is based on the assumption that 2.03.05.05 is met, and a hypercube-primary relation can be assumed with @xlink:arcrole='http://xbrl.org/int/dim/arcrole/all'",
        ),
        ValidationRule(
            rule_nr="2.03.05.07",
            test_type="linkbase_xpath",
            text="De arcrole hypercube-dimension MOET NIET @targetRole gebruiken bij een typed dimensie",
            xpath="//link:definitionArc[@xlink:arcrole='http://xbrl.org/int/dim/arcrole/all' and @xbrldt:targetRole]",
        ),
        #
        # ValidationRule(
        #     rule_nr="2.03.05.09",
        #     test_type="linkbase_xpath",
        #     text="Primaries in een entrypoint MOETEN allen dimensioneel of niet-dimensionaal zijn (niet beide)",
        #     xpath="",
        # ),
    ]
    return rules
