import logging

from ..classes import ValidationRule

logger = logging.getLogger("validator.linkbase")


def linkbase_other():

    rules = [
        ValidationRule(
            rule_nr="2.03.10.01",
            test_type="linkbase_xpath",
            text="@xlink:actuate MOET NIET gebruikt worden",
            xpath="//*[@xlink:actuate]",
        ),
        ValidationRule(
            rule_nr="2.03.10.02",
            test_type="linkbase_xpath",
            text="@xlink:show MOET NIET gebruikt worden",
            xpath="//*[@xlink:show]",
        ),
        ValidationRule(
            rule_nr="2.03.10.03",
            test_type="linkbase_xpath",
            text="@xlink:title MOET NIET gebruikt worden",
            xpath="//*[@xlink:title]",
        ),
        ValidationRule(
            rule_nr="2.03.10.04",
            test_type="linkbase_xpath",
            text="link:linkbase/@id MOET NIET gebruikt worden",
            xpath="//link:linkbase[@id]",
        ),
        ValidationRule(
            rule_nr="2.03.10.05",
            test_type="linkbase_xpath",
            text="link:linkbase/@xsi:nil MOET NIET gebruikt worden",
            xpath="//link:linkbase[@xsi:nil]",
        ),
        ValidationRule(
            rule_nr="2.03.10.06",
            test_type="linkbase_xpath",
            text="link:linkbase/@xsi:noNamespaceSchemaLocation MOET NIET gebruikt worden",
            xpath="//link:linkbase[@xsi:noNamespaceSchemaLocation]",
        ),
        ValidationRule(
            rule_nr="2.03.10.07",
            test_type="linkbase_xpath",
            text="link:linkbase/@xsi:type MOET NIET gebruikt worden",
            xpath="//link:linkbase[@xsi:type]",
        ),
        ValidationRule(
            rule_nr="2.03.10.08",
            test_type="linkbase_xpath",
            text="link:loc/@xlink:role MOET NIET gebruikt worden",
            xpath="//link:loc[@xlink:role]",
        ),
        ValidationRule(
            rule_nr="2.03.10.09",
            test_type="linkbase_xpath",
            text="link:roleRef/@xlink:arcrole MOET NIET gebruikt worden",
            xpath="//link:roleRef[@xlink:arcrole]",
        ),
        ValidationRule(
            rule_nr="2.03.10.10",
            test_type="linkbase_xpath",
            text="link:roleRef/@xlink:role MOET NIET gebruikt worden",
            xpath="//link:roleRef[@xlink:role]",
        ),
        ValidationRule(  # dit is een SCHEMA test, of komen er ook roleTypes in linkbases voor?
            rule_nr="2.03.10.11",
            test_type="linkbase_xpath",
            text="link:roleType/@id MOET gebruikt worden",
            xpath="//link:roleType[not(@id)]",
        ),
        ValidationRule(
            rule_nr="2.03.10.12",
            test_type="linkbase_xpath",
            text="link:documentation MOET NIET gebruikt worden",
            xpath="//link:documentation",
        ),
    ]
    return rules


# TODO: skipped, non-existent at the moment; 10.13, 10.14, 10.15
# model-tests

# niet te testen?
