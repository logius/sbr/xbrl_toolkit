import logging

from ..classes import ValidationRule

logger = logging.getLogger("validator.linkbase")


def linkbase_arcroles():

    rules = [
        ValidationRule(
            rule_nr="2.03.02.02",
            test_type="linkbase_xpath",
            text="De arcrole ‘essence-alias’ MOET NIET gebruikt worden",
            xpath='//*[@xlink:arcrole="http://www.xbrl.org/2003/arcrole/essence-alias"]',
        ),
        ValidationRule(
            rule_nr="2.03.02.03",
            test_type="linkbase_xpath",
            text="De arcrole ‘similar-tuples’ MOET NIET gebruikt worden",
            xpath='//*[@xlink:arcrole="http://www.xbrl.org/2003/arcrole/similar-tuples"]',
        ),
        ValidationRule(
            rule_nr="2.03.02.04",
            test_type="linkbase_xpath",
            text="De arcrole ‘requires-element’ MOET NIET gebruikt worden",
            xpath='//*[@xlink:arcrole="http://www.xbrl.org/2003/arcrole/requires-element"]',
        ),
        ValidationRule(
            rule_nr="2.03.02.05",
            test_type="linkbase_xpath",
            text="<link:arcroleRef> @xlink:arcrole MOET NIET gebruikt worden",
            xpath="//link:arcroleRef[@xlink:arcrole]",
        ),
        ValidationRule(
            rule_nr="2.03.02.06",
            test_type="linkbase_xpath",
            text="<link:arcroleRef> @xlink:role MOET NIET gebruikt worden",
            xpath="//link:arcroleRef[@xlink:role]",
        ),
    ]
    return rules
