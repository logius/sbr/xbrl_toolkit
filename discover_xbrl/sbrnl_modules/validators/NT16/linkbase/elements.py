import logging

from ..classes import ValidationRule

logger = logging.getLogger("validator.linkbase")


def linkbase_elements():

    rules = [
        ValidationRule(
            rule_nr="2.03.01.01",
            test_type="linkbase_xpath",
            text="Een Linkbase MOET NIET namespaces declareren op element niveau",
            xpath="//*[@base]",
            note="Nakijken, dit zou een heel eenvoudige xpath moeten zijn; //*/*[@xmlns:*] (any element not being root having a xmlns-prefix, the results will stun you",
        )
    ]
    return rules
