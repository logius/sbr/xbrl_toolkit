import logging

from ..classes import ValidationRule

logger = logging.getLogger("validator.linkbase")


def linkbase_presentation():

    rules = [
        ValidationRule(
            rule_nr="2.03.04.04",
            test_type="linkbase_xpath",
            text="<link:presentationArc/@order> MOET gebruikt worden",
            xpath="//link:presentationArc and //link:presentationArc[not(@order)]",
        ),
        # think about 2.03.04.05
        # think about 2.03.04.06
        # think about 2.03.04.07
        # think about 2.03.04.08
        # Z-azis is allowed starting NT16
        # ValidationRule(
        #     rule_nr="2.03.04.09",
        #     test_type="linkbase_xpath",
        #     text="tableBreakdownArc axis='z' MOET NIET gebruikt worden",
        #     xpath="//table:tableBreakdownArc[@axis='z']",
        # ),
        # think about 2.03.04.10,
        # think about 2.03.04.11,
        # think about 2.03.04.12,
        # think about 2.03.04.13,
        # think about 2.03.04.14,
        # think about 2.03.04.15,
        # think about 2.03.04.16,
        # think about 2.03.04.17,
        # think about 2.03.04.18,
        # think about 2.03.04.19 WARNING,
        # think about 2.03.04.20,
        # think about 2.03.04.21,
    ]
    return rules
