import logging
import copy
import re
from lxml import etree

from ..classes import ValidationRule

logger = logging.getLogger("validator.linkbase")


def nta_2_03_00_01(linkbase):
    linkbase_elementtree = etree.ElementTree(linkbase)
    if "1.0" not in linkbase_elementtree.docinfo.xml_version:
        return True
    return False


def nta_2_03_00_02(linkbase):
    schemalocations = linkbase.attrib.get(
        "{http://www.w3.org/2001/XMLSchema-instance}schemaLocation", None
    )
    if len(schemalocations.split(" ")) != len(linkbase.nsmap):
        return True
    return False


def nta_2_03_00_03(file):
    lines = file.split("\n")
    # If the string is found, the test returns false
    pattern = """encoding=['|"]utf-8['|"]"""
    if re.search(pattern, lines[0], flags=re.IGNORECASE):
        return False
    return True


def nta_2_03_00_04(file):
    # Een schema MOET een XML commentaarsectie op regel twee (en verder) bevatten met daarin de IP rechten, releasedatum en versie.
    lines = file.split("\n")

    if not "<!--" in lines[1]:  # Line 2 should  start with a comment
        return True
    return False


def nta_2_03_00_05(file):
    # Een schema MOET NIET meer dan 1 XML comment node bevatten
    lines = file.split("\n")
    for line in lines[2:]:
        # If a start comment tag is found after line 2, there are either more comment sections, or the first one starts
        # after line 2, also triggering 2.02.00.04
        if "<!--" in line:
            return True
    return False


def nta_2_03_00_08(linkbase):
    linkbase_copy = copy.copy(linkbase)  # we don't want to change the original document
    etree.cleanup_namespaces(linkbase_copy)
    # next get all text-values, check if they are of the form aaa:somevalue, if so
    # the prefix should be present conforming XBRL-specs (XML is oblivious of that)
    # xpath select all text values with ':' and no spaces ...
    textvalues = linkbase_copy.xpath(
        "//*[text()[contains(., ':')]]/text()[not(contains(., ' '))]"
    )
    used_namespaces = linkbase_copy.nsmap  # nsmap is not writeable ?
    if textvalues:
        prefixed_values(textvalues, used_namespaces, linkbase)

    # any attribute could potentially hold an namespaced-value as well :(
    # this xpath select _attributes_ who's value contain a ':' and no spce
    attributes = linkbase_copy.xpath("//*/@*[contains(., ':')][not(contains(., ' '))]")
    if attributes:
        prefixed_values(attributes, used_namespaces, linkbase)

    if not len(linkbase.nsmap.keys()) == len(used_namespaces.keys()):
        # if what remains contains xff, xfi and xs used inside validations.
        # we grant those for the moment clemency
        remainder = [x for x in linkbase.nsmap.keys() if x not in used_namespaces]
        if remainder:
            for prefix in remainder:
                if prefix in ["xff", "xs", "xfi"]:
                    used_namespaces[prefix] = f"http://uri.org/{prefix}"

    if not len(linkbase.nsmap.keys()) == len(used_namespaces.keys()):
        tests = linkbase_copy.xpath("//*/@test[contains(., ':')]")
        if tests:
            for unused_prefix in [
                x for x in linkbase.nsmap.keys() if x not in used_namespaces
            ]:
                for test in tests:
                    if (
                        f"{unused_prefix}:" in test
                        and unused_prefix not in used_namespaces
                    ):
                        used_namespaces[
                            unused_prefix
                        ] = f"http://uri.org/{unused_prefix}"

    # yet, it's also possible to use qualified names in variable:parameter,
    # hidden inside the select attribute
    if not len(linkbase.nsmap.keys()) == len(used_namespaces.keys()):
        selects = linkbase_copy.xpath("//*/@select[contains(., ':')]")
        if selects:
            for unused_prefix in [
                x for x in linkbase.nsmap.keys() if x not in used_namespaces
            ]:
                for select in selects:
                    if (
                        f"{unused_prefix}:" in select
                        and unused_prefix not in used_namespaces
                    ):
                        used_namespaces[
                            unused_prefix
                        ] = f"http://uri.org/{unused_prefix}"

    # Can also be part of the text in a message
    if (
        not len(linkbase.nsmap.keys()) == len(used_namespaces.keys())
        and "http://xbrl.org/2010/message" in linkbase.nsmap.values()
    ):
        selects = linkbase_copy.xpath("//msg:message", namespaces=linkbase.nsmap)
        if selects:
            for unused_prefix in [
                x for x in linkbase.nsmap.keys() if x not in used_namespaces
            ]:
                for select in selects:
                    if (
                        f"{unused_prefix}:" in select.text
                        and unused_prefix not in used_namespaces
                    ):
                        used_namespaces[
                            unused_prefix
                        ] = f"http://uri.org/{unused_prefix}"

    if not len(linkbase.nsmap.keys()) == len(used_namespaces.keys()):
        # debug; which prefixes seem to be unused? This should be empty by now

        print(
            f"2.03.00.08: unused prefixes: {[x for x in linkbase.nsmap.keys() if x not in used_namespaces]}"
        )

    return not len(linkbase.nsmap.keys()) == len(used_namespaces.keys())


def nta_2_03_00_12(linkbase):
    # Een linkbase MOET relaties bevatten

    arctypes = [
        "//link:definitionArc",
        "//link:presentationArc",
        "//link:labelArc",
        "//link:referenceArc",
    ]
    if "gen" in linkbase.nsmap.keys():
        arctypes.append("//gen:arc")
    if "table" in linkbase.nsmap.keys():
        arctypes.append("//table:definitionNodeSubtreeArc")
        arctypes.append("//table:tableBreakdownArc")
        arctypes.append("//table:tableParameterArc")
        arctypes.append("//table:breakdownTreeArc")
        arctypes.append("//table:tableParameterArc")
    if "variable" in linkbase.nsmap.keys():
        arctypes.append("//variable:variableFilterArc")
        arctypes.append("//variable:variableSetFilterArc")
        arctypes.append("//variable:parameter")
        # techncally thisis of course not an Arc, but  they are in the variable namespace
        # and inside those files where parameters are listed, no Arc will be defined

    arcs = 0

    for arcType in arctypes:
        arcs = arcs + len(linkbase.xpath(arcType, namespaces=linkbase.nsmap))

    if arcs <= 0:
        # If there are no acts, this counts as no (outgoing) relations.
        return True
    return False


def prefixed_values(nodes, used_namespaces, linkbase):
    for elem in nodes:
        if len(elem.split(":")) == 2 and not elem.split(":")[0].startswith("http"):
            prefix, value = elem.split(":")
            if prefix in linkbase.nsmap.keys() and prefix not in used_namespaces.keys():
                used_namespaces[prefix] = f"http://uri.org/{prefix}"


"""
All NL 2.03.00.* validations
"""


def linkbase_base():
    rules = [
        ValidationRule(
            rule_nr="2.03.00.01",
            test_type="linkbase_hardcoded",
            text="Een linkbase MOET opgesteld zijn conform W3C XLink 1.0 Specificatie",
            hardcoded_test=nta_2_03_00_01,
            note="Does check for tag, but not for extra info. Should check for allowed imports as well",
        ),
        ValidationRule(
            rule_nr="2.03.00.02",
            test_type="linkbase_tag",
            text="Een linkbase MOET alleen inhoud hebben gebaseerd op XML 1.0 van de W3C",
            tag="{http://www.xbrl.org/2003/linkbase}linkbase",
            note="Does check for tag, but not for extra info. Should check for allowed imports as well",
        ),
        ValidationRule(
            rule_nr="2.03.00.03",
            test_type="linkbase_file_hardcoded",
            text="Een linkbase MOET UTF-8 karakterset voor de inhoud ondersteunen",
            hardcoded_test=nta_2_03_00_03,
        ),
        ValidationRule(
            rule_nr="2.03.00.04",
            test_type="linkbase_file_hardcoded",
            text="Een linkbase MOET een XML commentaarsectie op regel twee (en verder) bevatten met daarin de IP rechten, releasedatum en versie.",
            hardcoded_test=nta_2_03_00_04,
        ),
        ValidationRule(
            rule_nr="2.03.00.05",
            test_type="linkbase_file_hardcoded",
            text="XML Linkbase MOET NIET meer dan één XML comment node bevatten",
            hardcoded_test=nta_2_03_00_05,
        ),
        ValidationRule(
            rule_nr="2.03.00.06",
            test_type="linkbase_xpath",
            text="XML Linkbase MOET NIET ongeprefixte nodes bevatten",
            xpath="//*[namespace-uri()='']",
            note="This is meant for all XML elements",
        ),
        ValidationRule(
            rule_nr="2.03.00.08",
            test_type="linkbase_hardcoded",
            text="XML Linkbase MOET NIET ongebruikte namespaces declareren",
            hardcoded_test=nta_2_03_00_08,
        ),
        ValidationRule(
            rule_nr="2.03.00.10",
            test_type="linkbase_xpath",
            text="<link:presentationLink>, <link:labelLink>, <gen:link> en <link:referenceLink> MAG GEEN relatie bevatten die @use='prohibit' bevat",
            xpath='//*[@use="prohibit"]',
            note="Rule applied more broadly as attr/value combination may not be used",
        ),
        ValidationRule(
            rule_nr="2.03.00.12",
            test_type="linkbase_hardcoded",
            text="Een linkbase MOET relaties bevatten",
            hardcoded_test=nta_2_03_00_12,
        ),
    ]
    return rules
