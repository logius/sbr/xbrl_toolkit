"""
File that loads all relevant module parts.
"""
from .widget import validatorActionsBox


def load_module(module_object={}):
    module_object["name"] = "Validators"
    module_object["type"] = "validation"
    module_object["gui"] = validatorActionsBox
    module_object[
        "description"
    ] = """
    Contains validators that can be used when parsing a XBRL taxonomy. Validators contain different kind of rules.
    As of now, most rules are XPath based expressions, but validations build against the logical data model are planned
    as well.
    """

    return module_object
