from PySide2.QtWidgets import QGroupBox, QVBoxLayout, QComboBox

from PySide2.QtCore import Qt
from PySide2.QtWidgets import QGroupBox, QVBoxLayout, QComboBox

from discover_xbrl.conf.conf import Config


class validatorActionsBox(QGroupBox):
    def __init__(self, parent=None):
        super(validatorActionsBox, self).__init__(parent)
        self.title = "Choose validator"
        self.parent = parent
        self.conf = Config()

        # Create widgets
        self.validatorsDropdown = QComboBox()

        #  todo: automatically find new validators by crawling subdirs for module.py
        from .NT16.module import load_module as NT16_validator_module

        self.validator_options = {"No validator": None, "NT16": NT16_validator_module}

        for validator_name, validator_function in self.validator_options.items():
            self.validatorsDropdown.addItem(validator_name)

        # Set signal so validator_dropdown_changed is called every time the choice is changed.
        self.validatorsDropdown.currentIndexChanged.connect(
            self.validator_dropdown_changed
        )

        # Set the default choice to the preference set in the vars file
        index = self.validatorsDropdown.findText(
            self.conf.validator["default"], Qt.MatchFixedString
        )
        if index >= 0:
            self.validatorsDropdown.setCurrentIndex(index)

        # Set widgets to layout
        layout = QVBoxLayout()
        layout.addWidget(self.validatorsDropdown)

        # Set layout to class attribute
        self.setDisabled(False)
        self.setLayout(layout)

    def validator_dropdown_changed(self):
        selected_validator = self.validator_options[
            self.validatorsDropdown.currentText()
        ]

        self.parent.validator = selected_validator
