import pickle
import pandas as pd
from zipfile import ZipFile

from lxml import etree


test_taxonomy = "/tmp/xbrl/instances/NT15_KVK_20201209-middelgroot.xbrl"
# test_taxonomy = "/tmp/xbrl/instances/mini.xbrl"
contexts = {}


class Context:
    def __init__(
        self,
        id,
        identifier,
        identifier_scheme,
        period_start=None,
        period_end=None,
        period_instant=None,
        period_forever=False,
        scenario_members=None,
    ):

        self.id = id
        self.identifier = identifier
        self.identifier_scheme = identifier_scheme

        self.period_start = period_start
        self.period_end = period_end

        if not (period_start or period_end) and period_instant and period_forever:
            print("Context has no valid period defined!")

        self.period_instant = period_instant
        self.period_forever = period_forever

        if scenario_members:
            self.scenario_members = scenario_members
        else:
            self.scenario_members = []


class Scenario:
    def __init__(
        self,
        scenario_type,
        dimension,
        member_prefix,
        member_name,
        member_value=None,
        dimension_obj=None,
        member_obj=None,
    ):
        self.scenario_type = scenario_type
        self.dimension = dimension
        self.dimension_obj = dimension_obj
        self.member_prefix = member_prefix
        self.member_name = member_name
        self.member_obj = member_obj

        # for typed members, we expect a value as well
        if scenario_type == "typed" and member_value is None:
            print("Value expected for typed member")
        self.member_value = member_value

    def __str__(self):
        return f"{self.dimension} -> {self.member_name}"


class Fact:
    def __init__(
        self,
        concept_tag,
        concept_name,
        concept_prefix,
        context,
        decimals,
        unit,
        value,
        concept_obj=None,
    ):
        self.concept_tag = concept_tag
        self.concept_name = concept_name
        self.concept_prefix = concept_prefix
        # todo: Concept tag can be matched with namespace + id for concepts in DTS

        self.concept_obj = concept_obj
        self.context = context
        self.decimals = decimals
        self.unit = unit
        self.value = value


def get_context(xml_context):
    # Required
    id = xml_context.get("id")

    ### entity is required, the identifier subelement is the only thing we care about for now
    xml_identifier = xml_context.find(
        "{http://www.xbrl.org/2003/instance}entity/{http://www.xbrl.org/2003/instance}identifier"
    )
    # segment is optional (not implemented for now)

    # Entity.Scheme is required
    identifier_scheme = xml_identifier.get("scheme")
    identifier = xml_identifier.text

    ### period is required
    xml_identifier = xml_context.find("{http://www.xbrl.org/2003/instance}period")

    # Can have: [startDate, endDate], instant, forever
    xml_period_start = xml_identifier.find(
        "{http://www.xbrl.org/2003/instance}startDate"
    )
    if xml_period_start is not None:
        period_start = xml_period_start.text
    else:
        period_start = None

    xml_period_end = xml_identifier.find("{http://www.xbrl.org/2003/instance}endDate")
    if xml_period_end is not None:
        period_end = xml_period_end.text
    else:
        period_end = None

    xml_period_instant = xml_identifier.find(
        "{http://www.xbrl.org/2003/instance}instant"
    )
    if xml_period_instant is not None:
        period_instant = xml_period_instant.text
    else:
        period_instant = None

    xml_period_forever = xml_identifier.find(
        "{http://www.xbrl.org/2003/instance}forever"
    )
    if xml_period_forever is not None:
        period_forever = True
    else:
        period_forever = False

    # scenario is optional
    scenario_list = []

    xml_scenario = xml_context.find("{http://www.xbrl.org/2003/instance}scenario")
    if xml_scenario is not None:

        # Need to get a list of all scenario's

        # First the explicit members
        for xml_member in xml_scenario.findall(
            "{http://xbrl.org/2006/xbrldi}explicitMember"
        ):
            member_prefix, member_name = xml_member.text.split(":", 1)
            scenario_list.append(
                Scenario(
                    scenario_type="explicit",
                    dimension=xml_member.get("dimension"),
                    member_prefix=member_prefix,
                    member_name=member_name,
                )
            )

        # Then, typed members
        for xml_member in xml_scenario.findall(
            "{http://xbrl.org/2006/xbrldi}typedMember"
        ):

            xml_typed_member = xml_member[0]
            qname = etree.QName(xml_typed_member.tag)

            # Try and get the prefix
            member_prefix = None
            for prefix, namespace in xml_typed_member.nsmap.items():
                if namespace == qname.namespace:
                    member_prefix = prefix

            if member_prefix is None:
                print(
                    f"Did not find prefix for member {qname.localname}, namespace: {qname.namespace}"
                )

            scenario_list.append(
                Scenario(
                    scenario_type="typed",
                    dimension=xml_member.get("dimension"),
                    member_prefix=member_prefix,
                    member_name=qname.localname,
                    member_value=xml_typed_member.text,
                )
            )

    return Context(
        id=id,
        identifier=identifier,
        identifier_scheme=identifier_scheme,
        period_start=period_start,
        period_end=period_end,
        period_instant=period_instant,
        period_forever=period_forever,
        scenario_members=scenario_list,
    )


def get_unit(xml_unit):
    # Required
    id = xml_unit.get("id")

    ### entity is required, the identifier subelement is the only thing we care about for now
    xml_measure = xml_unit.find("{http://www.xbrl.org/2003/instance}measure")
    return {"id": id, "measure": xml_measure.text}


with open(test_taxonomy) as instance_file:
    contexts = {}
    units = {}
    facts = []

    tree = etree.fromstring(bytes(instance_file.read(), encoding="utf-8"))

    # First we need to gather all of the context's used in the instance
    for xml_context in tree.findall("{http://www.xbrl.org/2003/instance}context"):
        context = get_context(xml_context=xml_context)
        contexts[context.id] = context

    print(f"found {len(contexts)} contexts")

    # Second, we should get the units
    for xml_unit in tree.findall("{http://www.xbrl.org/2003/instance}unit"):
        unit = get_unit(xml_unit=xml_unit)

        units[unit.get("id")] = unit.get("measure")
    # Todo: the specification allows for more information, but I do not think we support that
    print(f"found {len(units)} units")

    # Now, get the facts. This probably needs more tweaking because what counts as a correct fact?
    # For now, I'll look at the declared namespaces and go though all custom (non XBRL) namespaces.

    for prefix, namespace in tree.nsmap.items():

        # We do not need to check namespaces we know do not contain Elements.
        if namespace not in [
            "http://www.xbrl.org/2003/linkbase",
            "http://www.xbrl.org/2003/instance",
            "http://www.w3.org/1999/xlink",
            "http://www.xbrl.org/2003/iso4217",
            "http://xbrl.org/2006/xbrldi",
        ]:
            # We do still look through namespaces with DomainMembers and Dimensions
            # But if so, the instance would be invalid anyway.

            ns_facts = []
            for xml_fact in tree.findall(f"{prefix}:*", namespaces=tree.nsmap):

                # Unit is optional
                if xml_fact.get("unitRef"):
                    unit = units[xml_fact.get("unitRef")]
                else:
                    unit = None

                qname = etree.QName(xml_fact.tag)
                ns_facts.append(
                    Fact(
                        concept_tag=xml_fact.tag,
                        concept_name=qname.localname,
                        concept_prefix=xml_fact.prefix,
                        context=contexts[xml_fact.get("contextRef")],
                        decimals=xml_fact.get("decimals"),
                        unit=unit,
                        value=xml_fact.text,
                    )
                )

            print(f"Found {len(ns_facts)} facts for {prefix}")
            facts.extend(ns_facts)

    print(f"Found {len(facts)} facts for instance")

# Get the DTS
dts_file = ZipFile(
    "/home/xiffy/develop/kenniscentrumxbrl/tmp/kvk-middelgroot2020.zip", "r"
)
for file in dts_file.namelist():
    dts = pickle.loads(dts_file.read(file))

### Enrich the contexts
for context in contexts.values():

    for scenario_members in context.scenario_members:

        # Add dimensional objects to scenario's
        dimension = scenario_members.dimension.replace(":", "_")
        if dimension in dts.concepts:
            scenario_members.dimension_obj = dts.concepts[dimension]
        else:
            print(f"Dimension for scenario not found in DTS!")

        # Add member objects to scenario's
        member = f"{scenario_members.member_prefix}_{scenario_members.member_name}"
        if member in dts.concepts:
            scenario_members.member_obj = dts.concepts[member]
        else:
            print(f"Member for scenario not found in DTS!")

instances = []
series_dict = {"identifier": None}
transform_to_series = set([])
## Enrich the facts
for fact in facts:
    if not series_dict["identifier"]:
        series_dict["identifier"] = fact.context.identifier
    # Add concepts to facts
    if f"{fact.concept_prefix}_{fact.concept_name}" in dts.concepts:
        fact.concept_obj = dts.concepts[f"{fact.concept_prefix}_{fact.concept_name}"]
    else:
        print(f"Concept for fact {fact.concept_name} not found in DTS!")
    key = f"{fact.concept_prefix}:{fact.concept_name}"
    if fact.context.period_start:
        key += f":{fact.context.period_start}"
    if fact.context.period_end:
        key += f":{fact.context.period_end}"
    if fact.context.period_instant:
        key += f":{fact.context.period_instant}"
    for scenario_member in fact.context.scenario_members:
        key += f":{scenario_member.dimension}:{scenario_member.member_name}"
    if (
        key in series_dict
    ):  # Aha, a multiple reporting dimension like Boardmember / Boardmember Address
        print(f"{fact.context.id} Adding to: {key}")
        series_dict[key].append(fact.value)
        transform_to_series.add(key)
    else:
        series_dict[key] = [fact.value]
print("Done")
for key in transform_to_series:
    series_dict[key] = ",".join(series_dict[key])
    print(f"{key}\n {series_dict[key]}")
#  helaas, pandas vindt het niet leuk een series op een datapunt te ontvangen.
instances.append(series_dict)
second_instance = series_dict.copy()
second_instance["identifier"] = int(second_instance["identifier"]) + 1
del second_instance[
    "jenv-bw2-i:ShareCapitalNumberSharesGranting:2020-01-01:2020-12-31:jenv-bw2-dim:BasisOfPreparationAxis:CommercialMember:jenv-bw2-dim:FinancialStatementsTypeAxis:SeparateMember:jenv-bw2-dim:ShareCapitalAxis:ShareCapitalNumberOfSharesTypedMember"
]
instances.append(second_instance)
pd_df = pd.DataFrame.from_dict(instances)
print(pd_df)
