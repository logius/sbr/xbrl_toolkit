import asyncio

from PySide2.QtWidgets import QGroupBox, QVBoxLayout, QPushButton

from discover_xbrl.sbrnl_modules.instance.queries.dataframes import write_df
from discover_xbrl.sbrnl_modules.instance.enrich_instances import enrich_instances
from discover_xbrl.sbrnl_modules.instance.formula import test_assertions
from discover_xbrl.sbrnl_modules.instance.queries.dicts import (
    get_dict_elements,
    get_dict_dimmem,
    get_dict_escaped_chars,
)
from discover_xbrl.sbrnl_modules.instance.read_instances import read_instances
from discover_xbrl.sbrnl_modules.instance.retrieval_functions import (
    get_instances,
)
from discover_xbrl.sbrnl_modules.instance.unused_instances import (
    test_elements_instance_count,
    get_list_expected_elements,
    test_element_linkrole_instance_count,
)


class instancesActionsBox(QGroupBox):
    def __init__(self, parent=None):
        super(instancesActionsBox, self).__init__(parent)
        self.title = "Instances toolbox"
        self.parent = parent

        # Create widgets
        findContextsButton = QPushButton("Find contexts")
        generateDataframesButton = QPushButton("generate dataframes")
        escapedCharactersButton = QPushButton("escaped characters")
        calltestElementUsageCountButton = QPushButton("Element usage count")
        calltestformulasButton = QPushButton("Test formulas")

        # Connect functions to buttons
        findContextsButton.clicked.connect(self.callfindContexts)
        generateDataframesButton.clicked.connect(self.callGenerateDataframes)
        escapedCharactersButton.clicked.connect(self.callCountEscapedCharacters)
        calltestElementUsageCountButton.clicked.connect(self.calltestElementUsageCount)
        calltestformulasButton.clicked.connect(self.callTestFormulas)

        # Set widgets to layout
        layout = QVBoxLayout()
        layout.addWidget(findContextsButton)
        layout.addWidget(generateDataframesButton)
        layout.addWidget(calltestElementUsageCountButton)
        layout.addWidget(escapedCharactersButton)
        layout.addWidget(calltestformulasButton)

        # Set layout to class attribute
        self.setDisabled(False)
        self.setLayout(layout)

    def callfindContexts(self):
        """
        Attempt to find out how the contexts reported on relate to linkroles in the DTS

        :return:
        """
        # dts = self.parent.discovered_dts_list[0]

        import datetime

        print(f"{datetime.datetime.now()} - reading lxml instances")
        # Read instances
        lxml_instances = read_instances()
        print(f"{datetime.datetime.now()} - done reading lxml instances")

        print(f"{datetime.datetime.now()} - creating instance objects")
        # Get instance objects
        instances = get_instances(lxml_instances)
        print(f"{datetime.datetime.now()} - done creating instance objects")

        print(f"{datetime.datetime.now()} - enriching instance objects")
        # Enrich contexts with information out of the DTS
        instances = asyncio.run(enrich_instances(instances=instances))
        print(f"{datetime.datetime.now()} - done enriching instance objects")

        """ The functions above can be put into a generic 'instance' parser.
            The stuff below is specific to us creating an overview of elements/lr occurences"""
        print(f"{datetime.datetime.now()} - creating overview")
        test_element_linkrole_instance_count(instances=instances)
        print(f"{datetime.datetime.now()} - done creating overview")

    def calltestElementUsageCount(self):

        # Get instances
        lxml_instances = read_instances()

        # Get instance objects
        instances = get_instances(lxml_instances)

        # Get dicts
        dict_of_elements = get_dict_elements(instances)

        dts = self.parent.discovered_dts_list[0]
        # Feed dict in test
        test_elements_instance_count(
            expected_elements=get_list_expected_elements(dts=dts),
            list_of_dicts=dict_of_elements,
        )

    def callGenerateDataframes(self):

        # Get instances
        lxml_instances = read_instances()

        # Get instance objects
        instances = get_instances(lxml_instances)

        # Get dicts
        dict_of_elements = get_dict_dimmem(instances)

        # Feed dict in test
        write_df(dict_of_elements, f"/tmp/dimmem.csv")

    def callCountEscapedCharacters(self):

        # Get instances
        lxml_instances = read_instances()
        # Get instance objects
        instances = get_instances(lxml_instances)

        elements = get_dict_escaped_chars(instances)
        for elem_name, chars in elements.items():
            for char_name, counts in chars.items():
                print(f"{elem_name}, '{char_name}', {counts['count_char']}")

        print("done")

    def callTestFormulas(self):

        # dts = self.parent.discovered_dts_list[0]

        from datetime import datetime

        start_time = datetime.now()

        print(f"{datetime.now() - start_time} - reading lxml instances")
        # Read instances
        lxml_instances = read_instances()
        print(f"{datetime.now() - start_time} - done reading lxml instances")

        print(f"{datetime.now() - start_time} - creating instance objects")
        # Get instance objects
        instances = get_instances(lxml_instances)
        print(f"{datetime.now() - start_time} - done creating instance objects")

        print(f"{datetime.now() - start_time} - enriching instance objects")
        # Enrich contexts with information out of the DTS
        instances = asyncio.run(enrich_instances(instances=instances))
        print(f"{datetime.now() - start_time} - done enriching instance objects")

        print(f"{datetime.now() - start_time} - Testing Assertions")
        test_assertions(dts=dts, instances=instances)
        print(f"{datetime.now() - start_time} - Done testing Assertions")

        instances_ea_fails = [
            instance for instance in instances if instance.failed_assertions
        ]

        error_messages = {}

        for instance in instances_ea_fails:
            for ea in instance.failed_assertions:
                key = f"{ea.id} - {ea.xlink_label.text}"
                if key not in error_messages.keys():
                    error_messages[key] = 1
                else:
                    error_messages[key] = error_messages[key] + 1

        print(error_messages)
        print("done")
