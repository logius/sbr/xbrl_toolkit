class Context:
    def __init__(
        self,
        id,
        identifier,
        identifier_scheme,
        period_start=None,
        period_end=None,
        period_instant=None,
        period_forever=False,
        scenario_members=None,
    ):

        self.id = id
        self.identifier = identifier
        self.identifier_scheme = identifier_scheme

        self.period_start = period_start
        self.period_end = period_end

        if not (period_start or period_end) and period_instant and period_forever:
            print("Context has no valid period defined!")

        self.period_instant = period_instant
        self.period_forever = period_forever

        if scenario_members:
            self.scenario_members = scenario_members
        else:
            self.scenario_members = []

    def __hash__(self):
        """
        Returns a hash based on everything that should be comparable between instances.

        :return:
        """
        return hash(self.hashable_string)

    def __repr__(self):
        return f"Context '{self.id}'"

    @property
    def hashable_string(self):
        """
        Creates a string that can either be used as an identifier, or hashed into an unreadable unique identifier.

        :return:
        """

        # Get a period string to hash
        hash_string = ""
        if self.period_start and self.period_end:
            hash_string += self.period_start + "_" + self.period_end
        elif self.period_forever:
            hash_string += str(self.period_forever)
        elif self.period_instant:
            hash_string += self.period_instant

        # Get a string to represent the scenario dimensions + members
        if self.scenario_members:
            for scenario in sorted(
                self.scenario_members, key=lambda x: x.dimension_name
            ):
                if scenario.scenario_type == "typed":
                    # If the dimension is typed, we should only use the dimension name and not the member
                    # As the member will likely be specific to the report
                    hash_string += f"{scenario.dimension_name}"
                else:
                    hash_string += f"{scenario.dimension_name}:{scenario.member_name}"

            # Add the member value if it is typed.
            if scenario.scenario_type == "typed":
                # Add a flag to indicate the context has a typed dimension.
                # This means the facts reported in this context may not be comparable.

                hash_string += scenario.member_name

                ## Uncomment to add the value of the dimension. This is identifiable information in cols
                # member_value = re.sub('[^A-Za-z0-9]+', '', scenario.member_value)
                # hash_string += member_value

        return hash_string

    def __eq__(self, other):
        if not isinstance(other, Context):
            # don't attempt to compare against unrelated types
            return NotImplemented

        return self.__hash__() == other.__hash__()


# The scenario is an imporant part of the context. By checking for the occurance of dimensions and members
# we can compare facts between instances.
class Scenario:
    def __init__(
        self,
        scenario_type,
        dimension_name,
        dimension_nsprefix,
        dimension_namespace,
        member_prefix,
        member_namespace,
        member_name,
        member_value=None,
        dimension_obj=None,
        member_obj=None,
    ):
        self.scenario_type = scenario_type
        self.dimension_name = dimension_name
        self.dimension_nsprefix = dimension_nsprefix
        self.dimension_namespace = dimension_namespace
        self.dimension_obj = dimension_obj
        self.member_prefix = member_prefix
        self.member_namespace = member_namespace
        self.member_name = member_name
        self.member_obj = member_obj

        # for typed members, we expect a value as well
        if scenario_type == "typed" and member_value is None:
            print("Value expected for typed member")
        self.member_value = member_value

    def __str__(self):
        return f"{self.dimension} -> {self.member_name}"


class Fact:
    def __init__(
        self,
        concept_tag,
        concept_name,
        concept_prefix,
        concept_namespace,
        context,
        decimals,
        unit,
        value,
        concept_obj=None,
        linkrole=None,
    ):
        self.concept_tag = concept_tag
        self.concept_name = concept_name
        self.concept_prefix = concept_prefix
        # todo: Concept tag can be matched with namespace + id for concepts in DTS

        self.concept_namespace = concept_namespace
        self.concept_obj = concept_obj
        self.context = context
        self.decimals = decimals
        self.unit = unit
        self.value = value
        self.linkrole = linkrole

    def __repr__(self):
        return f"{self.concept_name} in {self.context}"


class Instance:
    def __init__(
        self,
        contexts,
        facts,
        units,
        taxonomy_ep,
        lxml_object=None,
        parameters=None,
        failed_assertions=None,
    ):

        self.contexts = contexts
        self.facts = facts
        self.units = units
        self.taxonomy_ep = taxonomy_ep
        self.lxml_object = lxml_object
        self.parameters = parameters

        # Used when validating instances. Holders assertions which the instance did not pass
        if failed_assertions:
            self.failed_assertions = failed_assertions
        else:
            self.failed_assertions = []
