"""
File that loads all relevant module parts.
"""
from .widget import instancesActionsBox


def load_module(module_object={}):

    module_object["name"] = "SBRWonen"
    module_object["type"] = "export"
    module_object["gui"] = instancesActionsBox
    module_object[
        "description"
    ] = """
    Analytical tools for instances.
    """
    return module_object
