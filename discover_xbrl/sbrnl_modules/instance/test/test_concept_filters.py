from unittest import TestCase

from discover_xbrl.sbrnl_modules.instance.test.classes import (
    fact_a,
    fact_b,
    fact_c,
    concept_a_obj,
    concept_b_obj,
)
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.formulas.classes.filter_concept import (
    FilterConceptName,
    FilterConceptPeriod,
    FilterConceptBalance,
)
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.formulas.classes.filter_concept_relation import (
    FilterConceptRelation,
)


class TestConceptFilters(TestCase):
    def test_concept_name(self):
        # Create a set of Facts and Concepts reported in that fact

        filter_a = FilterConceptName(
            id="filter_a", xlink_label="get_a", qname=concept_a_obj
        )

        # Get the outcome
        outcome = filter_a.test_facts(facts=[fact_a, fact_b, fact_c])

        # We expect to get back fact_a and fact_b
        self.assertEqual(outcome, [fact_a])

        # but not any other combination
        self.assertNotEqual(outcome, [fact_a, fact_c])
        self.assertNotEqual(outcome, [fact_a, fact_b])
        self.assertNotEqual(outcome, [fact_b, fact_c])
        self.assertNotEqual(outcome, [fact_a, fact_b, fact_c])

    def test_concept_period(self):
        # Create a set of Facts and Concepts reported in that fact

        filter_instant = FilterConceptPeriod(
            id="filter_a", label="get_a", period_type="instant"
        )
        filter_duration = FilterConceptPeriod(
            id="filter_a", label="get_a", period_type="duration"
        )

        # Get the outcome
        outcome_instant = filter_instant.test_facts(facts=[fact_a, fact_b, fact_c])
        outcome_duration = filter_duration.test_facts(facts=[fact_a, fact_b, fact_c])

        # We expect to get back fact_a and fact_b
        self.assertEqual(outcome_instant, [fact_b])
        self.assertEqual(outcome_duration, [fact_a, fact_c])

        # but not any other combination
        self.assertNotEqual(outcome_instant, [fact_a, fact_c])
        self.assertNotEqual(outcome_instant, [fact_a])
        self.assertNotEqual(outcome_duration, [fact_b, fact_c])
        self.assertNotEqual(outcome_duration, [fact_a, fact_b, fact_c])

    def test_concept_balance(self):
        # Create a set of Facts and Concepts reported in that fact

        filter_credit = FilterConceptBalance(
            id="filter_a", xlink_label="get_a", balance_type="credit"
        )
        filter_debit = FilterConceptBalance(
            id="filter_a", xlink_label="get_a", balance_type="debit"
        )

        # Get the outcome
        outcome_credit = filter_credit.test_facts(facts=[fact_a, fact_b, fact_c])
        outcome_debit = filter_debit.test_facts(facts=[fact_a, fact_b, fact_c])

        # We expect to get back fact_a and fact_b
        self.assertEqual(outcome_credit, [fact_a, fact_b])
        self.assertEqual(outcome_debit, [fact_c])

        # but not any other combination
        self.assertNotEqual(outcome_credit, [fact_a, fact_c])
        self.assertNotEqual(outcome_credit, [fact_a])
        self.assertNotEqual(outcome_debit, [fact_b, fact_c])
        self.assertNotEqual(outcome_debit, [fact_a, fact_b, fact_c])
