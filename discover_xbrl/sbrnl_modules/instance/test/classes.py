from discover_xbrl.sbrnl_modules.instance.classes import Fact
from discover_xbrl.sbrnl_xbrl_parser.schema.classes import Concept


concept_a_obj = Concept(
    id="concept_a",
    name="prefix:concept_a",
    nillable=False,
    periodType="duration",
    balance="credit",
)
fact_a = Fact(
    concept_tag="",
    concept_name="concept_a",
    concept_prefix="prefix",
    concept_obj=concept_a_obj,
    concept_namespace="http://namespace/of/concept",
    context=None,
    decimals="INF",
    unit=None,
    value=134,
)

concept_b_obj = Concept(
    id="concept_b",
    name="prefix:concept_b",
    nillable=False,
    periodType="instant",
    balance="credit",
)
fact_b = Fact(
    concept_tag="",
    concept_name="concept_b",
    concept_prefix="prefix",
    concept_obj=concept_b_obj,
    concept_namespace="http://namespace/of/concept",
    context=None,
    decimals="INF",
    unit=None,
    value=133,
)

concept_c_obj = Concept(
    id="concept_c",
    name="prefix:concept_c",
    nillable=False,
    periodType="duration",
    balance="debit",
)
fact_c = Fact(
    concept_tag="",
    concept_name="concept_c",
    concept_prefix="prefix",
    concept_obj=concept_c_obj,
    concept_namespace="http://namespace/of/concept",
    context=None,
    decimals="INF",
    unit=None,
    value=112,
)
