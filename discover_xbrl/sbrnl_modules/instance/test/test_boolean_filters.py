from unittest import TestCase

from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.formulas.classes.filter_boolean import (
    FilterBooleanOr,
    FilterBooleanAnd,
)
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.formulas.classes.filter_concept import (
    FilterConceptName,
)

from discover_xbrl.sbrnl_modules.instance.test.classes import (
    fact_a,
    fact_b,
    fact_c,
    concept_a_obj,
    concept_b_obj,
    concept_c_obj,
)


class TestBooleanFilters(TestCase):
    def test_boolean_or(self):

        # The subfilters are set for concept_a and concept_b. These are the concepts we expect to get back
        subfilters = [
            FilterConceptName(
                qname=concept_a_obj,
                qname_expression=None,
                xlink_label="concept_a",
            ),
            FilterConceptName(
                qname=concept_b_obj,
                qname_expression=None,
                xlink_label="concept_b",
            ),
        ]
        filter = FilterBooleanOr(
            id="filter_a", xlink_label="get_a_or_b", subfilters=subfilters
        )

        # Get the outcome
        outcome = filter.test_facts(facts=[fact_a, fact_b, fact_c])

        # We expect to get back fact_a and fact_b
        self.assertEqual(outcome, [fact_a, fact_b])

        # but not any other combination
        self.assertNotEqual(outcome, [fact_a, fact_c])
        self.assertNotEqual(outcome, [fact_b, fact_c])
        self.assertNotEqual(outcome, [fact_a, fact_b, fact_c])

    def test_boolean_and(self):
        # Create a set of Facts and Concepts reported in that fact

        # The subfilters are set for concept_a and concept_b. These are the concepts we expect to get back
        subfilters = [
            FilterConceptName(
                qname=concept_a_obj,
                qname_expression=None,
                xlink_label="concept_a",
            ),
            FilterConceptName(
                qname=concept_b_obj,
                qname_expression=None,
                xlink_label="concept_b",
            ),
        ]
        filter = FilterBooleanAnd(
            id="filter_a", xlink_label="get_a_and_b", subfilters=subfilters
        )

        # We expect to get back fact_a and fact_b
        self.assertEqual(
            filter.test_facts(facts=[fact_a, fact_b, fact_c]), [fact_a, fact_b]
        )

        # But if one cannot be found, there is an issue
        self.assertNotEqual(filter.test_facts(facts=[fact_a, fact_c]), [fact_a, fact_b])

        # Instead, we expect the filter to come back empty (False) if not all concepts are found in facts
        self.assertEqual(filter.test_facts(facts=[fact_b, fact_c]), [])
