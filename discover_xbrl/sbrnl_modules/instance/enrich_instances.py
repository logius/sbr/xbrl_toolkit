"""
Functions to enrich instances with information out of DTS files.

Obviously rely on some information out of DTS files or - in the future - API calls to our server.
"""
import asyncio
import json
import requests
from discover_xbrl.conf.conf import Config

conf = Config()

protocol = conf.api.get("protocol")
ip = conf.api.get("host")
port = conf.api.get("port")


class Concept:
    """
    Concept element.
    The API gives more information than is used at the moment.
    If more information is needed, just add it here.
    """

    def __init__(self, name, namespace, prefix, labels=None):
        self.name = name
        self.namespace = namespace
        self.prefix = prefix
        self.labels = labels

    @property
    def fullname(self):
        return f"{self.namespace}:{self.name}"


def get_dict_namespace_name_concepts(dts):

    return_concepts = {}

    query = f"{protocol}://{ip}:{port}/dts/{dts}/concepts"
    response = requests.get(query)

    if response.status_code == 200:
        concepts = json.loads(response.content)
    elif response.status_code == 500:
        raise Exception(f"Could not connect to API. Query: {query}")

    for dts_concept in concepts:
        return_concepts[f"{dts_concept['namespace']}:{dts_concept['name']}"] = Concept(
            name=dts_concept["name"],
            namespace=dts_concept["namespace"],
            prefix=dts_concept["ns_prefix"],
            labels=dts_concept["labels"],
        )

    return return_concepts


async def enrich_context(concept_dict, context):

    for scenario_members in context.scenario_members:

        # Add dimensional objects to scenario's

        scenario_members.dimension_obj = concept_dict.get(
            f"{scenario_members.dimension_namespace}:{scenario_members.dimension_name}",
            None,
        )

        if not scenario_members.dimension_obj:
            print(f"Dimension for scenario not found in DTS!")

        # Add member objects to scenario's

        scenario_members.member_obj = concept_dict.get(
            f"{scenario_members.member_namespace}:{scenario_members.member_name}", None
        )

        if not scenario_members.member_obj:
            print(f"Dimension for scenario not found in DTS!")
        # if member in dts.concepts:
        #     scenario_members.member_obj = dts.concepts[member]
        # else:
        #     print(f"Member for scenario not found in DTS!")
    return context


async def enrich_fact(concept_dict, fact):
    """
    Enrich a fact and return it

    :param concept_dict:
    :param fact:
    :return:
    """
    # Add concepts to facts

    fact.concept = concept_dict.get(
        f"{fact.concept_namespace}:{fact.concept_name}", None
    )

    if not fact.concept:
        print(
            f"Concept for '{fact.concept_name}' in namespace '{fact.concept_namespace}' not found in DTS!"
        )

    return fact


async def enrich_instances(instances):
    """
    Enrich instances by:
    -   adding linkroles to facts
    -   (etc?)

    :param dts:
    :param instances:
    :return:
    """
    print(f"Adding DTS data to {len(instances)} instances")
    # dts_dict = get_dict_namespace_name_concepts(dts=instances[0].taxonomy_ep)
    # if not len(dts_dict):
    #    print("Could not get concepts of DTS")
    concept_dict = None
    current_dts = None
    print("Starting to add information to facts and contexts")
    for instance in instances:
        if not instance.taxonomy_ep:
            continue
        if instance.taxonomy_ep != current_dts:
            if current_dts:  # process last batch
                print(f"Done adding information to facts and context {current_dts}")
                await proces_per_ep(
                    current_dts,
                    [inst for inst in instances if inst.taxonomy_ep == current_dts],
                )

            concept_dict = get_dict_namespace_name_concepts(dts=instance.taxonomy_ep)
            current_dts = instance.taxonomy_ep
            if not len(concept_dict):
                print("Could not get concepts of DTS")
        tasks_facts = [
            enrich_fact(concept_dict=concept_dict, fact=fact) for fact in instance.facts
        ]
        instance.facts = await asyncio.gather(*tasks_facts)

        tasks_contexts = [
            enrich_context(concept_dict=concept_dict, context=context)
            for context in instance.contexts.values()
        ]
        instance.contexts = await asyncio.gather(*tasks_contexts)
    await proces_per_ep(
        current_dts, [inst for inst in instances if inst.taxonomy_ep == current_dts]
    )
    print("Done adding information to facts and context")
    return instances

    # Get an overview of distinct contexts with their dimensions and members


async def proces_per_ep(dts_name, instances):
    print(f"Starting to add linkrole to facts {dts_name}")

    # Get an overview of concept / context combinations
    context_concepts = {}
    for instance in instances:
        for fact in instance.facts:
            if fact.context not in context_concepts.keys():
                context_concepts[fact.context] = {}
                context_concepts[fact.context][f"{fact.concept.fullname}"] = []

            else:
                if fact.concept.fullname not in context_concepts[fact.context].keys():
                    context_concepts[fact.context][fact.concept.fullname] = []
    print(f"Got {len(context_concepts)} unique contexts in {len(instances)} instances")

    # Get which DTS we need to ask for from the API. This is a bit of a shortcut, because we assume all instances are
    # using the same DTS.
    # Ah shoot, here i will fail
    query = f"{protocol}://{ip}:{port}/dts/{dts_name}/linkroles?full=1"
    response = requests.get(query)
    directly_referenced_linkbases = json.loads(response.content)

    # Go through all combinations, and add the linkroles to them
    for context, concepts in context_concepts.items():
        for concept, lr_list in concepts.items():

            linkroles = await match_linkroles(
                linkbases=directly_referenced_linkbases,
                fact_context=context,
                fact_concept=concept,
            )
            context_concepts[context][concept] = linkroles

    # Go through instances/facts once again, but now add the linkroles to facts based on the list we created earlier
    for instance in instances:
        for fact in instance.facts:
            if fact.context in context_concepts.keys():
                if fact.concept.fullname in context_concepts[fact.context].keys():
                    fact.linkrole = context_concepts[fact.context][
                        fact.concept.fullname
                    ]
                else:
                    print(f"Concept not in dict {fact.concept.fullname}")
            else:
                print("Context not in dict")

    print("Done adding linkrole to facts")

    print("Done adding DTS data")
    return instances


async def match_linkroles_per_instance(list_of_elr, instance):

    for fact in instance.facts:

        linkroles = await match_linkroles(
            linkbases=list_of_elr,
            fact_context=fact.context,
            fact_concept=fact.concept_obj,
        )

        fact.linkrole = linkroles
    return instance


def get_distinct_contexts(instances):
    """
    Try and connect contexts to the DTS

    :param dts:
    :param list_of_dicts:
    :return:
    """

    # Get a distinct list of contexts over all instances

    contexts_obj = {}

    for instance in instances:

        for context in instance.contexts.values():

            if context.hashable_string not in contexts_obj.keys():

                dim_mem_objs = []

                # Go through scenarios, sort on dimension.
                for scenario in sorted(
                    context.scenario_members, key=lambda x: x.dimension_name
                ):

                    dim_mem_objs.append(
                        {"dim": scenario.dimension_obj, "mem": scenario.member_obj}
                    )

                contexts_obj[context.hashable_string] = dim_mem_objs

    return contexts_obj


def concept_in_lineitems(concept, lineitems):

    for lineitem_tuple in lineitems:
        if (
            concept
            == f"{lineitem_tuple['concept']['namespace']}:{lineitem_tuple['concept']['name']}"
        ):
            return True
    return False


def scenario_matches(scenario_members, hypercube_dims):
    dimensions_matched = 0

    for c_dimmem in scenario_members:
        if dimension_matches(scenario_dimmem=c_dimmem, hypercube_dims=hypercube_dims):

            # Found dim.
            dimensions_matched = dimensions_matched + 1
            # print(f"Found {dimensions_matched} dimensions of {len(scenario_members)}")

    if dimensions_matched == len(scenario_members):
        # print(
        #     f"Ended up finding {dimensions_matched} of the expected {len(scenario_members)}"
        # )
        return True

    # print(
    #     f"Ended up finding {dimensions_matched} of the expected {len(scenario_members)}"
    # )
    return False


# Found hypercube which is used for reporting this fact


def dimension_matches(scenario_dimmem, hypercube_dims):
    """

    Checks if the combination of dimension/member in scenario is found in the hypercube dimensions.

    :param scenario_dimmem:
    :param hypercube_dims:
    :return:
    """
    # print(
    #     f"Checking for combination: Dim '{scenario_dimmem.dimension_obj.name}' -  Mem'{scenario_dimmem.member_obj.name}'"
    # )
    for hc_dim in hypercube_dims:
        if (
            f"{scenario_dimmem.dimension_obj.fullname}"
            == f"{hc_dim['concept']['namespace']}:{hc_dim['concept']['name']}"
        ):
            # print(f"Found correct dimension: {hc_dim.concept.name}")
            return member_matches(
                scenario_dimmem.member_obj.fullname, hc_dim["members"]
            )
    return False


def member_matches(obj_fullname, members):
    for member in members:
        if (
            obj_fullname
            == f"{member['concept']['namespace']}:{member['concept']['name']}"
        ):
            return True
        if member.get("members"):
            if member_matches(obj_fullname, member["members"]):
                return True
    return False


async def match_linkroles(linkbases, fact_context, fact_concept):
    linkroles_found = []
    # First get a dictionary with expected elements.
    alt_linkbases = linkbases.copy()

    for linkrole in linkbases:
        if linkrole["hypercube"] is not None:
            if check_linkrole(
                linkrole=linkrole, fact_context=fact_context, fact_concept=fact_concept
            ):
                if linkrole not in linkroles_found:
                    linkroles_found.append(linkrole)
        elif len(linkrole["tables"]):
            pass  # todo: these are 'half'-ELR's, their hypercube lives with the 'current' variant
            # print(f"Bugger {linkrole['role_uri']}, pls find my brethren")

    return linkroles_found


def check_linkrole(linkrole, fact_concept, fact_context):

    if linkrole["hypercube"] is not None:
        lineitems = linkrole["hypercube"]["line_items"]
        hc_dims = linkrole["hypercube"]["dimensions"]
        """
        Here we want to match Dim/Member combinations to the contexts that are found.
        """

        # Check if concept shows up in lineitems
        if concept_in_lineitems(fact_concept, lineitems):

            # for c_hash, c_dimmems in contexts_obj.items():

            # We are only interested in hypercubes that have the same amount of dimensions.
            if len(fact_context.scenario_members) == len(hc_dims):
                # print(
                #     f"Checking linkrole '{linkrole.roleURI}', where lineitem is found and amount of dimensions matches"
                # )
                if scenario_matches(
                    scenario_members=fact_context.scenario_members,
                    hypercube_dims=hc_dims,
                ):
                    return True
    return False
