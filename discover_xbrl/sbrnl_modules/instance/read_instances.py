from pathlib import Path
from lxml import etree

import os
from discover_xbrl.conf.conf import Config
from discover_xbrl.sbrnl_modules.instance.retrieval_functions import (
    get_instance_entrypoint,
)

conf = Config()


def read_instance(file, file_encoding="UTF-8", xml_encoding="UTF-8"):

    with open(file=file, encoding=file_encoding) as instance_file:
        # We will try to read the file. This can fail if the file does not use the correct encoding.
        try:
            file_contents = instance_file.read()

        except UnicodeDecodeError as e:
            # Exceptions on unicode errors seem to have two different reasons: 'invalid continuation byte' and 'invalid start byte'.
            # These filings likely do not conform to filing rules (FR-NL-1.01 or FR-NL-1.05) although by proxy. (UTF header can be present without the file being the correct encoding.)

            return {"result": e.reason}

        # While we expect most errors to be an UnicodeDecodeError, we also want to catch generic errors.
        # Right now, we could actually just only do this, as we do not keep a separate list for unicode errors.
        except Exception as e:
            return {"result": e.reason}

        # If we can read the file, we will append the name to the list of successfully read files and continue on
        else:
            return {"result": "success", "contents": file_contents}


def parse_xml(file_contents, encoding="UTF-8"):
    # This time we try to parse the contents as XML
    try:
        tree = etree.fromstring(bytes(file_contents, encoding=encoding))
    except Exception as e:
        if hasattr(e, "reason"):
            return {"result": e.reason}
        else:
            return {"result": "failed"}

    else:
        # Add the tree to the list of instances
        return {"result": "success", "contents": tree}


def read_instances():

    instance_dir = conf.instances.get("instance_dir")
    read_files = (
        []
    )  # Keep track of the files that have been read correctly (could still fail when parsing as XML)
    unreadable_files = (
        []
    )  # Keep track of the amount of files that could not be read, and the reason why

    # Somehow the KVK instances are XML, not XBRL
    extension_types = ("*.xml", "*.xbrl")
    pathlist = []
    for extension_type in extension_types:
        pathlist.extend(Path(instance_dir).rglob(extension_type))

    for file in pathlist:
        # todo: put this in a recursive function with proper error handling, also for retries
        instance = read_instance(file=file)
        if instance["result"] == "success":
            # File has been read successfully

            # Add filename for later use
            instance["filename"] = file.name
            read_files.append(instance)

        elif instance["result"] in ["invalid start byte", "invalid continuation byte"]:
            # Try again with different encoding
            instance = read_instance(file=file, file_encoding="iso8859-15")
            instance["filename"] = file.name
            if instance["result"] == "success":
                # File has been read successfully

                read_files.append(instance)
            else:
                unreadable_files.append(instance)

        else:
            print("Unknown error while reading file")

    print(f"Read {len(read_files)} files")
    print(f"Couldn't read {len(unreadable_files)} files")

    # Now parse XML from the files
    lxml_tree_instances = []
    unparsable_files = []  # Keep track of the files that could not be parsed with LXML

    for read_file in read_files:
        tree = parse_xml(file_contents=read_file["contents"], encoding="UTF-8")

        if tree["result"] == "success":
            # File has been read successfully
            lxml_tree_instances.append(tree["contents"])

        else:
            unparsable_files.append(tree["result"])

    print(f"Couldn't parse {len(unparsable_files)} files")
    return lxml_tree_instances


def order_instances():
    """
    Used to order instances into directories per entrypoint.

    :return:
    """

    found_eps = {}
    read_files = (
        []
    )  # Keep track of the files that have been read correctly (could still fail when parsing as XML)

    # Somehow the KVK instances are XML, not XBRL
    extension_types = ("*.xml", "*.xbrl", "*.XBRL")
    pathlist = []
    for extension_type in extension_types:
        pathlist.extend(Path(conf.instances.get("instance_dir")).rglob(extension_type))

    for file in pathlist:
        # todo: put this in a recursive function with proper error handling, also for retries
        instance = read_instance(file=file)
        if instance["result"] == "success":
            # File has been read successfully

            # Add filename for later use
            instance["filename"] = file.name
            read_files.append(instance)

        elif instance["result"] in ["invalid start byte", "invalid continuation byte"]:
            # Try again with different encoding
            instance = read_instance(file=file, file_encoding="iso8859-15")
            instance["filename"] = file.name

        if instance["result"] == "success":
            # File has been read successfully

            tree = parse_xml(file_contents=instance["contents"], encoding="UTF-8")
            entrypoint = get_instance_entrypoint(tree["contents"])

            if not entrypoint:
                print(f"No ep? {file}")
                continue
            if entrypoint not in found_eps.keys():
                found_eps[entrypoint] = 1
            else:
                found_eps[entrypoint] = found_eps[entrypoint] + 1

            new_dir = os.path.join(str(file.parent), entrypoint.replace(".xsd", ""))
            os.makedirs(new_dir, exist_ok=True)
            with open(f"{new_dir}/{file.name}", "w") as output_file:
                output_file.write(instance["contents"])
            os.remove(file)

    print("Done")
    for ep, count in found_eps.items():
        print(f"{ep}: {count}")
