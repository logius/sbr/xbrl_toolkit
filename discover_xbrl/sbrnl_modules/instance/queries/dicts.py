import re


def get_dict_elements(instances):
    """
    Get a dictionary with elements found in the reports

    :return:
    """
    dict_of_elements = []

    for instance in instances:
        element_list = []

        for fact in instance.facts:

            element_list.append(fact.concept_name)

        dict_of_elements.append(element_list)

    return dict_of_elements


def get_dict_dimmem(instances):
    """
    Get a dictionary with elements found in the reports

    :return:
    """
    dict_of_instances = []

    for instance in instances:
        instance_dict = {}
        for fact in instance.facts:
            # instance_dict[f"{fact.context.__hash__()}_{fact.concept_name}"] = fact.value
            instance_dict[
                f"{fact.context.hashable_string}_{fact.concept_name}"
            ] = fact.value

        dict_of_instances.append(instance_dict)

    return dict_of_instances


def get_dict_elements_linkroles(instances):
    """
    Get a dictionary with elements found in the reports

    :return:
    """
    dict_of_elements = []

    for instance in instances:
        element_list = []

        for fact in instance.facts:

            element_list.append(
                {"concept": fact.concept_obj, "linkrole": fact.linkrole}
            )

        dict_of_elements.append(element_list)

    return dict_of_elements


def get_dict_escaped_chars(instances):
    # List of elements found
    print(
        f"Creating an overview of used excaped xhtml characters in fact values for {len(instances)} instances"
    )
    elements = {}

    for instance in instances:

        for fact in instance.facts:

            # found_escaped_characters = set(re.findall("(&\w*;)", fact.value))
            found_escaped_characters = set(re.findall("(&lt;.*?&gt;)", fact.value))

            if found_escaped_characters:

                # if (
                #     "&lt;" in found_escaped_characters
                #     and "&gt;" in found_escaped_characters
                # ):
                #     print("")

                # Add element to list if there haven't been any escaped characters found before
                if fact.concept_name not in elements.keys():
                    elements[fact.concept_name] = {}

                for found_escaped_character in found_escaped_characters:

                    if (
                        found_escaped_character
                        not in elements[fact.concept_name].keys()
                    ):
                        elements[fact.concept_name][found_escaped_character] = {
                            "count_char": 1
                        }
                    else:
                        current_count = elements[fact.concept_name][
                            found_escaped_character
                        ]["count_char"]
                        elements[fact.concept_name][found_escaped_character][
                            "count_char"
                        ] = (current_count + 1)

    return elements
