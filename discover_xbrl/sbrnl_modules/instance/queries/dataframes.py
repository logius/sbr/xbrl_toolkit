import io

# text buffer to return dataframe
s_buf = io.StringIO()


def write_df(dict, output_file):
    import pandas as pd

    df = pd.DataFrame(dict)

    # print(f"Saving dataframe as {output_file}")
    # df.to_csv(output_file)
    df.to_csv(s_buf)
    return s_buf
    # print("Done Saving")
