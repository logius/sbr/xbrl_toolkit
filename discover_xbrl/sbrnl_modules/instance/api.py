import asyncio

from fastapi import FastAPI, Request
from fastapi.responses import HTMLResponse, JSONResponse, StreamingResponse
from os.path import join, dirname

from fastapi.templating import Jinja2Templates

from discover_xbrl.conf.conf import Config

from read_instances import read_instances
from enrich_instances import enrich_instances

from retrieval_functions import get_instances
from queries.dicts import get_dict_elements, get_dict_dimmem, get_dict_escaped_chars
from queries.dataframes import write_df

conf = Config()
api_server = FastAPI()

templates = Jinja2Templates(directory=join(dirname(__file__), "templates"))


@api_server.get("/", response_class=HTMLResponse)
def get_instance_options(request: Request):
    """
    Returns options for the Logius XBRL instance processor
    """
    return templates.TemplateResponse("index.html", {"request": request})


@api_server.get("/find_contexts", response_class=HTMLResponse)
def find_contexts(request: Request):

    # todo: Need DTS for this one
    pass


@api_server.get("/generate_dataframes", response_class=StreamingResponse)
def generate_dataframes(request: Request):
    # Get instances

    lxml_instances = read_instances()

    # Get instance objects
    instances = get_instances(lxml_instances)

    # Get dicts
    dict_of_elements = get_dict_dimmem(instances)

    # Feed dict in test
    df_file = write_df(
        dict_of_elements, f"{conf.instances['dataframes']['output_dir']}/dimmem.csv"
    )
    return StreamingResponse(df_file, media_type="text/csv")


@api_server.get("/count_escaped_characters", response_class=JSONResponse)
def count_escaped_characters(request: Request):

    # Get instances
    lxml_instances = read_instances()
    # Get instance objects
    instances = get_instances(lxml_instances)

    elements = get_dict_escaped_chars(instances)

    for elem_name, chars in elements.items():
        for char_name, counts in chars.items():
            print(f"{elem_name}, '{char_name}', {counts['count_char']}")

    return elements


@api_server.get("/count_used_elements", response_class=JSONResponse)
def count_used_elements(request: Request):
    # TODO: NEEDS DTS
    pass

    # # Get instances
    # lxml_instances = read_instances()
    #
    # # Get instance objects
    # instances = get_instances(lxml_instances)
    #
    # # Get dicts
    # dict_of_elements = get_dict_elements(instances)
    #
    # dts = self.parent.discovered_dts_list[0]
    # # Feed dict in test
    # test_elements_instance_count(
    #     expected_elements=get_list_expected_elements(dts=dts),
    #     list_of_dicts=dict_of_elements,
    # )


@api_server.get("/test_formulas", response_class=JSONResponse)
def callTestFormulas(request: Request):
    # dts = self.parent.discovered_dts_list[0]

    from datetime import datetime

    start_time = datetime.now()

    print(f"{datetime.now() - start_time} - reading lxml instances")
    # Read instances
    lxml_instances = read_instances()
    print(f"{datetime.now() - start_time} - done reading lxml instances")

    print(f"{datetime.now() - start_time} - creating instance objects")
    # Get instance objects
    instances = get_instances(lxml_instances)
    print(f"{datetime.now() - start_time} - done creating instance objects")

    print(f"{datetime.now() - start_time} - enriching instance objects")
    # Enrich contexts with information out of the DTS
    instances = asyncio.run(enrich_instances(instances=instances))
    print(f"{datetime.now() - start_time} - done enriching instance objects")

    print(f"{datetime.now() - start_time} - Testing Assertions")
    test_assertions(dts=dts, instances=instances)
    print(f"{datetime.now() - start_time} - Done testing Assertions")

    instances_ea_fails = [
        instance for instance in instances if instance.failed_assertions
    ]

    error_messages = {}

    for instance in instances_ea_fails:
        for ea in instance.failed_assertions:
            key = f"{ea.id} - {ea.xlink_label.text}"
            if key not in error_messages.keys():
                error_messages[key] = 1
            else:
                error_messages[key] = error_messages[key] + 1

    print(error_messages)
    print("done")


if __name__ == "__main__":
    import uvicorn

    port = conf.instances["api"].get("port", 8000)
    host = conf.instances["api"].get("host", "127.0.0.1")
    uvicorn.run(api_server, host=host, port=port)
