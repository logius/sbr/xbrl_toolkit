import csv

from anytree import PreOrderIter
from openpyxl import Workbook

from discover_xbrl.conf.conf import Config
from discover_xbrl.sbrnl_modules.instance.enrich_instances import enrich_context
from discover_xbrl.sbrnl_modules.db.dts_to_sql import Dot

conf = Config()


# Try this for every expected element.
def has_element_count(expected_element, dict_of_elements):

    times_found = 0
    total_times_fount = 0

    for elements_of_instance in dict_of_elements:

        # Exists in instance
        if expected_element in elements_of_instance:
            times_found = times_found + 1

            total_times_fount = total_times_fount + elements_of_instance.count(
                expected_element
            )

    return (times_found, total_times_fount)


def get_list_expected_elements(dts):
    """
    Get a list of elements/concepts we can expect to find in reports.
    :param dts:
    :param list_of_dicts:
    :return:
    """

    expected_elements = []

    # First get a dictionary with expected elements.
    for linkrole in dts.directly_referenced_linkbases:
        if linkrole.presentation_hierarchy:
            if linkrole.roleURI != "http://www.xbrl.org/2008/role/link":
                for element in PreOrderIter(linkrole.presentation_hierarchy):
                    if not element.is_abstract and element not in expected_elements:
                        expected_elements.append(element)

    return expected_elements


def get_count_dict_value(count_dict, value):

    previous_value = 0
    for k, v in count_dict.items():
        # Found the value that is bigger than the dict
        if k > value:

            return previous_value
        else:
            previous_value = k

    return previous_value


def test_elements_instance_count(expected_elements, list_of_dicts):
    elements = []
    print(f"Expecting {len(expected_elements)} elements")

    # Make a dictionary with groups of counts.
    count_dict = {}

    """
    All I want is 10 tally counters to keep check of how many elements
    are found in a specific range.
    """

    interval = int(len(list_of_dicts) / 10)
    count_dict[0] = 0
    for x in range(1, len(list_of_dicts), interval):
        count_dict[x] = 0
    count_dict[len(list_of_dicts)] = 0

    for expected_element in expected_elements:

        count, total_count = has_element_count(expected_element.name, list_of_dicts)

        count_dict_value = get_count_dict_value(count_dict=count_dict, value=count)
        count_dict[count_dict_value] = count_dict[count_dict_value] + 1

        elem_label = [
            l
            for l in expected_element.labels.values()
            if l.linkRole == "http://www.xbrl.org/2003/role/label"
            and l.language == "nl"
        ]
        if elem_label:
            elem_label = elem_label[0].text

        elements.append((expected_element.name, elem_label, count, total_count))

    # Sort the list
    elements.sort(key=lambda a: a[2])

    with open(conf.instances["output_file"], "w") as output_file:
        csv_out = csv.writer(output_file)
        csv_out.writerow(("concept", "label", "aantal berichten", "totaal aantal"))
        for row in elements:
            csv_out.writerow(row)

    """Print a summary of found elements"""
    print("Amount of elements found in ranges")
    # previous_key = 0
    for k, v in count_dict.items():
        # if k == 0:
        #     # key_value = 0
        #
        # else:
        #     # key_value = f"{int(previous_key)}-{int(k - 1)}"
        #     previous_key = k

        print(f"{k},{v}")
    print("Done")


def test_element_linkrole_instance_count(instances):
    """ At the moment I assume the instancces are ordered per EP and have their own subdirictory
        This is however only implied by execution order so maybe we should add a sorter if
        this method needs to be more generic
        """
    dot = Dot()
    workbook = Workbook()
    dict = {}
    current_ep = None
    for instance in instances:
        if instance.taxonomy_ep != current_ep:
            if dict and current_ep:
                # create report for the previous EP
                write_element_linkrole_instace_count(dict, current_ep)
                create_ep_element_linkrole_instance_count_sheet(data=dict, ep=current_ep, workbook=workbook)
            dict = {}
            current_ep = instance.taxonomy_ep
        for fact in instance.facts:
            if not fact.linkrole:
                dot.dot()
                continue

            # Get a unique string
            lr_uris = [lr["role_uri"] for lr in fact.linkrole]

            unique_string = f"{fact.concept_tag}-{str('-'.join(lr_uris))}"

            if unique_string not in dict.keys():
                elem_label = [
                    l
                    for l in fact.concept.labels
                    if l["link_role"] == "http://www.xbrl.org/2003/role/label"
                    and l["lang"] == "nl"
                ][0]["label_text"]

                # This is the information we actually want to get into the overview
                row = {
                    "element_name": fact.concept.name,
                    "element_label": elem_label,
                    "count": 1,
                    "linkrole_uri": str(fact.linkrole[0]["role_uri"]),
                    "linkrole_definition": fact.linkrole[0]["definition"],
                }
                if len(fact.linkrole) > 1:
                    row.update({"other_linkroles": lr_uris[1:]})

                dict[unique_string] = row

            else:
                dict[unique_string]["count"] = dict[unique_string]["count"] + 1
    write_element_linkrole_instace_count(dict, current_ep)
    create_ep_element_linkrole_instance_count_sheet(data=dict, ep=current_ep, workbook=workbook)
    del workbook['Sheet']
    workbook.save(conf.instances["count_report"])


def write_element_linkrole_instace_count(dict: dict, ep: str):
    print(ep)
    with open(conf.instances["output_file"], "w") as output_file:
        csv_out = csv.DictWriter(
            output_file,
            fieldnames=[
                "element_name",
                "element_label",
                "count",
                "linkrole_uri",
                "linkrole_definition",
                "other_linkroles",
            ],
        )
        csv_out.writeheader()
        for row in dict.values():
            csv_out.writerow(row)
        print("Done writing file")


def create_ep_element_linkrole_instance_count_sheet(
    data: dict, ep: str, workbook: Workbook
):
    sheet = workbook.create_sheet(title=ep)
    sheet.append(
        [
            "Element name",
            "Element label",
            "Count",
            "Linkrole URI",
            "Definition",
            "Other linkroles",
        ]
    )
    sheet.column_dimensions['A'].width = 50
    sheet.column_dimensions['B'].width = 50
    sheet.column_dimensions['C'].width = 5
    sheet.column_dimensions['D'].width = 50
    sheet.column_dimensions['E'].width = 50
    for row in data.values():
        clean_list = []
        for value in row.values():
            clean_list.append(value if type(value) != list else ",".join(value))
        sheet.append(clean_list)
