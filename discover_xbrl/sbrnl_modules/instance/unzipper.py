import os
from zipfile import ZipFile
from discover_xbrl.conf.conf import Config
from discover_xbrl.sbrnl_modules.instance.read_instances import order_instances


class InstanceUnZipper:
    # This class assumes that there are instances zipped inside the main archive

    def __init__(self, source=None, main_archive=None):
        self.source = source
        self.archive = main_archive
        self.conf = Config()

    def unzip(self):
        unzipdir = self.conf.instances.get("unzipdir")
        if self.archive and unzipdir:
            with ZipFile(self.archive) as bundle:
                for archive in bundle.namelist():
                    bundle.extract(archive, unzipdir)
            # now we have a directory full of zipfiles
            # unzip further
            for file in os.listdir(unzipdir):
                if file.endswith(".zip"):
                    with ZipFile(os.path.join(unzipdir, file)) as instance:
                        for member in instance.namelist():
                            instance.extract(member, unzipdir)
                    os.remove(os.path.join(unzipdir, file))

            order_instances()


def main():
    unzipper = InstanceUnZipper(main_archive="/tmp/xbrl/2022-03-16_15-13-14-1830.zip")
    unzipper.unzip()


if __name__ == "__main__":
    main()
