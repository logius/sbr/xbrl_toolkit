from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.formulas.classes.assertion import (
    ExistenceAssertion,
    ValueAssertion,
)

from xpyth_parser.parse import Parser


def test_variable(variable, facts, resolved_variables):

    # Set variable for holding facts. This list gets filtered out in the next loop
    # If the list turns out to be empty, the assertion has failed for this loop

    all_facts = facts  # All facts. Does never change and is used to pass onto complementing filters
    current_facts = facts  # List of facts after each filter has, well, filtered it.

    for filter in variable.filters:
        # todo: filters should be resolved based on arc_order.

        # todo: implement filter.arc_cover - Not sure what this does though.
        if filter.arc_cover == "true":
            if filter.arc_complement == "false":
                if hasattr(filter, "variable"):
                    # Check if we have the variable value
                    if filter.variable in resolved_variables.keys():
                        filter_variable = resolved_variables[filter.variable]
                        current_facts = filter.test_facts(
                            facts=current_facts, variable_value=filter_variable
                        )
                    else:
                        print("Filter Variable value not available")

                else:
                    current_facts = filter.test_facts(facts=current_facts)
            else:
                # Add the complemented facts to the list of current facts
                complemented_facts = filter.test_facts(facts=all_facts)
                current_facts = current_facts + complemented_facts

    return current_facts


def assert_xpath_test(facts, test):
    # todo: EK: now follows some hard coded XPath stuff. Ultimately we'd want to be able to parse this dynamically
    if test == ".eq 1":
        if len(facts) == 1:
            return True
        else:
            return False

    elif test == ".le 1":
        if len(facts) <= 1:
            return True
        else:
            return False

    elif test == ".lt 1":
        if len(facts) < 1:
            return True
        else:
            return False

    elif test == ".gt 1":
        if len(facts) > 1:
            return True
        else:
            return False

    elif test == ".ge 1":
        if len(facts) >= 1:
            return True
        else:
            return False

    elif test == ".eq 0 or .eq 1":
        if len(facts) == 0 or len(facts) == 1:
            return True
        else:
            return False

    else:
        outcome = Parser(test, variable_map=facts).run()

        raise ("Xpath test not understood")


def test_assertion(instance, assertion):
    """
    Tests if an assertion is followed properly.
    Returns true if assertion is successfully tested

    :return: Boolean
    """

    # EK: Not sure where the filterset of assertions comes from. I think this may be wrongly added to the assertion itself.
    test_asssertion_filterset = False
    if test_asssertion_filterset is True:
        facts = instance.facts

        # first, filter the facts depending on the assertion's filterSet
        for filter in assertion.filterSet:
            facts = filter.test_facts(facts=facts)

        # If the initial filterset already yields no results, it has no use to test the variableSets.
        if len(facts) == 0:
            return False

    resolved_variables = {}
    # Go through each variableSet. If one of the sets yields a positive result, we return the assertion to be succeeded
    for variable in assertion.variableSet:
        variable = variable["variable"]

        # todo: Create a list of variable values to be used in tests
        # if variable.variable_arc_name not in resolved_variables.keys():
        #     var_val = test_variable(
        #         variable=variable,
        #         facts=instance.facts,
        #         resolved_variables=resolved_variables,
        #     )
        #
        #     resolved_variables[variable.variable_arc_name] = var_val

        facts = instance.facts

        # Filter facts by variableSet filters
        facts = test_variable(
            variable=variable, facts=facts, resolved_variables=resolved_variables
        )

    # If there is an (xpath) test, run this to check if the variable is deemed to be correct
    if assertion.test:
        test_outcome = assert_xpath_test(facts=facts, test=assertion.test)

    # If there is no XPath test, but there are facts that are not filtered out from filters.
    # We regard the variable to be correct
    elif len(facts) > 0:
        test_outcome = True

    else:
        # If there are no facts left though, the test failed
        test_outcome = False

    # If the variable is asserted to be correct, we'll return a positive result.
    #  if not, we either try the next variable, or return False at the end of the function
    if test_outcome is True:
        return test_outcome

    # If all variables in variableSet have been checked, and were asserted to be False, we'll return False as an answer
    return False


#
#
# def test_existence_assertion(instance, assertion):
#     """
#     Tests if an existence assertion is followed properly.
#     Returns true if assertion is successfully tested
#
#     :return:
#     """
#     variable = assertion.variableSet[0]["variable"]
#
#     # Set variable for holding facts. This list gets filtered out in the next loop
#     # If the list turns out to be empty, the assertion has failed for this loop
#     facts = instance.facts
#     for filter in variable.filters:
#         # todo: filters should be resolved based on arc_order.
#
#         facts = filter.test_facts(facts=facts)
#
#         if facts is False:
#             return False
#
#     return bool(facts)


def test_assertions(dts, instances):

    print("Testing assertions")
    existence_assertions = [
        a for a in dts.assertions if isinstance(a, ExistenceAssertion)
    ]

    print(f"{len(existence_assertions)} existence assertions")

    value_assertions = [a for a in dts.assertions if isinstance(a, ValueAssertion)]

    print(f"{len(value_assertions)} value assertions")

    for instance in instances:

        instance.failed_assertions = []
        # instance.failed_existence_assertion_text = []

        for ea in existence_assertions:
            outcome = test_assertion(instance=instance, assertion=ea)

            if outcome is False:
                instance.failed_assertions.append(ea)

        for va in value_assertions:
            outcome = test_assertion(instance=instance, assertion=va)

            if outcome is False:
                instance.failed_assertions.append(va)

    print("done testing assertions")
