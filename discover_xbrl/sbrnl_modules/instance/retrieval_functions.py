from discover_xbrl.sbrnl_modules.instance.classes import (
    Scenario,
    Context,
    Fact,
    Instance,
)

from lxml import etree

import os

parse_typed_members = True


def get_context(xml_context):
    """
    Tries to parse a Context. This includes a Scenario
    """

    # Required
    id = xml_context.get("id")

    ### entity is required, the identifier subelement is the only thing we care about for now
    xml_identifier = xml_context.find(
        "{http://www.xbrl.org/2003/instance}entity/{http://www.xbrl.org/2003/instance}identifier"
    )
    # segment is optional (not implemented for now)

    # Entity.Scheme is required
    identifier_scheme = xml_identifier.get("scheme")
    identifier = xml_identifier.text

    ### period is required
    xml_identifier = xml_context.find("{http://www.xbrl.org/2003/instance}period")

    # Can have: [startDate, endDate], instant, forever
    xml_period_start = xml_identifier.find(
        "{http://www.xbrl.org/2003/instance}startDate"
    )
    if xml_period_start is not None:
        period_start = xml_period_start.text
    else:
        period_start = None

    xml_period_end = xml_identifier.find("{http://www.xbrl.org/2003/instance}endDate")
    if xml_period_end is not None:
        period_end = xml_period_end.text
    else:
        period_end = None

    xml_period_instant = xml_identifier.find(
        "{http://www.xbrl.org/2003/instance}instant"
    )
    if xml_period_instant is not None:
        period_instant = xml_period_instant.text
    else:
        period_instant = None

    xml_period_forever = xml_identifier.find(
        "{http://www.xbrl.org/2003/instance}forever"
    )
    if xml_period_forever is not None:
        period_forever = True
    else:
        period_forever = False

    # scenario is optional
    scenario_list = []

    xml_scenario = xml_context.find("{http://www.xbrl.org/2003/instance}scenario")
    if xml_scenario is not None:

        # Need to get a list of all scenario's

        # First the explicit members
        for xml_member in xml_scenario.findall(
            "{http://xbrl.org/2006/xbrldi}explicitMember"
        ):
            member_prefix, member_name = xml_member.text.split(":", 1)
            dimension_nsprefix, dimension_name = xml_member.get("dimension").split(
                ":", 1
            )
            scenario_list.append(
                Scenario(
                    scenario_type="explicit",
                    dimension_name=dimension_name,
                    dimension_nsprefix=dimension_nsprefix,
                    dimension_namespace=xml_member.nsmap.get(dimension_nsprefix),
                    member_name=member_name,
                    member_prefix=member_prefix,
                    member_namespace=xml_member.nsmap.get(member_prefix),
                )
            )

        # Then, typed members
        if parse_typed_members is True:
            for xml_member in xml_scenario.findall(
                "{http://xbrl.org/2006/xbrldi}typedMember"
            ):

                xml_typed_member = xml_member[0]
                qname = etree.QName(xml_typed_member.tag)

                # Try and get the prefix
                member_prefix = None
                for prefix, namespace in xml_typed_member.nsmap.items():
                    if namespace == qname.namespace:
                        member_prefix = prefix

                if member_prefix is None:
                    print(
                        f"Did not find prefix for member {qname.localname}, namespace: {qname.namespace}"
                    )

                dimension_nsprefix, dimension_name = xml_member.get("dimension").split(
                    ":", 1
                )
                scenario_list.append(
                    Scenario(
                        scenario_type="typed",
                        dimension_name=dimension_name,
                        dimension_nsprefix=dimension_nsprefix,
                        dimension_namespace=xml_member.nsmap.get(dimension_nsprefix),
                        member_prefix=member_prefix,
                        member_namespace=qname.namespace,
                        member_name=qname.localname,
                        member_value=xml_typed_member.text,
                    )
                )

    return Context(
        id=id,
        identifier=identifier,
        identifier_scheme=identifier_scheme,
        period_start=period_start,
        period_end=period_end,
        period_instant=period_instant,
        period_forever=period_forever,
        scenario_members=scenario_list,
    )


def get_contexts(tree):
    for xml_context in tree.findall("{http://www.xbrl.org/2003/instance}context"):

        context = get_context(xml_context=xml_context)
        yield context


def get_instances(lxml_tree_instances):
    """
    Return the contexts and facts from instances

    :return:
    """
    list_of_instances = []

    print(f"Parsing {len(lxml_tree_instances)} instances")
    for tree in lxml_tree_instances:

        ### Gives context to the fact. Dimensions and Members give this context.
        contexts = {}

        ### Defines the value type of the fact. Not to be confused with itemtype.
        units = {}

        # The reported fact, plus the itemtype of the fact.
        facts = []

        # First we need to gather all of the context's used in the instance
        for context in get_contexts(tree=tree):
            contexts[context.id] = context

        # Second, we should get the units
        for xml_unit in tree.findall("{http://www.xbrl.org/2003/instance}unit"):
            units[xml_unit.get("id")] = xml_unit.find(
                "{http://www.xbrl.org/2003/instance}measure"
            )

        # Todo: the specification allows for more information, but I do not think we support that

        # Now, get the facts. This probably needs more tweaking because what counts as a correct fact?
        # For now, I'll look at the declared namespaces and go though all custom (non XBRL) namespaces.

        for prefix, namespace in tree.nsmap.items():

            # We do not need to check namespaces we know do not contain Elements.
            if prefix and namespace not in [
                "http://www.xbrl.org/2003/linkbase",
                "http://www.xbrl.org/2003/instance",
                "http://www.w3.org/1999/xlink",
                "http://www.xbrl.org/2003/iso4217",
                "http://xbrl.org/2006/xbrldi",
            ]:
                # We do still look through namespaces with DomainMembers and Dimensions
                # But if so, the instance would be invalid anyway.

                ns_facts = []
                for xml_fact in tree.findall(f"{prefix}:*", namespaces=tree.nsmap):
                    if xml_fact.get("contextRef"):
                        # Unit is optional
                        if xml_fact.get("unitRef"):
                            unit = units[xml_fact.get("unitRef")]
                        else:
                            unit = None

                        qname = etree.QName(xml_fact.tag)
                        ns_facts.append(
                            Fact(
                                concept_tag=xml_fact.tag,
                                concept_name=qname.localname,
                                concept_prefix=xml_fact.prefix,
                                concept_namespace=namespace,
                                context=contexts[xml_fact.get("contextRef")],
                                decimals=xml_fact.get("decimals"),
                                unit=unit,
                                value=xml_fact.text,
                            )
                        )

                facts.extend(ns_facts)

        taxonomy_ep = get_instance_entrypoint(instance=tree)
        # Try and create as series with fact as index?
        instance = Instance(
            facts=facts, contexts=contexts, units=units, taxonomy_ep=taxonomy_ep
        )
        list_of_instances.append(instance)

    return list_of_instances


def get_instance_entrypoint(instance) -> str:
    """
    Tries to find a schemaRef inside the xml-element
    :return:  entrypoint name (string) or None
    """
    try:
        namespaces = instance.nsmap
        if None in namespaces.keys():
            del namespaces[None]  # drop empty namespace if it exists
        schemaref = instance.xpath("//link:schemaRef", namespaces=namespaces)[0]
        schema_href = schemaref.get("{http://www.w3.org/1999/xlink}href")

        ep = schema_href.split("/")[-1]
        return ep
    except (IndexError, TypeError, etree.XPathEvalError) as e:
        return None
