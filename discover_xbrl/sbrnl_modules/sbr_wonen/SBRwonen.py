import logging


from openpyxl import load_workbook
# todo: add choose model dialogue
# file = Path("input/20200206 CONCEPT Integraal gegevensmodel dPi2020 EK.xlsx")
# wb = load_workbook(filename=file)

'''
Setting up logger
'''
logger = logging.getLogger("SBRWonenTaxonomyChecker") # Set logger name

# Adding a file output to the logger with timestamp and extra information
fh = logging.FileHandler('/tmp/outputwonen.csv', 'w+')

formatter = logging.Formatter('%(name)s,%(levelname)s,%(message)s')
fh.setFormatter(formatter)
fh.setLevel(logging.DEBUG)
logger.addHandler(fh)
logger.setLevel(logging.DEBUG)
sh = logging.StreamHandler() # Set a handler for displaying errors as a debugstream
sh.setFormatter(formatter)
sh.setLevel(logging.ERROR)
logger.addHandler(sh)



class ImportedConcept:

    '''
    todo: combine this class with 'generic' class used in discovery
    '''

    def __init__(self, conceptId, sheet, fields):
        self.conceptId = conceptId
        self.sheet = sheet

        for k, v in fields.items():

            if k == "Labeltext":
                self.labelText = v

            if k == "Veldtype":
                self.itemType = v

            if k == "Keuzemenu":
                self.formChoices = v

            if k == "Requirement":
                self.requirement = v

            if k == "Periodetype":
                self.periodType = v

            if k == "Balans":
                self.balanceType = v


def get_headings(worksheet):
    rows = worksheet.iter_rows(min_row=1, max_row=1)  # returns a generator of rows
    first_row = next(rows)  # get the first row
    headings = [c.value for c in first_row]  # extract the values from the cells

    return headings


def get_concepts_from_datamodel(wb):
    discovered_concepts = {}
    for sheet in wb:

        headings = get_headings(sheet)

        if "Concept ID" in headings:  # If there is no concept ID in the header, ignore the sheet

            for row in sheet.iter_rows(min_row=2, values_only=True):

                id = headings.index("Concept ID")

                if row[id]: # If the concept ID cell is not filled, ignore the row

                    fields = {}

                    if row[id]:
                        conceptId = headings.index("Concept ID")

                        for heading in headings:
                            col = headings.index(heading)
                            if row[col]:
                                fields[heading] = row[col]

                    discovered_concepts[row[id]] = ImportedConcept(fields["Concept ID"], sheet.title, fields)
    return discovered_concepts


def has_label_text(concept, text):
    '''
    Checks if a label with the given text exists for the given concept in DTS
    '''
    for labelname, label in concept.labels.items():
        if text == label.text:
            return labelname
    return False


def label_tests(dts, model_concept):

    # Check if the concept in DTS has a label of same value as in the model
    if model_concept.conceptId in dts.concepts.keys():

        # If concept is found, but no labels match
        labelID = has_label_text(dts.concepts[model_concept.conceptId], model_concept.labelText)

        if not labelID:
            # Concept_model, concept DTS, message
            logger.warning(f"{model_concept.sheet},{model_concept.conceptId},{model_concept.conceptId},Concept found but no label matches for concept")

        else:
            logger.info(f"{model_concept.sheet},{model_concept.conceptId},{model_concept.conceptId},Label matches correctly,{labelID}")

    else:
        # If no concept is found
        try:
            domain, labelName = model_concept.conceptId.split('_', 1)
        except:
            logger.error(f"{model_concept.sheet},A label without '-' is found. is the Concept ID column wrong?")




def test_sbrwonen(dts_list, workbook_path):
    # Get concepts from datamodel
    wb = load_workbook(workbook_path)

    concepts_from_model = get_concepts_from_datamodel(wb=wb)

    for dts in dts_list:
        # Loop through all concepts from the excel

        for row, model_concept in concepts_from_model.items():
            # todo: ook tests per tabbladen.

            if model_concept.conceptId in dts.concepts.keys():


                concept_in_dts = dts.concepts[model_concept.conceptId]

                # Only if the label also exists according to the model
                if hasattr(model_concept, 'labelText'):
                    label_tests(dts, model_concept)
                    # todo: labelmatch duidelijker terugkoppelen.

                # check periodType
                if hasattr(model_concept, 'periodType'):
                    if concept_in_dts.periodType:

                        if model_concept.periodType == "<NVT>":
                            pass
                        elif model_concept.periodType == concept_in_dts.periodType:
                            # Concept_model, concept DTS, message
                            logger.info(f"{model_concept.sheet},{model_concept.conceptId},{concept_in_dts.id},Period type matches")
                        else:
                            logger.error(
                                f"{model_concept.sheet},{model_concept.conceptId},{concept_in_dts.id},Period of does not match,"
                                f"model: {model_concept.periodType},DTS: {concept_in_dts.periodType}")
                    else:
                        logger.error(f"{model_concept.sheet},{model_concept.conceptId},{concept_in_dts.id},Concept does not have a period type in DTS")

                # check itemType (veld type)
                if hasattr(model_concept, 'itemType'):

                    # Strip string to remove namespace and "Itemtype"
                    stripped_dts_element_type = concept_in_dts.element_type.replace("ItemType", "").split(":")[1]

                    if model_concept.itemType == "<NVT>"\
                            or model_concept.itemType == 'NVT':
                        if stripped_dts_element_type:
                            logger.info(f"{model_concept.sheet},{model_concept.conceptId},{concept_in_dts.id},"
                                        f"Itemtype is not checked as it is not applicable according to model,"
                                        f"model: {model_concept.itemType},DTS: {stripped_dts_element_type}")
                    elif model_concept.itemType == stripped_dts_element_type:
                        logger.info(f"{model_concept.sheet},{model_concept.conceptId},{concept_in_dts.id},Element type of matches")
                    else:
                        logger.error(f"{model_concept.sheet},{model_concept.conceptId},{concept_in_dts.id},"
                                     f"Type does not match,model: {model_concept.itemType},DTS: {stripped_dts_element_type}")

                # check balanceType
                if hasattr(model_concept, 'balanceType'):

                    # If balancetype is not needed according to model, ignore.
                    if model_concept.balanceType == "<NVT>" \
                            or model_concept.balanceType is None\
                            or model_concept.balanceType == "NVT":
                        if concept_in_dts.balance is None:
                            concept_balance = "not set"
                        else:
                            concept_balance = concept_in_dts.balance

                        logger.info(f"{model_concept.sheet},{model_concept.conceptId},{concept_in_dts.id},"
                                    f"Balance is not checked as it is not applicable according to model,"
                                    f"model: {model_concept.balanceType},DTS: {concept_balance}")
                    elif concept_in_dts.balance:

                        if model_concept.balanceType == concept_in_dts.balance:
                            logger.info(f"{model_concept.sheet},{model_concept.conceptId},{concept_in_dts.id},balance matches")
                        else:
                            logger.error(f"{model_concept.sheet},{model_concept.conceptId},{concept_in_dts.id},balance does not match,"
                                         f"model: {model_concept.balanceType},DTS: {concept_in_dts.balance}")
                    else:
                        logger.error(f"{model_concept.sheet},{concept_in_dts.id},{concept_in_dts.id}Concept does not have a balance type in DTS")

                '''Next ones are not testable at the moment'''
                # check Requirement
                if hasattr(model_concept, 'requirement'):
                    pass

                # check Keuzemenu
                if hasattr(model_concept, 'formChoices'):
                    pass

            else:
                logger.error(f"{model_concept.sheet},'',{model_concept.conceptId},Concept not found in DTS")