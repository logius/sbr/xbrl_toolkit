from PySide2.QtWidgets import QGroupBox, QVBoxLayout, QPushButton, QFileDialog

from .SBRwonen import test_sbrwonen


class sbrWonenReportingActionsBox(QGroupBox):

    def __init__(self, parent=None):
        super(sbrWonenReportingActionsBox, self).__init__(parent)
        self.title = "SBR-Wonen toolbox"
        self.parent = parent
        # reportingActionsBox = QGroupBox("Reporting")

        # Create widgets
        sbrWonenButton = QPushButton("SBR Wonen report")

        # Connect functions to buttons
        sbrWonenButton.clicked.connect(self.calltestSBRwonen)

        # Set widgets to layout
        layout = QVBoxLayout()
        layout.addWidget(sbrWonenButton)

        # Set layout to class attribute
        self.setDisabled(False)
        self.setLayout(layout)

    def calltestSBRwonen(self):
        save_path = QFileDialog.getOpenFileName(self, "Select saved DTS)", "", "Entrypoint (*.xlsx)")
        save_path = str(save_path[0])


        test_sbrwonen(self.parent.discovered_dts_list, workbook_path=save_path)
