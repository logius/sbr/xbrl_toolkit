'''
File that loads all relevant module parts.
'''
from .widget import sbrWonenReportingActionsBox

def load_module(module_object={}):

    module_object['name'] = "SBRWonen"
    module_object['type'] = "export"
    module_object['gui'] = sbrWonenReportingActionsBox
    module_object['description'] = """
    Analytical tools specific to the use-case of SBR-Wonen. Is used to compare taxonomy information with a given
    data model.
    """
    return module_object
