import logging
import anytree

from openpyxl.styles import NamedStyle, colors, PatternFill
from openpyxl.cell import Cell

from discover_xbrl.sbrnl_xbrl_parser.schema.classes import Concept, RoleType

logger = logging.getLogger("modules.exports.reports")
"""
Presentation Report

Excel sheet per taxonomy



"""


def presentationReport(workbook, dts_list, hierarchy_nr):
    """
    Takes a workbook, adds a sheet with concept/EP overview and returns workbook.

    :param workbook:
    :return: workbook
    """

    sheet = workbook.create_sheet(title=f"Concept-EP-{hierarchy_nr}")
    header = ["Concept", "Label", "Order"]

    # Add DTS to headers
    for dts in dts_list:
        header.append(dts.entrypoint_name)

    # Write header to sheet
    sheet.append(header)

    # Color tints to used to add a gradient to depth of node.
    color_tints = [
        "e5a42d",
        "e7ad42",
        "eab656",
        "ecbf6c",
        "efc881",
        "f2d196",
        "f4daab",
        "f7e3c0",
        "f9ecd5",
        "fcf5ea",
        "ffffff",
    ]

    # As we checked before if presentation hierarchies are the same for all given DTS objects,
    # we can assume the hierarchy in the first DTS to be applicable to all.
    presentation_hierarchy = dts_list[0].presentation_hierarchy

    # We can not assume that the first DTS holds all RoleTypes, so we need to get all roleTypes beforehand.
    # That way we can use the most complete hierachy to build the rows.
    # If we do not do this, we will end up only writing down the RoleTypes in the first DTS of the list.
    all_roletypes = {}
    for dts in dts_list:
        for roletype in dts.roletypes.values():
            if roletype.presentation_hierarchy is not None:
                if roletype.roleURI not in all_roletypes.keys():

                    # Add roletype if it does not exist
                    all_roletypes[roletype.roleURI] = roletype

                # Replace the linkrole if we find one with more nodes.
                elif len(
                    all_roletypes[roletype.roleURI].presentation_hierarchy.descendants
                ) < len(roletype.presentation_hierarchy.descendants):
                    all_roletypes[roletype.roleURI] = roletype

    # Go loop though the presentation hierarchy. This gives the framework of linkroles and abstract concepts.
    for presentation_hierarchy_node in [
        node for node in anytree.PreOrderIter(presentation_hierarchy)
    ]:

        # If the instance is a concept, we use the Dutch label and ID
        if isinstance(presentation_hierarchy_node, Concept):
            if presentation_hierarchy_node.substitutionGroup == "sbr:presentationItem":
                col_1 = presentation_hierarchy_node.id
                col_2 = presentation_hierarchy_node.get_label(
                    "nl", "http://www.xbrl.org/2003/role/label"
                )[0].text
                col_3 = presentation_hierarchy_node.order

        # If the node is a RoleType, get the ID and definition
        elif isinstance(presentation_hierarchy_node, RoleType):
            col_1 = presentation_hierarchy_node.id
            col_2 = presentation_hierarchy_node.definition
            col_3 = presentation_hierarchy_node.order

        # Iterate to the next row
        max_row = sheet.max_row + 1

        # Add indentations to lower nodes
        str_level = int(presentation_hierarchy_node.depth + 1) * "  "

        sheet.cell(row=max_row, column=1, value=str_level + col_1).fill = PatternFill(
            patternType="solid", fgColor=color_tints[presentation_hierarchy_node.depth]
        )
        sheet.cell(row=max_row, column=2, value=str_level + col_2).fill = PatternFill(
            patternType="solid", fgColor=color_tints[presentation_hierarchy_node.depth]
        )
        sheet.cell(row=max_row, column=3, value=col_3).fill = PatternFill(
            patternType="solid", fgColor=color_tints[presentation_hierarchy_node.depth]
        )
        # If the instance is a RoleType, we also list the concepts in it's hierarchy.
        if isinstance(presentation_hierarchy_node, RoleType):
            if presentation_hierarchy_node.presentation_hierarchy:

                for concept_node in [
                    node
                    for node in anytree.PreOrderIter(
                        all_roletypes[
                            presentation_hierarchy_node.roleURI
                        ].presentation_hierarchy
                    )
                ]:

                    # Also add more indents  to the concepts.
                    concept_str_level = str_level + int(concept_node.depth + 1) * "  "

                    row = [
                        concept_str_level + concept_node.id,
                        concept_str_level + concept_node.name,
                        concept_node.order,
                    ]

                    # Now go though the DTS and add X'es if a concept is used in the DTS.
                    # This can probably be done a lot more effective by first creating these lists
                    # instead of walking though them ad-hoc.
                    found = False
                    for dts in dts_list:
                        cell = ""

                        # If the DTS Roletype has a presentation hierarchy (is used) continue
                        if dts.roletypes[
                            presentation_hierarchy_node.roleURI
                        ].presentation_hierarchy:

                            # Loop through nodes in the DTS Roletype
                            for desc_node in dts.roletypes[
                                presentation_hierarchy_node.roleURI
                            ].presentation_hierarchy.descendants:

                                # If the concept is found, Add an X
                                if concept_node.id == desc_node.id:
                                    cell = "X"
                                    found = True

                        # If the concept is not found in the DTS, show a "/" ("" after debugging)
                        if not found:
                            cell = "X"
                            found = False

                        row.append(cell)

                    sheet.append(row)

    sheet.column_dimensions["A"].width = 100
    sheet.column_dimensions["B"].width = 50
    sheet.column_dimensions["C"].width = 5
    return workbook


def old_presentationReport(workbook, dts_list):

    sheet = workbook.create_sheet(title=f"Concept-EP")
    header = ["Concept", "Label", "Order"]

    max_column_a_size = 0  # Keep track of the maximum size the first column should be

    shared_linkroles = {}

    for dts in dts_list:

        # Add EP column if this doesn't exist yet.
        new_index = len(header) + 1
        header.insert(new_index, (str(dts.entrypoint_name).strip(".xsd")))

        # Create a list of all linkroles to iterate through.
        for lr_name, lr in dts.roletypes.items():
            if lr.presentation_hierarchy:
                if lr_name not in shared_linkroles:

                    # Add only the name. We need to get unique lr occurances per DTS anyway
                    shared_linkroles[lr_name] = lr.presentation_hierarchy.root

                else:
                    if (
                        shared_linkroles[lr_name].root
                        is not lr.presentation_hierarchy.root
                    ):
                        if len(shared_linkroles[lr_name].root.children) < len(
                            lr.presentation_hierarchy.root.children
                        ):
                            # todo: quick fix; if a bigger tree is found, use that one. Better to fully merge trees instead.
                            shared_linkroles[lr_name] = lr.presentation_hierarchy.root

    """
    Loop through all known linkroles. For each occurance in an EP, an "X" is inserted in the coresponding cell
    """
    for lr_name, lr in shared_linkroles.items():

        lr_tree = lr.presentation_hierarchy.root
        max_row = sheet.max_row
        # sheet.append([lr_name, "", "", ""])  # append linkrole row
        sheet.cell(row=max_row, column=1, value=lr_name).fill = PatternFill(
            patternType="solid", fgColor="FFB732"
        )
        sheet.cell(row=max_row, column=2, value="").fill = PatternFill(
            patternType="solid", fgColor="FFB732"
        )
        sheet.cell(row=max_row, column=3, value="").fill = PatternFill(
            patternType="solid", fgColor="FFB732"
        )
        sheet.cell(row=max_row, column=4, value="").fill = PatternFill(
            patternType="solid", fgColor="FFB732"
        )
        # sheet.row_dimensions[max_row].style = 'Accent1'

        # Here we already need to have the superset of elements within the linkrole
        for node in [node for node in anytree.PreOrderIter(lr_tree)]:

            row = []  # Create row for node

            # Create columns for every DTS
            for dts in dts_list:

                for roletype_uri, roletype in dts.roletypes.items():

                    if roletype_uri == lr_name and roletype.presentation_hierarchy:
                        col_nr = header.index(str(dts.entrypoint_name).strip(".xsd"))

                        # If this is the first DTS to list this linkrole, add extra information to row
                        if len(row) == 0:

                            # Set value for concept name
                            if hasattr(node, "name"):
                                value = node.name
                            elif hasattr(node, "id"):
                                value = node.id
                            else:
                                value = ""
                            if isinstance(object, bool):
                                logger.error(f"Concept is not given")

                            # Add spaces for each extra depth in tree
                            str_level = int(node.depth + 1) * "  "
                            concept_name = str_level + value

                            if hasattr(node, "order"):
                                order = node.order
                            else:
                                order = ""
                            row.extend([concept_name, "", order])
                            # Add empty cells if row is shorter than header:
                            for i in range(len(header) - len(row)):
                                row.append("")

                        row.insert(col_nr, "X")

            sheet.append(row)  # Add row for element occurrence
            if len(row[0]) > max_column_a_size:
                max_column_a_size = len(row[0])

    # Add header with EPs
    sheet.insert_rows(1)

    for i, value in enumerate(header):
        sheet.cell(row=sheet.min_row, column=i + 1, value=header[i])

    sheet.column_dimensions["A"].width = max_column_a_size
    sheet.column_dimensions["B"].width = 50
    sheet.column_dimensions["C"].width = 5

    EP_col = ord("D")
    for dts in dts_list:
        sheet.column_dimensions[chr(EP_col)].width = 5
        EP_col = EP_col + 1

    return workbook
