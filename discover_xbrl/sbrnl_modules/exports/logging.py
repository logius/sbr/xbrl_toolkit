import logging

from discover_xbrl.conf.conf import Config


def setup_logger():
    """
    Setting up logger
    """
    formatter = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    )
    conf = Config()

    # Setup exports logger
    logger_builder = logging.getLogger("modules.exports")
    logger_builder.addHandler(
        logging.FileHandler(f"{conf.logging['maindir']}/exports.log", "a+")
    )
    logger_builder.addHandler(logging.StreamHandler())

    # Setup postprocessing logger
    logger_reports = logging.getLogger("modules.exports.reports")
    logger_reports.propagate = False
    logger_reports.setLevel(logging.WARNING)
    logger_reports.addHandler(
        logging.FileHandler(f"{conf.logging['maindir']}/exports_reports.log", "a+")
    )
    logger_reports.addHandler(logging.StreamHandler())
