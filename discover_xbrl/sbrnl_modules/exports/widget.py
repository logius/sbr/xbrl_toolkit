import logging
from pathlib import Path

from PySide2.QtWidgets import QFileDialog, QGroupBox, QVBoxLayout, QPushButton

from openpyxl import Workbook

from .presentationReport import presentationReport
from .dimensionReport import dimensionReport
from .export import TaxonomyJSONExporter, TaxonomyExcelExporter
from discover_xbrl.sbrnl_xbrl_toolbox.taxonomyeditor import XBRLEditorGui
from discover_xbrl.sbrnl_modules.db.dts_to_sql import DtsSQLWorker

from discover_xbrl.conf.conf import Config

logger = logging.getLogger("modules.exports")


class exportsWidget(QGroupBox):
    def __init__(self, parent=None):
        super(exportsWidget, self).__init__(parent)
        self.title = "Exports"
        self.parent = parent
        self.xbrl_editor = XBRLEditorGui()
        self.conf = Config()

        # reportingActionsBox = QGroupBox("Reporting")

        # Create widgets
        saveTaxonomyJSONButton = QPushButton("Save Taxonomy as JSON")
        saveTaxonomyExcelButton = QPushButton("Save Taxonomy as Excel")
        saveTaxonomySQLButton = QPushButton("Save Taxonomy to Database")
        generateRenderingReportsButton = QPushButton("Generate rendering reports")
        startEditorButton = QPushButton("Start taxonomy editor")

        # Connect functions to buttons
        saveTaxonomyJSONButton.clicked.connect(self.saveTaxonomyJSON)
        saveTaxonomyExcelButton.clicked.connect(self.saveTaxonomyExcel)
        saveTaxonomySQLButton.clicked.connect(self.save_to_db)
        generateRenderingReportsButton.clicked.connect(self.generateRenderingReports)
        startEditorButton.clicked.connect(self.start_editor)

        # Set widgets to layout
        layout = QVBoxLayout()
        layout.addWidget(saveTaxonomyJSONButton)
        layout.addWidget(saveTaxonomyExcelButton)
        if hasattr(self.conf, "db") and self.conf.db.get("file", None):
            layout.addWidget(saveTaxonomySQLButton)
        layout.addWidget(generateRenderingReportsButton)
        layout.addWidget(startEditorButton)

        # Set layout to class attribute
        if not self.parent.discovered_dts_list:
            self.setDisabled(True)
        self.setLayout(layout)

    def saveTaxonomyExcel(self):
        save_path = QFileDialog.getSaveFileName(
            self, "Select saved DTS)", "", "Entrypoint (*.xlsx)"
        )
        save_path = str(save_path[0])
        exporter = TaxonomyExcelExporter(self.parent.discovered_dts_list, save_path)
        # exporter.add_generic_sheets()
        exporter.save_DTS_list_excel()

    def saveTaxonomyJSON(self):
        export_dir = Path(self.parent.selectFolderPopup())
        exporter = TaxonomyJSONExporter(self.parent.discovered_dts, export_dir)
        exporter.writeDictionary()

    def save_to_db(self):
        info_widget = None
        if hasattr(self.parent, "info"):
            self.parent.info.append("starting SQL export")
            info_widget = self.parent.info

        # call a QRunnable so we have control of the Gui in the main thread
        # in this case w are no interested in the result, we give the handle to our info widget, if any,
        # the called process has the freedom to spew it's message if it needs to.
        # if you need the result, you have to use Signals, sbrnl_xbrl_parser/widget.py works like this

        sql_worker = DtsSQLWorker(
            dts_list=self.parent.discovered_dts_list, info_widget=info_widget
        )
        self.parent.threadpool.start(sql_worker)

    def generateRenderingReports(self, workbook=None):
        logger.info("Starting to generate rendering reports")
        if hasattr(self.parent, "info"):
            self.parent.info.append("Starting to generate rendering reports")

        if not workbook:
            workbook = Workbook()

        # presentation
        logger.info("Adding presentation report")
        if hasattr(self.parent, "info"):
            self.parent.info.append("Adding presentation report")
            self.parent.info.update()

        # First, make sure all loaded DTS object use the same presentation hierarchy.
        # Hashing Anynode gives a unique hash, so comparing these object is difficult
        # As a workaround, we'll take the number of nodes as a sort of indicator of uniqueness
        # This workaround obviously won't work if the contents of the hierarchies differ,
        # but the amount of nodes is the same.

        hierarchies = {}
        for dts in self.parent.discovered_dts_list:
            if len(dts.presentation_hierarchy.descendants) not in hierarchies:
                hierarchies[len(dts.presentation_hierarchy.descendants)] = [dts]
            else:
                hierarchies[len(dts.presentation_hierarchy.descendants)].append(dts)

        for i, dts_list in enumerate(hierarchies.values()):
            workbook = presentationReport(workbook, dts_list, hierarchy_nr=i)

        for dts in self.parent.discovered_dts_list:
            workbook = dimensionReport(workbook, dts)

        # logger.info("Adding dimension report")
        # dimension

        # logger.info("Adding formula report")
        # Formula

        workbook.save("/tmp/reports.xlsx")
        if hasattr(self.parent, "info"):
            self.parent.info.append("Report saved to: '/tmp/reports.xlsx'")
            self.parent.info.update()

    def start_editor(self):
        self.xbrl_editor.set_dts(self.parent.discovered_dts)
        self.xbrl_editor.show()
        self.xbrl_editor.raise_()
        self.xbrl_editor.activateWindow()

    def enable_widget(self, payload):
        if "dts_list_updated" in payload:
            self.setDisabled(False)
