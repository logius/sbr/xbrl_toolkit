'''
File that loads all relevant module parts.
'''
from .widget import exportsWidget
from .logging import setup_logger

def load_module(module_object={}):

    setup_logger()
    module_object['name'] = "exports"
    module_object['type'] = "export"
    module_object['gui'] = exportsWidget
    module_object['description'] = """
    The exports module houses generic export functions for parts of the taxonomy, mostly as Excel files. 
    Functions are build in such a way that they can be chained together. Every module adds another sheet to the Excel
    workbook. This allows for reuse, such as adding information to the larger analytical modules such as the semantics 
    checker.
    """
    return module_object
