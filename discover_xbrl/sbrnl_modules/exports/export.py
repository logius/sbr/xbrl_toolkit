import json
import logging
from pathlib import Path

import anytree
from openpyxl import Workbook

from ...sbrnl_xbrl_parser.schema.classes import Concept

logger = logging.getLogger("modules.exports")


class TaxonomyExporter:
    """
    Base class for taxonomy exporters
    """

    def __init__(self, dts, dir, taxonomy_format):

        if isinstance(dts, list):
            self.dts_list = dts
        else:
            self.dts = dts

        self.dir = dir
        self.taxonomy_format = taxonomy_format

    def writeDictionary(self):

        logger.warning("write dictionary not implemented")

    def writeTaxonomy(self):

        logger.warning("write taxonomy not implemented")


class TaxonomyJSONExporter(TaxonomyExporter):
    """
    Exports the taxonomy into experimental JSON format
    """

    def __init__(self, dts, dir, taxonomy_format="JSON"):
        super(TaxonomyJSONExporter, self).__init__(dts, dir, taxonomy_format)

    def writeTaxonomy(self):
        self.writeDictionary()

    def writeDictionary(self):
        dictionary = {}

        if self.dts:
            for name, concept in self.dts.concepts.items():
                dictionary[name] = {}
                labels_dictionary = {}
                for label_name, label in concept.labels.items():

                    # Add language and linkrole label if they do not yet exist
                    if label.language not in labels_dictionary:
                        labels_dictionary[label.language] = {}

                    labels_dictionary[label.language][label.linkRole] = label.text

                dictionary[name]["labels"] = labels_dictionary

            with open(Path.joinpath(self.dir, "dictionary.json"), "wb") as p:
                p.write(
                    json.dumps(dictionary, indent=4, ensure_ascii=False).encode("utf8")
                )
        else:
            print("Nothing to see here")


class TaxonomyExcelExporter(TaxonomyExporter):
    def __init__(self, dts_list, dir, taxonomy_format="Excel"):
        super(TaxonomyExcelExporter, self).__init__(dts_list, dir, taxonomy_format)

    def get_headings(worksheet):
        rows = worksheet.iter_rows(min_row=1, max_row=1)  # returns a generator of rows
        first_row = next(rows)  # get the first row
        headings = [c.value for c in first_row]  # extract the values from the cells
        return headings

    def save_DTS_list_excel(self, workbook=None):
        """
        Saves part of the DTS to an excel file
        :param path:
        :return:
        """
        if not workbook:
            workbook = Workbook()

        # workbook = self.concept_ep_export(workbook)
        workbook = self.reference_exports(workbook)
        workbook = self.concepts_export(workbook)
        workbook = self.label_export(workbook)
        workbook = self.dts_statistics(workbook)

        # todo: EK quick and dirty way to save the file.
        """
        Right now, the filepath comes frome the 'vars.py' configuration file.
        This is probably not a great way to maintain static information, and is better to be placed in a YAML or
        JSON file. For now I'll solve it like this, but in the future we should change this for something better.
        """
        workbook.save(self.dir)

    def dts_statistics(self, workbook):
        # creates a sheet with general information about the DTS
        """


        - Aantal domeinen(prefixes)
        - Aantal concepten(per domein)
        - Aantal itemTypes                  EK: Not sure if this metric tells much but sure
        - Aantal rapportage – niet - rapportage(abstract) concepten EK: maybe the rendering reports are better to use? amount of roletypes?
        - Aantal entrypoints                EK: Not really useful per DTS, will be 1:1, or with all, consolidated to 1
        - Aantal dimensies                  EK: per Hypercube maybe?
        - Aantal linkbases(label, formula, reference, presentation)
        - etc

        """
        sheet = workbook.create_sheet(title="Statistics")
        concepts = []
        itemtypes = []
        roletypes = []
        references = []
        hypercubes = []

        """
        Because we do not know how many DTS objects are offered, we need to make a set of all relevant elements.
    
        """
        for dts in self.dts_list:

            #  EK: there is probably a better way using set(), but for now this will do
            for concept in dts.concepts.values():  # is still a dict
                if concept not in concepts:
                    concepts.append(concept)

            # todo EK: does not disregard false positives, such as XML schema stuff
            for itemtype in dts.datatypes:
                if itemtype not in itemtypes:
                    itemtypes.append(itemtype)

            for roletype in dts.roletypes.values():  # is still a dict
                if roletype not in roletypes:
                    roletypes.append(roletype)

            for reference in dts.references:
                if reference not in references:
                    references.append(reference)

            for hypercube in dts.hypercubes:
                if hypercube not in hypercubes:
                    hypercubes.append(hypercube)
        """
        CONCEPTS:
        labels and such?
        avg types of labels per concept
        avg languages per type
        """

        """
        DIMENSIONS
        EK: Quick and dirty way to get some info on hypercubes.
        HC dimensions is probably only a valid metric after the Z-axis is put into use, but we'll add it now anyway
        Members per dimension might say something about complexity as well
        """
        hc_rows = []

        for hc in hypercubes:
            amount_of_dimensions = len(hc.dimensions)
            amount_of_lineitems = len(hc.lineItems)
            for dim in hc.dimensions:
                avg_members_per_domain = len([m for m in dim.members])

            hc_rows.append(
                [amount_of_dimensions, amount_of_lineitems, avg_members_per_domain]
            )

        sheet.append(
            [
                "These are the figures for the entire DTS. For data on referenced only, "
                "please consult the specific sheets."
            ]
        )
        sheet.append(["concepts", len(concepts)])
        sheet.append(["itemtypes", len(itemtypes)])
        sheet.append(["roletypes", len(roletypes)])
        sheet.append(["references", len(references)])
        sheet.append(["hypercubes", len(hypercubes)])

        """
        EK: The following numbers do not say much, but could indicate some extra examination is required.
        However, if all HC's turn out to have two dimensions with each 1 member, the data is not exciting
        enough to warrant extra research.
        
        For getting the average there are probably a lot more pythonic ways, but this will do for now.
        """
        avg_dims = []
        avg_lineitems = []
        avg_members = []

        for hc_row in hc_rows:
            avg_dims.append(hc_row[0])
            avg_lineitems.append(hc_row[1])
            avg_members.append(hc_row[2])

        sheet.append(["hypercube avg dimensions", len(avg_dims)])
        sheet.append(["hypercube avg lineitems", len(avg_lineitems)])
        sheet.append(["hypercube avg members", len(avg_members)])

        """
        EK: todo:
        -   table, type of nodes
        -   formulas?
        -   filters?
        -   assertions / variables?
        """

        return workbook

    def reference_exports(self, workbook):
        # Export references
        sheet = workbook.create_sheet(title="References")

        max_column_a_size = (
            0  # Keep track of the maximum size the first column should be
        )

        found_references = []
        for dts in self.dts_list:

            for export_type, parts in {"reference": dts.references}.items():
                if parts:  # Only add sheet if there are references

                    # Get the headers by looking at the attributes of the first item.
                    # This only works for objects without variable attributes
                    first_val = next(iter(parts))
                    headers = list(vars(first_val).keys())
                    headers.insert(0, export_type)
                    sheet.append(headers)

                    for part in parts:
                        row = []
                        row.insert(0, part.id)

                        for attr, cell in part.__dict__.items():
                            row.insert(headers.index(attr), cell)

                        if row not in found_references:

                            found_references.append(row)
                            sheet.append(row)
                            if len(row[0]) > max_column_a_size:
                                max_column_a_size = len(row[0])

        sheet.column_dimensions["A"].width = max_column_a_size
        return workbook

    def concepts_export(self, workbook):

        """
        Lists all concepts used in the DTS.

        :param workbook:
        :return:

        We could run through dts.concepts, but that would give us all concepts present in the DTS.
        Not only those that are referenced. For that we need to go through all directly referenced linkbases
        and check which arcs refer to which concepts


        """
        sheet = workbook.create_sheet(title="Concepts")

        max_column_a_size = (
            0  # Keep track of the maximum size the first column should be
        )

        found_concepts_old = []
        found_concepts = []

        """
        First we need to build a list of all concepts referenced from the DTS.
        """
        # todo: explicitly add which DTS is referencing the concept
        for dts in self.dts_list:
            for linkrole in dts.directly_referenced_linkbases:
                for arc in linkrole.arcs:
                    if isinstance(arc.from_concept_obj, Concept):
                        if arc.from_concept_obj not in found_concepts:
                            found_concepts.append(arc.from_concept_obj)
                    if isinstance(arc.to_concept_obj, Concept):
                        if arc.to_concept_obj not in found_concepts:
                            found_concepts.append(arc.to_concept_obj)

        # first_val = next(iter(parts.values()))
        # headers = list(vars(first_val).keys())
        headers = [
            "id",
            "name",
            "nillable",
            "balance",
            "substitutionGroup",
            "periodType",
            "ns_prefix",
            "typedDomainRef",
            "is_abstract",
            "element_type",
        ]

        sheet.append(headers)

        for concept in found_concepts:

            # row = []

            row = [
                "" for i in range(len(headers))
            ]  # Row is empty by default. Otherwise .index does not seem to work.

            for attr, cell in concept.__dict__.items():

                if attr not in ["labels", "references"]:
                    """
                    Some attributes are explicitly not used in this overview

                    EK: Maybe add labels sheet here as well?
                    """

                    if not isinstance(cell, str) and cell is not None:
                        """
                        EK: Strings are put into Excel cells just fine, but for some attributes
                            extra steps are necessary.
                        """

                        if isinstance(cell, tuple):
                            cell = cell[0]
                        elif isinstance(cell, bool):
                            pass  # booleans are added correctly to the excel sheet.

                            """
                            EK: The following cases are used to get the name of the referenced
                                ItemType.
                            """
                        elif attr == "typedDomainRef" and cell is not None:
                            cell = cell.name

                        elif attr == "element_type" and cell is not None:
                            cell = cell.name

                        else:
                            logger.error(
                                f"Value: '{cell}' of attr: '{attr}' is not a string and cannot be added to a cell"
                            )

                        """
                        EK: Same as above, attributes can contain objects, rather than only strings
                        """
                        if not isinstance(attr, str):

                            if isinstance(attr, tuple):
                                attr = attr[0]
                            elif attr == "element_type" and cell is not None:
                                attr = attr.name
                            else:
                                logger.error(f"attr: '{attr}' is not a string")

                if attr in headers:
                    # Add the cell to the right column
                    index = headers.index(attr)

                    row[index] = cell

            # For each concept, add a row
            try:
                sheet.append(row)
                max_column_a_size = max(max_column_a_size, len(row[0]))

            except ValueError as e:
                logger.error(
                    f"Row could not be added to the Excel. "
                    f"Probably an invalid cell value is passed"
                )

                found_concepts_old.append(
                    row
                )  # always add, once failed, always failing
        sheet.column_dimensions["A"].width = max_column_a_size
        return workbook

    def label_export(self, workbook):
        sheet = workbook.create_sheet(title="Labels")

        max_column_a_size = (
            0  # Keep track of the maximum size the first column should be
        )

        found_labels = []
        found_concepts = []

        """
        First we need to build a list of all concepts referenced from the DTS.
        """
        # todo: explicitly add which DTS is referencing the concept
        for dts in self.dts_list:
            for linkrole in dts.directly_referenced_linkbases:
                for arc in linkrole.arcs:
                    if isinstance(arc.from_concept_obj, Concept):
                        if arc.from_concept_obj not in found_concepts:
                            found_concepts.append(arc.from_concept_obj)

                            # But also get the labels
                            for label in arc.from_concept_obj.labels.values():
                                found_labels.append(label)

        headers = list(vars(found_labels[0]).keys())
        headers.insert(0, "labels")
        sheet.append(headers)

        for label in found_labels:

            # Get the headers by looking at the attributes of the first item.
            # This only works for objects without variable attributes
            row = []
            # row.insert(0, label)
            if hasattr(label, "__dict__"):
                for attr, cell in label.__dict__.items():
                    row.insert(headers.index(attr), cell)

                if row not in found_labels:

                    sheet.append(row)
                    if len(row[0]) > max_column_a_size:
                        max_column_a_size = len(row[0])

        sheet.column_dimensions["A"].width = max_column_a_size
        return workbook

    def concept_ep_export(self, workbook):
        """
        Takes a workbook, adds a sheet with concept/EP overview and returns workbook.

        :param workbook:
        :return: workbook
        """

        sheet = workbook.create_sheet(title="Concept-EP")
        header = ["Concept", "Label", "Order"]

        max_column_a_size = (
            0  # Keep track of the maximum size the first column should be
        )

        # sheet.append(header)

        """
        Print blocks of linkbases + presentation hierarchy, combined with usage in entrypoints. 
        """

        """
        Build table structure:
           1. create columns per EP.
           2. Create a list of linkroles that all DTS objects have in common.
        """
        shared_linkroles = {}
        for dts in self.dts_list:

            # Add EP column if this doesn't exist yet.
            new_index = len(header) + 1
            header.insert(new_index, dts.entrypoint_name)

            # Create a list of all linkroles to iterate through.
            for lr_name, lr in dts.roletypes.items():
                if lr.presentation_hierarchy:
                    if lr_name not in shared_linkroles:

                        # Add only the name. We need to get unique lr occurances per DTS anyway
                        shared_linkroles[lr_name] = lr.presentation_hierarchy.root

                    else:
                        if (
                            shared_linkroles[lr_name].root
                            is not lr.presentation_hierarchy.root
                        ):
                            if len(shared_linkroles[lr_name].root.children) < len(
                                lr.presentation_hierarchy.root.children
                            ):
                                # todo: quick fix; if a bigger tree is found, use that one. Better to fully merge trees instead.
                                shared_linkroles[
                                    lr_name
                                ] = lr.presentation_hierarchy.root

        """
        Loop through all known linkroles. For each occurance in an EP, an "X" is inserted in the coresponding cell
        """
        for lr_name, lr_tree in shared_linkroles.items():

            sheet.append([lr_name, ""])  # append linkrole row
            # Here we already need to have the superset of elements within the linkrole
            for node in [node for node in anytree.PreOrderIter(lr_tree)]:

                row = []  # Create row for node

                # Create columns for every DTS
                for dts in self.dts_list:

                    for roletype_uri, roletype in dts.roletypes.items():

                        if roletype_uri == lr_name and roletype.presentation_hierarchy:
                            col_nr = header.index(dts.entrypoint_name)

                            # if anytree.findall(roletype.presentation_hierarchy.root, lambda n: n.arc == node.arc):

                            # If this is the first DTS to list this linkrole, add extra information to row
                            if len(row) == 0:

                                # Set value for concept name
                                if hasattr(node.arc.to_concept_obj, "name"):
                                    value = node.arc.to_concept_obj.name
                                elif hasattr(node.arc.to_concept_obj, "id"):
                                    value = node.arc.to_concept_obj.id
                                else:
                                    value = ""
                                    logger.error(
                                        f"Arc {node.arc.from_concept} -> {node.arc.to_concept} does not have a 'to' value to display in hierarchy"
                                    )
                                str_level = (
                                    int(node.depth + 1) * "  "
                                )  # Add spaces for each extra depth in tree
                                concept_name = str_level + value

                                row.extend([concept_name, "", node.arc.order])
                                # Add empty cells if row is shorter than header:
                                for i in range(len(header) - len(row)):
                                    row.append("")

                            row.insert(col_nr, "X")

                sheet.append(row)  # Add row for element occurrence
                if len(row[0]) > max_column_a_size:
                    max_column_a_size = len(row[0])

        # Add header with EPs
        sheet.insert_rows(1)

        for i, value in enumerate(header):
            sheet.cell(row=sheet.min_row, column=i + 1, value=header[i])

        sheet.column_dimensions["A"].width = max_column_a_size
        return workbook
