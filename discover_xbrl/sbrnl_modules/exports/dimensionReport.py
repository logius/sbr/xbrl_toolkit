import logging

import anytree
from openpyxl.cell import Cell
from openpyxl.styles import NamedStyle, colors, PatternFill

logger = logging.getLogger("modules.exports.reports")
"""
Presentation Report

Excel sheet per taxonomy



"""


def add_hypercube_headers(hypercube):

    """
    Header is setup with three rows:


    ['Concept', 'Label', 'linkrole (Width depends on amount of dimensions)']
    ['','','dimension (For every dimension in a linkrole/hypercube)']
    ['','',member (For every member underneath a dimension]
    Look at breakdowns for weird situations!

    """

    h1 = []
    h2 = []
    h3 = []

    # Add the header on row 1
    h1.append(hypercube.linkrole.__str__())

    dim = 1
    for dimension in hypercube.dimensions:
        # add dimensions vertically

        # Add the header on row 2
        h2.append(dimension.concept.name)

        # For each extra dimension, add a column to row 1
        if not dim <= 1:
            h1.append("")
        dim = dim + 1

        mem = 1
        for member in dimension.members:
            # Add an extra column on the third row for each domain member
            h3.append(member.concept.name)

            # For each extra member, add a column to row 1 and 2
            if not mem <= 1:

                h1.append("")
                h2.append("")

            mem = mem + 1

        if not dimension.members:
            h2.append("No-dim Domain")
            h3.append("No-dim Member")

    return (h1, h2, h3)


def add_lineitem_rows(lineitems, relevant_columns):
    rows = []

    for lineitem in lineitems:
        row = {}
        lineitem_name = lineitem[1].name
        row[0] = lineitem_name  # Add to first column
        for relevant_column in relevant_columns:
            row[relevant_column] = "X"
        rows.append(row)

    return rows


def process_presentation_hierarchy(hierarchy, relevant_columns):
    rows = []
    for node in [node for node in anytree.PreOrderIter(hierarchy)]:
        row = {}

        # Combine name with node depth based indent
        str_level = (
            int(node.depth + 1) * "  "
        )  # Add spaces for each extra depth in tree
        concept_name = str_level + node.name

        row[0] = concept_name
        if hasattr(node, "preferred_label"):
            if node.preferred_label is not None:
                # If there is a preffered label, get that one instead of the standard label
                row[1] = node.get_label("nl", node.preferred_label)[0].text

            else:

                row[1] = node.get_label("nl", "http://www.xbrl.org/2003/role/label")[
                    0
                ].text
        else:
            row[1] = node.get_label("nl", "http://www.xbrl.org/2003/role/label")[0].text

        if row[1] is None:
            print("")

        # Add empty cells if row is shorter, for each relevant column
        for relevant_column in relevant_columns:
            row[relevant_column] = "X"

        rows.append(row)
    return rows


def dimensionReport(workbook, dts):
    """
    Takes a workbook, adds a sheet with dimensions of an EP

    :param workbook:
    :return: workbook
    """

    sheet = workbook.create_sheet(title=f"Dim_{dts.entrypoint_name}")

    sheet["A1"] = "Concept"
    sheet["B1"] = "Label"
    sheet["A2"] = ""
    sheet["B2"] = ""
    sheet["A3"] = ""
    sheet["B3"] = ""

    # Get hypercubes via presentation_hierarchy that holds the linkrole order.
    hypercubes = []
    for lr in dts.linkrole_order.values():
        if lr.hypercube is not None:
            hypercubes.append(lr.hypercube)

    for hypercube in hypercubes:

        """
        COLUMNS
        """
        # Get columns

        extra_h1, extra_h2, extra_h3 = add_hypercube_headers(hypercube)

        relevant_columns = (
            []
        )  # Keep track of which columns where primaries need to be tagged
        max_column = sheet.max_column
        for row, header_values in enumerate([extra_h1, extra_h2, extra_h3], start=1):
            for col, value in enumerate(header_values, start=1):

                # max column + (relative) column - 1 (for correcting Excel starting at 1 instead of 0
                relevant_columns.append(max_column + col - 1)
                sheet.cell(row=row, column=max_column + col, value=value)

        """
        # Try and merge cells in header
        for row_nr, row in enumerate(sheet.iter_rows(min_row=1, max_row=3, min_col=3), start=1):
            for cell in row:
                if cell.value == "":
                    # Set start to previous column (the current, empty cell is to the right of it)
                    start_cell = cell.column -1
                    end_cell = cell.column

                    for cell in row[col:]:
                        if cell.value == "":
                            # Cell is empty, should also be merged
                            end_cell = end_cell + 1
                        else:
                            # Found the first non empty cell. Merge and stop looking

                            sheet.merge_cells(start_row=row_nr, start_column=start_cell, end_row=row_nr, end_column=end_cell)
                            sheet.cell(column=start_cell, row=row_nr).value = value
                            break
        """

        """
        ROWS
        """

        if hypercube.linkrole.presentation_hierarchy:
            # If there is a presentation hierarchy, use this as it contains more information
            rows = process_presentation_hierarchy(
                hypercube.linkrole.presentation_hierarchy, relevant_columns
            )
        else:
            # Otherwise, use the lineitems for creating rows
            rows = add_lineitem_rows(hypercube.lineItems, relevant_columns)

        max_row = sheet.max_row
        sheet.cell(
            row=max_row + 1, column=1, value=hypercube.linkrole.__str__()
        ).fill = PatternFill(patternType="solid", fgColor="FFB732")
        max_row = max_row + 1

        for rel_row, row in enumerate(rows, start=1):
            for rel_col, cell in row.items():
                if cell == "X":
                    sheet.cell(
                        row=rel_row + max_row, column=rel_col + 1, value=cell
                    ).fill = PatternFill(patternType="solid", fgColor="32CD32")
                elif cell != "":
                    sheet.cell(
                        row=rel_row + max_row, column=rel_col + 1, value=cell
                    ).fill = PatternFill(patternType="solid", fgColor="B0E0E6")
                elif "Title" in cell:
                    sheet.cell(
                        row=rel_row + max_row, column=rel_col + 1, value=cell
                    ).fill = PatternFill(patternType="solid", fgColor="E6E6FA")

            # max_row = max_row + 1

    sheet.column_dimensions["A"].width = 50
    sheet.column_dimensions["B"].width = 50
    return workbook
