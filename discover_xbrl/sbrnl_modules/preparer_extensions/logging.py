import logging

from discover_xbrl.conf.conf import Config


def setup_logger():
    """
    # Set all loggers for the parser
    # """

    formatter = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    )
    conf = Config()

    # Setup parser logger
    logger_parser = logging.getLogger("preparer_extensions")
    # logger_parser.setLevel(logging.WARNING)  # container for parser loggers. Do not log info and debug.
    logger_parser.addHandler(
        logging.FileHandler(f"{conf.logging['maindir']}/preparer_extensions.log", "a+")
    )
    logger_parser.addHandler(logging.StreamHandler())
