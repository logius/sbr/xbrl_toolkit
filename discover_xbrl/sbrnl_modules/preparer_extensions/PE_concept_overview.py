import os
import logging
from lxml import etree

from ...sbrnl_xbrl_parser.schema.subparser.resources import discover_elements


logger = logging.getLogger("preparer_extensions")
"""
Separate script to show an overview of Preparer Extensions within a given directory.
For now, this script only parses XML directly. 

TODO:
-   In the future the XBRL parser may be used as well.
-   Have the script create an 'ep' for the normal parser to discover ==> DTS with referenced taxonomies

Creates a list of all elements used in a Preparer Extension.
"""


def pe_overview(url):

    """
    :param url:
    :return:
    """
    concepts = {}
    detailed_concept_list = []
    concepts_used_more_often = {}

    # First parse all schema's
    for subdir, dirs, files in os.walk(url):
        for filename in files:
            filepath = subdir + os.sep + filename
            file_contents = open(filepath, "rb").read()

            if filename.endswith(".xsd"):
                # Do schema stuff
                schema = etree.fromstring(file_contents)

                # get elements
                xpath_elements = schema.xpath(
                    "//xs:schema/xs:element", namespaces=schema.nsmap
                )
                for elem in discover_elements(xpath_elements):

                    kvk_namespace = schema.xpath(
                        "//xs:schema/xs:import[contains(@namespace, 'kvk')]",
                        namespaces=schema.nsmap,
                    )[0].get("namespace")
                    detailed_concept_list.append(
                        [elem, kvk_namespace, kvk_namespace.split("/")[3]]
                    )

                    # if elem.name not in concepts.keys() and elem.name not in concepts_used_more_often.keys():
                    #     # concept is not found before, so we can add it to the normal list
                    #     concepts[elem.name] = elem
                    # else:
                    #     # if the concept has already been found, we need to take it out of this list and
                    #     # add it to the multiple use list
                    #     if elem.name in concepts.keys():
                    #         concepts.pop(elem.name)

                    if elem.name not in concepts_used_more_often.keys():
                        concepts_used_more_often[elem.name] = 1
                    else:
                        concepts_used_more_often[elem.name] = (
                            concepts_used_more_often[elem.name] + 1
                        )

    # Then parse all linkbase. These depend on information from schema's
    for subdir, dirs, files in os.walk(url):
        for filename in files:
            filepath = subdir + os.sep + filename
            file_contents = open(filepath, "rb").read()

            if filename.endswith(".xml"):
                linkbase = etree.fromstring(file_contents)
                # Do linkbase stuff

                """
                Cannot get the hierarchies yet. These (likely) depend on information from outside the PE.
                """

                """
                Should get labels as well. Alternatively, I could just input the PE into the parser, 
                but that does take awhile to process. This is not really usable for comparing PE's. 
                """

    for concept, namespace, nt in detailed_concept_list:
        pass
        # logger.warning(f"{concept.name},{namespace},{nt}")

    for concept, times in concepts_used_more_often.items():
        pass
        # logger.warning(f"{concept},{times}")
        #  print(f"{concept},{times}")
