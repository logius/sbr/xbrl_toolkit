from PySide2.QtWidgets import QFileDialog, QGroupBox, QVBoxLayout, \
    QPushButton

from .PE_concept_overview import pe_overview


class preparerExtensionsActionsBox(QGroupBox):

    def __init__(self, parent=None):
        super(preparerExtensionsActionsBox, self).__init__(parent)
        self.title = "SBR-Wonen toolbox"
        self.parent = parent
        # reportingActionsBox = QGroupBox("Reporting")

        # Create widgets
        sbrWonenButton = QPushButton("Select PE folder")

        # Connect functions to buttons
        sbrWonenButton.clicked.connect(self.call_pe_overview)

        # Set widgets to layout
        layout = QVBoxLayout()
        layout.addWidget(sbrWonenButton)

        # Set layout to class attribute
        self.setDisabled(False)
        self.setLayout(layout)

    def call_pe_overview(self):
        pe_folder = QFileDialog.getExistingDirectory(self, "Select Directory")
        if pe_folder:
            pe_overview(pe_folder)
