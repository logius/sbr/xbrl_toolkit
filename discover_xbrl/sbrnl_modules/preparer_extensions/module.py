from .logging import setup_logger

'''
File that loads all relevant module parts.
'''
from .widget import preparerExtensionsActionsBox

def load_module(module_object={}):

    setup_logger()

    module_object['name'] = "Preparer Extensions"
    module_object['type'] = "export"
    module_object['gui'] = preparerExtensionsActionsBox
    module_object['description'] = """
    A collection of analytical tools to inspect multiple preparer extensions. These are organization specific additions
    to the SBR NT taxonomy.
    Right now, this module only allows for rudimentary analysis. Only simple semantic checks between different PE's
    is possible. No NT Architecture rules are taken into consideration either.
    """
    return module_object
