from anytree import AnyNode

from discover_xbrl.sbrnl_modules.db.collections.itemtypes import ItemTypes
from discover_xbrl.sbrnl_modules.db.model.variable import Variable
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.dimensions.classes import (
    Hypercube,
    Dimension,
    Member,
)
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.formulas.classes.assertion import (
    ValueAssertion,
    ExistenceAssertion,
)
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.formulas.classes.filter_aspect_cover import (
    FilterAspectCover,
)
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.formulas.classes.filter_boolean import (
    FilterBooleanOr,
)
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.formulas.classes.filter_concept import (
    FilterConceptBalance,
    FilterConcept,
    FilterConceptDatatype,
    FilterConceptName,
    FilterConceptSubstitutionGroup,
)
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.formulas.classes.filter_concept_relation import (
    FilterConceptRelation,
)
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.formulas.classes.filter_dimension import (
    FilterDimensionExplicit,
    FilterDimensionTyped,
)
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.formulas.classes.filter_period import (
    FilterPeriodEnd,
    FilterPeriodInstant,
    FilterPeriodInstantDuration,
)
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.formulas.classes.formula import (
    FormulaPeriod,
    FormulaExplicitDimension,
    FormulaTypedDimension,
)
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.formulas.classes.variables import (
    Parameter,
    FactVariable,
    GeneralVariable,
    VariableSet,
)
from discover_xbrl.sbrnl_xbrl_parser.linkbase.subparser.tables.classes import (
    Table,
    Breakdown,
    RuleNode,
    ConceptRelationshipNode,
    Aspect,
    AspectNode,
    DimensionRelationshipNode,
    RuleSet,
)
from discover_xbrl.sbrnl_xbrl_parser.parsetools.classes import DTS
from discover_xbrl.sbrnl_xbrl_parser.linkbase.classes import Label, Reference, Arc
from discover_xbrl.sbrnl_xbrl_parser.schema.classes import (
    Concept,
    EnumerationChoice,
    ItemType,
    RoleType,
)

from discover_xbrl.sbrnl_modules.db.collections.dts import DTS as DbDTS

from discover_xbrl.sbrnl_modules.db.connection import get_connection


class SqlToDts:
    def __init__(self, dts_name=None):
        self.dts_name = dts_name
        self.DTS = None
        self.db = get_connection()
        self._itemtypes = {}
        self._concepts = {}
        self._labels = {}
        self._references = {}
        self._assertions = {}
        self._filters = {}
        self._variables = {}
        self._parameters = {}
        self._dimensions = {}
        self._table_breakdowns = {}
        if self.dts_name:
            self.create_dts()

    def create_dts(self):
        if self.dts_name:
            # First the DTS is returned as dicts
            db_dts = DbDTS(db=self.db, full=True)
            db_dts.get_dts(name=self.dts_name, referenced_only=False)
            self.DTS = DTS(name=self.dts_name)
            # we want clean itemtypes, so don't resolve the base
            self.handle_itemtypes()
            self.handle_namespaces(db_dts.namespaces)
            self.handle_concepts(db_dts.concepts)
            self.handle_linkroles(db_dts.linkroles)
            if db_dts.presentation_hierarchy:
                self.DTS.presentation_hierarchy = self.get_presentation(
                    db_dts.presentation_hierarchy, None
                )
            self.post_proces()

    def handle_namespaces(self, namespaces):
        self.DTS.namespaces = namespaces.copy()

    def handle_itemtypes(self):
        itemtypes = ItemTypes(db=self.db, full=True, resolve_base=False)
        itemtypes.itemtypes_of_dts(dts_name=self.dts_name)
        for db_itemtype in itemtypes.itemtypes:
            itemtype = self.get_itemtype(db_itemtype)

    def handle_concepts(self, concepts):
        for db_concept in concepts:
            concept = self.get_concept(db_concept)

            for db_label in db_concept["labels"]:
                label = self.get_label(db_label)
                if label.xlink_label not in concept.labels:
                    concept.labels[label.xlink_label] = label

            for db_reference in db_concept["references"]:
                reference = self.get_reference(db_reference)
                concept.references.append(reference)

            if db_concept.get("itemtype"):
                db_itemtype = db_concept["itemtype"]
                itemtype = self.get_itemtype(db_itemtype)
                concept.element_type = itemtype

            self.DTS.concepts[concept.id] = concept

    def handle_linkroles(self, linkroles):
        for db_linkrole in linkroles:
            linkrole = self.get_roletype(db_linkrole)

            self.DTS.roletypes[linkrole.roleURI] = linkrole
            if db_linkrole["order"]:
                self.DTS.linkrole_order[str(db_linkrole["order"])] = linkrole
            if db_linkrole["directly_referenced"]:
                self.DTS.directly_referenced_linkbases.append(linkrole)

            for db_label in db_linkrole["labels"]:
                label = self.get_label(db_label)
                linkrole.labels[label.xlink_label] = label

            if db_linkrole["hypercube"]:
                hypercube = self.get_hypercube(db_linkrole["hypercube"])
                linkrole.hypercube = hypercube
                for db_line_item in db_linkrole["hypercube"]["line_items"]:
                    hypercube.lineItems.append(
                        (
                            db_line_item["order"],
                            self.get_concept(db_line_item["concept"]),
                        )
                    )
                for db_dimension in db_linkrole["hypercube"]["dimensions"]:
                    dimension = self.get_dimension(db_dimension)
                    self.get_dimension_members(
                        db_dimension["members"], parent=dimension
                    )
                    hypercube.dimensions.append(dimension)
                    # todo: Domains!!

            if db_linkrole.get("presentation_hierarchy"):
                linkrole.presentation_hierarchy = self.get_presentation(
                    db_linkrole["presentation_hierarchy"], None
                )

            for db_table in db_linkrole["tables"]:
                table = self.get_table(db_table)
                linkrole.tables.append(table)
                for db_label in db_table["labels"]:
                    label = self.get_label(db_label)
                    table.labels[label.xlink_label] = label
                    linkrole.element_labels[label.xlink_label] = label

                for db_parameter in db_table["parameters"]:
                    parameter = self.get_parameter(db_parameter)
                    if parameter not in table.parameters:
                        table.parameters.append(parameter)
                        linkrole.resources["parameters"].append(parameter)

                for db_breakdown in db_table["table_breakdowns"]:
                    breakdown = self.get_table_breakdown(db_breakdown)
                    table.table_breakdowns.append(breakdown)
                    for db_rulenode in db_breakdown["table_rulenodes"]:
                        _ = self.get_table_rulenode(
                            db_rulenode, parent=breakdown, linkrole=linkrole
                        )
                    for db_crn in db_breakdown["concept_relationshipnodes"]:
                        self.get_concept_relationshipnode(
                            db_crn, parent=breakdown, linkrole=linkrole
                        )
                    for db_drn in db_breakdown["dimension_relationshipnodes"]:
                        self.get_dimension_relationshipnode(
                            db_drn, parent=breakdown, linkrole=linkrole
                        )
                    for db_aspectnode in db_breakdown["aspect_nodes"]:
                        self.get_aspectnode(db_aspectnode, parent=breakdown)

            for db_variable in db_linkrole["variables"]:
                variable = self.get_variable(db_variable)
                if variable not in linkrole.resources["variables"]:
                    linkrole.resources["variables"].append(variable)

            for db_assertion in db_linkrole["assertions"]:
                assertion = self.get_assertion(db_assertion, linkrole=linkrole)
                if assertion not in linkrole.resources["assertions"]:
                    linkrole.resources["assertions"].append(assertion)

            if db_linkrole.get("presentation_hierarchy"):
                linkrole.presentation_hierarchy = self.get_presentation(
                    db_linkrole["presentation_hierarchy"], None
                )

            if db_linkrole.get("references"):
                for db_reference in db_linkrole["references"]:
                    linkrole.references.append(self.get_reference(db_reference))

    def set_dts_name(self, dts_name=None):
        if dts_name:
            self.dts_name = dts_name
            self.create_dts()

    ###
    # Getter functions, returns the concept created from the dictionary
    # if it has already been instantiated,we return that object
    # the object holds dicts of each type
    def get_itemtype(self, dict):
        unique = dict["id"] if dict.get("id") else dict["name"]
        if unique in self._itemtypes.keys():
            return self._itemtypes[unique]
        i = ItemType(
            id=dict["id"],
            name=dict["name"],
            domain=dict["domain"],
            base=dict["base"],
            inheritance_type=dict["inheritance_type"],
            restriction_pattern=dict["restriction_pattern"],
            max_length=dict["restriction_max_length"],
            min_length=dict["restriction_min_length"],
            totalDigits=dict["total_digits"],
            fraction_digits=dict["fraction_digits"],
            min_inclusive=dict["min_inclusive"],
        )
        if dict["enumeration"]:
            i.enumerations = []
            for db_enum_option in dict["enumeration"]:
                enumeration_choice = EnumerationChoice(
                    id=db_enum_option["id"], value=db_enum_option["value"]
                )
                for db_label in db_enum_option["labels"]:
                    label = self.get_label(db_label)
                    enumeration_choice.labels.append(label)
                i.enumerations.append(enumeration_choice)
        if unique is None:
            print(
                "Something is going wrong. We did not get an id/name for the itemtype"
            )
            return None
        else:
            self._itemtypes[unique] = i
            self.DTS.datatypes.append(i)
            return i

    def get_concept(self, dict):
        unique = dict["id"] if dict.get("id") else f"{dict['namespace']:{dict['name']}}"
        if unique in self._concepts.keys():
            return self._concepts[unique]

        c = Concept(
            id=dict["id"],
            name=dict["name"],
            balance=dict["balance"],
            nillable=dict["nillable"],
            is_abstract=dict["abstract"],
            periodType=dict["periodtype"],
            ns_prefix=dict["ns_prefix"],
            namespace=dict["namespace"],
            substitutionGroup=dict["substitution_group"],
            typedDomainRef=dict["typed_domainref"],
            element_type=self.get_itemtype(dict["itemtype"])
            if dict["itemtype"] is not None
            else None,
        )
        self._concepts[unique] = c
        return c

    def get_label(self, dict):
        if dict["link_label"] in self._labels.keys():
            return self._labels[dict["link_label"]]
        l = Label(
            id=dict["id"],
            xlink_label=dict["link_label"],
            linkRole=dict["link_role"],
            language=dict["lang"],
            text=dict["label_text"],
        )
        self._labels[dict["link_label"]] = l
        return l

    def get_reference(self, dict):
        unique = dict["id"] if dict.get("id") else dict["label"]
        if unique in self._references.keys():
            return self._references[unique]
        r = Reference(
            id=dict["id"],
            xlink_label=dict["label"],
            name=dict["name"],
            number=dict["number"],
            issuedate=dict["issuedate"],
            article=dict["article"],
            note=dict["note"],
            section=dict["section"],
            subsection=dict["subsection"],
            publisher=dict["publisher"],
            paragraph=dict["paragraph"],
            subparagraph=dict["subparagraph"],
            clause=dict["clause"],
            subclause=dict["subclause"],
            appendix=dict["appendix"],
            example=dict["example"],
            page=dict["page"],
            exhibit=dict["exhibit"],
            footnote=dict["footnote"],
            sentence=dict["sentence"],
            uri=dict["uri"],
            uridate=dict["uridate"],
        )
        self._references[unique] = r
        return r

    def get_roletype(self, dict):
        r = RoleType(
            roleURI=dict["role_uri"],
            definition=dict["definition"],
            id=dict["id"],
            used_on=dict["used_on"],
        )
        return r

    def get_hypercube(self, dict):
        h = Hypercube(
            hc_type=dict["hc_type"],
            linkrole=dict["hc_role_uri"],
            context_element=dict["context_element"],
            closed=dict["closed"],
        )
        return h

    def get_dimension(self, dict):
        unique = dict["rowid"]
        if unique in self._dimensions:
            return self._dimensions[unique]
        d = Dimension(
            concept=self.get_concept(dict["concept"]),
            target_linkrole=dict["target_linkrole"],
        )
        self._dimensions[unique] = d
        return d

    def get_dimension_members(self, dict, parent=None):
        members = []
        for db_member in dict:
            member = Member(
                concept=self.get_concept(db_member["concept"]),
                parent=parent,
                order=db_member["order"],  # EK: sometimes order is None
                usable=db_member["usable"],
                target_linkrole=db_member["target_linkrole"],
            )
            if db_member.get("members"):
                self.get_dimension_members(db_member["members"], parent=member)
            members.append(member)
        return members

    def get_parameter(self, dict):
        unique = dict["id"] if dict.get("id") else dict["label"]
        if unique in self._parameters.keys():
            return self._parameters[unique]
        p = Parameter(
            as_datatype=dict["as_datatype"],
            xlink_label=dict["label"],
            name=dict["name"],
            required=dict["required"],
            select=dict["select"],
            id=dict["id"],
        )
        self._parameters[unique] = p
        return p

    def get_variable(self, dict):
        unique = dict["id"] if dict.get("id") else dict["label"]
        if unique in self._variables.keys():
            return self._variables[unique]
        if dict["type"] == "FactVariable":
            variable = FactVariable(
                id=dict["id"],
                xlink_label=dict["label"],
                matches=dict["matches"],
                bind_as_sequence=dict["bind_as_sequence"],
                nils=dict["nils"],
                fallback_value=dict["fallback_value"],
                filters=[self.get_filter(filter) for filter in dict["filters"]],
            )

        elif dict["type"] == "GeneralVariable":
            variable = GeneralVariable(
                id=dict["id"],
                xlink_label=dict["label"],
                bind_as_sequence=dict["bind_as_sequence"],
                select=dict["select"],
            )
        else:
            variable = Variable(
                id=dict["id"],
                label=dict["label"],
                bind_as_sequence=dict["bind_as_sequence"],
                matches=dict["matches"],
                select=dict["select"],
                nils=dict["nils"],
                fallback_value=dict["fallback_value"],
            )
        self._variables[unique] = variable
        return variable

    def get_assertion(self, dict, linkrole):
        unique = dict["id"] if dict.get("id") else dict["label"]
        if unique in self._assertions.keys():
            return self._assertions[unique]
        if dict["assertion_type"] == "ValueAssertion":
            assertion = ValueAssertion(
                id=dict["id"],
                xlink_label=dict["xlink_label"],
                aspect_model=dict["aspect_model"],
                implicit_filtering=dict["implicit_filtering"],
                test=dict["test"],
            )
        else:
            assertion = ExistenceAssertion(
                id=dict["id"],
                xlink_label=dict["xlink_label"],
                aspect_model=dict["aspect_model"],
                implicit_filtering=dict["implicit_filtering"],
                test=dict["test"],
            )
        for db_label in dict["labels"]:
            label = self.get_label(db_label)
            assertion.labels[label.xlink_label] = label
            linkrole.element_labels[label.xlink_label] = label
        if dict.get("variablesets"):
            for varset in dict["variablesets"]:
                assertion.variableSet.append(self.get_variableset(varset, linkrole))
        self._assertions[assertion.id] = assertion
        return assertion

    def get_variableset(self, dict, linkrole):

        # Get factVariables
        factVariables = []
        for factVariableDict in dict["variables"]:
            factVariable = linkrole.get_resource(
                xlink_label=factVariableDict.get("label", factVariableDict.get("id"))
            )
            if factVariable:
                factVariables.append(
                    {
                        "order": factVariableDict["order"],
                        "priority": factVariableDict["priority"],
                        "variable": factVariable,
                    }
                )
            else:
                print(
                    f"could not find variable '{factVariableDict['id']}' for variableSet '{dict['name']}'"
                )
        variableset = VariableSet(name=dict["name"], variables=factVariables)
        return variableset

    def get_presentation(self, dict, parent):
        node = AnyNode(
            parent=parent,
            id=dict["id"],
            order=dict["order"],
            preferred_label=dict["preferred_label"]
            if dict.get("preferred_label")
            else None,
            object=self.get_concept(dict["concept"])
            if dict["node_type"] == "concept"
            else self.DTS.roletypes.get(dict["linkrole_role_uri"]),
        )
        if dict.get("children"):
            for child in dict["children"]:
                _ = self.get_presentation(child, node)
        return node

    def get_table(self, dict):
        t = Table(
            id=dict["id"],
            parentChildOrder=dict["parent_child_order"],
            xlink_label=dict["label"],
        )
        return t

    def get_table_breakdown(self, dict):
        unique = dict["id"] if dict.get("id") else dict["label"]
        if unique in self._table_breakdowns.keys():
            return self._table_breakdowns[unique]
        breakdown = Breakdown(
            parentChildOrder=dict["parent_child_order"],
            xlink_label=dict["label"],
            axis=dict["axis"],
            order=dict["order"],
            id=dict["id"],
        )
        self._table_breakdowns[unique] = breakdown
        return breakdown

    def get_table_rulenode(self, dict, parent, linkrole):
        # this function is recursive, that means that
        # all work needs to be done here for this node
        # hence we need the parent linkrole for reference
        node = RuleNode(
            id=dict["id"],
            xlink_label=dict["label"],
            parent=parent,
            order=dict["order"],
            parentChildOrder=dict["parent_child_order"],
            is_abstract=dict["is_abstract"],
            merge=dict["merge"],
            tagselector=dict["tagselector"],
            rulesets=[],
        )
        if dict.get("concept_relationshipnodes"):
            for cr_node in dict.get("concept_relationshipnodes"):
                self.get_concept_relationshipnode(
                    dict=cr_node, parent=node, linkrole=linkrole
                )
        if dict.get("aspect_nodes"):
            for aspect_node in dict.get("aspect_nodes"):
                self.get_aspectnode(dict=aspect_node, parent=node)
        if dict.get("dimension_relationshipnodes"):
            for dr_node in dict.get("dimension_relationshipnodes"):
                self.get_dimension_relationshipnode(
                    dict=dr_node, parent=node, linkrole=linkrole
                )
        if dict.get("formula_periods"):
            for db_formula_period in dict["formula_periods"]:
                node.rulesets.append(self.get_formula_period(db_formula_period))
        if dict.get("formula_explicit_dimensions"):
            for db_formula_explicit_dimension in dict["formula_explicit_dimensions"]:
                node.rulesets.append(
                    self.get_formula_explicit_dimension(db_formula_explicit_dimension)
                )
        if dict.get("formula_typed_dimensions"):
            for db_formula_typed_dimension in dict["formula_typed_dimension"]:
                node.rulesets.append(
                    self.get_formula_typed_dimension(db_formula_typed_dimension)
                )
        if dict.get("rulesets"):
            for db_ruleset in dict["rulesets"]:
                node.rulesets.append(self.get_ruleset(db_ruleset))

        for db_label in dict["labels"]:
            label = self.get_label(db_label)
            node.labels[label.xlink_label] = label
            linkrole.element_labels[label.xlink_label] = label

        if dict.get("children"):
            for child_node in dict["children"]:
                _ = self.get_table_rulenode(child_node, node, linkrole=linkrole)
        return node

    def get_concept_relationshipnode(self, dict, parent, linkrole):
        # We don't need to return anything, we're building a anynode-tree,
        # the parent will get this as a child
        ConceptRelationshipNode(
            id=dict["id"],
            xlink_label=dict["label"],
            order=dict["order"],
            parent=parent,
            parentChildOrder=dict["parent_child_order"],
            relationship_source=dict["relationship_source"],
            linkrole=linkrole,
            formula_axis=dict["formula_axis"],
            generations=dict["generations"],
            arcrole=dict["arcrole"],
            linkname=dict["linkname"],
            arcname=dict["arcname"],
            tagselector=dict["tagselector"],
        )

    def get_dimension_relationshipnode(self, dict, parent, linkrole):
        # We don't need to return anything, we're building a anynode-tree,
        # the parent will get this as a child

        # Get the dimension as well
        dim_obj = None
        for dim in self._dimensions.values():
            if dict["dimension_id"] == f"{dim.concept.ns_prefix}:{dim.concept.name}":
                dim_obj = dim

        DimensionRelationshipNode(
            id=dict["id"],
            xlink_label=dict["label"],
            order=dict["order"],
            dimension=dim_obj if dim_obj is not None else dict["dimension_id"],
            parent=parent,
            parentChildOrder=dict["parent_child_order"],
            relationship_source=dict["relationship_source"],
            linkrole=linkrole,
            formula_axis=dict["formula_axis"],
            generations=dict["generations"],
            tagselector=dict["tagselector"],
        )

    def get_aspectnode(self, dict, parent):
        # We don't need to return anything, we're building a anynode-tree,
        # the parent will get this as a child
        aspectnode = AspectNode(
            id=dict["id"],
            xlink_label=dict["label"],
            order=dict["order"],
            merge=dict["merge"],
            is_abstract=dict["is_abstract"],
            parent=parent,
            tagselector=dict["tagselector"],
        )
        aspect = Aspect(
            aspect_type=dict["aspect_type"],
            value=dict["value"],
            includeUnreportedValue=dict["include_unreported_value"],
        )
        aspectnode.aspect = aspect

    def get_formula_period(self, dict):
        fp = FormulaPeriod(
            periodtype=dict["period_type"],
            start=dict["start"],
            end=dict["end"],
            value=dict["value"],
        )
        return fp

    def get_formula_explicit_dimension(self, dict):
        qname_object = self.get_qname_object(dict["qname_dimension"])
        qname_member_object = (
            self.get_qname_object(dict["qname_member"])
            if dict["qname_member"]
            else dict["qname_member_expression"]
        )
        fed = FormulaExplicitDimension(
            qname_dimension=qname_object,
            qname_member=qname_member_object,
            omit=dict["omit"],
        )
        return fed

    def get_formula_typed_dimension(self, dict):
        qname_object = self.get_qname_object(dict["qname_dimension"])
        ftd = FormulaTypedDimension(
            qname_dimension=qname_object, xpath=dict["xpath"], value=dict["value"]
        )
        return ftd

    def get_ruleset(self, dict):
        ruleset = RuleSet(tag=dict["tag"])
        for formula_period in dict["formula_periods"]:
            ruleset.formulas.append(self.get_formula_period(formula_period))
        for ftd in dict["formula_typed_dimensions"]:
            ruleset.formulas.append(self.get_formula_typed_dimension(ftd))
        for fed in dict["formula_explicit_dimensions"]:
            ruleset.formulas.append(self.get_formula_explicit_dimension(fed))
        return ruleset

    def get_filter(self, dict):
        unique = dict["id"] if dict.get("id") else dict["label"]
        if unique in self._filters.keys():
            return self._filters[unique]
        if dict["type"] == "FilterAspectCover":
            filter = FilterAspectCover(
                id=dict["id"],
                xlink_label=dict["label"],
                aspect=dict["aspect"],
                include_qname=dict["include_qname"],
            )
        elif dict["type"] == "FilterBooleanOr":
            filter = FilterBooleanOr(
                id=dict["id"], xlink_label=dict["label"], subfilters=dict["subfilters"]
            )
        elif dict["type"] == "FilterConceptBalance":
            filter = FilterConceptBalance(
                balance_type=dict["balance_type"],
                xlink_label=dict["label"],
                id=dict["id"],
            )
        elif dict["type"] == "FilterConceptDatatype":
            filter = FilterConceptDatatype(
                strict=dict["strict"],
                id=dict["id"],
                xlink_label=dict["label"],
                type_qname=dict["type_qname"],
                type_qname_expression=dict["type_qname_expression"],
            )
        elif dict["type"] == "FilterConceptName":
            qname_object = (
                self.get_qname_object(dict["qname"])
                if dict["qname"]
                else dict["qname_expression"]
            )
            filter = FilterConceptName(
                qname=qname_object,
                qname_expression=dict["qname_expression"],
                id=dict["id"],
                xlink_label=dict["label"],
            )
        elif dict["type"] == "FilterConceptRelation":
            qname_object = (
                self.get_qname_object(dict["qname"])
                if dict["qname"]
                else dict["qname_expression"]
            )
            filter = FilterConceptRelation(
                id=dict["id"],
                xlink_label=dict["label"],
                qname=qname_object,
                linkrole=self.DTS.roletypes[dict["linkrole_role_uri"]]
                if dict["linkrole_role_uri"]
                else None,
                arcrole=dict["arcrole"],
                axis=dict["axis"],
            )
        elif dict["type"] == "FilterConceptSubstitutionGroup":
            filter = FilterConceptSubstitutionGroup(
                id=dict["id"],
                xlink_label=dict["label"],
                type_qname=dict["type_qname"],
                type_qname_expression=dict["type_qname_expression"],
                strict=dict["strict"],
            )
        elif dict["type"] == "FilterDimensionExplicit":
            qname_dimension_object = self.get_qname_object(dict["qname_dimension"])
            qname_member_object = (
                self.get_qname_object(dict["qname_member"])
                if dict["qname_member"]
                else None
            )

            filter = FilterDimensionExplicit(
                id=dict["id"],
                xlink_label=dict["label"],
                qname_expression=dict["qname_expression"],
                qname_dimension=qname_dimension_object,
                qname_member=qname_member_object,
            )

        elif dict["type"] == "FilterDimensionTyped":
            qname_dimension_object = self.get_qname_object(dict["qname_dimension"])
            filter = FilterDimensionTyped(
                id=dict["id"],
                xlink_label=dict["label"],
                qname_dimension=qname_dimension_object,
                test=dict["test"],
            )
        elif dict["type"] == "FilterPeriodEnd":
            filter = FilterPeriodEnd(
                id=dict["id"],
                xlink_label=dict["label"],
                date=dict["date"],
                time=dict["time"],
            )
        elif dict["type"] == "FilterPeriodInstant":
            filter = FilterPeriodInstant(
                id=dict["id"],
                xlink_label=dict["label"],
                date=dict["date"],
                time=dict["time"],
            )
        elif dict["type"] == "FilterPeriodInstantDuration":

            filter = FilterPeriodInstantDuration(
                id=dict["id"],
                xlink_label=dict["label"],
                boundary=dict["boundary"],
                variable=dict["variable"],  # todo: get the varArc object?
            )
        else:
            return None
        filter.arc_cover = dict["arc_cover"]
        filter.arc_complement = dict["arc_complement"]
        filter.arc_priority = dict["arc_priority"]
        filter.arc_order = dict["arc_order"]
        self._filters[unique] = filter
        return filter

    def get_qname_object(self, qname):
        for concept in self._concepts.values():
            if concept.ns_prefix and f"{concept.ns_prefix}:{concept.name}" == qname:
                return concept
        return None

    def post_proces(self):
        # some strings must be replaced by objects
        for linkrole in self.DTS.roletypes.values():
            if linkrole.hypercube:
                for dimension in linkrole.hypercube.dimensions:
                    if dimension.target_linkrole:
                        if self.DTS.roletypes.get(dimension.target_linkrole):
                            dimension.target_linkrole = self.DTS.roletypes.get(
                                dimension.target_linkrole
                            )
                        else:
                            print("may-day")


def main():
    # object_maker = SqlToDts("abm-rpt-aedes-benchmark.xsd")
    object_maker = SqlToDts("kvk-rpt-jaarverantwoording-2021-nlgaap-micro.xsd")
    # object_maker = SqlToDts("kvk-rpt-jaarverantwoording-2021-nlgaap-groot.xsd")
    print(object_maker.DTS)


if __name__ == "__main__":
    main()
