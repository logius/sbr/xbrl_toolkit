from discover_xbrl.conf.conf import Config

global_conf = Config()  # reads current running configuration


def get_connection(configfile=None, filename=None, conf=None, nt_version=None):
    """
    Gives back a database connection, when called without parameters the default connection will be returned
    This can be overruled in different ways:
    @param configfile -> string path to configuration to use
    @param filename: str sqlite only, path to sqlite database file on disk
    @param conf, like configfile, but instead already initialized object is consumed
    @param nt_version: str Determines DBB file/name based on confg-defaults and nt-version
    """
    engine = global_conf.db["type"] if global_conf.db.get("type") else "sqlite"
    if engine == "postgres":
        from discover_xbrl.sbrnl_modules.db.postgres.db_connection import DBConnection

        return DBConnection(
            nt_version=nt_version, configfile=configfile, dbname=filename, conf=conf
        )
    else:
        from discover_xbrl.sbrnl_modules.db.sqlite.db_connection import DBConnection

        return DBConnection(
            nt_version=nt_version, configfile=configfile, filename=filename, conf=conf
        )
