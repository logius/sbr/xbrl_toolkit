create table if not exists dts
(
    name        text not NULL,
    shortname   text,
    nice_name   text,
    description text
);
create unique index if not exists dts_name on dts (name);


create table if not exists namespace
(
    prefix text not NULL,
    uri    text not NULL
);
create index if not exists prefix on namespace (prefix);
create index if not exists uri on namespace (uri);


create table if not exists dts_namespace
(
    namespace_prefix text not NULL,
    dts_name         text not NULL,
    foreign key (dts_name) references dts (name),
    foreign key (namespace_prefix) references namespace (prefix)
);
create index if not exists dts_namespace_prefix on dts_namespace (namespace_prefix);
create index if not exists dts_namespace_name on dts_namespace (dts_name);


create table if not exists label
(
    id         text,
    link_label text not NULL,
    link_role  text not NULL,
    lang       text not NULL,
    label_text text not NULL
);
create unique index if not exists label_unique on label (id, link_label, lang, link_role);
create index if not exists link_label on label (link_label);
create index if not exists label_id on label (id);
create index if not exists label_label_text on label (label_text);
create index if not exists label_lang on label (lang);
create index if not exists label_link_role on label (link_role);



create table if not exists dts_label
(
    label_link_label text not null,
    label_id         text not null,
    label_lang       text not null,
    label_link_role  text not null,
    dts_name         text not null,
    foreign key (label_id, label_link_label, label_lang, label_link_role) references label (id, link_label, lang, link_role),
    foreign key (dts_name) references dts (name)
);
create index if not exists dts_label_label on dts_label (label_id, label_link_label, label_lang);
create index if not exists dts_label_dts_name on dts_label (dts_name);


create table if not exists itemtype
(
    id                     text,
    name                   text,
    domain                 text,
    base                   text,
    inheritance_type       text,
    restriction_type       text,
    restriction_length     int,
    restriction_pattern    text,
    restriction_max_length int,
    restriction_min_length int,
    total_digits           int,
    fraction_digits        int,
    min_inclusive          text
);
create index if not exists itemtype_name on itemtype (name);


create table if not exists enumeration_option
(
    id    text,
    value text
);
create index if not exists enumeration_option_id on enumeration_option (id);


create table if not exists enumeration_option_label
(
    enumeration_option_id text,
    label_link_label      text,
    label_id              text,
    label_lang            text,
    label_link_role       text,
    foreign key (enumeration_option_id) references enumeration_option (id),
    foreign key (label_id, label_link_label, label_lang, label_link_role) references label (id, link_label, lang, link_role)
);
create index if not exists enumeration_option_label_option_id
    on enumeration_option_label (enumeration_option_id);
create index if not exists enumeration_option_label_unique
    on enumeration_option_label (label_id, label_link_label, label_lang);


create table if not exists itemtype_enumeration_option
(
    itemtype_name         text,
    enumeration_option_id text,
    foreign key (itemtype_name) references itemtype (name),
    foreign key (enumeration_option_id) references enumeration_option (id)
);
create index if not exists itemtype_enumeration_option_itemtype_name
    on itemtype_enumeration_option (itemtype_name);
create index if not exists itemtype_enumeration_option_enumeration_option_id
    on itemtype_enumeration_option (enumeration_option_id);


create table if not exists dts_itemtype
(
    dts_name      text not NULL,
    itemtype_name text not NULL,
    foreign key (itemtype_name) references itemtype (name),
    foreign key (dts_name) references dts (name)
);
create index if not exists dts_itemtype_dts_name on dts_itemtype (dts_name);
create index if not exists dts_itentype_itemtype_name on dts_itemtype (itemtype_name);


create table if not exists concept
(
    id                 text not NULL,
    name               text not NULL,
    balance            text,
    nillable           integer,
    abstract           integer,
    periodtype         text,
    ns_prefix          text,
    namespace          text,
    substitution_group text,
    typed_domainref    text,
    itemtype_name      text,
    foreign key (itemtype_name) references itemtype (name),
    foreign key (ns_prefix) references namespace (prefix)
);
create unique index if not exists concept_id on concept (id);
create unique index if not exists concept_name_ns on concept (name, ns_prefix);


create table if not exists dts_concept
(
    dts_name            text not NULL,
    concept_id          text not NULL,
    directly_referenced int,
    p_link              int,
    d_link              int,
    t_link              int,
    foreign key (concept_id) references concept (id),
    foreign key (dts_name) references dts (name)
);
create unique index if not exists dts_concept_unique on dts_concept (dts_name, concept_id);
create index if not exists dts_concept_concept_id on dts_concept (concept_id);
create index if not exists dts_concept_dts_name on dts_concept (dts_name);


create table if not exists concept_label
(
    concept_id       text not NULL,
    label_link_label text not null,
    label_id         text not null,
    label_lang       text not null,
    label_link_role  text not null,
    dts_name         text not null,
    foreign key (label_id, label_link_label, label_lang, label_link_role)
        references label (id, link_label, lang, link_role),
    foreign key (concept_id) references concept (id),
    foreign key (dts_name) references dts (name)
);
create unique index if not exists concept_label_unique on concept_label
    (concept_id, label_id, label_link_label, label_lang, dts_name, label_link_role);
create index if not exists concept_label_concept_id on concept_label (concept_id);
create index if not exists concept_label_link_label on concept_label (label_link_label);
create index if not exists concept_label_id on concept_label (label_id);
create index if not exists concept_label_dts_name on concept_label (dts_name);


create table if not exists reference
(
    id           text not NULL,
    label        text,
    name         text,
    number       text,
    issuedate    text,
    chapter      text,
    article      text,
    note         text,
    section      text,
    subsection   text,
    publisher    text,
    paragraph    text,
    subparagraph text,
    clause       text,
    subclause    text,
    appendix     text,
    example      text,
    page         text,
    exhibit      text,
    footnote     text,
    sentence     text,
    uri          text,
    uridate      text
);
create index if not exists reference_id on reference (id);
create index if not exists reference_name on reference (name);


create table if not exists concept_reference
(
    concept_id   text not NULL,
    reference_id text not NULL,
    foreign key (concept_id) references concept (id),
    foreign key (reference_id) references reference (id)
);
create index if not exists concept_reference_reference_id on concept_reference (reference_id);
create index if not exists concept_reference_concept_id on concept_reference (concept_id);


create table if not exists used_on
(
    used_on text unique not null
);
create index if not exists used_on_used_on on used_on (used_on);


create table if not exists linkrole
(
    rowid               integer primary key asc,
    id                  text,
    role_uri            text not NULL,
    definition          text,
    dts_name            text,
    directly_referenced int,
    _order              int,
    --- used ons!
    foreign key (dts_name) references dts (name)
);
create index if not exists linkrole_role_uri on linkrole (role_uri);
create index if not exists linkrole_dts_name on linkrole (dts_name);


create table if not exists linkrole_label
(
    label_link_label text not null,
    label_id         text not null,
    label_lang       text not null,
    label_link_role  text not null,
    linkrole_rowid   text not null,
    foreign key (label_id, label_link_label, label_lang, label_link_role)
        references label (id, link_label, lang, link_role),
    foreign key (linkrole_rowid) references linkrole (rowid)
);
create index if not exists linkrole_label_label on linkrole_label (label_link_label);
create index if not exists linkrole_label_assertion_id on linkrole_label (linkrole_rowid);


create table if not exists linkrole_used_on
(
    linkrole_rowid  integer not null,
    used_on_used_on text    not null,
    foreign key (linkrole_rowid) references linkrole (rowid),
    foreign key (used_on_used_on) references used_on (used_on)
);
create index if not exists linkrole_used_on_linkrole_rowid on linkrole_used_on (linkrole_rowid);
create index if not exists linkrole_used_on_used_on on linkrole_used_on (used_on_used_on);


create table if not exists hypercube
(
    linkrole_rowid  integer not NULL,
    hc_role_uri     text    not NULL,
    hc_type         text,
    context_element text,
    closed          text
);
create index if not exists hypercube_hc_role_uri on hypercube (hc_role_uri);
create index if not exists hypercube_linkrole_rowid on hypercube (linkrole_rowid);


--- Here we make the rowid explicit because we want to us it as a foreign_key
create table if not exists dimension
(
    rowid                    integer primary key asc,
    concept_id               text not NULL,
    hypercube_linkrole_rowid integer,
    target_linkrole          text,
    linkrole_rowid           integer,

    foreign key (concept_id) references concept (id),
    foreign key (hypercube_linkrole_rowid) references hypercube (linkrole_rowid),
    foreign key (linkrole_rowid) references linkrole (rowid)
);
create index if not exists dimension_concept_id on dimension (concept_id);
create index if not exists dimension_target_linkrole on dimension (target_linkrole);
create index if not exists dimension_linkrole_rowid on dimension (linkrole_rowid);


create table if not exists domain
(
    rowid           integer primary key asc,
    concept_id      text not NULL,
    target_linkrole text,
    usable          int,
    dimension_rowid int,
    foreign key (dimension_rowid) references dimension (rowid),
    foreign key (concept_id) references concept (id)
);
create index if not exists domain_concept_id on domain (concept_id);


create table if not exists member
(
    rowid           integer primary key asc,
    concept_id      text not NULL,
    _order          text,
    usable          int,
    parent_rowid    int,
    parent_type     text,
    target_linkrole text,
    preferred_label text,
    foreign key (concept_id) references concept (id)
);
create index if not exists member_concept_id on member (concept_id);
create index if not exists member_parent on member (parent_rowid, parent_type);


create table if not exists line_item
(
    _order                   text,
    hypercube_linkrole_rowid integer not NULL,
    concept_id               text    not NULL,
    parent_concept_id        text,
    foreign key (concept_id) references concept (id),
    foreign key (parent_concept_id) references concept (id)
);
create index if not exists line_item_concept on line_item (concept_id);
create index if not exists line_item_hypercube on line_item (hypercube_linkrole_rowid);


create table if not exists linkrole_reference
(
    reference_id   text    not NULL,
    linkrole_rowid integer not NULL,
    foreign key (reference_id) references reference (id),
    foreign key (linkrole_rowid) references linkrole (rowid)
);
create index if not exists linkrole_reference_reference_id on linkrole_reference (reference_id);
create index if not exists linkrole_reference_linkrole_rowid on linkrole_reference (linkrole_rowid);


create table if not exists assertion
(
    id                 text not NULL,
    aspect_model       text,
    implicit_filtering text,
    xlink_label        text,
    test               text,
    assertion_type     text,
    severity           text
);
create index if not exists assertion_id on assertion (id);


create table if not exists linkrole_assertion
(
    linkrole_rowid integer not NULL,
    assertion_id   text    not NULL,
    foreign key (linkrole_rowid) references linkrole (rowid),
    foreign key (assertion_id) references assertion (id)
);
create index if not exists linkrole_assertion_assertion_id on linkrole_assertion (assertion_id);
create index if not exists linkrole_assertion_linkrole_rowid on linkrole_assertion (linkrole_rowid);


create table if not exists assertion_label
(
    label_link_label text not null,
    label_id         text not null,
    label_lang       text not null,
    label_link_role  text not null,
    assertion_id     text not null,
    foreign key (label_id, label_link_label, label_lang, label_link_role)
        references label (id, link_label, lang, link_role),
    foreign key (assertion_id) references assertion (id)
);
create index if not exists assertion_label_id on assertion_label (label_id);
create index if not exists assertion_label_label on assertion_label (label_link_label);
create index if not exists assertion_label_assertion_id on assertion_label (assertion_id);


create table if not exists xbrl_table
(
    id                 text not NULL,
    label              text not NULL,
    parent_child_order text not NULL
);
create index if not exists xbrl_table_id on xbrl_table (id);
create index if not exists xbrl_table_link_label on xbrl_table (label);


create table if not exists linkrole_table
(
    linkrole_rowid integer not NULL,
    table_id       text    not NULL,
    foreign key (linkrole_rowid) references linkrole (rowid),
    foreign key (table_id) references xbrl_table (id)
);
create index if not exists linkrole_table_linkrole_rowid on linkrole_table (linkrole_rowid);
create index if not exists linkrole_table_table_id on linkrole_table (table_id);


create table if not exists xbrl_table_label
(
    label_id         text not null,
    label_link_label text not null,
    label_lang       text not null,
    label_link_role  text not null,
    xbrl_table_id    text not null,
    foreign key (label_id, label_link_label, label_lang, label_link_role)
        references label (id, link_label, lang, link_role),
    foreign key (xbrl_table_id) references xbrl_table (id)
);
create index if not exists xbrl_table_label_id on xbrl_table_label (label_id);
create index if not exists xbrl_table_label_label on xbrl_table_label (label_link_label);
create index if not exists xbrl_table_label_table_id on xbrl_table_label (xbrl_table_id);


create table if not exists table_breakdown
(
    id       text      not NULL,
    label    text      not NULL,
    parent_child_order not NULL,
    axis     text      not NULL,
    table_id text      not NULL,
    _order   int,
    foreign key (table_id) references xbrl_table (id)
);
create index if not exists table_breakdown_id on table_breakdown (id);
create index if not exists table_breakdown_table_id on table_breakdown (table_id);


create table if not exists table_rulenode
(
    id                 text not NULL,
    label              text not NULL,
    parent_child_order text,
    merge              int,
    _order             int,
    is_abstract        int,
    parent             text not NULL,
    tagselector        text
);
create index if not exists table_rulenode_id on table_rulenode (id);
create index if not exists table_rulenode_link_label on table_rulenode (label);


create table if not exists table_rulenode_label
(
    label_id          text not null,
    label_link_label  text not null,
    label_lang        text not null,
    label_link_role   text not null,
    table_rulenode_id text not null,
    foreign key (label_id, label_link_label, label_lang, label_link_role)
        references label (id, link_label, lang, link_role),
    foreign key (table_rulenode_id) references table_rulenode (id)
);
create index if not exists table_rulenode_label_label on table_rulenode_label (label_link_label);
create index if not exists table_rulenode_label_table_id on table_rulenode_label (table_rulenode_id);


create table if not exists concept_relationshipnode
(
    id                  text not NULL,
    label               text not NULL,
    parent_child_order  text,
    parent              text,
    tagselector         text,
    _order              int,
    relationship_source text not NULL,
    linkrole_role_uri   text not NULL,
    formula_axis        text,
    generations         text,
    arcrole             text,
    linkname            text,
    arcname             text,

    foreign key (linkrole_role_uri) references linkrole (role_uri),
    foreign key (relationship_source) references concept (id)
);
create index if not exists concept_relationshipnode_id on concept_relationshipnode (id);
create index if not exists concept_relationshipnode_relationship_source on concept_relationshipnode (relationship_source);


create table if not exists aspect_node
(
    id                       text not NULL,
    label                    text not NULL,
    parent_child_order       text,
    parent                   text,
    tagselector              text,
    merge                    int,
    _order                   int,
    is_abstract              int,
    aspect_type              text,
    include_unreported_value int,
    value                    text
);
create index if not exists aspect_node_id on aspect_node (id);
create index if not exists aspect_node_parent on aspect_node (parent);


create table if not exists dimension_relationshipnode
(
    id                  text not NULL,
    label               text not NULL,
    parent_child_order  text,
    parent              text,
    _order              int,
    dimension_id        text not NULL,
    linkrole_role_uri   text not NULL,
    formula_axis        text not NULL,
    generations         text,
    relationship_source text,
    tagselector         text,
    foreign key (dimension_id) references dimension (rowid),
    foreign key (linkrole_role_uri) references linkrole (role_uri)
);
create index if not exists dimension_relationshipnode_id on dimension_relationshipnode (id);
create index if not exists dimension_relationshipnode_parent on dimension_relationshipnode (parent);


create table if not exists ruleset
(
    rowid       integer primary key asc,
    rulenode_id text not NULL,
    tag         text not NULL,
    foreign key (rulenode_id) references table_rulenode (id)
);
create index if not exists ruleset_rulenode_id on ruleset (rulenode_id);


create table if not exists formula_period
(
    rowid          integer primary key asc,
    period_type    text,
    _start         text,
    _end           text,
    value          text,
    parent_ruleset int,
    parent_node    text,
    foreign key (parent_ruleset) references ruleset (rowid)
);
create index if not exists formula_period_parent_node on formula_period (parent_node);
create index if not exists formula_period_parent_ruleset on formula_period (parent_ruleset);


create table if not exists formula_explicit_dimension
(
    rowid                   integer primary key asc,
    qname_dimension         text not NULL,
    qname_member            text,
    qname_member_expression text,
    type_qname              text,
    omit                    int,
    parent_ruleset          int,
    parent_node             text,
    foreign key (parent_ruleset) references ruleset (rowid)
);
create index if not exists formula_explicit_dimension_parent_ruleset
    on formula_explicit_dimension (parent_ruleset);
create index if not exists formula_explicit_dimension_parent_node
    on formula_explicit_dimension (parent_node);


create table if not exists formula_typed_dimension
(
    rowid           integer primary key asc,
    qname_dimension text not NULL,
    xpath           text,
    value           text,
    parent_ruleset  int,
    parent_node     text,
    foreign key (parent_ruleset) references ruleset (rowid)
);


-- all types of variables
create table if not exists variable
(
    id                text,
    type              text not NULL,
    label             text,
    variable_arc_name text,
    bind_as_sequence  text,
    _select           text,
    _matches          text,
    nils              text,
    fallback_value    text
);
create index if not exists variable_id on variable (id);
create index if not exists variable_label on variable (label);


create table if not exists linkrole_variable
(
    linkrole_rowid integer not NULL,
    variable_id    text    not NULL,
    foreign key (linkrole_rowid) references linkrole (rowid),
    foreign key (variable_id) references variable (id)
);
create index if not exists linkrole_variable_linkrole_rowid on linkrole_variable (linkrole_rowid);
create index if not exists linkrole_variable_variable_id on linkrole_variable (variable_id);


create table if not exists parameter
(
    id          text not NULL,
    label       text,
    as_datatype text,
    _select     text,
    name        text,
    required    text
);
create index if not exists parameter_id on parameter (id);


create table if not exists table_parameter
(
    table_id     text not null,
    parameter_id text not null,
    foreign key (table_id) references xbrl_table (id),
    foreign key (parameter_id) references parameter (id)
);
create index if not exists table_parameter_table_id on table_parameter (table_id);
create index if not exists table_parameter_parameter_id on
    table_parameter (parameter_id);


create table if not exists presentation_hierarchy_node
(
    rowid                 integer primary key asc,
    id                    text not NULL,
    node_type             text not NULL,
    concept_id            text,
    linkrole_role_uri     text,
    is_root               int,
    _order                int,
    preferred_label       text,
    parent_rowid          int,
    dts_name              text,
    parent_linkrole_rowid text,
    directly_referenced   int,
    foreign key (concept_id) references concept (id),
    foreign key (linkrole_role_uri) references linkrole (role_uri)
);
create index if not exists presentation_hierarchy_node_id on presentation_hierarchy_node (id);
create index if not exists presentation_hierarchy_node_parent_id
    on presentation_hierarchy_node (parent_rowid);


create table if not exists filter
(
    id                    text not NULL,
    label                 text not NULL,
    type                  text not NULL,
    aspect                text,
    include_qname         text,
    exclude_qname         text,
    qname                 text,
    qname_expression      text,
    period_type           text,
    value                 text,
    qname_member          text,
    qname_dimension       text,
    linkrole_role_uri     text,
    arcrole               text,
    axis                  text,
    balance_type          text,
    strict                text,
    type_qname            text,
    type_qname_expression text,
    test                  text,
    boundary              text,
    date                  text,
    time                  text,
    parent                text,
    variable              text,
    arc_complement        text,
    arc_cover             text,
    arc_order             int,
    arc_priority          text
);
create index if not exists filter_id on filter (id);
create index if not exists filter_label on filter (label);


create table if not exists variable_filter
(
    variable_id text not null,
    filter_id   text not null,
    foreign key (variable_id) references variable (id),
    foreign key (filter_id) references filter (id)
);
create index if not exists variable_filter_variable_id
    on variable_filter (variable_id);
create index if not exists variable_filter_filter_id
    on variable_filter (filter_id);


create table if not exists variableset
(
    name text not null
);
create index if not exists variableset_name on variableset (name);


create table if not exists variableset_variable
(
    variable_id      text not null,
    variableset_name text not null,
    _order           text,
    priority         int,
    foreign key (variable_id) references variable (id),
    foreign key (variableset_name) references variableset (name)
);
create index if not exists variableset_variable_variable_id on variableset_variable (variable_id);
create index if not exists variableset_variable_variableset_name on variableset_variable (variableset_name);


create table if not exists assertion_variableset
(
    assertion_id     text not null,
    variableset_name text not null,
    foreign key (assertion_id) references assertion (id),
    foreign key (variableset_name) references variableset (name)
);
create index if not exists assertion_variableset_name on assertion_variableset (variableset_name);
create index if not exists assertion_variableset_variable_id on assertion_variableset (assertion_id);


create table if not exists table_filter
(
    table_id  text not null,
    filter_id text not null,
    foreign key (table_id) references xbrl_table (id),
    foreign key (filter_id) references filter (id)
);


create table if not exists arc
(
    rowid          integer primary key asc,
    linkrole_rowid integer not null,
    arc_type       text    not null,
    arcrole        text,
    from_id        text,
    from_type      text,
    to_id          text,
    to_type        text,
    _order         real,
    name           text,
    dts_name       varchar not NULL,
    foreign key (dts_name) references dts (name) on delete cascade on update cascade,
    foreign key (linkrole_rowid) references linkrole (rowid)
);
create unique index if not exists arc_unique on arc (linkrole_rowid, arc_type, arcrole, from_id, to_id);
create index if not exists arc_linkrole_rowid on arc (linkrole_rowid);
create index if not exists arc_arc_type on arc (arc_type);
create index if not exists arc_arcrole on arc (arcrole);
create index if not exists arc_from on arc (from_id);
create index if not exists arc_to on arc (to_id);
create index if not exists arc_dts_name on arc (dts_name);


create table if not exists mutation
(
    rowid         integer primary key asc,
    run_id        text not null,
    object_type   text not null,
    object_id     text not null,
    mutation_type text not null,
    old_version   text not null
);

create unique index if not exists mutation_unique on mutation (run_id, object_type, object_id);
create index if not exists mutation_object on mutation (object_type, object_id);
