from os.path import join, dirname
import sqlite3

from discover_xbrl.conf.conf import Config


class DBConnection:
    def __init__(
        self,
        nt_version=None,
        filename=None,
        auto_create=True,
        configfile=None,
        conf=None,
    ):
        self.nt_version = nt_version
        self.conf = Config(configfile)
        self.connection = None
        self.engine = "sqlite"
        self.dbname = None
        self._filename = filename
        self._auto_create = auto_create
        if nt_version:
            if self.conf.db.get("files") and self.conf.db["files"].get(nt_version):
                self._filename = self.conf.db["files"].get(nt_version)
            else:
                raise ValueError(f"NT Versie: {nt_version} niet aanwezig")
        self._connect()

    @staticmethod
    def rewrite_query(query):
        return query.replace("%s", "?")

    def _connect(self):
        if hasattr(self.conf, "db"):
            if self._filename:
                filename = self._filename
            else:
                filename = self.conf.db.get("file", None)
            if filename:
                if not filename[:1] == "/":
                    filename = join(self.conf.project_root, filename)
                try:
                    self.connection = sqlite3.connect(filename)
                    if self._auto_create:
                        cursor = self.connection.cursor()
                        cursor.execute(
                            "select name from sqlite_master where type = 'table'"
                        )
                        tables = cursor.fetchall()
                        if not len(tables):
                            print(f"Creating database schema in: {filename}")
                            with open(
                                join(dirname(__file__), "create_db.sql")
                            ) as ddl_file:
                                ddl = ddl_file.read()
                                if ddl:
                                    cursor.executescript(ddl)
                except sqlite3.Error:
                    print(f"Could not connect: {filename}")
                self.dbname = filename
                return True
            print(f"Please configure db['file'] in config.yaml")
        else:
            print(f"Please configure 'db' in config.yaml")
