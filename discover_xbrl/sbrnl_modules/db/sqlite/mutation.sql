create table if not exists mutation
(
    rowid         integer primary key asc,
    run_id        text not null,
    object_type   text not null,
    object_id     text not null,
    mutation_type text not null,
    old_version   text not null
);

create unique index if not exists mutation_unique on mutation (run_id, object_type, object_id);
create index if not exists mutation_object on mutation (object_type, object_id);
