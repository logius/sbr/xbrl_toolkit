class LineItem:
    def __init__(
        self,
        db=None,
        order=None,
        hypercube_linkrole_rowid=None,
        concept_id=None,
        parent_concept_id=None,
        bind_to=None,
        auto_commit=True,
    ):
        self.db = db
        self.order = order
        self.hypercube_linkrole_rowid = hypercube_linkrole_rowid
        self.concept_id = concept_id
        self.parent_concept_id = parent_concept_id
        self._auto_commit = auto_commit
        self._bind_to = bind_to

        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select _order, hypercube_linkrole_rowid, concept_id from line_item "
            " where hypercube_linkrole_rowid = %s and concept_id = %s"
        )
        self.cursor.execute(query, (self.hypercube_linkrole_rowid, self.concept_id))
        result = self.cursor.fetchone()
        if result is None:
            self._create()

    def _create(self):
        query = self.db.rewrite_query(
            "insert into line_item (_order, hypercube_linkrole_rowid, concept_id, parent_concept_id) values "
            " (%s, %s, %s, %s)"
        )
        self.cursor.execute(
            query,
            (
                self.order,
                self.hypercube_linkrole_rowid,
                self.concept_id,
                self.parent_concept_id,
            ),
        )
        if self._auto_commit:
            self.db.connection.commit()
