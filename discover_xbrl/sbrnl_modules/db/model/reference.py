class Reference:
    def __init__(
        self,
        db=None,
        id=None,
        label=None,
        name=None,
        number=None,
        issuedate=None,
        article=None,
        chapter=None,
        note=None,
        section=None,
        subsection=None,
        publisher=None,
        paragraph=None,
        subparagraph=None,
        clause=None,
        subclause=None,
        appendix=None,
        example=None,
        page=None,
        exhibit=None,
        footnote=None,
        sentence=None,
        uri=None,
        uridate=None,
        bind_to=None,
        auto_commit=True,
    ):
        self.db = db
        self.id = id if id is not None else label
        self.label = label
        self.name = name
        self.number = number
        self.issuedate = issuedate
        self.chapter = chapter
        self.article = article
        self.note = note
        self.section = section
        self.subsection = subsection
        self.publisher = publisher
        self.paragraph = paragraph
        self.subparagraph = subparagraph
        self.clause = clause
        self.subclause = subclause
        self.appendix = appendix
        self.example = example
        self.page = page
        self.exhibit = exhibit
        self.footnote = footnote
        self.sentence = sentence
        self.uri = uri
        self.uridate = uridate
        self._auto_commit = auto_commit
        self._bind_to = bind_to

        if not self.db or not self.db.connection:
            raise ConnectionError("No sqlite connection provided!")
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select id, label, name, number, issuedate, chapter, article,"
            " note, section, subsection, publisher, paragraph, subparagraph, clause, "
            "subclause, appendix, example, page, exhibit, footnote, sentence, uri, uridate "
            "from reference where id = %s"
        )
        self.cursor.execute(query, (self.id,))
        result = self.cursor.fetchone()
        if result is None:
            self._create()
        if self._bind_to:
            for other, value in self._bind_to.items():
                if other == "dts":
                    pass

    def _create(self):
        query = self.db.rewrite_query(
            "insert into reference (id, label, name, number, issuedate, chapter,"
            " article, note, section, subsection, publisher, paragraph, subparagraph,"
            " clause, subclause, appendix, example, page, exhibit, footnote, sentence, uri, uridate) "
            " values(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        )
        self.cursor.execute(
            query,
            (
                self.id,
                self.label,
                self.name,
                self.number,
                self.issuedate,
                self.chapter,
                self.article,
                self.note,
                self.section,
                self.subsection,
                self.publisher,
                self.paragraph,
                self.subparagraph,
                self.clause,
                self.subclause,
                self.appendix,
                self.example,
                self.page,
                self.exhibit,
                self.footnote,
                self.sentence,
                self.uri,
                self.uridate,
            ),
        )
