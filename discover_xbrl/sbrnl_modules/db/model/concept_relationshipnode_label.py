# deprecated!
class ConceptRelationshipNodeLabel:
    def __init__(
        self,
        db=None,
        concept_relationshipnode_id=None,
        label_link_label=None,
        auto_commit=True,
    ):
        self.db = db
        self.concept_relationshipnode_id = concept_relationshipnode_id
        self.label_link_label = label_link_label
        self._auto_commit = auto_commit
        if not self.db or not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select concept_relationshipnode_id, label_link_label from concept_relationshipnode_label "
            "where concept_relationshipnode_id = ? and label_link_label = %s"
        )
        self.cursor.execute(
            query, (self.concept_relationshipnode_id, self.label_link_label)
        )
        result = self.cursor.fetchone()
        if result is None:
            self._create()

    def _create(self):
        self.cursor.execute(
            "insert into concept_relationshipnode_label "
            "(concept_relationshipnode_id, label_link_label) values (?, ?)",
            (self.concept_relationshipnode_id, self.label_link_label),
        )
        if self._auto_commit:
            self.db.connection.commit()
