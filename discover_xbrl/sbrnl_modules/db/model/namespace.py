from discover_xbrl.sbrnl_modules.db.model.dts_namespace import DTSNamespace


class Namespace:
    def __init__(self, db=None, prefix=None, uri=None, bind_to=None, auto_commit=True):
        self.db = db
        self.prefix = prefix
        self.uri = uri
        self._bind_to = bind_to
        self._auto_commit = auto_commit
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select prefix, uri from namespace where prefix = %s"
        )
        self.cursor.execute(query, (self.prefix,))
        result = self.cursor.fetchone()
        if result is None:
            self._create()
        else:
            self.prefix = result[0]
            self.uri = result[1]
        if self._bind_to:
            for other, value in self._bind_to.items():
                if other == "dts":
                    dts_namespace = DTSNamespace(
                        db=self.db,
                        dts_name=value,
                        namespace_prefix=self.prefix,
                        auto_commit=self._auto_commit,
                    )
                    dts_namespace.find_or_create()

    def _create(self):
        query = self.db.rewrite_query(
            "insert into namespace (prefix, uri) values (%s, %s)"
        )
        self.cursor.execute(query, (self.prefix, self.uri))
        if self._auto_commit:
            self.db.connection.commit()
