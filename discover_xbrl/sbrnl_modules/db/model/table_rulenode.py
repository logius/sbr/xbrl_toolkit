class TableRuleNode:
    def __init__(
        self,
        db=None,
        id=None,
        label=None,
        parent_child_order=None,
        parent=None,
        tagselector=None,
        order=None,
        merge=None,
        is_abstract=None,
        auto_commit=True,
        bind_to=None,
    ):
        self.db = db
        self.id = id
        self.label = label
        self.parent_child_order = parent_child_order
        self.parent = parent
        self.tagselector = tagselector
        self.order = order
        self.merge = merge
        self.is_abstract = is_abstract
        self._bind_to = bind_to
        self._auto_commit = auto_commit
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select id, label, parent_child_order, parent, tagselector, merge, _order, is_abstract "
            "from table_rulenode where id = %s"
        )
        self.cursor.execute(query, (self.id,))
        result = self.cursor.fetchone()
        if result is None:
            self._create()

    def _create(self):
        query = self.db.rewrite_query(
            "insert into table_rulenode (id, label, parent_child_order, merge, "
            "_order, is_abstract, parent, tagselector) "
            " values (%s, %s, %s, %s, %s, %s, %s, %s)"
        )
        self.cursor.execute(
            query,
            (
                self.id,
                self.label,
                self.parent_child_order,
                self.merge,
                self.order,
                self.is_abstract,
                self.parent,
                self.tagselector,
            ),
        )
        if self._auto_commit:
            self.db.connection.commit()
