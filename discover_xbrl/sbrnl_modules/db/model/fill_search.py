from discover_xbrl.sbrnl_modules.db.connection import get_connection


class FillSearchTable:
    def __init__(self):
        self.db = get_connection()
        self.cursor = self.db.connection.cursor()
        self.table_list = [
            "aspect_node",
            "assertion",
            "concept",
            "concept_relationshipnode",
            "dimension",
            "dimension_relationshipnode",
            "domain",
            "dts",
            "filter",
            "formula_period",
            "hypercube",
            "label",
            "linkrole",
            "reference",
            "table_breakdown",
            "table_rulenode",
            "xbrl_table",
        ]

    def fill_index(self):
        for tab in self.table_list:
            payload = self.create_sql(tab)
            #  print(payload)
            self.cursor.execute(payload)
        self.db.connection.commit()

    def create_sql(self, tab):
        #  pragme table_info geeft voor elke kolom in de tabel een row terug
        #  https://www.sqlite.org/pragma.html#pragma_table_info
        self.cursor.execute(f"PRAGMA table_info({tab})")
        rows = self.cursor.fetchall()
        text_fields = [f"IFNULL({row[1]}, '')" for row in rows if row[2] == "text"]
        text_fields = " || ' '  || ".join(text_fields)
        return (
            f"INSERT INTO search_index SELECT {text_fields}, '{tab}', rowid  FROM {tab}"
        )


def main():
    fs = FillSearchTable()
    fs.fill_index()


if __name__ == "__main__":
    main()
