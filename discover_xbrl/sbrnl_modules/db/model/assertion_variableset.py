class AssertionVariableset:
    def __init__(
        self, db=None, assertion_id=None, variableset_name=None, auto_commit=True
    ):
        self.db = db
        self.assertion_id = assertion_id
        self.variableset_name = variableset_name
        self._auto_commit = auto_commit
        if not self.db or not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select assertion_id, variableset_name from assertion_variableset "
            "where assertion_id = %s and variableset_name = %s"
        )
        self.cursor.execute(query, (self.assertion_id, self.variableset_name))
        result = self.cursor.fetchone()
        if result is None:
            self._create()

    def _create(self):
        query = self.db.rewrite_query(
            "insert into assertion_variableset (assertion_id, variableset_name) values (%s, %s)"
        )
        self.cursor.execute(query, (self.assertion_id, self.variableset_name))
        if self._auto_commit:
            self.db.connection.commit()
