class DimensionRelationshipNode:
    def __init__(
        self,
        db=None,
        id=None,
        label=None,
        parent_child_order=None,
        parent=None,
        order=None,
        dimension_id=None,
        relationship_source=None,
        formula_axis=None,
        generations=None,
        tagselector=None,
        linkrole_role_uri=None,
        auto_commit=True,
        bind_to=None,
    ):
        self.db = db
        self.id = id
        self.label = label
        self.parent_child_order = parent_child_order
        self.parent = parent
        self.tagselector = tagselector
        self.order = order
        self.dimension_id = dimension_id
        self.relationship_source = relationship_source
        self.linkrole_role_uri = linkrole_role_uri
        self.formula_axis = formula_axis
        self.generations = generations
        self._bind_to = bind_to
        self._auto_commit = auto_commit
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select id, label, parent_child_order, parent, _order, dimension_id, linkrole_role_uri, "
            "formula_axis, generations, relationship_source, tagselector "
            "from dimension_relationshipnode where id = %s"
        )
        self.cursor.execute(query, (self.id,))
        result = self.cursor.fetchone()
        if result is None:
            self._create()

    def _create(self):
        query = self.db.rewrite_query(
            "insert into dimension_relationshipnode (id, label, parent_child_order, parent, _order, "
            "dimension_id, linkrole_role_uri, formula_axis, generations, relationship_source, tagselector) "
            " values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        )
        self.cursor.execute(
            query,
            (
                self.id,
                self.label,
                self.parent_child_order,
                self.parent,
                self.order,
                self.dimension_id,
                self.linkrole_role_uri,
                self.formula_axis,
                self.generations,
                self.relationship_source,
                self.tagselector,
            ),
        )
        if self._auto_commit:
            self.db.connection.commit()
