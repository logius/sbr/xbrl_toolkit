class LinkroleVariable:
    def __init__(
        self,
        db=None,
        linkrole_rowid=None,
        variable_id=None,
        auto_commit=None,
        bind_to=None,
    ):
        self.db = db
        self.linkrole_rowid = linkrole_rowid
        self.variable_id = variable_id
        self._auto_commit = auto_commit
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select linkrole_rowid, variable_id from linkrole_variable "
            "where linkrole_rowid = %s and variable_id = %s "
        )
        self.cursor.execute(query, (self.linkrole_rowid, self.variable_id))
        result = self.cursor.fetchone()
        if result is None:
            self._create()

    def _create(self):
        query = self.db.rewrite_query(
            "insert into linkrole_variable (linkrole_rowid, variable_id) values (%s, %s)"
        )
        self.cursor.execute(query, (self.linkrole_rowid, self.variable_id))
        if self._auto_commit:
            self.db.connection.commit()
