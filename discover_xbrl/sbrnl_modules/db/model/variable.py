from discover_xbrl.sbrnl_modules.db.model.linkrole_variable import LinkroleVariable
from discover_xbrl.sbrnl_modules.db.model.variableset_variable import (
    VariablesetVariable,
)


class Variable:
    def __init__(
        self,
        db=None,
        id=None,
        type=None,
        label=None,
        bind_as_sequence=None,
        select=None,
        matches=None,
        nils=None,
        fallback_value=None,
        bind_to=None,
        auto_commit=True,
    ):
        self.db = db
        self.id = id
        self.type = type
        self.label = label
        self.bind_as_sequence = bind_as_sequence
        self.select = select
        self.matches = matches
        self.nils = nils
        self.fallback_value = fallback_value
        self._bind_to = bind_to
        self._auto_commit = auto_commit
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select id, type, label, bind_as_sequence, _select, _matches, nils, fallback_value "
            "from variable where id = %s"
        )
        self.cursor.execute(query, (self.id,))
        result = self.cursor.fetchone()
        if result is None:
            self._create()
        if self._bind_to:
            for other, value in self._bind_to.items():
                if other == "linkrole":
                    linkrole_variable = LinkroleVariable(
                        db=self.db,
                        linkrole_rowid=value,
                        variable_id=self.id,
                        auto_commit=self._auto_commit,
                    )
                    linkrole_variable.find_or_create()
                elif other == "variableset":
                    variableset_variable = VariablesetVariable(
                        db=self.db,
                        variable_id=self.id,
                        variableset_name=value,
                        auto_commit=self._auto_commit,
                    )
                    variableset_variable.find_or_create()

    def _create(self):
        query = self.db.rewrite_query(
            "insert into variable (id, type, label, bind_as_sequence, _select, "
            "_matches, nils, fallback_value) "
            "values (%s, %s, %s, %s, %s, %s, %s, %s)"
        )
        self.cursor.execute(
            query,
            (
                self.id,
                self.type,
                self.label,
                self.bind_as_sequence,
                self.select,
                self.matches,
                self.nils,
                self.fallback_value,
            ),
        )
        if self._auto_commit:
            self.db.connection.commit()
