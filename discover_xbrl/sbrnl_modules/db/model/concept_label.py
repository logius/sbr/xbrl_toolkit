class ConceptLabel:
    def __init__(
        self,
        db=None,
        concept_id=None,
        label_id=None,
        label_link_label=None,
        label_lang=None,
        label_link_role=None,
        dts_name=None,
        auto_commit=True,
    ):
        self.db = db
        self.concept_id = concept_id
        self.label_id = label_id
        self.label_link_label = label_link_label
        self.label_lang = label_lang
        self.label_link_role = label_link_role
        self.dts_name = dts_name
        self._auto_commit = auto_commit
        if not self.db or not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select concept_id, label_id, label_link_label, label_lang, dts_name, label_link_role "
            "from concept_label where concept_id = %s and label_id = %s and label_link_label = %s "
            "and label_lang = %s and dts_name = %s and label_link_role = %s"
        )
        self.cursor.execute(
            query,
            (
                self.concept_id,
                self.label_id,
                self.label_link_label,
                self.label_lang,
                self.dts_name,
                self.label_link_role,
            ),
        )
        result = self.cursor.fetchone()
        if result is None:
            self._create()

    def _create(self):
        query = self.db.rewrite_query(
            "insert into concept_label (concept_id, label_id, label_link_label, label_lang, dts_name, label_link_role) "
            "values (%s, %s, %s, %s, %s, %s)"
        )
        self.cursor.execute(
            query,
            (
                self.concept_id,
                self.label_id,
                self.label_link_label,
                self.label_lang,
                self.dts_name,
                self.label_link_role,
            ),
        )
        if self._auto_commit:
            self.db.connection.commit()
