class Arc:
    def __init__(
        self,
        db=None,
        linkrole_rowid=None,
        arc_type=None,
        arcrole=None,
        from_id=None,
        from_type=None,
        to_id=None,
        to_type=None,
        order=None,
        dts_name=None,
        auto_commit=True,
    ):
        self.db = db
        self.linkrole_rowid = linkrole_rowid
        self.arc_type = arc_type
        self.arcrole = arcrole
        self.from_id = from_id
        self.from_type = from_type
        self.to_id = to_id
        self.to_type = to_type
        self.order = int(float(order)) if order else None
        self.dts_name = dts_name
        self._auto_commit = auto_commit

        if not self.db or not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select rowid, linkrole_rowid, arc_type, arcrole, "
            " from_id, from_type, to_id, to_type, _order, name, dts_name "
            "from arc "
            "where linkrole_rowid = %s "
            "  and arc_type = %s "
            "  and arcrole = %s "
            "  and coalesce(from_id, '') = coalesce(%s, '') "
            "  and coalesce(to_id, '') = coalesce(%s, '') "
        )
        self.cursor.execute(
            query,
            (
                self.linkrole_rowid,
                self.arc_type,
                self.arcrole,
                self.from_id,
                self.to_id,
            ),
        )
        result = self.cursor.fetchone()
        if result is None:
            self._create()
        else:
            self.rowid = result[0]

    def _create(self):
        query = self.db.rewrite_query(
            "insert into arc "
            "(linkrole_rowid, arc_type, arcrole, "
            "from_id, from_type, to_id, to_type, _order, dts_name) values "
            "(%s, %s, %s, %s, %s, %s, %s, %s, %s)"
        )
        if self.db.engine == "postgres":
            query += " returning rowid"

        self.cursor.execute(
            query,
            (
                self.linkrole_rowid,
                self.arc_type,
                self.arcrole,
                self.from_id,
                self.from_type,
                self.to_id,
                self.to_type,
                self.order,
                self.dts_name,
            ),
        )
        if self.db.engine == "postgres":
            result = self.cursor.fetchone()
            self.rowid = result[0]
        else:
            self.rowid = self.cursor.lastrowid
        if self._auto_commit:
            self.db.connection.commit()
