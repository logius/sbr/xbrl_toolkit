class DTSNamespace:
    def __init__(self, db=None, dts_name=None, namespace_prefix=None, auto_commit=True):
        self.db = db
        self.dts_name = dts_name
        self.namespace_prefix = namespace_prefix
        self._auto_commit = auto_commit
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select dts_name, namespace_prefix from dts_namespace where dts_name = %s and namespace_prefix = %s"
        )
        self.cursor.execute(query, (self.dts_name, self.namespace_prefix))
        result = self.cursor.fetchone()
        if result is None:
            self._create()

    def _create(self):
        query = self.db.rewrite_query(
            "insert into dts_namespace (dts_name, namespace_prefix) values (%s, %s)"
        )
        self.cursor.execute(query, (self.dts_name, self.namespace_prefix))
        if self._auto_commit:
            self.db.connection.commit()
