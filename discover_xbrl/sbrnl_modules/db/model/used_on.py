from discover_xbrl.sbrnl_modules.db.model.linkrole_used_on import LinkroleUsedOn


class UsedOn:
    def __init__(self, db=None, used_on=None, bind_to=None, auto_commit=True):
        self.db = db
        self.used_on = used_on
        self._auto_commit = auto_commit
        self._bind_to = bind_to
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query("select used_on from used_on where used_on = %s")
        self.cursor.execute(query, (self.used_on,))
        result = self.cursor.fetchone()
        if result is None:
            self._create()
        if self._bind_to:
            for other, value in self._bind_to.items():
                if other == "linkrole":
                    linkrole_used_on = LinkroleUsedOn(
                        db=self.db,
                        linkrole_rowid=value,
                        used_on=self.used_on,
                        auto_commit=self._auto_commit,
                    )
                    linkrole_used_on.find_or_create()

    def _create(self):
        query = self.db.rewrite_query("insert into used_on (used_on) values (%s)")
        self.cursor.execute(query, (self.used_on,))
