from discover_xbrl.sbrnl_modules.db.model.dts_itemtype import DTSItemType


class ItemType:
    def __init__(
        self,
        db=None,
        id=None,
        name=None,
        domain=None,
        base=None,
        inheritance_type=None,
        restriction_type=None,
        restriction_length=None,
        restriction_pattern=None,
        restriction_max_length=None,
        restriction_min_length=None,
        total_digits=None,
        fraction_digits=None,
        min_inclusive=None,
        auto_commit=True,
        bind_to=None,
    ):
        self.db = db
        self.id = id
        self.name = name
        self.domain = domain
        self.base = base
        self.inheritance_type = inheritance_type
        self.restriction_type = restriction_type
        self.restriction_length = restriction_length
        self.restriction_pattern = restriction_pattern
        self.restriction_max_length = restriction_max_length
        self.restriction_min_length = restriction_min_length
        self.total_digits = total_digits
        self.fraction_digits = fraction_digits
        self.min_inclusive = min_inclusive
        self._auto_coommit = auto_commit
        self._bind_to = bind_to
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select id, name, base, inheritance_type, restriction_type, restriction_length, "
            "restriction_pattern, restriction_max_length, restriction_min_length, total_digits, "
            "fraction_digits, min_inclusive from itemtype where name = %s"
        )
        self.cursor.execute(query, (self.name,))
        result = self.cursor.fetchone()
        if result is None:
            self._create()
        if self._bind_to:
            for other, value in self._bind_to.items():
                if other == "dts":
                    dts_itemtype = DTSItemType(
                        db=self.db,
                        dts_name=value,
                        itemtype_name=self.name,
                        auto_commit=self._auto_coommit,
                    )
                    dts_itemtype.find_or_create()

    def _create(self):
        query = self.db.rewrite_query(
            "insert into itemtype "
            "(id, name, domain, base, inheritance_type, restriction_type, "
            "restriction_length, restriction_pattern, restriction_max_length, "
            "restriction_min_length, total_digits, fraction_digits, min_inclusive) "
            "values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        )
        self.cursor.execute(
            query,
            (
                self.id,
                self.name,
                self.domain,
                self.base,
                self.inheritance_type,
                self.restriction_type,
                self.restriction_length,
                self.restriction_pattern,
                self.restriction_max_length,
                self.restriction_min_length,
                self.total_digits,
                self.fraction_digits,
                self.min_inclusive,
            ),
        )
        if self._auto_coommit:
            self.db.connection.commit()
