class LinkroleAssertion:
    def __init__(
        self, db=None, linkrole_rowid=None, assertion_id=None, auto_commit=True
    ):
        self.db = db
        self.linkrole_rowid = linkrole_rowid
        self.assertion_id = assertion_id
        self._auto_commit = auto_commit
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select linkrole_rowid, assertion_id from linkrole_assertion "
            "where linkrole_rowid = %s and assertion_id = %s"
        )
        self.cursor.execute(query, (self.linkrole_rowid, self.assertion_id))
        result = self.cursor.fetchone()
        if result is None:
            self._create()

    def _create(self):
        query = self.db.rewrite_query(
            "insert into linkrole_assertion (linkrole_rowid, assertion_id) values (%s, %s)"
        )
        self.cursor.execute(query, (self.linkrole_rowid, self.assertion_id))
        if self._auto_commit:
            self.db.connection.commit()
