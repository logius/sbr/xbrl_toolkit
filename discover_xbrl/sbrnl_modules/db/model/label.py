from discover_xbrl.sbrnl_modules.db.model.assertion_label import AssertionLabel
from discover_xbrl.sbrnl_modules.db.model.concept_relationshipnode_label import (
    ConceptRelationshipNodeLabel,
)
from discover_xbrl.sbrnl_modules.db.model.dts_label import DTSLabel
from discover_xbrl.sbrnl_modules.db.model.concept_label import ConceptLabel
from discover_xbrl.sbrnl_modules.db.model.enumeration_option_label import (
    EnumerationOptionLabel,
)
from discover_xbrl.sbrnl_modules.db.model.linkrole_label import LinkroleLabel
from discover_xbrl.sbrnl_modules.db.model.table_label import TableLabel
from discover_xbrl.sbrnl_modules.db.model.table_rulenode_label import TableRulenodeLabel
from discover_xbrl.sbrnl_modules.db.model.aspect_node_label import AspectNodeLabel
from psycopg2.errors import UniqueViolation


class Label:
    def __init__(
        self,
        db=None,
        id=None,
        link_label=None,
        link_role=None,
        lang=None,
        label_text=None,
        bind_to=None,
        dts_name=None,
        auto_commit=True,
    ):
        self.db = db
        self.id = id
        self.link_label = link_label
        self.link_role = link_role
        self.lang = lang
        self.label_text = label_text
        self._bind_to = bind_to
        self._dts_name = dts_name
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()
        self._auto_commit = auto_commit
        if self.id is None:
            self.id = f"{self.link_label}_{self.lang}"
        if self.link_label is None:
            self.link_label = self.id

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select id, link_label, link_role, lang, label_text from label "
            "where id = %s and link_label = %s and link_role = %s and lang = %s "
        )
        self.cursor.execute(
            query, (self.id, self.link_label, self.link_role, self.lang)
        )
        result = self.cursor.fetchone()
        if result is None:
            self._create()
        else:
            self.id = result[0]
            self.link_label = result[1]
            self.link_role = result[2]
            self.lang = result[3]
            self.label_text = result[4]
        if self._bind_to:
            for other, value in self._bind_to.items():
                if other == "dts":
                    dts_label = DTSLabel(
                        db=self.db,
                        dts_name=value,
                        label_id=self.id,
                        label_link_label=self.link_label,
                        label_lang=self.lang,
                        label_link_role=self.link_role,
                        auto_commit=self._auto_commit,
                    )
                    dts_label.find_or_create()
                elif other == "concept":
                    concept_label = ConceptLabel(
                        db=self.db,
                        concept_id=value,
                        label_id=self.id,
                        label_link_label=self.link_label,
                        label_lang=self.lang,
                        label_link_role=self.link_role,
                        dts_name=self._dts_name,
                        auto_commit=self._auto_commit,
                    )
                    concept_label.find_or_create()
                elif other == "table":
                    table_label = TableLabel(
                        db=self.db,
                        table_id=value,
                        label_id=self.id,
                        label_link_label=self.link_label,
                        label_lang=self.lang,
                        label_link_role=self.link_role,
                        auto_commit=self._auto_commit,
                    )
                    table_label.find_or_create()
                elif other == "table_rulenode":
                    table_rulenode_label = TableRulenodeLabel(
                        db=self.db,
                        table_rulenode_id=value,
                        label_id=self.id,
                        label_link_label=self.link_label,
                        label_lang=self.lang,
                        label_link_role=self.link_role,
                        auto_commit=self._auto_commit,
                    )
                    table_rulenode_label.find_or_create()
                elif other == "assertion":
                    assertion_label = AssertionLabel(
                        db=self.db,
                        assertion_id=value,
                        label_id=self.id,
                        label_link_label=self.link_label,
                        label_link_role=self.link_role,
                        label_lang=self.lang,
                        auto_commit=self._auto_commit,
                    )
                    assertion_label.find_or_create()
                elif other == "enumeration_option":
                    enumeration_option_label = EnumerationOptionLabel(
                        db=self.db,
                        enumeration_option_id=value,
                        label_id=self.id,
                        label_link_label=self.link_label,
                        label_lang=self.lang,
                        label_link_role=self.link_role,
                        auto_commit=self._auto_commit,
                    )
                    enumeration_option_label.find_or_create()
                elif other == "linkrole":
                    linkrole_label = LinkroleLabel(
                        db=self.db,
                        linkrole_rowid=value,
                        label_id=self.id,
                        label_link_label=self.link_label,
                        label_lang=self.lang,
                        label_link_role=self.link_role,
                        auto_commit=self._auto_commit,
                    )
                    linkrole_label.find_or_create()

    def _create(self):
        query = self.db.rewrite_query(
            "insert into label (id, link_label, link_role, lang, label_text) values (%s, %s, %s, %s, %s)"
        )
        if not self.label_text:
            print(f"Tekstloos label: {self.id} {self.link_label} {self.link_role}")
            self.label_text = ""
        try:
            self.cursor.execute(
                query,
                (self.id, self.link_label, self.link_role, self.lang, self.label_text),
            )
        except UniqueViolation as e:
            print(e)
        if self._auto_commit:
            self.db.connection.commit()
