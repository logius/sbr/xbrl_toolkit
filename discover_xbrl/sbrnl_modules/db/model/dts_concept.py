class DTSConcept:
    def __init__(
        self,
        db=None,
        dts_name=None,
        concept_id=None,
        directly_referenced=None,
        d_link=None,
        p_link=None,
        t_link=None,
        auto_commit=True,
    ):
        self.db = db
        self.dts_name = dts_name
        self.concept_id = concept_id
        self.directly_referenced = directly_referenced
        self.d_link = d_link
        self.p_link = p_link
        self.t_link = t_link
        self._auto_commit = auto_commit

        if not self.db or not self.db.connection:
            raise ConnectionError("No sqlite connection provided!")
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select dts_name, concept_id from dts_concept where dts_name = %s and concept_id = %s"
        )
        self.cursor.execute(query, (self.dts_name, self.concept_id))
        result = self.cursor.fetchone()
        if result is None:
            self._create()

    def _create(self):
        query = self.db.rewrite_query(
            "insert into dts_concept (dts_name, concept_id, directly_referenced, "
            "d_link, p_link, t_link) values (%s, %s, %s, %s, %s, %s)"
        )
        self.cursor.execute(
            query,
            (
                self.dts_name,
                self.concept_id,
                self.directly_referenced,
                self.d_link,
                self.p_link,
                self.t_link,
            ),
        )
        if self._auto_commit:
            self.db.connection.commit()

    def update_reference(self, link, value):
        """
        @param:
        """
        query = self.db.rewrite_query(
            f"update dts_concept set {link} = %s where dts_name = %s and concept_id = %s"
        )
        self.cursor.execute(query, (value, self.dts_name, self.concept_id))
