class ConceptReference:
    def __init__(self, db=None, concept_id=None, reference_id=None, auto_commit=True):
        self.db = db
        self.concept_id = concept_id
        self.reference_id = reference_id
        self._auto_commit = auto_commit
        if not self.db or not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select concept_id, reference_id from concept_reference where concept_id = %s and reference_id = %s"
        )
        self.cursor.execute(query, (self.concept_id, self.reference_id))
        result = self.cursor.fetchone()
        if result is None:
            self._create()

    def _create(self):
        query = self.db.rewrite_query(
            "insert into concept_reference (concept_id, reference_id) values (%s, %s)"
        )
        self.cursor.execute(query, (self.concept_id, self.reference_id))
        if self._auto_commit:
            self.db.connection.commit()
