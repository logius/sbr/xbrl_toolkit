class TableBreakdown:
    def __init__(
        self,
        db=None,
        id=None,
        label=None,
        parent_child_order=None,
        axis=None,
        table_id=None,
        order=None,
        auto_commit=True,
        bind_to=None,
    ):
        self.db = db
        self.id = id
        self.label = label
        self.parent_child_order = parent_child_order
        self.axis = axis
        self.table_id = table_id
        self.order = order
        self._bind_to = bind_to
        self._auto_commit = auto_commit
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        #  id's zijn natuurlijk niet uniek over de hele NT :(
        query = self.db.rewrite_query(
            "select id, label, parent_child_order, axis, table_id, _order "
            "from table_breakdown "
            "where id = %s "
            "and table_id = %s "
        )
        self.cursor.execute(query, (self.id, self.table_id))
        result = self.cursor.fetchone()
        if result is None:
            self._create()

    def _create(self):
        query = self.db.rewrite_query(
            "insert into table_breakdown (id, label, parent_child_order, axis, table_id, _order) "
            "values (%s, %s, %s, %s, %s, %s)"
        )
        self.cursor.execute(
            query,
            (
                self.id,
                self.label,
                self.parent_child_order,
                self.axis,
                self.table_id,
                self.order,
            ),
        )
        if self._auto_commit:
            self.db.connection.commit()
