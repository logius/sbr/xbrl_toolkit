class FormulaPeriod:
    def __init__(
        self,
        db=None,
        period_type=None,
        start=None,
        end=None,
        value=None,
        parent_ruleset=None,
        parent_node=None,
        bind_to=None,
        auto_commit=None,
    ):
        self.db = db
        self.rowid = None
        self.period_type = period_type
        self.start = start
        self.end = end
        self.value = value
        self.parent_ruleset = parent_ruleset
        self.parent_node = parent_node

        self._auto_commit = auto_commit
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        if self.parent_ruleset:
            query = self.db.rewrite_query(
                "select rowid, period_type, _start, _end, value, parent_ruleset from formula_period "
                "where period_type= %s and parent_ruleset = %s "
            )
            self.cursor.execute(query, (self.period_type, self.parent_ruleset))
        else:
            query = self.db.rewrite_query(
                "select rowid, period_type, _start, _end, value, parent_ruleset from formula_period "
                "where period_type= %s and parent_node = %s "
            )
            self.cursor.execute(query, (self.period_type, self.parent_node))
        result = self.cursor.fetchone()
        if result is None:
            self._create()
        else:
            self.rowid = result[0]
            self.start = result[2]
            self.end = result[3]
            self.value = result[4]

    def _create(self):
        query = self.db.rewrite_query(
            "insert into formula_period (period_type, _start, _end, value, parent_ruleset, parent_node) "
            "values (%s, %s, %s, %s, %s, %s)"
        )
        if self.db.engine == "postgres":
            query += " returning rowid"
        self.cursor.execute(
            query,
            (
                self.period_type,
                self.start,
                self.end,
                self.value,
                self.parent_ruleset,
                self.parent_node,
            ),
        )
        if self.db.engine == "postgres":
            result = self.cursor.fetchone()
            self.rowid = result[0]
        else:
            self.rowid = self.cursor.lastrowid
        if self._auto_commit:
            self.db.connection.commit()
