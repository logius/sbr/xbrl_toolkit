class PresentationHierarchyNode:
    def __init__(
        self,
        db=None,
        id=None,
        concept_id=None,
        linkrole_role_uri=None,
        order=None,
        preferred_label=None,
        is_root=False,
        parent_rowid=None,
        dts_name=None,
        parent_linkrole_rowid=None,
        directly_referenced=None,
        bind_to=None,
        auto_commit=None,
    ):
        self.db = db
        self.rowid = None
        self.id = id
        self.concept_id = concept_id
        self.linkrole_role_uri = linkrole_role_uri
        self.order = int(float(order)) if order else None
        self.preferred_label = preferred_label
        self.is_root = is_root
        self.parent_rowid = parent_rowid
        self.dts_name = dts_name
        self.parent_linkrole_rowid = parent_linkrole_rowid
        self.directly_referenced = directly_referenced
        self.node_type = "linkrole" if concept_id is None else "concept"

        self._bind_to = bind_to
        self._auto_commit = auto_commit
        if not self.db or not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        if self.parent_rowid:
            query = self.db.rewrite_query(
                "select rowid, id, node_type, concept_id, linkrole_role_uri, is_root, _order, "
                "preferred_label, parent_rowid, dts_name, parent_linkrole_rowid,"
                "directly_referenced "
                "from presentation_hierarchy_node where id = %s and parent_rowid = %s "
            )
            self.cursor.execute(query, (self.id, self.parent_rowid))
        elif self.parent_linkrole_rowid:
            query = self.db.rewrite_query(
                "select rowid, id, node_type, concept_id, linkrole_role_uri, is_root, _order, "
                "preferred_label, parent_rowid, dts_name,parent_linkrole_rowid,"
                "directly_referenced "
                "from presentation_hierarchy_node where id = %s and parent_linkrole_rowid = %s "
            )
            self.cursor.execute(query, (self.id, self.parent_linkrole_rowid))
        else:
            query = self.db.rewrite_query(
                "select rowid, id, node_type, concept_id, linkrole_role_uri, is_root, _order, "
                "preferred_label, parent_rowid, dts_name, parent_linkrole_rowid,"
                "directly_referenced "
                "from presentation_hierarchy_node where id = %s and dts_name = %s "
            )
            self.cursor.execute(query, (self.id, self.dts_name))
        result = self.cursor.fetchone()
        if result is None:
            self._create()
        else:
            self.rowid = result[0]

    def _create(self):
        query = self.db.rewrite_query(
            "insert into presentation_hierarchy_node "
            "(id, node_type, concept_id, linkrole_role_uri, is_root, _order, preferred_label, "
            "parent_rowid, dts_name, parent_linkrole_rowid, directly_referenced) "
            "values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        )
        if self.db.engine == "postgres":
            query += " returning rowid"

        self.cursor.execute(
            query,
            (
                self.id,
                self.node_type,
                self.concept_id,
                self.linkrole_role_uri,
                self.is_root,
                self.order,
                self.preferred_label,
                self.parent_rowid,
                self.dts_name,
                self.parent_linkrole_rowid,
                self.directly_referenced,
            ),
        )
        if self.db.engine == "postgres":
            result = self.cursor.fetchone()
            self.rowid = result[0]
        else:
            self.rowid = self.cursor.lastrowid

        if self._auto_commit:
            self.db.connection.commit()
