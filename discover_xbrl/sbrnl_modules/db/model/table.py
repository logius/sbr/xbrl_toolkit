from discover_xbrl.sbrnl_modules.db.model.linkrole_table import LinkroleTable


class Table:
    def __init__(
        self,
        db=None,
        id=None,
        label=None,
        parent_child_order=None,
        auto_commit=True,
        bind_to=None,
    ):
        self.db = db
        self.id = id
        self.label = label
        self.parent_child_order = parent_child_order
        self._bind_to = bind_to
        self._auto_commit = auto_commit
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select id, label, parent_child_order from xbrl_table where id = %s"
        )
        self.cursor.execute(query, (self.id,))
        result = self.cursor.fetchone()
        if result is None:
            self._create()
        if self._bind_to:
            for other, value in self._bind_to.items():
                if other == "linkrole":
                    linkrole_table = LinkroleTable(
                        db=self.db,
                        linkrole_rowid=value,
                        table_id=self.id,
                        auto_commit=self._auto_commit,
                    )
                    linkrole_table.find_or_create()

    def _create(self):
        query = self.db.rewrite_query(
            "insert into xbrl_table (id, label, parent_child_order) values (%s, %s, %s)"
        )
        self.cursor.execute(query, (self.id, self.label, self.parent_child_order))
        if self._auto_commit:
            self.db.connection.commit()
