class VariableFilter:
    def __init__(self, db=None, variable_id=None, filter_id=None, auto_commit=True):
        self.db = db
        self.variable_id = variable_id
        self.filter_id = filter_id
        self._auto_commit = auto_commit
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select variable_id, filter_id from variable_filter where variable_id = %s and filter_id = %s "
        )
        self.cursor.execute(query, (self.variable_id, self.filter_id))
        result = self.cursor.fetchone()
        if result is None:
            self._create()

    def _create(self):
        if self.variable_id is None:
            print("Hola!")
        query = self.db.rewrite_query(
            "insert into variable_filter (variable_id, filter_id) values (%s, %s)"
        )
        self.cursor.execute(query, (self.variable_id, self.filter_id))
        if self._auto_commit:
            self.db.connection.commit()
