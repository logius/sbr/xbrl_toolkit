from discover_xbrl.sbrnl_modules.db.model.table_parameter import TableParameter


class Parameter:
    def __init__(
        self,
        db=None,
        id=None,
        label=None,
        as_datatype=None,
        select=None,
        name=None,
        required=None,
        bind_to=None,
        auto_commit=True,
    ):
        self.db = db
        self.id = id
        self.label = label
        self.as_datatype = as_datatype
        self.select = select
        self.name = name
        self.required = required
        self._bind_to = bind_to
        self._auto_commit = auto_commit
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select id, label, as_datatype, _select, name, required "
            "from parameter where id = %s "
        )
        self.cursor.execute(query, (self.id,))
        result = self.cursor.fetchone()
        if result is None:
            self._create()
        if self._bind_to:
            for other, value in self._bind_to.items():
                if other == "table":
                    table_parameter = TableParameter(
                        db=self.db,
                        table_id=value,
                        parameter_id=self.id,
                        auto_commit=self._auto_commit,
                    )
                    table_parameter.find_or_create()

    def _create(self):
        query = self.db.rewrite_query(
            "insert into parameter "
            "(id, label, as_datatype, _select, name, required) "
            "values (%s, %s, %s, %s, %s, %s)"
        )
        self.cursor.execute(
            query,
            (
                self.id,
                self.label,
                self.as_datatype,
                self.select,
                self.name,
                self.required,
            ),
        )
        if self._auto_commit:
            self.db.connection.commit()
