class Dimension:
    def __init__(
        self,
        db=None,
        rowid=None,
        concept_id=None,
        hypercube_linkrole_rowid=None,
        target_linkrole=None,
        linkrole_rowid=None,
        bind_to=None,
        auto_commit=True,
    ):
        self.db = db
        self.rowid = rowid
        self.concept_id = concept_id
        self.hypercube_linkrole_rowid = hypercube_linkrole_rowid
        self.target_linkrole = target_linkrole
        self.linkrole_rowid = linkrole_rowid
        self._bind_to = bind_to
        self._auto_commit = auto_commit
        if not self.db or not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select rowid, concept_id, hypercube_linkrole_rowid, target_linkrole, linkrole_rowid from dimension "
            "where concept_id = %s "
            "  and coalesce(hypercube_linkrole_rowid , 0) = coalesce(%s, 0) "
            "  and coalesce(linkrole_rowid, 0) = coalesce(%s, 0)"
        )
        self.cursor.execute(
            query, (self.concept_id, self.hypercube_linkrole_rowid, self.linkrole_rowid)
        )
        result = self.cursor.fetchone()
        if result is None:
            self._create()
        else:
            self.rowid = result[0]
            self.target_linkrole = result[3]

    def _create(self):
        query = self.db.rewrite_query(
            "insert into dimension (concept_id, hypercube_linkrole_rowid, target_linkrole, linkrole_rowid) "
            "values (%s, %s, %s, %s)"
        )
        if self.db.engine == "postgres":
            query += " returning rowid"
        self.cursor.execute(
            query,
            (
                self.concept_id,
                self.hypercube_linkrole_rowid,
                self.target_linkrole,
                self.linkrole_rowid,
            ),
        )
        if self.db.engine == "postgres":
            result = self.cursor.fetchone()
            self.rowid = result[0]
        else:
            self.rowid = self.cursor.lastrowid
        if self._auto_commit:
            self.db.connection.commit()
