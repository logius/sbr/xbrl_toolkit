from discover_xbrl.sbrnl_modules.db.model.assertion_variableset import (
    AssertionVariableset,
)


class VariableSet:
    def __init__(self, db=None, name=None, bind_to=None, auto_commit=True):
        self.name = name
        self.db = db
        self._bind_to = bind_to
        self._auto_commit = auto_commit
        if not self.db or not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query("select name from variableset where name = %s")
        self.cursor.execute(query, (self.name,))
        result = self.cursor.fetchone()
        if result is None:
            self._create()
        if self._bind_to:
            for other, value in self._bind_to.items():
                if other == "assertion":
                    assertion_variableset = AssertionVariableset(
                        db=self.db,
                        assertion_id=value,
                        variableset_name=self.name,
                        auto_commit=self._auto_commit,
                    )
                    assertion_variableset.find_or_create()

    def _create(self):
        query = self.db.rewrite_query("insert into variableset (name) values (%s)")
        self.cursor.execute(query, (self.name,))
        if self._auto_commit:
            self.db.connection.commit()
