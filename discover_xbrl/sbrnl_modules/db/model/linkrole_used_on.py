class LinkroleUsedOn:
    def __init__(self, db=None, linkrole_rowid=None, used_on=None, auto_commit=None):
        self.db = db
        self.linkrole_rowid = linkrole_rowid
        self.used_on = used_on
        self._auto_commit = auto_commit
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select linkrole_rowid, used_on_used_on from linkrole_used_on "
            "where linkrole_rowid = %s and used_on_used_on = %s "
        )
        self.cursor.execute(query, (self.linkrole_rowid, self.used_on))
        result = self.cursor.fetchone()
        if result is None:
            self._create()

    def _create(self):
        query = self.db.rewrite_query(
            "insert into linkrole_used_on (linkrole_rowid, used_on_used_on) values (%s, %s)"
        )
        self.cursor.execute(query, (self.linkrole_rowid, self.used_on))
        if self._auto_commit:
            self.db.connection.commit()
