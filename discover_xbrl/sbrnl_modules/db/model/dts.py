class DTS:
    def __init__(
        self,
        db=None,
        name=None,
        shortname=None,
        nice_name=None,
        description=None,
        auto_commit=True,
    ):
        self.db = db
        self.name = name
        self.shortname = shortname
        self.nice_name = nice_name
        self.description = description
        self._auto_commit = auto_commit
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select name, shortname, nice_name, description from dts where name=%s "
        )
        self.cursor.execute(query, (self.name,))
        result = self.cursor.fetchone()
        if result is None:
            self._create()
        else:
            self.shortname = result[1]

    def _create(self):
        query = self.db.rewrite_query(
            "insert into dts (name, shortname, nice_name, description) values (%s, %s, %s, %s)"
        )
        self.cursor.execute(
            query, (self.name, self.shortname, self.nice_name, self.description)
        )
        if self._auto_commit:
            self.db.connection.commit()

    def _update(self):
        query = self.db.rewrite_query("update dts set shortname = %s where name = %s ")
        self.cursor.execute(query, (self.shortname, self.name))
        if self._auto_commit:
            self.db.connection.commit()
