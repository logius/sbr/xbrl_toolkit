class TableParameter:
    def __init__(self, db=None, table_id=None, parameter_id=None, auto_commit=True):
        self.db = db
        self.table_id = table_id
        self.parameter_id = parameter_id
        self._auto_commit = auto_commit
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select table_id, parameter_id from table_parameter where table_id = %s and parameter_id = %s "
        )
        self.cursor.execute(query, (self.table_id, self.parameter_id))
        result = self.cursor.fetchone()
        if result is None:
            self._create()

    def _create(self):
        query = self.db.rewrite_query(
            "insert into table_parameter (table_id, parameter_id) values (%s, %s)"
        )
        self.cursor.execute(query, (self.table_id, self.parameter_id))
        if self._auto_commit:
            self.db.connection.commit()
