class LinkroleTable:
    def __init__(
        self,
        db=None,
        linkrole_rowid=None,
        table_id=None,
        auto_commit=None,
        bind_to=None,
    ):
        self.db = db
        self.linkrole_rowid = linkrole_rowid
        self.table_id = table_id
        self._auto_commit = auto_commit
        self._bind_to = bind_to
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select linkrole_rowid, table_id from linkrole_table where linkrole_rowid = %s and table_id = %s "
        )
        self.cursor.execute(query, (self.linkrole_rowid, self.table_id))
        result = self.cursor.fetchone()
        if result is None:
            self._create()

    def _create(self):
        query = self.db.rewrite_query(
            "insert into linkrole_table (linkrole_rowid, table_id) values (%s, %s)"
        )
        self.cursor.execute(query, (self.linkrole_rowid, self.table_id))
        if self._auto_commit:
            self.db.connection.commit()
