from discover_xbrl.sbrnl_modules.db.model.table_filter import TableFilter
from discover_xbrl.sbrnl_modules.db.model.variable_filter import VariableFilter


class Filter:
    def __init__(
        self,
        db=None,
        id=None,
        xlink_label=None,
        type=None,
        aspect=None,
        include_qname=None,
        exclude_qname=None,
        qname=None,
        qname_expression=None,
        period_type=None,
        value=None,
        qname_member=None,
        qname_dimension=None,
        linkrole_role_uri=None,
        role=None,
        arcrole=None,
        axis=None,
        balance_type=None,
        strict=None,
        type_qname=None,
        type_qname_expression=None,
        test=None,
        boundary=None,
        date=None,
        time=None,
        parent=None,
        variable=None,
        arc_complement=None,
        arc_cover=None,
        arc_order=None,
        arc_priority=None,
        exclude_qname_obj=None,
        include_qname_obj=None,
        qname_obj=None,
        __qname=None,
        bind_to=None,
        auto_commit=True,
    ):
        self.db = db
        self.id = id if id is not None else xlink_label
        self.label = xlink_label
        self.type = type
        self.aspect = aspect
        self.include_qname = include_qname
        self.exclude_qname = exclude_qname
        self.qname = qname
        self.qname_expression = qname_expression
        self.period_type = period_type
        self.value = value
        self.qname_member = qname_member
        self.qname_dimension = qname_dimension
        self.linkrole_role_uri = linkrole_role_uri
        self.arcrole = arcrole
        self.axis = axis
        self.balance_type = balance_type
        self.strict = strict
        self.type_qname = type_qname
        self.type_qname_expression = type_qname_expression
        self.test = test
        self.boundary = boundary
        self.date = date
        self.time = time
        self.parent = parent
        self.variable = variable
        self.arc_complement = arc_complement
        self.arc_priority = arc_priority
        self.arc_cover = arc_cover
        self.arc_order = arc_order

        self._bind_to = bind_to
        self._auto_commit = auto_commit
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select id, label, type, aspect, include_qname, exclude_qname, qname, qname_expression, period_type, "
            "value, qname_member, qname_dimension, linkrole_role_uri, arcrole, axis, balance_type, strict, "
            "type_qname, type_qname_expression, test, boundary, date, time, parent, "
            "arc_complement, arc_priority, arc_cover, arc_order "
            "from filter where id = %s"
        )
        self.cursor.execute(query, (self.id,))
        result = self.cursor.fetchone()
        if result is None:
            self._create()
        if self._bind_to:
            for other, value in self._bind_to.items():
                if other == "variable":
                    variable_filter = VariableFilter(
                        db=self.db,
                        variable_id=value,
                        filter_id=self.id,
                        auto_commit=self._auto_commit,
                    )
                    variable_filter.find_or_create()
                elif other == "table":
                    table_filter = TableFilter(
                        db=self.db,
                        table_id=value,
                        filter_id=self.id,
                        auto_commit=self._auto_commit,
                    )
                    table_filter.find_or_create()

    def _create(self):
        query = self.db.rewrite_query(
            "insert into filter (id, label, type, aspect, include_qname, exclude_qname, qname, qname_expression, "
            "period_type, value, qname_member, qname_dimension, linkrole_role_uri, arcrole, axis, balance_type, "
            "strict, type_qname, type_qname_expression, test, boundary, date, time, parent, "
            "arc_complement, arc_priority, arc_cover, arc_order, variable) "
            "values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
            "        %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,"
            "        %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        )
        self.cursor.execute(
            query,
            (
                self.id,
                self.label,
                self.type,
                self.aspect,
                self.include_qname,
                self.exclude_qname,
                self.qname,
                self.qname_expression,
                self.period_type,
                self.value,
                self.qname_member,
                self.qname_dimension,
                self.linkrole_role_uri,
                self.arcrole,
                self.axis,
                self.balance_type,
                self.strict,
                self.type_qname,
                self.type_qname_expression,
                self.test,
                self.boundary,
                self.date,
                self.time,
                self.parent,
                self.arc_complement,
                self.arc_priority,
                self.arc_cover,
                self.arc_order,
                self.variable.name if self.variable is not None else None,
            ),
        )
        if self._auto_commit:
            self.db.connection.commit()
