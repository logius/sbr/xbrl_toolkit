class Hypercube:
    def __init__(
        self,
        db=None,
        hc_role_uri=None,
        linkrole_rowid=None,
        hc_type=None,
        context_element=None,
        closed=None,
        bind_to=None,
        auto_commit=True,
    ):
        self.db = db
        self.hc_role_uri = hc_role_uri
        self.linkrole_rowid = linkrole_rowid
        self.hc_type = hc_type
        self.context_element = context_element
        self.closed = closed
        self._bind_to = bind_to
        self._auto_commit = auto_commit
        if not self.db or not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select hc_role_uri, linkrole_rowid, hc_type, context_element, closed "
            "from hypercube where linkrole_rowid = %s and hc_role_uri = %s"
        )
        self.cursor.execute(query, (self.linkrole_rowid, self.hc_role_uri))
        result = self.cursor.fetchone()
        if result is None:
            self._create()

    def _create(self):
        query = self.db.rewrite_query(
            "insert into hypercube (hc_role_uri, linkrole_rowid, hc_type, context_element, closed) "
            "values (%s, %s, %s, %s, %s)"
        )
        self.cursor.execute(
            query,
            (
                self.hc_role_uri,
                self.linkrole_rowid,
                self.hc_type,
                self.context_element,
                self.closed,
            ),
        )
        if self._auto_commit:
            self.db.connection.commit()
