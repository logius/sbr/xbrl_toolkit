class FormulaExplicitDimension:
    def __init__(
        self,
        db=None,
        qname_dimension=None,
        qname_member=None,
        qname_member_expression=None,
        type_qname=None,
        omit=None,
        parent_ruleset=None,
        parent_node=None,
        bind_to=None,
        auto_commit=None,
    ):
        self.db = db
        self.rowid = None
        self.qname_dimension = qname_dimension
        self.qname_member = qname_member
        self.qname_member_expression = qname_member_expression
        self.type_qname = type_qname
        self.omit = omit
        self.parent_ruleset = parent_ruleset
        self.parent_node = parent_node

        self._auto_commit = auto_commit
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        if self.parent_ruleset:
            query = self.db.rewrite_query(
                "select rowid, qname_dimension, qname_member, type_qname, omit, parent_ruleset, "
                "qname_member_expression "
                "from formula_explicit_dimension "
                "where qname_dimension= %s and parent_ruleset = %s "
            )
            self.cursor.execute(query, (self.qname_dimension, self.parent_ruleset))
        else:
            query = self.db.rewrite_query(
                "select rowid, qname_dimension, qname_member, type_qname, omit, parent_node, "
                "qname_member_expression "
                "from formula_explicit_dimension "
                "where qname_dimension= %s and parent_node = %s "
            )
            self.cursor.execute(query, (self.qname_dimension, self.parent_node))

        result = self.cursor.fetchone()
        if result is None:
            self._create()
        else:
            self.rowid = result[0]
            self.qname_member = result[2]
            self.qname_member_expression = result[6]
            self.type_qname = result[3]
            self.omit = result[4]

    def _create(self):
        query = self.db.rewrite_query(
            "insert into formula_explicit_dimension (qname_dimension, qname_member, type_qname, omit,"
            " parent_ruleset, parent_node, qname_member_expression) "
            "values (%s, %s, %s, %s, %s, %s, %s)"
        )
        if self.db.engine == "postgres":
            query += " returning rowid"

        self.cursor.execute(
            query,
            (
                self.qname_dimension,
                self.qname_member,
                self.type_qname,
                self.omit,
                self.parent_ruleset,
                self.parent_node,
                self.qname_member_expression,
            ),
        )
        if self.db.engine == "postgres":
            result = self.cursor.fetchone()
            self.rowid = result[0]
        else:
            self.rowid = self.cursor.lastrowid
        if self._auto_commit:
            self.db.connection.commit()
