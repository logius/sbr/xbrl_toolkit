from psycopg2.errors import ForeignKeyViolation
from discover_xbrl.sbrnl_modules.db.collections.variables import Variables


class VariablesetVariable:
    def __init__(
        self,
        db=None,
        variableset_name=None,
        variable_id=None,
        order=None,
        priority=None,
        auto_commit=True,
        bind_to=None,
    ):
        self.db = db
        self.variableset_name = variableset_name
        self.variable_id = variable_id
        self.order = int(float(order)) if order else None
        self.priority = priority
        self._auto_commit = auto_commit
        if not self.db or not self.db.connection:
            raise ConnectionError
        self._bind_to = bind_to
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select variableset_name, variable_id from variableset_variable "
            "where variableset_name = %s and variable_id = %s"
        )
        self.cursor.execute(query, (self.variableset_name, self.variable_id))
        result = self.cursor.fetchone()
        if result is None:
            var = Variables(db=self.db, full=False)
            var.variables_by_id(id=self.variable_id)
            if len(var.variables):
                self._create()
            else:
                print(f"Variable noton file; probably a Parameter: {self.variable_id}")

    def _create(self):
        query = self.db.rewrite_query(
            "insert into variableset_variable (variableset_name, variable_id, _order, priority) "
            "values (%s, %s, %s, %s)"
        )
        try:
            self.cursor.execute(
                query,
                (self.variableset_name, self.variable_id, self.order, self.priority),
            )
            if self._auto_commit:
                self.db.connection.commit()
        except ForeignKeyViolation as fk:
            print(fk)
            pass
