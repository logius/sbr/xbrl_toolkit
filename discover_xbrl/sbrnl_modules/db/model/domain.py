class Domain:
    def __init__(
        self,
        db=None,
        concept_id=None,
        usable=None,
        dimension_rowid=None,
        bind_to=None,
        auto_commit=True,
    ):
        self.db = db
        self.concept_id = concept_id
        self.usable = usable
        self.dimension_rowid = dimension_rowid
        self._bind_to = bind_to
        self._auto_commit = auto_commit
        self.rowid = None

        if not self.db or not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select rowid, concept_id, usable, dimension_rowid "
            "from domain where concept_id = %s and dimension_rowid = %s"
        )
        self.cursor.execute(query, (self.concept_id, self.dimension_rowid))
        result = self.cursor.fetchone()
        if result is None:
            self._create()
        else:
            self.rowid = result[0]

    def _create(self):
        query = self.db.rewrite_query(
            "insert into domain (concept_id, usable, dimension_rowid) values (%s, %s, %s)"
        )
        if self.db.engine == "postgres":
            query += " returning rowid"
        self.cursor.execute(query, (self.concept_id, self.usable, self.dimension_rowid))
        if self.db.engine == "postgres":
            result = self.cursor.fetchone()
            self.rowid = result[0]
        else:
            self.rowid = self.cursor.lastrowid

        if self._auto_commit:
            self.db.connection.commit()
