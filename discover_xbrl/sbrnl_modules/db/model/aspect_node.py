class AspectNode:
    def __init__(
        self,
        db=None,
        id=None,
        label=None,
        parent_child_order=None,
        parent=None,
        tagselector=None,
        order=None,
        merge=None,
        is_abstract=None,
        aspect_type=None,
        include_unreprted_value=None,
        value=None,
        auto_commit=True,
        bind_to=None,
    ):
        self.db = db
        self.id = id
        self.label = label
        self.parent_child_order = parent_child_order
        self.parent = parent
        self.tagselector = tagselector
        self.order = order
        self.merge = merge
        self.is_abstract = is_abstract
        self.aspect_type = aspect_type
        self.include_unreported_value = include_unreprted_value
        self.value = value
        self._bind_to = bind_to
        self._auto_commit = auto_commit
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select id, label, parent_child_order, parent, tagselector, merge, _order, is_abstract, "
            "aspect_type, include_unreported_value, value "
            "from aspect_node where id = %s"
        )
        self.cursor.execute(query, (self.id,))
        result = self.cursor.fetchone()
        if result is None:
            self._create()

    def _create(self):
        query = self.db.rewrite_query(
            "insert into aspect_node (id, label, parent_child_order, parent, tagselector, merge, "
            "_order, is_abstract, aspect_type, include_unreported_value, value) "
            " values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        )
        self.cursor.execute(
            query,
            (
                self.id,
                self.label,
                self.parent_child_order,
                self.parent,
                self.tagselector,
                self.merge,
                self.order,
                self.is_abstract,
                self.aspect_type,
                self.include_unreported_value,
                self.value,
            ),
        )
        if self._auto_commit:
            self.db.connection.commit()
