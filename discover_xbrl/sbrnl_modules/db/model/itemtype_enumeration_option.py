class ItemtypeEnumerationOption:
    def __init__(
        self,
        db=None,
        itemtype_name=None,
        enumeration_option_id=None,
        bind_to=None,
        auto_commit=True,
    ):
        self.db = db
        self.itemtype_name = itemtype_name
        self.enumeration_option_id = enumeration_option_id
        self._auto_commit = auto_commit

        if not self.db or not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select itemtype_name, enumeration_option_id "
            "from itemtype_enumeration_option "
            "where itemtype_name = %s and enumeration_option_id = %s"
        )
        self.cursor.execute(query, (self.itemtype_name, self.enumeration_option_id))
        result = self.cursor.fetchone()
        if result is None:
            self._create()

    def _create(self):
        query = self.db.rewrite_query(
            "insert into itemtype_enumeration_option "
            "(itemtype_name, enumeration_option_id) values (%s, %s)"
        )
        self.cursor.execute(query, (self.itemtype_name, self.enumeration_option_id))
        if self._auto_commit:
            self.db.connection.commit()
