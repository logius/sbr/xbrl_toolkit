from discover_xbrl.sbrnl_modules.db.model.itemtype_enumeration_option import (
    ItemtypeEnumerationOption,
)


class EnumerationOption:
    def __init__(self, db=None, id=None, value=None, bind_to=None, auto_commit=True):
        self.db = db
        self.id = id
        self.value = value
        self._bind_to = bind_to
        self._auto_commit = auto_commit

        if not self.db or not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select id, value from enumeration_option where id = %s"
        )
        self.cursor.execute(query, (self.id,))
        result = self.cursor.fetchone()
        if result is None:
            self._create()
        if self._bind_to:
            for other, value in self._bind_to.items():
                if other == "itemtype":
                    itemtype_enumeration_option = ItemtypeEnumerationOption(
                        db=self.db,
                        itemtype_name=value,
                        enumeration_option_id=self.id,
                        auto_commit=self._auto_commit,
                    )
                    itemtype_enumeration_option.find_or_create()

    def _create(self):
        query = self.db.rewrite_query(
            "insert into enumeration_option (id, value) values (%s, %s)"
        )
        self.cursor.execute(query, (self.id, self.value))
        if self._auto_commit:
            self.db.connection.commit()
