class TableLabel:
    def __init__(
        self,
        db=None,
        table_id=None,
        label_id=None,
        label_link_label=None,
        label_lang=None,
        label_link_role=None,
        auto_commit=True,
    ):
        self.db = db
        self.table_id = table_id
        self.label_id = label_id
        self.label_link_label = label_link_label
        self.label_lang = label_lang
        self.label_link_role = label_link_role
        self._auto_commit = auto_commit
        if not self.db or not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select xbrl_table_id, label_link_label from xbrl_table_label "
            "where xbrl_table_id = %s and label_id = %s and label_link_label = %s "
            "and label_lang = %s and label_link_role = %s "
        )
        self.cursor.execute(
            query,
            (
                self.table_id,
                self.label_id,
                self.label_link_label,
                self.label_lang,
                self.label_link_role,
            ),
        )
        result = self.cursor.fetchone()
        if result is None:
            self._create()

    def _create(self):
        query = self.db.rewrite_query(
            "insert into xbrl_table_label (xbrl_table_id, label_id, label_link_label, label_lang, label_link_role) "
            "values (%s, %s, %s, %s, %s)"
        )
        self.cursor.execute(
            query,
            (
                self.table_id,
                self.label_id,
                self.label_link_label,
                self.label_lang,
                self.label_link_role,
            ),
        )
        if self._auto_commit:
            self.db.connection.commit()
