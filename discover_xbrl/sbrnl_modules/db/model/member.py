class Member:
    def __init__(
        self,
        db=None,
        concept_id=None,
        order=None,
        usable=None,
        target_linkrole=None,
        preferred_label=None,
        parent_rowid=None,
        parent_type=None,
        bind_to=None,
        auto_commit=True,
    ):
        self.db = db
        self.concept_id = concept_id
        self.order = order
        self.usable = usable
        self.target_linkrole = target_linkrole
        self.preferred_label = preferred_label
        self.parent_rowid = parent_rowid
        self.parent_type = parent_type
        self.rowid = None
        self._bind_to = bind_to
        self._auto_commit = auto_commit

        if not self.db or not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select rowid, concept_id, _order, usable, parent_rowid, parent_type, target_linkrole, preferred_label "
            "from member where parent_rowid = %s and parent_type = %s and concept_id = %s"
        )
        self.cursor.execute(
            query, (self.parent_rowid, self.parent_type, self.concept_id)
        )
        result = self.cursor.fetchone()
        if result is None:
            self._create()
        else:
            self.rowid = result[0]

    def _create(self):
        query = self.db.rewrite_query(
            "insert into member (concept_id, _order, usable, target_linkrole, preferred_label, parent_rowid, parent_type) "
            "values (%s, %s, %s, %s, %s, %s, %s)"
        )
        if self.db.engine == "postgres":
            query += " returning rowid"
        self.cursor.execute(
            query,
            (
                self.concept_id,
                self.order,
                self.usable,
                self.target_linkrole,
                self.preferred_label,
                self.parent_rowid,
                self.parent_type,
            ),
        )
        if self.db.engine == "postgres":
            result = self.cursor.fetchone()
            self.rowid = result[0]
        else:
            self.rowid = self.cursor.lastrowid

        if self._auto_commit:
            self.db.connection.commit()
