## deprecated
class AspectNodeLabel:
    def __init__(
        self, db=None, aspect_node_id=None, label_link_label=None, auto_commit=True
    ):
        self.db = db
        self.aspect_node_id = aspect_node_id
        self.label_link_label = label_link_label
        self._auto_commit = auto_commit
        if not self.db or not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select aspect_node_id, label_link_label from aspect_node_label "
            "where aspect_node_id = %s and label_link_label = %s"
        )
        self.cursor.execute(query, (self.aspect_node_id, self.label_link_label))
        result = self.cursor.fetchone()
        if result is None:
            self._create()

    def _create(self):
        query = self.db.rewrite_query(
            "insert into aspect_node_label (aspect_node_id, label_link_label) values (%s, %s)"
        )
        self.cursor.execute(query, (self.aspect_node_id, self.label_link_label))
        if self._auto_commit:
            self.db.connection.commit()
