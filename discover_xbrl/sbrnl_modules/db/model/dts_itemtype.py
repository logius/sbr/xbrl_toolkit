class DTSItemType:
    def __init__(self, db=None, dts_name=None, itemtype_name=None, auto_commit=True):
        self.db = db
        self.dts_name = dts_name
        self.itemtype_name = itemtype_name
        self._auto_commit = auto_commit
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select dts_name, itemtype_name from dts_itemtype where dts_name = %s and itemtype_name = %s"
        )
        self.cursor.execute(query, (self.dts_name, self.itemtype_name))
        result = self.cursor.fetchone()
        if result is None:
            self._create()

    def _create(self):
        query = self.db.rewrite_query(
            "insert into dts_itemtype (dts_name, itemtype_name) values (%s, %s)"
        )
        self.cursor.execute(query, (self.dts_name, self.itemtype_name))
        if self._auto_commit:
            self.db.connection.commit()
