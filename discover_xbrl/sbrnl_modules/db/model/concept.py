from discover_xbrl.sbrnl_modules.db.model.dts_concept import DTSConcept


class Concept:
    def __init__(
        self,
        db=None,
        id=None,
        name=None,
        balance=None,
        nillable=None,
        abstract=None,
        periodtype=None,
        ns_prefix=None,
        namespace=None,
        substitution_group=None,
        typed_domainref=None,
        itemtype_name=None,
        auto_commit=True,
        bind_to=None,
    ):
        self.db = db
        self.id = id
        self.name = name
        self.abstract = abstract
        self.balance = balance
        self.nillable = nillable
        self.periodtype = periodtype
        self.ns_prefix = ns_prefix
        self.namespace = namespace
        self.substitution_group = substitution_group
        self.typed_domainref = typed_domainref
        self.itemtype_name = itemtype_name

        self._auto_commit = auto_commit
        self._bind_to = bind_to

        if not self.db or not self.db.connection:
            raise ConnectionError("No database connection provided!")
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select id, name, balance, nillable, abstract, periodtype, ns_prefix, namespace, "
            "substitution_group, typed_domainref, itemtype_name "
            "from concept where id = %s"
        )
        self.cursor.execute(query, (self.id,))
        result = self.cursor.fetchone()
        if result is None:
            self._create()
        if self._bind_to:
            for other, value in self._bind_to.items():
                if other == "dts":
                    dts_concept = DTSConcept(
                        db=self.db,
                        dts_name=value["dts_name"],
                        concept_id=self.id,
                        directly_referenced=value["directly_referenced"],
                        d_link=value["d_link"],
                        p_link=value["p_link"],
                        t_link=value["t_link"],
                        auto_commit=self._auto_commit,
                    )
                    dts_concept.find_or_create()

    def _create(self):
        query = self.db.rewrite_query(
            "insert into concept (id, name, balance, nillable, abstract, "
            "periodtype, ns_prefix, namespace, substitution_group, typed_domainref, itemtype_name) "
            " values(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        )
        self.cursor.execute(
            query,
            (
                self.id,
                self.name,
                self.balance,
                self.nillable,
                self.abstract,
                self.periodtype,
                self.ns_prefix,
                self.namespace,
                self.substitution_group,
                self.typed_domainref,
                self.itemtype_name,
            ),
        )
        if self._auto_commit:
            self.db.connection.commit()
