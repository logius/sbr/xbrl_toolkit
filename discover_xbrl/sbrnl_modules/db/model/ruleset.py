class RuleSet:
    def __init__(
        self, db=None, rulenode_id=None, tag=None, bind_to=None, auto_commit=True
    ):
        self.db = db
        self.rowid = None
        self.rulenode_id = rulenode_id
        self.tag = tag
        self._auto_commit = auto_commit
        self.bind_to = bind_to
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select rowid from ruleset where rulenode_id = %s and tag = %s"
        )
        self.cursor.execute(query, (self.rulenode_id, self.tag))
        result = self.cursor.fetchone()
        if result is None:
            self._create()
        else:
            self.rowid = result[0]

    def _create(self):
        query = self.db.rewrite_query(
            "insert into ruleset (rulenode_id, tag) values (%s, %s)"
        )
        self.cursor.execute(query, (self.rulenode_id, self.tag))
        self.rowid = self.cursor.lastrowid
        if self._auto_commit:
            self.db.connection.commit()
