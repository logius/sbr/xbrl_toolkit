class LinkroleReference:
    def __init__(
        self, db=None, linkrole_rowid=None, reference_id=None, auto_commit=True
    ):
        self.db = db
        self.linkrole_rowid = linkrole_rowid
        self.reference_id = reference_id
        self._auto_commit = auto_commit
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select linkrole_rowid, reference_id from linkrole_reference "
            "where linkrole_rowid = %s and reference_id = %s"
        )
        self.cursor.execute(query, (self.linkrole_rowid, self.reference_id))
        result = self.cursor.fetchone()
        if result is None:
            self._create()

    def _create(self):
        query = self.db.rewrite_query(
            "insert into linkrole_reference (linkrole_rowid, reference_id) values (%s, %s)"
        )
        self.cursor.execute(query, (self.linkrole_rowid, self.reference_id))
        if self._auto_commit:
            self.db.connection.commit()
