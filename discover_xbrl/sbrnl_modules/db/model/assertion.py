from discover_xbrl.sbrnl_modules.db.model.linkrole_assertion import LinkroleAssertion


class Assertion:
    def __init__(
        self,
        db=None,
        id=None,
        aspect_model=None,
        implicit_filtering=None,
        xlink_label=None,
        test=None,
        assertion_type=None,
        severity=None,
        bind_to=None,
        auto_commit=None,
    ):
        self.db = db
        self.id = id
        self.aspect_model = aspect_model
        self.implicit_filtering = implicit_filtering
        self.xlink_label = xlink_label
        self.assertion_type = assertion_type
        self.test = test
        self.severity = severity

        self._bind_to = bind_to
        self._auto_commit = auto_commit
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        query = self.db.rewrite_query(
            "select id, aspect_model, implicit_filtering, xlink_label, test, assertion_type, severity "
            "from assertion where id = %s"
        )
        self.cursor.execute(query, (self.id,))
        result = self.cursor.fetchone()
        if result is None:
            self._create()
        if self._bind_to:
            for other, value in self._bind_to.items():
                if other == "linkrole":
                    linkrole_assertion = LinkroleAssertion(
                        db=self.db,
                        assertion_id=self.id,
                        linkrole_rowid=value,
                        auto_commit=self._auto_commit,
                    )
                    linkrole_assertion.find_or_create()

    def _create(self):
        query = self.db.rewrite_query(
            "insert into assertion (id, aspect_model, implicit_filtering, xlink_label, test, assertion_type, severity) "
            "values (%s, %s, %s, %s, %s, %s, %s)"
        )
        self.cursor.execute(
            query,
            (
                self.id,
                self.aspect_model,
                self.implicit_filtering,
                self.xlink_label,
                self.test,
                self.assertion_type,
                self.severity,
            ),
        )
        if self._auto_commit:
            self.db.connection.commit()
