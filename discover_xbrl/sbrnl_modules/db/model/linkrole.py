class Linkrole:
    def __init__(
        self,
        db=None,
        id=None,
        role_uri=None,
        definition=None,
        dts_name=None,
        directly_referenced=None,
        order=None,
        bind_to=None,
        auto_commit=True,
    ):
        self.db = db
        self.rowid = None
        self.id = id
        self.role_uri = role_uri
        self.definition = definition
        self.dts_name = dts_name
        self.directly_referenced = directly_referenced
        self.order = order
        self._bind_to = bind_to
        self._auto_commit = auto_commit
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def find_or_create(self):
        if self.id is not None:
            query = self.db.rewrite_query(
                "select rowid, id, role_uri, definition, dts_name, directly_referenced, _order "
                "from linkrole where id = %s and role_uri = %s and dts_name = %s"
            )
            self.cursor.execute(query, (self.id, self.role_uri, self.dts_name))
        else:
            query = self.db.rewrite_query(
                "select rowid, id, role_uri, definition, dts_name, directly_referenced, _order "
                "from linkrole where role_uri = %s and dts_name = %s"
            )
            self.cursor.execute(query, (self.role_uri, self.dts_name))

        result = self.cursor.fetchone()
        if result is None:
            self._create()
        else:
            self.rowid = result[0]

    def _create(self):
        query = self.db.rewrite_query(
            "insert into linkrole (id, role_uri, definition, dts_name, directly_referenced, _order) "
            "values (%s, %s, %s, %s, %s, %s)"
        )
        if self.db.engine == "postgres":
            query += " returning rowid"
        self.cursor.execute(
            query,
            (
                self.id,
                self.role_uri,
                self.definition,
                self.dts_name,
                self.directly_referenced,
                self.order,
            ),
        )
        if self.db.engine == "postgres":
            result = self.cursor.fetchone()
            self.rowid = result[0]
        else:
            self.rowid = self.cursor.lastrowid
        if self._auto_commit:
            self.db.connection.commit()
