from os.path import join, dirname

from discover_xbrl.conf.conf import Config

conf = Config()
engine = "sqlite"

# We prefer Postgres, but we default to sqlite
if conf.db.get("type"):
    if conf.db["type"] == "postgres":
        from discover_xbrl.sbrnl_modules.db.postgres.db_connection import DBConnection

        engine = "postgres"
    elif conf.db["type"] == "sqlite":
        from discover_xbrl.sbrnl_modules.db.sqlite.db_connection import DBConnection
else:
    from discover_xbrl.sbrnl_modules.db.sqlite.db_connection import DBConnection


def main():
    db = DBConnection()
    with open(join(dirname(__file__), engine, "create_db.sql")) as ddl_file:
        ddl = ddl_file.read()
    if ddl:
        cursor = db.connection.cursor()
        if engine == "postgres":
            cursor.execute(ddl)
        else:
            cursor.executescript(ddl)


if __name__ == "__main__":
    main()
