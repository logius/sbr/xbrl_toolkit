import logging
import pickle
from zipfile import ZipFile
from pathlib import Path
from PySide2.QtCore import QRunnable

from discover_xbrl.conf.conf import Config

from discover_xbrl.sbrnl_modules.db.connection import get_connection
from discover_xbrl.sbrnl_modules.db.collections.concepts import Concepts
from discover_xbrl.sbrnl_modules.db.model.arc import Arc

from discover_xbrl.sbrnl_modules.db.model.aspect_node import AspectNode
from discover_xbrl.sbrnl_modules.db.model.assertion import Assertion
from discover_xbrl.sbrnl_modules.db.model.variableset_variable import (
    VariablesetVariable,
)
from discover_xbrl.sbrnl_modules.db.model.concept import Concept
from discover_xbrl.sbrnl_modules.db.model.concept_reference import ConceptReference
from discover_xbrl.sbrnl_modules.db.model.concept_relationshipnode import (
    ConceptRelationshipNode,
)
from discover_xbrl.sbrnl_modules.db.model.dimension import Dimension
from discover_xbrl.sbrnl_modules.db.model.dimension_relationshipnode import (
    DimensionRelationshipNode,
)
from discover_xbrl.sbrnl_modules.db.model.domain import Domain
from discover_xbrl.sbrnl_modules.db.model.member import Member
from discover_xbrl.sbrnl_modules.db.model.dts import DTS
from discover_xbrl.sbrnl_modules.db.model.dts_concept import DTSConcept
from discover_xbrl.sbrnl_modules.db.model.enumeration_option import EnumerationOption
from discover_xbrl.sbrnl_modules.db.model.filter import Filter
from discover_xbrl.sbrnl_modules.db.model.formula_explicit_dimension import (
    FormulaExplicitDimension,
)
from discover_xbrl.sbrnl_modules.db.model.formula_period import FormulaPeriod
from discover_xbrl.sbrnl_modules.db.model.hypercube import Hypercube
from discover_xbrl.sbrnl_modules.db.model.itemtype import ItemType
from discover_xbrl.sbrnl_modules.db.model.label import Label
from discover_xbrl.sbrnl_modules.db.model.linkrole import Linkrole
from discover_xbrl.sbrnl_modules.db.model.linkrole_reference import LinkroleReference
from discover_xbrl.sbrnl_modules.db.model.line_item import LineItem
from discover_xbrl.sbrnl_modules.db.model.namespace import Namespace
from discover_xbrl.sbrnl_modules.db.model.parameter import Parameter
from discover_xbrl.sbrnl_modules.db.model.presentation_hierarchy_node import (
    PresentationHierarchyNode,
)
from discover_xbrl.sbrnl_modules.db.model.reference import Reference
from discover_xbrl.sbrnl_modules.db.model.ruleset import RuleSet
from discover_xbrl.sbrnl_modules.db.model.table import Table
from discover_xbrl.sbrnl_modules.db.model.table_breakdown import TableBreakdown
from discover_xbrl.sbrnl_modules.db.model.table_rulenode import TableRuleNode
from discover_xbrl.sbrnl_modules.db.model.used_on import UsedOn
from discover_xbrl.sbrnl_modules.db.model.variable import Variable
from discover_xbrl.sbrnl_modules.db.model.variableset import VariableSet
from discover_xbrl.sbrnl_xbrl_parser.linkbase.classes import Resource

from discover_xbrl.sbrnl_xbrl_parser.schema.classes import Concept as DtsConcept


class DtsSQLWorker(QRunnable):
    def __init__(self, dts_list=None, info_widget=None):
        super().__init__()
        self.dts_list = dts_list
        self.info_widget = info_widget

    def run(self):
        worker = DtsToSql()
        for dts in self.dts_list:
            print(f"Exporting: {dts.entrypoint_name}")
            worker.set_dts(dts)
        print("All done")


class DtsToSql:
    def __init__(self, dts=None, auto_commit=False):
        self.dts = dts
        self.db = get_connection()
        self.concepts_directly_referenced = []
        self.concepts_definition_link = []
        self.concepts_table_link = []
        self.concepts_presentation_link = []
        self.itemtypes = []
        self.dot = Dot()

        self.auto_commit = auto_commit

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.db.connection:
            self.db.connection.close()

    def set_dts(self, dts=None, auto_run=True):
        if dts:
            self.dts = dts
            self.concepts_directly_referenced = []
            self.concepts_definition_link = []
            self.concepts_presentation_link = []
            self.concepts_table_link = []
            self.itemtypes = []
            if auto_run:
                self.store_dts()

    def store_dts(self):
        """
        Main method,
        Takes the complete DTS and stores alle the bits and pieces to
        a sqlite database
        """
        #  self.dts.referenced_concepts = self.find_referenced_concepts()
        #  doen we tijdens het opslaan van de linkroles
        db_dts = DTS(
            db=self.db,
            name=self.dts.entrypoint_name,
            shortname=self.dts.shortname,
            nice_name=self.dts.generic_info.get("name")
            if self.dts.generic_info
            else None,
            description=self.dts.generic_info.get("description")
            if self.dts.generic_info
            else None,
            auto_commit=self.auto_commit,
        )
        db_dts.find_or_create()
        self.store_namespaces()
        self.store_types()
        self.store_references()
        self.store_concepts()
        self.store_linkroles()
        self.store_presentation(
            presentation_node=self.dts.table_of_contents,
            dts_name=self.dts.entrypoint_name,
            parent_linkrole_rowid=None,
            parent_rowid=None,
        )
        # no matter what, at the end we will commit
        self.db.connection.commit()
        self.dot.dot(newline=True)

    def store_namespaces(self):
        for prefix, uri in self.dts.namespaces.items():
            ns = Namespace(
                db=self.db,
                prefix=prefix,
                uri=uri,
                bind_to={"dts": self.dts.entrypoint_name},
                auto_commit=self.auto_commit,
            )
            ns.find_or_create()

    def store_labels(self):
        for key, label in self.dts.labels.items():
            db_label = Label(
                db=self.db,
                id=label.id,
                link_label=label.xlink_label,
                link_role=label.linkRole,
                lang=label.language,
                label_text=label.text,
                auto_commit=self.auto_commit,
                bind_to={"dts": self.dts.entrypoint_name},
            )
            db_label.find_or_create()

    def store_types(self):
        for itemtype in self.dts.datatypes:
            self.itemtypes.append(itemtype.name)
            db_itemtype = ItemType(
                db=self.db,
                id=itemtype.id,
                name=itemtype.name,
                domain=itemtype.domain,
                base=itemtype.base,
                inheritance_type=itemtype.inheritance_type,
                restriction_type=itemtype.restriction_type,
                restriction_length=itemtype.restriction_length,
                restriction_pattern=itemtype.restriction_pattern,
                restriction_max_length=itemtype.restriction_max_length,
                restriction_min_length=itemtype.restriction_min_length,
                total_digits=itemtype.totalDigits,
                fraction_digits=itemtype.fraction_digits,
                min_inclusive=itemtype.min_inclusive,
                auto_commit=self.auto_commit,
                bind_to={"dts": self.dts.entrypoint_name},
            )
            db_itemtype.find_or_create()
            if itemtype.enumerations:
                for option in itemtype.enumerations:
                    enumeration_option = EnumerationOption(
                        db=self.db,
                        id=option.id,
                        value=option.value,
                        bind_to={"itemtype": itemtype.name},
                        auto_commit=self.auto_commit,
                    )
                    enumeration_option.find_or_create()
                    for label in option.labels:
                        db_label = Label(
                            db=self.db,
                            id=label.id,
                            link_label=label.xlink_label,
                            link_role=label.linkRole,
                            lang=label.language,
                            label_text=label.text,
                            bind_to={"enumeration_option": option.id},
                            auto_commit=self.auto_commit,
                        )
                        db_label.find_or_create()

    def store_concepts(self):
        for concept in self.dts.concepts.values():
            # if concept.id == "xml-abstract-aspect-spec":
            #    print(f"Skipping this MF: {concept.id} {concept.element_type}")
            #    continue

            if isinstance(concept.element_type, str):
                itemtype_name = concept.element_type
            else:
                itemtype_name = (
                    concept.element_type.name if concept.element_type else None
                )
            if itemtype_name and itemtype_name not in self.itemtypes:
                db_itemtype = ItemType(db=self.db, id=itemtype_name, name=itemtype_name)
                db_itemtype.find_or_create()
                self.itemtypes.append(itemtype_name)

            db_concept = Concept(
                db=self.db,
                id=concept.id,
                name=concept.name,
                balance=concept.balance if hasattr(concept, "balance") else None,
                nillable=concept.nillable,
                abstract=concept.is_abstract,
                periodtype=concept.periodType,
                ns_prefix=concept.ns_prefix,
                namespace=concept.namespace,
                substitution_group=concept.substitutionGroup,
                typed_domainref=concept.typedDomainRef,
                itemtype_name=itemtype_name,
                auto_commit=self.auto_commit,
                bind_to={
                    "dts": {
                        "dts_name": self.dts.entrypoint_name,
                        "directly_referenced": False,
                        "d_link": False,
                        "p_link": False,
                        "t_link": False,
                    }
                },
            )
            db_concept.find_or_create()
            for label in concept.labels.values():
                db_label = Label(
                    db=self.db,
                    id=label.id,
                    link_label=label.xlink_label,
                    link_role=label.linkRole,
                    lang=label.language,
                    label_text=label.text,
                    bind_to={"concept": concept.id},
                    dts_name=self.dts.entrypoint_name,
                    auto_commit=self.auto_commit,
                )
                db_label.find_or_create()
            for reference in concept.references:
                concept_reference = ConceptReference(
                    db=self.db,
                    concept_id=concept.id,
                    reference_id=reference.id
                    if reference.id is not None
                    else reference.xlink_label,
                    auto_commit=self.auto_commit,
                )
                concept_reference.find_or_create()

    def store_references(self):
        # they come from the linkroles (roletype.reference, we'll bind the later)
        for reference in self.dts.references:
            db_reference = Reference(
                db=self.db,
                id=reference.id,
                label=reference.xlink_label,
                name=reference.name,
                number=reference.number,
                issuedate=reference.issuedate,
                article=reference.article,
                chapter=reference.chapter,
                note=reference.note,
                section=reference.section,
                subsection=reference.subsection,
                publisher=reference.publisher,
                paragraph=reference.paragraph,
                subparagraph=reference.subparagraph,
                clause=reference.clause,
                subclause=reference.subclause,
                appendix=reference.appendix,
                example=reference.example,
                page=reference.page,
                exhibit=reference.exhibit,
                footnote=reference.footnote,
                sentence=reference.sentence,
                uri=reference.uri,
                uridate=reference.uridate,
                auto_commit=self.auto_commit,
            )
            db_reference.find_or_create()

    def store_linkroles(self):
        for linkrole in self.dts.roletypes.values():
            if linkrole.id is None:
                #  xbrl.org levert id-loze linkroles ... print(linkrole.roleURI)
                pass
            self.dot.dot()
            linkrole_order = None
            for order, ordered_linkrole in self.dts.linkrole_order.items():
                if (
                    hasattr(ordered_linkrole, "roleURI")
                    and ordered_linkrole.roleURI == linkrole.roleURI
                ):
                    linkrole_order = order
            db_linkrole = Linkrole(
                db=self.db,
                id=linkrole.id,
                role_uri=linkrole.roleURI,
                definition=linkrole.definition,
                dts_name=self.dts.entrypoint_name,
                directly_referenced=linkrole in self.dts.directly_referenced_linkbases,
                order=linkrole_order,
                auto_commit=self.auto_commit,
            )
            db_linkrole.find_or_create()

            for reference in linkrole.references:
                linkrole_reference = LinkroleReference(
                    db=self.db,
                    linkrole_rowid=db_linkrole.rowid,
                    reference_id=reference.id
                    if reference.id is not None
                    else reference.xlink_label,
                )
                linkrole_reference.find_or_create()

            if linkrole in self.dts.directly_referenced_linkbases:
                for variable in linkrole.resources["variables"]:

                    db_variable = Variable(
                        db=self.db,
                        id=variable.id if variable.id else variable.xlink_label,
                        type=type(variable).__name__,
                        label=variable.xlink_label,
                        bind_as_sequence=variable.bind_as_sequence
                        if hasattr(variable, "bind_as_sequence")
                        else None,
                        select=variable.select if hasattr(variable, "select") else None,
                        matches=variable.matches
                        if hasattr(variable, "matches")
                        else None,
                        nils=variable.nils if hasattr(variable, "nils") else None,
                        fallback_value=variable.fallback_value
                        if hasattr(variable, "fallback_value")
                        else None,
                        bind_to={"linkrole": db_linkrole.rowid},
                        auto_commit=self.auto_commit,
                    )
                    db_variable.find_or_create()
                    if hasattr(variable, "filters") and len(variable.filters):
                        self.store_filters(
                            filters=variable.filters,
                            bind_to={
                                "variable": variable.id
                                if variable.id
                                else variable.xlink_label
                            },
                        )

                for assertion in linkrole.resources["assertions"]:
                    # difference between OCW and KVK:
                    xlink_label_id = (
                        assertion.xlink_label
                        if isinstance(assertion.xlink_label, str)
                        else assertion.xlink_label.id
                    )
                    db_assertion = Assertion(
                        db=self.db,
                        id=assertion.id,
                        aspect_model=assertion.aspectModel,
                        implicit_filtering=assertion.implicitFiltering,
                        xlink_label=xlink_label_id,
                        test=assertion.test if hasattr(assertion, "test") else None,
                        assertion_type=type(assertion).__name__,
                        severity=assertion.severity,
                        bind_to={"linkrole": db_linkrole.rowid},
                        auto_commit=self.auto_commit,
                    )
                    db_assertion.find_or_create()
                    if assertion.labels:
                        for key, label in assertion.labels.items():
                            db_label = Label(
                                db=self.db,
                                id=label.id,
                                link_label=label.xlink_label,
                                link_role=label.linkRole,
                                lang=label.language,
                                label_text=label.text,
                                auto_commit=self.auto_commit,
                                bind_to={"assertion": assertion.id},
                            )
                            db_label.find_or_create()

                    if assertion.variableSet:
                        for varset in assertion.variableSet:
                            variableset = VariableSet(
                                db=self.db,
                                name=varset.name,
                                bind_to={"assertion": assertion.id},
                            )
                            variableset.find_or_create()

                            for var in varset.variables:
                                variableset_variable = VariablesetVariable(
                                    db=self.db,
                                    variableset_name=varset.name,
                                    variable_id=var["variable"].id
                                    if var["variable"].id
                                    else var["variable"].xlink_label,
                                    order=var["order"],
                                    priority=var["priority"],
                                )
                                variableset_variable.find_or_create()

                for used_on in linkrole.used_on:
                    db_used_on = UsedOn(
                        db=self.db,
                        used_on=used_on,
                        auto_commit=self.auto_commit,
                        bind_to={"linkrole": db_linkrole.rowid},
                    )
                    db_used_on.find_or_create()

                for arc in linkrole.arcs:
                    from_type = (
                        type(arc.from_concept_obj).__name__.lower()
                        if arc.from_concept_obj
                        else ""
                    )
                    to_type = (
                        type(arc.to_concept_obj).__name__.lower()
                        if arc.to_concept_obj
                        else ""
                    )
                    db_arc = Arc(
                        db=self.db,
                        linkrole_rowid=db_linkrole.rowid,
                        arc_type=arc.arc_type,
                        arcrole=arc.arcrole,
                        from_id=arc.from_concept,
                        to_id=arc.to_concept,
                        from_type=from_type,
                        to_type=to_type,
                        order=arc.order if hasattr(arc, "order") else 0,
                        dts_name=self.dts.entrypoint_name,
                        auto_commit=self.auto_commit,
                    )
                    db_arc.find_or_create()

                    nopelist = [
                        "roletype",
                        "label",
                        "enumerationchoice",
                        "reference",
                        "itemtype",
                        "",
                        "breakdown",
                        "message",
                        "parameter",
                        "valueassertion",
                        "existenceassertion",
                        "aspectnode",
                        "factvariable",
                    ]

                    if from_type == "concept":
                        self.update_concept_link(arc, arc.from_concept)

                    elif from_type == "table":
                        pass  # No concepts in here.
                    elif from_type == "rulenode":
                        # should go through rulesets
                        for ruleset in arc.from_concept_obj.rulesets:
                            ruleset_type = (
                                type(ruleset).__name__.lower() if ruleset else ""
                            )
                            if ruleset_type == "formulaexplicitdimension":
                                self.update_concept_link(
                                    arc, ruleset.qname_dimension_obj.id
                                )
                                if ruleset.qname_member_obj is not None:
                                    # Can also be a parameter, but if a member is set, also update this concept
                                    self.update_concept_link(
                                        arc, ruleset.qname_member_obj.id
                                    )
                            elif ruleset_type == " ruleset":
                                pass  # todo: EK: rulesets can be nested. Do we loop through these or
                                #  are we going to wait until these pop up as arcs?

                    elif from_type not in nopelist:
                        # filterperiodstart / filterperiodend is het antwoord
                        pass
                        # print(f"errm: {from_type}")

                    if to_type == "concept":
                        self.update_concept_link(arc, arc.to_concept)

                    elif to_type == "filterdimensionexplicit":
                        # Is referred via a filter.
                        self.update_concept_link(
                            arc, arc.to_concept_obj.qname_dimension_obj.id
                        )
                        if arc.to_concept_obj.qname_member_obj is not None:
                            self.update_concept_link(
                                arc, arc.to_concept_obj.qname_member_obj.id
                            )

                    elif to_type == "filterdimensiontyped":
                        self.update_concept_link(
                            arc, arc.to_concept_obj.qname_dimension_obj.id
                        )

                    elif to_type in ["conceptrelationshipnode"]:
                        self.update_concept_link_by_qname(
                            arc, arc.to_concept_obj.relationship_source
                        )
                    elif to_type == "dimensionrelationshipnode":
                        # Get the concept dimension
                        self.update_concept_link_by_qname(
                            arc, arc.to_concept_obj.dimension
                        )

                    elif from_type == "table":
                        pass  # No concepts in here.
                    elif from_type == "rulenode":
                        # should go through rulesets
                        for ruleset in arc.from_concept_obj.rulesets:
                            ruleset_type = (
                                type(ruleset).__name__.lower() if ruleset else ""
                            )
                            if ruleset_type == "formulaexplicitdimension":
                                self.update_concept_link(
                                    arc, ruleset.qname_dimension_obj.id
                                )
                                if ruleset.qname_member_obj is not None:
                                    # Can also be a parameter, but if a member is set, also update this concept
                                    self.update_concept_link(
                                        arc, ruleset.qname_member_obj.id
                                    )

                            elif ruleset_type in ["formulaperiod"]:
                                pass
                            elif ruleset_type in ["ruleset"]:
                                for subruleset in ruleset.formulas:
                                    subruleset_type = (
                                        type(subruleset).__name__.lower()
                                        if subruleset
                                        else ""
                                    )
                                    if subruleset_type == "formulaexplicitdimension":
                                        self.update_concept_link(
                                            arc, subruleset.qname_dimension_obj.id
                                        )
                                        if subruleset.qname_member_obj is not None:
                                            # Can also be a parameter, but if a member is set, also update this concept
                                            self.update_concept_link(
                                                arc, subruleset.qname_member_obj.id
                                            )
                                    elif subruleset_type in ["formulaperiod"]:
                                        pass
                                    else:
                                        print("")
                            else:
                                print("")

                    elif to_type not in nopelist:
                        # filterperiodstart / filterperiodend is het antwoord
                        pass
                        # print(f"errm: {to_type}")

                for key, label in linkrole.element_labels.items():
                    db_label = Label(
                        db=self.db,
                        id=label.id,
                        link_label=label.xlink_label,
                        link_role=label.linkRole,
                        lang=label.language,
                        label_text=label.text,
                        auto_commit=self.auto_commit,
                        bind_to={"dts": self.dts.entrypoint_name},
                    )
                    db_label.find_or_create()

            for key, label in linkrole.labels.items():
                db_label = Label(
                    db=self.db,
                    id=label.id,
                    link_label=label.xlink_label,
                    link_role=label.linkRole,
                    lang=label.language,
                    label_text=label.text,
                    auto_commit=self.auto_commit,
                    bind_to={"linkrole": db_linkrole.rowid},
                )
                db_label.find_or_create()

            if linkrole.hypercube:
                self.store_hypercube(
                    hypercube=linkrole.hypercube, linkrole_rowid=db_linkrole.rowid
                )

            if linkrole.definition_hierarchy:
                self.store_dimension(
                    dimension=linkrole.definition_hierarchy,
                    linkrole_rowid=db_linkrole.rowid,
                )

            for table in linkrole.tables:
                db_table = Table(
                    db=self.db,
                    id=table.id,
                    label=table.xlink_label,
                    parent_child_order=table.parentChildOrder if table.parentChildOrder else "parent-first",
                    auto_commit=self.auto_commit,
                    bind_to={"linkrole": db_linkrole.rowid},
                )
                db_table.find_or_create()
                if len(table.filters):
                    print(f"table filters!")
                    self.store_filters(
                        filters=table.filters, bind_to={"table": table.id}
                    )

                if hasattr(table, "labels"):
                    for key, label in table.labels.items():
                        db_label = Label(
                            db=self.db,
                            id=label.id,
                            link_label=label.xlink_label,
                            link_role=label.linkRole,
                            lang=label.language,
                            label_text=label.text,
                            auto_commit=self.auto_commit,
                            bind_to={"table": table.id},
                        )
                        db_label.find_or_create()

                for parameter in table.parameters:
                    if parameter is not None:
                        db_parameter = Parameter(
                            db=self.db,
                            id=parameter.id,
                            label=parameter.xlink_label,
                            as_datatype=parameter.as_datatype,
                            select=parameter.select,
                            name=parameter.name,
                            required=parameter.required,
                            bind_to={"table": table.id},
                            auto_commit=self.auto_commit,
                        )
                        db_parameter.find_or_create()

                for breakdown in table.table_breakdowns:
                    table_breakdown = TableBreakdown(
                        db=self.db,
                        id=breakdown.id,
                        label=breakdown.xlink_label,
                        parent_child_order=breakdown.parentChildOrder if breakdown.parentChildOrder else "parent-first",
                        axis=breakdown.axis,
                        table_id=table.id,
                        order=breakdown.order,
                        auto_commit=self.auto_commit,
                    )
                    table_breakdown.find_or_create()
                    for child in breakdown.children:
                        self.get_breakdown_subtree(child, table_breakdown)

            if linkrole.presentation_hierarchy:
                self.store_presentation(
                    presentation_node=linkrole.presentation_hierarchy,
                    parent_rowid=None,
                    parent_linkrole_rowid=db_linkrole.rowid,
                    dts_name=None,
                )

    #  this is not a store_function, move to the end when finished
    def get_breakdown_subtree(self, breakdown_child, parent):
        if type(breakdown_child).__name__ == "ConceptRelationshipNode":
            db_concept_relationshipnode = ConceptRelationshipNode(
                db=self.db,
                id=breakdown_child.id,
                label=breakdown_child.xlink_label,
                parent=parent.id,
                tagselector=breakdown_child.tagselector,
                order=breakdown_child.order,
                parent_child_order=breakdown_child.parentChildOrder,
                relationship_source=breakdown_child.relationship_source,
                linkrole_role_uri=breakdown_child.linkrole,
                formula_axis=breakdown_child.formula_axis,
                generations=breakdown_child.generations,
                arcrole=breakdown_child.arcrole,
                linkname=breakdown_child.linkname,
                arcname=breakdown_child.arcname,
                auto_commit=self.auto_commit,
            )
            db_concept_relationshipnode.find_or_create()

            for key, label in breakdown_child.labels.items():
                db_label = Label(
                    db=self.db,
                    id=label.id,
                    link_label=label.xlink_label,
                    link_role=label.linkRole,
                    lang=label.language,
                    label_text=label.text,
                    auto_commit=self.auto_commit,
                    bind_to={"concept_relationshipnode": breakdown_child.id},
                )
                db_label.find_or_create()

        elif type(breakdown_child).__name__ == "AspectNode":
            if not hasattr(breakdown_child, "aspect"):
                logging.warning(f"Aspectnode without children! {breakdown_child.xlink_label}")
            else:
                db_aspect_node = AspectNode(
                    db=self.db,
                    id=breakdown_child.id,
                    label=breakdown_child.xlink_label,
                    parent_child_order=None,
                    parent=parent.id,
                    tagselector=breakdown_child.tagselector,
                    auto_commit=self.auto_commit,
                    order=breakdown_child.order,
                    merge=breakdown_child.merge,
                    is_abstract=breakdown_child.is_abstract,
                    aspect_type=breakdown_child.aspect.aspect_type,
                    include_unreprted_value=breakdown_child.aspect.includeUnreportedValue,
                    value=breakdown_child.aspect.value,
                )
                db_aspect_node.find_or_create()

                for key, label in breakdown_child.labels.items():
                    db_label = Label(
                        db=self.db,
                        id=label.id,
                        link_label=label.xlink_label,
                        link_role=label.linkRole,
                        lang=label.language,
                        label_text=label.text,
                        auto_commit=self.auto_commit,
                        bind_to={"aspect_node": breakdown_child.id},
                    )
                    db_label.find_or_create()

        elif type(breakdown_child).__name__ == "DimensionRelationshipNode":
            db_dimension_relationship_node = DimensionRelationshipNode(
                db=self.db,
                id=breakdown_child.id,
                label=breakdown_child.xlink_label,
                parent_child_order=breakdown_child.parentChildOrder,
                parent=parent.id,
                order=breakdown_child.order,
                dimension_id=breakdown_child.dimension,
                relationship_source=breakdown_child.relationship_source,
                formula_axis=breakdown_child.formula_axis,
                generations=breakdown_child.generations,
                tagselector=breakdown_child.tagselector,
                linkrole_role_uri=breakdown_child.linkrole,
                auto_commit=self.auto_commit,
            )
            db_dimension_relationship_node.find_or_create()
        else:
            db_rulenode = TableRuleNode(
                db=self.db,
                id=breakdown_child.id,
                label=breakdown_child.xlink_label,
                parent_child_order=breakdown_child.parentChildOrder,
                parent=parent.id,
                tagselector=breakdown_child.tagselector,
                auto_commit=self.auto_commit,
                order=breakdown_child.order,
                merge=breakdown_child.merge == "true",
                is_abstract=breakdown_child.is_abstract == "true",
            )
            db_rulenode.find_or_create()
            for key, label in breakdown_child.labels.items():
                db_label = Label(
                    db=self.db,
                    id=label.id,
                    link_label=label.xlink_label,
                    link_role=label.linkRole,
                    lang=label.language,
                    label_text=label.text,
                    auto_commit=self.auto_commit,
                    bind_to={"table_rulenode": breakdown_child.id},
                )
                db_label.find_or_create()

            if breakdown_child.rulesets:
                for item in breakdown_child.rulesets:
                    if type(item).__name__ == "RuleSet":
                        db_ruleset = RuleSet(
                            db=self.db,
                            rulenode_id=db_rulenode.id,
                            tag=item.tag,
                            auto_commit=self.auto_commit,
                        )
                        db_ruleset.find_or_create()
                        for formula in item.formulas:
                            self.store_formula(formula, parent_ruleset=db_ruleset.rowid)
                    else:
                        self.store_formula(
                            item,
                            parent_node=breakdown_child.id,
                            dimension_id=item.dimension
                            if hasattr(item, "dimension")
                            else None,
                        )

        for child in breakdown_child.children:
            self.get_breakdown_subtree(breakdown_child=child, parent=breakdown_child)
            # print(f"{type(breakdown_child).__name__} {len(breakdown_child.children)}")

    def store_formula(
        self, formula, parent_ruleset=None, parent_node=None, dimension_id=None
    ):
        if type(formula).__name__ == "FormulaPeriod":
            db_formula_period = FormulaPeriod(
                db=self.db,
                period_type=formula.periodtype,
                start=formula.start if hasattr(formula, "start") else None,
                end=formula.end if hasattr(formula, "end") else None,
                value=formula.value if hasattr(formula, "value") else None,
                parent_node=parent_node,
                parent_ruleset=parent_ruleset,
                auto_commit=self.auto_commit,
            )
            db_formula_period.find_or_create()
            # print(db_formula_period.rowid)
        elif type(formula).__name__ == "FormulaExplicitDimension":
            db_formula_explicit_dimension = FormulaExplicitDimension(
                db=self.db,
                qname_dimension=formula.qname_dimension,
                qname_member=formula.qname_member,
                qname_member_expression=formula.qname_member_expression
                if hasattr(formula, "qname_member_expression")
                else None,
                type_qname=formula.type_qname,
                omit=formula.omit,
                parent_node=parent_node,
                parent_ruleset=parent_ruleset,
                auto_commit=self.auto_commit,
            )
            db_formula_explicit_dimension.find_or_create()
        else:
            # TODO: typed_dimension
            print(type(formula).__name__)

    def store_hypercube(self, hypercube, linkrole_rowid):
        db_hypercube = Hypercube(
            db=self.db,
            hc_role_uri=hypercube.linkrole.roleURI,
            linkrole_rowid=linkrole_rowid,
            hc_type=hypercube.hc_type,
            context_element=hypercube.context_element,
            auto_commit=self.auto_commit,
        )
        db_hypercube.find_or_create()

        self.store_line_items(hypercube.lineItems, linkrole_rowid)

        for dimension in hypercube.dimensions:
            db_dimension = Dimension(
                db=self.db,
                concept_id=dimension.concept.id,
                hypercube_linkrole_rowid=linkrole_rowid,
                target_linkrole=dimension.target_linkrole.roleURI
                if dimension.target_linkrole
                else None,
                linkrole_rowid=None,
                auto_commit=self.auto_commit,
            )
            db_dimension.find_or_create()
            self.dimension_hierarchy(dimension=dimension, db_dimension=db_dimension)

    def store_line_items(self, lineitems, linkrole_rowid):
        for line_item in lineitems:
            db_line_item = LineItem(
                db=self.db,
                order=line_item.order,
                hypercube_linkrole_rowid=linkrole_rowid,
                concept_id=line_item.id,
                parent_concept_id=line_item.parent_concept_id,
                auto_commit=self.auto_commit,
            )
            db_line_item.find_or_create()
            if line_item.children:
                self.store_line_items(line_item.children, linkrole_rowid)

    def dimension_hierarchy(self, dimension, db_dimension):
        if dimension.height > 1:
            for child in dimension.children:
                self.recursive_members(
                    member=child,
                    parent_rowid=db_dimension.rowid,
                    parent_type="dimension",
                )
        elif not len(dimension.domains):
            for member in dimension.members:
                db_member = Member(
                    db=self.db,
                    concept_id=member.concept.id,
                    order=member.order,
                    usable=member.usable,
                    target_linkrole=member.target_linkrole,
                    preferred_label=member.preferred_label,
                    parent_rowid=db_dimension.rowid,
                    parent_type="dimension",
                    auto_commit=self.auto_commit,
                )
                db_member.find_or_create()
        for domain in dimension.domains:
            usable = domain.usable if hasattr(domain, "usable") else False
            db_domain = Domain(
                db=self.db,
                concept_id=domain.concept.id,
                usable=usable,
                dimension_rowid=db_dimension.rowid,
                auto_commit=self.auto_commit,
            )
            db_domain.find_or_create()
            for member in domain.members:
                db_member = Member(
                    db=self.db,
                    concept_id=member.concept.id,
                    order=member.order,
                    usable=member.usable,
                    target_linkrole=member.target_linkrole,
                    preferred_label=member.preferred_label,
                    parent_rowid=db_domain.rowid,
                    parent_type="domain",
                    auto_commit=self.auto_commit,
                )
                db_member.find_or_create()
                for child in member.children:
                    self.recursive_members(
                        member=child,
                        parent_rowid=db_member.rowid,
                        parent_type=type(member).__name__.lower(),
                    )

    def recursive_members(self, member, parent_rowid, parent_type):
        db_member = Member(
            db=self.db,
            concept_id=member.concept.id,
            order=member.order,
            usable=member.usable,
            target_linkrole=member.target_linkrole,
            preferred_label=member.preferred_label,
            parent_rowid=parent_rowid,
            parent_type=parent_type,
            auto_commit=self.auto_commit,
        )
        db_member.find_or_create()

        for child in member.children:
            self.recursive_members(
                member=child,
                parent_rowid=db_member.rowid,
                parent_type=type(member).__name__.lower(),
            )

    def store_dimension(self, dimension, linkrole_rowid):
        db_dimension = Dimension(
            db=self.db,
            concept_id=dimension.concept.id,
            hypercube_linkrole_rowid=None,
            target_linkrole=dimension.target_linkrole.roleURI
            if dimension.target_linkrole
            else None,
            linkrole_rowid=linkrole_rowid,
            auto_commit=self.auto_commit,
        )
        db_dimension.find_or_create()
        self.dimension_hierarchy(dimension=dimension, db_dimension=db_dimension)

    def store_presentation(
        self,
        presentation_node=None,
        parent_rowid=None,
        dts_name=None,
        parent_linkrole_rowid=None,
    ):
        if presentation_node:
            id = (
                presentation_node.object.name
                if hasattr(presentation_node.object, "name")
                else presentation_node.object.roleURI
            )
            concept_id = (
                presentation_node.object.id
                if hasattr(presentation_node.object, "name")
                else None
            )
            linkrole_role_uri = (
                presentation_node.object.roleURI
                if hasattr(presentation_node.object, "roleURI")
                else None
            )
            preferred_label = (
                presentation_node.preferred_label
                if hasattr(presentation_node, "preferred_label")
                else None
            )
            if parent_rowid is None:
                is_root = True
            else:
                is_root = presentation_node.is_root
            # if this is a concept, check if any of the descendants is a linkrole
            # which is directly_referenced. False means there is no content to show
            directly_referenced = True
            if hasattr(presentation_node.object, "name"):
                linkroles = [
                    p_node
                    for p_node in presentation_node.descendants
                    if not hasattr(p_node.object, "name")
                    and p_node.object in self.dts.directly_referenced_linkbases
                ]
                # the weirdest shit man, the outcome differs per machine?
                if not len(linkroles):
                    directly_referenced = False

            presentation_hierarchy_node = PresentationHierarchyNode(
                db=self.db,
                id=id,
                concept_id=concept_id,
                linkrole_role_uri=linkrole_role_uri,
                is_root=is_root,
                order=presentation_node.order,
                preferred_label=preferred_label,
                parent_rowid=parent_rowid,
                dts_name=dts_name,
                parent_linkrole_rowid=parent_linkrole_rowid,
                directly_referenced=directly_referenced,
                auto_commit=self.auto_commit,
            )
            presentation_hierarchy_node.find_or_create()

            if presentation_node.children:
                for child in presentation_node.children:
                    self.store_presentation(
                        presentation_node=child,
                        parent_rowid=presentation_hierarchy_node.rowid,
                        dts_name=None,
                        parent_linkrole_rowid=None,
                    )

    def store_filters(self, filters=None, bind_to=None, parent_filter=None):
        for filter in filters:
            kwargs = filter.__dict__.copy()
            if hasattr(filter, "type_qname"):
                kwargs["type_qname"] = filter.type_qname
            # for the time being; alle lists platslaan tot 1e item:
            # delkeys; keys to delete because off ...
            delkeys = []
            for key, value in kwargs.items():
                if isinstance(value, list):
                    kwargs[key] = None
                    if len(value):
                        if isinstance(value[0], str):
                            kwargs[key] = value[0]
                        else:
                            kwargs[key] = value[0].id
                if key.startswith("_Filter"):
                    delkeys.append(key)
            for key in delkeys:
                del kwargs[key]

            if kwargs.get("linkrole"):
                if hasattr(kwargs["linkrole"], "roleURI"):
                    kwargs["linkrole_role_uri"] = kwargs["linkrole"].roleURI
                del kwargs["linkrole"]
            if "qname_obj" in kwargs.keys():
                if hasattr(filter, "qname"):
                    kwargs["qname"] = filter.qname
                elif hasattr(filter, "type_qname"):
                    kwargs["qname"] = filter.type_qname
                else:
                    print("geen qname, geen type_qname:")
                    print(kwargs)
                del kwargs["qname_obj"]
            if "qname_dimension_obj" in kwargs.keys():
                kwargs["qname_dimension"] = filter.qname_dimension
                del kwargs["qname_dimension_obj"]
            if "qname_member_obj" in kwargs.keys():
                kwargs["qname_member"] = filter.qname_member
                del kwargs["qname_member_obj"]
            if "subfilters" in kwargs.keys():
                subfilters = [subfilter for subfilter in filter.subfilters]
                del kwargs["subfilters"]
            else:
                subfilters = None

            if "parent_obj" in kwargs.keys() and parent_filter is None:
                parent_filter = (
                    filter.parent_obj.id
                    if filter.parent_obj.id
                    else filter.parent_obj.xlink_label
                )
            if "parent_obj" in kwargs.keys():
                del kwargs["parent_obj"]

            delkeys = []
            for key in kwargs.keys():
                if key.endswith("__qname") or key.endswith("__qname_member"):
                    delkeys.append(key)
            for key in delkeys:
                del kwargs[key]

            db_filter = Filter(
                db=self.db,
                type=type(filter).__name__,
                **kwargs,
                parent=parent_filter if parent_filter else None,
                auto_commit=self.auto_commit,
                bind_to=bind_to,
            )
            db_filter.find_or_create()
            if subfilters is not None:
                self.store_filters(
                    filters=subfilters, bind_to=None, parent_filter=filter.xlink_label
                )

    def update_concept_link_by_qname(self, arc, qname):
        concepts = Concepts(db=self.db, full=False)
        concepts.concept_by_qname(qname=qname, dts_name=self.dts.entrypoint_name)
        if len(concepts.concepts):
            self.update_concept_link(arc=arc, concept_id=concepts.concepts[0]["id"])

    def update_concept_link(self, arc, concept_id):
        #  only Arc's from directly referenced linkbases visit here
        concepts = Concepts(db=self.db, full=False)
        concepts.concept_by_id(id=concept_id, dts_name=self.dts.entrypoint_name)
        if len(concepts.concepts) > 0:
            dts_concept = DTSConcept(
                db=self.db,
                dts_name=self.dts.entrypoint_name,
                concept_id=concept_id,
                auto_commit=self.auto_commit,
            )
            dts_concept.find_or_create()
            if concept_id not in self.concepts_directly_referenced:
                dts_concept.update_reference("directly_referenced", True)
                self.concepts_directly_referenced.append(concept_id)
            if (
                arc.arc_type == "{http://www.xbrl.org/2003/linkbase}definitionArc"
                and concept_id not in self.concepts_definition_link
            ):
                dts_concept.update_reference("d_link", True)
                self.concepts_definition_link.append(concept_id)
            elif (
                arc.arc_type == "{http://www.xbrl.org/2003/linkbase}presentationArc"
                and concept_id not in self.concepts_presentation_link
            ):
                dts_concept.update_reference("p_link", True)
                self.concepts_presentation_link.append(concept_id)
            elif arc.arc_type.startswith("{http://xbrl.org/2014/table}"):
                dts_concept.update_reference("t_link", True)
                self.concepts_table_link.append(concept_id)
            elif arc.arc_type == "{http://xbrl.org/2008/generic}arc":
                pass
            elif arc.arc_type not in [
                "{http://www.xbrl.org/2003/linkbase}definitionArc",
                "{http://www.xbrl.org/2003/linkbase}presentationArc",
            ]:
                pass
        else:
            print("Strange concepts ....")
            print(arc.arcrole)

    def find_referenced_concepts(self):
        if not self.dts:
            return
        referenced_concepts = []

        for linkrole in self.dts.directly_referenced_linkbases:
            if linkrole.roleURI == "http://www.xbrl.org/2008/role/link":
                continue
            for arc in linkrole.arcs:
                if isinstance(arc.from_concept_obj, DtsConcept):
                    referenced_concepts.append(arc.from_concept)
                if isinstance(arc.to_concept_obj, DtsConcept):
                    referenced_concepts.append(arc.to_concept)

                if isinstance(arc.from_concept_obj, Resource):
                    referenced_concepts.append(arc.from_concept_obj.xlink_label)
                if isinstance(arc.to_concept_obj, Resource):
                    referenced_concepts.append(arc.to_concept_obj.xlink_label)
            if linkrole.hypercube:
                for dimension in linkrole.hypercube.dimensions:
                    referenced_concepts.append(dimension.concept.id)
                    for descendant in dimension.descendants:
                        referenced_concepts.append(descendant.concept.id)
        return list(set(referenced_concepts))


class Dot:
    def __init__(self, chars=132, silent=False):
        self.line_width = chars
        self.pos = 0
        self.silent = silent

    def dot(self, newline=False):
        if not self.silent:
            print(".", end="")
            self.pos += 1
            if self.pos > self.line_width or newline:
                self.pos = 0
                print("")


def main():
    dts_to_sql = DtsToSql()
    if hasattr(conf, "editor"):
        zip_url = Path(conf.editor["load_default_dts"])
        dts_list = []
        if zip_url.name:  # If a file is selected, else pass
            with ZipFile(zip_url, "r") as dts_package_zip:
                for file in dts_package_zip.namelist():
                    if str(file).startswith("dts/"):
                        obj = dts_package_zip.read(file)
                        dts = pickle.loads(obj)
                        dts_to_sql.set_dts(dts)


if __name__ == "__main__":
    conf = Config()
    main()
