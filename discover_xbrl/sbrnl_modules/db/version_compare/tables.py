from discover_xbrl.sbrnl_modules.db.version_compare.compare_mixin import CompareMixin


class Tables(CompareMixin):
    def __init__(self, parent, entrypoints=None, domein=None):
        self.parent = parent
        self.entrypoints = entrypoints
        self.domein = domein

    @property
    def queries(self):
        queries = {}
        if self.domein:
            queries["new"] = self.parent.compare_db.rewrite_query(
                "select distinct xbrl_table.id, label, parent_child_order from xbrl_table "
                "left join linkrole_table lt on xbrl_table.id = lt.table_id "
                "left join linkrole l on l.rowid = lt.linkrole_rowid "
                "where dts_name in %s and directly_referenced"
            )
            queries["compare"] = self.parent.compare_db.rewrite_query(
                "select xbrl_table.id, label, parent_child_order from xbrl_table "
                "left join linkrole_table lt on xbrl_table.id = lt.table_id "
                "left join linkrole l on l.rowid = lt.linkrole_rowid "
                "where table_id = %s and dts_name in %s and directly_referenced"
            )
            queries["backref"] = self.parent.compare_db.rewrite_query(
                "select dts_name, definition from linkrole "
                "left join linkrole_table lt on linkrole.rowid = lt.linkrole_rowid "
                "where table_id = %s and directly_referenced "
                "and dts_name in %s"
            )
        elif not self.entrypoints:
            queries["new"] = self.parent.compare_db.rewrite_query(
                "select id, label, parent_child_order from xbrl_table"
            )
            queries["compare"] = self.parent.compare_db.rewrite_query(
                "select dts_name, definition from linkrole "
                "left join linkrole_table lt on linkrole.rowid = lt.linkrole_rowid "
                "where table_id = %s and directly_referenced "
            )
            queries["backref"] = self.parent.compare_db.rewrite_query(
                "select dts_name, definition from linkrole "
                "left join linkrole_table lt on linkrole.rowid = lt.linkrole_rowid "
                "where table_id = %s and directly_referenced "
            )
        else:
            queries["new"] = self.parent.compare_db.rewrite_query(
                "select xbrl_table.id, label, parent_child_order from xbrl_table "
                "left join linkrole_table lt on xbrl_table.id = lt.table_id "
                "left join linkrole l on l.rowid = lt.linkrole_rowid "
                "where dts_name = %s and directly_referenced"
            )
            queries["compare"] = self.parent.compare_db.rewrite_query(
                "select xbrl_table.id, label, parent_child_order from xbrl_table "
                "left join linkrole_table lt on xbrl_table.id = lt.table_id "
                "left join linkrole l on l.rowid = lt.linkrole_rowid "
                "where table_id = %s and dts_name = %s and directly_referenced"
            )
        return queries

    def compare(self):
        if self.domein:
            new_tables = self.new_objects(
                args=(self.parent.new_domeinen.get_domein(self.domein),)
            )
        elif not self.entrypoints:
            new_tables = self.new_objects()
        else:
            new_tables = self.new_objects(args=(self.entrypoints["new_version"],))
        new = []
        for table in new_tables:
            if self.domein:
                args = (table[0], self.parent.old_domeinen.get_domein(self.domein))
            elif not self.entrypoints:
                args = (table[0],)
            else:
                args = (table[0], self.entrypoints["old_version"])

            self.parent.checkpoint_cursor.execute(self.queries["compare"], args)
            matches = self.parent.checkpoint_cursor.fetchall()
            if not len(matches):
                if self.domein and self.object_seen(
                    object_type="table", object_id=table[0], mutation_type="new"
                ):
                    continue
                new.append(table)

        block = {
            "title": "Tables",
            "summary": f"Total Tables: {len(new_tables)}.  New tables: {len(new)}.",
            "detail": [],
        }
        if not self.entrypoints:
            block["detail"].append(
                f"<p>Details linken naar een willekeurig entrypoint waarin dit item voorkomt. "
                f"De Dimension kan theoretisch in elk entrypoint aangeroepen worden.</p><ul>"
            )
        for table in sorted(new):
            if not self.entrypoints:
                args = (
                    (table[0],)
                    if not self.domein
                    else (table[0], self.parent.new_domeinen.get_domein(self.domein))
                )
                self.parent.compare_cursor.execute(self.queries["backref"], args)
                dts_name = self.parent.compare_cursor.fetchone()
                if dts_name:
                    block["detail"].append(
                        f"<li><a href='{self.webserver}/dts/{self.parent.new_version_db}/{dts_name[0]}"
                        f"/tables/table/{table[0]}'>{dts_name[1]}</a>"
                        f" <em class='smaller'>{table[0].replace('_', ' ')}</em>"
                    )
            else:
                block["detail"].append(
                    f"<li>"
                    f"<a href='{self.webserver}/dts/{self.parent.new_version_db}/{self.entrypoints['new_version']}"
                    f"/tables/table/{table[0]}'>"
                    f" <em class='smaller'>{table[0].replace('_', ' ')}</em></a>"
                )
        block["detail"].append("</ul")

        self.parent.report.html.append(block)
