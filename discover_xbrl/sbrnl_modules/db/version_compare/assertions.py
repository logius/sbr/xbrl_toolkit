from discover_xbrl.sbrnl_modules.db.version_compare.compare_mixin import CompareMixin


class Assertions(CompareMixin):
    def __init__(self, parent, entrypoints=None, domein=None):
        self.parent = parent
        self.entrypoints = entrypoints
        self.domein = domein

    @property
    def queries(self):
        queries = {}
        if self.domein:
            queries["new"] = self.parent.compare_db.rewrite_query(
                "select distinct(assertion.id), aspect_model, implicit_filtering, xlink_label, test, "
                "assertion_type, severity from assertion "
                "left join linkrole_assertion la on assertion.id = la.assertion_id "
                "left join linkrole l on l.rowid = la.linkrole_rowid "
                "where l.dts_name in %s and directly_referenced "
            )
            queries["compare"] = self.parent.compare_db.rewrite_query(
                "select distinct(assertion.id), aspect_model, implicit_filtering, xlink_label, test, "
                "assertion_type, severity from assertion "
                "left join linkrole_assertion la on assertion.id = la.assertion_id "
                "left join linkrole l on l.rowid = la.linkrole_rowid "
                "where assertion.id = %s and l.dts_name in %s "
            )
            queries["backref"] = self.parent.compare_db.rewrite_query(
                "select dts_name "
                "from assertion "
                "left join linkrole_assertion la on assertion.id = la.assertion_id "
                "left join linkrole l on l.rowid = la.linkrole_rowid and l.directly_referenced "
                "where assertion.id = %s "
                "and dts_name in %s"
            )
        elif not self.entrypoints:
            queries["new"] = self.parent.compare_db.rewrite_query(
                "select id, aspect_model, implicit_filtering, xlink_label, test, "
                "assertion_type, severity from assertion"
            )
            queries["compare"] = self.parent.compare_db.rewrite_query(
                "select id, aspect_model, implicit_filtering, xlink_label, test, "
                "assertion_type, severity from assertion "
                "where id = %s"
            )
            queries["backref"] = self.parent.compare_db.rewrite_query(
                "select dts_name "
                "from assertion "
                "left join linkrole_assertion la on assertion.id = la.assertion_id "
                "left join linkrole l on l.rowid = la.linkrole_rowid and l.directly_referenced "
                "where assertion.id = %s"
            )
        else:
            queries["new"] = self.parent.compare_db.rewrite_query(
                "select distinct(assertion.id), aspect_model, implicit_filtering, xlink_label, test, assertion_type, "
                " severity from assertion "
                "left join linkrole_assertion la on assertion.id = la.assertion_id "
                "left join linkrole l on l.rowid = la.linkrole_rowid "
                "where l.dts_name = %s and directly_referenced "
            )
            queries["compare"] = self.parent.compare_db.rewrite_query(
                "select distinct(assertion.id), aspect_model, implicit_filtering, xlink_label, test, "
                "assertion_type, severity from assertion "
                "left join linkrole_assertion la on assertion.id = la.assertion_id "
                "left join linkrole l on l.rowid = la.linkrole_rowid "
                "where assertion.id = %s and l.dts_name = %s "
            )

        return queries

    def compare(self):
        if self.domein:
            new_assertions = self.new_objects(
                args=(self.parent.new_domeinen.get_domein(self.domein),)
            )
        elif not self.entrypoints:
            new_assertions = self.new_objects()
        else:
            new_assertions = self.new_objects(args=(self.entrypoints["new_version"],))
        new = []
        for assertion in new_assertions:
            if self.domein:
                args = (
                    assertion[0],
                    (self.parent.old_domeinen.get_domein(self.domein)),
                )
            elif not self.entrypoints:
                args = (assertion[0],)
            else:
                args = (assertion[0], self.entrypoints["old_version"])

            self.parent.checkpoint_cursor.execute(self.queries["compare"], args)
            matches = self.parent.checkpoint_cursor.fetchall()
            if not len(matches):
                if self.domein and self.object_seen(
                    object_type="assertion", object_id=assertion[0], mutation_type="new"
                ):
                    continue
                new.append(assertion)

        block = {
            "title": "Assertions",
            "summary": f"Total Assertions: {len(new_assertions)}.  New assertions: {len(new)}. ",
            "detail": [],
        }

        if not self.entrypoints:
            block["detail"].append(
                f"<p>Details linken naar een willekeurig entrypoint waarin dit item voorkomt. "
                f"De Assertion kan theoretisch in elk entrypoint aangeroepen worden.</p>"
            )
        block["detail"].append("<ul>")
        for assertion in sorted(new):
            if not self.entrypoints:
                args = (
                    (assertion[0],)
                    if not self.domein
                    else (
                        assertion[0],
                        self.parent.new_domeinen.get_domein(self.domein),
                    )
                )
                self.parent.compare_cursor.execute(self.queries["backref"], args)
                dts_name = self.parent.compare_cursor.fetchone()
                block["detail"].append(
                    f"<li><a href='{self.webserver}/dts/{self.parent.new_version_db}/{dts_name[0]}"
                    f"/assertions/{assertion[0]}'>{assertion[0]}</a>"
                )
            else:
                block["detail"].append(
                    f"<li><a href='{self.webserver}/dts/{self.parent.new_version_db}"
                    f"/{self.entrypoints['new_version']}"
                    f"/assertions/{assertion[0]}'>{assertion[0]}</a>"
                )
        block["detail"].append("</ul>")

        self.parent.report.html.append(block)
