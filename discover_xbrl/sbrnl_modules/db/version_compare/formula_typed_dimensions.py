from discover_xbrl.sbrnl_modules.db.version_compare.compare_mixin import CompareMixin


class FormulaTypedDimensions(CompareMixin):
    def __init__(self, parent, entrypoints=None):
        self.parent = parent
        self.entrypoints = entrypoints

    @property
    def queries(self):
        queries = {}
        if not self.entrypoints:
            queries["new"] = self.parent.compare_db.rewrite_query(
                "select distinct qname_dimension, coalesce(xpath, ''), "
                "coalesce(value, '') "
                "from formula_typed_dimension order by qname_dimension"
            )
            queries["compare"] = self.parent.compare_db.rewrite_query(
                "select distinct qname_dimension, coalesce(xpath, ''), "
                "coalesce(value, '') "
                "from formula_typed_dimension "
                "where qname_dimension = %s "
                "  and coalesce(xpath, '') = coalesce(%s, '') "
                "  and coalesce(value, '') = coalesce(%s, '')"
            )
        else:
            # really? back-searching linkrole ..... meh
            pass
        return queries

    def compare(self):
        if not self.entrypoints:
            new_formulas = self.new_objects()
        else:
            new_formulas = self.new_objects((self.entrypoints["new_version"],))
        new = []
        for formula in new_formulas:
            args = (formula[0], formula[1], formula[2])
            self.parent.checkpoint_cursor.execute(self.queries["compare"], args)
            matches = self.parent.checkpoint_cursor.fetchall()
            if not len(matches):
                new.append(formula)

        block = {
            "title": "Formula Typed Dimensions",
            "summary": f"Total Formulas typed dimensions: {len(new_formulas)}.  New formulas: {len(new)}. ",
            "detail": [],
        }
        for formula in new:
            pass
            # these are currently non-existent.
            # I'll be back
        self.parent.report.html.append(block)
