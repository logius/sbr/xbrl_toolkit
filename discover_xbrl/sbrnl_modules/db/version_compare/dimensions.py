from discover_xbrl.sbrnl_modules.db.version_compare.compare_mixin import CompareMixin


class Dimensions(CompareMixin):
    def __init__(self, parent, entrypoints=None, domein=None):
        self.parent = parent
        self.entrypoints = entrypoints
        self.domein = domein

    @property
    def queries(self):
        queries = {}
        if self.domein:
            queries["new"] = self.parent.compare_db.rewrite_query(
                "select distinct concept_id, target_linkrole, hc_role_uri, dimension.linkrole_rowid from dimension "
                "left join hypercube on dimension.hypercube_linkrole_rowid = hypercube.linkrole_rowid "
                "left join linkrole l on l.rowid = hypercube.linkrole_rowid "
                "where l.dts_name in %s and directly_referenced and dimension.linkrole_rowid is null"
            )
            queries["compare"] = self.parent.checkpoint_db.rewrite_query(
                "select concept_id, target_linkrole, hc_role_uri, dimension.linkrole_rowid from dimension "
                "left join hypercube on dimension.hypercube_linkrole_rowid = hypercube.linkrole_rowid "
                "left join linkrole l on l.rowid = hypercube.linkrole_rowid "
                "where concept_id = %s and hc_role_uri = %s "
                "  and coalesce(target_linkrole, '') = coalesce(%s, '') "
                "  and dimension.linkrole_rowid is null "
                "  and dts_name in %s"
            )
            queries["backref"] = self.parent.compare_db.rewrite_query(
                "select dts_name, l.id from dimension "
                "left join linkrole l on dimension.hypercube_linkrole_rowid = l.rowid "
                "where dimension.concept_id = %s and role_uri = %s "
                "  and coalesce(target_linkrole, '') = coalesce(%s, '') "
                "  and dimension.linkrole_rowid is null "
                "  and dts_name in %s"
            )
        elif not self.entrypoints:
            queries["new"] = self.parent.compare_db.rewrite_query(
                "select distinct concept_id, target_linkrole, hc_role_uri, dimension.linkrole_rowid from dimension "
                "left join hypercube on dimension.hypercube_linkrole_rowid = hypercube.linkrole_rowid "
                "where dimension.linkrole_rowid is null "
            )
            queries["compare"] = self.parent.checkpoint_db.rewrite_query(
                "select concept_id, target_linkrole, hc_role_uri, dimension.linkrole_rowid from dimension "
                "left join hypercube on dimension.hypercube_linkrole_rowid = hypercube.linkrole_rowid "
                "where concept_id = %s and hc_role_uri = %s "
                "  and coalesce(target_linkrole, '') = coalesce(%s, '')"
                "  and dimension.linkrole_rowid is null "
            )
            queries["backref"] = self.parent.compare_db.rewrite_query(
                "select dts_name, l.id from dimension "
                "left join linkrole l on dimension.hypercube_linkrole_rowid = l.rowid "
                "where dimension.concept_id = %s and role_uri = %s "
                "  and coalesce(target_linkrole, '') = coalesce(%s, '') "
                "  and dimension.linkrole_rowid is null "
            )
        else:
            queries["new"] = self.parent.compare_db.rewrite_query(
                "select distinct concept_id, target_linkrole, hc_role_uri, dimension.linkrole_rowid from dimension "
                "left join hypercube on dimension.hypercube_linkrole_rowid = hypercube.linkrole_rowid "
                "left join linkrole l on l.rowid = hypercube.linkrole_rowid "
                "where l.dts_name = %s and directly_referenced and dimension.linkrole_rowid is null "
            )
            queries["compare"] = self.parent.checkpoint_db.rewrite_query(
                "select concept_id, target_linkrole, hc_role_uri, dimension.linkrole_rowid from dimension "
                "left join hypercube on dimension.hypercube_linkrole_rowid = hypercube.linkrole_rowid "
                "left join linkrole l on l.rowid = hypercube.linkrole_rowid "
                "where concept_id = %s and hc_role_uri = %s "
                "  and coalesce(target_linkrole, '') = coalesce(%s, '') "
                "  and dimension.linkrole_rowid is null "
                "  and dts_name = %s"
            )
        return queries

    def compare(self):
        if self.domein:
            new_dimensions = self.new_objects(
                args=(self.parent.new_domeinen.get_domein(self.domein),)
            )
        elif not self.entrypoints:
            new_dimensions = self.new_objects()
        else:
            new_dimensions = self.new_objects(args=(self.entrypoints["new_version"],))

        new = []
        for dm in new_dimensions:
            if self.domein:
                args = (
                    dm[0],
                    dm[2],
                    dm[1],
                    self.parent.old_domeinen.get_domein(self.domein),
                )
            elif not self.entrypoints:
                args = (dm[0], dm[2], dm[1])
            else:
                args = (dm[0], dm[2], dm[1], self.entrypoints["old_version"])

            self.parent.checkpoint_cursor.execute(self.queries["compare"], args)
            matches = self.parent.checkpoint_cursor.fetchall()
            if not len(matches) and dm not in new:
                fake_id = f"{dm[0]}-{dm[1]}-{dm[2]}-{dm[3]}"
                if self.domein and self.object_seen(
                    object_type="dimension", object_id=fake_id, mutation_type="new"
                ):
                    continue
                new.append(dm)

        block = {
            "title": "Dimensions",
            "summary": f"Total Dimensions: {len(new_dimensions)}.  New dimensions: {len(new)}.",
            "detail": [],
        }
        if self.domein:
            for dim in new:
                self.parent.compare_cursor.execute(
                    self.queries["backref"],
                    (
                        dim[0],
                        dim[2],
                        dim[1],
                        self.parent.new_domeinen.get_domein(self.domein),
                    ),
                )
                dts_name = self.parent.compare_cursor.fetchone()
                block["detail"].append(
                    f"<li><a href='{self.webserver}/dts/{self.parent.new_version_db}/{dts_name[0]}"
                    f"/linkroles/linkroles/{dts_name[1]}'>{dim[0]}</a>"
                    f" <em class='smaller'>{dim[2]}</em> target: {dim[1]}</li>"
                )
        elif not self.entrypoints:
            block["detail"].append(
                f"<p>Details linken naar een willekeurig entrypoint waarin dit item voorkomt. "
                f"De Dimension kan theoretisch in elk entrypoint aangeroepen worden.</p><ul>"
            )
            for dim in new:
                self.parent.compare_cursor.execute(
                    self.queries["backref"], (dim[0], dim[2], dim[1])
                )
                dts_name = self.parent.compare_cursor.fetchone()
                if dts_name:
                    block["detail"].append(
                        f"<li><a href='{self.webserver}/dts/{self.parent.new_version_db}/{dts_name[0]}"
                        f"/linkroles/linkroles/{dts_name[1]}'>{dim[0]}</a>"
                        f" <em class='smaller'>{dim[2]}</em> target: {dim[1]}</li>"
                    )
                else:
                    print(f"{dim[0]}-{dim[1]}-{dim[2]}-{dim[3]} kan geen dts vinden?")
        elif self.entrypoints:
            block["detail"].append("<ul>")
            for dim in new:
                block["detail"].append(
                    f"<li><a href='{self.webserver}/dts/{self.parent.new_version_db}/{self.entrypoints['new_version']}"
                    f"/linkroles/linkroles/{dim[0]}'>{dim[0]}</a>"
                    f" <em class='smaller'>{dim[2]}</em> target: {dim[1]}</li>"
                )
        block["detail"].append("</ul>")

        self.parent.report.html.append(block)
