from discover_xbrl.sbrnl_modules.db.version_compare.compare_mixin import CompareMixin


class AspectNodes(CompareMixin):
    def __init__(self, parent, entrypoints=None):
        self.parent = parent
        self.entrypoints = entrypoints

    @property
    def queries(self):
        queries = {}
        if not self.entrypoints:
            queries["new"] = self.parent.compare_db.rewrite_query(
                "select id, label, parent_child_order,tagselector, merge, _order, is_abstract, "
                "aspect_type, include_unreported_value, value from aspect_node"
            )
            queries["compare"] = self.parent.checkpoint_db.rewrite_query(
                "select distinct(id), label, parent_child_order, tagselector, merge, _order, is_abstract, "
                "aspect_type, include_unreported_value, value from aspect_node "
                "where id = %s "
            )
            queries["backref"] = self.parent.compare_db.rewrite_query(
                "select distinct(c.id), dts_name, c.ns_prefix, c.name from aspect_node "
                "left join concept c on concat(ns_prefix, ':', name) = value "
                "left join dts_concept dc on dc.concept_id = c.id and directly_referenced "
                "where aspect_node.id = %s"
            )
        else:
            queries["new"] = self.parent.compare_db.rewrite_query(
                "select an.id, label, parent_child_order,tagselector, merge, _order, is_abstract, "
                "aspect_type, include_unreported_value, value from aspect_node an "
                "left join concept c on concat(ns_prefix, ':', name )= value "
                "left join dts_concept dc on c.id = dc.concept_id "
                "where dts_name = %s and directly_referenced"
            )
            queries["compare"] = self.parent.checkpoint_db.rewrite_query(
                "select distinct(an.id), label, parent_child_order, tagselector, merge, _order, is_abstract, "
                "aspect_type, include_unreported_value, value from aspect_node an "
                "left join concept c on concat(ns_prefix, ':', name )= value "
                "left join dts_concept dc on c.id = dc.concept_id "
                "where an.id = %s and dts_name = %s "
            )
            queries["backref"] = self.parent.compare_db.rewrite_query(
                "select distinct(c.id), dts_name, c.ns_prefix, c.name from aspect_node "
                "left join concept c on concat(ns_prefix, ':', name) = value "
                "left join dts_concept dc on dc.concept_id = c.id and directly_referenced "
                "where aspect_node.id = %s and dts_name = %s "
            )

        return queries

    def compare(self):
        if not self.entrypoints:
            new_ans = self.new_objects()
        else:
            new_ans = self.new_objects(args=(self.entrypoints["new_version"],))

        new = []
        for an in new_ans:
            args = (
                (an[0],)
                if not self.entrypoints
                else (an[0], self.entrypoints["old_version"])
            )
            self.parent.checkpoint_cursor.execute(self.queries["compare"], args)
            matches = self.parent.checkpoint_cursor.fetchall()
            if not len(matches) and an not in new:
                new.append(an)
        block = {
            "title": "Aspectnodes",
            "summary": f"Total Aspectnodes: {len(new_ans)}.  New aspectnodes: {len(new)}.",
            "detail": [],
        }
        if not self.entrypoints:
            block["detail"].append(
                f"<p>Details linken naar een willekeurig entrypoint waarin dit item voorkomt. "
                f"De Aspect Node kan theoretisch in elk entrypoint aangeroepen worden."
                f"We linken naar het concept.</p><ul>"
            )
        for an in sorted(new):
            args = (
                (an[0],)
                if not self.entrypoints
                else (an[0], self.entrypoints["new_version"])
            )
            self.parent.compare_cursor.execute(self.queries["backref"], args)
            dts_name = self.parent.compare_cursor.fetchone()
            block["detail"].append(
                f"<li>"
                f"<a href='{self.webserver}/dts/{self.parent.new_version_db}/"
                f"{dts_name[1]}/concepts/concept/{dts_name[0]}'>{dts_name[2]}:{dts_name[3]}</a> "
                f"<br/><em class='smaller'>{an[0].replace('_', ' ')}</em>"
                f"</li>"
            )
        block["detail"].append("</ul>")
        self.parent.report.html.append(block)
