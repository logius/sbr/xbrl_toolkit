from discover_xbrl.conf.conf import Config
from os.path import join, dirname


class Mutation:
    def __init__(self, db, run_id, object_type, object_id, mutation_type, old_version):
        self.db = db
        self.run_id = run_id
        self.object_type = object_type
        self.object_id = object_id
        self.mutation_type = mutation_type
        self.old_version = old_version
        self.cursor = db.connection.cursor()

    @property
    def object_seen(self):
        query = self.db.rewrite_query(
            "select run_id, object_type, object_id, mutation_type, old_version from mutation "
            "where object_type = %s and object_id = %s and mutation_type = %s"
        )
        self.cursor.execute(
            query, (self.object_type, self.object_id, self.mutation_type)
        )
        rows = self.cursor.fetchall()
        if not len(rows):  # never seen
            query = self.db.rewrite_query(
                "insert into mutation (run_id, object_type, object_id, mutation_type, old_version) "
                "values (%s, %s, %s, %s, %s)"
            )
            self.cursor.execute(
                query,
                (
                    self.run_id,
                    self.object_type,
                    self.object_id,
                    self.mutation_type,
                    self.old_version,
                ),
            )
            self.db.connection.commit()
            return False
        elif rows[0][0] == self.run_id:  # seen, but in the current run
            return False
        else:  # this is noot a new change
            return True


class Mutations:
    def __init__(self, connection, run_id=None):
        self.connection = connection
        self.run_id = run_id
        self.cursor = connection.cursor()
        self.config = Config()
        # self.ensure_table()

    def ensure_table(self):
        # deprecated
        try:
            with open(
                join(dirname(__file__), f"../{self.config.db['type']}/mutation.sql")
            ) as sqlfile:
                if self.config.db["type"] == "sqlite":
                    self.cursor.executescript(sqlfile.read())
                else:
                    self.cursor.execute(sqlfile.read())
                self.connection.commit()
        except (FileNotFoundError, NotADirectoryError):
            print(
                f"could not find "
                + join(dirname(__file__), f"../{self.config.db['type']}/mutation.sql")
            )
