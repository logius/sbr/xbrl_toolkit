from discover_xbrl.sbrnl_modules.db.version_compare.compare_mixin import CompareMixin


class Linkroles(CompareMixin):
    def __init__(self, parent, entrypoints=None, domein=None):
        self.parent = parent
        self.entrypoints = entrypoints
        self.domein = domein

    @property
    def queries(self):
        queries = {}
        if self.domein:
            queries["new"] = self.parent.compare_db.rewrite_query(
                "select distinct(role_uri), definition, id from linkrole "
                "where dts_name in %s"
            )
            queries["compare"] = self.parent.compare_db.rewrite_query(
                "select role_uri, definition from linkrole where role_uri = %s "
                "and coalesce(definition, '') = coalesce(%s, '') "
                "and dts_name in %s "
            )
        elif not self.entrypoints:
            queries["new"] = self.parent.compare_db.rewrite_query(
                "select distinct(role_uri), definition, id from linkrole "
            )
            queries["compare"] = self.parent.compare_db.rewrite_query(
                "select role_uri, definition from linkrole where role_uri = %s "
                "and coalesce(definition, '') = coalesce(%s, '') "
            )
        else:
            queries["new"] = self.parent.compare_db.rewrite_query(
                "select distinct(role_uri), definition, id from linkrole "
                "where dts_name = %s"
            )
            queries["compare"] = self.parent.compare_db.rewrite_query(
                "select role_uri, definition from linkrole where role_uri = %s "
                "and coalesce(definition, '') = coalesce(%s, '') "
                "and dts_name = %s "
            )
        queries["backref"] = self.parent.compare_db.rewrite_query(
            "select dts_name from linkrole where role_uri = %s and directly_referenced"
        )
        return queries

    def compare(self):
        if self.domein:
            new_linkroles = self.new_objects(
                (self.parent.new_domeinen.get_domein(self.domein),)
            )
        elif not self.entrypoints:
            new_linkroles = self.new_objects()
        else:
            new_linkroles = self.new_objects((self.entrypoints["new_version"],))
        new = []
        for lr in new_linkroles:
            if self.domein:
                args = (lr[0], lr[1], self.parent.old_domeinen.get_domein(self.domein))
            elif not self.entrypoints:
                args = (lr[0], lr[1])
            else:
                args = (lr[0], lr[1], self.entrypoints["old_version"])

            self.parent.checkpoint_cursor.execute(self.queries["compare"], args)
            matches = self.parent.checkpoint_cursor.fetchall()
            if not len(matches):
                if self.domein and self.object_seen(
                    object_type="linkrole",
                    object_id=f"lr[0]-lr[1]",
                    mutation_type="new",
                ):
                    continue
                new.append(lr)

        block = {
            "title": "Linkroles",
            "summary": f"Total Linkroles: {len(new_linkroles)}.  New linkroles: {len(new)}.",
            "detail": ["Nieuwe Linkroles en voorkomens in entrypoints"],
        }
        block["detail"].append("<ul>")
        for lr in sorted(new):
            self.parent.compare_cursor.execute(self.queries["backref"], (lr[0],))
            dts_names = self.parent.compare_cursor.fetchall()
            block["detail"].append(
                f"<li id='{lr[0]}'><em class='smaller'>{lr[0]}</em><br/>{lr[1]}"
            )
            if dts_names:
                block["detail"].append("<ul>")
                for dts_name in dts_names:
                    block["detail"].append(
                        f"<li><a href='{self.webserver}/dts/{self.parent.new_version_db}/{dts_name[0]}"
                        f"/linkroles/linkroles/{lr[2]}'>"
                        f"{dts_name[0]}</a></li>"
                    )
                block["detail"].append("</ul>")
            block["detail"].append("</li>")
        block["detail"].append("</ul>")
        self.parent.report.html.append(block)
