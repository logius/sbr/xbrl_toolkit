from collections import defaultdict
from discover_xbrl.sbrnl_modules.db.version_compare.compare_mixin import CompareMixin


class References(CompareMixin):
    def __init__(self, parent, entrypoints=None, domein=None):
        self.parent = parent
        self.entrypoints = entrypoints
        self.domein = domein

    @property
    def queries(self):
        queries = {}
        if self.domein:
            queries["new"] = self.parent.compare_db.rewrite_query(
                "select distinct id, label, name, number, issuedate, chapter, article, note, section, subsection, "
                "publisher, paragraph, subparagraph, clause, subclause, appendix, example, page, exhibit, footnote, "
                "sentence, uri, uridate from reference "
                "left join concept_reference cr on reference.id = cr.reference_id "
                "left join dts_concept dc on cr.concept_id = dc.concept_id "
                "where dts_name in %s  "
            )
            queries["compare"] = self.parent.checkpoint_db.rewrite_query(
                "select id, label, name, number, issuedate, chapter, article, note, section, subsection, publisher, "
                "paragraph, subparagraph, clause, subclause, appendix, example, page, exhibit, footnote, sentence, "
                "uri, uridate from reference "
                "left join concept_reference cr on reference.id = cr.reference_id "
                "left join dts_concept dc on cr.concept_id = dc.concept_id "
                "where id = %s and dts_name in %s "
            )

        elif not self.entrypoints:
            queries["new"] = self.parent.compare_db.rewrite_query(
                "select distinct id, label, name, number, issuedate, chapter, article, note, section, subsection, "
                "publisher, paragraph, subparagraph, clause, subclause, appendix, example, page, exhibit, footnote, "
                "sentence, uri, uridate from reference "
            )
            queries["compare"] = self.parent.checkpoint_db.rewrite_query(
                "select id, label, name, number, issuedate, chapter, article, note, section, subsection, publisher, "
                "paragraph, subparagraph, clause, subclause, appendix, example, page, exhibit, footnote, sentence, "
                "uri, uridate from reference "
                "where id = %s"
            )
        else:
            queries["new"] = self.parent.compare_db.rewrite_query(
                "select distinct id, label, name, number, issuedate, chapter, article, note, section, subsection, publisher, "
                "paragraph, subparagraph, clause, subclause, appendix, example, page, exhibit, footnote, sentence, "
                "uri, uridate from reference "
                "left join concept_reference cr on reference.id = cr.reference_id "
                "left join dts_concept dc on cr.concept_id = dc.concept_id "
                "where dts_name = %s  "
            )
            queries["compare"] = self.parent.checkpoint_db.rewrite_query(
                "select id, label, name, number, issuedate, chapter, article, note, section, subsection, publisher, "
                "paragraph, subparagraph, clause, subclause, appendix, example, page, exhibit, footnote, sentence, "
                "uri, uridate from reference "
                "left join concept_reference cr on reference.id = cr.reference_id "
                "left join dts_concept dc on cr.concept_id = dc.concept_id "
                "where id = %s and dts_name = %s "
            )
        return queries

    def compare(self):
        if self.domein:
            new_references = self.new_objects(
                args=(self.parent.new_domeinen.get_domein(self.domein),)
            )
            old_references = self.old_objects(
                args=(self.parent.old_domeinen.get_domein(self.domein),)
            )
        elif not self.entrypoints:
            new_references = self.new_objects()
            old_references = self.old_objects()
        else:
            new_references = self.new_objects(args=(self.entrypoints["new_version"],))
            old_references = self.old_objects(args=(self.entrypoints["old_version"],))
        new = []
        for ref in new_references:
            if self.domein:
                args = (ref[0], self.parent.old_domeinen.get_domein(self.domein))
                alt_args = (
                    ref[0].replace("2022", "2021"),
                    self.parent.old_domeinen.get_domein(self.domein),
                )
            elif not self.entrypoints:
                args = (ref[0],)
                alt_args = (ref[0].replace("2022", "2021"),)
            else:
                args = (ref[0], self.entrypoints["old_version"])
                alt_args = (
                    ref[0].replace("2022", "2021"),
                    self.entrypoints["old_version"],
                )

            self.parent.checkpoint_cursor.execute(self.queries["compare"], args)
            matches = self.parent.checkpoint_cursor.fetchall()
            if not len(matches):  # second chance
                self.parent.checkpoint_cursor.execute(self.queries["compare"], alt_args)
                matches = self.parent.checkpoint_cursor.fetchall()
            if not len(matches) and ref not in new:
                if self.domein and self.object_seen(
                    object_type="reference", object_id=ref[0], mutation_type="new"
                ):
                    continue
                new.append(ref)
        names = defaultdict(int)
        for ref in new:
            names[ref[2]] += 1
        block = {
            "title": "References",
            "summary": f"Total References: {len(new_references)}.  New referencess: {len(new)}.",
            "detail": [],
        }
        block["detail"].append(
            "<p>References split by the 'name'-field. (Which usually represents a source)</p>"
        )
        for name, count in names.items():
            block["detail"].append(f"<li>{name}: {count}</li>")
        block["detail"].append("</ul>")
        self.parent.report.html.append(block)

        deleted = []
        for ref in old_references:
            if self.domein:
                args = (ref[0], self.parent.new_domeinen.get_domein(self.domein))
                alt_args = (
                    ref[0].replace("2021", "2022"),
                    self.parent.new_domeinen.get_domein(self.domein),
                )
            elif not self.entrypoints:
                args = (ref[0],)
                alt_args = (ref[0].replace("2021", "2022"),)
            else:
                args = (ref[0], self.entrypoints["new_version"])
                alt_args = (
                    ref[0].replace("2021", "2022"),
                    self.entrypoints["new_version"],
                )

            self.parent.compare_cursor.execute(self.queries["compare"], args)
            matches = self.parent.compare_cursor.fetchall()
            if not len(matches):  # second chance
                self.parent.compare_cursor.execute(self.queries["compare"], alt_args)
                matches = self.parent.compare_cursor.fetchall()
            if not len(matches) and ref not in deleted:
                if self.domein:
                    if self.object_seen(
                        object_type="reference",
                        object_id=ref[0],
                        mutation_type="delete",
                    ):
                        continue
                deleted.append(ref)

        names = defaultdict(int)
        for ref in deleted:
            names[ref[2]] += 1
        block = {
            "title": "Deleted references",
            "summary": f"Total References: {len(old_references)}.  Deleted referencess: {len(deleted)}.",
            "detail": [],
        }
        block["detail"].append(
            "<p>References split by the 'name'-field. (Which usually represents a source)</p>"
        )
        for name, count in names.items():
            block["detail"].append(f"<li>{name}: {count}</li>")
        block["detail"].append("</ul>")
        self.parent.report.html.append(block)
