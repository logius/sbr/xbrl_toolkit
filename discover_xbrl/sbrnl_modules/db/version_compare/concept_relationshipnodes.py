from discover_xbrl.sbrnl_modules.db.version_compare.compare_mixin import CompareMixin


class ConceptRelationshipNodes(CompareMixin):
    def __init__(self, parent, entrypoints=None):
        self.parent = parent
        self.entrypoints = entrypoints

    @property
    def queries(self):
        queries = {}
        if not self.entrypoints:
            queries["new"] = self.parent.compare_db.rewrite_query(
                "select id, label, parent_child_order, parent, tagselector, _order, relationship_source, "
                "linkrole_role_uri, formula_axis, generations, arcrole, linkname, arcname "
                "from concept_relationshipnode "
            )
            queries["compare"] = self.parent.checkpoint_db.rewrite_query(
                "select id, label, parent_child_order, parent, tagselector, _order, relationship_source, "
                "linkrole_role_uri, formula_axis, generations, arcrole, linkname, arcname "
                "from concept_relationshipnode "
                "where id = %s"
            )
            queries["backref"] = self.parent.compare_db.rewrite_query(
                "select dts_name, c.id, c.ns_prefix, c.name from concept_relationshipnode crn "
                "left join concept c on concat(ns_prefix, ':', name) = crn.relationship_source "
                "left join dts_concept dc on c.id = dc.concept_id and directly_referenced "
                "where crn.id = %s"
            )
        else:
            queries["new"] = self.parent.compare_db.rewrite_query(
                "select crn.id, label, parent_child_order, parent, tagselector, _order, relationship_source, "
                "linkrole_role_uri, formula_axis, generations, arcrole, linkname, arcname "
                "from concept_relationshipnode crn "
                "left join concept c "
                "  on crn.relationship_source = concat(c.ns_prefix, ':', c.name) "
                "left join dts_concept dc on c.id = dc.concept_id "
                "where dts_name = %s and directly_referenced"
            )
            queries["compare"] = self.parent.checkpoint_db.rewrite_query(
                "select crn.id, label, parent_child_order, parent, tagselector, _order, relationship_source, "
                "linkrole_role_uri, formula_axis, generations, arcrole, linkname, arcname "
                "from concept_relationshipnode crn "
                "left join concept c "
                "  on crn.relationship_source = concat(c.ns_prefix, ':', c.name) "
                "left join dts_concept dc on c.id = dc.concept_id "
                "where crn.id = %s "
                "  and dts_name = %s"
            )
            queries["backref"] = self.parent.compare_db.rewrite_query(
                "select dts_name, c.id, c.ns_prefix, c.name from concept_relationshipnode crn "
                "left join concept c on concat(ns_prefix, ':', name) = crn.relationship_source "
                "left join dts_concept dc on c.id = dc.concept_id and directly_referenced "
                "where crn.id = %s and dts_name = %s"
            )

        return queries

    def compare(self):
        if not self.entrypoints:
            new_crns = self.new_objects()
        else:
            new_crns = self.new_objects(args=(self.entrypoints["new_version"],))

        new = []
        for crn in new_crns:
            args = (
                (crn[0],)
                if not self.entrypoints
                else (crn[0], self.entrypoints["old_version"])
            )
            self.parent.checkpoint_cursor.execute(self.queries["compare"], args)
            matches = self.parent.checkpoint_cursor.fetchall()
            if not len(matches) and crn not in new:
                new.append(crn)
        block = {
            "title": "Concept Relationship Nodes",
            "summary": f"Total Concept Relationship Nodes: {len(new_crns)}. "
            f"New concept relationship nodes: {len(new)}.",
            "detail": [],
        }
        if not self.entrypoints:
            block["detail"].append(
                f"<p>Details linken naar een willekeurig entrypoint waarin dit item voorkomt. "
                f"De Concept Relationship Node kan theoretisch in elk entrypoint aangeroepen worden."
                f"We linken naar het concept.</p><ul>"
            )

        for crn in sorted(new):
            args = (
                (crn[0],)
                if not self.entrypoints
                else (crn[0], self.entrypoints["new_version"])
            )
            self.parent.compare_cursor.execute(self.queries["backref"], args)
            dts_name = self.parent.compare_cursor.fetchone()
            if dts_name:
                block["detail"].append(
                    f"<li>"
                    f"<a href='{self.webserver}/dts/{self.parent.new_version_db}/"
                    f"{dts_name[0]}/concepts/concept/{dts_name[1]}'>{dts_name[2]}:{dts_name[3]}</a> "
                    f"<br/><em class='smaller'>{crn[0].replace('_', ' ')}</em>"
                    f"</li>"
                )
            elif self.entrypoints:
                block["detail"].append(
                    f"<li>"
                    f"<a href='{self.webserver}/dts/{self.parent.new_version_db}/"
                    f"{self.entrypoints['new_version']}/tables/'>"
                    f"<em class='smaller'>{crn[0].replace('_', ' ')}</em></a>"
                    f"</li>"
                )
            else:
                block["detail"].append(
                    f"<li>"
                    f"<em class='smaller'>{crn[0].replace('_', ' ')}</em></a>"
                    f"</li>"
                )

        block["detail"].append("</ul>")

        self.parent.report.html.append(block)
