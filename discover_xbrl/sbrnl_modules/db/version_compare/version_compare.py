import argparse
import pathlib
from collections import defaultdict
from os.path import join
from os import scandir

from jinja2 import Environment, FileSystemLoader, select_autoescape

from discover_xbrl.conf.conf import Config
from discover_xbrl.sbrnl_modules.db.version_compare.mutation import Mutations

from discover_xbrl.sbrnl_modules.db.collections.helpers import get_dts_nice_name
from discover_xbrl.sbrnl_modules.db.connection import get_connection

from discover_xbrl.sbrnl_modules.db.version_compare.assertions import Assertions
from discover_xbrl.sbrnl_modules.db.version_compare.concepts import Concepts
from discover_xbrl.sbrnl_modules.db.version_compare.dimensions import Dimensions
from discover_xbrl.sbrnl_modules.db.version_compare.domeinen import Domeinen
from discover_xbrl.sbrnl_modules.db.version_compare.formula_explicit_dimensions import (
    FormulaExplicitDimensions,
)
from discover_xbrl.sbrnl_modules.db.version_compare.formula_typed_dimensions import (
    FormulaTypedDimensions,
)
from discover_xbrl.sbrnl_modules.db.version_compare.formula_period import FormulaPeriod
from discover_xbrl.sbrnl_modules.db.version_compare.itemtypes import ItemTypes
from discover_xbrl.sbrnl_modules.db.version_compare.labels import Labels
from discover_xbrl.sbrnl_modules.db.version_compare.linkroles import Linkroles
from discover_xbrl.sbrnl_modules.db.version_compare.presentations import (
    PresentationHierarchyNodes,
)
from discover_xbrl.sbrnl_modules.db.version_compare.references import References
from discover_xbrl.sbrnl_modules.db.version_compare.tables import Tables


class VersionCompareReport:
    def __init__(self, old_db, new_db):
        self.old_db = old_db
        self.new_db = new_db
        self.lines = []
        self.html = []
        self.conf = Config()
        self.env = Environment(
            loader=FileSystemLoader(f"{self.conf.project_root}/templates"),
            autoescape=select_autoescape(["html", "xml"]),
        )
        self.report_name = (
            f"Version comparison between "
            f"{get_dts_nice_name(new_db)} and {get_dts_nice_name(old_db)}"
        )

    @property
    def header(self):
        return f"Comparing: {self.old_db} with {self.new_db}"

    def write(self, line):
        self.lines.append(line)

    def to_screen(self):
        print(self.header)
        for line in self.lines:
            print(line)

    def to_html(self):
        template = self.env.get_template("version_compare.html")
        return template.render(report=self)


class VersionCompare:
    def __init__(self, run_id=None, old_db=None, new_db=None):
        """ Compares to NTA versions (databases) with each other.
        The report will contain all newly defined objects in the taxonomy.
        args:
            @run_id: str (SBR, KVK, BZK, BD, OCW)
            @old_db: str
            @new_db: str
        The older db will be used as 'checkpoint' database,
        the newer will be called 'compare'

        If you want to change the direction (and remember that 'new' concepts
        are then in fact deleted concepts (available in old, gone in new) """

        self.run_id = run_id
        self.old_version_db = old_db
        self.new_version_db = new_db
        self.checkpoint_db = get_connection(nt_version=self.old_version_db)
        self.checkpoint_cursor = self.checkpoint_db.connection.cursor()
        self.compare_db = get_connection(nt_version=self.new_version_db)
        self.compare_cursor = self.compare_db.connection.cursor()
        self.dts_compare_list = []
        self.old_not_in_new = []
        self.new_not_in_old = []
        self.report = VersionCompareReport(old_db=old_db, new_db=new_db)
        self.old_domeinen = Domeinen(parent=self, version="old")
        self.new_domeinen = Domeinen(parent=self, version="new")
        self.domeinen = ["belastingdienst", "bzk", "kvk", "ocw", "sbr"]
        self.mutations = Mutations(connection=self.compare_db.connection)

    def create_report(self):
        self.run()
        self.write_report()

    def create_ep_reports(self):
        self.comparable_entrypoints()

    def create_domeinen_reports(self):
        self.per_domein()

    def write_report(self, entrypoints=None, domein=None):
        html_report = self.report.to_html()
        conf = Config()
        filename = conf.admin.get("version_reports_dir")
        if filename:
            if domein:
                pathlib.Path(
                    join(filename, f"{self.old_version_db}-{self.new_version_db}")
                ).mkdir(exist_ok=True)
                filename = join(
                    filename,
                    f"{self.old_version_db}-{self.new_version_db}",
                    f"{domein.upper()}.html",
                )
            elif not entrypoints:
                filename = join(
                    filename,
                    f"{self.old_version_db.upper()}-{self.new_version_db.upper()}.html",
                )
            else:
                pathlib.Path(
                    join(filename, f"{self.old_version_db}-{self.new_version_db}")
                ).mkdir(exist_ok=True)
                filename = join(
                    filename,
                    f"{self.old_version_db}-{self.new_version_db}",
                    f"{entrypoints['new_version'][:-4]}.html",
                )
            with open(filename, "w") as outfile:
                outfile.write(html_report)

    def run(self, entrypoints=None, domein=None):
        cc = Concepts(parent=self, entrypoints=entrypoints, domein=domein)
        cc.compare()
        rf = References(parent=self, entrypoints=entrypoints, domein=domein)
        rf.compare()
        lb = Labels(parent=self, entrypoints=entrypoints, domein=domein)
        lb.compare()
        lr = Linkroles(parent=self, entrypoints=entrypoints, domein=domein)
        lr.compare()
        it = ItemTypes(parent=self, entrypoints=entrypoints, domein=domein)
        it.compare()
        dm = Dimensions(parent=self, entrypoints=entrypoints, domein=domein)
        dm.compare()
        tb = Tables(parent=self, entrypoints=entrypoints, domein=domein)
        tb.compare()
        pn = PresentationHierarchyNodes(
            parent=self, entrypoints=entrypoints, domein=domein
        )
        pn.compare()
        asns = Assertions(parent=self, entrypoints=entrypoints, domein=domein)
        asns.compare()

        # if not domein and not entrypoints:
        #     fed = FormulaExplicitDimensions(parent=self, entrypoints=entrypoints)
        #     fed.compare()
        #     fp = FormulaPeriod(parent=self)
        #     fp.compare()
        #     ftd = FormulaTypedDimensions(parent=self)
        #     ftd.compare()

    @property
    def available_reports(self):
        conf = Config()
        dirname = conf.admin.get("version_reports_dir")
        servername = conf.api.get("webserver", "https://kc-xbrl.cooljapan.nl")
        if dirname:
            subdirs = [f.name for f in scandir(dirname) if f.is_dir()]
            reports = list(pathlib.Path(dirname).glob("*.html"))
            reports.sort(key=lambda x: str(x).replace(".", "z"), reverse=True)
            response = {"reports": []}
            description = ""
            for report in reports:
                filename = report.parts[-1]
                versions = filename.split(".")[0].split("-")
                if len(versions) > 1:
                    description = f"New in {versions[1]} since {versions[0]}"
                response["reports"].append(
                    {
                        "filename": filename,
                        "url": f"{servername}/nta/version-compare/{filename}",
                        "description": description,
                        "children": [],
                    }
                )
                if len(versions) and len(subdirs):
                    for subdir in subdirs:
                        if subdir.upper() == f"{versions[0]}-{versions[1]}":
                            eps = list(
                                pathlib.Path(join(dirname, subdir)).glob("*.html")
                            )
                            for ep in sorted(eps):
                                response["reports"][-1]["children"].append(
                                    {
                                        "filename": ep.parts[-1],
                                        "url": f"{servername}/nta/version-compare/{subdir}/{ep.name}",
                                        "description": "",
                                    }
                                )

            return response
        return None

    def per_domein(self):
        for domein in self.domeinen:
            check = "BD" if domein == "belastingdienst" else domein.upper()
            if not self.run_id == check:
                continue
            if not len(self.new_domeinen.get_domein(domein)):
                continue
            if not len(self.old_domeinen.get_domein(domein)):
                continue
            print(domein)
            self.report = VersionCompareReport(
                old_db=self.old_version_db,
                new_db=f"{self.new_version_db} - {domein.upper()}",
            )
            self.run(entrypoints=None, domein=domein)

            self.write_report(domein=domein)

    def comparable_entrypoints(self):
        domein = self.run_id.lower() if not self.run_id == "BD" else "belastingdienst"
        checkpoint_names = self.old_domeinen.get_domein(domein)
        compare_names = self.new_domeinen.get_domein(domein)
        for dts in checkpoint_names:
            if dts not in compare_names:
                new_name = (
                    dts.replace("2024", "2025")
                    .replace("2023", "2024")
                    .replace("2022", "2023")
                    .replace("2021", "2022")
                    .replace("2020", "2021")
                )
            else:
                new_name = dts
            if new_name in compare_names:
                self.dts_compare_list = [dts, new_name]
                print(
                    f"Comparing: {self.dts_compare_list[0]} with {self.dts_compare_list[1]}"
                )
                entrypoints = {"new_version": new_name, "old_version": dts}
                self.report = VersionCompareReport(old_db=dts, new_db=new_name)
                self.run(entrypoints=entrypoints, domein=None)

                self.write_report(entrypoints=entrypoints)


def main():
    vc = VersionCompare(run_id="KVK", old_db="nt17", new_db="nt18")
    vc.create_report()
    vc.create_domeinen_reports()
    vc.create_ep_reports()


if __name__ == "__main__":
    main()
