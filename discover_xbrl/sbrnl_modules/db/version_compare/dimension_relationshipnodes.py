from discover_xbrl.sbrnl_modules.db.version_compare.compare_mixin import CompareMixin


class DimensionRelationshipNodes(CompareMixin):
    def __init__(self, parent, entrypoints=None):
        self.parent = parent
        self.entrypoints = entrypoints

    @property
    def queries(self):
        queries = {}
        if not self.entrypoints:
            queries["new"] = self.parent.compare_db.rewrite_query(
                "select id, label, parent_child_order, parent, _order, dimension_id, linkrole_role_uri, "
                "formula_axis, generations, relationship_source, tagselector "
                "from dimension_relationshipnode "
            )
            queries["compare"] = self.parent.checkpoint_db.rewrite_query(
                "select id, label, parent_child_order, parent, _order, dimension_id, linkrole_role_uri, "
                "formula_axis, generations, relationship_source, tagselector "
                "from dimension_relationshipnode "
                "where id = %s"
            )
            queries["backref"] = self.parent.compare_db.rewrite_query(
                "select dts_name, c.id, c.ns_prefix, c.name from dimension_relationshipnode drn "
                "left join concept c on concat(ns_prefix, ':', name) = drn.dimension_id "
                "left join dts_concept dc on c.id = dc.concept_id and directly_referenced "
                "where drn.id = %s"
            )
        else:
            queries["new"] = self.parent.compare_db.rewrite_query(
                "select drn.id, label, parent_child_order, parent, _order, dimension_id, linkrole_role_uri, "
                "formula_axis, generations, relationship_source, tagselector "
                "from dimension_relationshipnode drn "
                "left join concept c "
                "  on drn.dimension_id = concat(c.ns_prefix, ':', c.name) "
                "left join dts_concept dc on c.id = dc.concept_id "
                "where dts_name = %s and directly_referenced "
            )
            queries["compare"] = self.parent.checkpoint_db.rewrite_query(
                "select drn.id, label, parent_child_order, parent, _order, dimension_id, linkrole_role_uri, "
                "formula_axis, generations, relationship_source, tagselector "
                "from dimension_relationshipnode drn "
                "left join concept c "
                "  on drn.dimension_id = concat(c.ns_prefix, ':', c.name) "
                "left join dts_concept dc on c.id = dc.concept_id "
                "where drn.id = %s and dts_name = %s"
            )
            queries["backref"] = self.parent.compare_db.rewrite_query(
                "select dts_name, c.id, c.ns_prefix, c.name from dimension_relationshipnode drn "
                "left join concept c on concat(ns_prefix, ':', name) = drn.dimension_id "
                "left join dts_concept dc on c.id = dc.concept_id and directly_referenced "
                "where drn.id = %s and dts_name = %s"
            )

        return queries

    def compare(self):
        if not self.entrypoints:
            new_drns = self.new_objects()
        else:
            new_drns = self.new_objects(args=(self.entrypoints["new_version"],))

        new = []
        for drn in new_drns:
            args = (
                (drn[0],)
                if not self.entrypoints
                else (drn[0], self.entrypoints["old_version"])
            )
            self.parent.checkpoint_cursor.execute(self.queries["compare"], args)
            matches = self.parent.checkpoint_cursor.fetchall()
            if not len(matches) and drn not in new:
                new.append(drn)

        block = {
            "title": "Dimension Relationship Nodes",
            "summary": f"Total Dimension Relationship Nodes: {len(new_drns)}. "
            f"New dimension relationship nodes: {len(new)}.",
            "detail": [],
        }
        if not self.entrypoints:
            block["detail"].append(
                f"<p>Details linken naar een willekeurig entrypoint waarin dit item voorkomt. "
                f"De Dimension Relationship Node kan theoretisch in elk entrypoint aangeroepen worden."
                f"We linken naar het concept.</p><ul>"
            )

        for drn in sorted(new):
            args = (
                (drn[0],)
                if not self.entrypoints
                else (drn[0], self.entrypoints["new_version"])
            )
            self.parent.compare_cursor.execute(self.queries["backref"], args)
            dts_name = self.parent.compare_cursor.fetchone()
            block["detail"].append(
                f"<li>"
                f"<a href='{self.webserver}/dts/{self.parent.new_version_db}/"
                f"{dts_name[0]}/concepts/concept/{dts_name[1]}'>{dts_name[2]}:{dts_name[3]}</a> "
                f"<br/><em class='smaller'>{drn[0].replace('_', ' ')}</em>"
                f"</li>"
            )

        block["detail"].append("</ul>")

        self.parent.report.html.append(block)
