from discover_xbrl.conf.conf import Config
from discover_xbrl.sbrnl_modules.db.version_compare.mutation import Mutation

conf = Config()


class CompareMixin:
    @property
    def webserver(self):
        return conf.api.get("webserver", "https://kc-xbrl.cooljapan.nl")

    def new_objects(self, args=None):
        if args:
            self.parent.compare_cursor.execute(self.queries["new"], args)
        else:
            self.parent.compare_cursor.execute(self.queries["new"])

        return self.parent.compare_cursor.fetchall()

    def old_objects(self, args=None):
        if args:
            self.parent.checkpoint_cursor.execute(self.queries["new"], args)
        else:
            self.parent.checkpoint_cursor.execute(self.queries["new"])
        return self.parent.checkpoint_cursor.fetchall()

    def object_seen(self, object_type=None, object_id=None, mutation_type=None):
        mutation = Mutation(
            self.parent.compare_db,
            run_id=self.parent.run_id,
            object_type=object_type,
            object_id=object_id,
            mutation_type=mutation_type,
            old_version=self.parent.old_version_db,
        )
        return mutation.object_seen
