from discover_xbrl.sbrnl_modules.db.version_compare.compare_mixin import CompareMixin


class PresentationHierarchyNodes(CompareMixin):
    def __init__(self, parent, entrypoints=None, domein=None):
        self.parent = parent
        self.entrypoints = entrypoints
        self.domein = domein

    @property
    def queries(self):
        queries = {}
        if self.domein:
            queries["new"] = self.parent.compare_db.rewrite_query(
                "select distinct phn.id, node_type, phn.concept_id, linkrole_role_uri "
                "from presentation_hierarchy_node phn "
                "left join dts_concept dc on dc.concept_id = phn.concept_id "
                "where dc.dts_name in %s  and dc.directly_referenced"
            )
            queries["compare"] = self.parent.checkpoint_db.rewrite_query(
                "select phn.id, node_type, phn.concept_id, linkrole_role_uri from "
                "presentation_hierarchy_node phn "
                "left join dts_concept dc on dc.concept_id = phn.concept_id "
                "where phn.id = %s and dc.dts_name in %s  "
            )
            queries["count"] = self.parent.compare_db.rewrite_query(
                "select count(distinct(phn.id)) from presentation_hierarchy_node phn "
                "left join dts_concept dc on dc.concept_id = phn.concept_id "
                "where dc.dts_name in %s and dc.directly_referenced "
            )
            queries["backref"] = self.parent.compare_db.rewrite_query(
                "select id, dts_name from linkrole where role_uri = %s "
                "and dts_name = %s and directly_referenced = true"
            )
            queries["concept_backref"] = self.parent.compare_db.rewrite_query(
                "select dts_name from dts_concept where concept_id = %s "
                "and dts_name in %s  "
            )
        elif not self.entrypoints:
            queries["new"] = self.parent.compare_db.rewrite_query(
                "select distinct id, node_type, phn.concept_id, linkrole_role_uri, is_root "
                "from presentation_hierarchy_node phn "
                "left join dts_concept dc on dc.concept_id = phn.concept_id "
                "where dc.directly_referenced "
            )
            queries["compare"] = self.parent.checkpoint_db.rewrite_query(
                "select id, node_type, concept_id, linkrole_role_uri, is_root from presentation_hierarchy_node "
                "where id = %s "
            )
            queries["count"] = self.parent.compare_db.rewrite_query(
                "select count(distinct(id)) from presentation_hierarchy_node phn "
                "left join dts_concept dc on dc.concept_id = phn.concept_id "
                "where dc.directly_referenced = true"
            )
            queries["backref"] = self.parent.compare_db.rewrite_query(
                "select id, dts_name from linkrole where role_uri = %s "
                "and directly_referenced = true"
            )
            queries["concept_backref"] = self.parent.compare_db.rewrite_query(
                "select dts_name from dts_concept where concept_id = %s and directly_referenced "
            )
        else:
            queries["new"] = self.parent.compare_db.rewrite_query(
                "select distinct phn.id, node_type, phn.concept_id, linkrole_role_uri "
                "from presentation_hierarchy_node phn "
                "left join dts_concept dc on dc.concept_id = phn.concept_id "
                "where dc.dts_name = %s and dc.directly_referenced "
            )
            queries["compare"] = self.parent.checkpoint_db.rewrite_query(
                "select phn.id, node_type, phn.concept_id, linkrole_role_uri from "
                "presentation_hierarchy_node phn "
                "left join dts_concept dc on dc.concept_id = phn.concept_id "
                "where phn.id = %s and dc.dts_name = %s and dc.directly_referenced "
            )
            queries["count"] = self.parent.compare_db.rewrite_query(
                "select count(distinct(phn.id)) from presentation_hierarchy_node phn "
                "left join dts_concept dc on dc.concept_id = phn.concept_id "
                "where dc.dts_name = %s and dc.directly_referenced "
            )
            queries["backref"] = self.parent.compare_db.rewrite_query(
                "select id, dts_name from linkrole where role_uri = %s "
                "and dts_name = %s and directly_referenced = true"
            )
        return queries

    def compare(self):
        if self.domein:
            new_pns = self.new_objects(
                args=(self.parent.new_domeinen.get_domein(self.domein),)
            )
        elif not self.entrypoints:
            new_pns = self.new_objects()
        else:
            new_pns = self.new_objects(args=(self.entrypoints["new_version"],))

        new = []
        for pn in new_pns:
            if self.domein:
                args = (pn[0], self.parent.old_domeinen.get_domein(self.domein))
            elif not self.entrypoints:
                args = (pn[0],)
            else:
                args = (pn[0], self.entrypoints["old_version"])

            self.parent.checkpoint_cursor.execute(self.queries["compare"], args)
            matches = self.parent.checkpoint_cursor.fetchall()
            if not len(matches) and pn not in new:
                if self.domein and self.object_seen(
                    object_type="presentation", object_id=pn[0], mutation_type="new"
                ):
                    continue
                new.append(pn)

        if self.domein:
            self.parent.compare_cursor.execute(
                self.queries["count"],
                (self.parent.new_domeinen.get_domein(self.domein),),
            )
        elif not self.entrypoints:
            self.parent.compare_cursor.execute(self.queries["count"])
        else:
            self.parent.compare_cursor.execute(
                self.queries["count"], (self.entrypoints["new_version"],)
            )
        total_ids = self.parent.compare_cursor.fetchone()[0]

        block = {
            "title": "Presentationnodes",
            "summary": f"Total Presentation nodes: {total_ids}.  New presentation nodes: {len(new)}.",
            "detail": [],
        }
        block["detail"].append("<ul>")

        for row in sorted(new, key=lambda item: item[2] if item[2] else item[3]):
            if row[1] == "concept":
                if self.entrypoints:
                    block["detail"].append(
                        f"<li><a href='{self.webserver}/dts/"
                        f"{self.parent.new_version_db}/{self.entrypoints['new_version']}"
                        f"/concepts/concept/{row[2]}'>"
                        f"{row[2]}</a></li>"
                    )
                else:
                    args = (
                        (row[2],)
                        if not self.domein
                        else (row[2], self.parent.new_domeinen.get_domein(self.domein))
                    )
                    self.parent.compare_cursor.execute(
                        self.queries["concept_backref"], args
                    )
                    dts_name = self.parent.compare_cursor.fetchone()
                    block["detail"].append(
                        f"<li><a href='{self.webserver}/dts/"
                        f"{self.parent.new_version_db}/{dts_name[0]}"
                        f"/concepts/concept/{row[2]}'>"
                        f"{row[2]}</a></li>"
                    )
            else:
                args = (
                    (row[3],)
                    if not self.entrypoints
                    else (row[3], self.entrypoints["new_version"])
                )
                self.parent.compare_cursor.execute(self.queries["backref"], args)
                lrs = self.parent.compare_cursor.fetchone()
                dts_name = (
                    lrs[1] if not self.entrypoints else self.entrypoints["new_version"]
                )
                block["detail"].append(
                    f"<li><a href='{self.webserver}/dts/"
                    f"{self.parent.new_version_db}/{dts_name}/linkroles/"
                    f"linkroles/{lrs[0]}'>{row[3]}</a> [LR]</li>"
                )
        block["detail"].append("</ul>")
        self.parent.report.html.append(block)
