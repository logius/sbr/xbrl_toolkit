from collections import defaultdict
from discover_xbrl.sbrnl_modules.db.version_compare.compare_mixin import CompareMixin


class Concepts(CompareMixin):
    def __init__(self, parent, entrypoints=None, domein=None):
        self.parent = parent
        self.entrypoints = entrypoints
        self.domein = domein
        self.concepts = []

    @property
    def queries(self):
        queries = {}
        if self.domein:
            queries["new"] = self.parent.compare_db.rewrite_query(
                "select distinct id, name, balance, nillable, abstract, periodtype, ns_prefix, namespace, "
                "substitution_group, typed_domainref, itemtype_name from concept "
                "left join dts_concept dc on concept.id = dc.concept_id "
                "where dts_name in %s "
            )
            queries["compare"] = self.parent.compare_db.rewrite_query(
                "select id, name, balance, nillable, abstract, periodtype, ns_prefix, namespace, substitution_group, "
                "typed_domainref, itemtype_name from concept "
                "left join dts_concept dc on concept.id = dc.concept_id "
                "where name = %s and ns_prefix = %s "
                "and dts_name in %s "
            )  # find by QName
            queries["backref"] = self.parent.compare_db.rewrite_query(
                "select dts_name from dts_concept dc where concept_id = %s "
                "and dts_name in %s "
            )
        elif not self.entrypoints:
            queries["new"] = self.parent.compare_db.rewrite_query(
                "select distinct id, name, balance, nillable, abstract, periodtype, ns_prefix, namespace, "
                "substitution_group, typed_domainref, itemtype_name from concept "
            )  # We leave out the namespace URI, because they WILL differ
            queries["compare"] = self.parent.compare_db.rewrite_query(
                "select id, name, balance, nillable, abstract, periodtype, ns_prefix, namespace, substitution_group, "
                "typed_domainref, itemtype_name from concept "
                "where name = %s and ns_prefix = %s "
            )  # find by QName
            queries["backref"] = self.parent.compare_db.rewrite_query(
                "select dts_name from dts_concept where concept_id = %s "
            )
        else:
            queries["new"] = self.parent.compare_db.rewrite_query(
                "select distinct id, name, balance, nillable, abstract, periodtype, ns_prefix, namespace, "
                "substitution_group, typed_domainref, itemtype_name from concept "
                "left join dts_concept dc on concept.id = dc.concept_id "
                "where dts_name = %s "
            )  # We leave out the namespace URI, because they WILL differ
            queries["compare"] = self.parent.compare_db.rewrite_query(
                "select id, name, balance, nillable, abstract, periodtype, ns_prefix, namespace, substitution_group, "
                "typed_domainref, itemtype_name from concept "
                "left join dts_concept dc on concept.id = dc.concept_id "
                "where name = %s and ns_prefix = %s "
                "and dts_name = %s "
            )
        return queries

    def compare(self):
        if self.domein:
            new_concepts = self.new_objects(
                (self.parent.new_domeinen.get_domein(self.domein),)
            )  # the attribute is a tuple, which represents 1 argument!
            old_concepts = self.old_objects(
                (self.parent.old_domeinen.get_domein(self.domein),)
            )  # the attribute is a tuple, which represents 1 argument!

        elif not self.entrypoints:
            new_concepts = self.new_objects()
            old_concepts = self.old_objects()
        else:
            new_concepts = self.new_objects((self.entrypoints["new_version"],))
            old_concepts = self.old_objects((self.entrypoints["old_version"],))

        new = []
        for c in new_concepts:
            if self.domein:
                args = (c[1], c[6], self.parent.old_domeinen.get_domein(self.domein))
            elif self.entrypoints is None:
                args = (c[1], c[6])
            else:
                args = (c[1], c[6], self.entrypoints["old_version"])
            self.parent.checkpoint_cursor.execute(self.queries["compare"], args)
            matches = self.parent.checkpoint_cursor.fetchall()
            if not len(matches) and c not in new:
                if self.domein and self.object_seen(
                    object_type="concept",
                    object_id=f"{c[6]}:{c[1]}",
                    mutation_type="new",
                ):
                    continue
                new.append(c)

        owner = self.split_by_owner(new, new=True)

        block = {
            "title": "Concepts",
            "summary": f"Total Concepts: {len(new_concepts)}.  New concepts: {len(new)}.",
            "detail": [],
        }

        block["detail"].append("Aantal nieuwe concepten per domein: <ul>")
        for own, count in owner.items():
            block["detail"].append(f"<li>{own}: {count}</li>")
        block["detail"].append("</ul>")
        block["detail"].append("<p>Nieuwe Concepten en voorkomens in entrypoints</p>")
        for dict in self.concepts:
            block["detail"].append(
                f"<h4 id='{dict['concept'][0]}'>{dict['concept'][0]}</h4><ul>"
            )
            for backref in dict["backrefs"]:
                block["detail"].append(
                    f"<li>"
                    f"<a href='{self.webserver}/dts/{self.parent.new_version_db}/"
                    f"{backref[0]}/concepts/concept/{dict['concept'][0]}'>{backref[0]}"
                    f"</a></li>"
                )
            if not dict["backrefs"]:
                block["detail"].append("\ta mysterious unused concept")
            block["detail"].append("</ul>")

        self.parent.report.html.append(block)

        deleted = []
        self.concepts = []
        for c in old_concepts:
            if self.domein:
                args = (c[1], c[6], self.parent.new_domeinen.get_domein(self.domein))
            elif self.entrypoints is None:
                args = (c[1], c[6])
            else:
                args = (c[1], c[6], self.entrypoints["new_version"])
            self.parent.compare_cursor.execute(self.queries["compare"], args)
            matches = self.parent.compare_cursor.fetchall()
            if not len(matches) and c not in deleted:
                if self.domein:
                    if self.object_seen(
                        object_type="concept",
                        object_id=f"{c[6]}:{c[1]}",
                        mutation_type="delete",
                    ):
                        continue
                deleted.append(c)
        owner = self.split_by_owner(concepts=deleted, new=False)
        block = {
            "title": "Deleted concepts",
            "summary": f"Total old Concepts: {len(old_concepts)}.  Deleted concepts: {len(deleted)}.",
            "detail": [],
        }
        block["detail"].append("Aantal verwijderde concepten per domein: <ul>")
        for own, count in owner.items():
            block["detail"].append(f"<li>{own}: {count}</li>")
        block["detail"].append("</ul>")
        for dict in self.concepts:
            block["detail"].append(
                f"<h4 id='{dict['concept'][0]}'>{dict['concept'][0]}</h4><ul>"
            )
            for backref in dict["backrefs"]:
                block["detail"].append(
                    f"<li>"
                    f"<a href='{self.webserver}/dts/{self.parent.old_version_db}/"
                    f"{backref[0]}/concepts/concept/{dict['concept'][0]}'>{backref[0]}"
                    f"</a></li>"
                )
            if not dict["backrefs"]:
                block["detail"].append("\ta mysterious unused concept")
            block["detail"].append("</ul>")

        self.parent.report.html.append(block)

    def split_by_owner(self, concepts=None, new=True):
        owner = defaultdict(int)  # count per 'domain' bd, bzk, kvk, jenv etc.
        for concept in sorted(concepts):
            parts = concept[0].split("_")
            owner[parts[0]] += 1

            if not self.entrypoints:
                if new:
                    args = (
                        (concept[0], self.parent.new_domeinen.get_domein(self.domein))
                        if self.domein
                        else (concept[0],)
                    )
                    self.parent.compare_cursor.execute(self.queries["backref"], args)
                    backrefs = self.parent.compare_cursor.fetchall()
                else:
                    args = (
                        (concept[0], self.parent.old_domeinen.get_domein(self.domein))
                        if self.domein
                        else (concept[0],)
                    )
                    self.parent.checkpoint_cursor.execute(self.queries["backref"], args)
                    backrefs = self.parent.checkpoint_cursor.fetchall()
            else:
                if new:
                    backrefs = [(self.entrypoints["new_version"],)]
                else:
                    backrefs = [(self.entrypoints["old_version"],)]
            self.concepts.append({"concept": concept, "backrefs": backrefs})
        return owner
