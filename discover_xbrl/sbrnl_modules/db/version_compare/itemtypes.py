from discover_xbrl.sbrnl_modules.db.version_compare.compare_mixin import CompareMixin


class ItemTypes(CompareMixin):
    def __init__(self, parent, entrypoints=None, domein=None):
        self.parent = parent
        self.entrypoints = entrypoints
        self.domein = domein

    @property
    def queries(self):
        queries = {}
        if self.domein:
            queries["new"] = self.parent.compare_db.rewrite_query(
                "select distinct itemtype.id, itemtype.name, domain, base, inheritance_type, restriction_type, "
                "restriction_length, restriction_pattern, restriction_max_length, restriction_min_length, "
                "total_digits, fraction_digits, min_inclusive from itemtype "
                "left join dts_itemtype di on itemtype.name = di.itemtype_name "
                "where dts_name in %s"
            )
            queries["compare"] = self.parent.checkpoint_db.rewrite_query(
                "select itemtype.id, itemtype.name, domain, base, inheritance_type, restriction_type, "
                "restriction_length, restriction_pattern, restriction_max_length, restriction_min_length, "
                "total_digits, fraction_digits, min_inclusive from itemtype "
                "left join dts_itemtype di on itemtype.name = di.itemtype_name "
                "where itemtype.name = %s and dts_name in %s"
            )
            queries["backref"] = self.parent.compare_db.rewrite_query(
                "select dts_name from dts_itemtype where itemtype_name = %s"
            )
        elif not self.entrypoints:
            queries["new"] = self.parent.compare_db.rewrite_query(
                "select distinct id, name, domain, base, inheritance_type, restriction_type, restriction_length, "
                "restriction_pattern, restriction_max_length, restriction_min_length, total_digits, "
                "fraction_digits, min_inclusive from itemtype"
            )
            queries["compare"] = self.parent.checkpoint_db.rewrite_query(
                "select id, name, domain, base, inheritance_type, restriction_type, restriction_length, "
                "restriction_pattern, restriction_max_length, restriction_min_length, total_digits, "
                "fraction_digits, min_inclusive from itemtype "
                "where name = %s"
            )
            queries["backref"] = self.parent.compare_db.rewrite_query(
                "select dts_name from dts_itemtype where itemtype_name = %s"
            )
        else:
            queries["new"] = self.parent.compare_db.rewrite_query(
                "select itemtype.id, itemtype.name, domain, base, inheritance_type, restriction_type, "
                "restriction_length, restriction_pattern, restriction_max_length, restriction_min_length, "
                "total_digits, fraction_digits, min_inclusive from itemtype "
                "left join dts_itemtype di on itemtype.name = di.itemtype_name "
                "where dts_name = %s"
            )
            queries["compare"] = self.parent.checkpoint_db.rewrite_query(
                "select itemtype.id, itemtype.name, domain, base, inheritance_type, restriction_type, "
                "restriction_length, restriction_pattern, restriction_max_length, restriction_min_length, "
                "total_digits, fraction_digits, min_inclusive from itemtype "
                "left join dts_itemtype di on itemtype.name = di.itemtype_name "
                "where itemtype.name = %s and dts_name = %s"
            )
            queries["backref"] = self.parent.compare_db.rewrite_query(
                "select dts_name from dts_itemtype where itemtype_name = %s"
            )
        return queries

    def compare(self):
        if self.domein:
            new_itemtypes = self.new_objects(
                args=(self.parent.new_domeinen.get_domein(self.domein),)
            )
        elif not self.entrypoints:
            new_itemtypes = self.new_objects()
        else:
            new_itemtypes = self.new_objects((self.entrypoints["new_version"],))
        new = []
        for item in new_itemtypes:
            if item[1] is None:
                continue
            if self.domein:
                args = (item[1], self.parent.old_domeinen.get_domein(self.domein))
            elif not self.entrypoints:
                args = (item[1],)
            else:
                args = (item[1], self.entrypoints["old_version"])

            self.parent.checkpoint_cursor.execute(self.queries["compare"], args)
            matches = self.parent.checkpoint_cursor.fetchall()
            if not len(matches) and item[0]:
                if self.domein and self.object_seen(
                    object_type="itemtype", object_id=item[0], mutation_type="new"
                ):
                    continue
                new.append(item)

        block = {
            "title": "Itemtypes",
            "summary": f"Total Itemtypes: {len(new_itemtypes)}.  New itemtypes: {len(new)}. ",
            "detail": [],
        }
        if not self.entrypoints:
            block["detail"].append(
                f"<p>Details linken naar een willekeurig entrypoint waarin dit item voorkomt. "
                f"Het  itemtype kan theoretisch in elk entrypoint aangeroepen worden.</p><ul>"
            )
        for it in sorted(new):
            if self.domein:
                self.parent.compare_cursor.execute(self.queries["backref"], (it[1],))
                dts_name = self.parent.compare_cursor.fetchone()
            elif not self.entrypoints:
                self.parent.compare_cursor.execute(self.queries["backref"], (it[1],))
                dts_name = self.parent.compare_cursor.fetchone()
            else:
                dts_name = [self.entrypoints["new_version"]]
            if dts_name:
                block["detail"].append(
                    f"<li><a href='{self.webserver}/dts/{self.parent.new_version_db}/{dts_name[0]}"
                    f"/itemtypes/{it[1]}'>{it[1]}</a> {it[2]}</li>"
                )
            else:
                block["detail"].append(f"<li>{it[2]}</li>")

        block["detail"].append("</ul>")
        self.parent.report.html.append(block)
