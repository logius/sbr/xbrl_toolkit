from collections import defaultdict
from discover_xbrl.sbrnl_modules.db.version_compare.compare_mixin import CompareMixin


class Labels(CompareMixin):
    def __init__(self, parent, entrypoints=None, domein=None):
        self.parent = parent
        self.entrypoints = entrypoints
        self.domein = domein
        self.labels = []

    @property
    def queries(self):
        # update coming
        queries = {}
        if self.domein:
            queries["new"] = self.parent.compare_db.rewrite_query(
                "select distinct id, link_label, link_role, lang, label_text from label "
                "left join concept_label cl on label.link_label = cl.label_link_label "
                "  and label.lang = cl.label_lang "
                "  and label.link_role = cl.label_link_role "
                "left join dts_concept dc on cl.concept_id = dc.concept_id "
                "where cl.dts_name in %s "
                "union "
                "select distinct id, link_label, link_role, lang, label_text from label "
                "left join dts_label dl on dl.label_link_label = link_label "
                "where dts_name in %s "
            )
            queries["compare"] = self.parent.compare_db.rewrite_query(
                "select id, link_label, link_role, lang, label_text from label "
                "left join concept_label cl on label.link_label = cl.label_link_label "
                "  and label.lang = cl.label_lang "
                "  and label.link_role = cl.label_link_role "
                "left join dts_concept dc on cl.concept_id = dc.concept_id "
                "where link_label = %s and cl.dts_name in %s "
                "union "
                "select distinct id, link_label, link_role, lang, label_text from label "
                "left join dts_label dl on dl.label_link_label = link_label "
                "where link_label = %s and dts_name in %s "
            )
            queries["backref"] = self.parent.compare_db.rewrite_query(
                "select distinct concept_label.dts_name, concept_label.concept_id, label_link_label "
                "from concept_label "
                "left join dts_concept on dts_concept.concept_id = concept_label.concept_id "
                "where label_link_label = %s and concept_label.dts_name in %s "
                "  and concept_label.label_lang = %s "
                "  and concept_label.label_link_role = %s "
                "union "
                "select distinct dts_name, 'other', label_link_label from  dts_label "
                "where label_link_label = %s and dts_name in %s "
            )
            queries["backref_enumeration"] = self.parent.compare_db.rewrite_query(
                "select distinct(dts_name), di.itemtype_name from enumeration_option_label eol "
                " left join enumeration_option eo on eo.id = eol.enumeration_option_id "
                " left join itemtype_enumeration_option ieo on eo.id = ieo.enumeration_option_id"
                " left join dts_itemtype di on ieo.itemtype_name = di.itemtype_name "
                "where label_link_label = %s and label_lang = %s and label_link_role = %s "
                " and dts_name in %s "
            )
        elif not self.entrypoints:
            queries["new"] = self.parent.compare_db.rewrite_query(
                "select id, link_label, link_role, lang, label_text from label "
            )
            queries["compare"] = self.parent.compare_db.rewrite_query(
                "select id, link_label, link_role, lang, label_text from label "
                "where link_label = %s and lang = %s and link_role = %s "
            )
            queries["backref"] = self.parent.compare_db.rewrite_query(
                "select distinct(concept_label.dts_name), concept_label.concept_id, label_link_label "
                "from concept_label "
                "left join dts_concept on dts_concept.concept_id = concept_label.concept_id "
                "where label_link_label = %s "
                "  and concept_label.label_lang = %s "
                "  and concept_label.label_link_role = %s "
            )
            queries["backref_enumeration"] = self.parent.compare_db.rewrite_query(
                "select distinct(dts_name), di.itemtype_name from enumeration_option_label eol "
                " left join enumeration_option eo on eo.id = eol.enumeration_option_id "
                " left join itemtype_enumeration_option ieo on eo.id = ieo.enumeration_option_id"
                " left join dts_itemtype di on ieo.itemtype_name = di.itemtype_name "
                "where label_link_label = %s and label_lang = %s and label_link_role = %s"
            )
        else:
            queries["new"] = self.parent.compare_db.rewrite_query(
                "select id, link_label, link_role, lang, label_text from label "
                "left join concept_label cl on label.link_label = cl.label_link_label "
                "  and cl.label_lang = label.lang "
                "  and cl.label_link_role = label.link_role "
                "left join dts_concept dc on cl.concept_id = dc.concept_id "
                "where cl.dts_name = %s "
                "union "
                "select id, link_label, link_role, lang, label_text from label "
                "left join dts_label dl on label.link_label = dl.label_link_label "
                "where dts_name = %s"
            )
            queries["compare"] = self.parent.compare_db.rewrite_query(
                "select id, link_label, link_role, lang, label_text from label "
                "left join concept_label cl on label.link_label = cl.label_link_label "
                "  and cl.label_lang = label.lang"
                "  and cl.label_link_role = label.link_role "
                "left join dts_concept dc on cl.concept_id = dc.concept_id "
                "where link_label = %s and cl.dts_name = %s "
                "union "
                "select id, link_label, link_role, lang, label_text from label "
                "left join dts_label dl on label.link_label = dl.label_link_label "
                "where link_label = %s and dts_name = %s "
            )
            queries["backref"] = self.parent.compare_db.rewrite_query(
                "select concept_label.dts_name, concept_label.concept_id, label_link_label "
                "from concept_label "
                "left join dts_concept on dts_concept.concept_id = concept_label.concept_id "
                "where label_link_label = %s and concept_label.dts_name = %s "
                "  and concept_label.label_lang = %s "
                "  and concept_label.label_link_role = %s "
                "union "
                "select dts_name, 'other', label_link_label from dts_label "
                "where label_link_label = %s and dts_name = %s "
            )
            queries["backref_enumeration"] = self.parent.compare_db.rewrite_query(
                "select distinct(dts_name), di.itemtype_name from enumeration_option_label eol "
                " left join enumeration_option eo on eo.id = eol.enumeration_option_id "
                " left join itemtype_enumeration_option ieo on eo.id = ieo.enumeration_option_id"
                " left join dts_itemtype di on ieo.itemtype_name = di.itemtype_name "
                "where label_link_label = %s and label_lang = %s and label_link_role = %s "
                "  and dts_name = %s "
            )

        return queries

    def compare(self):
        if self.domein:
            new_labels = self.new_objects(
                args=(
                    self.parent.new_domeinen.get_domein(self.domein),
                    self.parent.new_domeinen.get_domein(self.domein),
                )
            )
            old_labels = self.old_objects(
                args=(
                    self.parent.old_domeinen.get_domein(self.domein),
                    self.parent.old_domeinen.get_domein(self.domein),
                )
            )
        elif not self.entrypoints:
            new_labels = self.new_objects()
            old_labels = self.old_objects()
        else:
            new_labels = self.new_objects(
                args=(self.entrypoints["new_version"], self.entrypoints["new_version"])
            )
            old_labels = self.old_objects(
                args=(self.entrypoints["old_version"], self.entrypoints["old_version"])
            )

        new = []
        for lb in new_labels:
            if self.domein:
                args = (
                    lb[1],
                    self.parent.old_domeinen.get_domein(self.domein),
                    lb[1],
                    self.parent.old_domeinen.get_domein(self.domein),
                )
            elif not self.entrypoints:
                args = (lb[1], lb[3], lb[2])  # id, lang, role
            else:
                args = (
                    lb[1],
                    self.entrypoints["old_version"],
                    lb[1],
                    self.entrypoints["old_version"],
                )

            self.parent.checkpoint_cursor.execute(self.queries["compare"], args)
            matches = self.parent.checkpoint_cursor.fetchall()
            if not len(matches) and lb not in new:
                if self.domein and self.object_seen(
                    object_type="label",
                    object_id=f"{lb[1]}-{lb[3]}-{lb[2]}",
                    mutation_type="new",
                ):
                    continue
                new.append(lb)

        roles = defaultdict(int)
        languages = defaultdict(int)
        for label in new:  # tel pet taal en label-rol
            if self.domein:
                args = (
                    label[1],
                    self.parent.new_domeinen.get_domein(self.domein),
                    label[3],
                    label[2],
                    label[1],
                    self.parent.new_domeinen.get_domein(self.domein),
                )
            elif not self.entrypoints:
                args = (label[1], label[3], label[2])  # id, lang, role
            else:
                args = (
                    label[1],
                    self.entrypoints["new_version"],
                    label[3],
                    label[2],
                    label[1],
                    self.entrypoints["new_version"],
                )
            roles[label[2]] += 1
            languages[label[3]] += 1
            self.parent.compare_cursor.execute(self.queries["backref"], args)
            backrefs = self.parent.compare_cursor.fetchall()
            backref_type = "concept"
            if not backrefs:
                self.parent.compare_cursor.execute(
                    self.queries["backref_enumeration"], args
                )
                backrefs = self.parent.compare_cursor.fetchall()
                backref_type = "itemtype"

            self.labels.append(
                {"label": label, "type": backref_type, "backrefs": backrefs}
            )
        block = {
            "title": "Labels",
            "summary": f"Total Labels: {len(new_labels)}.  New labels: {len(new)}.",
            "detail": [],
        }
        block["detail"].append("per rol <ul>")
        for role, count in sorted(roles.items(), key=lambda item: item[0]):
            block["detail"].append(f"<li>{role}: {count}</li>")
            block["detail"].append("<details><summary class='summary'>labels</summary>")
            block["detail"].append("<div class='details'><ul>")
            rolelabels = [dct for dct in self.labels if dct["label"][2] == role]
            dedupped = {}
            for dict in rolelabels:
                if not dict["backrefs"]:
                    block["detail"].append(f"<li>{dict['label'][0]}</li>")
                    continue
                concept_id = dict["backrefs"][0][1]
                if not dedupped.get(concept_id):
                    dedupped[concept_id] = {
                        "dts_name": dict["backrefs"][0][0],
                        "languages": 1,
                    }
                else:
                    dedupped[concept_id]["languages"] += 1  # Ce n'est pas vrai!
            for concept_id, info in dedupped.items():
                block["detail"].append(
                    f"<li>"
                    f"<a href='{self.webserver}/dts/{self.parent.new_version_db}/"
                    f"{info['dts_name']}/concepts/"
                    f"concept/{concept_id}'>{concept_id}"
                    f"</a> ({info['languages']} talen)</li>"
                )
            block["detail"].append("</ul></div></details>")
        block["detail"].append("</ul>per taal <ul>")
        for language, count in languages.items():
            block["detail"].append(f"<li>{language}: {count}</li>")
        block["detail"].append("</ul>")
        self.parent.report.html.append(block)

        deleted = []
        self.labels = []
        for lb in old_labels:
            if self.domein:
                args = (
                    lb[1],
                    self.parent.new_domeinen.get_domein(self.domein),
                    lb[1],
                    self.parent.new_domeinen.get_domein(self.domein),
                )
            elif not self.entrypoints:
                args = (lb[1], lb[3], lb[2])  # id, lang, role
            else:
                args = (
                    lb[1],
                    self.entrypoints["new_version"],
                    lb[1],
                    self.entrypoints["new_version"],
                )

            self.parent.compare_cursor.execute(self.queries["compare"], args)
            matches = self.parent.compare_cursor.fetchall()
            if not len(matches) and lb not in deleted:
                if self.domein:
                    if self.object_seen(
                        object_type="label", object_id=lb[1], mutation_type="delete"
                    ):
                        continue
                deleted.append(lb)
        roles = defaultdict(int)
        languages = defaultdict(int)

        for label in deleted:  # tel pet taal en label-rol
            if self.domein:
                args = (
                    label[1],
                    self.parent.old_domeinen.get_domein(self.domein),
                    label[3],
                    label[2],
                    label[1],
                    self.parent.old_domeinen.get_domein(self.domein),
                )
            elif not self.entrypoints:
                args = (label[1], label[3], label[2])  # id, lang, role
            else:
                args = (
                    label[1],
                    self.entrypoints["old_version"],
                    label[3],
                    label[2],
                    label[1],
                    self.entrypoints["old_version"],
                )
            roles[label[2]] += 1
            languages[label[3]] += 1

            self.parent.checkpoint_cursor.execute(self.queries["backref"], args)
            backrefs = self.parent.checkpoint_cursor.fetchall()
            backref_type = "concept"
            if not backrefs:
                self.parent.checkpoint_cursor.execute(
                    self.queries["backref_enumeration"], args
                )
                backrefs = self.parent.checkpoint_cursor.fetchall()
                backref_type = "itemtype"

            self.labels.append(
                {"label": label, "type": backref_type, "backrefs": backrefs}
            )

        block = {
            "title": "Deleted labels",
            "summary": f"Total Labels: {len(old_labels)}.  Deleted labels: {len(deleted)}.",
            "detail": [],
        }
        block["detail"].append("per rol <ul>")
        for role, count in sorted(roles.items(), key=lambda item: item[0]):
            block["detail"].append(f"<li>{role}: {count}</li>")
            block["detail"].append("<details><summary class='summary'>labels</summary>")
            block["detail"].append("<div class='details'><ul>")
            rolelabels = [dct for dct in self.labels if dct["label"][2] == role]
            dedupped = {}
            for dict in rolelabels:
                if not dict["backrefs"]:
                    block["detail"].append(f"<li>{dict['label'][0]}</li>")
                    continue
                concept_id = dict["backrefs"][0][1]
                if not dedupped.get(concept_id):
                    dedupped[concept_id] = {
                        "dts_name": dict["backrefs"][0][0],
                        "languages": 1,
                    }
                else:
                    dedupped[concept_id]["languages"] += 1
            for concept_id, info in dedupped.items():
                block["detail"].append(
                    f"<li>"
                    f"<a href='{self.webserver}/dts/{self.parent.old_version_db}/"
                    f"{info['dts_name']}/concepts/"
                    f"concept/{concept_id}'>{concept_id}"
                    f"</a> ({info['languages']} talen)</li>"
                )
            block["detail"].append("</ul></div></details>")
        block["detail"].append("</ul>per taal <ul>")
        for language, count in languages.items():
            block["detail"].append(f"<li>{language}: {count}</li>")
        block["detail"].append("</ul>")
        self.parent.report.html.append(block)
