import re


class Domeinen:
    def __init__(self, parent, version=None):
        self.parent = parent
        self.db_cursor = (
            self.parent.checkpoint_cursor
            if version == "old"
            else self.parent.compare_cursor
        )
        self.entrypoints = self.get_entrypoints()
        self.belastingdienst_regex = re.compile("^bd-")
        self.bzk_regex = re.compile("^abm-.*|^bzk-rpt-de.*")
        self.kvk_regex = re.compile(
            "^bzk-rpt-ti-.*|^bzk-rpt-wnt.*|^ezk-|^kvk.*|^jenv.*|^rj-"
        )
        self.ocw_regex = re.compile("^ocw-.*")
        self.sbr_regex = re.compile("^sbr-.*")

    def get_entrypoints(self):
        self.db_cursor.execute("select name from dts order by name")
        names = self.db_cursor.fetchall()
        return [name for row in names for name in row]

    @property
    def belastingdienst_domein(self):
        return tuple(filter(self.belastingdienst_regex.match, self.entrypoints))

    @property
    def belastingdienst_prefix(self):
        return ["bd"]

    @property
    def bzk_domein(self):
        return tuple(filter(self.bzk_regex.match, self.entrypoints))

    @property
    def bzk_prefix(self):
        return ["bzk"]

    @property
    def kvk_domein(self):
        return tuple(filter(self.kvk_regex.match, self.entrypoints))

    @property
    def kvk_prefix(self):
        return ["ezk", "jenv", "kvk", "rj"]

    @property
    def ocw_domein(self):
        return tuple(filter(self.ocw_regex.match, self.entrypoints))

    @property
    def ocw_prefix(self):
        return ["ocw"]

    @property
    def sbr_domein(self):
        return tuple(filter(self.sbr_regex.match, self.entrypoints))

    @property
    def sbr_prefix(self):
        return ["abm", "bzk", "sbr"]

    def get_domein(self, prefix):
        try:
            return getattr(self, f"{prefix}_domein")
        except AttributeError:
            return None

    def get_prefix(self, domein):
        try:
            return getattr(self, f"{domein}_prefix")
        except AttributeError:
            return None

    def prefix_ok(self, domein, id):
        """"
        Wanneer je per domein draait, dan wil je id's uit een ander domein uitsluiten.
        geen jenv-abstr_xxx als domein OCW is.
        """
        prefix = id.split("-")[0] if "-" in id else id.split("_")[0]
        if prefix in self.get_prefix(domein=domein):
            return True
        return True


def main():
    from version_compare import VersionCompare

    vc = VersionCompare(old_db="nt16", new_db="nt17_beta")
    domeinen = Domeinen(parent=vc, version="old")
    print(domeinen.belastingdienst_domein)
    print(domeinen.bzk_domein)
    print(domeinen.kvk_domein)
    print(domeinen.ocw_domein)
    print(domeinen.sbr_domein)


if __name__ == "__main__":
    main()
