from collections import Counter
from discover_xbrl.sbrnl_modules.db.connection import get_connection
from discover_xbrl.sbrnl_modules.db.collections.linkroles import Linkroles
from discover_xbrl.sbrnl_modules.db.collections.dimensions import Dimensions
from discover_xbrl.sbrnl_modules.db.collections.presentation_hierarchies import (
    PresentationHierarchies,
)
from discover_xbrl.sbrnl_modules.db.render.grid import Grid
from discover_xbrl.sbrnl_modules.db.render.gridcell import GridCell


class TableGrid:
    def __init__(self, db=None, dts_name=None, linkrole_id=None):
        self.db = db
        self.linkrole_id = linkrole_id
        self.dts_name = dts_name
        self.linkrole = None
        self.grid = Grid(name=self.linkrole_id)
        self.fact_nodes = []
        self.aspects = []
        self.max_label_width = 1
        self.should_straighten = False
        if self.linkrole_id and self.dts_name:
            self.create_table()

    def create_table(self):
        self.get_linkrole()
        if (
            self.linkrole
            and len(self.linkrole["tables"])
            # and self.linkrole.get("presentation_hierarchy")
        ):  # a 'real' table consists of tables and presentation
            self.handle_x_axis()  # creates header rows, determines fact-columns
            self.remove_empty_x()
            self.handle_y_axis()  # tries to find the correct presentation
            self.handle_z_axis()  # adds z-axis as a "footer"
            if self.should_straighten:
                self.straighten_table()  # lines up the facts to the right columns
            self.add_context()
        elif self.linkrole and self.linkrole["hypercube"]:
            self.table_from_hypercube()
        elif self.linkrole and self.linkrole.get("presentation_hierarchy"):
            self.table_from_presentation()
            self.straighten_table()
        else:
            return {"css": None, "divs": None}

    def get_linkrole(self):
        """Retrieves the complete Linkrole from the database"""
        if not self.linkrole_id or not self.dts_name:
            return False
        linkroles = Linkroles(db=self.db, full=True)
        linkroles.linkrole_of_dts_by_id(
            dts_name=self.dts_name, linkrole_id=self.linkrole_id
        )
        if not len(linkroles.linkroles):
            return False
        self.linkrole = linkroles.linkroles[0]

    def handle_x_axis(self):
        # every x-axis breakdown adds a header-row
        breakdowns = self.linkrole["tables"][0]["table_breakdowns"]
        total_x = len([bd for bd in breakdowns if bd["axis"] == "x"])
        for index, breakdown in enumerate(
            [bd for bd in breakdowns if bd["axis"] == "x"]
        ):
            self.grid.add_row()
            blanco = GridCell(
                id=f"blanco_{self.grid.rows}_{self.grid.id}",
                row=self.grid.rows,
                column=self.grid.get_free_column(self.grid.rows),
                type="heading",
            )  # headers get an empty cell above the labels of the rows
            self.grid.cells.append(blanco)
            for rulenode in breakdown["table_rulenodes"]:
                self.x_axis_from_rulenode(
                    node=rulenode,
                    add_members=index + 1 == total_x,
                    parent_child_order=rulenode["parent_child_order"],
                )
            for dimension_relationshipnode in breakdown["dimension_relationshipnodes"]:
                self.x_axis_from_dimensionnode(
                    node=dimension_relationshipnode,
                    add_members=index + 1 == total_x,
                    parent_child_order=dimension_relationshipnode["parent_child_order"],
                )
            for concept_relation in breakdown["concept_relationshipnodes"]:
                self.x_axis_from_conceptnode(
                    node=concept_relation,
                    add_members=True,
                    parent_child_order=concept_relation["parent_child_order"],
                )
        # The heading has now been built
        for cell in self.grid.cells:
            if cell.restructure:
                self.restructure_heading(cell.row)
                break  # just once

    def x_axis_from_rulenode(
        self, node, add_members, parent_child_order=None, fact_node=None
    ):
        if not node["is_abstract"]:
            label = ""
            labels = []
            if node["formula_periods"]:
                label = self.formula_period_label(formula=node["formula_periods"][0])
            if node["formula_explicit_dimensions"]:
                labels = self.formula_explicit_dimension_label(
                    formula=node["formula_explicit_dimensions"][0]
                )
            elif node["rulesets"]:
                label = self.rulesets_label(rulesets=node["rulesets"])
            # if this rulenode has a label of type 'message' use that (Consistent Presentation)
            if node["labels"]:
                dynamic_label = self.get_dynamic_label(node=node)
                if (
                    dynamic_label
                ):  # Returns False if no message found, keep the ruleset dates?
                    label = dynamic_label
            column = self.grid.get_free_column(self.grid.rows)
            cell = GridCell(
                id=f"x_{node['id']}",
                row=self.grid.rows,
                column=column,
                type="context_heading",
                parent_child_order=node["parent_child_order"]
                if node.get("parent_child_order")
                else parent_child_order,
                rulenode=node,
                contaxt_heading=label,
                labels=labels,
            )
            self.grid.cells.append(cell)
            node["concept"] = None
            if fact_node is None:
                self.fact_nodes.append(
                    {"node": node, "column": column, "rulenode": node}
                )
            else:
                fact_node["column"] = column
                fact_node["rulenode"] = node
                fact_node["path"].append(node["id"])
                self.fact_nodes.append(fact_node)

        # elif node["formula_periods"]:
        # Todo: BUT they DO add their Aspect or Dimension to the Column
        #  here we need to look @merge Als die False is, dan moet er toch een cell worden toegevoegd
        else:
            if fact_node is None:
                fact_node = {"node": node, "_type": "rulenode", "path": [node["id"]]}
            else:
                fact_node["path"].append(node["id"])

        for dimension_relation in node["dimension_relationshipnodes"]:
            self.x_axis_from_dimensionnode(
                node=dimension_relation,
                add_members=add_members,
                parent_child_order=dimension_relation["parent_child_order"]
                if dimension_relation["parent_child_order"]
                else parent_child_order,
            )
        for concept_relation in node["concept_relationshipnodes"]:
            self.x_axis_from_conceptnode(
                node=concept_relation,
                add_members=add_members,
                parent_child_order=concept_relation["parent_child_order"]
                if concept_relation["parent_child_order"]
                else parent_child_order,
            )

        if node.get("children"):
            for child in node["children"]:
                self.x_axis_from_rulenode(
                    child,
                    add_members=add_members,
                    parent_child_order=node["parent_child_order"]
                    if node.get("parent_child_order")
                    else parent_child_order,
                    fact_node=fact_node,
                )

    def x_axis_from_conceptnode(self, node, add_members=True, parent_child_order=None):
        presentation_node = self.match_presentation(node=node)
        if presentation_node:
            if presentation_node.get("children"):
                for node in presentation_node["children"]:
                    self.x_axis_from_presentation(
                        node=node,
                        add_members=add_members,
                        parent_child_order=parent_child_order,
                    )
            else:
                self.x_axis_from_presentation(
                    node=presentation_node,
                    add_members=add_members,
                    parent_child_order=parent_child_order,
                )

    def x_axis_from_presentation(
        self, node, add_members=True, parent_child_order=None, restructure=False
    ):
        if not node["concept"]["abstract"]:
            column = self.grid.get_free_column(self.grid.rows)
            height = 1
            width = 1
            if node.get("children"):
                height = 2
                width = len(node["children"]) + 1

            cell = GridCell(
                id=f"x_{column}_{node['id']}",
                row=self.grid.rows,
                row_end=self.grid.rows + height,
                column=column,
                column_end=column + width,
                type="fact_heading",
                concept=node["concept"],
                preferred_label=node["preferred_label"],
                parent_child_order=parent_child_order,
                restructure=restructure,
            )
            self.grid.cells.append(cell)
            if add_members:
                self.fact_nodes.append(
                    {"node": node, "column": column, "concept": node["concept"]}
                )
        if node.get("children"):
            current_row = self.grid.rows
            self.grid.add_row(row=current_row + 1, move_pointer=False)
            self.grid.cells[0].row_end = 3  # raise the blanco cell
            for child in node["children"]:
                self.x_axis_from_presentation(
                    node=child,
                    add_members=add_members,
                    parent_child_order=parent_child_order,
                    restructure=True,
                )

    def x_axis_from_dimensionnode(self, node, add_members, parent_child_order=None):
        if self.linkrole["hypercube"]:
            for dimension in self.linkrole["hypercube"]["dimensions"]:
                if (
                    f'{dimension["concept"]["ns_prefix"]}:{dimension["concept"]["name"]}'
                    == node["dimension_id"]
                ):
                    if dimension["members"]:
                        self.x_cells_from_members(
                            dimension["members"],
                            add_members,
                            node,
                            parent_child_order=node["parent_child_order"]
                            if node["parent_child_order"]
                            else parent_child_order,
                        )
        else:
            alt_dimensions = Dimensions(db=self.db, full=True)
            alt_dimensions.alternative_dimensions(
                dts_name=self.dts_name, qname=node["dimension_id"]
            )
            if len(alt_dimensions.dimensions):
                dimension = alt_dimensions.dimensions[0]
                if dimension["members"]:
                    self.x_cells_from_members(
                        dimension["members"],
                        add_members,
                        node,
                        parent_child_order=node["parent_child_order"]
                        if node["parent_child_order"]
                        else parent_child_order,
                    )

    def x_cells_from_members(self, members, add_members, node, parent_child_order=None):
        for index, member in enumerate(members):
            if member.get("members") and parent_child_order == "children-first":
                current_row = self.grid.rows
                height = 2
                width = len(member["members"]) + 1
                self.grid.add_row()
                self.grid.cells[0].row_end += 1  # raise the blanco cell
                self.x_cells_from_members(
                    member["members"],
                    add_members,
                    node,
                    parent_child_order=parent_child_order,
                )
                column = (
                    self.grid.get_free_column(current_row) + 1
                )  # eentje opschuiven ....
            else:
                column = self.grid.get_free_column(self.grid.rows)
                height = 1
                width = 1
                current_row = self.grid.rows
                if member.get("members"):
                    height = 2
                    width = len(member["members"]) + 1

            cell = GridCell(
                id=f"x_{member['concept_id']}",
                row=current_row,
                row_end=current_row + height,
                column=column,
                column_end=column + width,
                type="fact_heading",
                concept=member["concept"],
                preferred_label=member["preferred_label"],
                parent_child_order=node["parent_child_order"]
                if node.get("parent_child_order")
                else parent_child_order,
            )
            self.grid.cells.append(cell)
            if (
                not member["concept"]["abstract"]
                or member["concept"]["substitution_group"] == "sbr:domainMemberItem"
                or member["concept"]["substitution_group"] == "xbrli:item"
                and add_members
            ):
                self.fact_nodes.append(
                    {"node": node, "column": column, "member": member}
                )

            if member.get("members") and not parent_child_order == "children-first":
                self.grid.add_row()
                self.grid.cells[0].row_end += 1  # raise the blanco cell
                self.grid.move_row_start(self.grid.rows, columns=1)
                self.x_cells_from_members(
                    member["members"],
                    add_members,
                    node,
                    parent_child_order=parent_child_order,
                )

    def handle_y_axis(self):
        breakdowns = self.linkrole["tables"][0]["table_breakdowns"]
        total_y = len([bd for bd in breakdowns if bd["axis"] == "y"])
        for index, breakdown in enumerate(
            [bd for bd in breakdowns if bd["axis"] == "y"]
        ):
            if index == 0:
                self.grid.add_row()
            for concept_relationshipnode in breakdown["concept_relationshipnodes"]:
                presentation_node = self.match_presentation(
                    node=concept_relationshipnode
                )
                if presentation_node:
                    self.should_straighten = True
                    if presentation_node.get("children"):
                        for node in presentation_node["children"]:
                            self.handle_y_presentation_node(
                                node, parent_child_order=breakdown["parent_child_order"]
                            )
                    else:
                        self.handle_y_presentation_node(
                            presentation_node,
                            parent_child_order=breakdown["parent_child_order"],
                        )

            for dimension_relationshipnode in breakdown["dimension_relationshipnodes"]:
                self.should_straighten = True
                dimension = self.match_dimension(
                    dimension_relationshipnode["dimension_id"]
                )
                if dimension:
                    for member in dimension["members"]:
                        self.handle_y_dimension_member(
                            member=member,
                            parent_child_order=dimension_relationshipnode[
                                "parent_child_order"
                            ],
                        )

            for aspect in breakdown["aspect_nodes"]:
                if aspect["aspect_type"] == "dimension":
                    dimension = self.match_dimension(qname=aspect["value"])
                    if dimension:
                        for member in dimension["members"]:
                            self.handle_y_aspect_dimension_member(
                                member,
                                dimension=dimension,
                                add_members=index + 1 == total_y,
                                position=index,
                            )

            for rulenode in breakdown["table_rulenodes"]:
                self.handle_y_rulenode(
                    rulenode=rulenode,
                    parent_child_order=breakdown["parent_child_order"],
                    add_members=index + 1 == total_y,
                    position=index,
                )

    def handle_y_rulenode(
        self, rulenode, parent_child_order=None, add_members=True, position=None
    ):
        if rulenode.get("concept_relationshipnodes"):
            for concept_relationshipnode in rulenode["concept_relationshipnodes"]:
                presentation_node = self.match_presentation(
                    node=concept_relationshipnode
                )
                if presentation_node:
                    self.should_straighten = True
                    for node in presentation_node["children"]:
                        self.handle_y_presentation_node(
                            node,
                            parent_child_order=concept_relationshipnode[
                                "parent_child_order"
                            ],
                        )
        if rulenode.get("aspect_nodes"):
            for aspect_node in rulenode["aspect_nodes"]:
                if aspect_node["aspect_type"] == "dimension":
                    dimension = self.match_dimension(qname=aspect_node["value"])
                    if dimension:
                        for member in dimension["members"]:
                            self.handle_y_aspect_dimension_member(
                                member,
                                dimension=dimension,
                                add_members=add_members,
                                position=position,
                            )

        for dimension_relationshipnode in rulenode["dimension_relationshipnodes"]:
            dimension = self.match_dimension(dimension_relationshipnode["dimension_id"])
            if dimension:
                self.should_straighten = True
                for member in dimension["members"]:
                    self.handle_y_dimension_member(
                        member=member,
                        parent_child_order=dimension_relationshipnode[
                            "parent_child_order"
                        ],
                    )

        if rulenode.get("children"):
            for child in rulenode["children"]:
                self.handle_y_rulenode(
                    rulenode=child,
                    parent_child_order=parent_child_order,
                    add_members=add_members,
                    position=position,
                )

        if (
            rulenode.get("children") is None
            and not rulenode.get("dimension_relationshipnodes")
            and not rulenode.get("aspect_nodes")
            and not rulenode.get("concept_relationshipnodes")
        ):
            cell = GridCell(
                id=f"y_{rulenode['id']}",
                row=self.grid.rows,
                column=1,
                type="label",
                rulenode=rulenode,
                labels=rulenode["labels"],
            )
            self.grid.cells.append(cell)
            for index, fact_dict in enumerate(self.fact_nodes):
                fact_dict["column"] = max(
                    self.grid.get_free_column(self.grid.rows), fact_dict["column"]
                )
                factcell = GridCell(
                    id=f"fact_{index}_{self.grid.rows}_{rulenode['id']}",
                    row=self.grid.rows,
                    column=self.grid.get_free_column(self.grid.rows),
                    type="value",
                    concept=fact_dict["concept"]
                    if fact_dict.get("concept")
                    else None,  # here we add x-axis information
                    rulenode=rulenode,
                )
                self.grid.cells.append(factcell)

    def handle_y_aspect_dimension_member(
        self, member, dimension=None, add_members=True, position=None
    ):
        # Adds a value on column 1, and stuffs it's label inside the 'blanco'
        # adds x-axis fact-knowledge as the concept
        height = 1
        cell = GridCell(
            id=f"y_{self.grid.rows}_{member['concept']['name']}",
            row=self.grid.rows,
            column=position + 1,
            row_end=self.grid.rows + height,
            type="value",
            concept=member["concept"],
            labels=dimension["concept"]["labels"] if dimension.get("concept") else None,
        )
        self.grid.cells.append(cell)
        if position > 0:
            self.grid.insert_column(
                position=position,
                cell=GridCell(
                    type="fact_heading",
                    id=f"aspect_node_heading_{position}",
                    row=self.grid.rows - 1,
                    column=position + 1,
                    concept=dimension["concept"]
                    if dimension.get("concept")
                    else member["concept"],
                ),
            )
            for fact_node in self.fact_nodes:
                if fact_node["column"] > position:
                    fact_node["column"] += 1
        else:
            self.grid.cells[0].id = f"aspect_node_heading_{position}"
            self.grid.cells[0].type = "fact_heading"
            self.grid.cells[0].concept = (
                dimension["concept"] if dimension.get("concept") else member["concept"]
            )
        self.aspects.append(member)

        if (
            not member["concept"]["abstract"]
            or member["concept"]["substitution_group"] == "sbr:domainMemberItem"
            or member["concept"]["substitution_group"] == "xbrli:item"
        ) and add_members:
            for index, fact_dict in enumerate(self.fact_nodes):
                # print("Joe")
                # print(fact_dict.keys())
                fact_dict["column"] = max(
                    self.grid.get_free_column(self.grid.rows), fact_dict["column"]
                )
                factcell = GridCell(
                    id=f"fact_{index}_{self.grid.rows}_{member['concept']['name']}",
                    row=self.grid.rows,
                    column=self.grid.get_free_column(self.grid.rows),
                    type="value",
                    concept=fact_dict["concept"]
                    if fact_dict.get("concept")
                    else None,  # here we add x-axis information
                    dimension=dimension,
                    member=member,
                )
                self.grid.cells.append(factcell)

            # close the row
            self.grid.add_row()

    def handle_y_dimension_member(self, member, parent_child_order=None):
        # Plz refactore me, this and dimension are almost identical
        height = self.get_member_height(member)
        cell = None
        if height > 1:
            if (
                parent_child_order == "children-first"
                and member.get("members")
                and len(member["members"])
                and (
                    not member["concept"]["abstract"]
                    or member["concept"]["substitution_group"] == "sbr:domainMemberItem"
                    or member["concept"]["substitution_group"] == "xbrli:item"
                )  # children-first
            ):
                # make room for current node
                self.grid.move_row_start(row=self.grid.rows, columns=1)
                original_row = self.grid.rows
                for i in range(self.grid.rows + 1, self.grid.rows + height):
                    self.grid.add_row(row=i, move_pointer=False)
                    if i < (self.grid.rows + height) - 1:
                        self.grid.move_row_start(row=i, columns=1)
                # now do the children
                if member.get("members"):
                    for child in member["members"]:
                        self.handle_y_dimension_member(
                            member=child, parent_child_order=parent_child_order
                        )
                cell = GridCell(
                    id=f"y_{self.grid.rows}_{member['concept']['name']}",
                    row=original_row,
                    column=self.grid.get_free_column(self.grid.rows),
                    row_end=original_row + height,
                    type="label",
                    concept=member["concept"],
                    preferred_label=member["preferred_label"],
                    parent_child_order=parent_child_order,
                    children_first_css=True,
                )
            else:
                for i in range(self.grid.rows + 1, self.grid.rows + height):
                    self.grid.add_row(row=i, move_pointer=False)
                    self.grid.move_row_start(row=i, columns=1)

        if cell is None:
            cell = GridCell(
                id=f"y_{self.grid.rows}_{member['concept']['name']}",
                row=self.grid.rows,
                column=self.grid.get_free_column(self.grid.rows),
                row_end=self.grid.rows + height,
                type="label",
                concept=member["concept"],
                preferred_label=member["preferred_label"],
                parent_child_order=parent_child_order,
            )
        self.grid.cells.append(cell)
        if (
            not member["concept"]["abstract"]
            or member["concept"]["substitution_group"] == "sbr:domainMemberItem"
            or member["concept"]["substitution_group"] == "xbrli:item"
        ):
            for index, fact_dict in enumerate(self.fact_nodes):
                fact_dict["column"] = max(
                    self.grid.get_free_column(self.grid.rows), fact_dict["column"]
                )
                # print(fact_dict.keys())
                factcell = GridCell(
                    id=f"fact_{index}_{self.grid.rows}_{member['concept']['name']}",
                    row=self.grid.rows,
                    column=self.grid.get_free_column(self.grid.rows),
                    type="value",
                    concept=member["concept"],
                    parent_child_order=parent_child_order,
                    preferred_label=member["preferred_label"],
                    aspect=fact_dict["aspect"] if fact_dict.get("aspect") else None,
                )
                self.grid.cells.append(factcell)
            # close the row
            self.grid.add_row()

        if (parent_child_order == "parent-first" and member.get("members")) or (
            parent_child_order == "children_first"
            and member["concept"]["abstract"]
            and member["concept"]["substitution_group"] != "sbr:domainMemberItem"
            and member["concept"]["substitution_group"] != "xbrli:item"
            and member.get("members")
        ):
            for child in member["members"]:
                self.handle_y_dimension_member(
                    member=child, parent_child_order=parent_child_order
                )

    def handle_y_presentation_node(self, node, parent_child_order=None):
        height = self.get_height(node)
        cell = None
        if height > 1:
            if (
                parent_child_order == "children-first"
                and node.get("children")
                and len(node["children"])
                and not node["concept"]["abstract"]
            ):
                # make room for current node
                self.grid.move_row_start(row=self.grid.rows, columns=1)
                original_row = self.grid.rows
                for i in range(self.grid.rows + 1, self.grid.rows + height):
                    self.grid.add_row(row=i, move_pointer=False)
                    if i < (self.grid.rows + height) - 1:
                        self.grid.move_row_start(row=i, columns=1)
                # now do the children
                for child in node["children"]:
                    self.handle_y_presentation_node(
                        node=child, parent_child_order=parent_child_order
                    )
                cell = GridCell(
                    id=f"y_{self.grid.rows}_{node['concept']['name']}",
                    row=original_row,
                    column=self.grid.get_free_column(self.grid.rows),
                    row_end=original_row + height,
                    type="label",
                    concept=node["concept"],
                    preferred_label=node["preferred_label"],
                    parent_child_order=parent_child_order,
                    children_first_css=True,
                )
            else:
                for i in range(self.grid.rows + 1, self.grid.rows + height):
                    self.grid.add_row(row=i, move_pointer=False)
                    self.grid.move_row_start(row=i, columns=1)

        if cell is None:
            cell = GridCell(
                id=f"y_{self.grid.rows}_{node['concept']['name']}",
                row=self.grid.rows,
                column=self.grid.get_free_column(self.grid.rows),
                row_end=self.grid.rows + height,
                type="label",
                concept=node["concept"],
                preferred_label=node["preferred_label"],
                parent_child_order=parent_child_order,
            )
        self.grid.cells.append(cell)
        if not node["concept"]["abstract"]:
            for index, fact_dict in enumerate(self.fact_nodes):
                fact_dict["column"] = max(
                    self.grid.get_free_column(self.grid.rows), fact_dict["column"]
                )
                # print(fact_dict.keys())
                factcell = GridCell(
                    id=f"fact_{index}_{self.grid.rows}_{node['concept']['name']}",
                    row=self.grid.rows,
                    column=self.grid.get_free_column(self.grid.rows),
                    type="value",
                    concept=node["concept"],
                    parent_child_order=parent_child_order,
                )
                self.grid.cells.append(factcell)
            # close the row
            self.grid.add_row()

        if (
            parent_child_order != "children-first"
            and node.get("children")
            and len(node["children"])
        ) or (
            parent_child_order == "children-first"
            and node.get("children")
            and len(node["children"])
            and node["concept"]["abstract"]
        ):  # parent-first? abstract node?  then we do the children now
            for child in node["children"]:
                self.handle_y_presentation_node(
                    node=child, parent_child_order=parent_child_order
                )

    def handle_z_axis(self):
        breakdowns = self.linkrole["tables"][0]["table_breakdowns"]
        for index, breakdown in enumerate(
            [bd for bd in breakdowns if bd["axis"] == "z"]
        ):
            cell = GridCell(
                id="z_axis",
                row=self.grid.rows,
                column=self.grid.get_free_column(self.grid.rows),
                type="z_divider",
            )
            self.grid.cells.append(cell)
            self.grid.add_row()  # this adds a white line.
            if len(breakdown["aspect_nodes"]):
                for aspect in breakdown["aspect_nodes"]:
                    dimension = self.match_dimension(qname=aspect["value"])
                    cell = GridCell(
                        id=f"z_{aspect['label']}",
                        row=self.grid.rows,
                        column=self.grid.get_free_column(self.grid.rows),
                        type="label",
                        aspect=aspect,
                        labels=dimension["concept"]["labels"] if dimension else None,
                    )
                    self.grid.cells.append(cell)
                    cell = GridCell(
                        id=f"td_z_{aspect['value'].replace(':', '_')}",
                        row=self.grid.rows,
                        column=self.grid.get_free_column(self.grid.rows),
                        concept=None,
                        aspect=aspect,
                        type="value",
                    )
                    self.grid.cells.append(cell)

    def get_height(self, node):
        height = 0
        if node.get("is_abstact") and not node["is_abstract"]:
            height = 1
        if node.get("concept") and not node["concept"]["abstract"]:
            height = 1

        if node.get("children") and len(node["children"]):
            for child in node["children"]:
                height += self.get_height(child)

        return height

    def get_member_height(self, member):
        height = 0
        if (
            not member["concept"]["abstract"]
            or member["concept"]["substitution_group"] == "sbr:domainMemberItem"
            or member["concept"]["substitution_group"] == "xbrli:item"
        ):
            height = 1
        if member.get("members"):
            for child in member["members"]:
                height += self.get_member_height(child)
        return height

    def get_dynamic_label(self, node):
        for label in node["labels"]:
            if label["link_role"] == "http://www.xbrl.org/2010/role/message":
                # at the moment we don't care about languagae!
                tokens = label["label_text"].split(" ")
                retval = ""
                for token in tokens:
                    if "$" in token:
                        token = self.match_parameter(
                            token.replace("{", "").replace("}", "")
                        )
                    retval += token + " "
                return retval
        return False

    def formula_period_label(self, formula):
        label = f"searching formulas {formula['period_type']}"
        if formula["period_type"] == "instant":
            label = self.match_parameter(formula["value"])
        elif formula["period_type"] == "duration":
            label = (
                self.match_parameter(formula["start"])
                + " - "
                + self.match_parameter(formula["end"])
            )
        return label

    def formula_explicit_dimension_label(self, formula):
        qname = formula.get("qname_dimension")
        alt_dimensions = Dimensions(db=self.db, full=True)
        alt_dimensions.alternative_dimensions(dts_name=self.dts_name, qname=qname)
        if len(alt_dimensions.dimensions):
            dimension = alt_dimensions.dimensions[0]
            labels = dimension["concept"]["labels"]
            qname = formula.get("qname_member")
            if qname:
                for member in dimension["members"]:
                    if (
                        f'{member["concept"]["ns_prefix"]}:{member["concept"]["name"]}'
                        == qname
                    ):
                        labels = member["concept"]["labels"]
            return labels
        return ""

    def rulesets_label(self, rulesets=None):
        counting = Counter()
        for line_item in self.linkrole["hypercube"]["line_items"]:
            counting[line_item["concept"]["periodtype"]] += 1
        most_used_periodtype, _ = counting.most_common()[0]
        for ruleset in rulesets:
            if ruleset["tag"] == most_used_periodtype and len(
                ruleset["formula_periods"]
            ):
                return self.formula_period_label(ruleset["formula_periods"][0])

    def match_parameter(self, parameter_id):
        # todo: implement xpath2 parser and find the required values
        for parameter in self.linkrole["tables"][0]["parameters"]:
            if f"${parameter['id']}" == parameter_id:
                sel = parameter["select"].split("else")[-1]
                return (
                    sel.strip()
                    .replace("xs:date(", "")
                    .replace(")", "")
                    .replace("'", "")
                )
        if "xs:date" in parameter_id:
            return (
                parameter_id.strip()
                .replace("xs:date(", "")
                .replace(")", "")
                .replace("'", "")
            )

        return "parameter mismatch"

    def match_dimension(self, qname):
        if self.linkrole.get("hypercube"):
            for dimension in self.linkrole["hypercube"]["dimensions"]:
                if (
                    f'{dimension["concept"]["ns_prefix"]}:{dimension["concept"]["name"]}'
                    == qname
                ):
                    return dimension
        # so the dimension can not be found here, maybe it's defined in another linkrole
        alt_dimensions = Dimensions(db=self.db, full=True)
        alt_dimensions.alternative_dimensions(dts_name=self.dts_name, qname=qname)
        if len(alt_dimensions.dimensions):
            return alt_dimensions.dimensions[0]

    def match_presentation(self, node):
        qname = node["relationship_source"]
        if (
            self.linkrole.get("presentation_hierarchy")
            and f"{self.linkrole['presentation_hierarchy']['concept']['ns_prefix']}:{self.linkrole['presentation_hierarchy']['concept']['name']}"
            == qname
        ):
            return self.linkrole["presentation_hierarchy"]
        elif self.linkrole.get("presentation_hierarchy"):
            for top_node in self.linkrole["presentation_hierarchy"]["children"]:
                if (
                    f"{top_node['concept']['ns_prefix']}:{top_node['concept']['name']}"
                    == qname
                ):
                    return top_node
        alt_presentation = PresentationHierarchies(db=self.db, full=True)
        alt_presentation.alternative_presentation(
            dts_name=self.dts_name, qname=qname, role_uri=node["linkrole_role_uri"]
        )
        if len(alt_presentation.presentation_hierarchies):
            return alt_presentation.presentation_hierarchies[0]

    def table_from_hypercube(self):
        for line in self.linkrole["hypercube"]["line_items"]:
            self.grid.add_row()
            cell = GridCell(
                id=f"y_{self.grid.rows}_{line['concept']['name']}",
                row=self.grid.rows,
                column=self.grid.get_free_column(self.grid.rows),
                row_end=self.grid.rows + 1,
                type="label",
                concept=line["concept"],
            )
            self.grid.cells.append(cell)
            factcell = GridCell(
                id=f"fact_{self.grid.rows}_{line['concept']['name']}",
                row=self.grid.rows,
                column=self.grid.get_free_column(self.grid.rows),
                type="value",
                concept=line["concept"],
            )
            self.grid.cells.append(factcell)

    def table_from_presentation(self):
        self.fact_nodes.append({"node": None, "column": 2})
        self.grid.add_row()
        for node in self.linkrole["presentation_hierarchy"]["children"]:
            self.handle_y_presentation_node(node)

    def remove_empty_x(self):
        """
        after creating the x-aaxis, check if we have any labels.
        if none, remove the row from the grid_table
        """
        if self.grid.grid_rows:
            empty_rows = []
            for row in self.grid.grid_rows:
                has_label = False
                for cell in self.grid.cells:
                    if cell.row == row.row:
                        if cell.id.startswith("blanco_"):
                            continue
                        if (
                            cell.aspect is None
                            and cell.concept is None
                            and cell.rulenode
                            and len(cell.rulenode["labels"]) == 0
                        ):
                            continue
                    has_label = True
                if not has_label:
                    # filters out all cells belonging to current row,
                    # because there could potentialy be a second heading row which
                    # does have labels?
                    self.grid.cells = [
                        cell for cell in self.grid.cells if cell.row != row.row
                    ]
                    empty_rows.append(row)

            self.grid.grid_rows = [
                row for row in self.grid.grid_rows if row not in empty_rows
            ]
            self.grid.rows = max(0, self.grid.rows - len(empty_rows))

    def restructure_heading(self, row):
        for cell in reversed(self.grid.cells):
            if cell.row == row:
                if cell.restructure:
                    cell.row_end += 1
                    cell.row += 1
                    cell.restructure = False
                else:
                    cell.row_end += 1
        self.grid.add_row()

    def straighten_table(self):
        if len(self.fact_nodes) == 0:
            return False
        last_fact = self.fact_nodes[-1]
        current_row = 0
        fill_gap = True
        positions = 0
        context_start = 0
        self.grid.width = last_fact["column"]
        for cell in reversed(self.grid.cells):
            if not cell.row == current_row:
                fill_gap = True
                current_row = cell.row
                positions = 0
                if cell.column_end < last_fact["column"] + 1:
                    positions = (last_fact["column"] + 1) - cell.column_end
                    # print(f"row: {current_row} pos: {positions}")
                    # but we could have one trailing header
                    if (
                        cell.parent_child_order == "children-first"
                        and cell.type == "fact_heading"
                        and cell.row > 1
                    ):
                        positions -= 1
            if positions > 0:
                if cell.type in ["value", "fact_heading", "context_heading"]:
                    cell.column += positions
                    cell.column_end += positions
                elif fill_gap:
                    cell.column_end += positions
                    fill_gap = False

    def add_context(self):
        if len(self.fact_nodes) == 0:
            return False
        if len(self.grid.context):
            if self.grid.width == 0:
                self.grid.width = self.fact_nodes[-1]["column"]
            for cell in reversed(self.grid.cells):
                cell.row += 1
                cell.row_end += 1
                if cell.id.startswith("blanco_"):
                    # cell.row = 1  # keep this on pinned to the left top corner
                    cell.column_end = self.grid.width + 1 - len(self.fact_nodes)

            previous_start = (
                self.fact_nodes[-1]["column"] + 1
            )  # take thecolumn of the last fact

            for i, cell in enumerate(reversed(self.grid.context)):
                cell.column_end = previous_start
                previous_start = cell.column
                cell.row_end += 1
                cell.row += 1
        else:
            return False


def test_table(url_params=None):
    dts, linkrole = url_params
    db = get_connection()
    table_grid = TableGrid(
        db=db,
        # dts_name="kvk-rpt-jaarverantwoording-2021-nlgaap-middelgroot.xsd",
        # dts_name="ocw-rpt-jaarverantwoording-2021-nlgaap-onderwijsinstellingen.xsd",
        # dts_name="bd-rpt-ob-aangifte-2022.xsd",
        dts_name=dts,
        linkrole_id=linkrole,
    )
    # print(table_grid.grid.api_response)


if __name__ == "__main__":
    from discover_xbrl.conf.conf import Config

    linkroles = {
        # "previous": (
        #    "kvk-rpt-jaarverantwoording-2021-nlgaap-klein.xsd",
        #    "kvk-lr_NotesConsolidatedRemunerationNonSupervisorySeniorOfficialsOtherWntInstitutionPreviousSpecification",
        # ),
        "aspect_error": (
            "kvk-rpt-jaarverantwoording-2021-nlgaap-middelgroot.xsd",
            "urn:kvk:linkrole:notes-consolidated-intangible-assets-internal-external-generated-movement",
        )
        # "kasstroom": "kvk-lr_ConsolidatedCashFlowStatement",
        # "balans": "kvk-lr_ConsolidatedBalanceSheet",
        # "mutatie": "kvk-lr_NotesEquityStatementOfChanges",
        # "aspects": (
        #    "kvk-rpt-jaarverantwoording-2021-nlgaap-klein.xsd",
        #    "kvk-lr_NotesShareCapitalStatementOfChangesSpecification",
        # ),
        # "only rulenodes with periods": (
        #    "kvk-rpt-jaarverantwoording-2021-nlgaap-groot.xsd",
        #    "kvk-lr_NotesConsolidatedGeneralAccountingPrinciplesPriorPeriodErrors",
        # ),
        # "where are the facts": (
        #    "kvk-rpt-jaarverantwoording-2021-nlgaap-groot.xsd",
        #    "kvk-lr_NotesConsolidatedRemunerationNonSupervisorySeniorOfficialsOtherWntInstitutionSpecification"
        # ),  # as it turns out there is no y-axis
        # "what is going on?": (
        #    "kvk-rpt-jaarverantwoording-2021-nlgaap-groot.xsd",
        #    "kvk-lr_NotesConsolidatedRightsGrantedNotYetExercisedMovement"
        # )
        # "dimension label": (
        #   "kvk-rpt-jaarverantwoording-2021-nlgaap-groot.xsd",
        #    "kvk-lr_NotesConsolidatedNetRevenueSegmentedByIndustrySpecification",
        # )
        # "Raar x-wand en ik oneens": (
        #    "kvk-rpt-jaarverantwoording-2021-nlgaap-groot.xsd",
        #    "kvk-lr_NotesConsolidatedRightsGrantedNotYetExercisedByManagingDirectorSpecification",
        # )
        # "Straighten context!": (
        #    "kvk-rpt-jaarverantwoording-2021-nlgaap-groot.xsd",
        #    "kvk-lr_NotesConsolidatedLoansAdvancesGuarantees"
        # ),
        # "Dubbele dimensie prse-error? db-error?": (
        #    "kvk-rpt-jaarverantwoording-2021-nlgaap-groot.xsd",
        #    "kvk-lr_NotesIntangibleAssetsInternalExternalGeneratedMovement"
        # ),
        # "Straighten table2": (
        #    "kvk-rpt-jaarverantwoording-2021-nlgaap-groot.xsd",
        #    "kvk-lr_NotesInvestmentPropertiesMovement"
        # ),
        # "Aspect op de Z-As": (
        #    "kvk-rpt-jaarverantwoording-2021-nlgaap-klein.xsd",
        #    "kvk-lr_NotesConsolidatedGeneralNotesConsolidatedInterestsOneFifthInterestSpecification"
        # ),
        # "Y Dimension": (
        #    "abm-rpt-aedes-benchmark.xsd",
        #    "abm-lr_AedesBenchmarkPersonnelCosts",
        # ),
        # "Date_heading, double blanco": (
        #    "abm-rpt-aedes-benchmark.xsd",
        #    "abm-lr_AedesBenchmarkConservationMaintenanceExpensesSeparate",
        # ),
        # "double_x": "kvk-lr_NotesConsolidatedReceivablesBreakdownMaturity",
        # "z-axis": "kvk-lr_SigningFinancialStatements",
        # "Y-Aspect": "kvk-lr_NotesExceptionalItemsSpecification"
        # "hypercube": "bd_fbc1f24c08bd4d8d83957bd42a65ccd0_nodim",
        # "presentation": "bd_f503ec52e8ea4bd78f537f1441c01de3",
        # "empty x-heading": "kvk-lr_NotesConsolidatedExceptionalItems",
        # "rows children first": "kvk-lr_NotesConsolidatedAssetsLiabilitiesNotRecognisedOperatingLeaseBreakdown",
        # "Wetboek 2": (
        #    "jenv-rpt-bw2-burgerlijk-wetboek-2.xsd",
        #    "jenv-bw2-lr_DutchCivilCodeBook2",
        # ),
        # "Aspect": (
        #    "kvk-rpt-jaarverantwoording-2021-nlgaap-groot.xsd",
        #    "kvk-lr_NotesConsolidatedIntangibleAssetsIndividualAssetOfImportanceSpecification",
        # ),
    }
    # nakijken:
    # urn:kvk:linkrole:notes-consolidated-investment-properties-movement 79
    # urn:kvk:linkrole:notes-consolidated-investment-properties-economic-life 81
    # urn:kvk:linkrole:notes-consolidated-receivables-breakdown-maturity 109
    # urn:kvk:linkrole:notes-consolidated-provisions-provisions-other-movement-specification 129 (aspect label!)
    # urn:kvk:linkrole:notes-consolidated-noncurrent-liabilities-negative-goodwill-movement 145 heading width

    # Verslag van de Raad van Commissarissen - Rotatierooster: Specificatie
    # wat doet -ie nou? urn:kvk:linkrole:notes-consolidated-intangible-assets-internal-external-generated-movement
    # waar is die fact_cell?? urn:kvk:linkrole:notes-consolidated-receivables-breakdown-maturity

    # straigten table: kvk-lr_NotesConsolidatedLoansAdvancesGuarantees Context

    Config()
    for name, lr in linkroles.items():
        print(f"testing: {name} with linkrole: {lr}")
        test_table(url_params=lr)
