import re
from anytree import NodeMixin, RenderTree
from discover_xbrl.sbrnl_modules.db.connection import get_connection
from discover_xbrl.sbrnl_modules.db.collections.linkroles import Linkroles
from discover_xbrl.sbrnl_modules.db.collections.dimensions import Dimensions
from discover_xbrl.sbrnl_modules.db.collections.presentation_hierarchies import (
    PresentationHierarchies,
)
from discover_xbrl.sbrnl_modules.db.render.render_table_tree import RenderTableTree


class TableTree(NodeMixin):
    def __init__(self, db=None, id=None):
        self.db = db
        self.id = id
        self.linkrole = None
        self.table = None
        self.parent_child_order = None
        self.dts_name = None
        self.y_aspects = False
        self.x_aspects = False
        self.z_axis = False

    def build_table_tree(self, dts_name=None, linkrole_id=None, linkrole_role_uri=None):
        if self.get_linkrole(dts_name=dts_name, linkrole_id=linkrole_id, linkrole_role_uri=linkrole_role_uri):
            breakdowns = self.table["table_breakdowns"]
            if len([bd for bd in breakdowns if bd["axis"] == "x"]) > 0:
                x_axis = TableAxis(axis="x", parent=self)
                x_axis.build_axis([bd for bd in breakdowns if bd["axis"] == "x"])
            if len([bd for bd in breakdowns if bd["axis"] == "y"]) > 0:
                y_axis = TableAxis(axis="y", parent=self)
                y_axis.build_axis([bd for bd in breakdowns if bd["axis"] == "y"])
            if len([bd for bd in breakdowns if bd["axis"] == "z"]) > 0:
                self.z_axis = True
                z_axis = TableAxis(axis="z", parent=self)
                z_axis.build_axis([bd for bd in breakdowns if bd["axis"] == "z"])
        elif (
                self.linkrole
                and self.linkrole["hypercube"]
                and self.linkrole["hypercube"].get("line_items")
        ):
            print("Hypercubing")
            self.table_from_hypercube()
        elif self.linkrole and self.linkrole.get("presentation_hierarchy"):
            print("presenting")
            self.table_from_presentation()
        else:
            print("I do not have a table")
            return False
        return True

    def build_hypercube(self, dts_name=None, linkrole_rowid=None):
        linkroles = Linkroles(db=self.db, full=True)
        linkroles.linkrole_by_rowid(rowid=linkrole_rowid)
        self.linkrole = linkroles.linkroles[0]
        if self.linkrole and self.linkrole["hypercube"]:
            self.table_from_hypercube()
            return True
        return False

    def get_linkrole(self, dts_name=None, linkrole_id=None, linkrole_role_uri=None):
        """Retrieves the complete Linkrole from the database.
        Returns True if there is a table inside, else false """
        linkroles = Linkroles(db=self.db, full=True)
        linkroles.linkrole_of_dts_by_id(dts_name=dts_name, linkrole_id=linkrole_id, roleURI=linkrole_role_uri)
        if not len(linkroles.linkroles):
            return False
        self.linkrole = linkroles.linkroles[0]
        self.dts_name = dts_name
        if len(self.linkrole["tables"]):
            self.table = self.linkrole["tables"][0]
            self.id = self.table["id"]
            return True
        else:
            return False

    @property
    def grid_dimensions(self):
        height = 0
        width = 0
        for axis in self.children:
            if axis.axis == "x":
                height += axis.visible_height
                width += len(axis.slices)
            elif axis.axis == "y":
                height += len(axis.slices)
                width += axis.visible_height
        return width, height

    @property
    def render_html(self):
        x, y = self.grid_dimensions
        if len(self.children):  # we have at least some axis !
            if x * y < 3000:
                engine = RenderTableTree(table_tree=self, swap_axis=False)
                return engine.render()
            else:
                engine = RenderTableTree(table_tree=self, swap_axis=False)
                print(f"Table too big: {x * y} ({x}, {y})")
                return engine.render_too_big((x, y))
        else:
            print("Render something else?")

    def table_from_hypercube(self):
        def resolve_lineitems(line_items, parent):
            for line in line_items:
                node = TableNode(
                    id=line["concept"]["id"],
                    parent=parent,
                    is_abstract=line["concept"]["abstract"],
                    concept=line["concept"],
                    labels=line["concept"]["labels"],
                    merge=False,
                    parent_child_order="parent-first",
                )
                if line["line_items"]:
                    resolve_lineitems(line["line_items"], parent=node)

        x_axis = TableAxis(axis="x", parent=self)
        for dimension in self.linkrole["hypercube"]["dimensions"]:
            if dimension.get("members"):  # skip top-level
                for member in dimension["members"]:
                    if len(x_axis.descendants) > 0:
                        for leaf in x_axis.leaves:
                            x_axis.resolve_members(
                                parent=leaf,
                                members=member.get("members")
                                if member.get("members")
                                else dimension.get("members"),
                                parent_child_order="parent-first",
                            )
                        if not member.get("members"):
                            break
                    else:
                        x_axis.resolve_members(
                            parent=x_axis,
                            members=member.get("members")
                            if member.get("members")
                            else dimension.get("members"),
                            parent_child_order="parent-first",
                        )
                        if not member.get("members"):
                            break
        if not len(self.linkrole["hypercube"]["dimensions"]) > 0:
            node = TableNode(
                id="hypercube_concept", parent=x_axis, is_abstract=False, merge=True
            )

        for leaf in x_axis.descendants:
            if not leaf.is_leaf:
                leaf.is_abstract = True

        # fact = TableNode(parent=x_axis, id="hypercube_fact", merge=False)
        y_axis = TableAxis(axis="y", parent=self)
        resolve_lineitems(self.linkrole["hypercube"]["line_items"], parent=y_axis)

    def table_from_presentation(self):
        x_axis = TableAxis(axis="x", parent=self)
        fact = TableNode(
            parent=x_axis,
            id="presentation_fact",
            merge=False,
            parent_child_order="parent-first",
        )
        y_axis = TableAxis(axis="y", parent=self)
        if self.linkrole["presentation_hierarchy"].get("children"):
            y_axis.resolve_presentation(
                parent=y_axis,
                presentation_nodes=self.linkrole["presentation_hierarchy"]["children"],
                parent_child_order="parent-first",
            )

    def match_dimension(self, qname, linkrole_role_uri=None):
        if self.linkrole.get("hypercube"):
            for dimension in self.linkrole["hypercube"]["dimensions"]:
                if (
                        f'{dimension["concept"]["ns_prefix"]}:{dimension["concept"]["name"]}'
                        == qname
                ):
                    return dimension
        # so the dimension can not be found here, maybe it's defined in another linkrole
        alt_dimensions = Dimensions(db=self.db, full=True)
        alt_dimensions.alternative_dimensions(
            dts_name=self.dts_name, qname=qname, linkrole_role_uri=linkrole_role_uri
        )
        if len(alt_dimensions.dimensions):
            return alt_dimensions.dimensions[0]

    def match_member(self, qname):
        if self.linkrole.get("hypercube"):
            for dimension in self.linkrole["hypercube"]["dimensions"]:
                for member in dimension["members"]:
                    if (
                            f'{member["concept"]["ns_prefix"]}:{member["concept"]["name"]}'
                            == qname
                    ):
                        return member

    def css_grid(self):
        width, height = self.grid_dimensions
        if self.z_axis:
            height += 2
        return (
                f".{self.linkrole['id']} "
                + "{\n  display: grid; \n"
                + f"  grid-template-columns: repeat({width}, minmax(120px, max-content)); \n"
                + f"  grid-template-rows: repeat({height}, minmax(30px, max-content)); \n"
                + f"  margin-bottom: 20px; \n"
                + f"  grid-auto-flow: column dense; \n"
                + "}\n"
        )


class TableAxis(NodeMixin):
    def __init__(self, axis=None, parent=None, parent_child_order=None):
        self.axis = axis
        self.id = axis
        self.parent = parent
        self.parent_child_order = parent_child_order

    @property
    def slices(self):
        """ Slices add a row or a column to the table, they render visibly """
        return [
            node
            for node in self.descendants
            if node.merge is False or node.is_abstract is False
        ]

    @property
    def ordered_slices(self):
        """ 'Same' as slices, but parent-child-order is taken in consideration """
        ordered_slices = []
        for child in self.children:
            ordered_slices.extend(child.ordered_slices)
        return ordered_slices

    @property
    def visible_labels(self):
        """ All labels are shown, except from Merge-rulenodes  """
        return [node for node in self.descendants if node.visible]

    @property
    def visible_height(self):
        try:
            deepest = [
                leave for leave in self.descendants if leave.depth == self.height + 1
            ][0]
            visible_height = 1 if deepest in self.visible_labels else 0
            for parent in deepest.ancestors:
                if parent in self.visible_labels:
                    visible_height += 1
        except IndexError:
            visible_height = 0
        return visible_height

    @property
    def visible_width(self):
        return len(self.slices)

    def build_axis(self, breakdowns=None):
        bd = breakdowns[0]
        self.parent_child_order = bd["parent_child_order"]
        self.proces_node(node=bd, parent=self)
        #  project the other breakdowns (if any) on the slices
        if len(breakdowns) > 1:
            for bd in breakdowns[1:]:
                for slice in self.children[0].slices:
                    slice.is_abstract = True  # we get x-children who will add slices
                    self.proces_node(node=bd, parent=slice)
            print(f"Projected to effective breakdown? {self.axis}")

    def proces_node(self, node, parent):
        for crn in node["concept_relationshipnodes"]:
            self.resolve_concept_relationship_node(parent=parent, crn=crn)
        for drn in node["dimension_relationshipnodes"]:
            self.resolve_dimension_relationship_node(parent=parent, drn=drn)
        for an in node["aspect_nodes"]:
            self.resolve_aspect_node(parent=parent, an=an)
        if node.get("table_rulenodes"):
            for rulenode in node["table_rulenodes"]:
                self.resolve_rulenode(parent=parent, rulenode=rulenode)

    def resolve_rulenode(self, parent, rulenode):
        node = TableNode(
            parent=parent,
            id=rulenode["id"],
            merge=rulenode["merge"],
            is_abstract=rulenode["is_abstract"],
            parent_child_order=rulenode["parent_child_order"],
            rulenode=rulenode,
        )
        if not node.context_heading and len(rulenode["labels"]):
            node.labels = rulenode["labels"]

        self.proces_node(node=rulenode, parent=node)
        if rulenode.get("children"):
            for child in rulenode["children"]:
                self.resolve_rulenode(parent=node, rulenode=child)

    def resolve_concept_relationship_node(self, parent, crn):
        presentation = self.match_presentation(
            qname=crn["relationship_source"], role_uri=crn["linkrole_role_uri"]
        )
        if not presentation.get("children"):
            self.resolve_presentation(
                parent=parent,
                presentation_nodes=[presentation],
                parent_child_order=crn["parent_child_order"]
                if crn["parent_child_order"]
                else parent.parent_child_order,
            )
        else:
            self.resolve_presentation(
                parent=parent,
                presentation_nodes=presentation.get("children"),
                parent_child_order=crn["parent_child_order"]
                if crn["parent_child_order"]
                else parent.parent_child_order,
            )

    def resolve_dimension_relationship_node(self, parent, drn):
        dimension = self.root.match_dimension(
            drn["dimension_id"], linkrole_role_uri=drn["linkrole_role_uri"]
        )
        self.resolve_members(
            parent=parent,
            members=dimension.get("members"),
            parent_child_order=drn["parent_child_order"]
            if drn["parent_child_order"]
            else parent.parent_child_order,
        )

    def resolve_aspect_node(self, parent, an):
        if an["aspect_type"] == "dimension":
            dimension = self.root.match_dimension(
                qname=an["value"], linkrole_role_uri=None
            )
            if dimension:
                for member in dimension["members"]:
                    node = TableNode(
                        parent=parent,
                        id=an["id"],
                        is_abstract=member["concept"]["abstract"],
                        concept=member["concept"],
                        aspect=dimension,
                        labels=dimension["concept"]["labels"],
                    )
                if self.axis == "x":
                    self.root.x_aspects = True
                elif self.axis == "y":
                    self.root.y_aspects = True

    def resolve_members(self, parent, members, parent_child_order=None):
        if members:
            for member in members:
                if member["concept"]["substitution_group"] in [
                    "sbr:domainMemberItem",
                    "xbrli:item",
                ]:
                    is_abstract = False
                else:
                    is_abstract = member["concept"]["abstract"]
                node = TableNode(
                    parent=parent,
                    id=member["concept"]["id"],
                    labels=member["concept"]["labels"],
                    concept=member["concept"],
                    preferred_label=member["preferred_label"],
                    is_abstract=is_abstract,
                    parent_child_order=parent_child_order,
                )
                if member.get("members"):
                    self.resolve_members(
                        parent=node,
                        members=member["members"],
                        parent_child_order=parent_child_order,
                    )

    def resolve_presentation(self, parent, presentation_nodes, parent_child_order):
        if presentation_nodes:
            for presentation_node in presentation_nodes:
                node = TableNode(
                    parent=parent,
                    id=presentation_node["id"],
                    is_abstract=presentation_node["concept"]["abstract"]
                    if presentation_node["concept"].get("itemtype_name", None)
                    else True,  # BD has abstract: False, itemtype_name: None.
                    concept=presentation_node["concept"],
                    preferred_label=presentation_node["preferred_label"],
                    parent_child_order=parent_child_order,
                    labels=presentation_node["concept"]["labels"],
                )
                if presentation_node.get("children"):
                    self.resolve_presentation(
                        parent=node,
                        presentation_nodes=presentation_node["children"],
                        parent_child_order=parent_child_order,
                    )

    def match_presentation(self, qname, role_uri):
        if (
                self.parent.linkrole.get("presentation_hierarchy")
                and f"{self.parent.linkrole['presentation_hierarchy']['concept']['ns_prefix']}:{self.parent.linkrole['presentation_hierarchy']['concept']['name']}"
                == qname
        ):
            return self.parent.linkrole["presentation_hierarchy"]
        elif self.parent.linkrole.get("presentation_hierarchy"):
            for top_node in self.parent.linkrole["presentation_hierarchy"]["children"]:
                if (
                        f"{top_node['concept']['ns_prefix']}:{top_node['concept']['name']}"
                        == qname
                ):
                    return top_node
        alt_presentation = PresentationHierarchies(db=self.parent.db, full=True)
        alt_presentation.alternative_presentation(
            dts_name=self.parent.dts_name, qname=qname, role_uri=role_uri
        )
        if len(alt_presentation.presentation_hierarchies):
            return alt_presentation.presentation_hierarchies[0]

    def __repr__(self):
        return f"{self.axis.upper()}-Axis {self.parent_child_order}"


class TableNode(NodeMixin):
    def __init__(
            self,
            parent=None,
            id=None,
            is_abstract=None,
            merge=None,
            labels=None,
            concept=None,
            aspect=None,
            rulenode=None,
            parent_child_order=None,
            preferred_label=None,
    ):
        self.id = id
        self.parent = parent
        self.is_abstract = is_abstract
        self.merge = merge
        self.preferred_label = preferred_label
        self.parent_child_order = parent_child_order
        self.__labels = labels
        self.concept = concept
        self.aspect = aspect
        self.rulenode = rulenode

    @property
    def slices(self) -> list:
        slices = (
            [self]
            if (self.merge is False and not len(self.descendants))
               or self.is_abstract is False
            else []
        )
        slices.extend(
            [
                node
                for node in self.descendants
                if (node.merge is False and not len(node.descendants))
                   or node.is_abstract is False
            ]
        )
        return slices

    @property
    def ordered_slices(self) -> list:
        # slow method, looks at parent_child_order to determinethe position in the list:
        def get_child_slices(node):
            slices = []
            if node.children:
                for child in node.children:
                    if child.parent_child_order == "children-first":
                        slices.extend(get_child_slices(child))
                    if (
                            child.merge is False and child.is_leaf
                    ) or child.is_abstract is False:
                        slices.append(child)
                    if child.parent_child_order == "parent-first":
                        slices.extend(get_child_slices(child))
            return slices

        ordered_slices = []
        if self.parent_child_order == "children-first":
            ordered_slices.extend(get_child_slices(self))
        if (self.merge is False and self.is_leaf) or self.is_abstract is False:
            ordered_slices.append(self)
        if self.parent_child_order == "parent-first":
            ordered_slices.extend(get_child_slices(self))

        return ordered_slices

    @property
    def visible_width(self) -> int:
        return len(self.slices)

    @property
    def visible(self) -> bool:
        visible = True
        if self.merge is False:
            if not self.context_heading and not self.labels:
                visible = False
        elif self.merge:
            visible = False
        return visible

    @property
    def axis(self) -> str:
        def find_axis(node):
            if hasattr(node, "axis"):
                return node.axis
            else:
                find_axis(node.parent)

        return find_axis(self.parent)

    @property
    def axis_height(self) -> int:
        height = 1
        for axis in self.root.children:
            if axis.axis == self.axis:
                height = axis.visible_height
        return height

    @property
    def added_aspect(self):
        """ This is turning out to be a mini-resolver on self, or the great question: 'What am I?'"""
        added_aspect = {}
        if self.aspect:
            added_aspect["type"] = f"typed dimension: {self.aspect['concept']['name']}"
            added_aspect["value"] = self.get_eng_label(self.aspect["concept"]["labels"])
        elif self.concept:
            added_aspect["type"] = "concept"
            added_aspect["value"] = (
                self.get_eng_label(self.concept["labels"])
                if self.concept.get("labels")
                else "unknown"
            )
        elif self.rulenode:
            if len(self.rulenode["rulesets"]):
                for ruleset in self.rulenode["rulesets"]:
                    added_aspect["type"] = f"{ruleset['tag']} (ruleSet)"
                    for fp in ruleset["formula_periods"]:
                        if fp["period_type"] == ruleset["tag"]:
                            value = ""
                            if fp["start"]:
                                value += f"start={fp['start']}"
                            if fp["end"]:
                                value += f" end={fp['end']}"
                            if fp["value"]:
                                value += f" value={fp['value']}"
                            added_aspect["value"] = value
                    for fep in ruleset["formula_explicit_dimensions"]:
                        added_aspect["type"] = "ruleset expl. d."
                        print("fep")
                    for ftp in ruleset["formula_typed_dimensions"]:
                        added_aspect["type"] = "ruleset typed. d."
                        print(f"ftp")

            if len(self.rulenode["formula_periods"]):
                for fp in self.rulenode["formula_periods"]:
                    added_aspect["type"] = f"period"
                    value = ""
                    if fp["start"]:
                        value += f"start={fp['start']}"
                    if fp["end"]:
                        value += f" end={fp['end']}"
                    if fp["value"]:
                        value += f" value={fp['value']}"
                    added_aspect["value"] = value
            if len(self.rulenode["formula_typed_dimensions"]):
                for fd in self.rulenode["formula_typed_dimensions"]:
                    added_aspect["type"] = f"typed dimension: "
            if len(self.rulenode["formula_explicit_dimensions"]):
                for fd in self.rulenode["formula_explicit_dimensions"]:
                    if fd.get("qname_dimension"):
                        dim = self.root.match_dimension(fd.get("qname_dimension"))
                        if dim:
                            qname_dimension = self.get_eng_label(
                                dim["concept"]["labels"]
                            )
                        else:
                            qname_dimension = "@"
                    else:
                        qname_dimension = "~"
                    if fd.get("qname_member"):
                        member = self.root.match_member(fd.get("qname_member"))
                        if member:
                            qname_member = self.get_eng_label(
                                member["concept"]["labels"]
                            )
                        elif fd.get("qname_dimension"):
                            dim = self.root.match_dimension(fd.get("qname_dimension"))
                            if dim and len(dim["members"]):
                                qname_member = self.get_eng_label(
                                    dim["members"][0]["concept"]["labels"]
                                )
                            else:
                                qname_member = "@"
                    else:
                        qname_member = "~"

                    qname_member_expression = (
                        fd.get("qname_member_expression")
                        if fd.get("qname_member_expression")
                        else "~"
                    )
                    added_aspect["type"] = f"explicit dimension: "
                    added_aspect[
                        "value"
                    ] = f"{qname_dimension} - {qname_member} - {qname_member_expression}"
            if not added_aspect.get("type"):
                return None  # no formula attached to this node, let's skip it
        if not (self.concept or self.aspect or self.rulenode):
            if self.id.startswith("presentation_fact"):
                return None  # this is a simulation
            added_aspect["type"] = "Todo"
            print("todo: added aspect ?")
        added_aspect["axis"] = self.axis
        return added_aspect

    @property
    def labels(self) -> list:
        if self.__labels:
            return self.__labels
        elif self.rulenode:
            if self.rulenode["formula_explicit_dimensions"]:
                return self.formula_explicit_dimension_label(
                    formula=self.rulenode["formula_explicit_dimensions"][0]
                )

    @labels.setter
    def labels(self, labels: dict):
        self.__labels = labels

    def get_eng_label(self, labels):
        for label in labels:
            if (
                    label["link_role"] == "http://www.xbrl.org/2003/role/label"
                    or label["link_role"] == "http://www.xbrl.org/2008/role/label"
            ) and label["lang"] == "en":
                return label["label_text"]
        for label in labels:
            if (
                    label["link_role"] == "http://www.xbrl.org/2003/role/label"
                    or label["link_role"] == "http://www.xbrl.org/2008/role/label"
            ) and label["lang"] == "nl":
                return label["label_text"]
        return None

    @property
    def context_heading(self) -> str:
        if self.rulenode:
            if self.rulenode["labels"]:
                dynamic_label = self.get_dynamic_label(node=self.rulenode)
                if (
                        dynamic_label
                ):  # Returns None if no message found, keep the ruleset dates?
                    return dynamic_label
            if self.rulenode["formula_periods"]:
                return self.formula_period_label(
                    formula=self.rulenode["formula_periods"][0]
                )
        return None

    def css(self, id, row, column, row_end, column_end) -> str:
        style = (
                f".{self.root.linkrole['id']} .{id} "
                + "{\ngrid-area: "
                + f" {row} /  {column}"
                + f" / {row_end} / {column_end}; \n"
                + "border: 1px solid black;\n"
                + "box-sizing: border-box;\n"
                + "height: 100%;\n"
                + "min-height: 30px; \n"
                + "padding: 5px;\n"
                + "}\n"
        )
        return style

    def formula_period_label(self, formula):
        label = f"searching formulas {formula['period_type']}"
        if formula["period_type"] == "instant":
            label = self.match_parameter(formula["value"])
        elif formula["period_type"] == "duration":
            label = (
                    self.match_parameter(formula["start"])
                    + " - "
                    + self.match_parameter(formula["end"])
            )
            year = re.findall(r"(\d\d\d\d)-01-01 - \1-12-31", label)
            if len(year):
                label = year[0]
        return label

    def formula_explicit_dimension_label(self, formula):
        qname = formula.get("qname_dimension")
        alt_dimensions = Dimensions(db=self.root.db, full=True)
        alt_dimensions.alternative_dimensions(dts_name=self.root.dts_name, qname=qname)
        if len(alt_dimensions.dimensions):
            dimension = alt_dimensions.dimensions[0]
            labels = dimension["concept"]["labels"]
            qname = formula.get("qname_member")
            if qname:
                for member in dimension["members"]:
                    if (
                            f'{member["concept"]["ns_prefix"]}:{member["concept"]["name"]}'
                            == qname
                    ):
                        labels = member["concept"]["labels"]
            return labels
        return "~"

    def get_dynamic_label(self, node):
        for label in node["labels"]:
            if label["link_role"] == "http://www.xbrl.org/2010/role/message":
                # at the moment we don't care about languagae!
                tokens = label["label_text"].split(" ")
                retval = ""
                for token in tokens:
                    if "$" in token:
                        token = self.match_parameter(
                            token.replace("{", "").replace("}", "")
                        )
                    retval += token + " "
                return retval
        return False

    def match_parameter(self, parameter_id):
        # todo: implement xpath2 parser and find the required values
        for parameter in self.root.table["parameters"]:
            if f"${parameter['id']}" == parameter_id:
                sel = parameter["select"].split("else")[-1]
                return (
                    sel.strip()
                    .replace("xs:date(", "")
                    .replace(")", "")
                    .replace("'", "")
                )
        if "xs:date" in parameter_id:
            return (
                parameter_id.strip()
                .replace("xs:date(", "")
                .replace(")", "")
                .replace("'", "")
            )
        return "parameter mismatch"

    def __repr__(self):
        return f"{self.id} {self.parent_child_order} merge: {self.merge} abstr: {self.is_abstract} "


def test_table(db=None, entry=None):
    table = TableTree(db=db)
    dts_name, linkrole_id = entry
    table.build_table_tree(dts_name=dts_name, linkrole_id=linkrole_id)
    for child in table.children:
        print(child.visible_height)
    print(RenderTree(table))
    print(table.render_html["divs"])
    # print(table.render_html["css"])


if __name__ == "__main__":
    from discover_xbrl.conf.conf import Config

    Config()
    db = get_connection()
    tables = [
        # (
        #     "ocw-rpt-wet-normering-topinkomens-2021.xsd",
        #     "kvk-lr_NotesRemunerationNonSupervisorySeniorOfficialsOtherWntInstitutionSpecification"
        # ),
        (
            "ocw-rpt-jaarverantwoording-2021-nlgaap-onderwijsinstellingen.xsd",
            "kvk-lr_NotesConsolidatedIntangibleAssetsIndividualAssetOfImportanceSpecification",
        ),
        # (
        #     "ocw-rpt-beleidsinformatie-2021.xsd",
        #     "ocw-lr-o_SocialThemesCovenantResourcesAccountabilityEducation"
        # ),
        # (
        #      "kvk-rpt-jaarverantwoording-2021-nlgaap-toegelaten-instellingen-volkshuisvesting.xsd",
        #      "kvk-lr-ti_ConsolidatedBalanceSheetHousing",
        # ),
        # (
        #     "kvk-rpt-jaarverantwoording-2021-nlgaap-klein.xsd",
        #     "kvk-lr_NotesConsolidatedGeneralNotesConsolidatedInterestsOneFifthInterestSpecification"
        # ),
        # (
        #       "kvk-rpt-jaarverantwoording-2021-nlgaap-klein.xsd",
        #       "kvk-lr_NotesLoansAdvancesGuaranteesManagingDirectorsSpecification"
        # ),
        # (
        #      "kvk-rpt-jaarverantwoording-2021-nlgaap-toegelaten-instellingen-volkshuisvesting.xsd",
        #      "kvk-lr-ti_NotesCashFlowStatementBreakdownHousing",
        # ),
        # (
        #     "kvk-rpt-jaarverantwoording-2021-nlgaap-groot.xsd",
        #     "kvk-lr_NotesIntangibleAssetsInternalExternalGeneratedMovement",
        # ),
        # (
        #       "kvk-rpt-jaarverantwoording-2021-nlgaap-groot.xsd",
        #       "kvk-lr_NotesConsolidatedGeneralAccountingPrinciplesPriorPeriodErrors",
        # ),
        # (
        #     "kvk-rpt-jaarverantwoording-2021-nlgaap-toegelaten-instellingen-volkshuisvesting.xsd",
        #     "kvk-lr-ti_NotesConsolidatedLandPositionsAreaMovementHousing",
        # ),
        # (
        #     "kvk-rpt-jaarverantwoording-2021-nlgaap-micro.xsd",
        #     "kvk-lr_BalanceSheetMicroEntities",
        # ),
        # (
        #     "kvk-rpt-jaarverantwoording-2021-nlgaap-klein.xsd",
        #     "kvk-lr_NotesConsolidatedIntangibleAssetsIndividualAssetOfImportanceSpecification"
        # ),
        # (
        #     "kvk-rpt-jaarverantwoording-2021-nlgaap-cv-vof.xsd",
        #     "kvk-lr_NotesConsolidatedIntangibleAssetsIntangibleAssetsOtherSpecification",
        # ),
        # (
        #     "abm-rpt-aedes-benchmark.xsd",
        #     "bzk-dvi-lr_DataCorporationHousingDvi"
        # )
    ]
    for entry in tables:
        test_table(db=db, entry=entry)
