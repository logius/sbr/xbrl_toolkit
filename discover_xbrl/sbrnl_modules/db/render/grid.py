class Grid:
    def __init__(self, name=None):
        self.tag = "div"
        self.name = name
        self.id = name
        self.rows = 0
        self.labels = 0
        self.width = 0
        self.context = []
        self.cells = []
        self.grid_rows = []

    @property
    def css(self):
        rows = self.rows - 1 if not len(self.context) else self.rows
        style = (
            f".{self.name} "
            + "{\n  display: grid; \n"
            + f"  grid-template-columns: repeat({self.width}, minmax(120px, max-content)); \n"
            + f"  grid-template-rows: repeat({rows}, minmax(30px, max-content)); \n"
            + f"  margin-bottom: 20px; \n"
            + f"  grid-auto-flow: column dense; \n"
            + "}\n"
        )

        for cell in self.context + self.cells:
            style += f".{self.name} {cell.css}"
        return style

    @property
    def divs(self):
        div = self.__dict__.copy()
        del div["cells"]
        del div["grid_rows"]
        del div["context"]
        div["children"] = [cell.div for cell in self.context + self.cells]
        return div

    @property
    def api_response(self):
        # for Vue technical reasons -we can't add style or script inside templates-
        # we add some nast html-style-tags ourselves ...
        response = {"css": f"<style>{self.css}</style>", "divs": self.divs}
        return response

    def add_row(self, row=None, move_pointer=True):
        if move_pointer:
            if row:
                self.rows = row
            else:
                self.rows += 1
        if not row:
            row = self.rows
        self.ensure_row(row)

    def get_row_start(self, row):
        for grid_row in self.grid_rows:
            if grid_row.row == row:
                return grid_row.start_column
        return 1

    def move_row_start(self, row, columns):
        for grid_row in self.grid_rows:
            if grid_row.row == row:
                grid_row.start_column += columns
                break

    def insert_column(self, position, cell):
        for old_cell in self.cells:
            if old_cell.row == cell.row and old_cell.column > position:
                old_cell.column += 1
                old_cell.column_end += 1
        self.cells.insert(position, cell)

    def get_free_column(self, row):
        return len([c for c in self.cells if c.row == row]) + self.get_row_start(
            row=row
        )

    def add_context(self, start_col=None):
        row = 0
        return self.ensure_row(row)

    def ensure_row(self, row):
        found = False
        for grid_row in self.grid_rows:
            if grid_row.row == row:
                found = True
                break
        if not found:
            grid_row = GridRow(row=row)
            self.grid_rows.append(grid_row)
            return True
        return False


class GridRow:
    def __init__(self, row, column=None):
        self.row = row
        self.start_column = column if column else 1
        self.cells = []
