class GridCell:
    def __init__(
        self,
        id=None,
        row=None,
        column=None,
        row_end=None,
        column_end=None,
        type=None,
        preferred_label=None,
        parent_child_order=None,
        rulenode=None,
        concept=None,
        aspect=None,
        dimension=None,
        member=None,
        labels=None,
        children_first_css=False,
        contaxt_heading=None,
        restructure=None,
    ):
        self.tag = "div"
        self.id = id.replace(".", "_")
        self.row = row
        self.column = column
        self.row_end = row_end if row_end else row + 1
        self.column_end = column_end if column_end else column + 1
        self.type = type
        self.preferred_label = preferred_label
        self.parent_child_order = parent_child_order
        self.rulenode = rulenode
        self.concept = concept
        self.aspect = aspect
        self.dimension = dimension
        self.member = member
        self.labels = labels
        self.children_first_css = children_first_css
        self.context_heading = contaxt_heading
        self.restructure = restructure

    @property
    def css(self):
        style = (
            f".{self.id} "
            + "{\ngrid-area: "
            + f" {self.row} /  {self.column}"
            + f" / {self.row_end} / {self.column_end}; \n"
            + "border: 1px solid black;\n"
            + "box-sizing: border-box;\n"
            + "height: 100%;\n"
            + "min-height: 30px; \n"
            + "padding: 5px;\n"
            + "}\n"
        )
        # if self.type == 'fact_heading':
        #    style += f".{self.id} " + "{background-color: silver;}"
        return style

    @property
    def div(self):
        return self.__dict__

    def __repr__(self):
        return (
            f"'<Gridcell> {self.type} id: {self.id} grid position: {self.row} / {self.column} /"
            f" {self.row_end} / {self.column_end}'"
        )
