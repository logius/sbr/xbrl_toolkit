class RenderTableTree:
    def __init__(self, table_tree=None, swap_axis=False):
        self.tree = table_tree
        self.swap_axis = swap_axis
        if not swap_axis:
            self.x = "x"
            self.y = "y"
            self.grid_width, self.grid_height = self.tree.grid_dimensions
        else:
            self.x = "y"
            self.y = "x"
            self.grid_height, self.grid_width = self.tree.grid_dimensions
        self.style = ""
        self.html = {}

    def render(self):
        if self.tree:
            self.style += self.tree.css_grid()
            self.html = {
                "tag": "div",
                "name": self.tree.linkrole["id"],
                "id": self.tree.linkrole["id"],
                "rows": self.grid_height,
                "width": self.grid_width,
                "labels": self.tree.table["labels"] if self.tree.table else [],
                "children": [],
            }
            for axis in self.tree.children:
                if axis.axis == self.x:
                    self.parse_header(axis)
            for axis in self.tree.children:
                if axis.axis == self.y:
                    self.parse_labels(axis)
            self.parse_body()

            for axis in self.tree.children:
                if axis.axis == "z":
                    self.parse_z_axis(axis)

            return {"css": f"<style>{self.style}</style>", "divs": self.html}

    def parse_header(self, axis):
        # Header is the logical X direction, so we build left-right
        # print(
        #     f"X axis_height: {axis.visible_height}, axis_width: {axis.visible_width} grid: {self.tree.grid_dimensions}"
        # )
        column = (
            self.grid_width + 1 - axis.visible_width
        )  # the difference is the width of the other axis' labels
        row = 1
        self.parse_subtree(axis.children, row=row, column=column)

    def parse_labels(self, axis):
        # Labels is the logical Y direction, we build top down
        # print(
        #     f"Y axis_height: {axis.visible_height}, axis_width: {axis.visible_width} grid: {self.tree.grid_dimensions}"
        # )
        column = 1
        row = self.grid_height + 1 - axis.visible_width
        self.parse_label_subtree(subtree=axis.children, row=row, column=column)

    def parse_subtree(self, subtree, row, column):
        start = column
        new_row = row
        for node in subtree:
            if node.visible:
                if node.context_heading:
                    div_type = "context_heading"
                else:
                    div_type = "label" if node.axis == "y" else "heading"

                column_end = column + node.visible_width
                row_end = node.axis_height + 1
                div = {
                    "tag": "div",
                    "id": f"{node.id.replace('.', '_')}_{row}_{column}",
                    "type": div_type,
                    "preferred_label": node.preferred_label,
                    "parent_child_order": node.parent_child_order,
                    "concept": node.concept,
                    "context_heading": node.context_heading,
                    "labels": node.labels if node.labels else [],
                }
                self.html["children"].append(div)
                self.style += node.css(
                    id=f"{node.id.replace('.', '_')}_{row}_{column}",
                    row=row,
                    column=column,
                    row_end=row_end,
                    column_end=column_end,
                )
                column = column_end
                new_row = row + 1

        column = start
        width = 0
        for node in subtree:
            column += width
            width = len(node.slices)
            if node.children:
                self.parse_subtree(subtree=node.children, row=new_row, column=column)

    def parse_label_subtree(self, subtree, row, column):
        start = row
        new_column = column
        for node in subtree:
            if node.visible:
                if node.context_heading:
                    div_type = "context_heading"
                else:
                    div_type = "label"
                if node.aspect:
                    row -= 1
                column_end = node.axis_height + 1 if node in node.slices else column + 1
                row_end = row + len(node.slices)
                div = {
                    "tag": "div",
                    "id": f"{node.id.replace('.', '_')}_{row}_{column}",
                    "type": div_type,
                    "preferred_label": node.preferred_label,
                    "parent_child_order": node.parent_child_order,
                    "concept": node.concept,
                    "context_heading": node.context_heading,
                    "labels": node.labels,
                }
                if (
                    node.parent_child_order == "children-first"
                    and len(node.children) > 0
                ):
                    div["children_first_css"] = True

                self.html["children"].append(div)
                self.style += node.css(
                    id=f"{node.id.replace('.', '_')}_{row}_{column}",
                    row=row,
                    column=column,
                    row_end=row_end,
                    column_end=column_end,
                )
                row = row_end
                if node.aspect:
                    row += 1
                new_column = column + 1

        row = start
        width = 0
        for node in subtree:
            row += width
            width = len(node.slices)
            if node.children:
                if node in node.slices and node.parent_child_order == "parent-first":
                    row += 1
                self.parse_label_subtree(
                    subtree=node.children, row=row, column=new_column
                )

    def parse_body(self):
        x_axis = [axis for axis in self.tree.children if axis.axis == self.x][0]
        y_axis = [axis for axis in self.tree.children if axis.axis == self.y][0]

        aspect_done = False
        _col = y_axis.visible_height
        if y_axis.root.y_aspects:
            _col = 0
        for column in x_axis.ordered_slices:
            _col += 1
            _row = x_axis.visible_height
            for row in y_axis.ordered_slices:
                if not row.concept:
                    row.concept = {"id": "some_rulenode"}
                _row += 1
                if row.aspect and not aspect_done:  # we have a raised label
                    aspects = self.get_aspects(column)
                    aspects.extend(self.get_aspects(row))
                    div = {
                        "tag": "div",
                        "id": f"fact_{_row}_{_col}_{row.concept['id']}",
                        "type": "value",
                        "concept": row.concept,
                        "aspects": aspects,
                    }
                    self.html["children"].append(div)
                    self.style += row.css(
                        id=f"fact_{_row}_{_col}_{row.concept['id']}",
                        row=_row,
                        column=_col,
                        row_end=_row + 1,
                        column_end=_col + 1,
                    )
                    _col += 1
                    aspect_done = True
                aspects = self.get_aspects(column)
                aspects.extend(self.get_aspects(row))

                div = {
                    "tag": "div",
                    "id": f"fact_{_row}_{_col}_{row.concept['id']}",
                    "type": "value",
                    "concept": row.concept if not row.aspect else column.concept,
                    # "labels": node.labels,
                    "aspects": aspects,
                }
                self.html["children"].append(div)
                self.style += row.css(
                    id=f"fact_{_row}_{_col}_{row.concept['id']}",
                    row=_row,
                    column=_col,
                    row_end=_row + 1,
                    column_end=_col + 1,
                )

    def parse_z_axis(self, z_axis):
        div = {"id": "z_axis", "tag": "div", "type": "z_divider"}
        if z_axis.children:
            node = z_axis.children[0]  # i need a node for the CSS
            width, height = z_axis.root.grid_dimensions
            self.html["children"].append(div)
            self.style += node.css(
                id="z_axis",
                row=height + 1,
                column=1,
                row_end=height + 2,
                column_end=width + 1,
            )
            for child in z_axis.ordered_slices:
                div = {
                    "tag": "div",
                    "id": f"z_{child.concept['id']}",
                    "type": "label",
                    "concept": child.concept,
                    "labels": child.concept["labels"],
                }
                self.html["children"].append(div)
                self.style += child.css(
                    id=f"z_{child.concept['id']}",
                    row=height + 2,
                    column=1,
                    row_end=height + 3,
                    column_end=width,
                )
                aspects = self.get_aspects(child)
                div = {
                    "tag": "div",
                    "id": f"z_{child.concept['id']}_value",
                    "type": "value",
                    "concept": child.concept,
                    "aspects": aspects,
                }
                self.html["children"].append(div)
                self.style += child.css(
                    id=f"z_{child.concept['id']}_value",
                    row=height + 2,
                    column=width,
                    row_end=height + 3,
                    column_end=width + 1,
                )

    def get_aspects(self, slice):
        aspects = []
        if hasattr(slice, "added_aspect") and slice.added_aspect:
            aspects.append(slice.added_aspect)
        for ancestor in slice.ancestors:
            if hasattr(ancestor, "added_aspect") and ancestor.added_aspect:
                if ancestor.added_aspect not in aspects:
                    aspects.append(ancestor.added_aspect)
        return aspects

    def render_too_big(self, dimensions):
        self.style = ".z_axis {background-color: yellow}"
        self.html = {
            "tag": "div",
            "id": "z_axis",
            "type": "z_divider",
            "children": [],
            "rows": 1,
            "width": 1,
        }
        labels = [
            {
                "id": "error_1",
                "lang": "en",
                "label_text": f"Table too big to render {dimensions[0]}x{dimensions[1]}",
                "link_role": "http://www.xbrl.org/2003/role/label",
                "linnk_label": "err_en",
            },
            {
                "id": "error_2",
                "lang": "nl",
                "label_text": f"Tabel te groot om te renderen {dimensions[0]}x{dimensions[1]}",
                "link_role": "http://www.xbrl.org/2003/role/label",
                "linnk_label": "err_nl",
            },
        ]
        div = {"tag": "div", "id": "error", "type": "label", "labels": labels}
        self.html["labels"] = labels
        self.html["children"].append(div)
        return {"css": f"<style>{self.style}</style>", "divs": self.html}
