class Filters:
    def __init__(self, db=None, full=True):
        self.db = db
        self.filters = []
        self.full = full
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def filters_of_variable(self, variable_id=None):
        if not variable_id:
            return False
        query = self.db.rewrite_query(
            "select id, label, type, aspect, include_qname, exclude_qname, qname, qname_expression, period_type, "
            "value, qname_member, qname_dimension, linkrole_role_uri, arcrole, axis, balance_type, strict, "
            "type_qname, type_qname_expression, test, boundary, date, time, parent, variable,"
            "arc_complement, arc_priority, arc_cover, arc_order "
            "from filter "
            "left join variable_filter on id = filter_id "
            "where variable_id = %s "
        )
        self.cursor.execute(query, (variable_id,))
        rows = self.cursor.fetchall()
        if len(rows):
            self.cache_rows(rows=rows)
        if self.full:
            self.get_relations()

    def filters_of_table(self, table_id=None):
        if not table_id:
            return False
        query = self.db.rewrite_query(
            "select id, label, type, aspect, include_qname, exclude_qname, qname, qname_expression, period_type, "
            "value, qname_member, qname_dimension, linkrole_role_uri, arcrole, axis, balance_type, strict, "
            "type_qname, type_qname_expression, test, boundary, date, time, parent, variable,"
            "arc_complement, arc_priority, arc_cover, arc_order "
            "from filter "
            "left join table_filter tf on filter.id = tf.filter_id "
            "where tf.table_id = %s "
        )
        self.cursor.execute(query, (table_id,))
        rows = self.cursor.fetchall()
        if len(rows):
            self.cache_rows(rows=rows)
        if self.full:
            self.get_relations()

    def subfilters_of_filter(self, label=None):
        if not label:
            return False
        query = self.db.rewrite_query(
            "select id, label, type, aspect, include_qname, exclude_qname, qname, qname_expression, period_type, "
            "value, qname_member, qname_dimension, linkrole_role_uri, arcrole, axis, balance_type, strict, "
            "type_qname, type_qname_expression, test, boundary, date, time, parent, variable,"
            "arc_complement, arc_priority, arc_cover, arc_order "
            "from filter where parent = %s "
        )
        self.cursor.execute(query, (label,))
        rows = self.cursor.fetchall()
        if len(rows):
            self.cache_rows(rows=rows)

    def cache_rows(self, rows=None):
        for row in rows:
            self.filters.append(
                {
                    "id": row[0],
                    "label": row[1],
                    "type": row[2],
                    "aspect": row[3],
                    "include_qname": row[4],
                    "exclude_qname": row[5],
                    "qname": row[6],
                    "qname_expression": row[7],
                    "period_type": row[8],
                    "value": row[9],
                    "qname_member": row[10],
                    "qname_dimension": row[11],
                    "linkrole_role_uri": row[12],
                    "arcrole": row[13],
                    "axis": row[14],
                    "balance_type": row[15],
                    "strict": row[16],
                    "type_qname": row[17],
                    "type_qname_expression": row[18],
                    "test": row[19],
                    "boundary": row[20],
                    "date": row[21],
                    "time": row[22],
                    "parent": row[23],
                    "variable": row[24],
                    "arc_complement": row[25],
                    "arc_priority": row[26],
                    "arc_cover": row[27],
                    "arc_order": row[28],
                }
            )

    def get_relations(self):
        for filter in self.filters:
            subfilters = Filters(db=self.db, full=True)
            subfilters.subfilters_of_filter(label=filter["label"])
            if len(subfilters.filters):
                filter["subfilters"] = subfilters.filters
