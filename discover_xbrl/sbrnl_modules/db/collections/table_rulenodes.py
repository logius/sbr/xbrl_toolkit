from discover_xbrl.sbrnl_modules.db.collections.aspect_nodes import AspectNodes
from discover_xbrl.sbrnl_modules.db.collections.concept_relationshipnodes import (
    ConceptRelationshipnodes,
)
from discover_xbrl.sbrnl_modules.db.collections.dimension_relationshipnodes import (
    DimensionRelationshipnodes,
)
from discover_xbrl.sbrnl_modules.db.collections.formula_periods import FormulaPeriods
from discover_xbrl.sbrnl_modules.db.collections.formula_typed_dimensions import (
    FormulaTypedDimensions,
)
from discover_xbrl.sbrnl_modules.db.collections.formula_explicitdimensions import (
    FormulaExplicitDimensions,
)
from discover_xbrl.sbrnl_modules.db.collections.labels import Labels
from discover_xbrl.sbrnl_modules.db.collections.rulesets import Rulesets


class TableRulenodes:
    def __init__(self, db=None, full=True):
        self.db = db
        self.full = full
        self.table_rulenodes = []
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def table_rulenodes_of_parent(self, parent_id=None):
        if not parent_id:
            return False
        query = self.db.rewrite_query(
            "select id, label, parent_child_order, merge, _order, is_abstract, parent, tagselector "
            "from table_rulenode where parent = %s "
        )
        self.cursor.execute(query, (parent_id,))
        rows = self.cursor.fetchall()
        for row in rows:
            self.table_rulenodes.append(
                {
                    "id": row[0],
                    "label": row[1],
                    "parent_child_order": row[2],
                    "merge": True if row[3] else False,
                    "order": row[4],
                    "is_abstract": True if row[5] else False,
                    "parent": row[6],
                    "tagselector": row[7],
                }
            )
        if self.full:
            self.get_relations()

    def get_children(self, parent_id=None):
        childnode = TableRulenodes(db=self.db, full=self.full)
        childnode.table_rulenodes_of_parent(parent_id=parent_id)
        return childnode.table_rulenodes

    def get_relations(self):
        for rulenode in self.table_rulenodes:
            formula_periods = FormulaPeriods(db=self.db, full=self.full)
            formula_periods.formula_periods_of_rulenode(parent_rulenode=rulenode["id"])
            rulenode["formula_periods"] = formula_periods.formula_periods

            formula_typed_dimension = FormulaTypedDimensions(db=self.db, full=self.full)
            formula_typed_dimension.formula_typed_dimensions_of_rulenode(
                parent_node=rulenode["id"]
            )
            rulenode[
                "formula_typed_dimensions"
            ] = formula_typed_dimension.formula_typed_dimensions

            formula_explicit_dimensions = FormulaExplicitDimensions(
                db=self.db, full=self.full
            )
            formula_explicit_dimensions.formula_explicit_dimensions_of_rulenode(
                parent_node=rulenode["id"]
            )
            rulenode[
                "formula_explicit_dimensions"
            ] = formula_explicit_dimensions.formula_explicit_dimensions

            rulesets = Rulesets(db=self.db, full=self.full)
            rulesets.rulesets_of_rulenode(rulenode_id=rulenode["id"])
            rulenode["rulesets"] = rulesets.rulesets

            labels = Labels(db=self.db)
            labels.labels_of_table_rulenode(rulenode["id"])
            rulenode["labels"] = labels.labels

            children = self.get_children(parent_id=rulenode["id"])
            if len(children):
                rulenode["children"] = children

            aspect_nodes = AspectNodes(db=self.db, full=self.full)
            aspect_nodes.aspect_nodes_of_parent(parent_id=rulenode["id"])
            rulenode["aspect_nodes"] = aspect_nodes.aspect_nodes

            concept_relationshipnodes = ConceptRelationshipnodes(
                db=self.db, full=self.full
            )
            concept_relationshipnodes.concept_relationshipnodes_of_parent(
                parent_id=rulenode["id"]
            )
            rulenode[
                "concept_relationshipnodes"
            ] = concept_relationshipnodes.concept_relationshipnodes

            dimension_relationshipnodes = DimensionRelationshipnodes(
                db=self.db, full=self.full
            )
            dimension_relationshipnodes.dimension_relationshipnodes_of_parent(
                parent_id=rulenode["id"]
            )
            rulenode[
                "dimension_relationshipnodes"
            ] = dimension_relationshipnodes.dimension_relationshipnodes
