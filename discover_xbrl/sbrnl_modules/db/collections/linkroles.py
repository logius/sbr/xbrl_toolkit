from discover_xbrl.sbrnl_modules.db.collections.assertions import Assertions
from discover_xbrl.sbrnl_modules.db.collections.dimensions import Dimensions
from discover_xbrl.sbrnl_modules.db.collections.hypercubes import HyperCubes
from discover_xbrl.sbrnl_modules.db.collections.labels import Labels
from discover_xbrl.sbrnl_modules.db.collections.presentation_hierarchies import (
    PresentationHierarchies,
)
from discover_xbrl.sbrnl_modules.db.collections.references import References
from discover_xbrl.sbrnl_modules.db.collections.tables import Tables
from discover_xbrl.sbrnl_modules.db.collections.used_ons import UsedOns
from discover_xbrl.sbrnl_modules.db.collections.variables import Variables


class Linkroles:
    def __init__(self, db=None, full=True):
        self.db = db
        self.full = full
        self.linkroles = []
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def linkroles_from_dts(self, dts_name=None, referenced_only=True):
        # die 'And definition > '', dat is dus Nessie specifiek :(
        if dts_name is None:
            return False
        if referenced_only:
            query = self.db.rewrite_query(
                "select id, role_uri, definition, dts_name, rowid, _order, directly_referenced "
                "from linkrole where linkrole.dts_name = %s "
                "and directly_referenced "
                "and definition > '' "
                "order by _order * 1, definition"
            )
            self.cursor.execute(query, (dts_name,))
        else:
            query = self.db.rewrite_query(
                "select id, role_uri, definition, dts_name, rowid, _order, directly_referenced "
                "from linkrole where linkrole.dts_name = %s "
            )
            self.cursor.execute(query, (dts_name,))

        rows = self.cursor.fetchall()
        if len(rows):
            self.cache_rows(rows)

        if self.full:
            self.get_relations()

    def linkroles_of_dts_with_presentation(self, dts_name=None):
        query = self.db.rewrite_query(
            "select linkrole.id, role_uri, definition, linkrole.dts_name, linkrole.rowid, "
            "linkrole._order, linkrole.directly_referenced "
            "from linkrole "
            "left join presentation_hierarchy_node phn on phn.parent_linkrole_rowid = linkrole.rowid "
            "where linkrole.dts_name = %s "
            "and linkrole.directly_referenced "
            "and definition > '' "
            "and phn.rowid is not null "
            "order by linkrole._order * 1, linkrole.definition "
        )
        self.cursor.execute(query, (dts_name,))
        rows = self.cursor.fetchall()
        if len(rows):
            self.cache_rows(rows)
        self.get_relations()
        self.cast_to_presentation()

    def cast_to_presentation(self):
        top = {
            "node_type": "concept",
            "rowid": 0,
            "id": "top_node",
            "concept": {
                "id": "presentation",
                "name": "pres_name",
                "ns_prefix": "",
                "namespace": "",
                "labels": [
                    {
                        "id": "",
                        "lang": "nl",
                        "label_text": "",
                        "link_role": "http://some-domain/label",
                    }
                ],
            },
        }
        for linkrole in self.linkroles:
            # 'Cast' the linkroles to a presentation_node
            linkrole["is_root"] = False
            linkrole["node_type"] = "linkrole"
            linkrole["linkrole_id"] = linkrole["id"]
            linkrole["linkrole_role_uri"] = linkrole["role_uri"]
            linkrole["has_presentation"] = True
        top["children"] = self.linkroles
        self.linkroles = [top]

    def linkrole_of_dts_by_id(self, dts_name=None, linkrole_id=None, roleURI=None):
        if dts_name is None or linkrole_id is None:
            return False
        if roleURI is None:
            query = self.db.rewrite_query(
                "select id, role_uri, definition, dts_name, rowid, _order, directly_referenced "
                "from linkrole where id = %s and dts_name = %s "
            )
            self.cursor.execute(query, (linkrole_id, dts_name))
        else:
            query = self.db.rewrite_query(
                "select id, role_uri, definition, dts_name, rowid, _order, directly_referenced "
                "from linkrole where id = %s and dts_name = %s and role_uri = %s"
            )
            self.cursor.execute(query, (linkrole_id, dts_name, roleURI))

        rows = self.cursor.fetchall()
        if len(rows):
            self.cache_rows(rows)
        self.get_relations()

    def linkrole_by_rowid(self, rowid=None):
        if rowid is None:
            return False
        query = self.db.rewrite_query(
            "select id, role_uri, definition, dts_name, rowid, _order, directly_referenced "
            "from linkrole where rowid = %s "
        )
        self.cursor.execute(query, (rowid,))
        rows = self.cursor.fetchall()
        if len(rows):
            self.cache_rows(rows)
        if self.full:
            self.get_relations()

    def cache_rows(self, rows):
        for row in rows:
            labels = Labels(db=self.db)
            labels.labels_of_linkrole(linkrole_rowid=row[4])
            self.linkroles.append(
                {
                    "id": row[0],
                    "role_uri": row[1],
                    "definition": row[2],
                    "dts_name": row[3],
                    "rowid": row[4],
                    "order": row[5],
                    "directly_referenced": row[6],
                    "labels": labels.labels,
                }
            )
            # print(self.linkroles)

    def get_relations(self):
        for linkrole in self.linkroles:
            hypercube = HyperCubes(db=self.db, full=self.full)
            hypercube.hypercube_of_linkrole(
                linkrole_rowid=linkrole["rowid"], dts_name=linkrole["dts_name"]
            )
            linkrole["hypercube"] = (
                hypercube.hypercubes[0] if len(hypercube.hypercubes) else None
            )
            references = References(db=self.db)
            references.references_of_linkrole(linkrole_rowid=linkrole["rowid"])
            linkrole["references"] = references.references

            tables = Tables(db=self.db)
            tables.tables_of_linkrole(linkrole_rowid=linkrole["rowid"])
            linkrole["tables"] = tables.tables

            variables = Variables(db=self.db)
            variables.variables_of_linkrole(linkrole_rowid=linkrole["rowid"])
            linkrole["variables"] = variables.variables

            assertions = Assertions(db=self.db, full=self.full)
            assertions.assertions_of_linkrole(linkrole_rowid=linkrole["rowid"])
            linkrole["assertions"] = assertions.assertions

            used_on = UsedOns(db=self.db, full=self.full)
            used_on.used_ons_of_linkrole(linkrole_rowid=linkrole["rowid"])
            linkrole["used_on"] = used_on.used_ons

            presentation = PresentationHierarchies(db=self.db, full=self.full)
            presentation.presentation_hierarchies_of_linkrole(
                parent_linkrole_rowid=linkrole["rowid"]
            )
            if len(presentation.presentation_hierarchies):
                linkrole[
                    "presentation_hierarchy"
                ] = presentation.presentation_hierarchies[0]

            dimensions = Dimensions(db=self.db, full=self.full)
            dimensions.dimensions_of_target_linkrole(
                target_linkrole=linkrole["role_uri"], dts_name=linkrole["dts_name"]
            )
            if len(dimensions.dimensions):
                linkrole["targeted_by"] = dimensions.dimensions
            dimensions.dimensions = []
            dimensions.dimensions_of_linkrole(
                linkrole_rowid=linkrole["rowid"], dts_name=linkrole["dts_name"]
            )
            if len(dimensions.dimensions):
                linkrole["dimensions"] = dimensions.dimensions
