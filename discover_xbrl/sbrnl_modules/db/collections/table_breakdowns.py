from discover_xbrl.sbrnl_modules.db.collections.concept_relationshipnodes import (
    ConceptRelationshipnodes,
)
from discover_xbrl.sbrnl_modules.db.collections.aspect_nodes import AspectNodes
from discover_xbrl.sbrnl_modules.db.collections.dimension_relationshipnodes import (
    DimensionRelationshipnodes,
)
from discover_xbrl.sbrnl_modules.db.collections.table_rulenodes import TableRulenodes


class TableBreakdowns:
    def __init__(self, db=None, full=True):
        self.db = db
        self.full = full
        self.table_breakdowns = []
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def table_breakdowns_of_table(self, table_id=None):
        if not table_id:
            return False
        query = self.db.rewrite_query(
            "select table_breakdown.id, table_breakdown.label, table_breakdown.parent_child_order, axis, "
            "table_id, _order "
            "from table_breakdown "
            "left join xbrl_table on xbrl_table.id = table_breakdown.table_id "
            "where table_breakdown.table_id = %s "
            "order by _order * 1"
        )
        self.cursor.execute(query, (table_id,))
        rows = self.cursor.fetchall()
        for row in rows:
            self.table_breakdowns.append(
                {
                    "id": row[0],
                    "label": row[1],
                    "parent_child_order": row[2],
                    "axis": row[3],
                    "table_id": row[4],
                    "order": row[5],
                }
            )
        if self.full:
            self.get_relations()

    def get_relations(self):
        for table_breakdown in self.table_breakdowns:
            concept_relationshipnodes = ConceptRelationshipnodes(
                db=self.db, full=self.full
            )
            concept_relationshipnodes.concept_relationshipnodes_of_parent(
                parent_id=table_breakdown["id"]
            )
            table_breakdown[
                "concept_relationshipnodes"
            ] = concept_relationshipnodes.concept_relationshipnodes

            aspect_nodes = AspectNodes(db=self.db, full=self.full)
            aspect_nodes.aspect_nodes_of_parent(parent_id=table_breakdown["id"])
            table_breakdown["aspect_nodes"] = aspect_nodes.aspect_nodes

            dimension_relationshipnodes = DimensionRelationshipnodes(
                db=self.db, full=self.full
            )
            dimension_relationshipnodes.dimension_relationshipnodes_of_parent(
                parent_id=table_breakdown["id"]
            )
            table_breakdown[
                "dimension_relationshipnodes"
            ] = dimension_relationshipnodes.dimension_relationshipnodes

            table_rulenodes = TableRulenodes(db=self.db, full=self.full)
            table_rulenodes.table_rulenodes_of_parent(parent_id=table_breakdown["id"])
            table_breakdown["table_rulenodes"] = table_rulenodes.table_rulenodes

            # labels = Labels(db=self.db)
            # labels.all_labels_by_partial_id(partial_id=table_breakdown["label"])
            # table_breakdown["labels"] = labels.labels
