class UsedOns:
    def __init__(self, db=None, full=True):
        self.db = db
        self.full = full
        self.used_ons = []
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def used_ons_of_linkrole(self, linkrole_rowid=None):
        if linkrole_rowid is None:
            return False
        query = self.db.rewrite_query(
            "select used_on_used_on "
            "from linkrole_used_on "
            "where linkrole_rowid = %s "
        )
        self.cursor.execute(query, (linkrole_rowid,))
        rows = self.cursor.fetchall()
        if rows:
            self.cache_rows(rows)

    def cache_rows(self, rows):
        for row in rows:
            self.used_ons.append(row[0])
