import itertools


class Labels:
    def __init__(self, db=None):
        self.db = db
        self.labels = []
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def all_labels(self, filter=None):
        if filter is None:
            self.cursor.execute(
                "select id, link_label, link_role, lang, label_text " "from label"
            )
        else:
            query = self.db.rewrite_query(
                "select id, link_label, link_role, lang, label_text "
                "from label where label_text like %s or id like %s "
            )
            self.cursor.execute(query, (f"%{filter}%", f"%{filter}%"))
        for row in self.cursor.fetchall():
            self.cache_row(row)

    def labels_of_concept(self, concept_id=None, dts_name=None):
        if concept_id is None:
            return False
        if dts_name is None:
            query = self.db.rewrite_query(
                "select id, link_label, link_role, lang, label_text "
                "from label "
                "left join concept_label cl on label.link_label = cl.label_link_label "
                "                          and cl.label_id = label.id "
                "where cl.concept_id = %s "
                "order by lang desc, link_role "
            )
            self.cursor.execute(query, (concept_id,))
        else:
            query = self.db.rewrite_query(
                "select id, link_label, link_role, lang, label_text "
                "from label "
                "left join concept_label cl on label.link_label = cl.label_link_label "
                "                          and cl.label_id = label.id "
                "                          and cl.label_link_role = label.link_role "
                "where cl.concept_id = %s and cl.dts_name = %s "
                "order by lang desc, link_role "
            )
            self.cursor.execute(query, (concept_id, dts_name))
        if dts_name is None:
            print(f"Called without DTS_name ")
            import traceback

            for line in traceback.format_stack():
                print(line.strip())
        for row in self.cursor.fetchall():
            self.cache_row(row)

    def labels_of_dts(self, dts_name=None, q=None, referenced_only=True):
        """ Op dit moment halen we alleen Concept-Labels op, Generic labels komen
        niet door (op dit moment...) """
        if dts_name is None:
            return False
        if not q:
            if referenced_only is True:
                query = self.db.rewrite_query(
                    "select id, link_label, link_role, lang, label_text "
                    "from dts_concept dc "
                    "left join concept_label cl on "
                    " cl.concept_id = dc.concept_id and cl.dts_name = dc.dts_name "
                    "left join label on "
                    " label.link_label = cl.label_link_label and label.id = cl.label_id "
                    " and label.lang = cl.label_lang and cl.label_link_role = label.link_role "
                    "where cl.dts_name = %s "
                    "and directly_referenced "
                    "and label.link_label is not null "
                    "order by label_text "
                )
            else:
                query = self.db.rewrite_query(
                    "select id, link_label, link_role, lang, label_text "
                    "from dts_concept dc "
                    "left join concept_label cl on "
                    " cl.concept_id = dc.concept_id and cl.dts_name = dc.dts_name "
                    "left join label on "
                    " label.link_label = cl.label_link_label and label.id = cl.label_id "
                    " and label.lang = cl.label_lang and cl.label_link_role = label.link_role "
                    "where cl.dts_name = %s "
                    "and label.link_label is not null "
                    "order by label_text "
                )
            self.cursor.execute(query, (dts_name,))
            rows = self.cursor.fetchall()

        else:
            query = self.db.rewrite_query(
                "select id, link_label, link_role, lang, label_text "
                "from dts_concept dc "
                "left join concept_label cl on cl.concept_id = dc.concept_id and cl.dts_name = dc.dts_name "
                "left join label on label.link_label = cl.label_link_label and label.id = cl.label_id "
                " and label.lang = cl.label_lang and cl.label_link_role = label.link_role "
                "where cl.dts_name = %s "
                "and directly_referenced "
                "and (label_text like %s or link_label like %s) "
                "order by label_text "
            )
            self.cursor.execute(query, (dts_name, f"%{q}%", f"%{q}%"))
            rows = self.cursor.fetchall()
        # Start finding generics labels
        #   1. Linkroles
        query = self.db.rewrite_query(
            "select l.id, link_label, link_role, lang, label_text "
            "from linkrole lr "
            "left join linkrole_label ll on lr.rowid = ll.linkrole_rowid "
            "left join label l on l.id = ll.label_id and l.link_label = ll.label_link_label "
            " and l.lang = ll.label_lang and l.link_role = ll.label_link_role "
            " where lr.dts_name = %s "
            " and lr.directly_referenced "
            " and not l.label_text is null "
            " order by label_text "
        )
        self.cursor.execute(query, (dts_name,))
        lr_labels = self.cursor.fetchall()
        rows.extend(lr_labels)
        #   2. Tabkle
        query = self.db.rewrite_query(
            "select l.id, link_label, link_role, lang, label_text "
            "from xbrl_table xt "
            "left join xbrl_table_label xtl on xt.id = xtl.xbrl_table_id "
            "left join linkrole_table lt on lt.table_id = xt.id "
            "left join linkrole lr on lr.rowid = lt.linkrole_rowid "
            "left join label l on l.id = xtl.label_id and l.link_role = xtl.label_link_role "
            "  and l.lang = xtl.label_lang and l.link_label = xtl.label_link_label "
            "where lr.dts_name = %s and lr.directly_referenced "
            " and l.label_text > '' "
            "order by label_text"
        )
        self.cursor.execute(query, (dts_name,))
        xt_labels = self.cursor.fetchall()
        rows.extend(xt_labels)
        # 3. Table rulenodes
        # Yeah right, good luck with this down - top search query;
        # table_rulenode_label, where parent table part of linkrole of dts
        # "select label.* from linkrole l
        # left join linkrole_table lt on l.rowid = lt.linkrole_rowid
        # left join xbrl_table xt on xt.id = lt.table_id
        # left join table_breakdown tb on tb.table_id = xt.id
        # left join table_rulenode tr on tr.parent = tb.id
        # left join table_rulenode_label trl on trl.table_rulenode_id = tr.id
        # left join label on label.id = trl.label_id
        # 	and label.link_role = trl.label_link_role
        # 	and label.lang = trl.label_lang
        # 	and label.link_label = trl.label_link_label
        # where trl.label_id is not null
        # and trl.label_link_role = 'http://www.xbrl.org/2008/role/label'
        # and l.dts_name = 'kvk-rpt-jaarverantwoording-2022-nlgaap-micro.xsd'
        # union
        # select label.* from linkrole l
        # left join linkrole_table lt on l.rowid = lt.linkrole_rowid
        # left join xbrl_table xt on xt.id = lt.table_id
        # left join table_breakdown tb on tb.table_id = xt.id
        # left join table_rulenode tr on tr.parent = tb.id
        # left join table_rulenode tr1 on tr1.parent = tr.id
        # left join table_rulenode_label trl on trl.table_rulenode_id = tr1.id
        # left join label on label.id = trl.label_id
        # 	and label.link_role = trl.label_link_role
        # 	and label.lang = trl.label_lang
        # 	and label.link_label = trl.label_link_label
        # where trl.label_id is not null
        # and trl.label_link_role = 'http://www.xbrl.org/2008/role/label'
        # and l.dts_name = 'kvk-rpt-jaarverantwoording-2022-nlgaap-micro.xsd'
        # union
        # select label.* from linkrole l
        # left join linkrole_table lt on l.rowid = lt.linkrole_rowid
        # left join xbrl_table xt on xt.id = lt.table_id
        # left join table_breakdown tb on tb.table_id = xt.id
        # left join table_rulenode tr on tr.parent = tb.id
        # left join table_rulenode tr1 on tr1.parent = tr.id
        # left join table_rulenode tr2 on tr2.parent = tr1.id
        # left join table_rulenode_label trl on trl.table_rulenode_id = tr2.id
        # left join label on label.id = trl.label_id
        # 	and label.link_role = trl.label_link_role
        # 	and label.lang = trl.label_lang
        # 	and label.link_label = trl.label_link_label
        # where trl.label_id is not null
        # and trl.label_link_role = 'http://www.xbrl.org/2008/role/label'
        # and l.dts_name = 'kvk-rpt-jaarverantwoording-2022-nlgaap-micro.xsd'"

        # 4. Assertions
        query = self.db.rewrite_query(
            "select l.id, link_label, link_role, lang, label_text "
            "from assertion ass "
            "left join assertion_label asl on ass.id = asl.assertion_id "
            "left join linkrole_assertion la on la.assertion_id = ass.id "
            "left join linkrole lr on lr.rowid = la.linkrole_rowid "
            "left join label l on l.id = asl.label_id and l.link_role = asl.label_link_role "
            "  and l.lang = asl.label_lang and l.link_label = asl.label_link_label "
            "where lr.dts_name = %s and lr.directly_referenced "
            " and l.label_text > '' "
            "order by label_text"
        )
        self.cursor.execute(query, (dts_name,))
        ass_labels = self.cursor.fetchall()
        rows.extend(ass_labels)

        # 5. Enumeration options
        query = self.db.rewrite_query(
            "select l.id, link_label, link_role, lang, label_text "
            "from enumeration_option eno "
            "left join enumeration_option_label eol on eol.enumeration_option_id = eno.id "
            "left join itemtype_enumeration_option ieo on ieo.enumeration_option_id = eno.id "
            "left join itemtype i on i.name = ieo.itemtype_name "
            "left join dts_itemtype di on di.itemtype_name = i.name "
            "left join label l on l.id = eol.label_id and l.link_role = eol.label_link_role "
            "  and l.lang = eol.label_lang and l.link_label = eol.label_link_label "
            "where di.dts_name = %s "
            " and l.label_text > '' "
            "order by label_text"
        )
        self.cursor.execute(query, (dts_name,))
        eo_labels = self.cursor.fetchall()
        rows.extend(eo_labels)

        # sorteer de hele lijst opnieuw op tekst
        rows.sort(key=lambda row: row[4])
        for row in rows:
            self.cache_row(row)

    def labels_of_table(self, table_id=None):
        if table_id is None:
            return False
        query = self.db.rewrite_query(
            "select id, link_label, link_role, lang, label_text "
            "from label "
            "left join xbrl_table_label on label.link_label = xbrl_table_label.label_link_label "
            "                           and label.id = xbrl_table_label.label_id "
            "where xbrl_table_id = %s "
        )
        self.cursor.execute(query, (table_id,))
        for row in self.cursor.fetchall():
            self.cache_row(row)

    def labels_of_table_rulenode(self, table_rulenode_id=None):
        if table_rulenode_id is None:
            return False
        query = self.db.rewrite_query(
            "select id, link_label, link_role, lang, label_text "
            "from label "
            "left join table_rulenode_label on label.link_label = table_rulenode_label.label_link_label "
            "                               and label.id = table_rulenode_label.label_id "
            "where table_rulenode_id = %s "
        )
        self.cursor.execute(query, (table_rulenode_id,))
        for row in self.cursor.fetchall():
            self.cache_row(row)

    def labels_of_assertion(self, assertion_id=None):
        if assertion_id is None:
            return False
        query = self.db.rewrite_query(
            "select id, link_label, link_role, lang, label_text "
            "from label "
            "left join assertion_label on label.link_label = assertion_label.label_link_label "
            "                         and label.id = assertion_label.label_id "
            "where assertion_id = %s "
        )
        self.cursor.execute(query, (assertion_id,))
        for row in self.cursor.fetchall():
            self.cache_row(row)

    def labels_of_enumeration_option(self, enumeration_option_id=None):
        if enumeration_option_id is None:
            return False
        query = self.db.rewrite_query(
            "select id, link_label, link_role, lang, label_text "
            "from label "
            "left join enumeration_option_label "
            "on label.link_label = enumeration_option_label.label_link_label "
            " and label.id = enumeration_option_label.label_id "
            "where enumeration_option_id = %s "
        )
        self.cursor.execute(query, (enumeration_option_id,))
        for row in self.cursor.fetchall():
            self.cache_row(row)

    def labels_of_linkrole(self, linkrole_rowid=None):
        if linkrole_rowid is None:
            return False
        query = self.db.rewrite_query(
            "select id, link_label, link_role, lang, label_text "
            "from label "
            "left join linkrole_label ll "
            "on label.link_label = ll.label_link_label and label.id = ll.label_id "
            "where linkrole_rowid = %s "
        )
        self.cursor.execute(query, (linkrole_rowid,))
        for row in self.cursor.fetchall():
            self.cache_row(row)

    def cache_row(self, row=None):
        if not row:
            return False
        self.labels.append(
            {
                "id": row[0],
                "link_label": row[1],
                "link_role": row[2],
                "lang": row[3],
                "label_text": row[4],
            }
        )

    def languages_of_dts(self, dts_name=None):
        """Determine which languages are supported by this DTS.
        Only those languages which have at least 10% the amount
        of the largest amount of labels.
        So: NL: 6000 labels, FR 300 labels? No FR in the list
            NL: 6000 labels, FR 2000 labels? We show FR as an option"""
        if not dts_name:
            return False
        query = self.db.rewrite_query(
            "select lang, count(lang) from dts_concept dc "
            "left join concept c on c.id = dc.concept_id "
            "left join concept_label cl on cl.concept_id = c.id "
            "left join label l on l.link_label = cl.label_link_label "
            "                  and l.id = cl.label_id "
            "where dc.dts_name = %s "
            "  and l.lang is not null "
            "group by l.lang "
            "order by count(lang) desc "
        )
        self.cursor.execute(query, (dts_name,))
        rows = self.cursor.fetchall()
        response = []
        if rows:
            maxcount = rows[0][
                1
            ]  # we ordered descending, this is the largest amount of labels for one language
            threshold = (
                maxcount / 10
            )  # if the language contains at least 10% of the largest, it's in!
            for row in rows:
                if row[1] >= threshold:
                    response.append(row[0])
        return response

    def label_roles(self):
        self.cursor.execute("select distinct(link_role) from label")
        rows = self.cursor.fetchall()
        return list(itertools.chain(*rows))
