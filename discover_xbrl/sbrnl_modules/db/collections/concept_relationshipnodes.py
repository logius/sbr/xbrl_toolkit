from discover_xbrl.sbrnl_modules.db.collections.labels import Labels


class ConceptRelationshipnodes:
    def __init__(self, db=None, full=True):
        self.db = db
        self.full = full
        self.concept_relationshipnodes = []
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def concept_relationshipnodes_of_parent(self, parent_id=None):
        if not parent_id:
            return False
        query = self.db.rewrite_query(
            "select id, label, parent_child_order, parent, tagselector, _order, relationship_source, "
            "linkrole_role_uri, formula_axis, generations, arcrole, linkname, arcname "
            "from concept_relationshipnode where parent = %s "
        )
        self.cursor.execute(query, (parent_id,))
        rows = self.cursor.fetchall()
        for row in rows:
            self.concept_relationshipnodes.append(
                {
                    "id": row[0],
                    "label": row[1],
                    "parent_child_order": row[2],
                    "parent": row[3],
                    "tagselector": row[4],
                    "order": row[5],
                    "relationship_source": row[6],
                    "linkrole_role_uri": row[7],
                    "formula_axis": row[8],
                    "generations": row[9],
                    "arcrole": row[10],
                    "linkname": row[11],
                    "arcname": row[12],
                }
            )
