import json
from anytree import AnyNode, RenderTree
from anytree.exporter import JsonExporter
from discover_xbrl.sbrnl_modules.db.collections.linkroles import Linkroles
from discover_xbrl.sbrnl_modules.db.collections.concepts import Concepts


class Arcs:
    def __init__(self, db=None, full=True):
        self.db = db
        self.arcs = []
        self.full = full
        self.arcroles = []
        self.tree = None
        self.queryable_arcroles = [
            "http://xbrl.org/int/dim/arcrole/all",
            "http://xbrl.org/int/dim/arcrole/domain-member",
            "http://xbrl.org/int/dim/arcrole/dimension-domain",
            "http://www.xbrl.org/2003/arcrole/parent-child",
            "http://xbrl.org/int/dim/arcrole/hypercube-dimension",
            "http://www.nltaxonomie.nl/2017/arcrole/ConceptDisclosure",
            "http://www.nltaxonomie.nl/2017/arcrole/ConceptPolicy",
            "http://www.nltaxonomie.nl/2017/arcrole/ConceptMemberEquivalence",
            "http://www.nltaxonomie.nl/2019/sbr-arcrole-2019/ParentChildSummation",
        ]
        self.arcrole_dropdown = []
        self._dts_name = None
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    @property
    def json_tree(self):
        # FastAPI return JSON itself, if we would pass the json-exporter it would encode a string
        # instead of an object, but we _need_ the jsonexporter to create json out of the tree
        exporter = JsonExporter(indent=2, sort_keys=True)
        return json.loads(exporter.export(self.tree))

    def arcs_of_linkrole(self, linkrole_rowid=None):
        query = self.db.rewrite_query(
            "select rowid, linkrole_rowid, arc_type, arcrole, from_id, from_type, "
            "to_id, to_type, _order, name, dts_name "
            "from arc "
            "where rowid = %s "
            "order by _order * 1"
        )
        self.cursor.execute(query, (linkrole_rowid,))
        rows = self.cursor.fetchall()
        if rows:
            self.cache_rows(rows=rows)

    def distinct_arcroles(self, dts_name=None):
        if dts_name:
            query = self.db.rewrite_query(
                "select distinct(arcrole) as ar, arc_type from arc where dts_name = %s order by arc_type, ar"
            )
            self.cursor.execute(query, (dts_name,))
            rows = self.cursor.fetchall()
            if rows:
                for row in rows:
                    self.arcroles.append({"arcrole": row[0], "arc_type": row[1]})

    def get_arcrole_dropdown(self, dts_name=None):
        if not len(self.arcroles):
            self.distinct_arcroles(dts_name=dts_name)
        for role in self.arcroles:
            if role["arcrole"] in self.queryable_arcroles:
                value = role["arcrole"].split("/")[-1:][0]
                label = role["arc_type"].split("}")[1].replace("Arc", "")
                if label == "arc":
                    label = "link"
                self.arcrole_dropdown.append(
                    {
                        "value": value,
                        "arcrole": role["arcrole"],
                        "type": role["arc_type"],
                        "label": f"{label}: {value}",
                    }
                )

    def arc_tree(self, dts_name=None, arcrole=None):
        self._dts_name = dts_name
        if not len(self.arcrole_dropdown):
            self.get_arcrole_dropdown(dts_name=dts_name)
        for role in self.arcrole_dropdown:
            if role["value"] == arcrole:
                arcrole = role["arcrole"]
                break
        # find the top arc(s) first
        query = self.db.rewrite_query(
            "select rowid, linkrole_rowid, arc_type, arcrole, from_id, from_type, "
            "to_id, to_type, _order, name, dts_name from arc "
            "where "
            "(select count(*) from arc parent where parent.to_id = arc.from_id "
            "  and parent.arcrole = arc.arcrole "
            "  and parent.dts_name = arc.dts_name) = 0 "
            "and dts_name = %s and arcrole = %s "
            "order by arc._order"
        )
        self.cursor.execute(query, (dts_name, arcrole))
        for row in self.cursor:
            self.cache_rows([row])
        self.tree = AnyNode(id=arcrole)
        if self.arcs:
            self.build_tree(arcs=self.arcs, parent=self.tree)
            # print(RenderTree(self.tree))

    def cache_rows(self, rows):
        for row in rows:
            self.arcs.append(
                {
                    "rowid": row[0],
                    "linkrole_rowid": row[1],
                    "arc_type": row[2],
                    "arcrole": row[3],
                    "from_id": row[4],
                    "from_type": row[5],
                    "to_id": row[6],
                    "to_type": row[7],
                    "order": row[8],
                    "name": row[9],
                    "dts_name": row[10],
                }
            )

    def get_children(self, arc):
        query = self.db.rewrite_query(
            "select rowid, linkrole_rowid, arc_type, arcrole, from_id, from_type, "
            "to_id, to_type, _order, name, dts_name from arc "
            "where from_id = %s "
            "and dts_name = %s "
            "and arcrole = %s "
            "and linkrole_rowid = %s "
            "order by _order * 1"
        )
        self.cursor.execute(
            query,
            (arc["to_id"], arc["dts_name"], arc["arcrole"], arc["linkrole_rowid"]),
        )
        rows = self.cursor.fetchall()
        arcs = []
        if rows:
            for row in rows:
                arcs.append(
                    {
                        "rowid": row[0],
                        "linkrole_rowid": row[1],
                        "arc_type": row[2],
                        "arcrole": row[3],
                        "from_id": row[4],
                        "from_type": row[5],
                        "to_id": row[6],
                        "to_type": row[7],
                        "order": row[8],
                        "name": row[9],
                        "dts_name": row[10],
                    }
                )
        return arcs

    def build_tree(self, arcs, parent, top=True):
        for arc in arcs:
            node = None
            if top:
                for n in self.tree.descendants:
                    if n.id == f"{arc['from_id']}_{arc['linkrole_rowid']}":
                        node = n
                if not node:
                    node = AnyNode(
                        id=f"{arc['from_id']}_{arc['linkrole_rowid']}", parent=parent
                    )
                    if arc["from_type"] == "concept":
                        concepts = Concepts(db=self.db, full=False)
                        concepts.concept_by_id(arc["from_id"], dts_name=self._dts_name)
                        if len(concepts.concepts):
                            node.concept = concepts.concepts[0]
                    linkroles = Linkroles(db=self.db, full=False)
                    linkroles.linkrole_by_rowid(arc["linkrole_rowid"])
                    if len(linkroles.linkroles) and linkroles.linkroles[0][
                        "id"
                    ] not in ["standard-link-role"]:
                        linkrole = linkroles.linkroles[0]
                        linkrole_node = None
                        for l in self.tree.descendants:
                            if l.id == f"{linkrole['rowid']}":
                                linkrole_node = l
                        if not linkrole_node:
                            linkrole_node = AnyNode(
                                id=f"{linkrole['rowid']}",
                                parent=parent,
                                linkrole=linkrole,
                            )
                        node.parent = linkrole_node

            to = None
            for n in self.tree.descendants:
                if n.id == f"{arc['to_id']}_{arc['linkrole_rowid']}":
                    to = n
            if not to:
                to = AnyNode(
                    id=f"{arc['to_id']}_{arc['linkrole_rowid']}",
                    parent=node if top else parent,
                )
                if arc["to_type"] == "concept":
                    concepts = Concepts(db=self.db, full=False)
                    concepts.concept_by_id(arc["to_id"], dts_name=self._dts_name)
                    if len(concepts.concepts):
                        to.concept = concepts.concepts[0]

            children = self.get_children(arc)
            if children:
                self.build_tree(arcs=children, parent=to, top=False)
