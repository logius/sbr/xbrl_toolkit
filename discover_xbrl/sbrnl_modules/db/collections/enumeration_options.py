from discover_xbrl.sbrnl_modules.db.collections.labels import Labels


class EnumerationOptions:
    def __init__(self, db=None, full=True):
        self.db = db
        self.full = full
        self.enumeration_options = []
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def enumeration_options_of_itemtype(self, itemtype_name):
        query = self.db.rewrite_query(
            "select id, value from enumeration_option "
            "left join itemtype_enumeration_option ieo "
            "on enumeration_option.id = ieo.enumeration_option_id "
            "where ieo.itemtype_name = %s "
        )
        self.cursor.execute(query, (itemtype_name,))
        rows = self.cursor.fetchall()
        if len(rows):
            self._cache_rows(rows)
        if self.full:
            self.get_relations()

    def _cache_rows(self, rows):
        for row in rows:
            self.enumeration_options.append({"id": row[0], "value": row[1]})

    def get_relations(self):
        for option in self.enumeration_options:
            labels = Labels(db=self.db)
            labels.labels_of_enumeration_option(enumeration_option_id=option["id"])
            option["labels"] = labels.labels
