from discover_xbrl.sbrnl_modules.db.connection import get_connection


class ConceptBackReferences:
    def __init__(self, db=None, full=True):
        self.db = db
        self.back_references = []
        self.full = full
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def back_references_of_concept(self, concept_id=None, dts_name=None):
        """ finds line_items, dimensions, domains, domain_members
        presentations and tables that use this concept """
        # hypercube / line_item
        query = self.db.rewrite_query(
            "select linkrole.rowid, linkrole.definition, linkrole.id from line_item "
            "left join hypercube on hypercube_linkrole_rowid = linkrole_rowid "
            "left join linkrole on rowid = linkrole_rowid "
            "where line_item.concept_id = %s "
            "and linkrole.dts_name = %s "
        )
        self.cursor.execute(query, (concept_id, dts_name))
        rows = self.cursor.fetchall()
        if len(rows):
            for row in rows:
                self.back_references.append(
                    {
                        "type": "primair domein [posten]",
                        "source": "definition link",
                        "linkrole_rowid": row[0],
                        "definition": row[1],
                        "linkrole_id": row[2],
                    }
                )
        # dimensions (via hypercube)
        query = self.db.rewrite_query(
            "select linkrole.rowid, linkrole.definition, linkrole.id from dimension "
            "left join hypercube on hypercube_linkrole_rowid = hypercube.linkrole_rowid "
            "left join linkrole on linkrole.rowid = hypercube.linkrole_rowid "
            "where dimension.concept_id = %s "
            "and linkrole.dts_name = %s "
        )
        self.cursor.execute(query, (concept_id, dts_name))
        rows = self.cursor.fetchall()
        if len(rows):
            for row in rows:
                self.back_references.append(
                    {
                        "type": "dimension",
                        "source": "definition link",
                        "linkrole_rowid": row[0],
                        "definition": row[1],
                        "linkrole_id": row[2],
                    }
                )
        # domain via hypercube
        query = self.db.rewrite_query(
            "select hypercube.linkrole_rowid, definition, linkrole.id from domain "
            "left join dimension on domain.dimension_rowid = dimension.rowid "
            "left join hypercube on hypercube_linkrole_rowid = hypercube.linkrole_rowid "
            "left join linkrole on linkrole.rowid = hypercube.linkrole_rowid "
            "where domain.concept_id = %s "
            "and linkrole.dts_name = %s "
        )
        self.cursor.execute(query, (concept_id, dts_name))
        rows = self.cursor.fetchall()
        if len(rows):
            for row in rows:
                self.back_references.append(
                    {
                        "type": "domain",
                        "source": "definition link",
                        "linkrole_rowid": row[0],
                        "definition": row[1],
                        "linkrole_id": row[2],
                    }
                )

        # domain_members via dimension
        query = self.db.rewrite_query(
            "select hypercube.linkrole_rowid, definition, linkrole.id from member "
            "left join dimension on member.parent_rowid = dimension.rowid "
            "left join hypercube on hypercube_linkrole_rowid = hypercube.linkrole_rowid "
            "left join linkrole on linkrole.rowid = hypercube.linkrole_rowid "
            "where member.concept_id = %s "
            "and member.parent_type = 'dimension' "
            "and linkrole.dts_name = %s "
        )
        self.cursor.execute(query, (concept_id, dts_name))
        rows = self.cursor.fetchall()
        if len(rows):
            for row in rows:
                self.back_references.append(
                    {
                        "type": "member (dim)",
                        "source": "definition link",
                        "linkrole_rowid": row[0],
                        "definition": row[1],
                        "linkrole_id": row[2],
                    }
                )

        # domain_member via member
        query = self.db.rewrite_query(
            "select hypercube.linkrole_rowid, definition, linkrole.id from member "
            "left join member m on member.parent_rowid = m.rowid "
            "left join dimension on dimension.rowid = m.parent_rowid "
            "left join hypercube on hypercube_linkrole_rowid = hypercube.linkrole_rowid "
            "left join linkrole on linkrole.rowid = hypercube.linkrole_rowid "
            "where member.concept_id = %s "
            "and member.parent_type = 'member' "
            "and linkrole.dts_name = %s "
        )
        self.cursor.execute(query, (concept_id, dts_name))
        rows = self.cursor.fetchall()
        if len(rows):
            for row in rows:
                self.back_references.append(
                    {
                        "type": "member (dom)",
                        "source": "definition link",
                        "linkrole_rowid": row[0],
                        "definition": row[1],
                        "linkrole_id": row[2],
                    }
                )
        # somewhere in the presentation?
        # this one is a bitch, it selects recursively rows from the DB
        # the first query selects *all* presentation_nodes with the requested concept_id,
        # but we only want those that belong to the DTS, for that we must traverse up the tree
        # this is done by the union of all the presentation_nodes which parent the current node
        # the end result could have way to many rows selected,
        # but the root_nodes point to the place where the concept_id is used in the requested DTS
        # keywords Common Table Expressions (cte) and with
        # https://sqlite.org/lang_with.html
        query = self.db.rewrite_query(
            "with recursive cte_pres "
            "  (id, rowid, parent_rowid, concept_id, dts_name, is_root, definition, dts_name, linkrole_id) as ( "
            "  select p.id, p.rowid, p.parent_rowid, p.concept_id, p.dts_name, p.is_root, "
            "         l.definition, l.dts_name, l.id as linkrole_id"
            "  from presentation_hierarchy_node p "
            "  left join linkrole l on l.rowid = p.parent_linkrole_rowid "
            "  where p.concept_id = %s "
            "  and (p.dts_name = %s or p.dts_name is null) "
            "  and (l.dts_name = %s or l.dts_name is null) "
            "  union "
            "  select p.id, p.rowid, p.parent_rowid, p.concept_id, p.dts_name, p.is_root, "
            "         l.definition, l.dts_name, l.id as linkrole_id "
            "  from presentation_hierarchy_node p "
            "  left join linkrole l on l.rowid = p.parent_linkrole_rowid "
            "  join cte_pres on cte_pres.parent_rowid = p.rowid "
            "  where (l.dts_name = %s or l.dts_name is null) "
            "  and (p.dts_name = %s or p.dts_name is null) "
            ") "
            "select * from cte_pres"
        )
        self.cursor.execute(query, (concept_id, dts_name, dts_name, dts_name, dts_name))
        rows = self.cursor.fetchall()
        if len(rows):
            seen = []
            for row in rows:
                if (
                    row[5] and row not in seen
                ):  # row[5]: is_root, so this iis the top node
                    seen.append(row)
                    self.back_references.append(
                        {
                            "type": "presentation node",
                            "source": "presentation",
                            "presentation_id": row[0],
                            "definition": row[6],
                            "linkrole_id": row[8],
                        }
                    )

        # Tables via concept_relationshipnodes
        # dit lukt niet met een cte, we moeten van
        # concept_relationsipnode naar table_rulenode naar rulenode naar breakdown
        #  OF rechtstreeks naar table_breakdown om uiteindelijk de tabel te vinden
        # 1. Gather Concept_relationshipnodes
        query = self.db.rewrite_query(
            "select crn.id, crn.label, crn.parent, crn.relationship_source, "
            "crn.linkrole_role_uri "
            "from concept_relationshipnode crn "
            "left join concept on concept.ns_prefix || ':' || concept.name = crn.relationship_source "
            "where concept.id = %s "
        )
        self.cursor.execute(query, (concept_id,))
        crns = self.cursor.fetchall()
        self.find_table_top(
            leaves=crns, dts_name=dts_name, node_type="concept relationshipnode"
        )

        # 2. Gather aspect_nodes
        # make sure the fields correspond!
        query = self.db.rewrite_query(
            "select a.id, a.label, a.parent, value from aspect_node a "
            "left join concept on concept.ns_prefix || ':' || concept.name = a.value "
            "where concept.id = %s "
        )
        self.cursor.execute(query, (concept_id,))
        aspects = self.cursor.fetchall()
        self.find_table_top(leaves=aspects, dts_name=dts_name, node_type="aspectnode")

        # 3. Gather dimension_relationshipnodes
        # make sure the fields correspond!
        query = self.db.rewrite_query(
            "select d.id, d.label, d.parent, relationship_source from dimension_relationshipnode d "
            "left join concept on concept.ns_prefix || ':' || concept.name = d.dimension_id "
            "where concept.id = %s "
        )
        self.cursor.execute(query, (concept_id,))
        drns = self.cursor.fetchall()
        self.find_table_top(
            leaves=drns, dts_name=dts_name, node_type="dimension relationshipnode"
        )

    def find_table_top(self, leaves, dts_name, node_type):
        for leave in leaves:
            # rulenodes can be in between the breakdown; recursively! #
            # leave[2] needs to be the parent.id we are looking for.
            # At the top we should find a table_breakdown, which points to the table,
            # which needs to be connected to a linkrole inside the current DTS
            # first we gather all nodes up to the table_breakdown
            query = self.db.rewrite_query(
                "with recursive cte_table_finder (id, type, parent, label, axis, table_id) as ( "
                "    select id, 'rulenode' as type, parent, label, '' as axis, '' as table_id "
                "    from table_rulenode where id = %s "
                "    union "
                "    select id, 'breakdown' as type, label, '-' as parent, axis, table_id "
                "    from table_breakdown where id = %s "
                "  union "
                "    select tb.id, 'breakdown' as type, '-' as parent, tb.label, tb.axis, tb.table_id "
                "    from table_breakdown tb"
                "    join cte_table_finder on tb.id = cte_table_finder.parent "
                ")"
                "select * from cte_table_finder"
            )
            self.cursor.execute(query, (leave[2], leave[2]))
            rows = self.cursor.fetchall()
            for row in rows:
                if row[5]:
                    query = self.db.rewrite_query(
                        "select xbrl_table.id, linkrole.id, linkrole.definition, linkrole.dts_name from xbrl_table "
                        "left join linkrole_table on linkrole_table.table_id = xbrl_table.id "
                        "left join linkrole on linkrole.rowid = linkrole_table.linkrole_rowid "
                        "where xbrl_table.id = %s "
                        "and linkrole.dts_name = %s "
                    )
                    # if there is a table_id in the returned row, find the table and check if it's in DTS scope
                    self.cursor.execute(query, (row[5], dts_name))
                    linkroles = self.cursor.fetchall()
                    for linkrole in linkroles:
                        back_ref = {
                            "type": node_type,
                            "source": "table",
                            "table_id": linkrole[0],
                            "linkrole_id": linkrole[1],
                            "definition": linkrole[2],
                        }

                        if back_ref not in self.back_references:
                            self.back_references.append(back_ref)
                elif row[2] != "-":
                    # the recursion goes only 1 rulenode up and you can't joun the cte_query (yet)
                    self.find_table_top(
                        leaves=[row], dts_name=dts_name, node_type=node_type
                    )


def main():
    db = get_connection()
    backref = ConceptBackReferences(db=db)
    backref.back_references_of_concept(
        concept_id="kvk-i_DocumentResubmissionDueToUnsurmountableInaccuracies",
        dts_name="kvk-rpt-jaarverantwoording-2021-nlgaap-micro.xsd",
    )
    for b in backref.back_references:
        print(b)


if __name__ == "__main__":
    main()
