class FormulaPeriods:
    def __init__(self, db=None, full=True):
        self.db = db
        self.full = full
        self.formula_periods = []
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def formula_periods_of_ruleset(self, parent_ruleset=None):
        if not parent_ruleset:
            return False
        query = self.db.rewrite_query(
            "select rowid, period_type, _start, _end, value, parent_ruleset, parent_node "
            "from formula_period where parent_ruleset = %s "
        )
        self.cursor.execute(query, (parent_ruleset,))
        rows = self.cursor.fetchall()
        if len(rows):
            self.cache_rows(rows=rows)

    def formula_periods_of_rulenode(self, parent_rulenode=None):
        if not parent_rulenode:
            return False
        query = self.db.rewrite_query(
            "select rowid, period_type, _start, _end, value, parent_ruleset, parent_node "
            "from formula_period where parent_node = %s "
        )
        self.cursor.execute(query, (parent_rulenode,))
        rows = self.cursor.fetchall()
        if len(rows):
            self.cache_rows(rows=rows)

    def cache_rows(self, rows=None):
        for row in rows:
            self.formula_periods.append(
                {
                    "rowid": row[0],
                    "period_type": row[1],
                    "start": row[2],
                    "end": row[3],
                    "value": row[4],
                    "parent_ruleset": row[5],
                    "parent_node": row[6],
                }
            )
