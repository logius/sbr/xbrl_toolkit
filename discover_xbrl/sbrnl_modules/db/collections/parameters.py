class Parameters:
    def __init__(self, db=None, full=True):
        self.db = db
        self.parameters = []
        self.full = full
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def parameters_of_table(self, table_id=None):
        if not table_id:
            return False
        query = self.db.rewrite_query(
            "select id, label, as_datatype, _select, name, required "
            "from parameter "
            "left join table_parameter tp on parameter.id = tp.parameter_id "
            "where tp.table_id = %s "
        )
        self.cursor.execute(query, (table_id,))
        rows = self.cursor.fetchall()
        if len(rows):
            self.cache_rows(rows=rows)

    def cache_rows(self, rows):
        for row in rows:
            self.parameters.append(
                {
                    "id": row[0],
                    "label": row[1],
                    "as_datatype": row[2],
                    "select": row[3],
                    "name": row[4],
                    "required": row[5],
                }
            )
