from discover_xbrl.sbrnl_modules.db.collections.enumeration_options import (
    EnumerationOptions,
)


class ItemTypes:
    def __init__(self, db=None, full=True, resolve_base=True):
        self.db = db
        self.full = full
        self.resolve_base = resolve_base
        self.itemtypes = []
        self._dts_name = None
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def itemtypes_of_dts(self, dts_name=None):
        if not dts_name:
            return False
        self._dts_name = dts_name
        query = self.db.rewrite_query(
            "select distinct(it.id), it.name, domain, base, inheritance_type, restriction_type, restriction_length, "
            "restriction_pattern, restriction_max_length, restriction_min_length, "
            "total_digits, fraction_digits, min_inclusive, count(c.name) as occ "
            "from itemtype it "
            "left join concept c on it.name = c.itemtype_name  "
            "left join dts_concept on dts_concept.concept_id = c.id "
            "where dts_name = %s "
            "and it.name is not null "
            "group by it.id, it.name, domain, base, inheritance_type, restriction_type, "
            "         restriction_length, restriction_pattern, restriction_max_length, restriction_min_length, "
            "         total_digits, fraction_digits, min_inclusive "
            "order by it.name"
        )
        self.cursor.execute(query, (dts_name,))
        rows = self.cursor.fetchall()
        if rows:
            self.cache_rows(rows)

    def itemtypes_of_concept(self, name=None):
        query = self.db.rewrite_query(
            "select id, name, domain, base, inheritance_type, restriction_type, restriction_length, "
            "restriction_pattern, restriction_max_length, restriction_min_length, "
            "total_digits, fraction_digits, min_inclusive "
            "from itemtype where name = %s "
            "order by itemtype.name "
        )
        self.cursor.execute(query, (name,))
        rows = self.cursor.fetchall()
        if rows:
            self.cache_rows(rows)

    def itemtype_by_name(self, itemtype_name=None, dts_name=None):
        if not itemtype_name:
            return False
        if not dts_name:
            query = self.db.rewrite_query(
                "select it.id, it.name, domain, base, inheritance_type, restriction_type, restriction_length, "
                "restriction_pattern, restriction_max_length, restriction_min_length, "
                "total_digits, fraction_digits, min_inclusive, count(c.name) as occ "
                "from itemtype it left join concept c on c.itemtype_name = it.name "
                "where it.name = %s "
                "group by it.id, it.name, domain, base, inheritance_type, restriction_type, "
                "         restriction_length, restriction_pattern, restriction_max_length, restriction_min_length, "
                "         total_digits, fraction_digits, min_inclusive "
            )
            self.cursor.execute(query, (itemtype_name,))
        else:
            query = self.db.rewrite_query(
                "select it.id, it.name, domain, base, inheritance_type, restriction_type, restriction_length, "
                "restriction_pattern, restriction_max_length, restriction_min_length, "
                "total_digits, fraction_digits, min_inclusive, count(c.name) as occ "
                "from itemtype it left join concept c on c.itemtype_name = it.name "
                " left join dts_concept dc on c.id = dc.concept_id "
                "where it.name = %s  and dts_name = %s "
                "group by it.id, it.name, domain, base, inheritance_type, restriction_type, "
                "         restriction_length, restriction_pattern, restriction_max_length, restriction_min_length, "
                "         total_digits, fraction_digits, min_inclusive "
            )
            self._dts_name = dts_name
            self.cursor.execute(query, (itemtype_name, dts_name))

        rows = self.cursor.fetchall()
        if rows:
            self.cache_rows(rows)

    def cache_rows(self, rows=None):
        for row in rows:
            enumeration_options = EnumerationOptions(db=self.db, full=self.full)
            enumeration_options.enumeration_options_of_itemtype(itemtype_name=row[1])
            current_itemtype = {
                "id": row[0],
                "name": row[1],
                "domain": row[2],
                "base": row[3],
                "inheritance_type": row[4],
                "restriction_type": row[5],
                "restriction_length": row[6],
                "restriction_pattern": row[7],
                "restriction_max_length": row[8],
                "restriction_min_length": row[9],
                "total_digits": row[10],
                "fraction_digits": row[11],
                "min_inclusive": row[12],
                "enumeration": enumeration_options.enumeration_options
                if len(enumeration_options.enumeration_options)
                else None,
                "number_of_concepts": row[13] if len(row) > 13 else 9,
            }
            # Resolve a possible 'base' and copy it's restrictions if current is None
            if current_itemtype["base"] and self.resolve_base:
                # if there is no domain part in the 'base' we point to an unrestricted primitive
                # like date, string or decimal
                if ":" in current_itemtype["base"]:
                    domain, name = current_itemtype["base"].split(":")
                    query = self.db.rewrite_query(
                        "select inheritance_type, restriction_type, restriction_length, "
                        "restriction_pattern, restriction_max_length, restriction_min_length, "
                        "total_digits, fraction_digits, min_inclusive "
                        "from itemtype "
                        "where name = %s "
                        " and domain = %s "
                    )
                    self.cursor.execute(query, (name, domain))
                    base_itemtype = self.cursor.fetchone()
                    if base_itemtype:
                        base = {
                            "inheritance_type": base_itemtype[0],
                            "restriction_type": base_itemtype[1],
                            "restriction_length": base_itemtype[2],
                            "restriction_pattern": base_itemtype[3],
                            "restriction_max_length": base_itemtype[4],
                            "restriction_min_length": base_itemtype[5],
                            "total_digits": base_itemtype[6],
                            "fraction_digits": base_itemtype[7],
                            "min_inclusive": base_itemtype[8],
                        }
                        self.merge_restrictions(current=current_itemtype, base=base)
            self.itemtypes.append(current_itemtype)

    def merge_restrictions(self, current, base):
        if not current["restriction_type"] and base["restriction_type"]:
            current["restriction_type"] = base["restriction_type"]
        if not current["restriction_length"] and base["restriction_length"]:
            current["restriction_length"] = base["restriction_length"]
        if not current["restriction_pattern"] and base["restriction_pattern"]:
            current["restriction_pattern"] = base["restriction_pattern"]
        if not current["restriction_max_length"] and base["restriction_max_length"]:
            current["restriction_max_length"] = base["restriction_max_length"]
        if not current["restriction_min_length"] and base["restriction_min_length"]:
            current["restriction_min_length"] = base["restriction_min_length"]
        if not current["total_digits"] and base["total_digits"]:
            current["total_digits"] = base["total_digits"]
        if not current["fraction_digits"] and base["fraction_digits"]:
            current["fraction_digits"] = base["fraction_digits"]
        if not current["min_inclusive"] and base["min_inclusive"]:
            current["min_inclusive"] = base["min_inclusive"]
