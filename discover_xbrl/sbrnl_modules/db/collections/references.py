class References:
    def __init__(self, db=None):
        self.db = db
        self.references = []
        self._dts_name = None
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def references_of_linkrole(self, linkrole_rowid):
        query = self.db.rewrite_query(
            "select distinct(reference.id), label, name, number, issuedate, chapter, article, note, section, "
            "subsection, publisher, paragraph, subparagraph, clause, subclause, appendix, "
            "example, page, exhibit, footnote, sentence, uri, uridate "
            "from reference left join linkrole_reference  "
            "on reference.id = linkrole_reference.reference_id "
            "left join linkrole l on linkrole_reference.linkrole_rowid = l.rowid "
            "where l.rowid = %s "
        )
        self.cursor.execute(query, (linkrole_rowid,))
        rows = self.cursor.fetchall()
        if len(rows):
            self.cache_rows(rows)

    def references_of_concept(self, concept_id):
        query = self.db.rewrite_query(
            "select distinct(id), label, name, number, issuedate, chapter, article, note, section, "
            "subsection, publisher, paragraph, subparagraph, clause, subclause, appendix, "
            "example, page, exhibit, footnote, sentence, uri, uridate "
            "from reference left join concept_reference "
            "on reference.id = concept_reference.reference_id "
            "where concept_id = %s "
        )
        self.cursor.execute(query, (concept_id,))
        rows = self.cursor.fetchall()
        if len(rows):
            self.cache_rows(rows)

    def references_of_dts(self, dts_name):
        self._dts_name = dts_name
        query = self.db.rewrite_query(
            "select distinct(r.id), label, name, number, issuedate, chapter, article, note, section, "
            "subsection, publisher, paragraph, subparagraph, clause, subclause, appendix, "
            "example, page, exhibit, footnote, sentence, uri, uridate "
            "from dts_concept dc  "
            "left join concept_reference cr on cr.concept_id = dc.concept_id "
            "left join reference r on r.id = cr.reference_id  "
            "where dc.dts_name = %s and directly_referenced"
            "  and r.id is not null "
            "order by r.id"
        )
        self.cursor.execute(query, (dts_name,))
        rows = self.cursor.fetchall()
        if len(rows):
            self.cache_rows(rows)

    def reference_by_id(self, reference_id=None, dts_name=None):
        self._dts_name = dts_name
        query = self.db.rewrite_query(
            "select id, label, name, number, issuedate, chapter, article, note, section, "
            "subsection, publisher, paragraph, subparagraph, clause, subclause, appendix, "
            "example, page, exhibit, footnote, sentence, uri, uridate "
            "from reference "
            "where id = %s "
        )
        self.cursor.execute(query, (reference_id,))
        rows = self.cursor.fetchall()
        if len(rows):
            self.cache_rows(rows)

    def cache_rows(self, rows):
        for row in rows:
            self.references.append(
                {
                    "id": row[0],
                    "label": row[1],
                    "name": row[2],
                    "number": row[3],
                    "issuedate": row[4],
                    "chapter": row[5],
                    "article": row[6],
                    "note": row[7],
                    "section": row[8],
                    "subsection": row[9],
                    "publisher": row[10],
                    "paragraph": row[11],
                    "subparagraph": row[12],
                    "clause": row[13],
                    "subclause": row[14],
                    "appendix": row[15],
                    "example": row[16],
                    "page": row[17],
                    "exhibit": row[18],
                    "footnote": row[19],
                    "sentence": row[20],
                    "uri": row[21],
                    "uridate": row[22],
                }
            )
