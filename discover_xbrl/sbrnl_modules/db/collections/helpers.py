def get_dts_nice_name(dts_name):
    if not dts_name:
        return "unknown"
    nice_name = (
        dts_name.replace("-ti-", "-")
        .replace("rpt", "")
        .replace("-", " ")
        .replace(".xsd", "")
    )
    parts = nice_name.split(" ")
    nice_parts = []
    for i, part in enumerate(parts):
        if i == 0:
            nice_parts.append(part.upper())
        elif part > " ":
            nice_parts.append(part.capitalize())
    return " ".join(nice_parts)
