from discover_xbrl.sbrnl_modules.db.collections.concepts import Concepts
from discover_xbrl.sbrnl_modules.db.collections.itemtypes import ItemTypes
from discover_xbrl.sbrnl_modules.db.collections.labels import Labels
from discover_xbrl.sbrnl_modules.db.collections.linkroles import Linkroles
from discover_xbrl.sbrnl_modules.db.collections.namespaces import Namespaces
from discover_xbrl.sbrnl_modules.db.collections.presentation_hierarchies import (
    PresentationHierarchies,
)
from discover_xbrl.sbrnl_modules.db.collections.helpers import get_dts_nice_name
from discover_xbrl.api.schema import DtsFeatures


class DTSes:
    def __init__(self, db=None, full=False):
        self.db = db
        self.dtses = []
        self.full = full
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def get_all(self, filter=None):
        if not filter:
            self.cursor.execute(
                "select name, shortname, nice_name, description from dts order by name"
            )
        else:
            query = self.db.rewrite_query(
                "select name, shortname from dts where name like %s "
            )
            self.cursor.execute(query, (f"%{filter}%",))
        rows = self.cursor.fetchall()
        for row in rows:
            nicename = get_dts_nice_name(row[0])
            ph = PresentationHierarchies(db=self.db, full=False)
            if not self.full:
                self.dtses.append(
                    {
                        "name": row[0],
                        "shortname": row[1],
                        "nice_name": row[2] if row[2] else nicename,
                        "description": row[3],
                        "has_presentation": ph.dts_has_presentation(row[0]),
                    }
                )
            else:
                full_dts = DTS(db=self.db, full=True)
                self.dtses.append(full_dts.get_dts(name=row[0]))


class DTS:
    def __init__(self, db=None, full=True):
        self.db = db
        self.name = None
        self._nice_name = None
        self.description = None
        self.shortname = None
        self.concepts = []
        self.labels = []
        self.linkroles = []
        self.namespaces = []
        self.presentation_hierarchy = None
        self.full = full
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    @property
    def nice_name(self):
        if self._nice_name:
            return self._nice_name
        else:
            return get_dts_nice_name(self.name)

    def get_dts(self, name=None, referenced_only=True):
        """

        :param name:
        :param directly_referenced: Get only elements that are directly referenced by the EP. This is conform the P/D/T-filter of SBR-NL.
        :return:
        """
        query = self.db.rewrite_query(
            "select name, shortname, nice_name, description from dts where name = %s "
        )
        self.cursor.execute(query, (name,))
        row = self.cursor.fetchone()
        if row:
            self.name = row[0]
            self.shortname = row[1]
            self._nice_name = row[2]
            self.description = row[3]
        else:
            # Did not find the taxonomy in the database.
            raise ValueError(f"Could not find entrypoint '{name}' in database")
        if self.full:
            self.get_relations(referenced_only=referenced_only)

    def get_relations(self, referenced_only=True):
        concepts = Concepts(db=self.db, full=self.full)
        concepts.concepts_of_dts(dts_name=self.name, referenced_only=referenced_only)
        self.concepts = concepts.concepts

        labels = Labels(db=self.db)
        labels.labels_of_dts(dts_name=self.name, referenced_only=referenced_only)
        self.labels = labels.labels

        linkroles = Linkroles(db=self.db, full=self.full)
        linkroles.linkroles_from_dts(
            dts_name=self.name, referenced_only=referenced_only
        )
        self.linkroles = linkroles.linkroles

        namespaces = Namespaces(db=self.db, full=self.full)
        namespaces.namespaces_of_dts(dts_name=self.name)
        self.namespaces = namespaces.namespaces

        itemtypes = ItemTypes(db=self.db, full=self.full)
        itemtypes.itemtypes_of_dts(dts_name=self.name)
        self.itemtypes = itemtypes.itemtypes

        presentation_hierarchies = PresentationHierarchies(db=self.db, full=self.full)
        presentation_hierarchies.table_of_contents_of_dts(dts_name=self.name)
        if len(presentation_hierarchies.presentation_hierarchies):
            self.presentation_hierarchy = presentation_hierarchies.presentation_hierarchies[
                0
            ]

    def dts_features(self, dts_name=None):
        # Determine which features this Entrypoint possesses
        features = DtsFeatures()
        try:
            self.get_dts(name=dts_name)
        except ValueError:
            return features
        query = self.db.rewrite_query(
            "select rowid from presentation_hierarchy_node where dts_name = %s and is_root "
        )
        self.cursor.execute(query, (dts_name,))
        rows = self.cursor.fetchall()
        features.toc = len(rows) > 0

        query = self.db.rewrite_query(
            "select l.rowid from linkrole l "
            "left join linkrole_table lt on lt.linkrole_rowid = l.rowid "
            "where l.dts_name = %s"
        )
        self.cursor.execute(query, (dts_name,))
        rows = self.cursor.fetchall()
        features.tables = len(rows) > 0

        query = self.db.rewrite_query(
            "select distinct(hc_role_uri), linkrole_rowid, hc_type, context_element, closed, definition "
            "from hypercube "
            "left join linkrole on linkrole.rowid = linkrole_rowid "
            "where dts_name = %s "
        )
        self.cursor.execute(query, (dts_name,))
        rows = self.cursor.fetchall()
        features.definition = len(rows) > 0

        query = self.db.rewrite_query(
            "select concept_id, min(dimension.rowid), min(hypercube_linkrole_rowid), min(target_linkrole),"
            "min(dimension.linkrole_rowid) "
            "from dimension "
            "left join hypercube h on dimension.hypercube_linkrole_rowid = h.linkrole_rowid "
            "left join linkrole l on h.linkrole_rowid = l.rowid "
            "where l.dts_name = %s "
            "group by concept_id "
        )
        self.cursor.execute(query, (dts_name,))
        rows = self.cursor.fetchall()
        features.dimensions = len(rows) > 0

        query = self.db.rewrite_query(
            "select distinct(r.id), label, name, number, issuedate, chapter, article, note, section, "
            "subsection, publisher, paragraph, subparagraph, clause, subclause, appendix, "
            "example, page, exhibit, footnote, sentence, uri, uridate "
            "from dts_concept dc  "
            "left join concept_reference cr on cr.concept_id = dc.concept_id "
            "left join reference r on r.id = cr.reference_id  "
            "where dc.dts_name = %s and directly_referenced"
            "  and r.id is not null "
            "order by r.id"
        )
        self.cursor.execute(query, (dts_name,))
        rows = self.cursor.fetchall()
        features.references = len(rows) > 0

        query = self.db.rewrite_query(
            "select xbrl_table.id, label, parent_child_order, linkrole_rowid, l.id from xbrl_table "
            "left join linkrole_table lt on xbrl_table.id = lt.table_id "
            "left join linkrole l on lt.linkrole_rowid = l.rowid "
            "where l.dts_name = %s "
            "order by l._order * 1"
        )
        self.cursor.execute(query, (dts_name,))
        rows = self.cursor.fetchall()
        features.tables = len(rows) > 0

        query = self.db.rewrite_query(
            "select distinct(assertion.id), aspect_model, implicit_filtering, xlink_label, test, "
            "assertion_type,severity from assertion "
            "left join linkrole_assertion la on assertion.id = la.assertion_id "
            "left join linkrole l on l.rowid = la.linkrole_rowid "
            "where l.dts_name = %s and directly_referenced "
        )
        self.cursor.execute(query, (dts_name,))
        rows = self.cursor.fetchall()
        features.formulas = len(rows) > 0
        if not features.toc:
            if features.tables or features.definition:
                features.toc = True
        # todo: presentations!
        return features


def main():
    from discover_xbrl.sbrnl_modules.db.connection import get_connection
    import json

    db = get_connection()

    # dtses = DTSes(db=db, full=False)
    # dtses.get_all()
    # print(dtses.dtses)
    # print(len(dtses.dtses))
    # ocw-rpt-jaarverantwoording-2021-nlgaap-onderwijsinstellingen.xsd
    # kvk-rpt-jaarverantwoording-2021-nlgaap-micro.xsd

    dts = DTS(db=db)
    dts.get_dts(name="ocw-rpt-jaarverantwoording-2021-nlgaap-onderwijsinstellingen.xsd")
    dts.db = None
    dts.cursor = None
    as_json = json.dumps(dts.__dict__, indent=4)
    with open(f"/tmp/from_sql_{dts.name}.json", "w+") as jsonfile:
        jsonfile.write(as_json)


if __name__ == "__main__":
    main()
