from discover_xbrl.sbrnl_modules.db.collections.hypercubes import HyperCubes
from discover_xbrl.sbrnl_modules.db.collections.concepts import Concepts
from discover_xbrl.sbrnl_modules.db.collections.labels import Labels
from discover_xbrl.sbrnl_modules.db.collections.tables import Tables


class PresentationHierarchies:
    def __init__(self, db=None, full=True):
        self.db = db
        self.full = full
        self.presentation_hierarchies = []
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()
        self._dts_name = None

    def get_all(self, filter=None):
        if not filter:
            query = self.db.rewrite_query(
                "select rowid, id, node_type, concept_id, linkrole_role_uri, is_root, "
                "_order, preferred_label, parent_rowid, dts_name, parent_linkrole_rowid, "
                "directly_referenced "
                "from presentation_hierarchy_node "
                "where is_root"
            )
            self.cursor.execute(query)
        else:
            query = self.db.rewrite_query(
                "select rowid, id, node_type, concept_id, linkrole_role_uri, is_root, "
                "_order, preferred_label, parent_rowid, dts_name, parent_linkrole_rowid,"
                "directly_referenced "
                "from presentation_hierarchy_node "
                "where is_root "
                "and (concept_id like %s or preferred_label like %s or id like %s or linkrole_role_uri like %s)"
            )
            self.cursor.execute(
                query, (f"%{filter}%", f"%{filter}%", f"%{filter}%", f"%{filter}%")
            )
        rows = self.cursor.fetchall()
        if len(rows):
            self.cache_rows(rows)
        self.get_child_nodes(self.presentation_hierarchies)

    def get_by_rowid(self, rowid=None):
        query = self.db.rewrite_query(
            "select rowid, id, node_type, concept_id, linkrole_role_uri, is_root, "
            "_order, preferred_label, parent_rowid, dts_name, parent_linkrole_rowid,"
            "directly_referenced "
            "from presentation_hierarchy_node "
            "where rowid = %s "
        )
        self.cursor.execute(query, (rowid,))
        rows = self.cursor.fetchall()
        if len(rows):
            self.cache_rows(rows)
            self.get_child_nodes(self.presentation_hierarchies)

    def get_by_id(self, id=None):
        query = self.db.rewrite_query(
            "select rowid, id, node_type, concept_id, linkrole_role_uri, is_root, "
            "_order, preferred_label, parent_rowid, dts_name, parent_linkrole_rowid,"
            "directly_referenced "
            "from presentation_hierarchy_node "
            "where id = %s "
            "and dts_name > ''"
        )
        self.cursor.execute(query, (id,))
        rows = self.cursor.fetchall()
        if len(rows):
            self.cache_rows(rows)
            self.get_child_nodes(self.presentation_hierarchies)

    def table_of_contents_of_dts(self, dts_name=None):
        # The TOC is a strange presentation_node, if the TOC exists
        # it can be found by dts_name. the other presentation nodes do NOT get this attribute
        self._dts_name = dts_name
        if not dts_name:
            return False
        query = self.db.rewrite_query(
            "select rowid, id, node_type, concept_id, linkrole_role_uri, is_root, "
            "_order, preferred_label, parent_rowid, dts_name, parent_linkrole_rowid, "
            "directly_referenced "
            " from presentation_hierarchy_node "
            "where dts_name = %s and is_root "
        )
        self.cursor.execute(query, (dts_name,))
        rows = self.cursor.fetchall()
        if rows:
            self.cache_rows(rows)
        self.get_child_nodes(self.presentation_hierarchies, toc=True)

    def presentation_hierarchies_of_dts(self, dts_name=None):
        """ We only deliver the top level (root) nodes. To see the details you need to call get_by_rowid """
        self._dts_name = dts_name
        query = self.db.rewrite_query(
            "select phn.rowid, phn.id, node_type, concept_id, linkrole_role_uri, "
            "is_root, phn._order, preferred_label, parent_rowid, phn.dts_name, parent_linkrole_rowid, "
            "phn.directly_referenced "
            "from presentation_hierarchy_node phn "
            "left join linkrole l on l.rowid = phn.parent_linkrole_rowid "
            "where l.dts_name = %s and is_root and l.directly_referenced "
            "  and l.role_uri != 'http://www.xbrl.org/2008/role/link'"  # this represents the T.O.C we don't need it
            "order by l._order * 1"
        )
        self.cursor.execute(query, (dts_name,))
        rows = self.cursor.fetchall()
        if rows:
            self.cache_rows(rows)

    def presentation_hierarchies_of_linkrole(
        self, parent_linkrole_rowid=None, dts_name=None
    ):
        if not parent_linkrole_rowid:
            return False
        self._dts_name = dts_name
        query = self.db.rewrite_query(
            "select rowid, id, node_type, concept_id, linkrole_role_uri, is_root, "
            "_order, preferred_label, parent_rowid, dts_name, parent_linkrole_rowid,"
            "directly_referenced "
            "from presentation_hierarchy_node "
            "where parent_linkrole_rowid = %s "
            "order by _order * 1"
        )
        self.cursor.execute(query, (parent_linkrole_rowid,))
        rows = self.cursor.fetchall()
        if rows:
            self.cache_rows(rows)
        self.get_child_nodes(self.presentation_hierarchies)
        if self.full:
            # dit gebeurt in cache_rows!!
            pass

    def alternative_presentation(self, dts_name=None, qname=None, role_uri=None):
        if not dts_name or not qname or not role_uri:
            return False
        query = self.db.rewrite_query(
            "select phn.rowid, phn.id, node_type, concept_id, linkrole_role_uri, is_root, "
            "phn._order, preferred_label, parent_rowid, phn.dts_name, parent_linkrole_rowid,"
            "phn.directly_referenced "
            "from linkrole lr "
            "left join presentation_hierarchy_node phn on lr.rowid = phn.parent_linkrole_rowid "
            "where lr.role_uri = %s "
            "and lr.dts_name = %s "
        )
        self.cursor.execute(query, (role_uri, dts_name))
        rows = self.cursor.fetchall()
        if len(rows):
            self.cache_rows(rows)
            self.get_child_nodes(self.presentation_hierarchies)
            for node in self.presentation_hierarchies:
                subtree = self.check_qname(qname, node)
                if subtree:
                    self.presentation_hierarchies = [subtree]

    def check_qname(self, qname, node):
        if (
            node["node_type"] == "concept"
            and node["concept"]["ns_prefix"] == qname.split(":")[0]
            and node["concept"]["name"] == qname.split(":")[1]
        ):
            return node
        if node.get("children"):
            for child in node["children"]:
                found_node = self.check_qname(qname, child)
                if found_node:
                    return found_node
        return False

    def dts_has_presentation(self, dts_name=None):
        query = self.db.rewrite_query(
            "select rowid from presentation_hierarchy_node "
            "where is_root and dts_name = %s "
        )
        self.cursor.execute(query, (dts_name,))
        rows = self.cursor.fetchall()
        return len(rows) > 0

    def find_dts_name(self, rowid):
        query = self.db.rewrite_query("select dts_name from linkrole where rowid = %s")
        self.cursor.execute(query, (rowid,))
        row = self.cursor.fetchone()
        if row:
            self._dts_name = row[0]

    def cache_rows(self, rows):
        for count, row in enumerate(rows):
            if self._dts_name is None:
                self.find_dts_name(row[10])

            node = {
                "rowid": row[0],
                "id": row[1],
                "node_type": row[2],
                "concept_id": row[3],
                "linkrole_role_uri": row[4],
                "is_root": True if row[5] > 0 else False,
                "order": row[6],
                "preferred_label": row[7],
                "parent_rowid": row[8],
                "dts_name": row[9],
                "parent_linkrole_rowid": row[10],
                "directly_referenced": row[11],
            }
            if row[9]:
                self._dts_name = row[9]
            if self.full:
                self.get_relations(node)
            self.presentation_hierarchies.append(node)

    def get_child_nodes(self, nodes=None, toc=None):
        for presentation_hierarchy in nodes:
            if toc:
                query = self.db.rewrite_query(
                    "select rowid, id, node_type, concept_id, linkrole_role_uri, is_root, _order, "
                    "preferred_label, parent_rowid, dts_name, parent_linkrole_rowid,"
                    "directly_referenced "
                    "from presentation_hierarchy_node "
                    "where parent_rowid = %s "
                    "  and directly_referenced "
                    "order by _order * 1 "
                )
                self.cursor.execute(query, (presentation_hierarchy["rowid"],))
            else:
                query = self.db.rewrite_query(
                    "select rowid, id, node_type, concept_id, linkrole_role_uri, is_root, _order, "
                    "preferred_label, parent_rowid, dts_name, parent_linkrole_rowid,"
                    "directly_referenced "
                    "from presentation_hierarchy_node "
                    "where parent_rowid = %s "
                    "and id is not null "
                    "order by _order * 1"
                )
                self.cursor.execute(query, (presentation_hierarchy["rowid"],))
            children = self.cursor.fetchall()

            if len(children):
                presentation_hierarchy["children"] = []
                for row in children:
                    child = {
                        "rowid": row[0],
                        "id": row[1],
                        "node_type": row[2],
                        "concept_id": row[3],
                        "linkrole_role_uri": row[4],
                        "is_root": True if row[5] > 0 else False,
                        "order": row[6],
                        "preferred_label": row[7],
                        "parent_rowid": row[8],
                        "dts_name": row[9],
                        "parent_linkrole_rowid": row[10],
                        "directly_referenced": row[11],
                    }
                    if self.get_relations(child):
                        self.get_child_nodes([child], toc=toc)
                        presentation_hierarchy["children"].append(child)

    def get_relations(self, node):
        if node["linkrole_role_uri"]:
            rowid, directly_referenced = self.linkrole_rowid_by_role_uri_dts_name(
                dts_name=self._dts_name, role_uri=node["linkrole_role_uri"]
            )
            if not directly_referenced:
                return False

            hypercubes = HyperCubes(db=self.db, full=False)
            hypercubes.hypercube_of_linkrole(
                linkrole_rowid=rowid, dts_name=self._dts_name
            )
            if len(hypercubes.hypercubes):
                node["hypercube"] = hypercubes.hypercubes[0]
            tables = Tables(db=self.db, full=False)
            tables.tables_of_linkrole(linkrole_rowid=rowid)
            if len(tables.tables):
                node["tables"] = tables.tables
            node["definition"] = self.definition_of_linkrole(rowid)
            node["linkrole_id"] = self.id_of_linkrole(rowid)
            node["has_presentation"] = self.linkrole_has_presentation(rowid)
            labels = Labels(db=self.db)
            labels.labels_of_linkrole(linkrole_rowid=rowid)
            node["labels"] = labels.labels

        else:
            concepts = Concepts(db=self.db)
            concepts.concept_by_id(node["concept_id"], dts_name=self._dts_name)
            node["concept"] = concepts.concepts[0] if len(concepts.concepts) else None
        return True

    #  this should not be here, but linkrole is overloaded with imports as is dts,
    #  it get's circulair quite fast
    def linkrole_rowid_by_role_uri_dts_name(self, dts_name=None, role_uri=None):
        if dts_name is None or role_uri is None:
            return False, False
        query = self.db.rewrite_query(
            "select rowid, directly_referenced from linkrole "
            "where dts_name = %s and role_uri = %s "
        )
        self.cursor.execute(query, (dts_name, role_uri))
        rowid = self.cursor.fetchone()
        return rowid[0], rowid[1]

    def definition_of_linkrole(self, rowid):
        query = self.db.rewrite_query(
            "select definition from linkrole where rowid = %s "
        )
        self.cursor.execute(query, (rowid,))
        row = self.cursor.fetchone()
        return row[0] if row else ""

    def id_of_linkrole(self, rowid):
        query = self.db.rewrite_query("select id from linkrole where rowid = %s ")
        self.cursor.execute(query, (rowid,))
        row = self.cursor.fetchone()
        return row[0] if row else ""

    def linkrole_has_presentation(self, rowid):
        query = self.db.rewrite_query(
            "select id from presentation_hierarchy_node "
            "where is_root and parent_linkrole_rowid = %s "
        )
        self.cursor.execute(query, (rowid,))
        row = self.cursor.fetchone()
        if row:
            return True
        else:
            return False
