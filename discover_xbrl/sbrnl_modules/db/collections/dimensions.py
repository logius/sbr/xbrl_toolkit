from discover_xbrl.sbrnl_modules.db.collections.members import Members
from discover_xbrl.sbrnl_modules.db.collections.domains import Domains
from discover_xbrl.sbrnl_modules.db.collections.concepts import Concepts


class Dimensions:
    def __init__(self, db=None, full=True):
        self.db = db
        self.dimensions = []
        self.full = full
        self._dts_name = None
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def dimensions_of_hypercube(self, hypercube_linkrole_rowid=None, dts_name=None):
        self._dts_name = dts_name
        query = self.db.rewrite_query(
            "select concept_id, rowid, hypercube_linkrole_rowid, target_linkrole, linkrole_rowid "
            "from dimension "
            "where hypercube_linkrole_rowid = %s "
        )
        self.cursor.execute(query, (hypercube_linkrole_rowid,))
        rows = self.cursor.fetchall()
        if rows:
            self.cache_rows(rows)
        if self.full:
            self.get_relations()

    def dimensions_of_linkrole(self, linkrole_rowid, dts_name=None):
        if not linkrole_rowid or not dts_name:
            return False
        self._dts_name = dts_name
        query = self.db.rewrite_query(
            "select concept_id, dimension.rowid, hypercube_linkrole_rowid, target_linkrole, "
            "linkrole_rowid "
            "from dimension "
            "left join linkrole l on l.rowid = dimension.linkrole_rowid "
            "where l.rowid = %s and l.dts_name = %s"
        )
        self.cursor.execute(query, (linkrole_rowid, dts_name))
        rows = self.cursor.fetchall()
        if rows:
            self.cache_rows(rows)
        if self.full:
            self.get_relations()

    def dimensions_of_target_linkrole(self, target_linkrole=None, dts_name=None):
        if not target_linkrole or not dts_name:
            return False
        # to check for existence in the current dts, we have to do
        # hypercube of dimension, linkrole of hypercube, where linkrole in dts
        self._dts_name = dts_name
        query = self.db.rewrite_query(
            "select concept_id, min(dimension.rowid), min(hypercube_linkrole_rowid), min(target_linkrole), "
            "min(dimension.linkrole_rowid) "
            "from dimension "
            "left join hypercube h on dimension.hypercube_linkrole_rowid = h.linkrole_rowid "
            "left join linkrole l on h.linkrole_rowid = l.rowid "
            "where target_linkrole = %s "
            "and l.dts_name = %s "
            "group by concept_id "
        )
        self.cursor.execute(query, (target_linkrole, dts_name))
        rows = self.cursor.fetchall()
        if rows:
            self.cache_rows(rows)
        if self.full:
            self.get_relations()

    def dimensions_of_dts(self, dts_name=None):
        if not dts_name:
            return False
        self._dts_name = dts_name
        query = self.db.rewrite_query(
            "select concept_id, min(dimension.rowid), min(hypercube_linkrole_rowid), min(target_linkrole),"
            "min(dimension.linkrole_rowid) "
            "from dimension "
            "left join hypercube h on dimension.hypercube_linkrole_rowid = h.linkrole_rowid "
            "left join linkrole l on h.linkrole_rowid = l.rowid "
            "where l.dts_name = %s "
            "group by concept_id "
        )
        self.cursor.execute(query, (dts_name,))
        rows = self.cursor.fetchall()
        if rows:
            self.cache_rows(rows)
        if self.full:
            self.get_relations()

    def dimension_by_rowid(self, rowid=None, dts_name=None):
        if not rowid:
            return False
        self._dts_name = dts_name
        query = self.db.rewrite_query(
            "select concept_id, dimension.rowid, hypercube_linkrole_rowid, target_linkrole, linkrole_rowid "
            "from dimension where rowid = %s "
        )
        self.cursor.execute(query, (rowid,))
        rows = self.cursor.fetchall()
        if rows:
            self.cache_rows(rows)
        if self.full:
            self.get_relations()

    def alternative_dimensions(self, dts_name=None, qname=None, linkrole_role_uri=None):
        if not dts_name or not qname:
            return False
        self._dts_name = dts_name
        if linkrole_role_uri:
            query = self.db.rewrite_query(
                "select concept_id, dimension.rowid, hypercube_linkrole_rowid, target_linkrole, linkrole_rowid "
                "from linkrole l "
                "left join dimension on dimension.hypercube_linkrole_rowid = l.rowid "
                "left join concept c on c.id = dimension.concept_id "
                "where l.dts_name = %s  and l.directly_referenced "
                "and c.ns_prefix || ':' || c.name = %s "
                "and l.role_uri = %s "
                "limit 1"
            )
            self.cursor.execute(query, (dts_name, qname, linkrole_role_uri))
        else:
            query = self.db.rewrite_query(
                "select concept_id, dimension.rowid, hypercube_linkrole_rowid, target_linkrole, linkrole_rowid "
                "from linkrole l "
                "left join dimension on dimension.hypercube_linkrole_rowid = l.rowid "
                "left join concept c on c.id = dimension.concept_id "
                "where l.dts_name = %s  and l.directly_referenced "
                "and c.ns_prefix || ':' || c.name = %s "
                "limit 1"
            )
            self.cursor.execute(query, (dts_name, qname))
        rows = self.cursor.fetchall()
        if rows:
            self.cache_rows(rows)
        if self.full:
            self.get_relations()

    def cache_rows(self, rows):
        for row in rows:
            self.dimensions.append(
                {
                    "concept_id": row[0],
                    "rowid": row[1],
                    "hypercube_linkrole_rowid": row[2],
                    "target_linkrole": row[3],
                    "linkrole_rowid": row[4],
                    "domains": None,
                    "members": None,
                }
            )

    def get_relations(self):
        for dimension in self.dimensions:
            domains = Domains(db=self.db, full=self.full)
            domains.domains_of_dimension(dimension_rowid=dimension["rowid"])
            dimension["domains"] = domains.domains
            domain_members = Members(db=self.db, full=self.full)
            domain_members.members_of_dimension(
                dimension_id=dimension["rowid"], dts_name=self._dts_name
            )
            dimension["members"] = domain_members.members
            concept = Concepts(db=self.db, full=self.full)
            concept.concept_by_id(dimension["concept_id"], dts_name=self._dts_name)
            dimension["concept"] = concept.concepts[0]
