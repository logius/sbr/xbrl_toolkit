from discover_xbrl.sbrnl_modules.db.collections.concepts import Concepts
from discover_xbrl.sbrnl_modules.db.collections.members import Members


class Domains:
    def __init__(self, db=None, full=True):
        self.db = db
        self.domains = []
        self.full = full
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def domains_of_dimension(self, dimension_rowid):
        query = self.db.rewrite_query(
            "select domain.rowid, concept_id, target_linkrole, usable, dimension_rowid "
            "from domain where dimension_rowid = %s "
        )
        self.cursor.execute(query, (dimension_rowid,))
        rows = self.cursor.fetchall()
        if len(rows):
            for row in rows:
                self.domains.append(
                    {
                        "rowid": row[0],
                        "concept_id": row[1],
                        "target_linkrole": row[2],
                        "usable": row[3],
                        "dimension_rowid": row[4],
                        "type": "domain",
                    }
                )
            if self.full:
                self.get_relations()

    def get_relations(self):
        for domain in self.domains:
            domain_members = Members(db=self.db, full=self.full)
            domain_members.members_of_domain(domain_id=domain["rowid"])
            domain["members"] = domain_members.members
            concept = Concepts(db=self.db, full=self.full)
            concept.concept_by_id(domain["concept_id"])
            domain["concept"] = concept.concepts[0]
