from discover_xbrl.sbrnl_modules.db.collections.table_breakdowns import TableBreakdowns
from discover_xbrl.sbrnl_modules.db.collections.labels import Labels
from discover_xbrl.sbrnl_modules.db.collections.parameters import Parameters
from discover_xbrl.sbrnl_modules.db.collections.variables import Variables


class Tables:
    def __init__(self, db=None, full=True):
        self.db = db
        self.full = full
        self.tables = []
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def tables_of_linkrole(self, linkrole_rowid=None):
        if not linkrole_rowid:
            return False
        query = self.db.rewrite_query(
            "select xbrl_table.id, label, parent_child_order, linkrole_rowid, l.id from xbrl_table "
            "left join linkrole_table on xbrl_table.id = linkrole_table.table_id "
            "left join linkrole l on linkrole_table.linkrole_rowid = l.rowid "
            "where linkrole_table.linkrole_rowid = %s "
        )
        self.cursor.execute(query, (linkrole_rowid,))
        rows = self.cursor.fetchall()
        if len(rows):
            self.cache_rows(rows)
        if self.full:
            self.get_relations()
        self.get_labels()

    def tables_of_dts(self, dts_name=None):
        if not dts_name:
            return False
        query = self.db.rewrite_query(
            "select xbrl_table.id, label, parent_child_order, linkrole_rowid, l.id from xbrl_table "
            "left join linkrole_table lt on xbrl_table.id = lt.table_id "
            "left join linkrole l on lt.linkrole_rowid = l.rowid "
            "where l.dts_name = %s "
            "order by l._order * 1"
        )
        self.cursor.execute(query, (dts_name,))
        rows = self.cursor.fetchall()
        if len(rows):
            self.cache_rows(rows)

        if self.full:
            self.get_relations()
        self.get_labels()

    def get_table_of_dts_by_id(self, dts_name=None, table_id=None):
        if not dts_name or not table_id:
            return False
        query = self.db.rewrite_query(
            "select xbrl_table.id, label, parent_child_order, linkrole_rowid, l.id from xbrl_table "
            "left join linkrole_table lt on xbrl_table.id = lt.table_id "
            "left join linkrole l on lt.linkrole_rowid = l.rowid "
            "where xbrl_table.id = %s and l.dts_name = %s "
        )
        self.cursor.execute(query, (table_id, dts_name))
        rows = self.cursor.fetchall()
        if len(rows):
            self.cache_rows(rows)

        if self.full:
            self.get_relations()
        self.get_labels()

    def cache_rows(self, rows):
        for row in rows:
            self.tables.append(
                {
                    "id": row[0],
                    "label": row[1],
                    "parent_child_order": row[2],
                    "linkrole_rowid": row[3],
                    "linkrole_id": row[4],
                }
            )

    def get_relations(self):
        for table in self.tables:
            table_breakdowns = TableBreakdowns(db=self.db, full=self.full)
            table_breakdowns.table_breakdowns_of_table(table_id=table["id"])
            table["table_breakdowns"] = table_breakdowns.table_breakdowns

            parameters = Parameters(db=self.db)
            parameters.parameters_of_table(table_id=table["id"])
            table["parameters"] = parameters.parameters

            variables = Variables(db=self.db, full=self.full)
            variables.variables_of_linkrole(linkrole_rowid=table["linkrole_rowid"])
            table["variables"] = variables.variables

    def get_labels(self):
        for table in self.tables:
            labels = Labels(db=self.db)
            labels.labels_of_table(table_id=table["id"])
            table["labels"] = labels.labels
