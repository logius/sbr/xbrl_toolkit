from discover_xbrl.sbrnl_modules.db.connection import get_connection
from discover_xbrl.sbrnl_modules.db.collections.concepts import Concepts


class LabelBackreferences:
    def __init__(self, db=None):
        self.db = db
        self.back_references = []
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def back_references_of_label(self, label_id, dts_name, role=None, lang=None):
        # Concepts
        query = self.db.rewrite_query(
            "select c.name, cl.concept_id, label.id "
            "from label "
            "left join concept_label cl on label.link_label = cl.label_link_label "
            " and cl.label_link_role = label.link_role and cl.label_lang = label.lang "
            "left join dts_concept dc on cl.concept_id = dc.concept_id "
            "left join concept c on cl.concept_id = c.id "
            "where label.id = %s "
            "and dc.dts_name = %s "
            "and cl.dts_name = %s "
            "and cl.label_link_role = %s "
            "and cl.label_lang = %s "
        )
        self.cursor.execute(query, (label_id, dts_name, dts_name, role, lang))
        rows = self.cursor.fetchall()
        for row in rows:
            concepts = Concepts(db=self.db, full=True)
            concepts.concept_by_id(row[1], dts_name=dts_name)
            self.back_references.append(
                {
                    "type": "Concept",
                    "source": "Concept",
                    "concept_name": row[0],
                    "concept_id": row[1],
                    "concept": concepts.concepts[0] if len(concepts.concepts) else None,
                }
            )
        # Enumerations
        query = self.db.rewrite_query(
            "select di.itemtype_name "
            "from label "
            "left join enumeration_option_label eol on label.link_label = eol.label_link_label "
            "left join enumeration_option eo on eol.enumeration_option_id = eo.id "
            "left join itemtype_enumeration_option ieo on eo.id = ieo.enumeration_option_id "
            "left join itemtype i on ieo.itemtype_name = i.name "
            "left join dts_itemtype di on i.name = di.itemtype_name "
            "where label.id = %s "
            "and di.dts_name = %s "
        )
        self.cursor.execute(query, (label_id, dts_name))
        rows = self.cursor.fetchall()
        print(f"Aantal enumeration options {len(rows)}")
        for row in rows:
            self.back_references.append(
                {
                    "type": "Enumeration option",
                    "source": "Itemtype",
                    "itemtype_name": row[0],
                    "concept_id": None,
                }
            )

        query = self.db.rewrite_query(
            "select xtl.xbrl_table_id  "
            "from label "
            "left join xbrl_table_label xtl on label.link_label = xtl.label_link_label "
            "left join linkrole_table lt on lt.table_id = xtl.xbrl_table_id "
            "left join linkrole l on lt.linkrole_rowid = l.rowid "
            "where label.id = %s "
            "and l.dts_name = %s "
        )
        self.cursor.execute(query, (label_id, dts_name))
        rows = self.cursor.fetchall()
        for row in rows:
            self.back_references.append(
                {
                    "type": "Table",
                    "source": "Table",
                    "table_id": row[0],
                    "concept_id": None,
                }
            )

        query = self.db.rewrite_query(
            "select xbrl_table.id "
            "from label "
            "left join table_rulenode_label trl on label.link_label = trl.label_link_label "
            "left join table_rulenode tr on trl.table_rulenode_id = tr.id "
            "left join xbrl_table on tr.parent = xbrl_table.id "
            "left join linkrole_table lt on xbrl_table.id = lt.table_id "
            "left join linkrole l on lt.linkrole_rowid = l.rowid "
            "where label.id = %s "
            "and l.dts_name = %s "
        )
        self.cursor.execute(query, (label_id, dts_name))
        rows = self.cursor.fetchall()
        # this is untested, and only covers top rulenodes (parent to table is implicit!)
        for row in rows:
            self.back_references.append(
                {"type": "Rulenode", "source": "Table", "table_id": row[0]}
            )
        query = self.db.rewrite_query(
            "select linkrole.rowid, linkrole.role_uri from linkrole "
            "left join linkrole_label ll on linkrole.rowid = ll.linkrole_rowid "
            "left join label on ll.label_id = label.id and ll.label_lang = label.lang "
            "  and ll.label_link_role = label.link_role and label.link_label = ll.label_link_label "
            "where label.id = %s "
            " and linkrole.dts_name = %s "
        )
        self.cursor.execute(query, (label_id, dts_name))
        rows = self.cursor.fetchall()
        # this is untested, and only covers top rulenodes (parent to table is implicit!)
        for row in rows:
            self.back_references.append(
                {
                    "type": "Linkrole",
                    "source": "Linkrole",
                    "linkrole_rowid": row[0],
                    "concept_name": row[1],
                    "concept_id": None,
                }
            )


def main(label_ids):
    for label_id in label_ids:
        lb = LabelBackreferences(db=get_connection())
        lb.back_references_of_label(
            dts_name="kvk-rpt-jaarverantwoording-2021-nlgaap-micro.xsd",
            label_id=label_id,
        )
        print(f"{lb.back_references}")


if __name__ == "__main__":
    link_labels = [
        "rj-dm_OtherAssetsOtherTypedMember_label_en",
        "kvk-lr_BalanceSheetMicroEntitiesIndicationNoAccruedAssetsLiabilitiesAreRecorded_label_en",
        "sbi_93119_label_nl",
        "kvk-table_2PeriodesTypeJaarrekeningMergedMemberInstant_NotesNoncurrentLiabilitiesFinanceLeaseLiabilitiesDiscountedValueMaturityBreakdownTable_label_nl",
        "kvk-table_2PeriodesTypeJaarrekeningInstant_NotesCurrentLiabilitiesBreakdownTable-x.1.1.1_label_en",
        "kvk-abstr_NotesGrossOperatingResultTitle_label_en",
        "rj-dim_SegmentsReportingAxis_label_nl",
    ]
    main(link_labels)
