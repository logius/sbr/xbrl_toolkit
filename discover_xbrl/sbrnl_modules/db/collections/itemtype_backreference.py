from discover_xbrl.sbrnl_modules.db.collections.concepts import Concepts


class ItemtypeBackreferences:
    def __init__(self, db=None):
        self.db = db
        self.back_references = []
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()
        self.concepts = Concepts(db=db, full=True)
        self._dts_name = None

    def backreferences_of_itemtype(self, itemtype_name=None, dts_name=None):
        if not itemtype_name or not dts_name:
            return False
        self._dts_name = dts_name
        query = self.db.rewrite_query(
            "select c.id, c.name "
            "from  concept c "
            "left join dts_concept dc on c.id = dc.concept_id "
            "where c.itemtype_name = %s "
            "and dts_name = %s "
            "order by c.name "
        )
        self.cursor.execute(query, (itemtype_name, dts_name))
        rows = self.cursor.fetchall()
        for row in rows:
            self.concepts.concepts = []
            self.concepts.concept_by_id(id=row[0], dts_name=self._dts_name)
            concept = self.concepts.concepts[0] if len(self.concepts.concepts) else None
            self.back_references.append(
                {
                    "type": "Concept",
                    "source": "Concept",
                    "concept_id": row[0],
                    "concept_name": row[1],
                    "concept": concept,
                }
            )
