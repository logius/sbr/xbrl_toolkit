from discover_xbrl.sbrnl_modules.db.collections.concepts import Concepts


class LineItems:
    def __init__(self, db=None):
        self.db = db
        self.line_items = []
        self._dts_name = None
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def line_items_of_hypercube(self, hypercube_linkrole_rowid, dts_name=None):
        query = self.db.rewrite_query(
            "select _order, hypercube_linkrole_rowid, concept_id, parent_concept_id  from line_item "
            "where hypercube_linkrole_rowid = %s "
            "and parent_concept_id is null order by _order * 1"
        )
        self._dts_name = dts_name
        self.cursor.execute(query, (hypercube_linkrole_rowid,))
        rows = self.cursor.fetchall()
        if rows:
            self.cache_rows(rows)
            self.get_relations()

    def line_items_of_line_items(
        self, hypercube_linkrole_rowid, concetpt_id, dts_name=None
    ):
        query = self.db.rewrite_query(
            "select _order, hypercube_linkrole_rowid, concept_id, parent_concept_id  from line_item "
            "where hypercube_linkrole_rowid = %s "
            "and parent_concept_id = %s order by _order * 1"
        )
        self._dts_name = dts_name
        self.cursor.execute(query, (hypercube_linkrole_rowid, concetpt_id))
        rows = self.cursor.fetchall()
        if rows:
            self.cache_rows(rows)
            self.get_relations()

    def cache_rows(self, rows):
        for row in rows:
            concepts = Concepts(db=self.db)
            concepts.concept_by_id(row[2], dts_name=self._dts_name)
            self.line_items.append(
                {
                    "order": row[0],
                    "hypercube_linkrole_rowid": row[1],
                    "concept_id": row[2],
                    "concept": concepts.concepts[0] if len(concepts.concepts) else None,
                    "parent_concept_id": row[3],
                    "line_items": None,
                }
            )

    def get_relations(self):
        for row in self.line_items:
            own_line_items = LineItems(db=self.db)
            own_line_items.line_items_of_line_items(
                row["hypercube_linkrole_rowid"],
                row["concept_id"],
                dts_name=self._dts_name,
            )
            if len(own_line_items.line_items) > 0:
                row["line_items"] = own_line_items.line_items
