from discover_xbrl.sbrnl_modules.db.collections.variables import Variables


class Variablesets:
    def __init__(self, db=None, full=True):
        self.db = db
        self.full = full
        self.variablesets = []
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def variablesets_of_assertion(self, assertion_id=None):
        if not assertion_id:
            return False
        query = self.db.rewrite_query(
            "select name from variableset "
            "left join assertion_variableset av on variableset.name = av.variableset_name "
            "where assertion_id = %s "
        )
        self.cursor.execute(query, (assertion_id,))
        rows = self.cursor.fetchall()
        for row in rows:
            self.variablesets.append({"name": row[0]})
        if self.full:
            self.get_relations()

    def get_relations(self):
        for variableset in self.variablesets:
            variables = Variables(db=self.db, full=self.full)
            variables.variables_of_variableset(variableset_name=variableset["name"])
            variableset["variables"] = variables.variables
