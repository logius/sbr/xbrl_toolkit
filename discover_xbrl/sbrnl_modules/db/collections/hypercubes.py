from discover_xbrl.sbrnl_modules.db.collections.labels import Labels
from discover_xbrl.sbrnl_modules.db.collections.line_items import LineItems
from discover_xbrl.sbrnl_modules.db.collections.dimensions import Dimensions


class HyperCubes:
    def __init__(self, db=None, full=True):
        self.db = db
        self.full = full
        self.hypercubes = []
        self._dts_name = None
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def hypercube_of_linkrole(self, linkrole_rowid=None, dts_name=None):
        if not linkrole_rowid:
            return False
        self._dts_name = dts_name
        query = self.db.rewrite_query(
            "select hc_role_uri, linkrole_rowid, hc_type, context_element, closed, definition  "
            "from hypercube "
            "left join linkrole on linkrole.rowid = linkrole_rowid "
            "where linkrole.rowid = %s "
        )
        self.cursor.execute(query, (linkrole_rowid,))
        row = self.cursor.fetchall()
        if row:
            self.cache_rows([row][0])

        if self.full:
            self.get_relations()

    def hypercubes_of_dts(self, dts_name=None):
        if not dts_name:
            return False
        self._dts_name = dts_name
        query = self.db.rewrite_query(
            "select distinct(hc_role_uri), linkrole_rowid, hc_type, context_element, "
            " closed, definition, _order "
            "from hypercube "
            "left join linkrole on linkrole.rowid = linkrole_rowid "
            "where dts_name = %s "
            "order by _order, definition"
        )
        self.cursor.execute(query, (dts_name,))
        rows = self.cursor.fetchall()
        self.cache_rows(rows)
        if self.full:
            self.get_relations()

    def hypercube_of_linkrole_dts(self, role_uri=None, dts_name=None):
        if not dts_name or not role_uri:
            return False
        self._dts_name = dts_name
        query = self.db.rewrite_query(
            "select distinct(hc_role_uri), linkrole_rowid, hc_type, context_element, closed, definition "
            "from hypercube "
            "left join linkrole on rowid = linkrole_rowid "
            "where dts_name = %s and role_uri = %s "
        )
        self.cursor.execute(query, (dts_name, role_uri.replace("|", "/")))
        rows = self.cursor.fetchall()
        self.cache_rows(rows)

        if self.full:
            self.get_relations()

    def cache_rows(self, rows):
        for row in rows:
            labels = Labels(db=self.db)
            labels.labels_of_linkrole(linkrole_rowid=row[1])
            self.hypercubes.append(
                {
                    "hc_role_uri": row[0],
                    "linkrole_rowid": row[1],
                    "hc_type": row[2],
                    "context_element": row[3],
                    "closed": row[4],
                    "definition": row[5],
                    "labels": labels.labels,
                }
            )

    def get_relations(self):
        for hypercube in self.hypercubes:
            line_items = LineItems(db=self.db)
            line_items.line_items_of_hypercube(
                hypercube_linkrole_rowid=hypercube["linkrole_rowid"],
                dts_name=self._dts_name,
            )
            hypercube["line_items"] = line_items.line_items
            dimensions = Dimensions(db=self.db, full=self.full)
            dimensions.dimensions_of_hypercube(
                hypercube_linkrole_rowid=hypercube["linkrole_rowid"],
                dts_name=self._dts_name,
            )
            hypercube["dimensions"] = dimensions.dimensions
