from discover_xbrl.sbrnl_modules.db.collections.labels import Labels


class DimensionRelationshipnodes:
    def __init__(self, db=None, full=True):
        self.db = db
        self.full = full
        self.dimension_relationshipnodes = []
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def dimension_relationshipnodes_of_parent(self, parent_id=None):
        if not parent_id:
            return False
        query = self.db.rewrite_query(
            "select id, label, parent_child_order, parent, _order, dimension_id, linkrole_role_uri, "
            "formula_axis, generations, relationship_source, tagselector "
            "from dimension_relationshipnode where parent = %s "
        )
        self.cursor.execute(query, (parent_id,))
        rows = self.cursor.fetchall()
        for row in rows:
            self.dimension_relationshipnodes.append(
                {
                    "id": row[0],
                    "label": row[1],
                    "parent_child_order": row[2],
                    "parent": row[3],
                    "order": row[4],
                    "dimension_id": row[5],
                    "linkrole_role_uri": row[6],
                    "formula_axis": row[7],
                    "generations": row[8],
                    "relationship_source": row[9],
                    "tagselector": row[10],
                }
            )
