from discover_xbrl.sbrnl_modules.db.collections.labels import Labels


class AspectNodes:
    def __init__(self, db=None, full=True):
        self.db = db
        self.full = full
        self.aspect_nodes = []
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def aspect_nodes_of_parent(self, parent_id):
        query = self.db.rewrite_query(
            "select id, label, parent_child_order, parent, tagselector, merge, _order, is_abstract, "
            "aspect_type, include_unreported_value, value "
            "from aspect_node where parent = %s"
        )
        self.cursor.execute(query, (parent_id,))
        rows = self.cursor.fetchall()

        for row in rows:
            self.aspect_nodes.append(
                {
                    "id": row[0],
                    "label": row[1],
                    "parent_child_order": row[2],
                    "parent": row[3],
                    "tagselector": row[4],
                    "merge": row[5],
                    "order": row[6],
                    "is_abstract": row[7],
                    "aspect_type": row[8],
                    "include_unreported_value": row[9],
                    "value": row[10],
                }
            )
