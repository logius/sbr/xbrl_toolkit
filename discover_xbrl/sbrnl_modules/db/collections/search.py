class Search:
    def __init__(self, db=None):
        self.db = db
        self.cursor = self.db.connection.cursor()
        self.result = []

    def search(self, query, filter=None):
        if not filter:
            self.cursor.execute(
                "select * from search_index where search_text like ? ", (f"%{query}%",)
            )
        else:
            self.cursor.execute(
                "select * from search_index where search_text like ? and element_type = ?",
                (f"%{query}%", filter),
            )
        for row in self.cursor.fetchall():
            self.result.append(
                {"text": row[0], "element_type": row[1], "element_rowid": row[2]}
            )
