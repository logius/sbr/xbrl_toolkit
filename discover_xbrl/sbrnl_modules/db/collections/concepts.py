from discover_xbrl.sbrnl_modules.db.collections.itemtypes import ItemTypes
from discover_xbrl.sbrnl_modules.db.connection import get_connection
from discover_xbrl.sbrnl_modules.db.collections.labels import Labels
from discover_xbrl.sbrnl_modules.db.collections.references import References


class Concepts:
    def __init__(self, db=None, full=True):
        self.db = db
        self.concepts = []
        self.full = full
        self._dts_name = None
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def get_all(self, filter=None):
        if not filter:
            # no rewrite :)
            self.cursor.execute(
                "select id, name, balance, nillable, abstract, periodtype, ns_prefix, namespace, "
                "substitution_group, typed_domainref, itemtype_name, null, null "
                "from concept "
            )
        else:
            query = self.db.rewrite_query(
                "select id, name, balance, nillable, abstract, periodtype, ns_prefix, namespace, "
                "substitution_group, typed_domainref, itemtype_name, null, null "
                "from concept where id like %s or name like %s "
            )
            self.cursor.execute(query, (f"%{filter}%", f"%{filter}%"))
        rows = self.cursor.fetchall()
        if rows:
            self.cache_rows(rows)
        if self.full:
            self.get_relations()

    def concept_by_id(self, id=None, dts_name=None):
        if id is None:
            return False
        self.concepts = []
        self._dts_name = dts_name
        query = self.db.rewrite_query(
            "select id, name, balance, nillable, abstract, periodtype, ns_prefix, namespace, "
            "substitution_group, typed_domainref, itemtype_name, null, null "
            "from concept where id = %s "
        )
        self.cursor.execute(query, (id,))
        rows = self.cursor.fetchall()
        if rows:
            self.concepts = []
            self.cache_rows(rows)
        if self.full:
            self.get_relations()

    def concept_by_qname(self, qname=None, dts_name=None):
        if not qname:
            return False
        self._dts_name = dts_name
        self.concepts = []
        parts = qname.rsplit(":", 1)
        if len(parts) != 2:
            return False
        if parts[0].startswith("{"):  # Clark notation
            parts[0] = parts[0][1:-1]  # remove '{' and '}' from the URI
            query = self.db.rewrite_query(
                "select id, name, balance, nillable, abstract, periodtype, ns_prefix, namespace, "
                "substitution_group, typed_domainref, itemtype_name, null, null "
                "from concept where namespace = %s and name = %s "
            )
        else:
            query = self.db.rewrite_query(
                "select id, name, balance, nillable, abstract, periodtype, ns_prefix, namespace, "
                "substitution_group, typed_domainref, itemtype_name, null, null "
                "from concept where ns_prefix = %s and name = %s "
            )
        self.cursor.execute(query, (parts[0], parts[1]))
        rows = self.cursor.fetchall()
        if rows:
            self.cache_rows(rows)
        if self.full:
            self.get_relations()

    def concepts_of_dts(self, dts_name=None, referenced_only=True):
        if dts_name is None:
            return False
        self._dts_name = dts_name
        # todo: EK: When only getting directly referenced concepts, some things in builder fail.
        #  Probably because rulenodes are not parsed?
        if referenced_only is True:
            query = self.db.rewrite_query(
                "select id, name, balance, nillable, abstract, periodtype, ns_prefix, namespace, "
                "substitution_group, typed_domainref, itemtype_name, p_link, d_link "
                "from concept left join dts_concept on concept.id = dts_concept.concept_id "
                "where dts_concept.dts_name = %s and directly_referenced"
            )
        else:
            query = self.db.rewrite_query(
                "select id, name, balance, nillable, abstract, periodtype, ns_prefix, namespace, "
                "substitution_group, typed_domainref, itemtype_name, p_link, d_link "
                "from concept left join dts_concept on concept.id = dts_concept.concept_id "
                "where dts_concept.dts_name = %s"
            )
        self.cursor.execute(query, (dts_name,))
        rows = self.cursor.fetchall()
        if rows:
            self.cache_rows(rows)

        if self.full:
            self.get_relations()

    def cache_rows(self, rows):
        for row in rows:
            labels = Labels(db=self.db)
            labels.labels_of_concept(concept_id=row[0], dts_name=self._dts_name)
            self.concepts.append(
                {
                    "id": row[0],
                    "name": row[1],
                    "balance": row[2],
                    "nillable": row[3],
                    "abstract": True if row[4] and row[4] > 0 else False,
                    "periodtype": row[5],
                    "ns_prefix": row[6],
                    "namespace": row[7],
                    "substitution_group": row[8],
                    "typed_domainref": row[9],
                    "itemtype_name": row[10],
                    "p_link": row[11],
                    "d_link": row[12],
                    "labels": labels.labels,
                }
            )

    def get_relations(self):
        for concept in self.concepts:
            references = References(db=self.db)
            references.references_of_concept(concept_id=concept["id"])
            concept["references"] = references.references
            itemtype = ItemTypes(db=self.db)
            itemtype.itemtype_by_name(itemtype_name=concept["itemtype_name"])
            concept["itemtype"] = (
                itemtype.itemtypes[0] if len(itemtype.itemtypes) else None
            )

    def all_concepts_of_dts(self, dts_name=None):
        if dts_name is None:
            return False
        self._dts_name = dts_name
        query = self.db.rewrite_query(
            "select id, name, balance, nillable, abstract, periodtype, ns_prefix, namespace, "
            "substitution_group, typed_domainref, itemtype_name, p_link, d_link "
            "from concept left join dts_concept on concept.id = dts_concept.concept_id "
            "where dts_concept.dts_name = %s "
        )
        self.cursor.execute(query, (dts_name,))
        rows = self.cursor.fetchall()
        if rows:
            self.cache_rows(rows)

        if self.full:
            self.get_relations()


def main():
    db = get_connection()
    concepts = Concepts(db=db)
    concepts.concepts_of_dts(
        dts_name="kvk-rpt-jaarverantwoording-2021-nlgaap-micro.xsd"
    )
    print(f"Aantal concepten: {len(concepts.concepts)}")


if __name__ == "__main__":
    main()
