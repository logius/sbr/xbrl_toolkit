from discover_xbrl.sbrnl_modules.db.collections.formula_periods import FormulaPeriods
from discover_xbrl.sbrnl_modules.db.collections.formula_explicitdimensions import (
    FormulaExplicitDimensions,
)
from discover_xbrl.sbrnl_modules.db.collections.formula_typed_dimensions import (
    FormulaTypedDimensions,
)


class Rulesets:
    def __init__(self, db=None, full=True):
        self.db = db
        self.full = full
        self.rulesets = []
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def rulesets_of_rulenode(self, rulenode_id):
        if not rulenode_id:
            return False
        query = self.db.rewrite_query(
            "select rowid, rulenode_id, tag from ruleset where rulenode_id = %s "
        )
        self.cursor.execute(query, (rulenode_id,))
        rows = self.cursor.fetchall()
        for row in rows:
            self.rulesets.append(
                {"rowid": row[0], "rulennode_id": row[1], "tag": row[2]}
            )
        if self.full:
            self.get_relations()

    def get_relations(self):
        for ruleset in self.rulesets:
            formula_periods = FormulaPeriods(db=self.db, full=self.full)
            formula_periods.formula_periods_of_ruleset(parent_ruleset=ruleset["rowid"])
            ruleset["formula_periods"] = formula_periods.formula_periods

            formula_explicit_dimensions = FormulaExplicitDimensions(
                db=self.db, full=self.full
            )
            formula_explicit_dimensions.formula_explicit_dimensions_of_ruleset(
                parent_ruleset=ruleset["rowid"]
            )
            ruleset[
                "formula_explicit_dimensions"
            ] = formula_explicit_dimensions.formula_explicit_dimensions

            formula_typed_dimensions = FormulaTypedDimensions(
                db=self.db, full=self.full
            )
            formula_typed_dimensions.formula_typed_dimensions_of_ruleset(
                parent_ruleset=ruleset["rowid"]
            )
            ruleset[
                "formula_typed_dimensions"
            ] = formula_typed_dimensions.formula_typed_dimensions
