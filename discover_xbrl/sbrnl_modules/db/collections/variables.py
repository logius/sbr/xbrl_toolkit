from discover_xbrl.sbrnl_modules.db.collections.filters import Filters


class Variables:
    def __init__(self, db=None, full=True):
        self.db = db
        self.full = full
        self.variables = []
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def variables_of_linkrole(self, linkrole_rowid=None):
        if linkrole_rowid is None:
            return False
        query = self.db.rewrite_query(
            "select id, type, label, variable_arc_name, bind_as_sequence, _select, _matches, nils, fallback_value, "
            "linkrole_rowid, null, null "
            "from variable "
            "left join linkrole_variable on variable.id = linkrole_variable.variable_id "
            "where linkrole_rowid = %s "
        )
        self.cursor.execute(query, (linkrole_rowid,))
        rows = self.cursor.fetchall()
        if rows:
            self.cache_rows(rows)
        if self.full:
            self.get_relations()

    def variables_of_variableset(self, variableset_name=None):
        if not variableset_name:
            return False
        query = self.db.rewrite_query(
            "select id, type, label, variable_arc_name, bind_as_sequence, _select, _matches, nils, fallback_value,"
            "null, _order, priority "
            "from variableset_variable "
            "left join variable v on variableset_variable.variable_id = v.id "
            "where variableset_name = %s "
        )
        self.cursor.execute(query, (variableset_name,))
        rows = self.cursor.fetchall()
        if rows:
            self.cache_rows(rows)
        if self.full:
            self.get_relations()

    def variables_by_id(self, id=None):
        query = self.db.rewrite_query(
            "select id, type, label, variable_arc_name, bind_as_sequence, _select, _matches, nils, fallback_value, "
            "null, null, null "
            "from variable "
            "where id = %s "
        )
        self.cursor.execute(query, (id,))
        rows = self.cursor.fetchall()
        if rows:
            self.cache_rows(rows)
        if self.full:
            self.get_relations()

    def cache_rows(self, rows):
        for row in rows:
            self.variables.append(
                {
                    "id": row[0],
                    "type": row[1],
                    "label": row[2],
                    "variable_arc_name": row[3],
                    "bind_as_sequence": row[4],
                    "select": row[5],
                    "matches": row[6],
                    "nils": row[7],
                    "fallback_value": row[8],
                    "linkrole_rowid": row[9],
                    "order": row[10],
                    "priority": row[11],
                }
            )

    def get_relations(self):
        for row in self.variables:
            filters = Filters(db=self.db, full=self.full)
            filters.filters_of_variable(variable_id=row["id"])
            if len(filters.filters):
                row["filters"] = filters.filters
