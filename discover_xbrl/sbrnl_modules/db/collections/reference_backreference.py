from discover_xbrl.sbrnl_modules.db.collections.concepts import Concepts


class ReferenceBackreferences:
    def __init__(self, db=None):
        self.db = db
        self.back_references = []
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()
        self.concepts = Concepts(db=db, full=True)

    def backreferences_of_reference(self, reference_id=None, dts_name=None):
        if not reference_id or not dts_name:
            return False
        query = self.db.rewrite_query(
            "select c.id, c.name "
            "from concept_reference cr "
            "left join concept c on cr.concept_id = c.id "
            "left join dts_concept dc on c.id = dc.concept_id "
            "where cr.reference_id = %s "
            "and dts_name = %s and directly_referenced "
        )
        self.cursor.execute(query, (reference_id, dts_name))
        rows = self.cursor.fetchall()
        for row in rows:
            self.concepts.concepts = []
            self.concepts.concept_by_id(id=row[0], dts_name=dts_name)
            concept = self.concepts.concepts[0] if len(self.concepts.concepts) else None
            self.back_references.append(
                {
                    "type": "Concept",
                    "source": "Concept",
                    "concept_id": row[0],
                    "concept_name": row[1],
                    "concept": concept,
                }
            )
