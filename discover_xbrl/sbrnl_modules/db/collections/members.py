from discover_xbrl.sbrnl_modules.db.collections.concepts import Concepts


class Members:
    def __init__(self, db=None, full=True):
        self.db = db
        self.full = full
        self.members = []
        self._dts_name = None
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def members_of_dimension(self, dimension_id, dts_name=None):
        self._dts_name = dts_name
        query = self.db.rewrite_query(
            "select concept_id, _order, usable, target_linkrole, preferred_label, parent_rowid, parent_type, rowid "
            "from member where parent_rowid = %s and parent_type = %s "
            "order by _order * 1"
        )
        self.cursor.execute(query, (dimension_id, "dimension"))
        rows = self.cursor.fetchall()
        if len(rows):
            self._cache_rows(rows)
        if self.full:
            self.get_relations()

    def members_of_domain(self, domain_id, dts_name=None):
        self._dts_name = dts_name
        query = self.db.rewrite_query(
            "select concept_id, _order, usable, target_linkrole, preferred_label, parent_rowid, parent_type, rowid "
            "from member where parent_rowid = %s and parent_type = %s "
            "order by _order * 1"
        )
        self.cursor.execute(query, (domain_id, "domain"))
        rows = self.cursor.fetchall()
        if len(rows):
            self._cache_rows(rows)
        if self.full:
            self.get_relations()

    def members_of_members(self, member_id, dts_name=None):
        self._dts_name = dts_name
        query = self.db.rewrite_query(
            "select concept_id, _order, usable, target_linkrole, preferred_label, parent_rowid, parent_type, rowid "
            "from member where parent_rowid = %s and parent_type = %s "
            "order by _order * 1"
        )
        self.cursor.execute(query, (member_id, "member"))
        rows = self.cursor.fetchall()
        if len(rows):
            self._cache_rows(rows)
        if self.full:
            self.get_relations()

    def _cache_rows(self, rows):
        for row in rows:
            self.members.append(
                {
                    "concept_id": row[0],
                    "order": row[1],
                    "usable": row[2],
                    "target_linkrole": row[3],
                    "preferred_label": row[4],
                    "parent_rowid": row[5],
                    "parent_type": row[6],
                    "rowid": row[7],
                    "type": "member",
                }
            )

    def get_relations(self):
        for member in self.members:
            concept = Concepts(db=self.db, full=self.full)
            concept.concept_by_id(member["concept_id"], dts_name=self._dts_name)
            member["concept"] = concept.concepts[0]
            own_members = Members(db=self.db, full=self.full)
            own_members.members_of_members(
                member_id=member["rowid"], dts_name=self._dts_name
            )
            if len(own_members.members):
                member["members"] = own_members.members
