from discover_xbrl.sbrnl_modules.db.collections.labels import Labels
from discover_xbrl.sbrnl_modules.db.collections.variablesets import Variablesets


class Assertions:
    def __init__(self, db=None, full=True):
        self.db = db
        self.assertions = []
        self.full = full
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def assertions_of_linkrole(self, linkrole_rowid=None):
        if not linkrole_rowid:
            return False
        query = self.db.rewrite_query(
            "select id, aspect_model, implicit_filtering, xlink_label, test, assertion_type, severity "
            "from assertion "
            "left join linkrole_assertion la on assertion.id = la.assertion_id "
            "where la.linkrole_rowid = %s"
        )
        self.cursor.execute(query, (linkrole_rowid,))
        rows = self.cursor.fetchall()
        if rows:
            self.cache_rows(rows)
        if self.full:
            self.get_relations()
        self.get_labels()

    def assertions_of_dts(self, dts_name=None):
        if not dts_name:
            return False
        query = self.db.rewrite_query(
            "select distinct(assertion.id), aspect_model, implicit_filtering, xlink_label, test, "
            "assertion_type,severity from assertion "
            "left join linkrole_assertion la on assertion.id = la.assertion_id "
            "left join linkrole l on l.rowid = la.linkrole_rowid "
            "where l.dts_name = %s and directly_referenced "
        )
        self.cursor.execute(query, (dts_name,))
        rows = self.cursor.fetchall()
        if rows:
            self.cache_rows(rows)
        self.get_labels()
        if self.full:
            self.get_relations()

    def assertion_of_dts_by_id(self, dts_name=None, assertion_id=None):
        if not dts_name or not assertion_id:
            return False
        query = self.db.rewrite_query(
            "select distinct(assertion.id), aspect_model, implicit_filtering, xlink_label, test, "
            "assertion_type, severity from assertion "
            "left join linkrole_assertion la on assertion.id = la.assertion_id "
            "left join linkrole l on l.rowid = la.linkrole_rowid "
            "where l.dts_name = %s "
            "and assertion.id = %s "
        )
        self.cursor.execute(query, (dts_name, assertion_id))
        rows = self.cursor.fetchall()
        if rows:
            self.cache_rows(rows)
        self.get_labels()
        self.get_relations()

    def cache_rows(self, rows):
        for row in rows:
            self.assertions.append(
                {
                    "id": row[0],
                    "aspect_model": row[1],
                    "implicit_filtering": row[2],
                    "xlink_label": row[3],
                    "test": row[4],
                    "assertion_type": row[5],
                    "severity": row[6],
                }
            )

    def get_relations(self):
        for assertion in self.assertions:
            variablesets = Variablesets(db=self.db, full=self.full)
            variablesets.variablesets_of_assertion(assertion_id=assertion["id"])
            if len(variablesets.variablesets):
                assertion["variablesets"] = variablesets.variablesets

    def get_labels(self):
        for assertion in self.assertions:
            labels = Labels(db=self.db)
            labels.labels_of_assertion(assertion_id=assertion["id"])
            assertion["labels"] = labels.labels
