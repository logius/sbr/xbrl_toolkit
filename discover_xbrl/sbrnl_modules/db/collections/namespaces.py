class Namespaces:
    def __init__(self, db=None, full=True):
        self.db = db
        self.namespaces = []
        self.full = full
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def namespaces_of_dts(self, dts_name=None):
        query = self.db.rewrite_query(
            "select prefix, uri from namespace "
            "left join dts_namespace on namespace.prefix = dts_namespace.namespace_prefix "
            "where dts_namespace.dts_name = %s "
        )
        self.cursor.execute(query, (dts_name,))
        rows = self.cursor.fetchall()
        for row in rows:
            self.namespaces.append({"prefix": row[0], "uri": row[1]})

    def get_all(self, filter=None):
        if filter is None:
            self.cursor.execute("select prefix, uri from namespace order by prefix")
        else:
            query = self.db.rewrite_query(
                "select prefix, uri from namespace where prefix like %s or uri like %s "
            )
            self.cursor.execute(query, (f"%{filter}%", f"%{filter}%"))
        rows = self.cursor.fetchall()
        for row in rows:
            self.namespaces.append({"prefix": row[0], "uri": row[1]})
