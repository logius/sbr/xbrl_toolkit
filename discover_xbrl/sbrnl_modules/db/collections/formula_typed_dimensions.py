class FormulaTypedDimensions:
    def __init__(self, db=None, full=True):
        self.db = db
        self.full = full
        self.formula_typed_dimensions = []
        if not self.db.connection:
            raise ConnectionError
        self.cursor = self.db.connection.cursor()

    def formula_typed_dimensions_of_ruleset(self, parent_ruleset=None):
        if not parent_ruleset:
            return False
        query = self.db.rewrite_query(
            "select rowid, qname_dimension, xpath, value, parent_ruleset, parent_node "
            "from formula_typed_dimension where parent_ruleset = %s "
        )
        self.cursor.execute(query, (parent_ruleset,))
        rows = self.cursor.fetchall()
        if len(rows):
            self.cache_rows(rows)

    def formula_typed_dimensions_of_rulenode(self, parent_node=None):
        if not parent_node:
            return False
        query = self.db.rewrite_query(
            "select rowid, qname_dimension, xpath, value, parent_ruleset, parent_node "
            "from formula_typed_dimension where parent_node = %s "
        )
        self.cursor.execute(query, (parent_node,))
        rows = self.cursor.fetchall()
        if len(rows):
            self.cache_rows(rows)

    def cache_rows(self, rows=None):
        for row in rows:
            self.formula_typed_dimensions.append(
                {
                    "rowid": row[0],
                    "qname_dimension": row[1],
                    "xpath": row[2],
                    "value": row[3],
                    "parent_ruleset": row[4],
                    "parent_node": row[5],
                }
            )
