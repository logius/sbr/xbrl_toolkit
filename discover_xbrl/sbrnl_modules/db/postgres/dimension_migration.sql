alter table dimension
    add linkrole_rowid int;
alter table dimension add constraint fk_dimension_linkrole_rowid
    foreign key (linkrole_rowid) references linkrole(rowid);

create index if not exists dimension_linkrole_rowid on main.dimension(linkrole_rowid)
