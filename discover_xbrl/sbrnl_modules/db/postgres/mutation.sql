create table if not exists mutation
(
    rowid         serial,
    run_id        varchar not null,
    object_type   varchar not null,
    object_id     varchar not null,
    mutation_type varchar not null,
    old_version   varchar not null
);

create unique index if not exists mutation_unique on mutation (run_id, object_type, object_id, mutation_type);
create index if not exists mutation_object on mutation (object_type, object_id);
