create table if not exists dts
(
    name        varchar not NULL,
    shortname   varchar,
    nice_name   varchar,
    description varchar
);
create unique index if not exists dts_name on dts (name);


create table if not exists namespace
(
    prefix varchar unique not NULL,
    uri    varchar        not NULL
);
create unique index if not exists prefix on namespace (prefix);
create index if not exists uri on namespace (uri);


create table if not exists dts_namespace
(
    namespace_prefix varchar not NULL,
    dts_name         varchar not NULL,
    foreign key (dts_name) references dts (name) on delete cascade on update cascade,
    foreign key (namespace_prefix) references namespace (prefix) on delete cascade on update cascade
);
create index if not exists dts_namespace_prefix on dts_namespace (namespace_prefix);
create index if not exists dts_namespace_name on dts_namespace (dts_name);


create table if not exists label
(
    id         varchar not NULL,
    link_label varchar not NULL,
    link_role  varchar not NULL,
    lang       varchar not NULL,
    label_text varchar not NULL,
    unique (id, link_label, lang, link_role)
);
create unique index if not exists label_unique on label (id, link_label, link_role, lang);
create index if not exists link_label on label (link_label);
create index if not exists label_id on label (id);
create index if not exists label_label_text on label (label_text);
create index if not exists label_lang on label (lang);
create index if not exists label_link_role on label(link_role);


create table if not exists dts_label
(
    label_link_label varchar not null,
    label_id         varchar not null,
    label_lang       varchar not null,
    label_link_role  varchar not null,
    dts_name         varchar not null,
    foreign key (label_id, label_link_label, label_lang, label_link_role) references label (id, link_label, lang, link_role) on delete cascade on update cascade,
    foreign key (dts_name) references dts (name) on delete cascade on update cascade
);
create index if not exists dts_label_id on dts_label (label_id);
create index if not exists dts_label_label on dts_label (label_link_label);
create index if not exists dts_label_name on dts_label (dts_name);


create table if not exists itemtype
(
    id                     varchar,
    name                   varchar,
    domain                 varchar,
    base                   varchar,
    inheritance_type       varchar,
    restriction_type       varchar,
    restriction_length     int,
    restriction_pattern    varchar,
    restriction_max_length int,
    restriction_min_length int,
    total_digits           integer,
    fraction_digits        integer,
    min_inclusive          varchar
);
create unique index if not exists itemtype_name on itemtype (name);


create table if not exists enumeration_option
(
    id    varchar,
    value varchar
);
create unique index if not exists enumeration_option_id on enumeration_option (id);


create table if not exists enumeration_option_label
(
    enumeration_option_id varchar,
    label_id              varchar,
    label_link_label      varchar,
    label_lang            varchar,
    label_link_role       varchar,
    foreign key (enumeration_option_id) references enumeration_option (id) on delete cascade on update cascade,
    foreign key (label_id, label_link_label, label_lang, label_link_role) references label (id, link_label, lang, link_role) on delete cascade on update cascade
);
create index if not exists enumeration_option_label_option_id
    on enumeration_option_label (enumeration_option_id);
create index if not exists enumeration_option_label_label_link_label
    on enumeration_option_label (label_id, label_link_label, label_lang);



create table if not exists itemtype_enumeration_option
(
    itemtype_name         varchar,
    enumeration_option_id varchar,
    foreign key (itemtype_name) references itemtype (name) on delete cascade on update cascade,
    foreign key (enumeration_option_id) references enumeration_option (id) on delete cascade on update cascade
);
create index if not exists itemtype_enumeration_option_itemtype_name
    on itemtype_enumeration_option (itemtype_name);
create index if not exists itemtype_enumeration_option_enumeration_option_id
    on itemtype_enumeration_option (enumeration_option_id);


create table if not exists dts_itemtype
(
    dts_name      varchar not NULL,
    itemtype_name varchar not NULL,
    foreign key (itemtype_name) references itemtype (name) on delete cascade on update cascade,
    foreign key (dts_name) references dts (name) on delete cascade on update cascade
);
create index if not exists dts_itemtype_dts_name on dts_itemtype (dts_name);
create index if not exists dts_itentype_itemtype_name on dts_itemtype (itemtype_name);


create table if not exists concept
(
    id                 varchar not NULL,
    name               varchar not NULL,
    balance            varchar,
    nillable           boolean,
    abstract           boolean,
    periodtype         varchar,
    ns_prefix          varchar,
    namespace          varchar,
    substitution_group varchar,
    typed_domainref    varchar,
    itemtype_name      varchar,
    foreign key (itemtype_name) references itemtype (name) on delete cascade on update cascade,
    foreign key (ns_prefix) references namespace (prefix) on delete cascade on update cascade
);
create unique index if not exists concept_id on concept (id);
create unique index if not exists concept_name_ns on concept (name, ns_prefix);


create table if not exists dts_concept
(
    dts_name            varchar not NULL,
    concept_id          varchar not NULL,
    directly_referenced boolean,
    p_link              boolean,
    d_link              boolean,
    t_link              boolean,
    foreign key (concept_id) references concept (id) on delete cascade on update cascade,
    foreign key (dts_name) references dts (name) on delete cascade on update cascade
);
create unique index if not exists dts_concept_unique on dts_concept (dts_name, concept_id);
create index if not exists dts_concept_concept_id on dts_concept (concept_id);
create index if not exists dts_concept_dts_name on dts_concept (dts_name);


create table if not exists concept_label
(
    concept_id       varchar not NULL,
    label_link_label varchar not null,
    label_id         varchar not null,
    label_lang       varchar not null,
    label_link_role  varchar not null,
    dts_name         varchar not NULL,
    foreign key (label_id, label_link_label, label_lang, label_link_role)
        references label (id, link_label, lang, link_role) on delete cascade on update cascade,
    foreign key (concept_id) references concept (id) on delete cascade on update cascade,
    foreign key (dts_name) references dts (name) on delete cascade on update cascade
);
create unique index if not exists concept_label_unique on concept_label
    (concept_id, label_id, label_link_label, label_lang, dts_name, label_link_role);
create index if not exists concept_label_concept_id on concept_label (concept_id);
create index if not exists concept_label_link_label on concept_label (label_link_label);
create index if not exists concept_label_id on concept_label (label_id);
create index if not exists concept_label_dts_name on concept_label (dts_name);


create table if not exists reference
(
    id           varchar not NULL,
    label        varchar,
    name         varchar,
    number       varchar,
    issuedate    varchar,
    chapter      varchar,
    article      varchar,
    note         varchar,
    section      varchar,
    subsection   varchar,
    publisher    varchar,
    paragraph    varchar,
    subparagraph varchar,
    clause       varchar,
    subclause    varchar,
    appendix     varchar,
    example      varchar,
    page         varchar,
    exhibit      varchar,
    footnote     varchar,
    sentence     varchar,
    uri          varchar,
    uridate      varchar
);
create unique index if not exists reference_id on reference (id);
create index if not exists reference_name on reference (name);


create table if not exists concept_reference
(
    concept_id   varchar not NULL,
    reference_id varchar not NULL,
    foreign key (concept_id) references concept (id) on delete cascade on update cascade,
    foreign key (reference_id) references reference (id) on delete cascade on update cascade
);
create index if not exists concept_reference_reference_id on concept_reference (reference_id);
create index if not exists concept_reference_concept_id on concept_reference (concept_id);


create table if not exists used_on
(
    used_on varchar unique not null
);
create index if not exists used_on_used_on on used_on (used_on);


create table if not exists linkrole
(
    rowid               serial,
    id                  varchar,
    role_uri            varchar not NULL,
    definition          varchar,
    dts_name            varchar,
    directly_referenced boolean,
    _order              integer,
    --- used ons!
    foreign key (dts_name) references dts (name) on delete cascade on update cascade
);
create unique index if not exists linkrole_rowid on linkrole (rowid);
create index if not exists linkrole_role_uri on linkrole (role_uri);
create index if not exists linkrole_dts_name on linkrole (dts_name);

create table if not exists linkrole_label
(
    label_link_label varchar not null,
    label_id         varchar not null,
    label_lang       varchar not null,
    label_link_role  varchar not null,
    linkrole_rowid   integer not null,
    foreign key (label_id, label_link_label, label_lang, label_link_role)
        references label (id, link_label, lang, link_role) on delete cascade on update cascade,
    foreign key (linkrole_rowid) references linkrole (rowid) on delete cascade on update cascade
);
create index if not exists linkrole_label_label on linkrole_label (label_link_label);
create index if not exists linkrole_label_assertion_id on linkrole_label (linkrole_rowid);


create table if not exists linkrole_used_on
(
    linkrole_rowid  integer not null,
    used_on_used_on varchar not null,
    foreign key (linkrole_rowid) references linkrole (rowid),
    foreign key (used_on_used_on) references used_on (used_on)
);
create index if not exists linkrole_used_on_linkrole_rowid on linkrole_used_on (linkrole_rowid);
create index if not exists linkrole_used_on_used_on on linkrole_used_on (used_on_used_on);


create table if not exists hypercube
(
    linkrole_rowid  integer not NULL,
    hc_role_uri     varchar not NULL,
    hc_type         varchar,
    context_element varchar,
    closed          varchar
);
create index if not exists hypercube_hc_role_uri on hypercube (hc_role_uri);
create unique index if not exists hypercube_linkrole_rowid on hypercube (linkrole_rowid);


create table if not exists dimension
(
    rowid                    serial,
    concept_id               varchar not NULL,
    hypercube_linkrole_rowid integer,
    target_linkrole          varchar,
    linkrole_rowid           integer,
    foreign key (concept_id) references concept (id) on delete cascade on update cascade,
    foreign key (hypercube_linkrole_rowid) references hypercube (linkrole_rowid) on delete cascade on update cascade,
    foreign key (linkrole_rowid) references linkrole (rowid) on delete cascade on update cascade
);
create unique index if not exists dimension_rowid on dimension (rowid);
create index if not exists dimension_concept_id on dimension (concept_id);
create index if not exists dimension_target_linkrole on dimension (target_linkrole);
create index if not exists dimension_linkrole_rowid on dimension (linkrole_rowid);


create table if not exists domain
(
    rowid           serial,
    concept_id      varchar not NULL,
    target_linkrole varchar,
    usable          boolean,
    dimension_rowid integer,
    foreign key (dimension_rowid) references dimension (rowid) on delete cascade on update cascade,
    foreign key (concept_id) references concept (id) on delete cascade on update cascade
);
create unique index if not exists domain_rowid on domain (rowid);
create index if not exists domain_concept_id on domain (concept_id);


create table if not exists member
(
    rowid           serial,
    concept_id      varchar not NULL,
    _order          integer,
    usable          boolean,
    parent_rowid    integer,
    parent_type     varchar,
    target_linkrole varchar,
    preferred_label varchar,
    foreign key (concept_id) references concept (id) on delete cascade on update cascade
);
create unique index if not exists member_rowid on member (rowid);
create index if not exists member_concept_id on member (concept_id);
create index if not exists member_parent on member (parent_rowid, parent_type);


create table if not exists line_item
(
    _order                   integer,
    hypercube_linkrole_rowid integer not NULL,
    concept_id               varchar not NULL,
    parent_concept_id        varchar,
    foreign key (concept_id) references concept (id) on delete cascade on update cascade,
    foreign key (parent_concept_id) references concept (id)
);
create index if not exists line_item_concept on line_item (concept_id);
create index if not exists line_item_hypercube on line_item (hypercube_linkrole_rowid);


create table if not exists linkrole_reference
(
    reference_id   varchar not NULL,
    linkrole_rowid integer not NULL,
    foreign key (reference_id) references reference (id) on delete cascade on update cascade,
    foreign key (linkrole_rowid) references linkrole (rowid) on delete cascade on update cascade
);
create index if not exists linkrole_reference_reference_id on linkrole_reference (reference_id);
create index if not exists linkrole_reference_linkrole_rowid on linkrole_reference (linkrole_rowid);


create table if not exists assertion
(
    id                 varchar not NULL,
    aspect_model       varchar,
    implicit_filtering varchar,
    xlink_label        varchar,
    test               varchar,
    assertion_type     varchar,
    severity           varchar
);
create unique index if not exists assertion_id on assertion (id);


create table if not exists linkrole_assertion
(
    linkrole_rowid integer not NULL,
    assertion_id   varchar not NULL,
    foreign key (linkrole_rowid) references linkrole (rowid) on delete cascade on update cascade,
    foreign key (assertion_id) references assertion (id) on delete cascade on update cascade
);
create index if not exists linkrole_assertion_assertion_id on linkrole_assertion (assertion_id);
create index if not exists linkrole_assertion_linkrole_rowid on linkrole_assertion (linkrole_rowid);


create table if not exists assertion_label
(
    label_link_label varchar not null,
    label_id         varchar not null,
    label_lang       varchar not null,
    label_link_role  varchar not null,
    assertion_id     varchar not null,
    foreign key (label_id, label_link_label, label_lang, label_link_role)
        references label (id, link_label, lang, link_role) on delete cascade on update cascade,
    foreign key (assertion_id) references assertion (id) on delete cascade on update cascade
);
create index if not exists assertion_label_label on assertion_label (label_link_label);
create index if not exists assertion_label_assertion_id on assertion_label (assertion_id);


create table if not exists xbrl_table
(
    id                 varchar not NULL,
    label              varchar not NULL,
    parent_child_order text    not NULL
);
create unique index if not exists xbrl_table_id on xbrl_table (id);
create index if not exists xbrl_table_link_label on xbrl_table (label);


create table if not exists linkrole_table
(
    linkrole_rowid integer not NULL,
    table_id       varchar not NULL,
    foreign key (linkrole_rowid) references linkrole (rowid) on delete cascade on update cascade,
    foreign key (table_id) references xbrl_table (id) on delete cascade on update cascade
);
create index if not exists linkrole_table_linkrole_rowid on linkrole_table (linkrole_rowid);
create index if not exists linkrole_table_table_id on linkrole_table (table_id);


create table if not exists xbrl_table_label
(
    label_id         varchar not null,
    label_link_label varchar not null,
    label_lang       varchar not null,
    label_link_role  varchar not null,
    xbrl_table_id    varchar not null,
    foreign key (label_id, label_link_label, label_lang, label_link_role)
        references label (id, link_label, lang, link_role) on delete cascade on update cascade,
    foreign key (xbrl_table_id) references xbrl_table (id) on delete cascade on update cascade
);
create index if not exists xbrl_table_label_id on xbrl_table_label (label_id);
create index if not exists xbrl_table_label_label on xbrl_table_label (label_link_label);
create index if not exists xbrl_table_label_table_id on xbrl_table_label (xbrl_table_id);


create table if not exists table_breakdown
(
    id                 varchar not NULL,
    label              varchar not NULL,
    parent_child_order varchar not NULL,
    axis               varchar not NULL,
    table_id           varchar not NULL,
    _order             integer,
    foreign key (table_id) references xbrl_table (id) on delete cascade on update cascade
);
create index if not exists table_breakdown_id on table_breakdown (id);
create index if not exists table_breakdown_table_id on table_breakdown (table_id);


create table if not exists table_rulenode
(
    id                 varchar not NULL,
    label              varchar not NULL,
    parent_child_order varchar,
    merge              boolean,
    _order             integer,
    is_abstract        boolean,
    parent             varchar not NULL,
    tagselector        varchar
);
create unique index if not exists table_rulenode_id on table_rulenode (id);
create index if not exists table_rulenode_link_label on table_rulenode (label);


create table if not exists table_rulenode_label
(
    label_id          varchar not null,
    label_link_label  varchar not null,
    label_lang        varchar not null,
    label_link_role   varchar not null,
    table_rulenode_id varchar not null,
    foreign key (label_id, label_link_label, label_lang, label_link_role)
        references label (id, link_label, lang, link_role) on delete cascade on update cascade,
    foreign key (table_rulenode_id) references table_rulenode (id) on delete cascade on update cascade
);
create index if not exists table_rulenode_label_id on table_rulenode_label (label_id);
create index if not exists table_rulenode_label_label on table_rulenode_label (label_link_label);
create index if not exists table_rulenode_label_table_id on table_rulenode_label (table_rulenode_id);


create table if not exists concept_relationshipnode
(
    id                  varchar not NULL,
    label               varchar not NULL,
    parent_child_order  varchar,
    parent              varchar,
    tagselector         varchar,
    _order              integer,
    relationship_source varchar not NULL,
    linkrole_role_uri   varchar not NULL,
    formula_axis        varchar,
    generations         varchar,
    arcrole             varchar,
    linkname            varchar,
    arcname             varchar
);
create unique index if not exists concept_relationshipnode_id on concept_relationshipnode (id);
create index if not exists concept_relationshipnode_relationship_source on concept_relationshipnode (relationship_source);


create table if not exists aspect_node
(
    id                       varchar not NULL,
    label                    varchar not NULL,
    parent_child_order       varchar,
    parent                   varchar,
    tagselector              varchar,
    merge                    boolean,
    _order                   integer,
    is_abstract              boolean,
    aspect_type              varchar,
    include_unreported_value boolean,
    value                    varchar
);
create unique index if not exists aspect_node_id on aspect_node (id);
create index if not exists aspect_node_parent on aspect_node (parent);


create table if not exists dimension_relationshipnode
(
    id                  varchar not NULL,
    label               varchar not NULL,
    parent_child_order  varchar,
    parent              varchar,
    _order              integer,
    dimension_id        varchar not NULL,
    linkrole_role_uri   varchar not NULL,
    formula_axis        varchar not NULL,
    generations         varchar,
    relationship_source varchar,
    tagselector         varchar
);
create unique index if not exists dimension_relationshipnode_id on dimension_relationshipnode (id);
create index if not exists dimension_relationshipnode_parent on dimension_relationshipnode (parent);


create table if not exists ruleset
(
    rowid       serial,
    rulenode_id varchar not NULL,
    tag         varchar not NULL,
    foreign key (rulenode_id) references table_rulenode (id) on delete cascade on update cascade
);
create unique index if not exists ruleset_rowid on ruleset (rowid);
create index if not exists ruleset_rulenode_id on ruleset (rulenode_id);


create table if not exists formula_period
(
    rowid          serial,
    period_type    varchar,
    _start         varchar,
    _end           varchar,
    value          varchar,
    parent_ruleset integer,
    parent_node    varchar
);
create unique index if not exists formula_period_rowid on formula_period (rowid);
create index if not exists formula_period_parent_node on formula_period (parent_node);
create index if not exists formula_period_parent_ruleset on formula_period (parent_ruleset);


create table if not exists formula_explicit_dimension
(
    rowid                   serial,
    qname_dimension         varchar not NULL,
    qname_member            varchar,
    qname_member_expression varchar,
    type_qname              varchar,
    omit                    boolean,
    parent_ruleset          integer,
    parent_node             varchar
);
create unique index if not exists formula_explicit_dimension_rowid
    on formula_explicit_dimension (rowid);
create index if not exists formula_explicit_dimension_parent_ruleset
    on formula_explicit_dimension (parent_ruleset);
create index if not exists formula_explicit_dimension_parent_node
    on formula_explicit_dimension (parent_node);


create table if not exists formula_typed_dimension
(
    rowid           serial,
    qname_dimension varchar not NULL,
    xpath           varchar,
    value           varchar,
    parent_ruleset  integer,
    parent_node     varchar,
    foreign key (parent_ruleset) references ruleset (rowid) on delete cascade on update cascade
);
create unique index if not exists formula_typed_dimension_rowid
    on formula_typed_dimension (rowid);


create table if not exists variable
(
    id                varchar,
    type              varchar not NULL,
    label             varchar,
    variable_arc_name varchar,
    bind_as_sequence  varchar,
    _select           varchar,
    _matches          varchar,
    nils              varchar,
    fallback_value    varchar
);
create unique index if not exists variable_id on variable (id);
create index if not exists variable_label on variable (label);


create table if not exists linkrole_variable
(
    linkrole_rowid integer not NULL,
    variable_id    varchar not NULL,
    foreign key (linkrole_rowid) references linkrole (rowid) on delete cascade on update cascade,
    foreign key (variable_id) references variable (id) on delete cascade on update cascade
);
create index if not exists linkrole_variable_linkrole_rowid on linkrole_variable (linkrole_rowid);
create index if not exists linkrole_variable_variable_id on linkrole_variable (variable_id);


create table if not exists parameter
(
    id          varchar not NULL,
    label       varchar,
    as_datatype varchar,
    _select     varchar,
    name        varchar,
    required    varchar
);
create unique index if not exists parameter_id on parameter (id);


create table if not exists table_parameter
(
    table_id     varchar not null,
    parameter_id varchar not null,
    foreign key (table_id) references xbrl_table (id) on delete cascade on update cascade,
    foreign key (parameter_id) references parameter (id) on delete cascade on update cascade
);
create index if not exists table_parameter_table_id on table_parameter (table_id);
create index if not exists table_parameter_parameter_id on
    table_parameter (parameter_id);


create table if not exists presentation_hierarchy_node
(
    rowid                 serial,
    id                    varchar not NULL,
    node_type             varchar not NULL,
    concept_id            varchar,
    linkrole_role_uri     varchar,
    is_root               boolean,
    _order                integer,
    preferred_label       varchar,
    parent_rowid          integer,
    dts_name              varchar,
    parent_linkrole_rowid integer,
    directly_referenced   boolean,
    foreign key (concept_id) references concept (id) on delete cascade on update cascade,
    foreign key (dts_name) references dts (name) on delete cascade on update cascade
);
create unique index if not exists presentation_hierarchy_node_rowid on presentation_hierarchy_node (rowid);
create index if not exists presentation_hierarchy_node_id on presentation_hierarchy_node (id);


create table if not exists filter
(
    id                    varchar not NULL,
    label                 varchar not NULL,
    type                  varchar not NULL,
    aspect                varchar,
    include_qname         varchar,
    exclude_qname         varchar,
    qname                 varchar,
    qname_expression      varchar,
    period_type           varchar,
    value                 varchar,
    qname_member          varchar,
    qname_dimension       varchar,
    linkrole_role_uri     varchar,
    arcrole               varchar,
    axis                  varchar,
    balance_type          varchar,
    strict                varchar,
    type_qname            varchar,
    type_qname_expression varchar,
    test                  varchar,
    boundary              varchar,
    date                  varchar,
    time                  varchar,
    parent                varchar,
    variable              varchar,
    arc_complement        varchar,
    arc_cover             varchar,
    arc_order             varchar,
    arc_priority          varchar
);
create unique index if not exists filter_id on filter (id);
create index if not exists filter_label on filter (label);


create table if not exists variable_filter
(
    variable_id varchar not null,
    filter_id   varchar not null,
    foreign key (variable_id) references variable (id) on delete cascade on update cascade,
    foreign key (filter_id) references filter (id) on delete cascade on update cascade
);
create index if not exists variable_filter_variable_id
    on variable_filter (variable_id);
create index if not exists variable_filter_filter_id
    on variable_filter (filter_id);


create table if not exists variableset
(
    name varchar not null
);
create unique index if not exists variableset_name on variableset (name);


create table if not exists variableset_variable
(
    variable_id      varchar not null,
    variableset_name varchar not null,
    _order           integer,
    priority         integer,
    foreign key (variable_id) references variable (id) on delete cascade on update cascade,
    foreign key (variableset_name) references variableset (name) on delete cascade on update cascade
);
create index if not exists variableset_variable_variable_id on variableset_variable (variable_id);
create index if not exists variableset_variable_variableset_name on variableset_variable (variableset_name);


create table if not exists assertion_variableset
(
    assertion_id     varchar not null,
    variableset_name varchar not null,
    foreign key (assertion_id) references assertion (id) on delete cascade on update cascade,
    foreign key (variableset_name) references variableset (name) on delete cascade on update cascade
);
create index if not exists assertion_variableset_name on assertion_variableset (variableset_name);
create index if not exists assertion_variableset_variable_id on assertion_variableset (assertion_id);


create table if not exists table_filter
(
    table_id  varchar not null,
    filter_id varchar not null,
    foreign key (table_id) references xbrl_table (id) on delete cascade on update cascade,
    foreign key (filter_id) references filter (id) on delete cascade on update cascade
);


create table if not exists arc
(
    rowid          serial,
    linkrole_rowid integer not null,
    arc_type       varchar not null,
    arcrole        varchar,
    from_id        varchar,
    from_type      varchar,
    to_id          varchar,
    to_type        varchar,
    _order         real,
    name           varchar,
    dts_name       varchar not NULL,
    foreign key (dts_name) references dts (name) on delete cascade on update cascade,
    foreign key (linkrole_rowid) references linkrole (rowid) on delete cascade on update cascade
);
create unique index if not exists arc_unique on arc (linkrole_rowid, arc_type, arcrole, from_id, to_id);
create index if not exists arc_linkrole_rowid on arc (linkrole_rowid);
create index if not exists arc_arc_type on arc (arc_type);
create index if not exists arc_arcrole on arc (arcrole);
create index if not exists arc_from on arc (from_id);
create index if not exists arc_to on arc (to_id);
create index if not exists arc_dts_name on arc (dts_name);


create table if not exists mutation
(
    rowid         serial,
    run_id        varchar not null,
    object_type   varchar not null,
    object_id     varchar not null,
    mutation_type varchar not null,
    old_version   varchar not null
);

create unique index if not exists mutation_unique on mutation (run_id, object_type, object_id, mutation_type);
create index if not exists mutation_object on mutation (object_type, object_id);
