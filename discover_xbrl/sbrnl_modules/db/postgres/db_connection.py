import psycopg2
from os.path import join, dirname
from discover_xbrl.conf.conf import Config


class DBConnection:
    def __init__(self, nt_version=None, conf=None, configfile=None, dbname=None):
        self.engine = "postgres"
        if conf is None:
            conf = Config(configfile)
        self.conf = conf
        self.dbname = None  # we set this later, if the connection succeeds
        user = conf.db["user"]
        password = conf.db["password"]
        host = conf.db.get("host", "localhost")
        port = conf.db.get("port", 5432)
        dbname = conf.db["dbname"] if dbname is None else dbname
        if nt_version:
            if conf.db["dbnames"].get(nt_version):
                dbname = conf.db["dbnames"][nt_version]
            else:
                raise ValueError(f"NT Versie: {nt_version} niet aanwezig")
        # First connect to the default postgres database to find out if the required DB exists
        try:
            self.connection = psycopg2.connect(
                f"dbname=postgres user={user} password='{password}' host={host} port={port}"
            )
        except psycopg2.OperationalError as e:
            print(f"Unable to connect to database {dbname} on {host} port {port}")
            raise ValueError(
                f"Unable to connect to database {dbname} on {host} port {port}"
            )

        if dbname != "postgres":
            cur = self.connection.cursor()
            self.connection.set_session(autocommit="on")
            cur.execute("select * from pg_database where datname = %s", (dbname,))
            rows = cur.fetchall()
            if not len(rows):
                # we need to create this database
                # print(f"Creating: {dbname}")
                cur.execute(f"create database {dbname}")
                self.connection.commit()
                try:
                    self.connection = psycopg2.connect(
                        f"dbname={dbname} user={user} password='{password}' host={host} port={port}"
                    )
                    cur = self.connection.cursor()
                    print(f"Creating database schema in: {dbname}")
                    with open(join(dirname(__file__), "create_db.sql")) as ddl_file:
                        ddl = ddl_file.read()
                        if ddl:
                            cur.execute(ddl)
                    self.connection.commit()
                except psycopg2.OperationalError as e:
                    print(f"Unable to connect! 2\n{e}")
                    raise psycopg2.OperationalError
            else:
                try:
                    self.connection = psycopg2.connect(
                        f"dbname={dbname} user={user} password='{password}' host={host} port={port}"
                    )
                except psycopg2.OperationalError as e:
                    print(f"Unable to connect! 3\n{e}")
                    raise psycopg2.OperationalError
            self.dbname = dbname

        self.connection.set_client_encoding("utf-8")  # We wants this,always.

    def switch_db(self, dbname=None):
        if not dbname:
            return False
        current_db = self.conf.db["dbname"]
        if dbname == current_db:
            return False
        # print(f"Switching to: {dbname}")
        self.connection = psycopg2.connect(
            f"dbname={dbname} "
            f"user={self.conf.db['user']} "
            f"password={self.conf.db['password']} "
            f"host={self.conf.db.get('host', 'localhost')} "
            f"port={self.conf.db.get('port', '5432')}"
        )

    @staticmethod
    def rewrite_query(query):
        # sql is written for the psycopg2-PDO, so we're good
        return query

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.connection:
            self.connection.close()
