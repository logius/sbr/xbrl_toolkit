"""
File that loads all relevant module parts.
"""
from .widget import TaxonomyPickerWidget
from .logging import setup_logger


def load_module(module_object={}):

    setup_logger()

    module_object["name"] = "XBRL_parser"
    module_object["type"] = "input"
    module_object["gui"] = TaxonomyPickerWidget
    module_object[
        "description"
    ] = """
    Used to load taxonomies into memory as a logical representation. During this parsing process, optionally validations
    are carried out as well. This can be done by choosing a validator beforehand. Currently loads most basic parts of a
    taxonomy, but does not process most parts of dimensions, tables or formula's.
    """

    return module_object
