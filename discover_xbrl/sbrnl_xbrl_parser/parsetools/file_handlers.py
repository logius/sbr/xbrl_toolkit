import logging

import requests.exceptions

from discover_xbrl.conf.conf import Config


import requests_cache
from requests.adapters import HTTPAdapter

from .classes import File, WWWPath

logger = logging.getLogger("parser.parsetools")
conf = Config()

"""
Setting up requests library
"""
nltaxonomie_adapter = HTTPAdapter(max_retries=3)

session = requests_cache.CachedSession("/tmp/xbrl_parser_cache")
session.mount("nltaxonomie.nl", nltaxonomie_adapter)


from .mem_fs import mem_fs


def return_file_contents(file, discovered_files):
    """
    Returns the contents of a local or remote file.
    If a file exists in memory within the discovered_files list, it will retrieve that one.
    :param file:
    :return:
    """
    if conf.parser["has_taxonomy_package"] is True and not isinstance(file, WWWPath):
        try:
            # print(mem_fs.tree(max_levels=3))
            filestr = str(file)
            file_bin = mem_fs.open(filestr)
            file_contents = file_bin.read()
            return file_contents
        except:
            logging.error(f"Could not find file in taxonomy package: {str(file)}")
            raise FileNotFoundError(f"{str(file)} niet in taxonomy package gevonden.")

    if hasattr(file, "name"):
        url = str(file)
    else:
        url = file

    # Return data if this is already saved in discovered files
    # dit gaat veel te vaak af, elke keer lopen door een steeds langer wordende lijst
    for saved_file in discovered_files:
        if url == saved_file.url:
            return saved_file.contents

    """If file hasn't been found before, try to access it locally or remotely"""

    if url.startswith("http"):
        logging.debug(f"Accessing remote file: {url}")

        # If file is external
        try:
            response = session.get(url)
            response.raise_for_status()
        except (ConnectionError, requests.exceptions.HTTPError) as ce:
            logging.error(ce)
            raise FileNotFoundError(f"URL: {url} kon niet worden geopend.")
        discovered_files.append(
            File(url, path=file, contents=response.text)
        )  # Save file for next query
        return response.text

    # Second option is it is a local file
    else:
        logging.debug(f"Accessing local file: {url}")
        # Try to open a local file and return contents
        try:
            with open(file) as f:
                response = f.read()
                discovered_files.append(
                    File(url, path=file, contents=response)
                )  # Save file for next query
                return response

        except FileNotFoundError:
            logging.error(f"Could not find local file: {url}.")
            raise FileNotFoundError(f"Bestand niet gevonden: {url}")
