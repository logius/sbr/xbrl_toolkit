import logging
import os
from datetime import datetime
from pathlib import Path

from lxml import etree
from .mem_fs import get_relative_path
from .classes import DTS, WWWPath
from .file_handlers import return_file_contents
from .helpers import pre_parse_dts_actions
from .taxonomy_package import Entrypoint, apply_remap
from ..linkbase.parse_linkbase import parse_linkbase, pre_parse_linkbase
from ..linkbase.postdiscovery import process_dts
from ..linkbase.postdiscovery.process_dts import rgs_postprocessing
from ..schema.parse_schema import parse_schema
from discover_xbrl.conf.conf import Config


logger = logging.getLogger("parser")


# Schema's that should not be checked as they are non-normative.
exempted_schemas = [
    # 'http://www.xbrl.org/2003/xl-2003-12-31.xsd'
    # 'http://www.xbrl.org/2003/xbrl-instance-2003-12-31.xsd',
    # 'http://www.xbrl.org/2003/xbrl-linkbase-2003-12-31.xsd',
    # 'http://www.xbrl.org/2005/xbrldt-2005.xsd',
    # 'http://www.xbrl.org/2008/generic-label.xsd',
    # 'http://www.xbrl.org/2008/generic-reference.xsd',
    # 'http://www.xbrl.org/2008/generic-link.xsd',
    # 'http://www.xbrl.org/dtr/type/nonNumeric-2009-12-16.xsd',
    # 'http://www.xbrl.org/2010/generic-message.xsd',
]

exempted_linkbases = {}


"""
HELPER FUNCTIONS
"""


def is_excluded_schema(url):
    """
    Returns true if schema is excluded from taxonomy.
    To be extended with extra checks and rules
    """

    for row in exempted_schemas:
        if row.__contains__(url.__str__()):
            # Given url is (partially) found in list
            return True

        elif url.__str__().__contains__(row):
            # An exempted url is (partially) found in given url
            return True

        elif "#" in url.__str__():
            url_parts = url.__str__().split("#")
            for url_part in url_parts:
                if row.__contains__(url_part):
                    # Given url is (partially) found in list
                    return True

                elif url_part.__contains__(row):
                    # An exempted url is (partially) found in given url
                    return True

    return False


def is_excluded_linkbase(url):
    """
    Returns true if schema is excluded from taxonomy.
    To be extended with extra checks and rules
    """
    if url in exempted_linkbases:
        return True
    else:
        return False


class Discover:
    """A collection of functions which are used to discover schema and linkbase files of an XBRL 2.1 taxonomy

    Note: this code is originally created to discover taxonomies created according to the Dutch Taxonomy Architecture.
    Please be aware that this approach may have led to specific assumptions.
    """

    def __init__(
        self,
        ep=None,
        discover_all_ep=False,
        validator=None,
        info_widget=None,
        taxonomy_package=None,
    ):
        """
        :param ep: Optional string, list of strings or Path instance referencing one or more entrypoint.py schema's.
        :param dts: Optional DTS class.
        """

        # If a taxonomy package is loaded, add this information to the discoverer
        self.taxonomy_package = taxonomy_package

        # Setup logging
        self.logger = logging.getLogger("parser")

        self.validator = validator

        self.ep = ep
        self.file_list = []  # List with all files found for all EP's

        self.dts_list = []
        self.discover_all_ep = discover_all_ep
        self.info_widget = info_widget
        self.ep_imports = []
        self.conf = Config()

    def get_ep_linkbaserefs(self, dts_file, discovered_files, ep=None, handeled=None):
        """
        Get the linkbaserefs that are referred to directly from the EP file.
        This information can be used to later find overlapping information between ep's/DTS universes

        :return: List with absolute paths for linkbaserefs
        """

        linkbaseref_list = []

        try:
            file_str = return_file_contents(dts_file, discovered_files=discovered_files)
        except FileNotFoundError as er:
            print(er)
            return None

        if file_str is not None:
            schema = etree.fromstring(bytes(file_str, encoding="utf-8"))
        else:
            logger.error(f"Entrypoint not found: {str(dts_file)}")
            return None

        if "xs" or "xsd" in schema.nsmap:
            # If the entrypoint contains imports, we should treat the found linkbases as directly referenced
            if "xs" in schema.nsmap:
                imports = schema.xpath("//xs:import", namespaces=schema.nsmap)
            else:
                imports = schema.xpath("//xsd:import", namespaces=schema.nsmap)
            if imports:
                for imp in imports:
                    schema_location = imp.get("schemaLocation")

                    if schema_location and not schema_location.startswith("http"):
                        location = Path(os.path.join(dts_file.parent, schema_location))
                        self.ep_imports.append(location)
                        linkbaseref_list.extend(
                            self.get_ep_linkbaserefs(location, discovered_files, ep=ep)
                        )

        discovererable_linkbaserefs = schema.xpath(
            f"//*[local-name() = 'schema']/*[local-name() = 'annotation']/*[local-name() = 'appinfo']/*[local-name() = 'linkbaseRef']",
            namespaces=schema.nsmap,
        )

        for linkbaseref in discovererable_linkbaserefs:
            linkbaseref_href = linkbaseref.get("{http://www.w3.org/1999/xlink}href")
            if not self.taxonomy_package:
                if isinstance(dts_file.parent, WWWPath):
                    abs_linkbaseref_href = str(dts_file.parent) + "/" + linkbaseref_href
                    linkbaseref_path = WWWPath(abs_linkbaseref_href)
                else:
                    abs_linkbaseref_href = os.path.join(
                        dts_file.parent, linkbaseref_href
                    )
                    linkbaseref_path = Path(os.path.normpath(abs_linkbaseref_href))
            else:
                remapped_rel_href = apply_remap(
                    linkbaseref_href, self.taxonomy_package["remappings"]
                )
                linkbaseref_path = get_relative_path(dts_file, remapped_rel_href)

            if str(linkbaseref_path) not in linkbaseref_list:
                linkbaseref_list.append(str(linkbaseref_path))

        """
        Note to self:
        Here I am really only interested in @roleURI of <link:roleRef> elements that are referred.
        Maybe also <link:{presentation/definition}Link> @xlink:role, although that should be the same.
        In theory, a XML file can contain multiple linkbases as well.
        
        Maybe it would be better to discover these right away, rather than puzzle these connections together at a later point.
        
        For now I choose to add a check an the linkbase discovery function to check this list, to add a flag at that point.
        """

        return linkbaseref_list

    def collect_dts(self, dts):

        self.dts_list.append(dts)

    def create_all_entrypoint(self):
        """
        Load each EP's schema, add linkbaseRef and namespaces to new (ALL) EP.

        :return:
        """
        logger.info(f"starting to create an all EP with {len(self.ep)} EP files")
        namespaces = {}
        all_ep = etree.Element("{http://www.w3.org/2001/XMLSchema}schema")

        all_ep_annotation = etree.Element(
            "{http://www.w3.org/2001/XMLSchema}annotation"
        )
        all_ep.append(all_ep_annotation)  # Add notation to new schema

        all_ep_appinfo = etree.Element("{http://www.w3.org/2001/XMLSchema}appinfo")
        all_ep_annotation.append(all_ep_appinfo)  # Add app info to annotation

        known_refs = []
        import time

        start = time.time()
        for ep in self.ep:
            logger.info(f"Adding EP: {ep} to 'all' entrypoint")
            ep_start = time.time()

            ep_path = Path(ep).parent  # Absolute path of the EP
            try:
                file = return_file_contents(ep, self.file_list)
            except FileNotFoundError as er:
                print(er)
                return None

            schema = etree.fromstring(bytes(file, encoding="utf-8"))
            # Handle namespaces
            if hasattr(schema, "nsmap"):
                namespaces.update(schema.nsmap)
            else:
                for elem in schema.getroot():
                    namespaces.update(elem.nsmap)

            # Handle references
            xpath_linkbaseref = schema.xpath(
                "//xs:schema/xs:annotation/xs:appinfo/link:linkbaseRef",
                namespaces=namespaces,
            )
            logger.info(f"Adding {len(xpath_linkbaseref)} linkbase references from EP")

            for linkbaseref in xpath_linkbaseref:
                linkbase_href = linkbaseref.attrib["{http://www.w3.org/1999/xlink}href"]
                # linkbase_arcrole = linkbaseref.attrib['{http://www.w3.org/1999/xlink}arcrole']
                # linkbase_role = linkbaseref.attrib['{http://www.w3.org/1999/xlink}role']

                # Get the absolute path of the file, check if it exists
                linkrole_abs_path = Path(
                    os.path.normpath(os.path.join(ep_path.__str__(), linkbase_href))
                )

                # we only need to check this once
                if not linkrole_abs_path.exists():
                    logger.error(
                        f"File referred in EP {ep} does not exist: {linkrole_abs_path}"
                    )
                    raise

                linkbaseref.attrib[
                    "{http://www.w3.org/1999/xlink}href"
                ] = linkrole_abs_path.__str__()

                # add linkbaseref if this particular file has not been added before

                if linkrole_abs_path not in known_refs:
                    all_ep_appinfo.append(linkbaseref)
                    known_refs.append(linkrole_abs_path)

            logger.info(
                f"After adding last EP, there are {len(known_refs)} referenced files in all"
            )
            # print(f"{ep} took {time.time() - ep_start} currently knows {len(known_refs)} files")
            ep_start = time.time()

        logger.info(f"Got an all EP with {len(known_refs)}referenced files")
        # print(f" all took: {time.time() - start}")

        etree.cleanup_namespaces(all_ep, top_nsmap=namespaces, keep_ns_prefixes=False)

        ep_string = etree.tostring(
            all_ep,
            pretty_print=True,
            xml_declaration=True,
            encoding="UTF-8",
            standalone=True,
        )

        # Check if path exists, otherwise create the path before writing.
        if not os.path.exists(self.conf.parser["ep_outputdir"]):
            os.makedirs(self.conf.parser["ep_outputdir"])

        url = Path.joinpath(Path(self.conf.parser["ep_outputdir"]), Path("all_ep.xsd"))

        with open(url, "wb") as all_ep_file:
            all_ep_file.write(ep_string)
        # print(f"with xml out: {time.time() - start}")
        return url

    def start_discovering(self):
        """
        Discovers all schema and linkbase files referenced by one or more entrypoints.
        :return:
        """
        conf = Config()

        # If all_EP is chosen, combine all EP's and only discover the set.
        if self.discover_all_ep:
            logger.warning("Creating an 'all' EP is deprecated for now")
            # all_ep = self.create_all_entrypoint()
            #
            # self.ep = []  # Clean EP list, only the all EP should be discovered
            # self.ep.append(Path(all_ep))

        if isinstance(self.ep, Entrypoint):
            print(self.ep.extra_documents)
            ep = self.discover_ep(self.ep)

        elif isinstance(self.ep, Path):
            logger.info(f"Discovering EP: {self.ep.name} ")
            ep = self.discover_ep(self.ep)

        else:
            logger.info(f"Discovering EP: {self.ep} ")
            ep = self.discover_ep(
                Path(self.ep) if not self.ep.startswith("http") else WWWPath(self.ep)
            )

        if ep:
            self.dts_list.append(ep)
            self.logger.info(f"Finished loading entrypoint")
            postprocess = True
        else:
            self.logger.info(f"Could not load entrypoint")
            print(f"{self.ep} is not a valid entrypoint")
            return None

        if postprocess:
            logger.info("Starting post processing")
            for dts in self.dts_list:
                # parse_objects is currently only used to create hierarchies.
                # All objects are discovered as part of the initial proces

                process_dts.parse_objects(dts=dts)
                if (
                    "http://www.nltaxonomie.nl/rgs/2015/arcrole/mapping"
                    in dts.arcroletypes
                ):
                    # Do some special RGS post processing
                    rgs_postprocessing(dts=dts)

                # Sort linkroleOrder
                lro = sorted(dts.linkrole_order)
                order_dict = {}
                for o in lro:
                    order_dict[o] = dts.linkrole_order[o]
                dts.linkrole_order = order_dict

        if self.validator:
            for dts in self.dts_list:
                logger.info("Starting model validations")
                self.validator.validate_dts(dts)
                self.validator.report.update_name_subversion(self.ep)
                self.validator.report.generate_report(
                    entrypoint_name=dts.entrypoint_name,
                    writefile=self.conf.validator.get("write_report"),
                )

        # # Write extra info to log
        # self.statistics()
        return True

    def discover_ep(self, ep, ep_schema=None):
        """
        Discover the entrypoint xsd
        :param ep:
        :return: None
        """

        ep_discoverable_files = (
            []
        )  # List of all files that still need to be discovered for this EP run
        ep_discovered_files = []  # List with all discovered files for this EP
        if self.conf.parser["has_taxonomy_package"]:
            # I want the filename on this location
            name = ep.name.replace(" ", "-").replace("/", "-")
            dts = DTS(name=name, generic_info=ep.__dict__)
        else:
            dts = DTS(ep.name)

        if self.taxonomy_package:
            ep_discoverable_files.append(Path(ep.href_remapped))
            for file in ep.extra_documents_remapped:
                ep_discoverable_files.append(
                    WWWPath(file) if file.startswith("http") else Path(file)
                )
        else:
            ep_discoverable_files.append(ep)

        # Get the paths of all directly referenced linkbaserefs
        if self.taxonomy_package:

            dts.directly_referenced_linkbases_str = self.get_ep_linkbaserefs(
                Path(ep.href_remapped), discovered_files=self.file_list, ep=ep
            )
            for href in ep.extra_documents_remapped:
                dts.directly_referenced_linkbases_str.extend(
                    self.get_ep_linkbaserefs(
                        WWWPath(href) if href.startswith("http") else Path(href),
                        discovered_files=self.file_list,
                    )
                )
            if len(self.ep_imports):
                ep_discoverable_files.extend(self.ep_imports)
            if dts.directly_referenced_linkbases_str is None:
                return None

        else:
            dts.directly_referenced_linkbases_str = self.get_ep_linkbaserefs(
                ep, discovered_files=self.file_list
            )
            if dts.directly_referenced_linkbases_str is None:
                return None

        logger.info(
            f"got {len(dts.directly_referenced_linkbases_str)} directly referenced linkbase files in EP: {ep.name}"
        )
        dts = pre_parse_dts_actions(dts=dts)

        while len(ep_discoverable_files) > 0:

            for (
                discoverable_file
            ) in (
                ep_discoverable_files
            ):  # Keep going as long as there are discoverable files

                self.discover_file(
                    discoverable_file,
                    ep,
                    ep_discoverable_files,
                    ep_discovered_files,
                    dts=dts,
                    all_files=self.file_list,
                )

        logger.info(
            f"[{datetime.now(tz=None).isoformat(timespec='seconds')}] Discovered files: {len(ep_discovered_files)}, left undiscovered: {len(ep_discoverable_files)}"
        )
        if self.taxonomy_package:
            logger.info(f"with EP '{ep.name}' The DTS consists of:")
        else:
            logger.info(f"with EP '{ep}' The DTS consists of:")

        logger.info(f"{len(dts.schemas)} schema files:")
        logger.info(f"{len(dts.linkbases)} linkbase files")
        logger.info(f"{len(dts.locs)} locators")
        logger.info(f"{len(dts.references)} references")
        logger.info(f"{len(dts.namespaces)} namespaces")
        logger.info(f"{len(dts.concepts)} concepts")
        logger.info(f"{len(dts.labels)} labels")

        return dts

    def discover_file(
        self,
        discoverable_file,
        ep,
        ep_discoverable_files,
        ep_discovered_files,
        dts,
        all_files,
    ):
        """
        Discover a file. Input can be a linkbase or schema. Can be called recursively.
        :return:
        """

        if discoverable_file.suffix == ".xml":
            # If file is not in DTS yet, add it and do the full discovery

            self.logger.debug(f"Discovering linkbase {discoverable_file.__str__()}")

            """
            If a schema is found in a linkbase, it needs to be discovered before continuing.
            """
            found_files = pre_parse_linkbase(
                discoverable_file, dts, ep, all_files, validator=self.validator
            )

            # If schema's are found within the linkbase, deal with those first as they contain relevant information.
            for found_file in found_files:
                if (
                    not is_excluded_schema(found_file)
                    and found_file not in ep_discovered_files
                ):
                    # print(f"pre_parse_linkbase discovering: {found_file}")
                    self.discover_file(
                        discoverable_file=found_file,
                        ep=ep,
                        ep_discoverable_files=ep_discoverable_files,
                        ep_discovered_files=ep_discovered_files,
                        dts=dts,
                        all_files=all_files,
                    )

            # Parse the linkbase itself
            parse_linkbase(
                discoverable_file, dts, ep, all_files, validator=self.validator
            )

        # iterate through undiscovered schemas
        elif discoverable_file.suffix == ".xsd":

            self.logger.debug(f"Discovering schema {discoverable_file.__str__()}")

            found_schemas = parse_schema(
                url=discoverable_file,
                dts=dts,
                ep=ep,
                discoverable_files=ep_discoverable_files,
                discovered_files=ep_discovered_files,
                all_files=self.file_list,
                validator=self.validator,
            )

            for found_schema in found_schemas:
                # print(f"Found: {found_schema} Discovered: {ep_discovered_files}" )
                if (
                    not is_excluded_schema(found_schema)
                    and found_schema not in ep_discovered_files
                ):
                    self.discover_file(
                        discoverable_file=found_schema,
                        ep=ep,
                        ep_discoverable_files=ep_discoverable_files,
                        ep_discovered_files=ep_discovered_files,
                        dts=dts,
                        all_files=all_files,
                    )

        # take out of discoverable queue, add to discovered queue.
        if discoverable_file in ep_discoverable_files:
            ep_discoverable_files.pop(ep_discoverable_files.index(discoverable_file))
        ep_discovered_files.append(discoverable_file)
