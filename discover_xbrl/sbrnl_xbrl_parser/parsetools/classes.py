import pickle
import re
from os.path import normpath
from pathlib import Path
from urllib.parse import urlparse


class File:
    """
    A file found during discovery
    """

    all_paths = []

    def __init__(self, url, path=None, contents=""):
        self.url = url  # the absolute url of the file
        self.path = path  # The local path if available
        self.contents = contents
        self.all_paths.append(path)

        extension = ""  # xsd or xml
        namespaces = []  # list of namespaces
        refers = []  # list of files it refers to

    def __unicode__(self):
        return self.url

    def __str__(self):
        return self.url


class DTS:
    def __init__(self, name, shortname=None, generic_info=None):
        self.entrypoint_name = name  # Name of the EP that is used to start discovery
        if shortname is not None:
            self.__shortname = shortname

        # Generic info is used when building a DTS out of anything other than an existing taxonomy.
        # Such as information on domain, name of entrypoint and publication date can be passed
        # to the builder.
        self.generic_info = generic_info

        self.directly_referenced_linkbases = []
        self.indirectly_referenced_linkbases = []
        self.directly_referenced_linkbases_str = []

        self.linkrole_order = {}
        self.namespaces = {}
        self.schemas = []
        self.filters = []
        self.linkbases = []
        self.locs = {}
        self.concepts = {}
        self.labels = {}
        # self.tables = {}
        self.roletypes = {}
        self.arcroletypes = {}
        self.discovered_files = []
        self.entrypoints = []
        self.datatypes = []
        self.presentation_hierarchy = None

    @property
    def all_labels(self):
        labels = {}
        for roletype in self.roletypes.values():
            if roletype.labels:
                labels.update(roletype.labels)
            if roletype.element_labels:
                labels.update(roletype.element_labels)
        for concept in self.concepts.values():
            if concept.labels:
                labels.update(concept.labels)
        return labels

    @property
    def tables(self):
        tables = []
        for roletype in self.roletypes.values():
            if roletype.tables:
                tables.extend(roletype.tables)
        return tables

    @property
    def assertions(self):
        assertions = []
        for roletype in self.roletypes.values():
            assertions.extend(roletype.resources["assertions"])
        return list(set(assertions))

    @property
    def references(self):
        references = []
        for roletype in self.roletypes.values():
            references.extend(roletype.references)
        return list(set(references))

    @property
    def variables(self):
        variables = []
        for roletype in self.roletypes.values():
            variables.extend(roletype.resources["variables"])
        return list(set(variables))

    @property
    def get_resources(self):
        resources = {}
        for roletype in self.roletypes.values():
            for resourcetype, resourcelist in roletype.resources.items():
                if resourcetype not in resources.keys():
                    resources[resourcetype] = resourcelist.copy()
                else:
                    resources[resourcetype].extend(resourcelist)
                    resources[resourcetype] = list(set(resources[resourcetype]))

        return resources

    def get_resource(self, xlink_label):
        """
        Returns a resource with a given xlink_label
        """
        for type, resource_list in self.get_resources.items():
            for resource in resource_list:
                if resource.xlink_label == xlink_label:
                    return resource
        return None

    @property
    def hypercubes(self):
        hypercubes = []
        for roletype in self.roletypes.values():
            if roletype.hypercube:
                hypercubes.append(roletype.hypercube)
        return hypercubes

    @property
    def table_of_contents(self):
        return self.presentation_hierarchy

    @property
    def shortname(self):
        """
        Get a short name if this has not yet been given.
        This is ofthen the case when loading existing taxonomies

        todo: In the future, it is probably better to deduce this from the @id of the entrypoint schema

        :return:
        """
        if hasattr(self, "__shortname"):
            if self.__shortname:
                return self.__shortname

        # Try to come up with a short name by removing parts of the entrypoint name

        shortname = re.sub(".xsd", "", self.entrypoint_name)  # remove extension
        shortname = shortname.rsplit("-", 1)[
            -1
        ]  # Remove everything before the last dash
        if shortname == "micro":
            return "rpt-h-u"
        elif shortname == "klein":
            return "rpt-h-k"
        elif shortname == "middelgroot":
            return "rpt-h-m"
        elif shortname == "groot":
            return "rpt-h-g"
        elif shortname == "premiepensioeninstellingen":
            return "rpt-ppi"
        elif shortname == "zorginstellingen":
            return "rpt-zi"
        else:
            pass
            # print("No shortname!")

        return shortname

    def save_DTS(self, path):
        """
        Save the DTS to the given path
        :return:
        """
        dts_to_save = self

        json = Path(str(path))
        with open(json, "wb") as p:
            pickle.dump(dts_to_save, p)

    def __str__(self):

        return self.entrypoint_name


class WWWPath(object):
    """
    Placeholder to work with URI's instead of local paths
    """

    def __init__(self, path):
        self.url = self.absolute_url(path)

        protocol, parts = str(path).split("//", 2)
        self.protocol = protocol + "//"
        self.parts = [self.protocol] + str(parts).split("/")

        self.root = self.parts[0] + self.parts[1] + "/"
        self.name = self.parts[-1]

    def __str__(self):
        return self.url

    def __repr__(self):
        return f"WWWPath: {self.url}"

    def __eq__(self, other):
        if not isinstance(other, WWWPath):
            # don't attempt to compare against unrelated types
            return NotImplemented

        return self.url == other.url

    def __hash__(self):
        return hash(self.url)

    @property
    def parent(self):
        parsed_url = urlparse(self.url)
        parent_path = parsed_url.path.rsplit("/", 1)[0]
        parent_path = parsed_url.scheme + "://" + parsed_url.hostname + parent_path
        return WWWPath(parent_path)

    @property
    def suffix(self):
        """
        The final component's last suffix, if any.

        This includes the leading period. For example: '.txt'
        """
        name = self.name
        i = name.rfind(".")
        if 0 < i < len(name) - 1:
            return name[i:]
        else:
            return ""

    def absolute_url(self, path):
        url = urlparse(path)
        new = normpath(url.path)

        absolute = url.scheme + "://" + url.hostname + new
        return absolute


class VirtualPath(object):
    """
    Placeholder to work with virtual paths, like in memory constructed EPs
    """

    def __init__(self, path):

        self.url = path
        self.parts = str(path).split("/")
        self.root = "virtual://"
        self.name = self.parts[-1]

    def __str__(self):
        return self.url

    def __eq__(self, other):
        if not isinstance(other, VirtualPath):
            # don't attempt to compare against unrelated types
            return NotImplemented

        return self.url == other.url

    def __hash__(self):
        return hash(self.url)

    @property
    def suffix(self):
        """
        The final component's last suffix, if any.

        This includes the leading period. For example: '.txt'
        """
        name = self.name
        i = name.rfind(".")
        if 0 < i < len(name) - 1:
            return name[i:]
        else:
            return ""
