import zipfile
import logging
from os.path import dirname

from lxml import etree

logger = logging.getLogger("parser")


def get_multilingual_label(dict):
    # prefer the NL label
    if "nl" in dict.keys():
        return dict["nl"]

    # else, try to get the english label
    elif "en" in dict.keys():
        return dict["en"]

    # If all fails, just get the first one
    elif dict:
        return list(dict.values())[0]
    else:
        return ""


class Entrypoint:
    def __init__(
        self,
        name,
        description,
        version,
        href,
        href_remapped,
        extra_documents=None,
        extra_documents_remapped=None,
        languages=None,
        dts=None,
        remappings=None,
        package_name=None,
    ):

        # Name(s) and description(s) can contain multiple entries if it is defined as a multi-lingual element
        if isinstance(name, str):
            self.name = name

        elif isinstance(name, dict):
            self.names = name
            self.name = get_multilingual_label(name)

        if isinstance(description, str):
            self.description = description

        elif isinstance(description, dict):
            self.descriptions = description
            self.description = get_multilingual_label(description)

        self.version = version
        self.href = href
        self.href_remapped = href_remapped
        self.languages = languages
        self.dts = dts
        self.remappings = remappings
        self.package_name = package_name
        if isinstance(extra_documents, list):
            self.extra_documents = extra_documents
            self.extra_documents_remapped = extra_documents_remapped

    def __lt__(self, other):
        if hasattr(other, "name"):
            return self.name < other.name
        else:
            return self.name < other

    def __str__(self):
        return self.name

    def __repr__(self):
        return f"{self.name} ({self.href.split('/')[-1]})"


def apply_remap(href, remappings, package_name=None):
    """
    Apply remapping with best matched URL
    :return:
    """
    # Find the remapped value that fits best
    matched_key = ""

    for k, v in remappings.items():
        if k in href:

            if len(k) > len(matched_key):
                matched_key = k

    if matched_key:

        href = href.replace(matched_key, remappings[matched_key])  # replace key
        if package_name:
            href = href.replace(
                "..", package_name
            )  # replace axes selector with package name

        # If we found a match, replace the matched key from remappings with its value
        return href
    else:
        # Else, return the href unmodified
        return href


def get_multilang_elems(found_elems, document_lang):

    if len(found_elems) <= 1:
        # if only one element is found, return its text value with the document language

        if document_lang is None:
            lang = found_elems[0].get("{http://www.w3.org/XML/1998/namespace}lang")

            if lang is None:
                raise Exception("No document or element language set")
            else:
                return {lang: found_elems[0].text}
        if len(found_elems):
            return {document_lang: found_elems[0].text}
        else:
            return {}
    else:
        # Need to get a value for each language
        langs = {}

        for elem in found_elems:
            elem_lang = elem.get("{http://www.w3.org/XML/1998/namespace}lang")
            if elem_lang not in langs:
                langs[elem_lang] = elem.text
            else:
                print(f"Language '{elem_lang}' already defined")
        return langs


def read_catalog(xml):
    remappings = {}
    tree = etree.fromstring(xml)
    if tree.tag != "{urn:oasis:names:tc:entity:xmlns:xml:catalog}catalog":
        print("tpe:invalidCatalogFile")
    else:
        for xml_element in tree.iterchildren():

            ist = xml_element.get("uriStartString")
            soll = xml_element.get("rewritePrefix")

            if ist not in remappings.keys():
                remappings[ist] = soll
            else:
                print("value for 'uriStartString' defined multiple times!")

    return remappings


def read_package_metadata(xml, remappings):
    """
    Get all metadata information for taxonomy package

    :param xml:
    :return:
    """
    metadata = {}
    tree = etree.fromstring(xml)
    # Try getting the package language if it is not a multilanguage package

    lang = tree.get("{http://www.w3.org/XML/1998/namespace}lang")

    metadata["identifier"] = (
        tree.find("{http://xbrl.org/2016/taxonomy-package}identifier").text
        if tree.find("{http://xbrl.org/2016/taxonomy-package}identifier") is not None
        else ""
    )
    metadata["version"] = (
        tree.find("{http://xbrl.org/2016/taxonomy-package}version").text
        if tree.find("{http://xbrl.org/2016/taxonomy-package}version") is not None
        else ""
    )

    license_xml = tree.find("{http://xbrl.org/2016/taxonomy-package}license")
    if license_xml is not None:
        metadata["licenseName"] = license_xml.get("name")

        metadata["licenseHref_remapped"] = apply_remap(
            href=license_xml.get("href"), remappings=remappings
        )

        metadata["licenseHref"] = apply_remap(
            href=license_xml.get("href"), remappings=remappings
        )

    metadata["publisherURL"] = (
        tree.find("{http://xbrl.org/2016/taxonomy-package}publisherURL").text
        if tree.find("{http://xbrl.org/2016/taxonomy-package}publisherURL") is not None
        else ""
    )

    metadata["publisherCountry"] = (
        tree.find("{http://xbrl.org/2016/taxonomy-package}publisherCountry").text
        if tree.find("{http://xbrl.org/2016/taxonomy-package}publisherCountry")
        is not None
        else ""
    )

    metadata["publicationDate"] = (
        tree.find("{http://xbrl.org/2016/taxonomy-package}publicationDate").text
        if tree.find("{http://xbrl.org/2016/taxonomy-package}publicationDate")
        is not None
        else ""
    )

    multilang_tags = {
        "name": "{http://xbrl.org/2016/taxonomy-package}name",
        "description": "{http://xbrl.org/2016/taxonomy-package}description",
        "publisher": "{http://xbrl.org/2016/taxonomy-package}publisher",
    }

    for elem_name, multilang_elem in multilang_tags.items():
        found_elems = tree.findall(multilang_elem)
        metadata[elem_name] = get_multilang_elems(
            found_elems=found_elems, document_lang=lang
        )

    return metadata


def read_entrypoints(xml, remappings, package_name, taxonomy_package):
    entrypoints = []
    tree = etree.fromstring(xml)

    # Try getting the package language if it is not a multilanguage package
    lang = tree.get("{http://www.w3.org/XML/1998/namespace}lang")

    entrypoints_xml = tree.find("{http://xbrl.org/2016/taxonomy-package}entryPoints")
    for ep_xml in entrypoints_xml.findall(
        "{http://xbrl.org/2016/taxonomy-package}entryPoint"
    ):

        ep_version = ep_xml.find("{http://xbrl.org/2016/taxonomy-package}version")
        if ep_version is not None:
            ep_version = ep_version.text
        else:
            ep_version = ""

        entryPointDocument = ep_xml.find(
            "{http://xbrl.org/2016/taxonomy-package}entryPointDocument"
        ).get("href")

        ep_href_remapped = apply_remap(
            href=entryPointDocument, remappings=remappings, package_name=package_name
        )

        ep_href = entryPointDocument

        # 20230627 entryPointDocument can be many
        # the rest of the documents will be added to a list and a remappped list of
        # follow-up documents.
        documents_all = ep_xml.findall(
            "{http://xbrl.org/2016/taxonomy-package}entryPointDocument"
        )[1:]
        extra_documents = []
        extra_documents_remapped = []
        for doc in documents_all:
            extra_documents.append(doc.get("href"))
            extra_documents_remapped.append(
                apply_remap(
                    href=doc.get("href"),
                    remappings=remappings,
                    package_name=package_name,
                )
            )

        ep_elem_name = get_multilang_elems(
            found_elems=ep_xml.findall("{http://xbrl.org/2016/taxonomy-package}name"),
            document_lang=lang,
        )
        ep_elem_description = get_multilang_elems(
            found_elems=ep_xml.findall(
                "{http://xbrl.org/2016/taxonomy-package}description"
            ),
            document_lang=lang,
        )

        languages = []
        languages_xml = ep_xml.find("{http://xbrl.org/2016/taxonomy-package}languages")

        if languages_xml is not None:
            for language in languages_xml.findall(
                "{http://xbrl.org/2016/taxonomy-package}language"
            ):
                lang = language.get("{http://www.w3.org/XML/1998/namespace}lang")
                if not lang:
                    lang = language.text
                languages.append(lang)

        ep_languages = languages

        ep = Entrypoint(
            name=ep_elem_name,
            description=ep_elem_description,
            version=ep_version,
            href=ep_href,
            href_remapped=ep_href_remapped,
            languages=ep_languages,
            extra_documents=extra_documents,
            extra_documents_remapped=extra_documents_remapped,
            remappings=remappings,
        )
        entrypoints.append(ep)

    return entrypoints


def read_taxonomy_package(selected_file):
    remappings = {}  # contains remappings described in the optional catalog.xml file
    entrypoints = {}
    metadata = {}

    if selected_file is not None:
        with zipfile.ZipFile(selected_file, mode="r") as taxonomy_zip:

            package_name = selected_file.split("/")[-1].replace(".zip", "")

            root_package_name = str(dirname(taxonomy_zip.namelist()[0])).split("/")[0]
            messages = []

            if root_package_name != package_name:
                # Expected a directory with the same name of the zip archive
                print(
                    f"Archive does not contain a folder like the zipfile name as recommended: "
                    f"{package_name} - {root_package_name}"
                )
                messages.append(
                    "Archive does not contain a folder like the zipfile name as recommended"
                )

            # Read the catalog file if this exists
            if (
                f"{root_package_name}/META-INF/catalog.xml"
                not in taxonomy_zip.namelist()
            ):
                print(
                    "Optional catalog.xml file does not exist. Not using any URI remapping"
                )
                messages.append(
                    "Optional catalog.xml file does not exist. Not using any URI remapping"
                )
            else:
                catalog_xml = taxonomy_zip.read(
                    f"{root_package_name}/META-INF/catalog.xml"
                )
                remappings = read_catalog(xml=catalog_xml)

            # Read the taxonomyPackage.xml file
            if (
                f"{root_package_name}/META-INF/taxonomyPackage.xml"
                not in taxonomy_zip.namelist()
            ):
                print("taxonomyPackage.xml file does not exist!")
                messages.append("taxonomyPackage.xml file does not exist!")
            else:
                taxonomyPackage_xml = taxonomy_zip.read(
                    f"{root_package_name}/META-INF/taxonomyPackage.xml"
                )

                metadata = read_package_metadata(
                    xml=taxonomyPackage_xml, remappings=remappings
                )

                entrypoints = read_entrypoints(
                    xml=taxonomyPackage_xml,
                    remappings=remappings,
                    package_name=root_package_name,
                    taxonomy_package=taxonomy_zip,
                )

    return {
        "metadata": metadata,
        "remappings": remappings,
        "entrypoints": entrypoints,
        "messages": messages,
    }
