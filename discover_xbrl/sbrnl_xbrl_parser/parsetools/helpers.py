import os
from pathlib import Path

from lxml import etree

from .classes import WWWPath
from .file_handlers import return_file_contents
from ..schema.classes import RoleType


def follow_loc(url, id, discovered_files):
    """
    Returns the XML element referred by a loc
    """

    file_contents = return_file_contents(url, discovered_files=discovered_files)
    if not file_contents:
        return None

    # Return the information
    root = etree.fromstring(bytes(file_contents, encoding="utf-8"))
    element = root.xpath(f"//*[@id = '{id}']")
    if len(element) > 0:

        return element[0]
    else:
        return None


def clean_url(url, abs_path=""):
    """
    combines the relative url with the absolute path.
    If the relative portion has an anchor (# with xlink statement), this will be cleaned first.
    """

    if isinstance(url, str):
        if "#" in url:
            # if the url has an anchor, remove that first
            url = url.split("#")[0]

        if "http" in url:
            # Url is probably external and should be treated as valid
            return WWWPath(url)

    # If instance is WWWPath, it can be treated the same as a string with 'http' in it.
    if isinstance(url, WWWPath):
        if "#" in url.url:
            # if the url has an anchor, remove that first
            url.url = url.url.split("#")[0]
        return url
    if isinstance(abs_path, WWWPath):
        abs_path = str(abs_path)
        return WWWPath(abs_path + "/" + url)

    href = os.path.normpath(os.path.join(abs_path, url))
    return Path(href)


def pre_parse_dts_actions(dts):
    """
    Runs some pre-parse actions on the DTS

    :param dts: DTS object
    :return: DTS object
    """

    # Add the 2003/role/link linkrole as this is not found by the parser automatically
    dts.roletypes["http://www.xbrl.org/2003/role/link"] = RoleType(
        roleURI="http://www.xbrl.org/2003/role/link", parent_dts=dts
    )
    return dts


def version_from_identifier(identifier):
    version = None
    if identifier.startswith("http"):
        version = identifier.split("/")[-1]
        if version[0].isnumeric():
            version = identifier.split("/")[-2]
            if version[0].isnumeric():
                version = identifier.split("/")[-3]
    elif identifier.startswith("urn:"):
        version = identifier.split(":")[-1]
    return version
