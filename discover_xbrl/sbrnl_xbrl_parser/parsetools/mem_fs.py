import fs
from fs.copy import copy_fs
from fs.zipfs import ZipFS

mem_fs = fs.open_fs("mem://")


def save_taxonomy_memfs(taxonomy_package):

    zip_fs = ZipFS(taxonomy_package)

    # Copy the zip to Mem_FS
    print("Unpacking taxonomy package to memory")
    copy_fs(src_fs=zip_fs, dst_fs=mem_fs)
    print("Done unpacking taxonomy package to memory")
    # print(mem_fs.tree())


def get_relative_path(base, relative):
    base_without_file = str(base).rsplit("/", 1)[0]
    joined_path = fs.path.combine(base_without_file, relative)
    normed_path = fs.path.normpath(joined_path)
    return normed_path
