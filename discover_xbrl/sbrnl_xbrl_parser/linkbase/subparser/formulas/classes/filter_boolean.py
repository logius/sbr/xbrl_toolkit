import logging

from .filter import Filter

logger = logging.getLogger("parser.linkbase")


class FilterBooleanOr(Filter):
    def __init__(self, id, xlink_label, subfilters):
        self.subfilters = subfilters

        super().__init__(xlink_label=xlink_label, id=id)

    def test_facts(self, facts):
        """
        Test to check if filter

        :param instance:
        :return:
        """

        returned_facts = []

        # expected_objects = [subfilter.qname_obj for subfilter in self.subfilters]

        for subfilter in self.subfilters:
            # Add the contents of the subfilter(s) to returned_facts
            if not subfilter.arc_cover == "false":
                # if subfilter.arc_complement == "false":
                sub_facts = subfilter.test_facts(facts=facts)
                returned_facts.extend(sub_facts)
                #     return sub_facts

        return returned_facts


class FilterBooleanAnd(Filter):
    def __init__(self, id, xlink_label, subfilters):
        self.subfilters = subfilters

        super().__init__(xlink_label=xlink_label, id=id)

    def test_facts(self, facts):

        returned_facts = []
        # Get the subfilters
        expected_objects = []
        for subfilter in self.subfilters:
            expected_objects.append(subfilter.qname_obj)

        # Test if all objects are found within the instance
        for fact in facts:
            if fact.concept_obj in expected_objects:
                if fact.concept_obj not in returned_facts:
                    returned_facts.append(fact)

        # if the list of returned facts is as long as the expected list, return this list. Otherwise, return an empty list
        if len(returned_facts) == len(expected_objects):
            return returned_facts
        else:
            return []
