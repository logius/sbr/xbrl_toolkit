import logging

from .filter import Filter

logger = logging.getLogger("parser.linkbase")


class FilterConceptRelation(Filter):
    def __init__(self, id, xlink_label, qname, linkrole, arcrole, axis):
        super().__init__(xlink_label=xlink_label, id=id)

        self.qname = qname
        self.linkrole = linkrole
        self.arcrole = arcrole
        self.axis = axis

    def test_facts(self, facts):
        returned_facts = []

        filter_linkrole = self.linkrole
        filter_axis = self.axis
        filter_arcrole = self.arcrole
        filter_qname = self.qname

        filter_concepts = []

        # todo: We are trying to not use dts.linkrole.arcs, so we should find a better way to store these relationships
        for arc in self.linkrole.arcs:

            # We are only interested in arcs of the arcrole given in the filter
            if arc.arcrole == filter_arcrole:

                # For a child axis, we want to find the children (to:) of the fact.concept
                if filter_axis in ["child", "child-or-self"]:

                    # If the filter qname matches with arc, add this concept to the list of expected objects
                    if (
                        f"{arc.from_concept_obj.ns_prefix}:{arc.from_concept_obj.name}"
                        == filter_qname
                    ):
                        filter_concepts.append(arc.to_concept_obj)

                        # Add self to set is that has not been done yet
                        if (
                            filter_axis == "child-or-self"
                            and arc.from_concept_obj not in filter_concepts
                        ):
                            filter_concepts.append(arc.from_concept_obj)

                elif filter_axis == "descendant-or-self":
                    raise NotImplemented

                elif filter_axis == "descendant":
                    # The descendant axis selects all descendants (children, grandchildren, etc)
                    raise NotImplemented

                elif filter_axis in ["parent", "parent-or-self"]:
                    # If the filter qname matches with arc, add this concept to the list of expected objects
                    if (
                        f"{arc.to_concept_obj.ns_prefix}:{arc.to_concept_obj.name}"
                        == filter_qname
                    ):
                        filter_concepts.append(arc.from_concept_obj)

                        # Add self to set is that has not been done yet
                        if (
                            filter_axis == "child-or-self"
                            and arc.to_concept_obj not in filter_concepts
                        ):
                            filter_concepts.append(arc.to_concept_obj)

                elif filter_axis == "ancestor-or-self":
                    raise NotImplemented

                elif filter_axis == "ancestor":
                    # The ancestor axis selects all ancestors element (parent, grandparent, great-grandparents, etc.)
                    raise NotImplemented

                elif filter_axis == "sibling":
                    raise NotImplemented

                elif filter_axis == "sibling-or-self":
                    raise NotImplemented

                elif filter_axis == "sibling-or-descendant":
                    raise NotImplemented

                else:
                    print("CRF axis not understood")

        for fact in facts:
            if fact.concept_obj in filter_concepts:

                returned_facts.append(fact)

        return returned_facts
