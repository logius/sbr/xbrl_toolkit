class FormulaPeriod:
    def __init__(self, periodtype=None, value=None, start=None, end=None, elem=None):

        # If LXML
        if elem is not None:
            # Create the period type using LXML element type
            for child in elem.getchildren():
                if child.tag == "{http://xbrl.org/2008/formula}duration":
                    self.start = child.get("start")
                    self.end = child.get("end")
                    self.periodtype = "duration"
                elif child.tag == "{http://xbrl.org/2008/formula}instant":
                    self.value = child.get("value")
                    self.periodtype = "instant"
                elif child.tag == "{http://xbrl.org/2008/formula}forever":
                    self.periodtype = "forever"
        else:
            # Create the period type using attributes
            self.periodtype = periodtype
            if self.periodtype == "duration":
                self.start = start
                self.end = end
            elif self.periodtype == "instant":
                self.value = value

    def __repr__(self):
        if not hasattr(self, "value"):
            if hasattr(self, "start"):
                return f"FormulaPeriod(periodtype='{self.periodtype}', start='{self.start}', end='{self.end}')"
            else:
                return f"FormulaPeriod(periodtype='{self.periodtype}')"
        else:
            return (
                f"FormulaPeriod(periodtype='{self.periodtype}', value='{self.value}')"
            )


class FormulaExplicitDimension:
    # def __init__(self, elem=None):
    def __init__(self, qname_dimension, qname_member=None, omit=False):

        self.dimensiontype = "explicit"
        self.omit = omit

        if isinstance(qname_dimension, str):
            self.__qname_dimension = qname_dimension
            self.qname_dimension_obj = None

        else:
            self.qname_dimension_obj = qname_dimension

        if isinstance(qname_member, str):
            self.__qname_member = qname_member
            self.qname_member_expression = qname_member
            self.qname_member_obj = None
        else:
            self.qname_member_obj = qname_member
            self.__qname_member = None

    @property
    def qname_dimension(self):
        if self.qname_dimension_obj:
            return (
                f"{self.qname_dimension_obj.ns_prefix}:{self.qname_dimension_obj.name}"
            )
        else:
            return self.__qname_dimension

    @property
    def qname_member(self):
        if self.qname_member_obj:
            return f"{self.qname_member_obj.ns_prefix}:{self.qname_member_obj.name}"
        else:
            return self.__qname_member

    @property
    def type_qname(self):
        if self.qname_member_obj:
            return f"{self.qname_member_obj.ns_prefix}:{self.qname_dimension_obj.name}"


class FormulaTypedDimension:
    # def __init__(self, xpath=None, value=None, elem=None):
    def __init__(self, qname_dimension, xpath=None, value=None):

        self.dimensiontype = "typed"

        self.xpath = xpath
        self.value = value

        if isinstance(qname_dimension, str):
            self.__qname = qname_dimension
            self.qname_dimension_obj = None

        else:
            self.qname_dimension_obj = qname_dimension

    @property
    def qname_dimension(self):
        if self.qname_dimension_obj:
            return (
                f"{self.qname_dimension_obj.ns_prefix}:{self.qname_dimension_obj.name}"
            )
        else:
            return self.__qname

        # self.dimensiontype = "typed"
        # self.dimension = elem.get("dimension")
        #
        # # If LXML
        # if elem is not None:
        #     # Create the period type using LXML element type
        #     for child in elem.getchildren():
        #         if child.tag == "xpath":
        #             pass
        #         elif child.tag == "value":
        #             pass
