from discover_xbrl.sbrnl_xbrl_parser.linkbase.classes import Resource


class Filter(Resource):

    # variable-filter
    # http://www.xbrl.org/2008/variable.xsd

    def __init__(
        self,
        xlink_label,
        id=None,
        arc_complement=None,
        arc_cover=None,
        arc_order=None,
        arc_priority=None,
    ):

        super().__init__(xlink_label=xlink_label, id=id)

        self.arc_complement = arc_complement
        self.arc_cover = arc_cover
        self.arc_order = arc_order
        self.arc_priority = arc_priority

    def __str__(self):

        if self.id:
            return self.id
        elif self.xlink_label:
            return self.xlink_label

    def __eq__(self, other):
        return self.xlink_label == other.xlink_label

    def test_facts(self, facts, variable_value=None):
        raise NotImplemented
