from discover_xbrl.sbrnl_xbrl_parser.linkbase.classes import Resource


class Assertion(Resource):
    def __init__(
        self,
        id,
        xlink_label,
        labels=None,
        aspect_model=None,
        implicit_filtering=None,
        filterSet=None,
        variableSet=None,
        severity=None,
    ):

        super().__init__(xlink_label=xlink_label, id=id)
        self.aspectModel = aspect_model
        self.implicitFiltering = implicit_filtering
        self.labels = labels if labels is not None else {}
        self.severity = severity

        # TODO: EK: Not sure if these sets use the correct nomenclature

        if variableSet:
            self.variableSet = variableSet
        else:
            self.variableSet = []

        if filterSet:
            self.filterSet = filterSet
        else:
            self.filterSet = []

    def __repr__(self):
        return self.xlink_label


class ExistenceAssertion(Assertion):
    def __init__(
        self,
        id: str,
        xlink_label: str,
        aspect_model: str,
        implicit_filtering: bool,
        labels: dict = None,
        test: str = None,
    ):
        super().__init__(
            id=id,
            xlink_label=xlink_label,
            labels=labels,
            aspect_model=aspect_model,
            implicit_filtering=implicit_filtering,
        )

        self.aspectModel = aspect_model
        self.implicitFiltering = implicit_filtering
        self.test = test


class ValueAssertion(Assertion):
    def __init__(
        self,
        id: str,
        xlink_label: str,
        aspect_model: str,
        implicit_filtering: bool,
        labels: dict = None,
        test: str = None,
    ):

        super().__init__(
            id=id,
            xlink_label=xlink_label,
            labels=labels,
            aspect_model=aspect_model,
            implicit_filtering=implicit_filtering,
        )

        self.aspectModel = aspect_model
        self.implicitFiltering = implicit_filtering
        self.test = test
