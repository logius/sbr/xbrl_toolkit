from .filter import Filter
import logging

logger = logging.getLogger()


class FilterConcept(Filter):

    # Does not seem to be more than an abstract placeholder. Maybe omit this step?
    pass


class FilterConceptName(FilterConcept):

    """
    name
    http://www.xbrl.org/specification/conceptfilters/rec-2009-06-22/conceptfilters-rec-2009-06-22.html#sec-concept-name-filter
    """

    def __init__(self, qname, xlink_label, qname_expression=None, id=None):
        super(FilterConceptName, self).__init__(id=id, xlink_label=xlink_label)

        self.qname_expression = qname_expression

        if isinstance(qname, str) or qname is None:
            self.__qname = qname
            self.qname_obj = None
        else:
            self.qname_obj = qname

    @property
    def qname(self):
        if self.qname_obj:
            return f"{self.qname_obj.ns_prefix}:{self.qname_obj.name}"
        else:
            return self.__qname

    def test_facts(self, facts):
        """
        Takes a list of facts, and returns facts which fit the filter.

        :param facts:
        :return:
        """
        returned_facts = []
        for fact in facts:
            if fact.concept_obj == self.qname_obj:
                returned_facts.append(fact)

        return returned_facts


class FilterConceptPeriod(FilterConcept):

    """
    period-type:
    http://www.xbrl.org/specification/conceptfilters/rec-2009-06-22/conceptfilters-rec-2009-06-22.html#example-concept-period-type-filter
    """

    def __init__(self, period_type, xlink_label, id=None):
        super(FilterConceptPeriod, self).__init__(id=id, xlink_label=xlink_label)

        self.period_type = period_type

    def test_facts(self, facts):
        """
        Takes a list of facts, and returns facts which fit the filter.

        :param facts:
        :return:
        """
        returned_facts = []
        for fact in facts:
            if fact.concept_obj.periodType == self.period_type:
                returned_facts.append(fact)

        return returned_facts


class FilterConceptBalance(FilterConcept):

    """
    balance:
    http://www.xbrl.org/specification/conceptfilters/rec-2009-06-22/conceptfilters-rec-2009-06-22.html#example-concept-balance-filter
    """

    def __init__(self, balance_type, xlink_label, id=None):
        super(FilterConceptBalance, self).__init__(id=id, xlink_label=xlink_label)

        self.balance_type = balance_type

    def test_facts(self, facts):
        """
        Takes a list of facts, and returns facts which fit the filter.

        :param facts:
        :return:
        """
        returned_facts = []
        for fact in facts:
            if hasattr(fact.concept_obj, "balance"):
                if fact.concept_obj.balance == self.balance_type:
                    returned_facts.append(fact)

        return returned_facts


class FilterConceptAttribute(FilterConcept):
    """
    custom-attribute:
    http://www.xbrl.org/specification/conceptfilters/rec-2009-06-22/conceptfilters-rec-2009-06-22.html#example-concept-custom-attribute-filter
    """

    def __init__(self, qname, qname_expression, value, xlink_label, id=None):
        super(FilterConceptAttribute, self).__init__(id=id, xlink_label=xlink_label)

        self.qname_expression = qname_expression
        self.value = value

        if isinstance(qname, str):
            self.__qname = qname
            self.qname_obj = None

        else:
            self.qname_obj = qname

    @property
    def qname(self):
        if self.qname_obj:
            return f"{self.qname_obj.ns_prefix}:{self.qname_obj.name}"
        else:
            return self.__qname


class FilterConceptDatatype(FilterConcept):
    """
    data-type:
    http://www.xbrl.org/specification/conceptfilters/rec-2009-06-22/conceptfilters-rec-2009-06-22.html#example-concept-data-type-filter
    """

    def __init__(
        self, strict, xlink_label, type_qname=None, type_qname_expression=None, id=None
    ):
        super(FilterConceptDatatype, self).__init__(id=id, xlink_label=xlink_label)

        self.strict = strict
        self.type_qname_expression = type_qname_expression

        if not type_qname and not type_qname_expression:
            logger.warning("No qname or qname expression")

        if isinstance(type_qname, str):
            self.__qname = type_qname
            self.qname_obj = None

        else:
            self.qname_obj = type_qname

    @property
    def type_qname(self):
        if self.qname_obj:
            return f"{self.qname_obj.ns_prefix}:{self.qname_obj.name}"
        else:
            return self.__qname


class FilterConceptSubstitutionGroup(FilterConcept):
    """
    substitution-group:
    http://www.xbrl.org/specification/conceptfilters/rec-2009-06-22/conceptfilters-rec-2009-06-22.html#example-concept-substitution-group-filter
    """

    def __init__(
        self, strict, xlink_label, type_qname=None, type_qname_expression=None, id=None
    ):
        super(FilterConceptSubstitutionGroup, self).__init__(
            id=id, xlink_label=xlink_label
        )

        self.strict = strict
        self.type_qname_expression = type_qname_expression

        if isinstance(type_qname, str):
            self.__qname = type_qname
            self.qname_obj = None

        else:
            self.qname_obj = type_qname

    @property
    def type_qname(self):
        if self.qname_obj:
            return f"{self.qname_obj.ns_prefix}:{self.qname_obj.name}"
        else:
            return self.__qname
