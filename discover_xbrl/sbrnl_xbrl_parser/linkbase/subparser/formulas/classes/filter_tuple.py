import logging

from .filter import Filter

logger = logging.getLogger("parser.linkbase")


class FilterTupleParent(Filter):
    def __init__(self, id, xlink_label, parent):
        super().__init__(xlink_label=xlink_label, id=id)

        if isinstance(parent, str):
            self.__qname = parent
            self.parent_obj = None

        else:
            self.parent_obj = parent

    @property
    def parent(self):
        if self.parent_obj:
            return f"{self.parent.ns_prefix}:{self.parent.name}"
        else:
            return self.__qname
