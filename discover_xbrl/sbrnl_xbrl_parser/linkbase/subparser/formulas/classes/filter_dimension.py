import logging

from .filter import Filter

logger = logging.getLogger("parser.linkbase")

# todo: EK: The specs are not very clear on the information expected in these filters. I am basing this on
#  the non-normative schema and specification document, but there are probably some disparities.


class FilterDimensionExplicit(Filter):
    def __init__(
        self, id, xlink_label, qname_dimension, qname_member=None, qname_expression=None
    ):
        super().__init__(xlink_label=xlink_label, id=id)

        if not qname_dimension:
            logger.warning(f"Explicit Dimension filter does not have a dimension")

        self.qname_expression = qname_expression

        if isinstance(qname_dimension, str):
            self.__qname_dimension = qname_dimension
            self.qname_dimension_obj = None

        else:
            self.qname_dimension_obj = qname_dimension

        if isinstance(qname_member, str):
            self.__qname_member = qname_member
            self.qname_member_obj = None

        else:
            self.__qname_member = None
            self.qname_member_obj = qname_member

    @property
    def qname_dimension(self):
        if self.qname_dimension_obj:
            return (
                f"{self.qname_dimension_obj.ns_prefix}:{self.qname_dimension_obj.name}"
            )
        else:
            return self.__qname_dimension

    @property
    def qname_member(self):
        if self.qname_member_obj:
            return f"{self.qname_member_obj.ns_prefix}:{self.qname_member_obj.name}"
        else:
            return self.__qname_member

    def test_facts(self, facts):
        """
        Takes a list of facts, and returns facts which fit the filter.
        In this case, all facts that have a scenario that matches the dimension filter.

        :param facts:
        :return:
        """

        returned_facts = []
        for fact in facts:

            for scenario in fact.context.scenario_members:

                if scenario.dimension_obj == self.qname_dimension_obj:

                    # A filter on member is optional
                    if self.qname_member_obj:
                        if scenario.member_obj == self.qname_member_obj:
                            returned_facts.append(fact)
                            continue

                    else:
                        returned_facts.append(fact)
                        continue

        return returned_facts


class FilterDimensionTyped(Filter):
    def __init__(self, id, xlink_label, qname_dimension, test=None):
        super().__init__(xlink_label=xlink_label, id=id)

        self.test = test

        if qname_dimension is None:
            logger.warning(f"Typed Dimension filter {id} is None")

        if isinstance(qname_dimension, str):
            self.__qname = qname_dimension
            self.qname_dimension_obj = None

        else:
            self.qname_dimension_obj = qname_dimension
            self.__qname = None

    @property
    def qname_dimension(self):
        if self.qname_dimension_obj:
            return (
                f"{self.qname_dimension_obj.ns_prefix}:{self.qname_dimension_obj.name}"
            )
        else:
            return self.__qname

    def test_facts(self, facts):
        """
        Takes a list of facts, and returns facts which fit the filter.
        In this case, all facts that have a scenario that matches the dimension filter.

        :param facts:
        :return:
        """
        # return facts

        returned_facts = []
        for fact in facts:
            for scenario in fact.context.scenario_members:

                if scenario.dimension_obj == self.qname_dimension_obj:
                    returned_facts.append(fact)
                    continue

        return returned_facts
