from discover_xbrl.sbrnl_xbrl_parser.linkbase.classes import Resource


class Variable(Resource):
    def __init__(self, id, xlink_label, bind_as_sequence):

        super().__init__(xlink_label=xlink_label, id=id)
        self.bind_as_sequence = bind_as_sequence

    def __repr__(self):
        return self.xlink_label


class GeneralVariable(Variable):
    def __init__(self, id, xlink_label, bind_as_sequence, select):
        super(GeneralVariable, self).__init__(
            id=id, xlink_label=xlink_label, bind_as_sequence=bind_as_sequence
        )

        self.select = select


class FactVariable(Variable):
    def __init__(
        self,
        id: str,
        xlink_label: str,
        matches,
        bind_as_sequence,
        nils,
        fallback_value,
        filters,
    ):
        super(FactVariable, self).__init__(
            id=id, xlink_label=xlink_label, bind_as_sequence=bind_as_sequence
        )

        self.matches = matches
        self.nils = nils
        self.fallback_value = fallback_value
        self.filters = filters


class Parameter(Resource):
    def __init__(self, as_datatype, xlink_label, name, required, select, id=None):

        super().__init__(xlink_label=xlink_label, id=id)
        self.as_datatype = as_datatype
        self.name = name
        self.required = required
        self.select = select

    def __str__(self):
        return f"label: '{self.name}', select: '{self.select}'"

    def __eq__(self, other):
        return self.xlink_label == other.xlink_label

    def __hash__(self):
        return hash(self.xlink_label)


class VariableSet:
    def __init__(self, name, variables=None):
        self.name = name
        self.variables = variables if variables is not None else []

    def __repr__(self):
        return self.name
