import logging

from .filter import Filter

logger = logging.getLogger("parser.linkbase")


class FilterPeriodInstantDuration(Filter):
    def __init__(self, id, xlink_label, boundary, variable):
        super().__init__(xlink_label=xlink_label, id=id)

        if boundary not in ["start", "end"]:
            logger.warning(f"Period filter '{id}' does not have a valid boundary value")

        self.boundary = boundary
        self.variable = variable


class FilterPeriodStart(Filter):
    def __init__(self, id, xlink_label, date, time=None):
        super().__init__(xlink_label=xlink_label, id=id)

        self.date = date
        self.time = time


class FilterPeriodEnd(Filter):
    def __init__(self, id, xlink_label, date, time=None):
        super().__init__(xlink_label=xlink_label, id=id)

        self.date = date
        self.time = time


class FilterPeriodInstant(Filter):
    def __init__(self, id, xlink_label, date, time=None):
        super().__init__(xlink_label=xlink_label, id=id)

        self.date = date
        self.time = time


class FilterPeriodForever(Filter):
    def __init__(self, id, xlink_label):
        super().__init__(xlink_label=xlink_label, id=id)
