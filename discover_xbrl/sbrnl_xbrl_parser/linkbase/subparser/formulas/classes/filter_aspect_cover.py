import logging


from .filter import Filter

logger = logging.getLogger("parser.linkbase")


class FilterAspectCover(Filter):
    def __init__(
        self, id, xlink_label, aspect=None, include_qname=None, exclude_qname=None
    ):

        super().__init__(xlink_label=xlink_label, id=id)

        if not aspect or include_qname or exclude_qname:
            logger.warning(
                f"Aspect cover filter '{id}' has no values. At least one is required"
            )
        self.aspect = aspect

        if isinstance(include_qname, str):
            self.__include_qname = include_qname
            self.include_qname_obj = None

        else:
            self.include_qname_obj = include_qname

        if isinstance(include_qname, str):
            self.__exclude_qname = include_qname
            self.exclude_qname_obj = None

        else:
            self.exclude_qname_obj = include_qname

    @property
    def include_qname(self):
        if self.include_qname_obj:
            return f"{self.include_qname_obj.ns_prefix}:{self.include_qname_obj.name}"
        else:
            return self.__include_qname

    @property
    def exclude_qname(self):
        if self.exclude_qname_obj:
            return f"{self.include_qname_obj.ns_prefix}:{self.include_qname_obj.name}"
        else:
            return self.__exclude_qname
