import logging

from .classes.variables import FactVariable, GeneralVariable, Parameter
from .classes.filter_aspect_cover import FilterAspectCover
from .classes.filter_boolean import FilterBooleanAnd, FilterBooleanOr
from .classes.filter_concept import (
    FilterConceptName,
    FilterConceptAttribute,
    FilterConceptBalance,
    FilterConceptPeriod,
    FilterConceptSubstitutionGroup,
    FilterConceptDatatype,
)
from .classes.filter_concept_relation import FilterConceptRelation
from .classes.filter_dimension import FilterDimensionExplicit, FilterDimensionTyped
from .classes.filter_period import (
    FilterPeriodInstantDuration,
    FilterPeriodStart,
    FilterPeriodEnd,
    FilterPeriodInstant,
    FilterPeriodForever,
)
from .classes.filter_tuple import FilterTupleParent

logger = logging.getLogger("parser.linkbase")


def get_variable_filter(elem, linkrole, linkbase=None):

    """
    Gets the specific filter

    :param elem:
    :return:
    """

    # Aspect Cover Filters
    # http://www.xbrl.org/specification/aspectcoverfilters/rec-2011-10-24/aspectcoverfilters-rec-2011-10-24.html
    if elem.tag == "{http://xbrl.org/2010/filter/aspect-cover}aspectCover":
        aspect = elem.find("{http://xbrl.org/2010/filter/aspect-cover}aspect")
        return FilterAspectCover(
            id=elem.get("id"),
            xlink_label=elem.get("{http://www.w3.org/1999/xlink}label"),
            aspect=aspect.text if aspect is not None else None,
            include_qname=get_qname(
                elem=elem,
                type="dimension",
                namespace="{http://xbrl.org/2010/filter/aspect-cover}",
                linkrole=linkrole,
            ),
            exclude_qname=get_qname(
                elem=elem,
                type="excludeDimension",
                namespace="{http://xbrl.org/2010/filter/aspect-cover}",
                linkrole=linkrole,
            ),
        )

    # Boolean Filters
    # http://www.xbrl.org/specification/booleanfilters/rec-2009-06-22/booleanfilters-rec-2009-06-22.html

    elif elem.tag == "{http://xbrl.org/2008/filter/boolean}orFilter":
        # Get all elements that are referenced by this filter
        subfilters = get_filters(linkbase, linkrole, elem.get("id"))

        return FilterBooleanOr(
            id=elem.get("id"),
            xlink_label=elem.get("{http://www.w3.org/1999/xlink}label"),
            subfilters=subfilters,
        )

    elif elem.tag == "{http://xbrl.org/2008/filter/boolean}andFilter":
        subfilters = get_filters(linkbase, linkrole, elem.get("id"))

        return FilterBooleanAnd(
            id=elem.get("id"),
            xlink_label=elem.get("{http://www.w3.org/1999/xlink}label"),
            subfilters=subfilters,
        )

    # Concept Filters
    # http://www.xbrl.org/specification/conceptfilters/rec-2009-06-22/conceptfilters-rec-2009-06-22.html
    #
    elif elem.tag == "{http://xbrl.org/2008/filter/concept}conceptName":

        return FilterConceptName(
            id=elem.get("id"),
            xlink_label=elem.get("{http://www.w3.org/1999/xlink}label"),
            qname=get_qname(
                elem=elem,
                type="concept",
                namespace="{http://xbrl.org/2008/filter/concept}",
                linkrole=linkrole,
            ),
            qname_expression=get_qname_expressions(
                elem=elem,
                type="concept",
                namespace="{http://xbrl.org/2008/filter/concept}",
            ),
        )

    elif elem.tag == "{http://xbrl.org/2008/filter/concept}conceptPeriodType":

        return FilterConceptPeriod(
            id=elem.get("id"),
            xlink_label=elem.get("{http://www.w3.org/1999/xlink}label"),
            period_type=elem.get("periodType"),
        )

    elif elem.tag == "{http://xbrl.org/2008/filter/concept}conceptBalance":

        return FilterConceptBalance(
            id=elem.get("id"),
            xlink_label=elem.get("{http://www.w3.org/1999/xlink}label"),
            balance_type=elem.get("balance"),
        )

    elif elem.tag == "{http://xbrl.org/2008/filter/concept}conceptCustomAttribute":
        return FilterConceptAttribute(
            id=elem.get("id"),
            xlink_label=elem.get("{http://www.w3.org/1999/xlink}label"),
            qname=get_qname(
                elem=elem,
                type="attribute",
                namespace="{http://xbrl.org/2008/filter/concept}",
                linkrole=linkrole,
            ),
            qname_expression=get_qname_expressions(
                elem=elem,
                type="attribute",
                namespace="{http://xbrl.org/2008/filter/concept}",
            ),
        )

    elif elem.tag == "{http://xbrl.org/2008/filter/concept}conceptDataType":

        return FilterConceptDatatype(
            id=elem.get("id"),
            xlink_label=elem.get("{http://www.w3.org/1999/xlink}label"),
            strict=elem.get("strict"),
            type_qname=get_qname(
                elem=elem,
                type="type",
                namespace="{http://xbrl.org/2008/filter/concept}",
                linkrole=linkrole,
            ),
            type_qname_expression=get_qname_expressions(
                elem=elem,
                type="type",
                namespace="{http://xbrl.org/2008/filter/concept}",
            ),
        )

    elif elem.tag == "{http://xbrl.org/2008/filter/concept}conceptSubstitutionGroup":

        return FilterConceptSubstitutionGroup(
            id=elem.get("id"),
            xlink_label=elem.get("{http://www.w3.org/1999/xlink}label"),
            strict=elem.get("strict"),
            type_qname=get_qname(
                elem=elem,
                type="substitutionGroup",
                namespace="{http://xbrl.org/2008/filter/concept}",
                linkrole=linkrole,
            ),
            type_qname_expression=get_qname_expressions(
                elem=elem,
                type="substitutionGroup",
                namespace="{http://xbrl.org/2008/filter/concept}",
            ),
        )

    # Concept Relation Filters
    # http://www.xbrl.org/specification/conceptrelationfilters/rec-2011-10-24/conceptrelationfilters-rec-2011-10-24.html
    #

    elif elem.tag == "{http://xbrl.org/2010/filter/concept-relation}conceptRelation":

        linkrole_str = get_xml_child_text(
            elem=elem,
            type="linkrole",
            namespace="{http://xbrl.org/2010/filter/concept-relation}",
        )
        if linkrole.roleURI == linkrole_str:
            crf_linkrole = linkrole
        else:
            crf_linkrole = linkrole_str

        return FilterConceptRelation(
            id=elem.get("id"),
            xlink_label=elem.get("{http://www.w3.org/1999/xlink}label"),
            qname=get_xml_child_text(
                elem=elem,
                type="qname",
                namespace="{http://xbrl.org/2010/filter/concept-relation}",
            ),
            linkrole=crf_linkrole,
            arcrole=get_xml_child_text(
                elem=elem,
                type="arcrole",
                namespace="{http://xbrl.org/2010/filter/concept-relation}",
            ),
            axis=get_xml_child_text(
                elem=elem,
                type="axis",
                namespace="{http://xbrl.org/2010/filter/concept-relation}",
            ),
        )

    # Dimension Filters
    # http://www.xbrl.org/specification/dimensionfilters/rec-2009-06-22/dimensionfilters-rec-2009-06-22+corrected-errata-2011-03-10.html
    #

    elif elem.tag == "{http://xbrl.org/2008/filter/dimension}explicitDimension":

        return FilterDimensionExplicit(
            id=elem.get("id"),
            xlink_label=elem.get("{http://www.w3.org/1999/xlink}label"),
            qname_dimension=get_qname(
                elem=elem,
                type="dimension",
                namespace="{http://xbrl.org/2008/filter/dimension}",
                linkrole=linkrole,
            ),
            qname_member=get_qname(
                elem=elem,
                type="member",
                namespace="{http://xbrl.org/2008/filter/dimension}",
                linkrole=linkrole,
            ),
            qname_expression=get_qname_expressions(
                elem=elem,
                type="attribute",
                namespace="{http://xbrl.org/2008/filter/dimension}",
            ),
        )

    elif elem.tag == "{http://xbrl.org/2008/filter/dimension}typedDimension":

        return FilterDimensionTyped(
            id=elem.get("id"),
            xlink_label=elem.get("{http://www.w3.org/1999/xlink}label"),
            qname_dimension=get_qname(
                elem=elem,
                type="dimension",
                namespace="{http://xbrl.org/2008/filter/dimension}",
                linkrole=linkrole,
            ),
            test=get_qname(
                elem=elem,
                type="test",
                namespace="{http://xbrl.org/2008/filter/dimension}",
                linkrole=linkrole,
            ),
        )

    # Entity Filters
    # http://www.xbrl.org/specification/entityfilters/rec-2009-06-22/entityfilters-rec-2009-06-22.html
    #

    # General Filters
    # http://www.xbrl.org/Specification/generalFilters/REC-2009-06-22/generalFilters-REC-2009-06-22.html
    #

    # Implicit Filters
    # http://www.xbrl.org/specification/implicitfilters/rec-2009-06-22/implicitfilters-rec-2009-06-22.html
    #

    # Period Filters
    # http://www.xbrl.org/specification/periodfilters/rec-2009-06-22/periodfilters-rec-2009-06-22.html
    #
    elif elem.tag == "{http://xbrl.org/2008/filter/period}period":
        # Not implemented yet, also not used at the moment.
        pass  # http://www.xbrl.org/specification/periodfilters/rec-2009-06-22/periodfilters-rec-2009-06-22.html#sec-period-filter

    elif elem.tag == "{http://xbrl.org/2008/filter/period}instantDuration":
        var_arc_name = elem.get("variable")

        for assertion in linkrole.resources["assertions"]:
            # Most likely, the variable is actually a variableSet that belongs to an assertion.
            varset = [
                varset
                for varset in assertion.variableSet
                if varset.name == var_arc_name
            ]
            if len(varset) > 0:
                variable = varset[0]

        if varset is None:
            # Could also be a direct link to a variable.
            vars = [
                var
                for var in linkrole.resources["variable"]
                if var.xlink_label == var_arc_name
            ]
            if len(variable) > 0:
                variable = vars[0]
            else:
                print(f"Could not find variable for var arc: '{var_arc_name}'")
                variable = None

        return FilterPeriodInstantDuration(
            id=elem.get("id"),
            xlink_label=elem.get("{http://www.w3.org/1999/xlink}label"),
            boundary=elem.get("boundary"),
            variable=variable,
        )

    elif elem.tag == "{http://xbrl.org/2008/filter/period}periodEnd":
        return FilterPeriodEnd(
            id=elem.get("id"),
            xlink_label=elem.get("{http://www.w3.org/1999/xlink}label"),
            date=elem.get("date"),
            time=elem.get("time"),
        )

    elif elem.tag == "{http://xbrl.org/2008/filter/period}periodStart":
        return FilterPeriodStart(
            id=elem.get("id"),
            xlink_label=elem.get("{http://www.w3.org/1999/xlink}label"),
            date=elem.get("date"),
            time=elem.get("time"),
        )

    elif elem.tag == "{http://xbrl.org/2008/filter/period}periodInstant":
        return FilterPeriodInstant(
            id=elem.get("id"),
            xlink_label=elem.get("{http://www.w3.org/1999/xlink}label"),
            date=elem.get("date"),
            time=elem.get("time"),
        )
    elif elem.tag == "{http://xbrl.org/2008/filter/period}forever":
        return FilterPeriodForever(
            id=elem.get("id"),
            xlink_label=elem.get("{http://www.w3.org/1999/xlink}label"),
        )

    # Relative Filters
    # http://www.xbrl.org/specification/relativefilters/rec-2009-06-22/relativefilters-rec-2009-06-22.html
    #

    # Segment Scenario Filters
    # http://www.xbrl.org/specification/segmentscenariofilters/rec-2009-06-22/segmentscenariofilters-rec-2009-06-22.html
    #

    # Tuple Filters
    # http://www.xbrl.org/specification/tuplefilters/rec-2009-06-22/tuplefilters-rec-2009-06-22.html
    #

    elif elem.tag == "{http://xbrl.org/2008/filter/tuple}parentFilter":
        return FilterTupleParent(
            id=elem.get("id"),
            xlink_label=elem.get("{http://www.w3.org/1999/xlink}label"),
            parent=get_qname(
                elem=elem,
                type="parent",
                namespace="{http://xbrl.org/2008/filter/tuple}",
                linkrole=linkrole,
            ),
        )
    elif elem.tag in [
        "{http://xbrl.org/2008/variable}factVariable",
        "{http://xbrl.org/2008/assertion/value}valueAssertion",
    ]:
        # We are finding 'self' if id and label are the same
        return None

    # Unit Filters
    # http://www.xbrl.org/specification/unitfilters/rec-2009-06-22/unitfilters-rec-2009-06-22.html
    #

    # Value Filters
    # http://www.xbrl.org/Specification/valueFilters/REC-2009-06-22/valueFilters-REC-2009-06-22.html
    #

    else:
        print(f"get_variable_filter: unknown {elem.tag}")


def get_filters(linkbase, linkrole, variable_label):
    """
    Attempt to get filters for given variable_label within the given linkbase file.

    :param linkbase:
    :param linkrole:
    :param variable_label:
    :return:
    """
    filters = []

    for variableFilterArc in linkbase.findall(
        f".//{{http://xbrl.org/2008/variable}}variableFilterArc[@{{http://www.w3.org/1999/xlink}}from='{variable_label}']",
        namespaces=linkbase.nsmap,
    ):
        to_string = variableFilterArc.get("{http://www.w3.org/1999/xlink}to")

        if to_string:

            # Find the filter element
            from_xml_element = linkbase.find(
                f".//*[@{{http://www.w3.org/1999/xlink}}label='{to_string}']"
            )

            if from_xml_element is not None:

                filter = get_variable_filter(
                    linkbase=linkbase, elem=from_xml_element, linkrole=linkrole
                )
                if filter is not None:
                    filter.arc_complement = variableFilterArc.get("complement")
                    filter.arc_cover = variableFilterArc.get("cover")
                    filter.arc_order = variableFilterArc.get("order")
                    filter.arc_priority = variableFilterArc.get("priority")

                    filters.append(filter)

            else:
                logger.warning(f"Could not find filter: {to_string}")

        else:
            logger.warning(f"Arc has no 'from' attribute (filter): {variable_label}")
    return filters


def get_filter_directly(linkbase, filter_label, linkrole):
    """
    Instead of first getting a variable, get the filter directly.

    :param linkbase:
    :param filter_label:
    :return:
    """

    xml_filter = linkbase.find(
        f".//*[@{{http://www.w3.org/1999/xlink}}label='{filter_label}']"
    )

    # if xml_filter.tag == "{http://xbrl.org/2008/filter/dimension}explicitDimension":
    return get_variable_filter(linkbase=linkbase, elem=xml_filter, linkrole=linkrole)


def get_xml_child_text(elem, type, namespace):
    # children = []

    for child_xml_elem in elem.findall(f"{namespace}{type}"):
        return child_xml_elem.text
        # children.append(child_xml_elem.text)
    # return children


def get_qname_object(qname, linkrole):

    if linkrole.parent_dts:

        for concept in linkrole.parent_dts.concepts.values():
            if hasattr(concept, "ns_prefix"):
                if f"{concept.ns_prefix}:{concept.name}" == qname:
                    return concept

    return None


def get_qname(elem, type, namespace, linkrole):
    # First get the Qname and/or Qname expression

    for qname_xml_elem in elem.findall(f"{namespace}{type}/{namespace}qname"):

        qname_element = get_qname_object(qname=qname_xml_elem.text, linkrole=linkrole)
        if qname_element is not None:

            # if a qname element is found, save this into the list
            return qname_element

        else:
            # Otherwise, return the string
            logger.debug(
                f"Element which QName refers to is not found.: {qname_xml_elem.text}"
            )
            return qname_xml_elem.text

    return None


def get_qname_expressions(elem, type, namespace):
    qname_expressions = []
    for qname_expression_xml_elem in elem.findall(
        f"{namespace}{type}/{namespace}qnameExpression"
    ):
        qname_expressions.append(qname_expression_xml_elem.text)

    if len(qname_expressions) == 0:
        return None  # Return none if the list is empty
    if len(qname_expressions) == 1:
        return qname_expressions[
            0
        ]  # Return only the Concept or string if only 1 entry is found
    else:
        return qname_expressions  # Return list if more than one qname is found
