from lxml import etree
import logging

from ..classes import Label, Reference, RGS_datapoint, Message, Severity
from .formulas.classes.assertion import ValueAssertion, ExistenceAssertion

logger = logging.getLogger("parser.linkbase")


def discover_generic_link(dts, elem, linkrole):
    """
    Loops through all /link:linkbase/gen:link elements.
    """
    #
    # for elems in list:  # list with gen:link's

    # Get the linkrole that this gen:link is referring to.

    # if uri not in dts.roletypes.keys():
    #     logging.error(f"Linkrole {uri} is not known in DTS")
    # linkrole = dts.roletypes[uri]
    #

    if elem.tag == "{http://xbrl.org/2008/generic}link":
        pass

    elif elem.tag == "{http://www.xbrl.org/2003/linkbase}loc":
        pass  # Are already processed with locs in general (maybe this leads to issues with double id's?

    elif elem.tag == "http://www.xbrl.org/2003/arcrole/general-special":
        pass

    elif elem.tag == "{http://xbrl.org/2008/label}label":
        pass  # Done in its own process before this generic step

    elif elem.tag == "{http://xbrl.org/2008/reference}reference":
        elem_id = elem.attrib["id"]
        elem_label = elem.attrib["{http://www.w3.org/1999/xlink}label"]
        reference = Reference(
            id=elem_id,
            xlink_label=elem_label,
            role=elem.get("{http://www.w3.org/1999/xlink}role", None),
        )

        # Add reference information
        for subelem in elem:
            cleaned_tag = subelem.tag.replace(
                "{http://www.xbrl.org/2006/ref}", ""
            ).lower()
            if hasattr(reference, cleaned_tag):
                reference.__setattr__(cleaned_tag, subelem.text)
            else:
                logging.warning(
                    f"Reference {elem_label} has non standard content: {cleaned_tag}"
                )

        linkrole.add_reference(reference=reference)

    elif elem.tag == "{http://xbrl.org/2010/message}message":
        pass  # done earlier

        """
         _________________ 
        /  ___| ___ \ ___ \
        \ `--.| |_/ / |_/ /
         `--. \ ___ \    / 
        /\__/ / |_/ / |\ \ 
        \____/\____/\_| \_|                               
        """
    elif (
        elem.tag
        == "{http://www.nltaxonomie.nl/2011/xbrl/xbrl-syntax-extension}linkroleOrder"
    ):
        """
        <sbr:linkroleOrder
             id=""
             xlink:label=""
             xlink:role="http://www.nltaxonomie.nl/2011/role/linkrole-info"
             xlink:type="resource">
                 1.0
             </sbr:linkroleOrder>
        """
        linkrole_label = elem.get("{http://www.w3.org/1999/xlink}label")
        dts.linkrole_order[float(elem.text)] = linkrole_label

        # DEPRECATED: DONE IN SEPARATE TABLE PARSER
        #     '''
        #       _____     _     _
        #      |_   _|   | |   | |
        #        | | __ _| |__ | | ___
        #        | |/ _` | '_ \| |/ _ \
        #        | | (_| | |_) | |  __/
        #        \_/\__,_|_.__/|_|\___|
        #      '''
        # elif elem.tag in [
        #     '{http://xbrl.org/2014/table}breakdown',
        #                   '{http://xbrl.org/2014/table}conceptRelationshipNode',
        #                   '{http://xbrl.org/2014/table}ruleNode',
        #                   '{http://xbrl.org/2014/table}dimensionRelationshipNode',
        #                    '{http://xbrl.org/2014/table}table'
        #                   ]:
        #     linkrole.elems.append(elem)

        """
        ______ _ _ _            
        |  ___(_) | |           
        | |_   _| | |_ ___ _ __ 
        |  _| | | | __/ _ \ '__|
        | |   | | | ||  __/ |   
        \_|   |_|_|\__\___|_|                                         
        """
    elif elem.tag in [
        "{http://xbrl.org/2010/filter/aspect-cover}aspectCover",
        "{http://xbrl.org/2008/filter/boolean}orFilter",
        "{http://xbrl.org/2008/filter/boolean}andFilter",
        "{http://xbrl.org/2008/filter/concept}conceptName",
        "{http://xbrl.org/2008/filter/concept}conceptPeriodType",
        "{http://xbrl.org/2008/filter/concept}conceptBalance",
        "{http://xbrl.org/2008/filter/concept}conceptCustomAttribute",
        "{http://xbrl.org/2008/filter/concept}conceptDataType",
        "{http://xbrl.org/2008/filter/concept}conceptSubstitutionGroup",
        "{http://xbrl.org/2010/filter/concept-relation}conceptRelation",
        "{http://xbrl.org/2008/filter/period}instantDuration",
        "{http://xbrl.org/2008/filter/period}periodEnd",
        "{http://xbrl.org/2008/filter/period}periodStart",
        "{http://xbrl.org/2008/filter/period}periodInstant",
        "{http://xbrl.org/2008/filter/dimension}explicitDimension",
        "{http://xbrl.org/2008/filter/dimension}typedDimension",
        "{http://xbrl.org/2008/filter/tuple}parentFilter",
    ]:
        pass  # done in filter subparser, through discovering the assertions

        """
          ___                    _   _             
         / _ \                  | | (_)            
        / /_\ \___ ___  ___ _ __| |_ _  ___  _ __  
        |  _  / __/ __|/ _ \ '__| __| |/ _ \| '_ \ 
        | | | \__ \__ \  __/ |  | |_| | (_) | | | |
        \_| |_/___/___/\___|_|   \__|_|\___/|_| |_|                                  
        """
    elif elem.tag == "{http://xbrl.org/2008/assertion/existence}existenceAssertion":

        linkrole.resources["assertions"].append(
            ExistenceAssertion(
                id=elem.get("id"),
                xlink_label=elem.get("{http://www.w3.org/1999/xlink}label"),
                aspect_model=elem.get("aspectModel"),
                implicit_filtering=elem.get("implicitFiltering"),
                test=elem.get("test"),
            )
        )

    elif elem.tag == "{http://xbrl.org/2008/assertion/value}valueAssertion":

        linkrole.resources["assertions"].append(
            ValueAssertion(
                id=elem.attrib["id"],
                aspect_model=elem.attrib["aspectModel"],
                implicit_filtering=elem.attrib["implicitFiltering"],
                xlink_label=elem.get("{http://www.w3.org/1999/xlink}label"),
                test=elem.get("test"),
            )
        )

    elif elem.tag in [
        "{http://xbrl.org/2016/assertion-severity}warning",
        "{http://xbrl.org/2016/assertion-severity}error",
        "{http://xbrl.org/2016/assertion-severity}ok",
    ]:
        linkrole.resources["severities"].append(
            Severity(
                xlink_label=elem.attrib["id"],
                id=elem.attrib["id"],
                severity=elem.attrib["id"],
                label=elem.get("{http://www.w3.org/1999/xlink}label"),
            )
        )

        """
         _   _            _       _     _      
        | | | |          (_)     | |   | |     
        | | | | __ _ _ __ _  __ _| |__ | | ___ 
        | | | |/ _` | '__| |/ _` | '_ \| |/ _ \
        \ \_/ / (_| | |  | | (_| | |_) | |  __/
         \___/ \__,_|_|  |_|\__,_|_.__/|_|\___|                                           
        """
    elif elem.tag in [
        "{http://xbrl.org/2008/variable}generalVariable",
        "{http://xbrl.org/2008/variable}variableFilterArc",
        "{http://xbrl.org/2008/variable}parameter",
        "{http://xbrl.org/2008/variable}factVariable",
        "{http://xbrl.org/2008/variable}precondition",
    ]:
        pass  # Done by Variable subparser

        """
         _   _       _ _     _       _   _             
        | | | |     | (_)   | |     | | (_)            
        | | | | __ _| |_  __| | __ _| |_ _  ___  _ __  
        | | | |/ _` | | |/ _` |/ _` | __| |/ _ \| '_ \ 
        \ \_/ / (_| | | | (_| | (_| | |_| | (_) | | | |
         \___/ \__,_|_|_|\__,_|\__,_|\__|_|\___/|_| |_|
        """
    elif elem.tag == "{http://xbrl.org/2008/validation}assertionSet":

        pass

    elif elem.tag in [
        "{http://xbrl.org/2014/table}table",
        "{http://xbrl.org/2014/table}tableBreakdownArc",
        "{http://xbrl.org/2014/table}breakdown",
    ]:
        pass  # handled in table discovery, Arc added in generic_arc.py

    elif elem.tag in [
        "{http://www.nltaxonomie.nl/rgs/2015/xbrl/rgs-syntax-extension}datapoint"
    ]:
        """
        DATAPOINT:
        <rgs:datapoint
            id="dp-rgs-i_BLasAclAllOvs-033"
            xlink:label="rgs-i_BLasAclAllOvs"
            xlink:role="http://www.nltaxonomie.nl/rgs/2015/role/from_dts"
            xlink:type="resource"
            >
            <rgs:entrypoint URI="http://www.nltaxonomie.nl/rgs/nt15/rgs/20210217/dictionary/rekeningen.xsd">
                <rgs:primary rgs:qname="rgs-i:BLasAclAllOvs"/>
            </rgs:entrypoint>
        </rgs:datapoint>

        or

        <rgs:datapoint id="dp-jenv-bw2-i_Equity_bopcm_fsts-002" xlink:label="jenv-bw2-i_Equity_bopcm_fsts" xlink:role="http://www.nltaxonomie.nl/rgs/2015/role/to_dts" xlink:type="resource">
            <rgs:entrypoint URI="http://www.nltaxonomie.nl/nt15/bzk/20210721/entrypoints/bzk-rpt-de-prospectieve-informatie-2021-toegelaten-instellingen-volkshuisvesting-verlicht-regime-geconsolideerd.xsd">
                <rgs:primary rgs:qname="jenv-bw2-i:Equity">
                    <rgs:explicitDimension rgs:qname="jenv-bw2-dim:BasisOfPreparationAxis" member="jenv-bw2-dm:CommercialMember" rgs:container="scenario"/>
                    <rgs:explicitDimension rgs:qname="jenv-bw2-dim:FinancialStatementsTypeAxis" member="jenv-bw2-dm:SeparateMember" rgs:container="scenario"/>
                </rgs:primary>
            </rgs:entrypoint>
        </rgs:datapoint>
        """
        ep = elem.find(
            "{http://www.nltaxonomie.nl/rgs/2015/xbrl/rgs-syntax-extension}entrypoint"
        )
        primary = ep.findall(
            "{http://www.nltaxonomie.nl/rgs/2015/xbrl/rgs-syntax-extension}primary"
        )
        if len(primary) > 1:
            print("ohoh")
        else:
            primary = primary[0]

        datapoint = RGS_datapoint(
            id=elem.get("id"),
            xlink_label=elem.get("{http://www.w3.org/1999/xlink}label"),
            role=elem.get("{http://www.w3.org/1999/xlink}role"),
            entrypoint=ep.get("URI"),
            primary_qname=primary.get(
                "{http://www.nltaxonomie.nl/rgs/2015/xbrl/rgs-syntax-extension}qname"
            ),
            explicit_dimensions=primary.findall(
                "{http://www.nltaxonomie.nl/rgs/2015/xbrl/rgs-syntax-extension}explicitDimension"
            ),
            typed_dimensions=primary.findall(
                "{http://www.nltaxonomie.nl/rgs/2015/xbrl/rgs-syntax-extension}typedDimension"
            ),
        )
        if datapoint.role in linkrole.resources.keys():
            linkrole.resources[datapoint.role].append(datapoint)
        else:
            linkrole.resources[datapoint.role] = [datapoint]

        pass  # RGS handled separately

    else:
        pass
        logging.error(f"tag '{elem.tag}' not known in generic link parser!")
