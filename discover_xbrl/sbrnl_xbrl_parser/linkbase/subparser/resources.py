import logging
import os
from pathlib import Path

from ..classes import Label
from ...parsetools.helpers import follow_loc

logger = logging.getLogger("parser.linkbase")

"""
Functions for discovering resources
"""


def discover_definitionlink(definitionlink_list, url, dts):
    """
    Find and yield schema references in linkbases.
    :param definitionlink_list:
    :param url:
    :param dts:
    :return:
    ADV:Currently unused?
    """

    for elem in definitionlink_list:

        xpath_locs = elem.xpath("link:loc", namespaces=dts.namespaces)
        for loc in xpath_locs:
            for key, val in loc.attrib.items():

                if "{http://www.w3.org/1999/xlink}href" in key:
                    # add all newly discovered files

                    href = val.split("#")[0]
                    id = val.split("#")[1]
                    href_local = Path(
                        os.path.normpath(os.path.join(url.parent.__str__(), href))
                    )
                    if "http" in val:
                        # the href refers to an external source. Validity should be checked (is this link allowed)

                        # Follow locators, add found elements to dts.concepts
                        print(f"==> {type(href)} {href} {id}")
                        ref_elem = follow_loc(href, id)
                        ref_id = ref_elem.get("id")
                        if ref_id not in dts.concepts:
                            logger.error(
                                f"Concept {ref_id} is not found when discovering definition link"
                            )

                        logger.warning(f"path {href} is external. Add to validation!")

                    elif os.path.exists(href_local):
                        print(f"Local definitionlink: {href_local}")
                        if href_local not in dts.schemas.keys():
                            dts.schemas[href_local] = None
                    else:
                        logger.error(f"Linkbase '{href_local}' is not a valid path")


def discover_labels(label_list):

    for elem in label_list:

        elem_id = elem.get("id")
        label = Label(
            id=elem_id,
            xlink_label=elem.attrib["{http://www.w3.org/1999/xlink}label"],
            linkRole=elem.attrib.get(
                "{http://www.w3.org/1999/xlink}role",
                "http://www.xbrl.org/2003/role/label",
            ),
            language=elem.attrib["{http://www.w3.org/XML/1998/namespace}lang"],
            text=elem.text,
        )

        yield label
