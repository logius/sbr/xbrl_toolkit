import logging

from .formulas.classes.variables import VariableSet
from .tables.classes import VariableArc
from ..classes import Label, GenericArc, Message

from ..functions import follow_local_loc, get_locator_from_arc_node
from ..subparser.formulas.variables import get_filter_directly


logger = logging.getLogger("parser.tag.generic")


def concept_to_concept(elem, dts, linkrole, from_elem, to_elem):
    from_concept = follow_local_loc(dts.concepts, dts.locs[from_elem])  # todo: DTS.LOCS
    to_concept = follow_local_loc(dts.concepts, dts.locs[to_elem])

    if from_concept is None:

        lr_from = [
            lr for lr in dts.roletypes.values() if lr.id == dts.locs[from_elem].href_id
        ]

        if len(lr_from) > 0:
            from_concept = lr_from[0]
        else:
            from_concept = None

    if from_concept == None:
        logger.warning("No 'from' concept found when parsing concept-to-concept Arc")

    if to_concept is None:
        # Could be a linkrole in a generic arc.
        lr_to = [
            lr for lr in dts.roletypes.values() if lr.id == dts.locs[to_elem].href_id
        ]
        if len(lr_to) > 0:
            to_concept = lr_to[0]
        else:
            to_concept = None

    if to_concept is None:
        logger.warning("No 'to' concept found when parsing concept-to-concept Arc")

    linkrole.arcs.append(
        GenericArc(
            tag=elem.tag,
            arcrole=elem.attrib.get("{http://www.w3.org/1999/xlink}arcrole"),
            order=elem.attrib.get("order", None),
            from_obj=from_concept,
            to_obj=to_concept,
        )
    )


def concept_to_label(elem, dts, linkrole, from_elem, to_elem):
    """
    Looks for label in different lists as generic arcs can be connected to different kind of elements.
    :param elem:
    :param dts:
    :param linkrole:
    :param from_elem:
    :param to_elem:
    :return:
    """

    #  from_concept = follow_local_loc(dts.concepts, dts.locs[from_elem])
    locator = get_locator_from_arc_node(elem)
    from_concept = follow_local_loc(dts.concepts, locator)

    to_label = linkrole.element_labels[to_elem]
    if from_concept:
        from_concept.labels[to_label.xlink_label] = to_label  # Add label to concept
        return GenericArc(
            tag=elem.tag,
            arcrole=elem.get("{http://www.w3.org/1999/xlink}arcrole"),
            from_obj=from_concept,
            to_obj=to_label,
        )
    else:
        """
        Try to find the element in other, slightly more random ways.
        feck?
        """
        loc_href = locator.href_id
        #
        # try roletypes by ID
        #
        for uri, roletype in dts.roletypes.items():

            if roletype.id == loc_href:
                # http://www.xbrl.org/2008/generic-label.xsd#standard-label
                from_concept = roletype
                from_concept.labels[
                    to_label.xlink_label
                ] = to_label  # Add the label to the roletype, by language
                return GenericArc(
                    tag=elem.tag,
                    arcrole=elem.get("{http://www.w3.org/1999/xlink}arcrole"),
                    from_obj=from_concept,
                    to_obj=to_label,
                )

        # try itemTypes and their enumerations
        for itemtype in dts.datatypes:
            # If the ID corresponds with the itemtype, the label belongs to the itemtype object.
            if itemtype.id == loc_href:

                itemtype.labels.append(
                    to_label
                )  # Add the label to the roletype, by language
                return GenericArc(
                    tag=elem.tag,
                    arcrole=elem.get("{http://www.w3.org/1999/xlink}arcrole"),
                    from_obj=itemtype,
                    to_obj=to_label,
                )

            # If the ID does not correspond with the tested itemtype,
            # it could still match an enumeration value of that itemtype.
            elif itemtype.enumerations:
                # If the ID corresponds with an enumeration of the itemtype, this is where the label belongs to.
                for enum in itemtype.enumerations:
                    if enum.id == loc_href:

                        enum.labels.append(
                            to_label
                        )  # Add the label to the enum, by language
                        return GenericArc(
                            tag=elem.tag,
                            arcrole=elem.get("{http://www.w3.org/1999/xlink}arcrole"),
                            from_obj=enum,
                            to_obj=to_label,
                        )

    if not from_concept:
        # We end up here with element-labels for elemets which did not get build yet,
        # tables are the primes suspect...
        # print(f"Fallen label: {from_elem} to {to_elem}")
        linkrole.arcs.append(
            GenericArc(
                tag=elem.tag,
                arcrole=elem.get("{http://www.w3.org/1999/xlink}arcrole"),
                order=elem.get("order"),
                from_elem=from_elem,
                to_elem=to_elem,
                to_obj=to_label,
            )
        )

        # linkrole.roleuri: http://www.xbrl.org/2008/generic-label.xsd#standard-label
        # uri: 'http://www.xbrl.org/2008/role/label'
        # logging.debug(
        #     f"did not find a 'from' concept for arc '{from_elem}' > '{to_elem}'"
        # )


def element_to_reference(elem, linkrole, dts, from_elem, to_elem):

    from_concept = follow_local_loc(dts.concepts, get_locator_from_arc_node(elem))
    to_reference = linkrole.get_reference(to_elem)

    # Add reference to concept
    from_concept.references.append(to_reference)

    linkrole.arcs.append(
        GenericArc(
            tag=elem.tag,
            arcrole=elem.get("{http://www.w3.org/1999/xlink}arcrole"),
            from_obj=from_concept,
            to_obj=to_reference,
        )
    )


def assertion_to_variable(elem, linkrole, dts, from_elem, to_elem):

    from_assertion = linkrole.get_assertion(from_elem)
    to_variable = linkrole.get_resource(xlink_label=to_elem, resource_type="variables")
    linkrole.arcs.append(
        GenericArc(
            tag=elem.tag,
            arcrole=elem.get("{http://www.w3.org/1999/xlink}arcrole"),
            from_obj=from_assertion,
            to_obj=to_variable,
        )
    )


def table_to_variable(elem, dts, linkrole, from_elem, to_elem):
    from_table = linkrole.get_table(from_elem)
    to_variable = linkrole.get_resource(xlink_label=to_elem, resource_type="variables")
    linkrole.arcs.append(
        GenericArc(
            tag=elem.tag,
            arcrole=elem.get("{http://www.w3.org/1999/xlink}arcrole"),
            from_obj=from_table,
            to_obj=to_variable,
        )
    )


def process_generic_arc(elem, linkrole, dts):
    """

    :param arc:
    :param ep:
    :return:
    """
    arcrole = elem.get("{http://www.w3.org/1999/xlink}arcrole")
    from_elem = elem.get("{http://www.w3.org/1999/xlink}from")
    to_elem = elem.get("{http://www.w3.org/1999/xlink}to")

    # Concept > Concept
    if arcrole in [
        "http://xbrl.org/int/dim/arcrole/domain-member",
        "http://www.xbrl.org/2003/arcrole/parent-child",
        "http://www.xbrl.org/2013/arcrole/parent-child",
        "http://www.nltaxonomie.nl/2017/arcrole/ConceptDisclosure",
    ]:

        concept_to_concept(elem, dts, linkrole, from_elem, to_elem)

    # Concept > Label
    elif arcrole in [
        "http://xbrl.org/arcrole/2008/element-label",
        "http://www.xbrl.org/2003/arcrole/concept-label",
    ]:
        arc = concept_to_label(elem, dts, linkrole, from_elem, to_elem)
        if arc:
            linkrole.arcs.append(arc)

    # Concept > linkroleOrder
    elif arcrole == "http://www.nltaxonomie.nl/2011/arcrole/linkrole-order":
        """
        <gen:arc
            xlink:arcrole="http://www.nltaxonomie.nl/2011/arcrole/linkrole-order"
            xlink:from="bzk-dvi-lr_AccountabilityComplianceWithSpecificLegalProvisionsHousingDvi_loc"
            xlink:to="bzk-dvi-lr_AccountabilityComplianceWithSpecificLegalProvisionsHousingDvi_rsc"
            xlink:type="arc"
        />

        FROM:
        <link:loc
            xlink:href="../dictionary/bzk-dvi-linkroles-housing.xsd#bzk-dvi-lr_AccountabilityComplianceWithSpecificLegalProvisionsHousingDvi"
            xlink:label="bzk-dvi-lr_AccountabilityComplianceWithSpecificLegalProvisionsHousingDvi_loc"
            xlink:type="locator"
        />

        TO:
        <sbr:linkroleOrder
            id="bzk-dvi-lr_AccountabilityComplianceWithSpecificLegalProvisionsHousingDvi_rsc"
            xlink:label="bzk-dvi-lr_AccountabilityComplianceWithSpecificLegalProvisionsHousingDvi_rsc"
            xlink:role="http://www.nltaxonomie.nl/2011/role/linkrole-info"
            xlink:type="resource">
                95.0
        </sbr:linkroleOrder>

        The TO element has already been gathered in the discover_generic_link() function.

        """
        from_elem_loc = dts.locs[from_elem]
        from_elem_obj = None
        for lr in dts.roletypes.values():
            if lr.id == from_elem_loc.href_id:
                from_elem_obj = lr

        for order, rsc_id in dts.linkrole_order.items():
            if rsc_id == to_elem:
                dts.linkrole_order[float(order)] = from_elem_obj

    # Concept > Reference
    elif arcrole == "http://xbrl.org/arcrole/2008/element-reference":
        element_to_reference(
            elem=elem, linkrole=linkrole, dts=dts, from_elem=from_elem, to_elem=to_elem
        )

    # Assertion > Variable
    elif arcrole == "http://xbrl.org/arcrole/2008/variable-set":
        assertion_to_variable(
            elem=elem, linkrole=linkrole, dts=dts, from_elem=from_elem, to_elem=to_elem
        )

    # Assertion > Severity
    elif arcrole == "http://xbrl.org/arcrole/2016/assertion-unsatisfied-severity":
        """
        todo: severities from http://www.xbrl.org/2016/severities.xml
        """
        from_assertion = linkrole.get_assertion(from_elem)
        if dts.locs.get(to_elem):
            to_resource = dts.get_resource(xlink_label=dts.locs[to_elem].href_id)
            linkrole.arcs.append(
                GenericArc(
                    tag=elem.tag,
                    from_obj=from_assertion,
                    to_obj=to_resource,
                    arcrole=arcrole,
                )
            )

    elif arcrole == "http://xbrl.org/arcrole/2008/boolean-filter":
        pass

    elif arcrole == "http://xbrl.org/arcrole/2008/variable-set":
        pass

    elif arcrole == "http://xbrl.org/arcrole/2008/variable-set-precondition":

        from_assertion = linkrole.get_assertion(from_elem)
        to_variable = linkrole.get_resource(
            xlink_label=to_elem, resource_type="variables"
        )

        linkrole.arcs.append(
            GenericArc(
                tag=elem.tag,
                arcrole=elem.get("{http://www.w3.org/1999/xlink}arcrole"),
                from_obj=from_assertion,
                to_obj=to_variable,
            )
        )
        """
        <variable:precondition 
            id="" 
            test="{xpath expression}" 
            xlink:label="" 
            xlink:type="resource"
        />
        """
        pass

    elif arcrole == "http://xbrl.org/arcrole/2008/variable-set-filter":
        # Done in variableArcs
        pass
        # from_assertion = linkrole.get_assertion(from_elem)
        # to_filter = linkrole.get_resource(to_elem, resource_type="variables")
        # linkrole.arcs.append(
        #     GenericArc(
        #         tag=elem.tag,
        #         arcrole=elem.get("{http://www.w3.org/1999/xlink}arcrole"),
        #         from_obj=from_assertion,
        #         to_obj=to_filter,
        #     )
        # )

    elif arcrole == "http://xbrl.org/arcrole/2008/assertion-set":
        pass

    elif arcrole == "http://xbrl.org/arcrole/2010/assertion-unsatisfied-message":
        """
        <gen:arc order="1"
        xlink:arcrole="http://xbrl.org/arcrole/2010/assertion-unsatisfied-message" x
        link:from="existenceAssertion_AccountabilityComplianceWithSpecificLegalProvisionsHousingDvi_PrtFST1Existence15_loc"
        xlink:to="AccountabilityComplianceWithSpecificLegalProvisionsHousingDvi_PrtFST1Existence15_assertion-unsatisfied-message_NL"
        xlink:type="arc"/>
        """

        # from_assertion = follow_local_loc(linkrole.assertions, dts.locs[from_elem])
        if dts.locs.get(from_elem):
            from_assertion = linkrole.get_assertion(dts.locs[from_elem].href_id)
        else:  # esrs: there is no locator!
            from_assertion = linkrole.get_assertion(from_elem)

        to_label = linkrole.element_labels[to_elem]
        linkrole.arcs.append(
            GenericArc(
                tag=elem.tag,
                arcrole=elem.get("{http://www.w3.org/1999/xlink}arcrole"),
                from_obj=from_assertion,
                to_obj=to_label,
            )
        )

    elif arcrole == "http://xbrl.org/arcrole/2014/table-parameter":
        table_to_variable(elem, dts, linkrole, from_elem, to_elem)

    elif arcrole == "http://xbrl.org/arcrole/2014/breakdown-tree":
        table_to_variable(elem, dts, linkrole, from_elem, to_elem)

        """SBR Specific"""

    elif arcrole == "http://www.nltaxonomie.nl/2017/arcrole/ConceptPolicy":
        concept_to_concept(elem, dts, linkrole, from_elem, to_elem)

    elif arcrole == "http://www.nltaxonomie.nl/2017/arcrole/ConceptMemberEquivalence":
        concept_to_concept(elem, dts, linkrole, from_elem, to_elem)

    elif (
        arcrole
        == "http://www.nltaxonomie.nl/2019/sbr-arcrole-2019/ParentChildSummation"
    ):
        concept_to_concept(elem, dts, linkrole, from_elem, to_elem)

    elif arcrole in ["{http://xbrl.org/2014/table}tableBreakdownArc"]:
        pass  # Arc is added in table discovery

    elif arcrole == "http://www.nltaxonomie.nl/2017/arcrole/DynamicLabel":
        # Objects for Dynamic Label will be discovered later.
        linkrole.arcs.append(
            GenericArc(
                tag=elem.tag,
                arcrole=elem.get("{http://www.w3.org/1999/xlink}arcrole"),
                from_elem=from_elem,
                to_elem=to_elem,
            )
        )
    else:
        linkrole.arcs.append(
            GenericArc(
                tag=elem.tag,
                arcrole=elem.get("{http://www.w3.org/1999/xlink}arcrole"),
                from_elem=from_elem,
                to_elem=to_elem,
            )
        )
        logging.error(f"arcrole: '{arcrole}' not yet known in generic arc parser")


def process_rgs_arc(elem, linkrole, dts):
    """
    Custom function to handle RGS arcs.

    :param elem:
    :param linkrole:
    :param dts:
    :return:
    """

    """
    MANY2ONE:
        <rgs:many2oneArc
            xlink:arcrole="http://www.nltaxonomie.nl/rgs/2015/arcrole/mapping"
            xlink:from="rgs-i_BLasAclAllOvs"
            xlink:to="jenv-bw2-i_LiabilitiesNoncurrent_bop_fst_ln_rjpon"
            xlink:type="arc"
            order="1"
            />

        ONE2ONE:
        <rgs:one2oneArc
            xlink:arcrole="http://www.nltaxonomie.nl/rgs/2015/arcrole/mapping"
            xlink:from="rgs-i_WWviWvmTbm"
            xlink:to="rj-i_ImpairmentPropertyPlantEquipmentReversalRecordedInIncomeStatement_bop_fst"
            xlink:type="arc"
            order="1"
            />
    """

    from_elem = elem.get("{http://www.w3.org/1999/xlink}from")
    to_elem = elem.get("{http://www.w3.org/1999/xlink}to")

    from_elem_obj = None
    for resource in linkrole.resources[
        "http://www.nltaxonomie.nl/rgs/2015/role/from_dts"
    ]:
        if resource.xlink_label == from_elem:
            from_elem_obj = resource

    to_elem_obj = None
    for resource in linkrole.resources[
        "http://www.nltaxonomie.nl/rgs/2015/role/to_dts"
    ]:
        if resource.xlink_label == to_elem:
            to_elem_obj = resource

    rgs_arc = GenericArc(
        tag=elem.tag,
        arcrole=elem.get("{http://www.w3.org/1999/xlink}arcrole"),
        order=elem.get("order"),
        from_elem=from_elem,
        from_obj=from_elem_obj,
        to_elem=to_elem,
        to_obj=to_elem_obj,
    )
    linkrole.arcs.append(rgs_arc)


def process_variable_arc(elem, linkrole, linkbase, dts=None):

    from_elem = elem.get("{http://www.w3.org/1999/xlink}from")
    to_elem = elem.get("{http://www.w3.org/1999/xlink}to")

    from_assertion = linkrole.get_assertion(from_elem)
    if not from_assertion:
        logger.warning(
            f"From assertion of variable arc '{from_elem}' -> '{to_elem} not found in DTS'"
        )

    if elem.tag == "{http://xbrl.org/2008/variable}variableArc":

        # Look for a variable or parameter
        to_variable = linkrole.get_resource(xlink_label=to_elem)

        if to_variable is not None:
            variable_arc_name = elem.get("name")
            # if (
            #     hasattr(to_variable, "variable_arc_names")
            #     and variable_arc_name not in to_variable.variable_arc_names
            # ):
            #     if len(to_variable.variable_arc_names) > 1:
            #         print("found a variable that is linked with multiple arcs!")
            #     to_variable.variable_arc_names.append(variable_arc_name)

            # Add variable to assertion
            varset = [
                varset
                for varset in from_assertion.variableSet
                if varset.name == variable_arc_name
            ]
            if not varset:
                from_assertion.variableSet.append(
                    VariableSet(
                        name=variable_arc_name,
                        variables=[
                            {
                                "order": elem.get("order"),
                                "priority": elem.get("priority"),
                                "variable": to_variable,
                            }
                        ],
                    )
                )

            else:
                # Add variable to set
                varset[0].variables.append(
                    {
                        "order": elem.get("order"),
                        "priority": elem.get("priority"),
                        "variable": to_variable,
                    }
                )

            # from_assertion.variableSet[variable_arc_name].append(
            #     {
            #         "order": elem.get("order"),
            #         "priority": elem.get("priority"),
            #         "variable": to_variable,
            #     }
            # )
        else:
            logger.warning(
                f"Variable: '{to_elem}' not found when processing variable arcs."
            )

        # Add assertion to linkrole
        if from_assertion not in linkrole.resources["assertions"]:
            linkrole.resources["assertions"].append(from_assertion)

        # Add arc to linkrole for future reference. Not sure if we still need it though, as it is implicitly
        # available through the linkrole.assertion now.

        linkrole.arcs.append(
            VariableArc(
                name=elem.get("name"),
                order=elem.get("order"),
                priority=elem.get("priority"),
                tag=elem.tag,
                arcrole=elem.get("{http://www.w3.org/1999/xlink}arcrole"),
                assertion=from_assertion,
                variable=to_variable,
            )
        )

    elif elem.tag == "{http://xbrl.org/2008/variable}variableSetFilterArc":

        filter = get_filter_directly(
            linkbase=linkbase, filter_label=to_elem, linkrole=linkrole
        )

        filter.arc_order = elem.get("order")
        filter.arc_priority = elem.get("priority")
        from_assertion.filterSet.append(filter)

        # Add arc
        linkrole.arcs.append(
            GenericArc(
                tag=elem.tag,
                arcrole=elem.get("{http://www.w3.org/1999/xlink}arcrole"),
                from_obj=from_assertion,
                to_obj=filter,
            )
        )


def process_labels(elem, linkrole):
    elem_arcrole = elem.get("{http://www.w3.org/1999/xlink}arcrole", None)
    if elem_arcrole:
        print("We currently don't do anything with an optional arcrole")
    # If a label is found, add it to the DTS if it isn't known
    if elem.attrib.get("id"):
        if elem.attrib["id"] not in linkrole.labels:
            elem_id = elem.attrib["id"]
            elem_label = elem.attrib["{http://www.w3.org/1999/xlink}label"]
            linkrole.element_labels[elem_label] = Label(
                id=elem_id,
                xlink_label=elem.attrib["{http://www.w3.org/1999/xlink}label"],
                linkRole=elem.attrib["{http://www.w3.org/1999/xlink}role"],
                language=elem.attrib["{http://www.w3.org/XML/1998/namespace}lang"],
                text=elem.text,
            )
    else:
        if elem.attrib["{http://www.w3.org/1999/xlink}label"] not in linkrole.labels:
            elem_label = elem.attrib["{http://www.w3.org/1999/xlink}label"]
            linkrole.element_labels[elem_label] = Label(
                id=None,
                xlink_label=elem.attrib["{http://www.w3.org/1999/xlink}label"],
                linkRole=elem.attrib["{http://www.w3.org/1999/xlink}role"],
                language=elem.attrib["{http://www.w3.org/XML/1998/namespace}lang"],
                text=elem.text,
            )


def process_messages(elem, linkrole):
    elem_label = elem.attrib["{http://www.w3.org/1999/xlink}label"]
    if elem_label not in linkrole.element_labels.keys():
        linkrole.element_labels[elem_label] = Message(
            id=elem.attrib["id"] if elem.attrib.get("id") else elem_label,
            xlink_label=elem_label,
            linkRole=elem.attrib["{http://www.w3.org/1999/xlink}role"],
            language=elem.attrib["{http://www.w3.org/XML/1998/namespace}lang"],
            text=elem.text,
        )
