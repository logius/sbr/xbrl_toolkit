import logging
from anytree import AnyNode

from .classes import Hypercube
from .hypercube_dimension import get_hypercube_dimension, get_domain_member_hierarchy
from .dimension_domain import get_explicit_domains
from .lineitems import get_lineitems
from .classes import Dimension

logger = logging.getLogger("parser.postprocessing")


def get_hc_type(roletype):
    # todo: this is a hacky way to do this, probably should be done differently as we do not expect both all and notAll

    hc_type = {}

    for arc in roletype.arcs:
        if arc.arcrole in [
            "http://xbrl.org/int/dim/arcrole/all",
            "http://xbrl.org/int/dim/arcrole/notAll",
        ]:
            if not hc_type:
                hc_type["type"] = arc.arcrole
                hc_type["closed"] = arc.closed
                hc_type["context_element"] = arc.context_element

            elif hc_type["type"] == arc.arcrole:

                print("got multiple types, but it is the same")
            else:
                print("got multiple types, and type differs!")

    return hc_type


def create_hypercubes(linkrole, roletypes):
    """
    Loop through linkroles, to process each according to their contents.

    :param roletypes:
    :return:
    """

    hypercube = get_hypercube(linkrole, roletypes)

    # If a hypercube has need found (HC type is know, etc) Try and add domains
    if hypercube:
        linkrole.hypercube = hypercube


def dimension_in_hypercube(arc, hypercube):
    """
    Takes an arc and hypercube as input.
    :param arc:
    :param hypercube:
    :return: Boolean, true if dimension exists within hypercube
    """

    for dimension in hypercube.dimensions:
        if dimension.concept == arc.to_concept_obj:
            return True
    return False


def get_hypercube(roletype, linkroles):
    """
    Builds a definition hierarchy, and dimensional  data structure.
    :param roletype:
    :return:
    """

    # Get hypercube type
    hc_type = get_hc_type(roletype)

    # If the hypercube has a type, it is valid and we can continue
    if len(hc_type) > 0:
        hypercube = Hypercube(
            hc_type=hc_type["type"],
            linkrole=roletype,
            closed=hc_type["closed"],
            context_element=hc_type["context_element"],
        )
        rootnode = AnyNode(id=hypercube.linkrole.id)
        # Lineitems are only available if the hypercube contains primaries
        get_lineitems(roletype, rootnode)
        lineitems = [child for child in rootnode.children]

        if len(lineitems) > 0:
            # Lineitems do not have to be available for a HC to be valid
            hypercube.lineItems = lineitems

        # Only try to find dimensions for definitionArcs, that are hypercube-dimension and point to type DimensionItem
        for arc in roletype.arcs:
            if (
                arc.arc_type == "{http://www.xbrl.org/2003/linkbase}definitionArc"
                and arc.arcrole == "http://xbrl.org/int/dim/arcrole/hypercube-dimension"
                and arc.to_concept_obj
                and arc.to_concept_obj.substitutionGroup == "xbrldt:dimensionItem"
            ):
                logger.debug(f"creating hypercube for: {roletype.definition}")
                # Only create a dimension if it does not exist within hypercube
                if not dimension_in_hypercube(arc, hypercube):
                    dim = get_hypercube_dimension(arc, linkroles, roletype)
                    logger.debug(f"Adding dimension: {dim} to Hypercube: {hypercube}")
                    hypercube.dimensions.append(dim)

        return hypercube
    else:
        #  En als we hier nou eens een 'default hypercube' retourneren?
        pass
    return None


def get_dimension_hierarchy(top, linkrole):
    # alleeen als dimension-domain de top is
    dimension = Dimension(concept=top.from_concept_obj, target_linkrole=top.target_role)
    get_domain_member_hierarchy(dimension, linkrole.arcs)
    if not dimension.concept.typedDomainRef:
        get_explicit_domains(dimension, [], linkrole)
    return dimension
