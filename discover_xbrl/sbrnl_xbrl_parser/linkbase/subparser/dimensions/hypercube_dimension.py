import logging

from anytree import AnyNode

from .classes import Dimension, Member
from .dimension_domain import get_explicit_domains
from .domain_member import get_typed_member

logger = logging.getLogger("parser.postprocessing")


def get_domain_member_hierarchy(dimension, arcs):
    """
    Bit of a workaround. Get the 'root' domain-member(s), and all children.
    The roots can be compared to domains, the children to normal members.
    """

    """
    Build dimension domains
    """

    # First get all the domain nodes
    for arc in arcs:
        if (
            arc.arc_type == "{http://www.xbrl.org/2003/linkbase}definitionArc"
            and arc.arcrole == "http://xbrl.org/int/dim/arcrole/dimension-domain"
            and arc.from_concept_obj == dimension.concept
        ):
            domain_node = Member(
                concept=arc.to_concept_obj,
                order=arc.order,
                preferred_label=arc.preferred_label,
            )
            domain_node.parent = dimension

            get_domain_member_children(arcs=arcs, parent_node=domain_node)

            if arc.target_role and len(arc.target_role.arcs):
                get_domain_member_children(
                    arcs=arc.target_role.arcs, parent_node=domain_node
                )

    return dimension


def get_domain_member_children(arcs, parent_node):
    # Get member nodes and add them to their respectable

    for arc in arcs:
        if arc.arc_type == "{http://www.xbrl.org/2003/linkbase}definitionArc":

            if arc.arcrole == "http://xbrl.org/int/dim/arcrole/domain-member":
                if arc.from_concept_obj == parent_node.concept:

                    child = Member(
                        concept=arc.to_concept_obj,
                        order=arc.order,
                        usable=arc.usable,
                        preferred_label=arc.preferred_label,
                    )
                    child.parent = parent_node
                    if arc.target_role is not None:
                        get_domain_member_children(
                            arcs=arc.target_role.arcs, parent_node=child
                        )
                    else:  # volgens mij zitten de children in dezelfde set arcs
                        get_domain_member_children(arcs=arcs, parent_node=child)


def get_hypercube_dimension(arc, linkroles, current_linkrole):
    """
    :return: list of dimensions
    Build hypercube dimension
    """
    dimension = Dimension(concept=arc.to_concept_obj, target_linkrole=arc.target_role)
    # Get the hierarchy of member-children
    if arc.target_role is not None:
        get_domain_member_hierarchy(dimension=dimension, arcs=arc.target_role.arcs)
    else:
        get_domain_member_hierarchy(dimension=dimension, arcs=current_linkrole.arcs)

    # Only get domains if the dimension is not a typed dimension.
    if not dimension.concept.typedDomainRef:

        get_explicit_domains(dimension, linkroles, current_linkrole)

    # Item is a typed
    else:
        # Get the item, make the dimension to be the parent.
        Member(concept=get_typed_member(dimension), order=arc.order, parent=dimension)

    return dimension
