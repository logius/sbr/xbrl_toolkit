from anytree import AnyNode


def get_lineitems(roletype, parent_node, is_top=True):
    """
        Build validation LineItems. This is a list of concepts that can be connected to a cube.
        Within the SBR taxonomy, this is a flat list as all elements point to 'sbr_dim-ValidationLineItems'.
        """
    for arc in roletype.arcs:
        if (
            arc.arc_type == "{http://www.xbrl.org/2003/linkbase}definitionArc"
            and arc.arcrole == "http://xbrl.org/int/dim/arcrole/domain-member"
        ):
            if arc.from_concept_obj and (
                (arc.from_concept_obj.name.endswith("LineItems") and is_top)
                or (
                    arc.from_concept_obj.id
                    == parent_node.id
                    # and not arc.from_concept_obj.is_abstract
                )
            ):
                order = arc.order if hasattr(arc, "order") else None
                lineitem = AnyNode(
                    parent=parent_node,
                    order=order,
                    id=arc.to_concept_obj.id,
                    concept=arc.to_concept_obj,
                    parent_concept_id=None if is_top else parent_node.id,
                )
                get_lineitems(roletype, parent_node=lineitem, is_top=False)
