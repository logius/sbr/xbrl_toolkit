import logging

from anytree import PreOrderIter, NodeMixin
from discover_xbrl.sbrnl_xbrl_parser.linkbase.classes import DefinitionArc

logger = logging.getLogger("parser.postprocessing")


class Hypercube:
    """
    A hypercube represents a set of dimensions.
    Hypercubes are abstract elements in the substitution group of hypercubeItem that participate in
    has-hypercube relations and hypercube-dimension relations.
    """

    def __init__(
        self,
        hc_type,
        linkrole,
        lineitems=None,
        context_element=None,
        closed=None,
        dimensions=None,
    ):

        # Set the type of Hypercube. This determines whether the items in lineitems are included or excluded
        if hc_type == "http://xbrl.org/int/dim/arcrole/all" or "all":
            self.hc_type = "all"
        elif hc_type == "http://xbrl.org/int/dim/arcrole/notAll" or "notAll":
            self.hc_type = "notAll"
        else:
            logger.error(f"Hypercube could not be created, unknown arcrole type")
            raise

        # Sets the linkrole URI that is referred to in this Hypercube
        self.linkrole = linkrole

        self.dimensions = dimensions if dimensions is not None else []

        # List of primary items (objects)
        self.lineItems = lineitems if lineitems else []

        if context_element is not None:
            # https://www.xbrl.org/specification/dimensions/rec-2012-01-25/dimensions-rec-2006-09-18+corrected-errata-2012-01-25-clean.html#sec-xbrldt-contextelement
            if context_element in ["segment", "scenario"]:
                self._context_element = context_element
            else:
                logger.warning(f"context element for HC {linkrole} not understood")
        else:
            self._context_element = None

        if closed is not None:
            # https://www.xbrl.org/specification/dimensions/rec-2012-01-25/dimensions-rec-2006-09-18+corrected-errata-2012-01-25-clean.html#sec-the-optional-xbrldt-closed-attribute
            if isinstance(closed, bool):
                self._closed = closed
            else:
                if closed in ["true", "True", "TRUE"]:
                    self._closed = True
                elif closed in ["false", "False", "FALSE"]:
                    self._closed = False
                else:
                    self._closed = None
                    logger.warning(
                        f"@closed for Dimension {self.concept.name} not understood"
                    )
        else:
            self._closed = None

    def __str__(self):

        return self.linkrole.definition

    def __eq__(self, other):
        return self.linkrole == other.linkrole

    @property
    def context_element(self):
        """
        Property that returns the private _context_element if this exists.
        In the future, this property could be deprecated in favor for interacting with _context_element directly.

        :return: self._context_element
        """
        if self._context_element is not None:
            return self._context_element
        else:
            return "scenario"

    @property
    def closed(self):
        """
        Property that returns the private _closed if this exists.
        In the future, this property could be deprecated in favor for interacting with _closed directly.

        :return: self._closed
        """
        if self._closed is not None:
            return self._closed
        else:
            return False


class Dimension(NodeMixin):
    """
    Wrapper class for hypercube dimensions

    Each of the different aspects by which a fact MAY be characterised. A dimension has only one effective domain.
    A typical example of a dimension is the "product" dimension that identifies for a concept (Sales) the domain
    consisting of the possible products that its fact can be expressed about.
    Dimensions are abstract elements in the substitution group of xbrldt:dimensionItem.

    """

    def __init__(self, concept, target_linkrole=None):
        self.concept = concept
        self.target_linkrole = target_linkrole
        self.domains = []  # Sets of members

    def __str__(self):
        # return self.concept.name
        if self.target_linkrole:
            return self.target_linkrole.definition
        else:
            return self.concept.name

    def __eq__(self, other):
        return (
            self.concept == other.concept
            and self.target_linkrole == other.target_linkrole
        )

    @property
    def members(self):
        """
        Combination set of self.domain_members and child domain members
        :return:
        """

        return [m for m in PreOrderIter(self) if not isinstance(m, Dimension)]

    def __str__(self):
        return f"Dimension: {self.concept.name}"


class Member(NodeMixin):
    def __init__(
        self,
        concept,
        parent=None,
        target_linkrole=None,
        order=None,
        usable=None,
        preferred_label=None,
    ):

        self.concept = concept
        self.order = order
        self.parent = parent

        self.target_linkrole = target_linkrole
        self.preferred_label = preferred_label

        if usable is not None:
            # https://www.xbrl.org/specification/dimensions/rec-2012-01-25/dimensions-rec-2006-09-18+corrected-errata-2012-01-25-clean.html#sec-the-optional-xbrldt-usable-attribute

            # If the instance is given as a boolean, use this directly.
            if isinstance(usable, bool):
                self.usable = usable
            else:
                if usable in ["true", "True", "TRUE"]:
                    self.usable = True
                elif usable in ["false", "False", "FALSE"]:
                    self.usable = False
                else:
                    self.usable = None
        else:
            self.usable = None

    @property
    def members(self):

        members = [m for m in PreOrderIter(self) if m is not self]
        return members

    def __str__(self):
        return f"DomainMember: {self.concept.name}"


class DimensionalArc(DefinitionArc):
    def __init__(
        self,
        tag,
        from_obj,
        to_obj,
        arcrole,
        attributes=None,
        context_element=None,
        closed=None,
        order=None,
        target_role=None,
        usable=None,
        preferred_label=None,
    ):

        # For legacy sake, process an empty attributes dict if attributes are not given.
        if attributes is None:
            attributes = {}

        self.arc_type = tag

        # Set to and from object to None, these attributes are filled in post discovery
        self.to_concept_obj = to_obj
        self.from_concept_obj = from_obj
        self.preferred_label = preferred_label

        if arcrole is None:
            self.arcrole = attributes["{http://www.w3.org/1999/xlink}arcrole"]
        else:
            self.arcrole = arcrole

        # self._from_concept = attributes["{http://www.w3.org/1999/xlink}from"]
        # self._to_concept = attributes["{http://www.w3.org/1999/xlink}to"]

        if self.arcrole in [
            "http://xbrl.org/int/dim/arcrole/all",
            "http://xbrl.org/int/dim/arcrole/notAll",
        ]:
            # Arc is an has-hypercube Arc
            if context_element is not None:
                # check if attributes have been passed in @attributes or as named attributes
                self.context_element = context_element
            elif "{http://xbrl.org/2005/xbrldt}contextElement" in attributes:
                self.context_element = attributes.get(
                    "{http://xbrl.org/2005/xbrldt}contextElement"
                )

            else:
                logger.error(
                    f"@context_element expected for has-hc arc: '{from_obj.id} -> {to_obj.id}'"
                )

            # @ closed is optional; may be none but must be a Boolean
            if closed is not None:
                # https://www.xbrl.org/specification/dimensions/rec-2012-01-25/dimensions-rec-2006-09-18+corrected-errata-2012-01-25-clean.html#sec-the-optional-xbrldt-closed-attribute
                if isinstance(closed, bool):
                    self.closed = closed
                else:
                    logger.error(
                        f"Attribute @closed for arc '{from_obj.id} -> {to_obj.id}' should be a Boolean"
                    )

            elif "{http://xbrl.org/2005/xbrldt}closed" in attributes:
                # Get it from attributes and change it into a Boolean
                closed = attributes.get("{http://xbrl.org/2005/xbrldt}closed")

                if closed in ["true", "True", "TRUE"]:
                    self.closed = True
                elif closed in ["false", "False", "FALSE"]:
                    self.closed = False
                else:
                    self.closed = None
                    logger.warning(
                        f"@closed for Dimensional arc '{from_obj.id} -> {to_obj.id}' not understood"
                    )
            else:
                self.closed = None

        if self.arcrole in [
            "http://xbrl.org/int/dim/arcrole/dimension-domain",
            "http://xbrl.org/int/dim/arcrole/domain-member",
        ]:
            """
            @Usable is optional on dimensional-domain and domain-member Arcs
            """
            if usable is not None:
                # https://www.xbrl.org/specification/dimensions/rec-2012-01-25/dimensions-rec-2006-09-18+corrected-errata-2012-01-25-clean.html#sec-the-optional-xbrldt-usable-attribute

                # If the instance is given as a boolean, use this directly.
                if isinstance(usable, bool):
                    self.usable = usable
                else:
                    logger.error(
                        f"Attribute @usable for arc '{from_obj.id} -> {to_obj.id}' should be a Boolean"
                    )

            elif "{http://xbrl.org/2005/xbrldt}usable" in attributes:
                usable = attributes.get("{http://xbrl.org/2005/xbrldt}usable")
                if usable in ["true", "True", "TRUE"]:
                    self.usable = True
                elif usable in ["false", "False", "FALSE"]:
                    self.usable = False
                else:
                    self.usable = None

                    logger.warning(
                        f"@usable for Dimensional arc '{from_obj.id} -> {to_obj.id}' not understood"
                    )

            else:
                # Default is true
                self.usable = True

        if self.arcrole in [
            "http://xbrl.org/int/dim/arcrole/dimension-domain",
            "http://xbrl.org/int/dim/arcrole/domain-member",
            "http://xbrl.org/int/dim/arcrole/hypercube-dimension",
            "http://xbrl.org/int/dim/arcrole/all",
            "http://xbrl.org/int/dim/arcrole/notAll",
        ]:
            if target_role is not None:

                self.target_role = target_role

            elif "{http://xbrl.org/2005/xbrldt}targetRole" in attributes:

                self.target_role = attributes.get(
                    "{http://xbrl.org/2005/xbrldt}targetRole"
                )
            else:
                self.target_role = None

            # Order is optional
            if order is not None:
                self.order = float(order)

            elif "order" in attributes:
                self.order = float(attributes["order"])

            else:
                self.order = None

        super().__init__(tag, arcrole=arcrole, from_obj=from_obj, to_obj=to_obj)
