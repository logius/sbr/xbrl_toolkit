import logging

from .classes import Member
from .domain_member import get_explicit_members

logger = logging.getLogger("parser.postprocessing")


def get_explicit_domains(dimension, roletypes, current_linkrole):
    """
    Get explicit domains.

    This function yields each discovered domain. Members are also added in a nested function.

    Also adds domain_members to given dimension if domainMemberItem are found.

    :param dimension:
    :param roletypes:
    :return:
    """

    domain_roletype = (
        dimension.target_linkrole if dimension.target_linkrole else current_linkrole
    )

    """
    Build dimension domains
    """
    if domain_roletype is not None:
        for arc in domain_roletype.arcs:
            if arc.arc_type == "{http://www.xbrl.org/2003/linkbase}definitionArc":

                # Match from object with dimension and substitutionGroup
                if (
                    arc.from_concept_obj == dimension.concept
                    and arc.to_concept_obj is not False
                    # and arc.to_concept_obj.substitutionGroup == "sbr:domainItem"
                ):

                    # Get the domain if it already exists
                    member = [
                        dm
                        for dm in dimension.domains
                        if dm.concept == arc.to_concept_obj
                    ]

                    # Create new domain or reuse existing
                    if not member:
                        member = Member(
                            concept=arc.to_concept_obj, target_linkrole=arc.target_role
                        )
                    else:
                        # Get rid of the tuple
                        member = member[0]

                    if hasattr(arc, "order"):
                        member.order = arc.order

                    # Get memberItems referenced by linkrole if this is an explicitMember
                    if member.target_linkrole is not None and hasattr(
                        member.target_linkrole, "roleURI"
                    ):
                        for roletype in roletypes:
                            if (
                                hasattr(roletype, "roleURI")
                                and roletype.roleURI == member.target_linkrole.roleURI
                            ):
                                member_roletype = roletype

                                # Get children. They will be appended to the anytree node hierarchy
                                get_explicit_members(
                                    member=member, member_roletype=member_roletype
                                )
                                break
