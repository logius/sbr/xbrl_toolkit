import logging

logger = logging.getLogger("parser.postprocessing")
from .classes import Member


def get_explicit_members(member, member_roletype):

    """
    Build dimension domains
    """
    members = []
    # Go through all arcs within the linkrole
    for arc in member_roletype.arcs:
        if arc.arc_type == "{http://www.xbrl.org/2003/linkbase}definitionArc":

            # only add members that have corresponding 'from' element
            if (
                arc.from_concept_obj == member.concept
                and arc.arcrole == "http://xbrl.org/int/dim/arcrole/domain-member"
            ):

                # Hypercube Dimensions are the root of our axis tree
                if (
                    arc.from_concept_obj == member.concept
                    and arc.to_concept_obj.substitutionGroup == "sbr:domainMemberItem"
                ):
                    if arc.to_concept_obj not in members:
                        members.append(
                            Member(
                                concept=arc.to_concept_obj,
                                usable=arc.usable,
                                order=arc.order,
                                parent=member,
                            )
                        )
                        _ = get_explicit_members(
                            member=members[-1], member_roletype=member_roletype
                        )

    return members


def get_typed_member(axis):
    try:
        member_object = axis.concept.typedDomainRef_obj
        return member_object
    except AttributeError:
        print(f"Has no object in the domain: {axis.concept.name}")
