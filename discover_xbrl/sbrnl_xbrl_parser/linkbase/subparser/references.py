import logging
import os
from pathlib import Path

from .dimensions.classes import DimensionalArc
from ..classes import Loc, Arc, Reference, PresentationArc, ReferenceArc
from ..functions import follow_local_loc
from ..functions import get_locator_from_arc_node
from ...parsetools.classes import WWWPath
from ...parsetools.helpers import clean_url

logger = logging.getLogger("parser.linkbase")


"""
References and locators
"""


def discover_linkbaseref(linkbase_list, url, dts):
    """
    Discover link:linkbase/link:roleRef/@xlink:href
    """
    for elem in linkbase_list:
        elem_href = elem.get("{http://www.w3.org/1999/xlink}href")
        if elem_href.startswith("http"):
            # add all newly discovered files
            # If the file is offline, find the directory used, otherwise use absolute path as URL string
            elem_href_path = clean_url(elem_href)
        elif isinstance(url, WWWPath):
            elem_href_path = clean_url(str(url.parent) + "/" + elem_href)
        else:
            if isinstance(url, Path):
                elem_href_path = clean_url(url=elem_href, abs_path=url.parent.__str__())
            else:
                elem_href_path = clean_url(url=elem_href)

        roleref = {"uri": elem.get("roleURI"), "path": elem_href_path}
        yield roleref


def discover_arcroleref(linkbase_list, url):
    """
    Discover link:linkbase/link:arcroleRef/@xlink:href
    """
    for elem in linkbase_list:
        elem_href = elem.get("{http://www.w3.org/1999/xlink}href")
        if "http" in elem_href:
            # add all newly discovered files
            # If the file is offline, find the directory used, otherwise use absolute path as URL string
            if "#" in elem_href:
                elem_href, elem_id = elem_href.split("#", 1)
            else:
                elem_id = None
            elem_href_path = WWWPath(elem_href)
        else:
            if isinstance(url, Path):
                elem_href_path = clean_url(url=elem_href, abs_path=url.parent.__str__())
                elem_id = None
            else:
                elem_href_path = clean_url(url=elem_href, abs_path=url.parent)
                elem_id = None

        arcroleref = {
            "uri": elem.get("arcroleURI"),
            "path": elem_href_path,
            "id": elem_id,
        }
        yield arcroleref


def get_reference_element(elems):
    for elem in elems:

        if elem.tag in [
            "{http://www.xbrl.org/2003/linkbase}reference",
            "{http://xbrl.org/2008/reference}reference",
        ]:
            elem_id = elem.get("id")
            elem_label = elem.get("{http://www.w3.org/1999/xlink}label")
            reference = Reference(
                id=elem_id,
                xlink_label=elem_label,
                role=elem.get("{http://www.w3.org/1999/xlink}role", None),
            )

            # Add reference information
            for subelem in elem:
                cleaned_tag = subelem.tag.replace(
                    "{http://www.xbrl.org/2006/ref}", ""
                ).lower()
                if hasattr(reference, cleaned_tag):
                    reference.__setattr__(cleaned_tag, subelem.text)
                else:
                    logging.debug(
                        f"Reference {elem_label} has non standard content: {cleaned_tag}"
                    )
            yield reference


def discover_locators(locator_list, url):
    """
    Takes a list of locator elements and yields every unique locator from a file

    :param locator_list:
    :param url:
    :param dts:
    :return:
    """
    local_files = []

    for loc in locator_list:
        loc_label = loc.get("{http://www.w3.org/1999/xlink}label")
        parent = loc.getparent()
        loc_href_file, loc_href_concept = loc.attrib[
            "{http://www.w3.org/1999/xlink}href"
        ].split("#", 1)

        # check if loc_ref_file is a local path
        if "http://" in loc_href_file or "https://" in loc_href_file:
            loc_href_file = WWWPath(loc_href_file)

        elif loc_href_file not in local_files:
            # Only add hrefs that have not been found in this file before. Path check is a lot more costly.
            if str(url.parent).startswith("http"):
                loc_href_local_file = WWWPath(f"{url.parent}/{loc_href_file}")
            else:
                joined_path = Path.joinpath(url.parent, loc_href_file)
                norm_path = os.path.normpath(joined_path)
                loc_href_local_file = Path(norm_path)
            loc_href_file = (
                loc_href_local_file
            )  # Set it to absolute so loc can be used later on as well.

        if loc_href_file not in local_files:
            yield loc_href_file

        local_files.append(loc_href_file)
        yield Loc(
            href_path=url,
            href_id=loc_href_concept,
            label=loc_label,
            type=loc.get("{http://www.w3.org/1999/xlink}type"),
        )


def discover_reference_arcs(arc_list, dts, references):

    for arc in [
        arc
        for arc in arc_list
        if arc.tag == "{http://www.xbrl.org/2003/linkbase}referenceArc"
    ]:

        from_elem = follow_local_loc(dts.concepts, get_locator_from_arc_node(arc))
        if from_elem is None:
            logging.warning(
                f"ReferenceArc 'from' not found for {arc.get('{http://www.w3.org/1999/xlink}from')} -> {arc.get('{http://www.w3.org/1999/xlink}to')}"
            )

        # Get the reference
        to_elem = [
            ref
            for ref in references
            if ref.xlink_label == arc.get("{http://www.w3.org/1999/xlink}to")
        ]
        if len(to_elem) == 1:
            to_elem = to_elem[0]
        elif len(to_elem) == 0:
            logging.warning(
                f"ReferenceArc 'to' not found for {arc.get('{http://www.w3.org/1999/xlink}from')} -> {arc.get('{http://www.w3.org/1999/xlink}to')}"
            )
            to_elem = None
        else:
            logging.debug(
                f"ReferenceArc has multiple 'to' elements for {arc.get('{http://www.w3.org/1999/xlink}from')} -> {arc.get('{http://www.w3.org/1999/xlink}to')}"
            )
            # todo: ga terug de xml (ancestors) in vind de juiste, niet random de eerste
            to_elem = to_elem[0]

        if to_elem is not None:
            arc_obj = ReferenceArc(
                tag=arc.tag,
                arcrole=arc.get("{http://www.w3.org/1999/xlink}arcrole"),
                from_obj=from_elem,
                to_obj=to_elem,
            )
            yield arc_obj

            # Add reference to Concept
            if from_elem is not None:
                from_elem.references.append(to_elem)


def discover_arcs(arc_list, dts):
    """
    Discover presentation and definition Arcs. At this point, an XML (arc) element is only saved as an Arc object.
    In later steps, the referenced objects are added to the Arc object.

    :param arc_list:  [XMLElements]
    :param dts: DTS() object
    :return: generator Arc's
    """
    # print('DTS Locs')
    # print(dts.locs)
    # print('DTS Concepts')
    # print(dts.concepts)
    for arc in arc_list:
        # print(etree.tostring(arc))
        from_elem = follow_local_loc(dts.concepts, get_locator_from_arc_node(arc))
        to_elem = follow_local_loc(
            dts.concepts, get_locator_from_arc_node(arc, to=True)
        )
        order = arc.get("order")

        if arc.tag == "{http://www.xbrl.org/2003/linkbase}presentationArc":
            arc_obj = PresentationArc(
                tag=arc.tag,
                arcrole=arc.get("{http://www.w3.org/1999/xlink}arcrole"),
                from_obj=from_elem,
                order=arc.get("order", None),
                to_obj=to_elem,
                preferred_label=arc.get("preferredLabel", None),
            )

        # Parse Dimensional Arcs separately as they contain specific attributes.
        elif arc.tag == "{http://www.xbrl.org/2003/linkbase}definitionArc":

            if arc.get("{http://www.w3.org/1999/xlink}arcrole") in [
                "http://xbrl.org/int/dim/arcrole/all",
                "http://xbrl.org/int/dim/arcrole/notAll",
                "http://xbrl.org/int/dim/arcrole/hypercube-dimension",
                "http://xbrl.org/int/dim/arcrole/dimension-domain",
                "http://xbrl.org/int/dim/arcrole/domain-member",
                "http://xbrl.org/int/dim/arcrole/dimension-default",
            ]:
                target_uri = arc.get("{http://xbrl.org/2005/xbrldt}targetRole")
                if target_uri is not None:
                    if target_uri in dts.roletypes.keys():
                        target_role = dts.roletypes[target_uri]
                    else:
                        logging.warning(
                            f"URI: '{target_uri}' referenced by {from_elem.id} -> {to_elem.id} not found in DTS"
                        )
                        target_role = target_uri
                else:
                    target_role = None

                arc_obj = DimensionalArc(
                    tag=arc.tag,
                    arcrole=arc.get("{http://www.w3.org/1999/xlink}arcrole"),
                    context_element=arc.get(
                        "{http://xbrl.org/2005/xbrldt}contextElement"
                    ),
                    target_role=target_role,
                    order=order,
                    from_obj=from_elem,
                    to_obj=to_elem,
                    preferred_label=arc.get(
                        "{http://xbrl.org/2013/preferred-label}preferredLabel", None
                    ),
                )
            else:

                arc_obj = Arc(
                    tag=arc.tag,
                    attributes=arc.attrib,
                    arcrole=arc.get("{http://www.w3.org/1999/xlink}arcrole"),
                    order=order,
                    from_obj=from_elem,
                    to_obj=to_elem,
                )

        yield arc_obj


def discover_label_arcs(arc_list, dts, labels):

    for arc in arc_list:
        # parent_elems = arc.getparent()

        from_elem_obj = follow_local_loc(dts.concepts, get_locator_from_arc_node(arc))
        if not from_elem_obj:
            _from = arc.get("{http://www.w3.org/1999/xlink}from")
            # to the logger
            continue

        to_elem = arc.get("{http://www.w3.org/1999/xlink}to")

        to_elem_objs = [label for label in labels if label.xlink_label == to_elem]
        for to_elem_obj in to_elem_objs:
            # Here, labels are connected to the concept object as well
            from_elem_obj.labels[f"{to_elem}|{to_elem_obj.role}"] = to_elem_obj

            arc_obj = Arc(
                tag=arc.tag,
                arcrole=arc.get("{http://www.w3.org/1999/xlink}arcrole"),
                attributes=arc.attrib,
                from_obj=from_elem_obj,
                to_obj=to_elem_obj,
            )
            yield arc_obj
