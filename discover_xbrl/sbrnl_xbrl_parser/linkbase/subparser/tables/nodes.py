import logging

from .classes import (
    RuleNode,
    ConceptRelationshipNode,
    AspectNode,
    DimensionRelationshipNode,
    Aspect,
    RuleSet,
)

from ..formulas.classes.formula import (
    FormulaTypedDimension,
    FormulaExplicitDimension,
    FormulaPeriod,
)
from ..formulas.variables import get_qname, get_qname_expressions, get_qname_object

logger = logging.getLogger("parser.tag.generic")


def get_definition_subtree_children(
    roletype, order, from_id=None, parent=None, dts=None
):
    """
    Recursively traverse presentationArcs.
    Hierarchies are build up leaving the Arcs in tact. The 'to' object of each arc is the object of that Node.
    Normalizing would make sense from a data model perspective, but in order to conduct analyses on the taxonomy
    it is better to leave the arc explicitly in there.


    :param roletype:
    :param parent:
    :return:
    """

    from_node = get_node(
        roletype, node_label=from_id, order=order, parent=parent, dts=dts
    )
    roletype.resources["table"].append(from_node)

    # These arcs should already be discovered in earlier in build_table()
    # for arc in [arc for arc in roletype.arcs if arc.arc_type == '{http://xbrl.org/2014/table}definitionNodeSubtreeArc']:
    for arc in [
        arc
        for arc in roletype.arcs
        if arc.arcrole == "http://xbrl.org/arcrole/2014/definition-node-subtree"
    ]:

        # if arc.from_concept == parent.arc.to_concept:
        if arc.from_concept == from_id:

            arc.from_concept_obj = from_node

            to_obj = get_definition_subtree_children(
                roletype,
                parent=from_node,
                from_id=arc.to_concept,
                order=arc.order,
                dts=dts,
            )
            arc.to_concept_obj = to_obj

            # to_node = get_node(roletype, node_id=arc.to_concept, parent=parent)

            # Add current Arc to linkrole Arc list for future reference.
            roletype.add_arc(arc=arc)

            # current_node = AnyNode(
            #     object=to_node,
            #     parent=parent,
            # )

            # look for children of this Arc

    # At the end, return the hierarchy so far
    return from_node


def get_node(roletype, node_label, parent, order=None, dts=None):
    """
    Takes a roletype and node_id. Returns the node with the given ID.

    :param roletype:
    :param node_label:
    :return:
    """

    for elem in [
        elem
        for elem in roletype.xml_table_elems
        if elem.tag == "{http://xbrl.org/2014/table}ruleNode"
    ]:
        if elem.get("{http://www.w3.org/1999/xlink}label") == node_label:
            roletype.xml_table_elems.pop(roletype.xml_table_elems.index(elem))
            return get_rule_node(
                elem=elem, roletype=roletype, parent=parent, order=order, dts=dts
            )

    for elem in [
        elem
        for elem in roletype.xml_table_elems
        if elem.tag == "{http://xbrl.org/2014/table}aspectNode"
    ]:
        if elem.get("id") == node_label:
            roletype.xml_table_elems.pop(roletype.xml_table_elems.index(elem))
            return get_aspect_node(
                elem=elem, roletype=roletype, parent=parent, order=order, dts=dts
            )

    for elem in [
        elem
        for elem in roletype.xml_table_elems
        if elem.tag == "{http://xbrl.org/2014/table}conceptRelationshipNode"
    ]:
        if elem.get("{http://www.w3.org/1999/xlink}label") == node_label:
            roletype.xml_table_elems.pop(roletype.xml_table_elems.index(elem))
            return get_concept_relationship_node(
                elem=elem, roletype=roletype, parent=parent, order=order, dts=dts
            )

    for elem in [
        elem
        for elem in roletype.xml_table_elems
        if elem.tag == "{http://xbrl.org/2014/table}dimensionRelationshipNode"
    ]:

        if elem.get("{http://www.w3.org/1999/xlink}label") == node_label:
            roletype.xml_table_elems.pop(roletype.xml_table_elems.index(elem))
            return get_dimension_relationship_node(
                elem=elem, parent=parent, order=order, roletype=roletype, dts=dts
            )

    else:
        print(f"could not get table node: {node_label}")


def get_rule_node(elem, roletype, parent, order, dts):
    """
    Source: https://www.xbrl.org/Specification/table-linkbase/REC-2014-03-18+errata-2018-07-17/table-linkbase-REC-2014-03-18+corrected-errata-2018-07-17.html#sec-rule-node

    A <table:ruleNode> element MAY have one or more elements from the <formula:aspectRule> substitution group as
    children of itself, or as children of <table:ruleSet> elements which are children of itself.
    These are used to specify aspects and aspect constraints for the node.
    """

    rulesets = []

    node = RuleNode(
        id=elem.get("id"),
        xlink_label=elem.get("{http://www.w3.org/1999/xlink}label"),
        order=order,
        parent=parent,
        parentChildOrder=elem.get("parentChildOrder"),
        is_abstract=elem.get("abstract"),
        merge=elem.get("merge"),
        tagselector=elem.get("tagselector"),
    )

    # get children
    for child in elem.getchildren():

        if child.tag == "{http://xbrl.org/2014/table}ruleSet":
            # Get ruleset
            rulesets.append(
                RuleSet(child.get("tag"), child_elements=child.getchildren())
            )

        elif child.tag == "{http://xbrl.org/2008/formula}explicitDimension":
            # http://www.xbrl.org/specification/formula/rec-2009-06-22/formula-rec-2009-06-22.html#sec-explicit-dimension-rules

            qname_dimension = get_qname_object(
                qname=child.get("dimension"), linkrole=roletype
            )
            if not qname_dimension:
                qname_dimension = child.get("dimension")

            explicit_dimension = FormulaExplicitDimension(
                qname_dimension=qname_dimension,
                qname_member=get_qname(
                    elem=child,
                    type="member",
                    namespace="{http://xbrl.org/2008/formula}",
                    linkrole=roletype,
                ),
            )

            # If no qname has been found, try to find a qname expression
            if not explicit_dimension.qname_member:
                expression = get_qname_expressions(
                    elem=child,
                    type="member",
                    namespace="{http://xbrl.org/2008/formula}",
                )

                explicit_dimension.qname_member_expression = expression

            rulesets.append(explicit_dimension)
        elif child.tag == "{http://xbrl.org/2008/formula}typedDimension":
            # http://www.xbrl.org/specification/formula/rec-2009-06-22/formula-rec-2009-06-22.html#sec-typed-dimension-rules

            qname_dimension = get_qname_object(
                qname=child.get("dimension"), linkrole=roletype
            )
            if not qname_dimension:
                qname_dimension = child.get("dimension")

            typed_dimension = FormulaTypedDimension(qname_dimension=qname_dimension)
            for grandchild in child.getchildren():  # Get nested children
                if (
                    grandchild.tag == "{http://xbrl.org/2008/formula}member"
                ):  # Get member
                    typed_dimension.member = get_qname(
                        elem=grandchild,
                        type="dimension",
                        namespace="{http://xbrl.org/2008/formula}",
                        linkrole=roletype,
                    )
            typed_dimension.xpath = child.get("xpath")

            typed_dimension.value = child.get("value")

            rulesets.append(typed_dimension)
        elif child.tag == "{http://xbrl.org/2008/formula}period":
            rulesets.append(
                FormulaPeriod(elem=child)
            )  # this is less nested, so don't feed the children
        else:
            logger.error(f"Child '{child.tag}' for node '{node.id}' is unknown")

    if rulesets is not None:
        node.rulesets = rulesets
    return node


def get_aspect_node(elem, roletype, parent, order, dts=None):
    """
    Source: https://www.xbrl.org/Specification/table-linkbase/REC-2014-03-18+errata-2018-07-17/table-linkbase-REC-2014-03-18+corrected-errata-2018-07-17.html#sec-aspect-node

    An aspect node is represented by a <table:aspectNode> element with exactly one child element in the
    <table:aspectSpec> substitution group and optionally one or more <variable:filter> resources related by
    aspect-node-filter relationships.

    The <table:conceptAspect> , <table:entityIdentifierAspect> , <table:periodAspect> ,
    <table:unitAspect> elements specify the concept, entityIdentifier, period and unit aspects respectively.

    The <table:dimensionAspect> element specifies a dimensional aspect by the dimension's QName.
    This MUST be the QName of a dimension that exists in the DTS. It has an optional @includeUnreportedValue attribute
    (which defaults to false) which specifies the includeUnreportedValue property of the aspect node.
    """

    node = AspectNode(
        id=elem.get("id"),
        xlink_label=elem.get("{http://www.w3.org/1999/xlink}label"),
        order=order,
        parent=parent,
        is_abstract=elem.get("abstract"),
        merge=elem.get("merge", "false"),
        tagselector=elem.get("tagselector"),
    )

    # get children
    for child in elem.getchildren():
        if child.tag == "{http://xbrl.org/2014/table}conceptAspect":
            node.aspect = Aspect(aspect_type="concept", value=get_element_text(child))

        elif child.tag == "{http://xbrl.org/2014/table}entityIdentifierAspect":
            node.aspect = Aspect(
                aspect_type="identity_identifier", value=get_element_text(child)
            )

        elif child.tag == "{http://xbrl.org/2014/table}periodAspect":
            node.aspect = Aspect(aspect_type="period", value=get_element_text(child))

        elif child.tag == "{http://xbrl.org/2014/table}unitAspect":
            node.aspect = Aspect(aspect_type="unit", value=get_element_text(child))

        elif child.tag == "{http://xbrl.org/2014/table}dimensionAspect":
            dimension_id = get_element_text(child)
            # String is added to the aspect node, Dimension is linked later on as we need access to the DTS.

            # dim = roletype.get_dimension(dimension_id)
            node.aspect = Aspect(
                aspect_type="dimension",
                value=dimension_id,
                includeUnreportedValue=child.get("includeUnreportedValue"),
            )
        else:
            logger.error(f"Child '{child.tag}' for node '{node.id}' is unknown")

    return node


def get_concept_relationship_node(elem, roletype, parent, order, dts):
    """
    Source: https://www.xbrl.org/Specification/table-linkbase/REC-2014-03-18+errata-2018-07-17/table-linkbase-REC-2014-03-18+corrected-errata-2018-07-17.html#sec-relationship-node

    A relationship node defines a tree walk of all or part of one or more networks of concepts.

    A concept relationship node MAY include any number of <table:relationshipSource> or <table:relationshipSourceExpression> elements, each containing, respectively, a QName (xs:QName) or an XPath expression that evaluates to a QName identifying a single relationship source for the tree walk. If a relationship source is specified, it MUST be either:

    the QName of a concept that exists in the DTS, or
    the special value xfi:root.

    """

    node = ConceptRelationshipNode(
        id=elem.get("id"),
        xlink_label=elem.get("{http://www.w3.org/1999/xlink}label"),
        order=order,
        parent=parent,
        parentChildOrder=elem.get("parentChildOrder"),
        tagselector=elem.get("tagSelector"),
    )

    # get children
    for child in elem.getchildren():
        if child.tag == "{http://xbrl.org/2014/table}arcrole":
            node.arcrole = get_element_text(child)

        elif child.tag == "{http://xbrl.org/2014/table}linkrole":
            node.linkrole = get_element_text(child)

        elif child.tag == "{http://xbrl.org/2014/table}relationshipSource":
            node.relationship_source = get_element_text(child)

        elif child.tag == "{http://xbrl.org/2014/table}formulaAxis":
            node.formula_axis = get_element_text(child)

        elif child.tag == "{http://xbrl.org/2014/table}linkname":
            node.linkname = get_element_text(child)

        elif child.tag == "{http://xbrl.org/2014/table}arcname":
            node.arcname = get_element_text(child)

        elif child.tag == "{http://xbrl.org/2014/table}relationshipSource":
            node.relationship_source = get_element_text(child)
        else:
            logger.error(f"Child '{child.tag}' for node '{node.id}' is unknown")

    return node


def get_dimension_relationship_node(elem, parent, order, roletype, dts):
    """

    A relationship node defines a tree walk of all or part of one or more networks of concepts.

    A dimension relationship node is a relationship node which describes a tree of explicit dimension members
    in terms of a tree walk of a dimensional relationship set (DRS).

    """
    node = DimensionRelationshipNode(
        id=elem.get("id"),
        xlink_label=elem.get("{http://www.w3.org/1999/xlink}label"),
        order=order,
        parent=parent,
        relationship_source=elem.get("relationshipSource"),
        parentChildOrder=elem.get("parentChildOrder"),
        tagselector=elem.get("tagselector"),
    )

    # get children
    for child in elem.getchildren():

        if child.tag == "{http://xbrl.org/2014/table}relationshipSource":
            node.relationship_source = get_element_text(child)
        elif child.tag == "{http://xbrl.org/2014/table}linkrole":
            node.linkrole = get_element_text(child)
        elif child.tag == "{http://xbrl.org/2014/table}formulaAxis":
            node.formula_axis = get_element_text(child)
        elif child.tag == "{http://xbrl.org/2014/table}dimension":
            node.dimension = get_element_text(child)
        else:
            logger.error(f"Child '{child.tag}' for node '{node.id}' is unknown")

    if not hasattr(node, "dimension"):
        logger.error(
            f"Dimensional Relationship node '{elem.get('id')}' does not have required attr 'dimension'"
        )

    return node


def get_element_text(elem):
    """
    Returns the text of an element, if available. Otherwise returns none

    :return:
    """
    if elem.text:
        return elem.text
    return None
