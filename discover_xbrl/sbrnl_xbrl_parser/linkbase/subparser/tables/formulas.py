import logging

from .breakdown import get_table_breakdown_arc
from .classes import Table
from ...classes import Arc

logger = logging.getLogger("parser.tag.generic")


def get_table_parameter(roletype, table):

    """
    Yield a table parameter when found
    :param roletype:
    :return:
    """

    # first get parameters

    for table_parameter in [
        arc
        for arc in roletype.arcs
        if arc.arcrole == "http://xbrl.org/arcrole/2014/table-parameter"
    ]:
        if table_parameter.from_concept == table.xlink_label:
            table_parameter.from_concept_obj = (
                table  # Set 'from' object as table did not exist before
            )

            yield table_parameter.to_concept_obj


def get_variable_filter(roletype, table):

    """
    Yield a table parameter when found

    :param roletype:
    :return:
    """

    # first get parameters

    for variable_filter in [
        arc
        for arc in roletype.arcs
        if arc.arcrole == "http://xbrl.org/arcrole/2008/variable-filter"
    ]:
        if variable_filter.from_concept == table.xlink_label:
            variable_filter.from_concept_obj = (
                table  # Set 'from' object as table did not exist before
            )

            yield variable_filter.to_concept_obj
