from anytree import AnyNode

from ...classes import Arc, GenericArc, Resource
from ..formulas.classes.formula import FormulaPeriod


class Table:
    def __init__(self, parentChildOrder, xlink_label, id=None):

        self.parentChildOrder = parentChildOrder
        self.xlink_label = xlink_label
        self.id = id

        self.table_breakdowns = []
        self.filters = []
        self.parameters = []
        self.labels = {}

    def __str__(self):
        return self.xlink_label


class Breakdown(Resource, AnyNode):
    def __init__(self, parentChildOrder, xlink_label, axis, order, id=None, **kwargs):

        super().__init__(xlink_label=xlink_label, id=id, **kwargs)

        self.parentChildOrder = parentChildOrder

        self.axis = axis
        self.order = order

    def __str__(self):
        return f"Axis: '{self.axis}' order: {self.order}. ordering: '{self.parentChildOrder}'"


class Node_Subtree:
    """
    This is really an arc, but with semantic meaning, which makes it a bit different

    """

    def __init__(self, order, breakdown=None, tree=None, arc=None):

        self.order = order

        self.arc = arc

        self.breakdown = breakdown  # From
        self.tree = tree  # To


class Node(Resource, AnyNode):
    def __init__(self, id, order, parent, xlink_label, tagselector=None):

        super().__init__(xlink_label, id)
        self.tagselector = tagselector
        self.id = id
        self.xlink_label = xlink_label
        self.parent = parent  # anynode parent
        self.order = order  # Arc order
        self.labels = {}


class RuleNode(Node):
    def __init__(
        self,
        id,
        xlink_label,
        parent,
        order,
        parentChildOrder,
        is_abstract,
        merge,
        rulesets=None,
        tagselector=None,
    ):

        self.parentChildOrder = parentChildOrder
        self.is_abstract = is_abstract
        self.merge = merge
        self.rulesets = rulesets if rulesets is not None else []
        super().__init__(
            id=id,
            xlink_label=xlink_label,
            order=order,
            tagselector=tagselector,
            parent=parent,
        )


class RuleSet:
    def __init__(self, tag, formulas=None, child_elements=None):

        self.tag = tag
        self.formulas = formulas if formulas else []
        if child_elements:
            for child in child_elements:
                if child.tag == "{http://xbrl.org/2008/formula}period":
                    self.formulas.append(FormulaPeriod(elem=child))


class ConceptRelationshipNode(Node):
    def __init__(
        self,
        id,
        xlink_label,
        order,
        parent,
        parentChildOrder,
        relationship_source=None,
        linkrole=None,
        formula_axis=None,
        generations=None,
        arcrole=None,
        linkname=None,
        arcname=None,
        tagselector=None,
    ):

        self.parentChildOrder = parentChildOrder
        self.relationship_source = relationship_source
        self.linkrole = linkrole

        self.formula_axis = formula_axis
        self.generations = generations
        self.arcrole = arcrole
        self.linkname = linkname
        self.arcname = arcname

        super().__init__(
            id=id,
            xlink_label=xlink_label,
            order=order,
            tagselector=tagselector,
            parent=parent,
        )


class DimensionRelationshipNode(Node):
    def __init__(
        self,
        id,
        xlink_label,
        parent,
        order,
        parentChildOrder,
        dimension=None,
        linkrole=None,
        formula_axis=None,
        generations=None,
        relationship_source=None,
        tagselector=None,
    ):

        self.parentChildOrder = parentChildOrder
        self.relationship_source = relationship_source
        self.linkrole = linkrole
        self.formula_axis = formula_axis
        self.generations = generations
        self.dimension = dimension

        super().__init__(
            id=id,
            xlink_label=xlink_label,
            order=order,
            tagselector=tagselector,
            parent=parent,
        )


class AspectNode(Node):
    def __init__(
        self,
        id,
        xlink_label,
        parent,
        order,
        is_abstract=False,
        merge=False,
        tagselector=None,
    ):

        self.is_abstract = is_abstract
        self.merge = merge

        super().__init__(
            id=id,
            xlink_label=xlink_label,
            order=order,
            tagselector=tagselector,
            parent=parent,
        )


class Aspect:
    def __init__(self, aspect_type, value, includeUnreportedValue=False):

        self.aspect_type = aspect_type
        self.value = value
        self.includeUnreportedValue = includeUnreportedValue


"""
ARCS
"""


class Table_Subtree_Arc(Arc):
    def __init__(self, tag, arcrole, order, from_object, to_object):

        self.order = order

        self._from_concept = from_object  # Set from ID so table can be added later

        self._to_concept = to_object

        super(Table_Subtree_Arc, self).__init__(tag=tag, arcrole=arcrole)


class Table_Breakdown_Arc(Arc):
    """
    This is an arc, but with semantic meaning

    """

    def __init__(self, tag, arcrole, axis, order, table=None, breakdown=None):

        self.axis = axis
        self.order = order

        self.table = table  # From
        self._from_concept = table  # Set from ID so table can be added later

        self.breakdown = breakdown  # To
        self._to_concept = breakdown

        super(Table_Breakdown_Arc, self).__init__(tag=tag, arcrole=arcrole)

    # def __str__(self):
    #     return f"{self.table} > {self.breakdown}"

    # @property
    # def from_concept_obj(self):
    #     return self.table
    #
    # @property
    # def to_concept_obj(self):
    #     return self.breakdown


class Table_Parameter_Arc(Arc):
    def __init__(self, tag, name, arcrole, table, parameter, order):

        self.name = name
        self.table = table
        self._from_concept = table  # Set from ID so table can be added later

        self.parameter = parameter
        self._to_concept = parameter  # Set from ID so table can be added later
        self.order = order
        super(Table_Parameter_Arc, self).__init__(tag=tag, arcrole=arcrole)


class VariableArc(GenericArc):
    def __init__(self, tag, arcrole, name, order, priority, assertion, variable):

        self.order = order
        self.priority = priority

        self.asssertion = assertion
        self.variable = variable

        super(VariableArc, self).__init__(
            tag=tag, arcrole=arcrole, name=name, from_obj=assertion, to_obj=variable
        )
