from .classes import Breakdown

from .nodes import get_definition_subtree_children


def get_table_breakdown_arc(roletype, table, dts):
    """
    Yields table breakdown per axis of a table

    :param roletype:
    :param table:
    :return:
    """

    for table_breakdown_arc in [
        arc
        for arc in roletype.arcs
        if (arc.arcrole == "http://xbrl.org/arcrole/2014/table-breakdown")
    ]:

        if table_breakdown_arc.from_concept == table.id:

            table_breakdown_arc.table = table

            # Get breakdown Object, add as 'to' object
            # This also gets all connected Nodes recursively
            breakdown = get_breakdown(
                roletype,
                breakdown_id=table_breakdown_arc.to_concept,
                axis=table_breakdown_arc.axis,
                order=table_breakdown_arc.order,
                dts=dts,
            )
            roletype.resources["table"].append(breakdown)

            # Add breakdown after getting it
            table_breakdown_arc.breakdown = breakdown

            yield table_breakdown_arc


def get_breakdown(roletype, breakdown_id, axis, order, dts):
    """
    Takes the breakdown id, axis and order (these are derived from a BreakdownArc)
    Returns a Breakdown object with all Nodes attached to it.

    :param roletype:
    :param breakdown_id:
    :param axis: axis of the breakdown
    :param order: order of the breakdown
    :param dts: holds the locators to find labels
    :return: Breakdown object
    """

    for elem in [
        elem
        for elem in roletype.xml_table_elems
        if elem.tag == "{http://xbrl.org/2014/table}breakdown"
    ]:
        # The correct element is the one that has tas the table
        if elem.get("id") == breakdown_id:

            breakdown = Breakdown(
                parentChildOrder=elem.get("parentChildOrder"),
                xlink_label=elem.get("{http://www.w3.org/1999/xlink}label"),
                axis=axis,
                order=order,
                id=elem.get("id"),
            )

            # Get nodes within the breakdown tree
            get_breakdown_tree(roletype=roletype, breakdown=breakdown, dts=dts)
            # take XML element out of the queue
            roletype.xml_table_elems.pop(roletype.xml_table_elems.index(elem))

            return breakdown


def get_breakdown_tree(roletype, breakdown, dts):
    """
    Gathers all nodes connected to a Breakdown

    :param roletype:
    :param breakdown:
    :param dts
    :return: Nynode
    """
    for breakdown_arc in [
        arc
        for arc in roletype.arcs
        if arc.arcrole == "http://xbrl.org/arcrole/2014/breakdown-tree"
    ]:

        # The correct element is the one that has tas the table

        if breakdown_arc.from_concept == breakdown.id:
            # Get the root node

            if breakdown_arc.arc_type == "{http://xbrl.org/2014/table}breakdownTreeArc":
                get_definition_subtree_children(
                    roletype=roletype,
                    parent=breakdown,
                    from_id=breakdown_arc.to_concept,
                    order=breakdown_arc.order,
                    dts=dts,
                )

    return breakdown
