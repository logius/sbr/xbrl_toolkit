import logging

from .classes import Table_Breakdown_Arc, Table_Subtree_Arc, Table_Parameter_Arc

logger = logging.getLogger("parser.linkbase")
"""
Functions to process table arcs.
These functions are called during discovery.
"""


def process_table_arcs(elem, linkrole, dts, url):
    if elem.tag == "{http://xbrl.org/2014/table}breakdownTreeArc":
        # Is an arc, but with @order. Used to connect a table Breakdown with a (root) Node

        # From: Breakdown
        # To:   Node
        linkrole.add_arc(
            Table_Subtree_Arc(
                tag=elem.tag,
                arcrole=elem.get("{http://www.w3.org/1999/xlink}arcrole"),
                order=float(elem.get("order")),
                from_object=elem.get("{http://www.w3.org/1999/xlink}from"),
                to_object=elem.get("{http://www.w3.org/1999/xlink}to"),
            )
        )

    elif elem.tag == "{http://xbrl.org/2014/table}definitionNodeSubtreeArc":
        # Is an arc, but with @order. Used between table nodes

        # From: Node
        # To:   Node
        linkrole.add_arc(
            Table_Subtree_Arc(
                tag=elem.tag,
                arcrole=elem.get("{http://www.w3.org/1999/xlink}arcrole"),
                order=float(elem.get("order")),
                from_object=elem.get("{http://www.w3.org/1999/xlink}from"),
                to_object=elem.get("{http://www.w3.org/1999/xlink}to"),
            )
        )

    elif elem.tag == "{http://xbrl.org/2014/table}tableBreakdownArc":
        # From: Table
        # To:   Breakdown

        arc = Table_Breakdown_Arc(
            tag=elem.tag,
            arcrole=elem.get("{http://www.w3.org/1999/xlink}arcrole"),
            order=elem.get("order"),
            axis=elem.get("axis"),
            table=elem.get("{http://www.w3.org/1999/xlink}from"),
            breakdown=elem.get("{http://www.w3.org/1999/xlink}to"),
        )
        linkrole.add_arc(arc)

    elif elem.tag == "{http://xbrl.org/2014/table}tableParameterArc":
        # From: Table
        # To:   Parameter (variable)

        arc = Table_Parameter_Arc(
            name=elem.attrib.get("name", None),
            tag=elem.tag,
            arcrole=elem.attrib.get("{http://www.w3.org/1999/xlink}arcrole"),
            order=elem.attrib.get("order", None),
            table=elem.attrib.get("{http://www.w3.org/1999/xlink}from"),
            parameter=elem.attrib.get("{http://www.w3.org/1999/xlink}to"),
        )
        for parameter in dts.get_resources["parameters"]:
            loc_ref = dts.locs[elem.get("{http://www.w3.org/1999/xlink}to")].href_id
            if parameter.id == loc_ref:
                arc.to_concept_obj = parameter
        if arc.to_concept_obj is None:
            logger.warning(f"TableArc 'to' object (Parameter) not found in DTS!")

        linkrole.add_arc(arc)

    else:
        logger.error(f"TableArc '{elem.tag}' not known.")
