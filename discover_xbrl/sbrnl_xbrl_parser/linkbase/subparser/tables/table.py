import logging

from .breakdown import get_table_breakdown_arc
from .classes import Table
from .formulas import get_table_parameter, get_variable_filter

logger = logging.getLogger("parser.tag.generic")


def build_table(roletype, dts):

    """
    Yield a table when found

    :param roletype:
    :return:
    """

    # First convert the 'definitionNodeSubtreeArc' elements to Arc objects as they are needed for recursively
    # discovering these subtrees
    # print(roletype.__dict__)

    for index, elem in enumerate(
        [
            elem
            for elem in roletype.xml_table_elems
            if elem.tag == "{http://xbrl.org/2014/table}table"
        ]
    ):
        table = Table(
            parentChildOrder=elem.get("parentChildOrder"),
            xlink_label=elem.get("{http://www.w3.org/1999/xlink}label"),
            id=elem.get("id", None),
        )
        roletype.resources["table"].append(table)

        # Add breakdown(s) tree
        for table_breakdown_arc in get_table_breakdown_arc(roletype, table, dts):

            # Add arc to roletype for future reference
            roletype.add_arc(table_breakdown_arc)

            logger.debug(f"roletype: {roletype.roleURI} adding arc")

            # Add the breakdown directly to the table.
            table.table_breakdowns.append(table_breakdown_arc.breakdown)

        # Get parameter(s)
        for parameter in get_table_parameter(roletype, table):
            table.parameters.append(parameter)

        for variablefilter in get_variable_filter(roletype=roletype, table=table):
            table.filters.append(variablefilter)

        # take XML element out of the queue
        roletype.xml_table_elems.pop(roletype.xml_table_elems.index(elem))

        yield table
