import logging
from copy import copy

from anytree import AnyNode
from lxml import etree

from ..subparser.dimensions.classes import Dimension
from ..subparser.dimensions.hypercube import create_hypercubes
from ..subparser.dimensions.hypercube import get_dimension_hierarchy
from ..subparser.dimensions.hypercube_dimension import get_domain_member_children
from ..subparser.tables.table import build_table
from ...schema.classes import Concept

logger = logging.getLogger("parser.postprocessing")


def parse_objects(dts):
    """
    Postprocessing

    :param dts:
    :return:
    """

    # Add itemType objects to concepts
    for concept in dts.concepts.values():
        if isinstance(concept.element_type, str):
            for datatype in dts.datatypes:

                if datatype.id == concept.element_type:

                    concept.element_type = datatype
                    break
                elif f"{datatype.domain}:{datatype.name}" == concept.element_type:

                    concept.element_type = datatype
                    break

                # Could be there is no prefix. In that case, make a guess solely based upon name
                if ":" in concept.element_type:
                    if datatype.name == concept.element_type.split(":", 1)[1]:

                        concept.element_type = datatype
                        break

    for concept in dts.concepts.values():
        if isinstance(concept.element_type, str):
            if concept.element_type in [
                # 'xbrli:stringItemType', 'xbrli:sharesItemType', 'xbrli:dateItemType', 'xbrli:nonNegativeIntegerItemType',
                # 'xbrli:decimalItemType', 'sbr:placeholder',
            ]:
                pass
            else:
                # Note: https://www.w3.org/2001/XMLSchema.xsd is not imported explicitly
                logger.warning(f"still a string: {concept.element_type}")

    for uri, roletype in dts.roletypes.items():
        # if roletype in dts.directly_referenced_linkbases:
        if roletype.used_on:
            if "link:presentationLink" in roletype.used_on:
                roletype.presentation_hierarchy = build_presentation_hierarchy(roletype)

            if "link:definitionLink" in roletype.used_on:
                # Create hypercube definition structure
                create_hypercubes(roletype, dts.roletypes)
                build_definition_hierarchy(roletype)

        if roletype.roleURI == "http://www.xbrl.org/2008/role/link":
            roletype.presentation_hierarchy = build_presentation_hierarchy(roletype)
            dts.presentation_hierarchy = roletype.presentation_hierarchy

        # Get and add tables to linkrole. This should be done after creating hypercubes
        # as dimensions can be referenced from tables
        for table in build_table(roletype, dts):
            roletype.tables.append(table)

        if roletype.xml_table_elems or len(roletype.xml_table_elems) > 0:
            if roletype not in dts.directly_referenced_linkbases:
                logger.warning(
                    f"Roletype {roletype.roleURI} still had table elements after building the tables, "
                    f"and roletype is not even directly referenced by EP!"
                )
            else:
                logger.warning(
                    f"Roletype {roletype.roleURI} still had table elements after building the tables"
                )
            for t in roletype.xml_table_elems:
                print(f"ID's left: {t.get('id')} {etree.tostring(t)}")
            roletype.xml_table_elems = []

    elements = {}
    for element_list in dts.get_resources.values():
        for element in element_list:
            if element is None:
                print("Trouble")
            else:
                elements[element.xlink_label] = element

    print("Parse remaining arcs")

    for roletype in dts.roletypes.values():
        for arc in roletype.arcs:
            if arc.from_concept_obj is None:
                """
                If we don't have a from_concept_obj, we need to find it.
                First, we check if the xlink_label matches
                Secondly, we check for the @id attribute
                Lastly, we try to locate it via a locator.
                """

                if arc.from_concept in elements.keys():
                    element = elements[arc.from_concept]
                else:
                    # Try to find on ID
                    elems = [e for e in elements.values() if e.id == arc.from_concept]
                    if len(elems) > 0:
                        element = elems[0]
                    else:
                        # Find the object via Locator
                        loc = [
                            loc
                            for loc in dts.locs.values()
                            if loc.label == arc.from_concept
                        ]
                        if len(loc) > 0:
                            if arc.from_concept == loc[0].label:
                                if not loc[0].href_id in elements.keys():
                                    logger.warning(
                                        f"resource not found when adding labels: {loc[0].href_id}"
                                    )
                                    element = None
                                    # todo: Enumerations are sometimes linked 'from', but these are not resources, but 'normal' elements.
                                else:
                                    element = elements[loc[0].href_id]
                            else:
                                element = None
                        else:
                            element = None

                if element is not None:
                    # Found the resource
                    arc.from_concept_obj = element
                else:
                    logger.warning(
                        f"Still not found: {arc.from_concept} Needs to be added to resources?"
                    )
            if arc.to_concept_obj is None:
                # Try to attach some labels to element that are created late in the process
                for label in roletype.element_labels.values():
                    if label.xlink_label == arc.to_concept:
                        arc.to_concept_obj = label
                        break

    print("Starting to link element-labels")
    for roletype in dts.roletypes.values():
        for arc in roletype.arcs:
            if arc.arcrole in [
                "http://xbrl.org/arcrole/2008/element-label",
                "http://www.nltaxonomie.nl/2017/arcrole/DynamicLabel",
                "http://xbrl.org/arcrole/2010/assertion-unsatisfied-message",
            ]:
                if arc.from_concept_obj is None or arc.to_concept_obj is None:
                    logger.warning(
                        f"Don't have a from or to object to attach element-label to: {arc.from_concept}"
                    )
                    continue
                if not hasattr(arc.from_concept_obj, "labels"):
                    print(
                        f"This could be an Unsatisfied message linked to: {type(arc.from_concept_obj)}"
                    )
                    print(f"No labels!")
                else:
                    if isinstance(arc.from_concept_obj.labels, dict):
                        arc.from_concept_obj.labels[
                            arc.to_concept_obj.xlink_label
                        ] = arc.to_concept_obj

                    elif isinstance(arc.from_concept_obj.labels, list):
                        arc.from_concept_obj.labels.append(arc.to_concept_obj)
            elif (
                arc.arcrole
                == "http://xbrl.org/arcrole/2016/assertion-unsatisfied-severity"
            ):
                arc.from_concept_obj.severity = arc.to_concept_obj.id


def get_presentation_children(roletype, parent_child_list, parent=None):
    """
    Recursively traverse presentationArcs

    :param roletype:
    :param parent:
    :return:
    """

    # Create a list with all children
    children = []
    looplist = copy(parent_child_list)
    if parent.object is None:
        return parent
    for arc in looplist:
        if arc.from_concept_obj is None:
            continue
        if (  # We can do this because only Concepts get inspected
            arc.from_concept_obj.name == parent.object.name
            and arc.from_concept_obj.element_type == parent.object.element_type
            and arc.from_concept_obj.periodType == parent.object.periodType
        ):
            if hasattr(arc, "preferred_label"):
                preferred_label = arc.preferred_label
            else:
                preferred_label = None

            if (
                arc.from_concept_obj is None
                or arc.from_concept_obj is False
                or arc.from_concept_obj == []
            ):
                logger.warning(
                    f"When parsing arc for presentation hierarchy, no 'from' object is found."
                )
            elif (
                arc.to_concept_obj is None
                or arc.to_concept_obj is False
                or arc.to_concept_obj == []
            ):
                logger.warning(
                    f"When parsing arc for presentation hierarchy, no 'to' object is found."
                )
            else:
                children.append((arc.order, arc.to_concept_obj, preferred_label))
                # parent_child_list.remove(arc)

    # Add children to parent
    for child in children:
        # Add current Arc to hierarchy

        if child[1] is None or child[1] == []:
            print(f"=empty children? {parent}")

        current_node = AnyNode(
            object=child[1], preferred_label=child[2], order=child[0], parent=parent
        )

        # look for grandchildren.
        # roleTypes are always leaves!
        if isinstance(child[1], Concept):
            get_presentation_children(
                roletype, parent_child_list=parent_child_list, parent=current_node
            )

    # At the end, return the hierarchy so far
    return parent


def build_presentation_hierarchy(roletype):

    # Build a list of elements Arcs point to.
    # The root element is found when a 'from' arc does not also appear as a 'to' attribute.

    to_list = []
    root = None
    for arc in roletype.arcs:
        to_list.append(arc.to_concept)
    for arc in roletype.arcs:

        if (
            arc.arc_type == "{http://www.xbrl.org/2003/linkbase}presentationArc"
            and arc.arcrole == "http://www.xbrl.org/2003/arcrole/parent-child"
        ):

            if arc.from_concept_obj is None:
                logger.warning(
                    f"When parsing arc for presentation hierarchy, no 'from' object is found."
                )
            else:
                # Try to find the root of the tree. The root is found if the 'to' is
                # is not in the arc list (nothing points towards the root).
                if root is None and arc.from_concept not in to_list:
                    root = AnyNode(object=arc.from_concept_obj, order=arc.order)

        elif (
            arc.arc_type == "{http://xbrl.org/2008/generic}arc"
            and arc.arcrole == "http://www.xbrl.org/2013/arcrole/parent-child"
        ):
            if arc.from_concept_obj is None:
                logger.warning(
                    f"When parsing arc for presentation hierarchy, no 'from' object is found."
                )
            else:
                # Try to find the root of the tree. The root is found if the 'to' is
                # is not in the arc list (nothing points towards the root).
                if root is None and arc.from_concept not in to_list:
                    root = AnyNode(object=arc.from_concept_obj, order=arc.order)

    if roletype.roleURI == "http://www.xbrl.org/2008/role/link":
        parent_child_list = [
            arc
            for arc in roletype.arcs
            if arc.arcrole == "http://www.xbrl.org/2013/arcrole/parent-child"
        ]
    else:
        parent_child_list = [
            arc
            for arc in roletype.arcs
            if arc.arcrole == "http://www.xbrl.org/2003/arcrole/parent-child"
        ]

    # If the root is found, return the hierarchy tree, otherwise return None.
    if root:
        hierarchy = get_presentation_children(roletype, parent_child_list, root)
        return hierarchy

    else:
        logger.info(
            f"Presentation hierarchy root for roletype: {roletype.roleURI} could not be found"
        )
        return None


def build_definition_hierarchy(linkrole):
    to_list = []
    top = None
    for arc in linkrole.arcs:
        to_list.append(arc.to_concept)
    for arc in linkrole.arcs:
        if arc.arc_type == "{http://www.xbrl.org/2003/linkbase}definitionArc":
            if arc.from_concept_obj is None:
                logger.warning(
                    f"When parsing arc for definition hierarchy, no 'from' object is found."
                )
            else:
                if top is None and arc.from_concept not in to_list:
                    top = arc
    if top and top.arcrole == "http://xbrl.org/int/dim/arcrole/dimension-domain":
        linkrole.definition_hierarchy = get_dimension_hierarchy(top, linkrole)
    elif top and top.arcrole == "http://xbrl.org/int/dim/arcrole/domain-member":
        if linkrole.definition_hierarchy is None:
            linkrole.definition_hierarchy = Dimension(concept=top.from_concept_obj)
            get_domain_member_children(
                arcs=linkrole.arcs, parent_node=linkrole.definition_hierarchy
            )


def rgs_mappings(rgs_mapping_arcs, linkrole):
    # Create a set of all child elements
    to_elements = set([arc.to_concept for arc in rgs_mapping_arcs])
    root_elements = []

    for arc in rgs_mapping_arcs:
        # go check what element is referenced as a 'from', but is not in 'to'. This should be the root.
        if arc.from_concept not in to_elements:
            if arc.from_concept_obj not in root_elements:
                # todo: EK: many2one have multiple 'roots' and one 'child' :S

                root_elements.append(arc.from_concept_obj)

    if len(root_elements) > 1:
        print(f"Got too many RGS root elements for ELR '{linkrole.roleURI}'")
    elif len(root_elements) == 1:
        root_element = AnyNode(root_elements[0])
        # todo: (somewhere in the rgs-branch is a start for this, rgs 2022 is a different beast from 2015
        # hierarchy = rgs_hierarchy_children(arcs=rgs_mapping_arcs, parent=root_element)
        # linkrole.definition_hierarchy = hierarchy


def rgs_postprocessing(dts):
    """
    Custom loop for RGS postprocessing.
    Here we need to build a presentation hierarchy based on arcs.

    :param dts:
    :return:
    """

    for linkrole in dts.roletypes.values():

        if len(linkrole.arcs) > 0:

            rgs_mapping_arcs = [
                arc
                for arc in linkrole.arcs
                if arc.arcrole == "http://www.nltaxonomie.nl/rgs/2015/arcrole/mapping"
            ]
            pass
            # rgs_mappings(rgs_mapping_arcs, linkrole)
