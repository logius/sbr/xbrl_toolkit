import logging

from ..functions import follow_local_loc

logger = logging.getLogger("parser.postprocessing")


def get_attribute_for_element(arc, dts_locs, dts_from, dts_to):
    """
    Takes and Arc and returns the referring Concept (from) and referenced attribute (to)

    :return:
    """

    # find the concept that is referenced

    loc = dts_locs[arc.from_concept]

    # First try to match to an already discovered label
    referenced_concept = follow_local_loc(dts_from, loc)

    if arc.to_concept in dts_to.keys():
        referenced_attribute = dts_to[arc.to_concept]
    else:
        referenced_attribute = None

    return [referenced_concept, referenced_attribute]
