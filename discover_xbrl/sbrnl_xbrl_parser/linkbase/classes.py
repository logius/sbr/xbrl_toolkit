import logging

logger = logging.getLogger("parser.linkbase")


class Resource:
    def __init__(self, xlink_label, role=None, id=None):
        # Fixed value
        # https://www.xbrl.org/Specification/XBRL-2.1/REC-2003-12-31/XBRL-2.1-REC-2003-12-31+corrected-errata-2013-02-20.html#_3.5.3.8.1
        # self.type = "resource"
        # But is not set because shadowing 'type' isn't adviced.

        # Used to identify the resource by Arcs and Locators
        # https://www.xbrl.org/Specification/XBRL-2.1/REC-2003-12-31/XBRL-2.1-REC-2003-12-31+corrected-errata-2013-02-20.html#_3.5.3.8.2
        self.xlink_label = xlink_label
        # A role is optional, but it should distinguish its contents to other resources
        # https://www.xbrl.org/Specification/XBRL-2.1/REC-2003-12-31/XBRL-2.1-REC-2003-12-31+corrected-errata-2013-02-20.html#_3.5.3.8.3
        # we differentiate this by subclassing
        self.role = role
        # XML ID is also optional.
        self.id = id

    def __eq__(self, other):
        return self.xlink_label == other.xlink_label

    def __hash__(self):
        return hash(self.xlink_label)


class Label(Resource):
    """
    Label elements are resources which communicate human readable information on Elements.

    Both '2003' as generic labels are using this class
    https://www.xbrl.org/Specification/XBRL-2.1/REC-2003-12-31/XBRL-2.1-REC-2003-12-31+corrected-errata-2013-02-20.html#_5.2.2.2
    https://www.xbrl.org/specification/genericlabels/rec-2011-10-24/genericlabels-rec-2011-10-24.html
    """

    def __init__(
        self, xlink_label: str, linkRole: str, language: str, text: str, id=None
    ):
        super().__init__(xlink_label=xlink_label, role=linkRole, id=id)
        self.language = language
        self.text = text

    def __str__(self):
        return str(f"{self.xlink_label}")

    @property
    def linkRole(self):
        # Property to be backwards compatible
        return self.role

    def __eq__(self, other):
        return self.xlink_label == other.xlink_label


class Message(Label):
    """
    Messages allow for dynamic content in labels using XPATH expressions.

    """

    pass


class Reference(Resource):
    def __init__(
        self,
        id,
        xlink_label,
        role=None,
        name=None,
        number=None,
        issuedate=None,
        chapter=None,
        article=None,
        note=None,
        section=None,
        subsection=None,
        publisher=None,
        paragraph=None,
        subparagraph=None,
        clause=None,
        subclause=None,
        appendix=None,
        example=None,
        page=None,
        exhibit=None,
        footnote=None,
        sentence=None,
        uri=None,
        uridate=None,
    ):
        # Attributes are explicitly declared as per XBRL specification
        super().__init__(xlink_label=xlink_label, id=id, role=role)
        self.name = name
        self.number = number
        self.issuedate = issuedate
        self.chapter = chapter
        self.article = article
        self.note = note
        self.section = section
        self.subsection = subsection
        self.publisher = publisher
        self.paragraph = paragraph
        self.subparagraph = subparagraph
        self.clause = clause
        self.subclause = subclause
        self.appendix = appendix
        self.example = example
        self.page = page
        self.exhibit = exhibit
        self.footnote = footnote
        self.sentence = sentence
        self.uri = uri
        self.uridate = uridate

    def __str__(self):
        return f"Reference:  {self.name} label: {self.label}"

    def __eq__(self, other):
        return (
            self.xlink_label == other.xlink_label
            and self.name == other.name
            and self.number == other.number
            and self.issuedate == other.issuedate
            and self.chapter == other.chapter
            and self.article == other.article
            and self.note == other.note
            and self.section == other.section
            and self.subsection == other.subsection
            and self.publisher == other.publisher
            and self.paragraph == other.paragraph
            and self.subparagraph == other.subparagraph
            and self.clause == other.clause
            and self.subclause == other.subclause
            and self.appendix == other.appendix
            and self.example == other.example
            and self.page == other.page
            and self.exhibit == other.exhibit
            and self.footnote == other.footnote
            and self.sentence == other.sentence
            and self.uri == other.uri
            and self.uridate == other.uridate
        )

    def __hash__(self):
        return hash(self.xlink_label)


class Table:
    def __init__(
        self,
        id,
        element_type=None,
        parent_child_order=None,
        labels={},
        merge=None,
        is_abstract=False,
    ):
        self.id = id
        self.element_type = element_type
        self.parentChildOrder = parent_child_order
        self.merge = merge
        self.labels = labels
        self.abstract = is_abstract


class Loc:
    def __init__(self, href_path, href_id, label, type=None):
        self.href_path = href_path  # xlink:href (split '#')[0]
        self.href_id = href_id  # xlink:href (split '#')[1]
        self.label = label  # xlink:label
        self.type = type  # xlink:type

    def __eq__(self, other):
        if not isinstance(other, Loc):
            # don't attempt to compare against unrelated types
            return NotImplemented

        return self.url == other.url and self.href_path == other.href_path

    def __hash__(self):
        return hash(self.href_path, self.href_id)

    def __repr__(self):
        return f"Loc(href_path='{self.href_path}', href_id='{self.href_id}', label='{self.label}', type='{self.type}')"


class Arc:
    """
    An arc has at least a Arc type. Attributes depend on the type of arc.
    """

    def __init__(
        self, tag, arcrole, attributes=None, order=None, from_obj=None, to_obj=None
    ):

        self.arc_type = tag

        if arcrole is not None:
            self.arcrole = arcrole
        # Set to and from object to None, these attributes are filled in post discovery
        if isinstance(from_obj, list) or isinstance(to_obj, list):
            print("Saving a list instead of one object. This should not happen!")

        self.to_concept_obj = to_obj
        self.from_concept_obj = from_obj

        if attributes is not None:
            self.arcrole = attributes["{http://www.w3.org/1999/xlink}arcrole"]

            if "{http://www.w3.org/1999/xlink}from" in attributes:
                self._from_concept = attributes["{http://www.w3.org/1999/xlink}from"]

            if "{http://www.w3.org/1999/xlink}to" in attributes:
                self._to_concept = attributes["{http://www.w3.org/1999/xlink}to"]

        if self.from_concept_obj is None and not hasattr(self, "_from_concept"):
            logger.warning("Arc does not have a valid 'from' attribute!")

        if self.to_concept_obj is None and not hasattr(self, "_to_concept"):
            logger.warning("Arc does not have a valid 'to ' attribute!")

        """
        Generally, @order is an int, but in some cases, this also seems to be allowed to be a float:
        http://www.xbrl.org/specification/gnl/rec-2009-06-22/gnl-rec-2009-06-22.html
        """

        if order:  # Order is explicitly given
            self.order = float(order)
        elif attributes is not None:
            if "order" in attributes:  # Order is in attributes.
                # '{http://xbrl.org/2008/generic}arc'
                self.order = float(attributes["order"])

    @property
    def from_concept(self):
        if hasattr(self, "_from_concept"):
            return self._from_concept
        else:
            return self.from_concept_obj.id

    @property
    def to_concept(self):
        if hasattr(self, "_to_concept"):
            return self._to_concept
        elif self.to_concept_obj:
            return self.to_concept_obj.id
        else:
            return None

    # def __eq__(self, other):
    #     '''
    #     DEPRECATED?   EK: There is a typo in here that make it only check if the other object is a Loc.
    #                   Which means this function did not work properly but has not been noticed.
    #                   I suspect the program works as intended as is without the custom check?

    #     :param other:
    #     :return:
    #     '''
    #     if not isinstance(other, Loc):
    #         # don't attempt to compare against unrelated types
    #         return NotImplemented
    #
    #     return (
    #         self.arc_type == other.arc_type
    #         and self.arcrole == other.arcrole
    #         and self.from_concept_obj == other.from_concept_obj
    #         and self.to_concept_obj == other.to_concept_obj
    #     )

    def __str__(self):
        return f"{self.arc_type}: {self.from_concept} -> {self.to_concept}"


class DefinitionArc(Arc):
    def __init__(self, tag, arcrole, from_obj, to_obj):
        super(DefinitionArc, self).__init__(
            tag, arcrole=arcrole, from_obj=from_obj, to_obj=to_obj
        )


class PresentationArc(Arc):
    def __init__(
        self, tag, arcrole, from_obj, to_obj, order=None, preferred_label=None
    ):
        self.preferred_label = preferred_label
        self.order = order

        super(PresentationArc, self).__init__(
            tag, arcrole=arcrole, from_obj=from_obj, to_obj=to_obj
        )


class ReferenceArc(Arc):
    def __init__(self, tag, arcrole, from_obj, to_obj):
        super(ReferenceArc, self).__init__(
            tag, arcrole=arcrole, from_obj=from_obj, to_obj=to_obj
        )


class GenericArc(Arc):
    def __init__(
        self,
        tag,
        arcrole,
        from_obj=None,
        from_elem=None,
        to_obj=None,
        to_elem=None,
        order=None,
        name=None,
    ):

        if from_obj:
            self.from_concept_obj = from_obj
        else:
            self._from_concept = from_elem

        if to_obj:
            self.to_concept_obj = to_obj
        else:
            self._to_concept = to_elem

        self.order = order
        self.name = name

        super(GenericArc, self).__init__(
            tag=tag, arcrole=arcrole, from_obj=from_obj, to_obj=to_obj
        )


class RGS_datapoint(Resource):
    # todo: maybe a bit of an afterthought, but maybe we should have a parent class 'resource'
    #  although, if we really want to be fancy, we should also discover the complex XML types.

    def __init__(
        self,
        id,
        xlink_label,
        role,
        entrypoint,
        primary_qname,
        explicit_dimensions,
        typed_dimensions,
    ):

        super().__init__(xlink_label, role, id)
        self.id = id
        self.xlink_label = xlink_label
        self.role = role
        self.entrypoint = entrypoint
        self.primary_qname = primary_qname

        self.dimensions = []
        for dimension in explicit_dimensions:
            type = "explicit"
            dim = dimension.get(
                "{http://www.nltaxonomie.nl/rgs/2015/xbrl/rgs-syntax-extension}qname"
            )
            mem = dimension.get("member")
            container = dimension.get(
                "{http://www.nltaxonomie.nl/rgs/2015/xbrl/rgs-syntax-extension}container"
            )
            self.dimensions.append(
                {"type": type, "dim": dim, "mem": mem, "container": container}
            )
        for dimension in typed_dimensions:
            print("-typed dimension-")


class Severity(Resource):
    def __init__(
        self,
        xlink_label: str,
        role: str = None,
        id: str = None,
        severity: str = None,
        label: str = None,
    ):
        super().__init__(xlink_label=xlink_label, role=role, id=id)
        self.severity = severity
        self.label = label
