# Standard library imports
import logging
from pathlib import Path

from lxml import etree

from .classes import Loc
from .subparser.formulas.classes.variables import (
    GeneralVariable,
    FactVariable,
    Parameter,
)
from .subparser.formulas.variables import get_filters
from .subparser.generic_link import discover_generic_link
from .subparser.references import (
    discover_linkbaseref,
    discover_locators,
    discover_arcs,
    discover_label_arcs,
    get_reference_element,
    discover_reference_arcs,
    discover_arcroleref,
)
from .subparser.resources import discover_labels
from .subparser.generic_arc import (
    process_generic_arc,
    process_variable_arc,
    process_labels,
    process_rgs_arc,
    process_messages,
)

from .subparser.tables.arcs import process_table_arcs

# other sbrnl_modules imports
from ..parsetools.classes import WWWPath
from ..parsetools.file_handlers import return_file_contents
from ..parsetools.helpers import clean_url
from ..parsetools.taxonomy_package import Entrypoint
from ..schema.classes import RoleType

logger = logging.getLogger("parser.linkbase")
"""
This file contains the loop that is called for every linkbase (XML) file to be parsed
"""


def parse_known_linkbase(url, dts, ep, discoverable_files, discovered_filed):
    """
    Parse a linkbase that has already been parsed before. This can be the case when loading multiple entrypoints,
    but also when a file is linked from multiple parts within a DTS.

    :param url:
    :param dts:
    :param ep:
    :return:
    """
    pass  # todo: go through locs to figure out usage of elements in EP (maybe per presentationLink?)
    #  But what about generic?


def pre_parse_linkbase(url, dts, ep, all_files, validator=None):
    """
    Parses a linkbase that is found within a schema.

    :param url:
    :param dts:
    :param ep:
    :param all_files:
    :return:
    """

    try:
        file = return_file_contents(url, all_files)  # Get the file contents
        if not file:
            return None
    except FileNotFoundError as er:
        print(er)
        return None

    linkbase = etree.fromstring(
        bytes(file, encoding="utf-8")
    )  # Parse it into an element tree

    if not isinstance(ep, Entrypoint):
        #  todo: EK: This does not work for an Entrypoint object and frankely this is also a bit of a crude way for checking
        #   if an instance needs to be checker. It seems to check if a schema is part of the right 'domain'. But
        #   EP's for a domain like KVK also uses schema's from other domains.
        if validator and url.parent.parent == ep.parent.parent:
            validator.validate_linkbase(url=url, linkbase=linkbase, file=file)
    # else:
    #     validator.validate_linkbase(url=url, linkbase=linkbase, file=file)

    # Look for role references
    # print(dts.namespaces)
    linkbaserefs = linkbase.xpath(
        "//link:linkbase/link:roleRef", namespaces=dts.namespaces
    )

    schema_locations = linkbase.get(
        "{http://www.w3.org/2001/XMLSchema-instance}schemaLocation"
    )
    if schema_locations:
        for schema_location in schema_locations.split(" "):
            if (
                schema_location is None
                or schema_location == " "
                or schema_location == ""
            ):
                pass
            elif schema_location.endswith(".xsd"):
                if schema_location.startswith("http"):
                    yield clean_url(schema_location)

                elif isinstance(url, Path):
                    yield clean_url(url=schema_location, abs_path=url.parent.__str__())

                elif isinstance(url, WWWPath):
                    yield clean_url(url=schema_location, abs_path=url.parent)

    # Get the path of the schema file to discover this right away.
    for linkbaseref in discover_linkbaseref(linkbaserefs, url, dts):

        if linkbaseref["path"] not in dts.schemas:
            # Yield a schema the moment it is found, so it can be discovered before continuing.
            yield linkbaseref["path"]

            """
            After yielding, the referred schema should be discovered and the link role known.
            
            Now. this is a bit of a hack, but here linkbases that are directly referenced by the EP
            are replaced when the linkrole is known. With this information we can recreate the taxonomy later on.
            """

        # Sometimes roleRefs are used that are not uri's
        # In that case, they probably also don't point to a linkrole uri.
        if linkbaseref["uri"] in dts.roletypes:
            roleref = dts.roletypes[linkbaseref["uri"]]
            if str(url) in dts.directly_referenced_linkbases_str:

                # Check if the linkrole has not been added before (could be already referred by another file)

                if roleref not in dts.directly_referenced_linkbases:
                    # Add it to the non-string list
                    dts.directly_referenced_linkbases.append(roleref)
            else:
                logger.info(
                    f"Linkrole {linkbaseref['uri']} is not a directly referenced linkbase file"
                )
                if roleref not in dts.indirectly_referenced_linkbases:
                    dts.indirectly_referenced_linkbases.append(roleref)

    arcroleRefs = linkbase.xpath(
        "//link:linkbase/link:arcroleRef", namespaces=dts.namespaces
    )
    for arcroleRef in discover_arcroleref(arcroleRefs, url=url):
        if arcroleRef["path"] not in dts.schemas:
            yield arcroleRef["path"]

    # Only take out the linkbase file after all schema references have been processed
    if url in dts.directly_referenced_linkbases_str:
        index = dts.directly_referenced_linkbases_str.index(url)
        dts.directly_referenced_linkbases_str.pop(index)

    loc_generator = discover_locators(
        linkbase.xpath("//link:loc", namespaces=dts.namespaces), url
    )
    # these locators will be of no use, we store the on the key xlink_label,
    # which is not unique within a dts per se.

    for loc in loc_generator:
        if isinstance(loc, Loc):
            # If a Loc is returned, add it to the DTS
            dts.locs[loc.label] = loc
        else:
            # if a path is returned, pass it on to the main parsing process
            yield loc


def parse_linkbase(url, dts, ep, all_files, validator=None):
    """
    Discover linkbases
    """

    try:
        file = return_file_contents(url, all_files)  # Get the file contents
        if not file:
            return None
    except FileNotFoundError as er:
        print(er)
        return None
    linkbase = etree.fromstring(
        bytes(file, encoding="utf-8")
    )  # Parse it into an element tree

    if not isinstance(ep, Entrypoint):
        if validator and url.parent.parent == ep.parent.parent:
            validator.validate_linkbase(url=url, linkbase=linkbase, file=file)
    # else:
    #     validator.validate_linkbase(url=url, linkbase=linkbase, file=file)

    # add namespaces to the DTS
    if hasattr(linkbase, "nsmap"):

        dts.namespaces.update(linkbase.nsmap)
        if None in dts.namespaces.keys():
            del dts.namespaces[None]  # drop empty namespace if it exists

    # get rolerefs, call schema parser if a new schema is found.
    linkbaserefs = linkbase.xpath(
        "//link:linkbase/link:roleRef", namespaces=dts.namespaces
    )

    # Get the path of the schema file to discover this right away.
    for linkbaseref in discover_linkbaseref(linkbaserefs, url, dts):

        if linkbaseref["path"] not in dts.schemas:
            id = None
            if hasattr(linkbaseref["path"], "name") and "#" in linkbaseref["path"].name:
                id = linkbaseref["path"].name.split("#")[1]

            if linkbaseref["uri"] not in dts.roletypes.keys():
                dts.roletypes[linkbaseref["uri"]] = RoleType(
                    roleURI=linkbaseref["uri"].__str__(), parent_dts=dts, id=id
                )
            elif isinstance(dts.roletypes[linkbaseref["uri"]], str):
                dts.roletypes[linkbaseref["uri"]] = RoleType(
                    roleURI=linkbaseref["uri"], parent_dts=dts, id=id
                )

    """
    Get referenceLink
    """
    for referenceLink in linkbase.xpath(
        "//link:linkbase/link:referenceLink", namespaces=dts.namespaces
    ):
        uri = referenceLink.get("{http://www.w3.org/1999/xlink}role")
        local_references = []
        # First, discover the references
        for reference in get_reference_element(elems=referenceLink):
            if not uri in dts.roletypes.keys():
                logger.error(f"{uri} in file {url.name}: linkrole not found in DTS")

            dts.roletypes[uri].add_reference(reference=reference)
            local_references.append(reference)

        # After, parse the arcs
        for arc in discover_reference_arcs(
            referenceLink.xpath("link:referenceArc", namespaces=dts.namespaces),
            dts=dts,
            references=local_references,
        ):
            dts.roletypes[uri].arcs.append(arc)

    """
    Get definitionLink
    """
    for definitionLink in linkbase.xpath(
        "//link:linkbase/link:definitionLink", namespaces=dts.namespaces
    ):

        uri = definitionLink.get("{http://www.w3.org/1999/xlink}role")
        if not uri in dts.roletypes.keys():

            logger.error(f"{uri} in file {url.name}: linkrole not found in DTS")

        # Get the arcs within the presentationLinks
        for arc in discover_arcs(
            definitionLink.xpath("link:definitionArc", namespaces=dts.namespaces),
            dts=dts,
        ):
            dts.roletypes[uri].arcs.append(arc)

    """
    Get presentationLink
    """
    for presentationLink in linkbase.xpath(
        "//link:linkbase/link:presentationLink", namespaces=dts.namespaces
    ):

        uri = presentationLink.get("{http://www.w3.org/1999/xlink}role")
        if not uri in dts.roletypes.keys():
            logger.error(f"{uri} in file {url.name}: linkrole not found in DTS")

        # Get the arcs within the presentationLinks
        for arc in discover_arcs(
            presentationLink.xpath("link:presentationArc", namespaces=dts.namespaces),
            dts=dts,
        ):
            dts.roletypes[uri].arcs.append(arc)

    """
    Get labelLink
    """
    for label_link in linkbase.xpath(
        "//link:linkbase/link:labelLink", namespaces=dts.namespaces
    ):
        uri = label_link.get("{http://www.w3.org/1999/xlink}role")
        if not uri in dts.roletypes.keys():

            logger.error(f"{uri} in file {url.name}: linkrole not found in DTS")

        # First, get the labels
        local_labels = {}
        new_local_labels = []
        for label in discover_labels(
            label_link.xpath("link:label", namespaces=dts.namespaces)
        ):
            # huh? x-link-label is not a unique key!!
            local_labels[label.xlink_label] = label
            new_local_labels.append(label)
            dts.labels[f"{label.xlink_label}|{label.role}"] = label

        # Second, get the arcs and create links between objects
        for arctype in ["link:labelArc"]:
            for arc in discover_label_arcs(
                label_link.xpath(arctype, namespaces=dts.namespaces),
                dts=dts,
                labels=new_local_labels,
            ):
                dts.roletypes[uri].arcs.append(arc)

    """
    Get referenceLink
    """
    # for reference_link in linkbase.xpath(
    #     "//link:linkbase/link:referenceLink", namespaces=dts.namespaces
    # ):
    #     uri = reference_link.get("{http://www.w3.org/1999/xlink}role")
    #     if not uri in dts.roletypes.keys():
    #
    #         logger.error(f"{uri} in file {url.name}: linkrole not found in DTS")
    #
    #     # Get all references and add them to the DTS (or should this be added to the linkrole,
    #     # and the dts should access the references indirectly through its linkroles
    #     for reference in get_reference_element(
    #         reference_link.xpath("link:reference", namespaces=dts.namespaces)
    #     ):
    #         dts.roletypes[uri].add_reference(reference=reference)

    # Get locators
    # every unique locator is yielded
    loc_generator = discover_locators(
        linkbase.xpath("//link:loc", namespaces=dts.namespaces), url
    )

    for loc in loc_generator:
        if isinstance(loc, Loc):
            # If a Loc is returned, add it to the DTS
            duplicate = dts.locs.get(loc.label)
            if not duplicate:
                dts.locs[loc.label] = loc
            elif (
                duplicate.href_path == loc.href_path
                and duplicate.href_id == loc.href_id
            ):
                continue
            else:
                # so here labels and presentation possibly get mixed up
                dts.locs[f"{loc.label}|{loc.href_id}"] = loc

    # Discover things from generic links, but only if the 'gen' namespace is already discovered
    if "gen" in linkbase.nsmap.keys():
        gen_elems = linkbase.xpath(
            "//link:linkbase/gen:link", namespaces=dts.namespaces
        )

        gen_arcs = []
        table_arcs = []
        variable_arcs = []
        rgs_arcs = []
        parameters = []
        variables = []
        labels = []
        messages = []
        other_elements = []

        """
        gen:link/arc (and possibly also other arcs) needs to be processed last,
         as they will probably reference other elements from the file
         
        Order of processing:
        1.  Labels and messages
        2.  Everything else
        3.  Arc's
        
        
        """

        for gen_extended_link in gen_elems:
            uri = gen_extended_link.get("{http://www.w3.org/1999/xlink}role")
            linkrole = dts.roletypes[uri]

            for elem in gen_extended_link:

                if elem.tag == "{http://xbrl.org/2008/generic}arc":
                    gen_arcs.append(elem)

                elif elem.tag in [
                    "{http://xbrl.org/2014/table}breakdownTreeArc",
                    "{http://xbrl.org/2014/table}definitionNodeSubtreeArc",
                    "{http://xbrl.org/2014/table}tableBreakdownArc",
                    "{http://xbrl.org/2014/table}tableParameterArc",
                ]:
                    table_arcs.append(elem)
                elif elem.prefix == "table":
                    if elem not in linkrole.xml_table_elems:
                        # XML elements are added to linkrole to parse after all files are loaded

                        linkrole.xml_table_elems.append(elem)
                    # table_arcs.append(elem)
                elif elem.tag in [
                    "{http://xbrl.org/2008/variable}variableArc",
                    "{http://xbrl.org/2008/variable}variableSetFilterArc",
                ]:
                    variable_arcs.append(elem)
                elif elem.tag == "{http://xbrl.org/2008/label}label":
                    labels.append(elem)
                elif elem.tag == "{http://xbrl.org/2010/message}message":
                    messages.append(elem)

                elif elem.tag in ["{http://xbrl.org/2008/variable}parameter"]:
                    parameters.append(elem)
                elif elem.tag in [
                    "{http://xbrl.org/2008/variable}factVariable",
                    "{http://xbrl.org/2008/variable}generalVariable",
                ]:
                    variables.append(elem)
                elif elem.tag in [
                    "{http://www.nltaxonomie.nl/rgs/2015/xbrl/rgs-syntax-extension}many2oneArc",
                    "{http://www.nltaxonomie.nl/rgs/2015/xbrl/rgs-syntax-extension}one2oneArc",
                    "{http://www.nltaxonomie.nl/rgs/2015/xbrl/rgs-syntax-extension}one2manyArc",
                ]:
                    rgs_arcs.append(elem)

                else:
                    other_elements.append(elem)

            """Step 1: labels and messages"""
            for label in labels:
                process_labels(label, linkrole)
            for message in messages:
                process_messages(message, linkrole)

            """Step 1.1: Parameters"""
            # todo: EK: find out if this should be done separately or maybe it should just be part of process_variable_arc.
            #  propably depends on how specific these parameters are.
            #  alternatively I could put "tableParameterArc" to discover in variables, rather than Arc's.

            for parameter in parameters:
                xlink_label = parameter.get("{http://www.w3.org/1999/xlink}label")
                if not linkrole.get_resource(
                    xlink_label=xlink_label, resource_type="parameters"
                ):
                    linkrole.resources["parameters"].append(
                        Parameter(
                            id=parameter.get("id"),
                            xlink_label=xlink_label,
                            as_datatype=parameter.get("as"),
                            name=parameter.get("name"),
                            select=parameter.get("select"),
                            required=parameter.get("required"),
                        )
                    )

            """ Step 1.2: Variables"""
            for variable in variables:

                if variable.tag == "{http://xbrl.org/2008/variable}generalVariable":
                    linkrole.resources["variables"].append(
                        GeneralVariable(
                            id=variable.get("id"),
                            xlink_label=variable.get(
                                "{http://www.w3.org/1999/xlink}label"
                            ),
                            bind_as_sequence=variable.get("bindAsSequence"),
                            select=variable.get("select"),
                        )
                    )
                if variable.tag == "{http://xbrl.org/2008/variable}factVariable":
                    variable_label = variable.get("{http://www.w3.org/1999/xlink}label")

                    variable = FactVariable(
                        id=variable.get("id"),
                        xlink_label=variable_label,
                        matches=variable.get("matches"),
                        bind_as_sequence=variable.get("bindAsSequence"),
                        nils=variable.get("nils"),
                        fallback_value=variable.get("fallbackValue"),
                        filters=[],
                    )
                    linkrole.resources["variables"].append(variable)

            """Step 2: everything else"""
            for elem in other_elements:
                discover_generic_link(dts=dts, elem=elem, linkrole=linkrole)

            """Step 3: Arc's"""
            for arc in gen_arcs:
                process_generic_arc(arc, linkrole, dts)
            for rgs_arc in rgs_arcs:
                process_rgs_arc(rgs_arc, linkrole, dts)

            """Step 4: Variables"""
            for arc in variable_arcs:
                process_variable_arc(arc, linkrole, linkbase=linkbase, dts=dts)

            """Step 4.1: Filters, these are dependend on VarArcs"""
            for variable in linkrole.resources["variables"]:
                filters = get_filters(
                    linkbase=linkbase,
                    variable_label=variable.xlink_label,
                    linkrole=linkrole,
                )
                if len(filters) > 0:
                    variable.filters.extend(filters)

            # Tables are done after discovery
            for arc in table_arcs:
                process_table_arcs(arc, linkrole, dts, url)
