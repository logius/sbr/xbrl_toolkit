import logging
from discover_xbrl.sbrnl_xbrl_parser.linkbase.classes import Loc

logger = logging.getLogger("parser.linkbase")


def follow_local_loc(dictionary, loc):
    # for key in dictionary:
    #    print(key, end='->')
    #    print(type(dictionary[key]), end=' ')
    #    print(dictionary[key].__dict__)
    # print(f"Looking for: {loc.__dict__}")
    if loc.href_id in dictionary.keys():
        # print(f"Returning: {loc.href_id}")
        # print(dictionary[loc.href_id].__dict__)
        return dictionary[loc.href_id]
    else:
        return None


def follow_loc_with_element_lists(element_type_lists, loc):
    """
    Tries to find an element within multiple dictionaries.

    :param element_type_lists:
    :param loc:
    :return:
    """

    for element_type_list in element_type_lists:
        # First try to match to an already discovered concept

        concept = follow_local_loc(element_type_list, loc)

        if concept:
            return concept

    return False


def get_locator_from_arc_node(arc, to=False):
    """
    Finds the root of the document, and searches, inside that document for the locator with that label,
    labels are not unique inside a DTS, so you can re-fetch them f rom a dict with the label as the key
    """
    query = (
        ".//{http://www.xbrl.org/2003/linkbase}loc[@{http://www.w3.org/1999/xlink}label='%s']"
        % arc.get("{http://www.w3.org/1999/xlink}from")
    )
    if to:
        query = (
            ".//{http://www.xbrl.org/2003/linkbase}loc[@{http://www.w3.org/1999/xlink}label='%s']"
            % arc.get("{http://www.w3.org/1999/xlink}to")
        )

    for ancestor in arc.iterancestors():
        locs = ancestor.findall(query)
        if locs:
            if len(locs) > 1:
                print(f"Multiple hits! {query}")
            path, loc_href_concept = (
                locs[0].attrib["{http://www.w3.org/1999/xlink}href"].split("#", 1)
            )
            # Cast it to a Loc-object,because we expect it here
            loc = Loc(
                label=locs[0].get("{http://www.w3.org/1999/xlink}label"),
                type=locs[0].get("{http://www.w3.org/1999/xlink}type"),
                href_id=loc_href_concept,
                href_path=path,
            )
            return loc
    print(f"{query} geen Locator gevonden")
