# Standard library imports
import logging

from lxml import etree

from .classes import RoleType, ArcRoleType
from .subparser.references import discover_linkbase_refs
from .subparser.resources import discover_elements, discover_itemtype
from ..parsetools.classes import WWWPath

# other sbrnl_modules imports
from ..parsetools.file_handlers import return_file_contents
from ..parsetools.helpers import clean_url
from ..parsetools.taxonomy_package import Entrypoint

logger = logging.getLogger("parser.schema")


def parse_known_schema(url, dts, ep, discoverable_files, discovered_files):
    """
    Parse a schema that has already been parsed before for this EP.
    :param url:
    :param dts:
    :param ep:
    :return:
    """
    pass


def parse_schema(
    url,
    dts,
    ep,
    discoverable_files,
    discovered_files,
    all_files,
    schema=None,
    validator=None,
):
    """
    This is the function that parses all information from XBRL schema files (XSD's).

    :param url:
    :param dts:
    :param ep:
    :param discoverable_files:
    :param discovered_files:
    :return:
    """

    # First get the file contents and transform it into an lxml elementTree, but only if schema is not already given
    if not schema:
        try:
            file = return_file_contents(url, all_files)
            if file:
                schema = etree.fromstring(bytes(file, encoding="utf-8"))
            else:
                # We moeten hier iets doen, terugkeren naar de buitenste procedure
                # en vertellen dat dit nooit gaat werken, er missen bestanden.
                pass
        except FileNotFoundError as er:
            print(er)
            return

    if not isinstance(ep, Entrypoint):
        # todo: EK: This does not work for an Entrypoint object and frankely this is also a bit of a crude way for checking
        #  if an instance needs to be checker. It seems to check if a schema is part of the right 'domain'. But
        #  EP's for a domain like KVK also uses schema's from other domains.
        if validator and url.parent.parent == ep.parent.parent:
            validator.validate_schema(url=url, schema=schema, file=file)
    # else:
    #     validator.validate_schema(url=url, schema=schema, file=file)

    # If there are namespaces, these are added to the dictionary. Without this it is not possible to execute queries.
    if hasattr(schema, "nsmap"):
        dts.namespaces.update(schema.nsmap)
        if None in dts.namespaces.keys():
            del dts.namespaces[None]  # drop empty namespace if it exists
    else:
        for elem in schema.getroot():
            dts.namespaces.update(elem.nsmap)
            if None in dts.namespaces.keys():
                del dts.namespaces[None]  # drop empty namespace if it exists

    # Get the correct schema prefix
    schema_prefix = [
        k for k, v in schema.nsmap.items() if v == "http://www.w3.org/2001/XMLSchema"
    ]
    if len(schema_prefix) > 0:
        # If we found a schema prefix, we can 'unpack' this list
        schema_prefix = schema_prefix[0]
    else:
        # Explicitly set to None if not found
        schema_prefix = None

    linkbase_prefix = [
        k for k, v in schema.nsmap.items() if v == "http://www.xbrl.org/2003/linkbase"
    ]
    if len(linkbase_prefix) > 0:
        linkbase_prefix = linkbase_prefix[0]
    else:
        # Explicitly set to None if not found
        linkbase_prefix = None

    targetNamespace = schema.get("targetNamespace")
    # call the function that parses comples and simple (item) types
    if schema_prefix is not None:
        xpath_complex_type = schema.xpath(
            f"//*[local-name() = 'schema']/*[local-name() = 'complexType']",
            namespaces=dts.namespaces,
        )
        xpath_simple_type = schema.xpath(
            f"//*[local-name() = 'schema']/*[local-name() = 'simpleType']",
            namespaces=dts.namespaces,
        )
    else:
        # If somehow we didn't get a schema prefix, we get two empty lists
        xpath_complex_type = []
        xpath_simple_type = []

        for elem in schema:
            # loop though instead of using an xpath statement as these elements likely have a None prefixed namespace
            if elem.tag == "{http://www.w3.org/2001/XMLSchema}complexType":
                xpath_complex_type.append(elem)
            elif elem.tag == "{http://www.w3.org/2001/XMLSchema}simpleType":
                xpath_simple_type.append(elem)

    for itemtype_queries in [xpath_simple_type, xpath_complex_type]:

        for itemtype_query in itemtype_queries:

            domain = schema.get("id")
            if not domain:
                # Try to get the domain by checking by which namespace prefix corresponds with the targetNamespace

                for k, v in schema.nsmap.items():
                    if v == targetNamespace:
                        domain = k

            # Discover itemType or Enumeration
            itemtype = discover_itemtype(
                itemtype_query, domain, namespaces=dts.namespaces, source=url
            )
            dts.datatypes.append(itemtype)

    xpath_elements = schema.xpath(
        f"//*[local-name() = 'schema']/*[local-name() = 'element']",
        namespaces=dts.namespaces,
    )

    for concept in discover_elements(
        xpath_elements, target_namespace=targetNamespace, source=url
    ):

        # Every concept/element found is returned and saved if it is not yet known.
        if concept.id not in dts.concepts.keys():

            # Try to add the referenced concept
            if concept.typedDomainRef:
                if str(concept.typedDomainRef).split("#")[1] in dts.concepts.keys():
                    concept.typedDomainRef_obj = dts.concepts[
                        str(concept.typedDomainRef).split("#")[1]
                    ]
                else:
                    # Yield the referenced schema so it can be discovered before trying again.
                    yield clean_url(
                        str(concept.typedDomainRef).split("#")[0], url.parent
                    )

                    # Try again after yielding
                    if str(concept.typedDomainRef).split("#")[1] in dts.concepts.keys():
                        concept.typedDomainRef_obj = dts.concepts[
                            str(concept.typedDomainRef).split("#")[1]
                        ]
                    else:
                        logger.warning(
                            f"Could not find typed domain object {concept.typedDomainRef} even after discovering referenced schema"
                        )

            dts.concepts[concept.id] = concept

    # call the function that handles imports
    xpath_imports = schema.xpath(
        f"//*[local-name() = 'schema']/*[local-name() = 'import']",
        namespaces=dts.namespaces,
    )

    for import_schema in xpath_imports:

        # get the attribute schemaLocation for each import line
        schemaLocation = import_schema.get("schemaLocation")

        # If imports are found, they need to be yielded as WWWPath or a normal Path
        if schemaLocation:
            if "http" in schemaLocation:
                yield clean_url(WWWPath(schemaLocation))
            else:
                # Need to yield the absolute url as otherwise the relative path from this scheme is incorrect.
                yield clean_url(schemaLocation, url.parent)

    # call the function that handles linkbasereferences. These are links towards XML files.
    # They can appear as part of an entrypoint file, but also in other schema's.
    # For instance, to link elements to labels of different types and languages.

    xpath_linkbaseref = schema.xpath(
        f"//*[local-name() = 'schema']/*[local-name() = 'annotation']/*[local-name() = 'appinfo']/*[local-name() = 'linkbaseRef']",
        namespaces=dts.namespaces,
    )

    for linkbaseref in discover_linkbase_refs(xpath_linkbaseref, url):
        # If file has not been discovered for this EP yet, add this to the list
        if (
            linkbaseref not in discovered_files
            and linkbaseref not in discoverable_files
        ):
            discoverable_files.append(linkbaseref)

            # Only add it to the DTS if this has not been done before
            if linkbaseref not in dts.linkbases:
                dts.linkbases.append(linkbaseref)

    # Call the function that handles roleTypes
    xpath_linkRoleType = schema.xpath(
        f"//*[local-name() = 'schema']/*[local-name() = 'annotation']/*[local-name() = 'appinfo']/*[local-name() = 'roleType']",
        namespaces=dts.namespaces,
    )
    for roletype in discover_link_roletype(xpath_linkRoleType):
        if roletype.roleURI not in dts.roletypes.keys():
            roletype.parent_dts = dts
            dts.roletypes[roletype.roleURI] = roletype

    # Get arcroleTypes
    xpath_linkarcRoleType = schema.xpath(
        f"//*[local-name() = 'schema']/*[local-name() = 'annotation']/*[local-name() = 'appinfo']/*[local-name() = 'arcroleType']",
        namespaces=dts.namespaces,
    )

    for arcroletype in discover_link_arcroletype(xpath_linkarcRoleType):
        if arcroletype.roleURI not in dts.arcroletypes.keys():

            dts.arcroletypes[arcroletype.roleURI] = arcroletype

    # When all information is parsed, save the url to the list of known schema's for future reference.
    dts.schemas.append(url)


def discover_link_roletype(link_roletype):
    """
    Discovers linkRoles. Takes one XML element as input, yields a RoleType element.

    :param link_roletype:
    :return:
    """

    for linkRole in link_roletype:  # List with all <link:roleType>'s
        if "id" in linkRole.attrib:
            id = linkRole.attrib["id"]
        else:
            id = None

        roleURI = linkRole.get("roleURI")
        used_on = []
        definition = ""

        # Get all XML available sub elements of the roleType and add those to the object.
        for subElem in linkRole:
            if subElem.tag == "{http://www.xbrl.org/2003/linkbase}definition":
                definition = subElem.text
            elif subElem.tag == "{http://www.xbrl.org/2003/linkbase}usedOn":
                used_on.append(subElem.text)

        yield RoleType(roleURI, id=id, definition=definition, used_on=used_on)


def discover_link_arcroletype(link_arcroletype):
    """
    Discovers custom arcRoles. Takes one XML element as input, yields a RoleType element.

    :param link_roletype:
    :return:
    """

    for arclinkRole in link_arcroletype:  # List with all <link:roleType>'s
        if "id" in arclinkRole.attrib:
            id = arclinkRole.attrib["id"]
        else:
            id = None

        # https://www.xbrl.org/Specification/XBRL-2.1/REC-2003-12-31/XBRL-2.1-REC-2003-12-31+corrected-errata-2013-02-20.html#_5.1.4.3
        link_cycles_allowed = arclinkRole.get("cyclesAllowed")

        roleURI = arclinkRole.get("arcroleURI")
        used_on = []
        definition = ""

        # Get all XML available sub elements of the roleType and add those to the object.
        for subElem in arclinkRole:
            if subElem.tag == "{http://www.xbrl.org/2003/linkbase}definition":
                definition = subElem.text
            elif subElem.tag == "{http://www.xbrl.org/2003/linkbase}usedOn":
                used_on.append(subElem.text)

        yield ArcRoleType(
            roleURI,  # required
            id=id,  # optional
            definition=definition,  # optional
            used_on=used_on,  # list of zero or more
            cycles_allowed=link_cycles_allowed,  # required
        )
