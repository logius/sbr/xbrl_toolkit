import logging
import re

logger = logging.getLogger("parser.schema")


class Concept:
    """
    A Concept is an element within a DTS that has an ID.
    """

    def __init__(
        self,
        id,
        name,
        nillable,
        balance=None,
        periodType=None,
        substitutionGroup=None,
        element_type=None,
        typedDomainRef=None,
        labels=None,
        is_abstract=False,
        ns_prefix=None,
        namespace=None,
        source=None,
    ):
        if name is None:
            logger.error(
                f"Concept with id '{id}' didn't get a correct name, this is not allowed!"
            )

        if not re.match("^[a-zA-Z0-9\-_]+$", name):
            logger.error(f"Illegal characters in concept name: '{name}'")

        """
        Defines a concept with XML element as input
        """
        self.id = id
        self.name = name
        self.nillable = nillable  # tuple
        # self.balance = balance
        self.substitutionGroup = substitutionGroup  # tuple
        self.periodType = periodType  # tuple
        if labels is None:
            self.labels = {}
        else:
            self.labels = labels
        self.element_type = element_type

        if isinstance(typedDomainRef, Concept):
            self.typedDomainRef_obj = typedDomainRef
            self.typedDomainRef = typedDomainRef
        else:
            self.typedDomainRef_obj = None
            self.typedDomainRef = typedDomainRef

        self.references = []
        self._source = source
        self.namespace = namespace

        if ns_prefix:
            self.ns_prefix = ns_prefix

        # Balance is optional
        if balance:
            self.balance = balance

        # @abstract is optional and has to be converted to boolean, if a boolean is not already passed
        if isinstance(is_abstract, bool):
            self.is_abstract = is_abstract
        elif is_abstract in ["false", "False"]:
            self.is_abstract = False
        elif is_abstract in ["true", "True"]:
            self.is_abstract = True
        elif is_abstract is None:
            self.is_abstract = None
        else:
            logger.error(f"@abstract of elem {name} is not recognized")

    def __eq__(self, other):
        if not isinstance(other, Concept):
            # don't attempt to compare against unrelated types
            return NotImplemented

        return self.name == other.name and self.namespace == other.namespace

    def __key(self):
        return (self.name, self.namespace)

    def __hash__(self):
        return hash(self.__key())

    def __str__(self):
        if hasattr(self, "id"):
            return self.id
        else:
            return self.name

    def __repr__(self):
        return f"Concept(name='{self.name}', id='{self.id}')'"

    def get_label(self, language, role="http://www.xbrl.org/2003/role/label"):
        if [
            label
            for label in self.labels.values()
            if label.linkRole == role and label.language == language
        ]:
            return [
                label
                for label in self.labels.values()
                if label.linkRole == role and label.language == language
            ]
        else:
            return None

    @property
    def _taxonomy_domain(self):
        return str(self.id).split("_")[0]

    @property
    def _name_parts(self):
        """
        Returns the parts a concept name is made of, in lower case.
        :return:
        """
        part_list = []
        parts = re.findall("[A-Z][^A-Z]*", str(self.name).split("-")[-1])
        for part in parts:
            part_list.append(part.lower())
        return part_list


class EnumerationChoice:
    def __init__(self, id, value, labels=None):
        self.id = id
        self.value = value
        if labels:
            self.labels = labels
        else:
            self.labels = []

    def __str__(self):
        return self.id


class ItemType:
    """
    An itemType or Enumeration object
    """

    def __init__(
        self,
        id,
        name,
        domain,
        base=None,
        inheritance_type=None,
        enumeration_type=None,
        enumeration_length=None,
        totalDigits=None,
        fraction_digits=None,
        min_inclusive=None,
        max_inclusive=None,
        min_length=None,
        max_length=None,
        restriction_pattern=None,
        enumerations=None,
        labels=None,
        source=None,
    ):
        self.id = id
        self.name = name
        self.base = base
        self.inheritance_type = inheritance_type
        self.restriction_type = enumeration_type
        self.totalDigits = totalDigits
        self.restriction_length = enumeration_length
        self.restriction_pattern = restriction_pattern
        self.restriction_max_length = max_length
        self.restriction_min_length = min_length
        self.fraction_digits = fraction_digits
        self.min_inclusive = min_inclusive
        self.max_inclusive = max_inclusive
        self.enumerations = enumerations
        if labels:
            self.labels = labels
        else:
            self.labels = []
        self.domain = domain
        self._source = source

    def __str__(self):
        return self.name

    @property
    def _name_parts(self):
        """
        Returns the parts a concept name is made of, in lower case.
        :return:
        """
        part_list = []

        name = str(self.name).strip("ItemType")

        parts = re.findall("[A-Z][^A-Z]*", str(name).split(":")[-1])
        for part in parts:
            part_list.append(part.lower())
        return part_list

    # @property
    # def _taxonomy_domain(self):
    #     return str(self.id).split(":")[0]


class RoleType:
    """
    Gives semantic meaning to a set of Arcs and the elements they refer to
    """

    def __init__(
        self,
        roleURI,
        hypercube=None,
        id=None,
        definition=None,
        used_on=None,
        parent_dts=None,
    ):
        if not isinstance(roleURI, str):
            logger.warning(f"roleuri is not a string")

        self.id = id
        self.roleURI = roleURI
        self.definition = definition
        if used_on is not None:
            self.used_on = used_on
        else:
            self.used_on = []

        self.labels = {}  # Labels linked to the LR via an arc.
        self.element_labels = {}
        # Collection of labels of elements belonging to this ELR.

        resources = {}
        for resource_type in [
            "variables",
            "parameters",
            "table",
            "assertions",
            "severities",
        ]:
            resources[resource_type] = []
        self.resources = resources

        self.tables = []
        self.references = []

        self.arcs = []
        self.locs = []
        self.xml_table_elems = []

        self.hypercube = hypercube
        self.presentation_hierarchy = None
        self.definition_hierarchy = None

        self.parent_dts = parent_dts

    def get_table(self, id):
        for table in self.tables:
            if table.id == id:
                return table
        return None

    def add_table(self, table):
        if table not in self.tables:
            self.tables.append(table)
            return True
        else:
            return False

    def get_filter(self, id):
        for filter in self.filters:
            if filter.id == id:
                return filter
        return None

    def add_filter(self, filter):
        if filter not in self.filters:
            self.filters.append(filter)
            return True
        else:
            return False

    def get_reference(self, id):
        for reference in self.references:
            if reference.id == id:
                return reference
        return None

    def add_reference(self, reference):
        if reference not in self.references:
            self.references.append(reference)
            return True
        else:
            return False

    def get_resource(self, xlink_label, resource_type=None):

        if resource_type is not None:
            for resource in self.resources[resource_type]:
                if resource.xlink_label == xlink_label:
                    return resource

        else:
            for key in self.resources.keys():
                for resource in self.resources[key]:
                    if resource.xlink_label == xlink_label:
                        return resource
        return None

    def add_variable(self, variable):
        if variable not in self.resources["variables"]:
            self.resources["variables"].append(variable)
            return True
        else:
            return False

    def get_assertion(self, id):

        for assertion in self.resources["assertions"]:
            if assertion.id == id:
                return assertion
            elif assertion.xlink_label == id:
                return assertion

        return None

    def get_arc(self, id):
        for arc in self.arcs:
            if arc.id == id:
                return arc
        return None

    def add_arc(self, arc):
        if arc not in self.arcs:
            self.arcs.append(arc)
            return True
        else:
            return False

    def get_dimension(self, dimension_id):
        if self.hypercube:
            for hypercube in self.hypercube:
                for dimension in hypercube.dimensions:
                    if dimension_id == dimension.concept.id:
                        return dimension
        else:
            logger.warning(f"Linkrole {self.roleURI} does not have any dimensions")

    def __eq__(self, other):
        if not isinstance(other, RoleType):
            # don't attempt to compare against unrelated types
            return NotImplemented

        return self.id == other.id and self.roleURI == other.roleURI

    def __key(self):
        return (self.id, self.roleURI)

    def __hash__(self):
        return hash(self.__key())

    def __str__(self):
        return self.roleURI

    @property
    def roletype_name(self):
        return self.roleURI.rsplit(":", 1)[1]


class ArcRoleType:
    def __init__(self, roleURI, cycles_allowed, used_on=None, definition=None, id=None):

        self.id = id
        self.roleURI = roleURI
        self.cycles_allowed = cycles_allowed

        self.used_on = used_on
        self.definition = definition
