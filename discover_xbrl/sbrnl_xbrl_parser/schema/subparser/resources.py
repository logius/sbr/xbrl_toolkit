import logging

from ..classes import Concept, ItemType, EnumerationChoice

logger = logging.getLogger("parser.schema")


def discover_itemtype(element, domain, namespaces, source=None):
    """
    Parses an enumerations and returns an Enumeration object
    :param element_list:
    :return:
    """

    choices = (
        []
    )  # Declare an empty list in case we have enums/choices we want to add to the itemType

    itemtype = ItemType(
        id=element.get("id"), name=element.get("name"), domain=domain, source=source
    )

    if element.tag == "{http://www.w3.org/2001/XMLSchema}complexType":
        """
        itemType is complex
        """

        # final = element.get("final") # Used by XBRL.org

        for XML_content in element:

            if XML_content.tag == "{http://www.w3.org/2001/XMLSchema}complexContent":

                for XML_inheritance_type in XML_content:
                    if (
                        XML_inheritance_type.tag
                        == "{http://www.w3.org/2001/XMLSchema}extension"
                    ):

                        itemtype.base = XML_inheritance_type.get(
                            "base"
                        )  # itemType extends this base element
                        itemtype.inheritance_type = "extents"

                        for XML_extension in XML_inheritance_type:
                            if (
                                XML_extension.tag
                                == "{http://www.w3.org/2001/XMLSchema}attribute"
                            ):

                                for elem in XML_extension:
                                    if (
                                        elem.tag
                                        == "{http://www.w3.org/2001/XMLSchema}simpleType"
                                    ):
                                        pass  # xs:schema
                                    elif (
                                        elem.tag
                                        == "{http://www.w3.org/2001/XMLSchema}group"
                                    ):
                                        pass  # xs:schema
                                    elif (
                                        elem.tag
                                        == "{http://www.w3.org/2001/XMLSchema}annotation"
                                    ):
                                        pass  # xs:schema

                            elif (
                                XML_extension.tag
                                == "{http://www.w3.org/2001/XMLSchema}anyAttribute"
                            ):
                                pass  # XBRL meta level, allows any attribute from a namespace as extension
                            elif (
                                XML_extension.tag
                                == "{http://www.w3.org/2001/XMLSchema}sequence"
                            ):
                                pass  # xs:schema
                            elif (
                                XML_extension.tag
                                == "{http://www.w3.org/2001/XMLSchema}attributeGroup"
                            ):
                                pass  # xs:schema
                            elif (
                                XML_extension.tag
                                == "{http://www.w3.org/2001/XMLSchema}group"
                            ):
                                pass  # xs:schema

                    elif (
                        XML_inheritance_type.tag
                        == "{http://www.w3.org/2001/XMLSchema}restriction"
                    ):

                        itemtype.base = XML_inheritance_type.get(
                            "base"
                        )  # itemType restricts this base element
                        itemtype.inheritance_type = "restricts"

                        for XML_restiction in XML_inheritance_type:

                            if (
                                XML_restiction.tag
                                == "{http://www.w3.org/2001/XMLSchema}choice"
                            ):

                                # min_occurs = XML_restiction.get("minOccurs")
                                # max_occurs = XML_restiction.get("maxOccurs")
                                # todo: Expect to get <element ref="">. Only used at XBRL meta level?
                                pass

                    else:
                        pass
            elif XML_content.tag == "{http://www.w3.org/2001/XMLSchema}annotation":
                pass
            elif XML_content.tag == "{http://www.w3.org/2001/XMLSchema}simpleContent":
                for XML_simple_content in XML_content:
                    if (
                        XML_simple_content.tag
                        == "{http://www.w3.org/2001/XMLSchema}extension"
                    ):
                        itemtype.base = XML_simple_content.get(
                            "base"
                        )  # itemType restricts this base element
                        itemtype.inheritance_type = "extends"

                    elif (
                        XML_simple_content.tag
                        == "{http://www.w3.org/2001/XMLSchema}restriction"
                    ):
                        itemtype.base = XML_simple_content.get(
                            "base"
                        )  # itemType restricts this base element
                        itemtype.inheritance_type = "restricts"
                        for XML_simple_content_elem in XML_simple_content:
                            if (
                                XML_simple_content_elem.tag
                                == "{http://www.w3.org/2001/XMLSchema}enumeration"
                            ):
                                choices.append(
                                    EnumerationChoice(
                                        id=XML_simple_content_elem.get("id"),
                                        value=XML_simple_content_elem.get("value"),
                                    )
                                )
                            elif (
                                XML_simple_content_elem.tag
                                == "{http://www.w3.org/2001/XMLSchema}totalDigits"
                            ):
                                itemtype.totalDigits = XML_simple_content_elem.get(
                                    "value"
                                )
                            elif (
                                XML_simple_content_elem.tag
                                == "{http://www.w3.org/2001/XMLSchema}length"
                            ):
                                itemtype.restriction_length = XML_simple_content_elem.get(
                                    "value"
                                )
                            elif (
                                XML_simple_content_elem.tag
                                == "{http://www.w3.org/2001/XMLSchema}pattern"
                            ):
                                itemtype.restriction_pattern = XML_simple_content_elem.get(
                                    "value"
                                )
                            elif (
                                XML_simple_content_elem.tag
                                == "{http://www.w3.org/2001/XMLSchema}maxLength"
                            ):
                                itemtype.restriction_max_length = XML_simple_content_elem.get(
                                    "value"
                                )
                            elif (
                                XML_simple_content_elem.tag
                                == "{http://www.w3.org/2001/XMLSchema}minLength"
                            ):
                                itemtype.restriction_min_length = XML_simple_content_elem.get(
                                    "value"
                                )
                            elif (
                                XML_simple_content_elem.tag
                                == "{http://www.w3.org/2001/XMLSchema}type"
                            ):
                                itemtype.restriction_type = XML_simple_content_elem.get(
                                    "value"
                                )
                            elif (
                                XML_simple_content_elem.tag
                                == "{http://www.w3.org/2001/XMLSchema}fractionDigits"
                            ):
                                itemtype.fraction_digits = XML_simple_content_elem.get(
                                    "value"
                                )
                            elif (
                                XML_simple_content_elem.tag
                                == "{http://www.w3.org/2001/XMLSchema}minInclusive"
                            ):
                                itemtype.min_inclusive = XML_simple_content_elem.get(
                                    "value"
                                )
                            elif (
                                XML_simple_content_elem.tag
                                == "{http://www.w3.org/2001/XMLSchema}maxInclusive"
                            ):
                                itemtype.max_inclusive = XML_simple_content_elem.get(
                                    "value"
                                )
                            elif (
                                XML_simple_content_elem.tag
                                == "{http://www.w3.org/2001/XMLSchema}attributeGroup"
                            ):
                                # print("Attributegroup; this one has children! ")
                                pass
                            else:
                                print(
                                    f"Itemtype-childs, not parsed: {XML_simple_content_elem.tag}"
                                )

            elif XML_content.tag == "{http://www.w3.org/2001/XMLSchema}sequence":
                pass  # Element can have these elements as children
            elif XML_content.tag == "{http://www.w3.org/2001/XMLSchema}attributeGroup":
                pass  # element can have this group of attributes
            elif XML_content.tag == "{http://www.w3.org/2001/XMLSchema}choice":
                pass
            elif XML_content.tag == "{http://www.w3.org/2001/XMLSchema}anyAttribute":
                pass  # xs:schema

    elif element.tag == "{http://www.w3.org/2001/XMLSchema}simpleType":
        """
        The itemType is a simple itemtype, which only restrict base type in some way
        """

        for XML_subelem in element:
            if XML_subelem.tag == "{http://www.w3.org/2001/XMLSchema}annotation":
                pass  # Used to add meta information
            elif XML_subelem.tag == "{http://www.w3.org/2001/XMLSchema}restriction":
                itemtype.enumeration_type = XML_subelem.get(
                    "base"
                )  # Get the type of the enumeration elements

                for XML_restriction_type in XML_subelem:

                    if (
                        XML_restriction_type.tag
                        == "{http://www.w3.org/2001/XMLSchema}minLength"
                    ):
                        itemtype.restriction_min_length = XML_restriction_type.get(
                            "value"
                        )

                    elif (
                        XML_restriction_type.tag
                        == "{http://www.w3.org/2001/XMLSchema}maxLength"
                    ):
                        itemtype.restriction_max_length = XML_restriction_type.get(
                            "value"
                        )

                    elif (
                        XML_restriction_type.tag
                        == "{http://www.w3.org/2001/XMLSchema}enumeration"
                    ):
                        choices.append(
                            EnumerationChoice(
                                id=XML_restriction_type.get("id"),
                                value=XML_restriction_type.get("value"),
                            )
                        )

                    elif (
                        XML_restriction_type.tag
                        == "{http://www.w3.org/2001/XMLSchema}length"
                    ):
                        itemtype.restriction_length = XML_restriction_type.get("value")
                    elif (
                        XML_restriction_type.tag
                        == "{http://www.w3.org/2001/XMLSchema}totalDigits"
                    ):
                        itemtype.restriction_length = XML_restriction_type.get("value")
                    elif (
                        XML_restriction_type.tag
                        == "{http://www.w3.org/2001/XMLSchema}pattern"
                    ):
                        itemtype.restriction_pattern = XML_restriction_type.get("value")
                    elif (
                        XML_restriction_type.tag
                        == "{http://www.w3.org/2001/XMLSchema}fractionDigits"
                    ):
                        itemtype.fraction_digits = XML_restriction_type.get("value")
                    elif (
                        XML_restriction_type.tag
                        == "{http://www.w3.org/2001/XMLSchema}minInclusive"
                    ):
                        itemtype.min_inclusive = XML_restriction_type.get("value")
                    elif (
                        XML_restriction_type.tag
                        == "{http://www.w3.org/2001/XMLSchema}whiteSpace"
                    ):
                        pass  # xs:schema
                    elif (
                        XML_restriction_type.tag
                        == "{http://www.w3.org/2001/XMLSchema}simpleType"
                    ):
                        pass  # xs:schema
                    elif (
                        XML_restriction_type.tag
                        == "{http://www.w3.org/2001/XMLSchema}maxInclusive"
                    ):
                        pass  # xs:schema
                    elif (
                        XML_restriction_type.tag
                        == "{http://www.w3.org/2001/XMLSchema}minInclusive"
                    ):
                        pass  # xs:schema
                    elif (
                        XML_restriction_type.tag
                        == "{http://www.w3.org/2001/XMLSchema}enumeration"
                    ):
                        choices.append(
                            EnumerationChoice(
                                id=XML_restriction_type.get("id"),
                                value=XML_restriction_type.get("value"),
                            )
                        )

            elif XML_subelem.tag == "{http://www.w3.org/2001/XMLSchema}union":
                pass  # itemType combines two other base itemTypes

    if len(choices):
        itemtype.enumerations = (
            choices  # Add the choices previously found, if applicable
        )

    return itemtype


#
# def get_element_prefix(elem, target_namespace):
#
#     for prefix, namespace in elem.nsmap.items():
#         stripped_prefix = str(elem.get("id")).split("_")[0]
#         """
#         TODO: EK: I don't really like how this function works. We are trying to guess the domain based on the part of an ID.
#         Better would be to check for the @targetNamespace. If this exists, the element prefix/domain should be the prefix
#         of that value.
#         """
#         if (
#             namespace
#             not in [
#                 "http://www.w3.org/2001/XMLSchema",
#                 "http://www.w3.org/1999/xlink",
#             ]
#             ## better might be the following
#             or elem.get("substitutionGroup") not in ["xl:resource"]
#             # elem.get("substitutionGroup")
#             # in [
#             #     "sbr:presentationItem",
#             #     "sbr:domainItem",
#             #     "sbr:domainMemberItem",
#             #     "sbr:primaryDomainItem",
#             #     "sbr:presentationTuple",
#             #     "sbr:specificationTuple",
#             # ]
#         ):
#
#             # this check works for the NT because we expect the namespace to be explicitly declared.
#             #  However, for XBRL 2.1 this is not a requirement so lets see if we can do without.
#
#             if prefix == stripped_prefix:
#                 return (namespace, prefix)
#
#     return (namespace, None)
#


def discover_elements(elements_list, target_namespace, source=None):
    """
    Discovers elements when doing schema discovery.
    :param elements_list:
    :return:
    """
    for elem in elements_list:

        if elem.get("id") is not None:
            if target_namespace is not None:
                ns_prefix = None
                ns = target_namespace
                # Only do this for taxonomies where targetNamespace is manditory
                for k, v in elem.nsmap.items():
                    if v == target_namespace:
                        ns_prefix = k
                        ns = v

            concept = Concept(
                id=elem.get("id"),
                name=elem.get("name"),
                nillable=elem.get("nillable"),
                balance=elem.get("{http://www.xbrl.org/2003/instance}balance"),
                periodType=elem.get("{http://www.xbrl.org/2003/instance}periodType"),
                substitutionGroup=elem.get("substitutionGroup"),
                element_type=elem.get("type"),
                typedDomainRef=elem.get("{http://xbrl.org/2005/xbrldt}typedDomainRef"),
                ns_prefix=ns_prefix,
                namespace=ns,
                labels={},
                is_abstract=elem.get("abstract"),
                source=source,
            )
            yield concept

            # else:
            #     print(f"not writing concept to DTS: {elem.get('name')}")
