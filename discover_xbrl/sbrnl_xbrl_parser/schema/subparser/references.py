import logging
import os
import pathlib
import zipfile

from ...parsetools.classes import WWWPath

# local imports
logger = logging.getLogger("parser.schema")


def discover_linkbase_refs(linkbaseref_list, url, taxonomy_package=None):
    for elem in linkbaseref_list:
        for key, val in elem.attrib.items():

            if key == "{http://www.w3.org/1999/xlink}href":
                # add all newly discovered files
                # If the file is offline, find the directory used, otherwise use absolute path as URL string
                if val.startswith("http"):
                    yield WWWPath(val)

                elif isinstance(url, pathlib.Path):
                    yield pathlib.Path(
                        os.path.normpath(os.path.join(url.parent.__str__(), val))
                    )

                elif isinstance(url, WWWPath):
                    yield WWWPath(f"{url.parent}/{val}")

                elif isinstance(url, zipfile.Path):
                    joined_path = zipfile.Path.joinpath(url.parent, val)
                    yield joined_path

                else:
                    logger.error(
                        f"{url} is neither a Path or an external URL. One of these is expected"
                    )
