import io
import os
from pathlib import Path

from PySide2.QtCore import QStringListModel, QRunnable, QObject, Signal, QUrl
from PySide2.QtWidgets import (
    QGroupBox,
    QVBoxLayout,
    QPushButton,
    QListView,
    QFileDialog,
    QWidget,
    QRadioButton,
)

from .parsetools.loadTaxonomy import Discover

from discover_xbrl.conf.conf import Config
from .parsetools.mem_fs import save_taxonomy_memfs
from .parsetools.taxonomy_package import read_taxonomy_package


conf = Config()

"""
GUI for taxonomy discovery module. 
"""


class DTSDiscoveryWorker(QRunnable):
    def __init__(self, ep, parent=None, validator=None, taxonomy_package=None):

        self.ep = ep
        self.taxonomy_package = taxonomy_package
        self.create_all_ep = False
        self.validator = validator() if validator else None

        super(DTSDiscoveryWorker, self).__init__()
        self.signals = DTSDiscoveryWorkerSignals()

    def update_infowidget(self, message):
        self.signals.messages.emit(message)

    def run(self):
        """
        Calls taxonomy discovery routine for one DTS/ep

        """
        discoverer = Discover(
            ep=self.ep,
            discover_all_ep=self.create_all_ep,
            validator=self.validator,
            info_widget=self.update_infowidget,
            taxonomy_package=self.taxonomy_package,
        )
        self.signals.messages.emit("starting")
        discoverer.start_discovering()
        self.signals.messages.emit("done")
        self.signals.result.emit(discoverer.dts_list)


class DTSDiscoveryWorkerSignals(QObject):
    result = Signal(list)
    finished = Signal()
    messages = Signal(str)


class TaxonomyPickerWidget(QGroupBox):
    """
    GUI Widget for picking a taxonomy. This is the central control panel for taxonomy discovery.
    """

    def __init__(self, parent=None):
        super(TaxonomyPickerWidget, self).__init__(parent)

        self.title = "Taxonomy Picker"
        self.parent = parent

        # Create widgets
        loadTaxonomyButton = QPushButton("&Load Taxonomy")
        loadTaxonomyPackageButton = QPushButton("Load Taxonomy Package")
        saveEntrypointListButton = QPushButton("Save Entrypoint List")
        startDiscoveryButton = QPushButton("Start &Discovery")
        self.generateAllEPRadioButton = QRadioButton("Create all EP")
        self.dts_worker = None
        self.entrypointList = QListView()
        self.taxonomy_package = None

        # Connect functions to buttons
        loadTaxonomyButton.clicked.connect(self.selectEntrypointsPopup)
        loadTaxonomyPackageButton.clicked.connect(self.selectTaxonomyPackagePopup)
        startDiscoveryButton.clicked.connect(self.callDiscovery)
        saveEntrypointListButton.clicked.connect(self.saveEntrypointList)

        # Set widgets to layout
        layout = QVBoxLayout()

        layout.addWidget(loadTaxonomyPackageButton)
        layout.addWidget(loadTaxonomyButton)
        layout.addWidget(self.entrypointList)
        layout.addWidget(startDiscoveryButton)
        layout.addWidget(saveEntrypointListButton)
        layout.addWidget(self.generateAllEPRadioButton)

        # Set layout to class attribute
        self.setLayout(layout)

    def selectEntrypointsPopup(self):
        """
        Popup for selecing entrypoints. These can either be (entrypoint.py) xsd's or a collection of full paths within a .txt.
        In case of a TXT, each EP should be referenced per row.
        """

        popup = QWidget()
        source_taxonomy = QFileDialog(popup, caption="Select Entrypoint(s)")
        dirs = [QUrl.fromLocalFile("/"), QUrl.fromLocalFile(str(Path.home()))]
        if conf.parser.get("local_taxonomy_dir"):
            dirs.append(QUrl.fromLocalFile(conf.parser.get("local_taxonomy_dir")))
            dirs.extend(
                [
                    QUrl.fromLocalFile(f.path)
                    for f in os.scandir(conf.parser.get("local_taxonomy_dir"))
                    if f.is_dir()
                ]
            )

        source_taxonomy.setSidebarUrls(dirs)

        file_list = source_taxonomy.getOpenFileNames(
            parent=self, caption="Select Entrypoint(s)", filter="Entrypoint (*.*)"
        )[0]
        if self.entrypointList.model():
            file_list = file_list + self.entrypointList.model().stringList()
        model = QStringListModel(file_list)
        self.entrypointList.setModel(model)

    def selectTaxonomyPackagePopup(self):
        """
        Popup for selecting a taxonomy package to load.
        Only one taxonomy zip can be loaded at the same time as we need to rewriteURI's

        """
        selected_file = None
        discover = True  # Discover right after loading taxonomy package

        if conf.parser.get("taxonomy_package"):
            selected_file = conf.parser["taxonomy_package"]
        else:
            popup = QWidget()
            source_taxonomy = QFileDialog(
                popup, "Select Entrypoint(s)", "", "Taxonomy Package (*.zip)"
            )

            selected_file = source_taxonomy.getOpenFileName(
                filter="Taxonomy Package (*.zip)"
            )

            if selected_file[0]:  # Only try to load if a file is given
                selected_file = selected_file[0]

        if selected_file:
            taxonomy_package_metadata = read_taxonomy_package(selected_file)

            metadata = taxonomy_package_metadata["metadata"]

            entrypoints = taxonomy_package_metadata["entrypoints"]

            save_taxonomy_memfs(taxonomy_package=selected_file)

            self.entrypointList = entrypoints
            self.taxonomy_package = taxonomy_package_metadata

        # Remove any validator if available. For packages this is not supported
        # Support could be added relatively easily, but is not needed for now.
        # There are two things that need to be changed:
        # 1. Change the way schema and linkbases are selected for validation
        # 2. Change BOM NTA rule check. Right now it tries to read the file from disk.
        # Removing validator from taxonomy package loading also makes the process less cluttered.

        # print("Removing any available validator from taxonomy package processing")  # why? except the need for speed?
        # self.parent.validator = None

        # Set a flag so we know in the discoverer to try and access packages in memfs
        conf.parser["has_taxonomy_package"] = True

        # Call discovery automatically after loading.
        self.callDiscovery()

    def callDiscovery(self, remappings=None):
        """
        Sets GUI elements before calling the discovery routine.
        """

        # Get validator if given, else keep None
        validator_module = (
            self.parent.validator()
            if self.parent.validator and conf.validator["write_report"] is True
            else None
        )
        if validator_module:
            validator_module = validator_module["setup_validator"]

        entrypoint_list = self.entrypointList
        if hasattr(self.entrypointList, "model"):
            if self.entrypointList.model():
                entrypoint_list = self.entrypointList.model().stringList()

        if not isinstance(entrypoint_list, list):
            if isinstance(conf.parser["default_ep_list"], str):
                entrypoint_list = [conf.parser["default_ep_list"]]
            else:
                entrypoint_list = conf.parser["default_ep_list"]

        for i, ep in enumerate(entrypoint_list):
            # If the EP entry is a .txt file, we assume it is an entrypoint list and need to process it.
            if isinstance(ep, str) and ep.endswith(".txt"):

                with open(ep) as entrypoint_file:
                    for row in entrypoint_file.readlines():
                        # explictly strip special characters
                        row = row.strip()
                        # We only want existing schema files to be accepted
                        if Path(row).is_file() and row.endswith(".xsd"):
                            entrypoint_list.append(row)
                # Pop the txt file.
                entrypoint_list.pop(i)

        for ep in entrypoint_list:
            dts_worker = DTSDiscoveryWorker(ep=ep, validator=validator_module)
            dts_worker.taxonomy_package = self.taxonomy_package

            dts_worker.signals.result.connect(self.signal_result)
            dts_worker.signals.messages.connect(self.signal_messages)

            if self.taxonomy_package:
                print(f"Discovering: {ep.name}")
            else:
                print(f"Discovering: {ep}")

            self.parent.threadpool.start(dts_worker)

        if self.taxonomy_package:
            print(f"Done discovering: {ep.name}")
        else:
            print(f"Done discovering: {ep}")

    # Set list with DTS objects in parent GUI element to parent.discovered_dts_list
    def signal_result(self, dtslist):
        self.parent.add_dts_list(
            dtslist
        )  # this method extends the already sicovered list with the current list
        # self.parent.setDTS_list(dtslist)
        print(f"Got result {len(dtslist)}")

    def signal_messages(self, message):
        self.parent.info.append(message)

    def saveEntrypointList(self,):
        """
        Save the current list of discoverable EP's to an .txt file for future usage.
        """
        EP_list = self.entrypointList.model().stringList()

        # Create path if it does not exist
        Path(conf.parser.get("ep_list_output").rsplit("/", 1)[0]).mkdir(
            parents=True, exist_ok=True
        )
        with io.open(conf.parser.get("ep_list_output"), "w+") as of:

            # If the row is an XSD, add it to the list
            for row in EP_list:
                of.write(f"{row}\n")

    def update_entrypointlistview(self, file_list):
        model = QStringListModel(file_list)
        self.entrypointList.setModel(model)
