import os

import logging

from discover_xbrl.conf.conf import Config


def setup_logger():
    """
    # Set all loggers for the parser
    #"""

    formatter = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    )
    conf = Config()

    os.makedirs(conf.logging["maindir"], exist_ok=True)

    # Setup parser logger
    logger_parser = logging.getLogger("parser")
    logger_parser.propagate = True
    logger_parser.setLevel(logging.INFO)
    # logger_parser.setLevel(logging.WARNING)  # container for parser loggers. Do not log info and debug.
    logger_parser.addHandler(
        logging.FileHandler(f"{conf.logging['maindir']}/parser.log", "a+")
    )
    logger_parser.addHandler(logging.StreamHandler())

    # Setup postprocessing logger
    logger_postprocessing = logging.getLogger("parser.postprocessing")
    logger_postprocessing.propagate = False
    logger_postprocessing.setLevel(logging.WARNING)
    logger_postprocessing.addHandler(
        logging.FileHandler(
            f"{conf.logging['maindir']}/parser_postprocessing.log", "a+"
        )
    )
    logger_postprocessing.addHandler(logging.StreamHandler())

    # Setup linkbase logger
    logger_linkbase = logging.getLogger("parser.linkbase")
    logger_linkbase.propagate = False
    logger_linkbase.setLevel(logging.WARNING)
    logger_linkbase.addHandler(
        logging.FileHandler(f"{conf.logging['maindir']}/parser_linkbase.log", "a+")
    )
    logger_linkbase.addHandler(logging.StreamHandler())

    # Setup schema logger
    logger_schema = logging.getLogger("parser.schema")
    logger_schema.propagate = False
    logger_schema.setLevel(logging.WARNING)
    logger_schema.addHandler(
        logging.FileHandler(f"{conf.logging['maindir']}/parser_schema.log", "a+")
    )
    logger_schema.addHandler(logging.StreamHandler())

    # Setup schema logger
    logger_parsetools = logging.getLogger("parser.parsetools")
    logger_parsetools.propagate = True
    logger_parsetools.setLevel(logging.WARNING)
    logger_parsetools.addHandler(
        logging.FileHandler(f"{conf.logging['maindir']}/parser_parsetools.log", "a+")
    )
    logger_parsetools.addHandler(logging.StreamHandler())
