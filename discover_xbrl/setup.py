from setuptools import setup

setup(name='funniest',
      version='0.1',
      description='A XBRL parser, primarily for the Dutch SBR ecosystem',
      author='Erwin Kaats',
      author_email='erwin.kaats@logius.nl',
      license='MIT',
      packages=['sbrnl_xbrl_parser, sbrnl_xbrl_toolbox', 'sbrnl_modules.sbr_wonen'],
      zip_safe=False)