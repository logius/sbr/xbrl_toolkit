from fastapi import FastAPI, Response, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles

from typing import List, Optional
from discover_xbrl.api.admin_schema import ApiConfig, ApiLoadTaxonomies
from discover_xbrl.api.schema import (
    ApiArc,
    ApiArcrole,
    ApiAssertion,
    ApiConcept,
    ApiDimension,
    ApiDts,
    ApiDtsFull,
    ApiHypercube,
    ApiItemtype,
    ApiLabel,
    ApiNamespace,
    ApiLinkRole,
    ApiPresentationHierarchyNode,
    ApiTable,
    SearchResponse,
    DtsNiceName,
    DtsFeatures,
    ApiReference,
    ErrorMessage,
    NTVersions,
    ApiBackreference,
    ApiTableGrid,
)
from discover_xbrl.api.admin_configuration import Configuration
from discover_xbrl.conf.conf import Config
from discover_xbrl.sbrnl_modules.db.connection import get_connection

from discover_xbrl.sbrnl_modules.db.collections.arcs import Arcs
from discover_xbrl.sbrnl_modules.db.collections.assertions import Assertions
from discover_xbrl.sbrnl_modules.db.collections.dimensions import Dimensions
from discover_xbrl.sbrnl_modules.db.collections.label_backreferences import (
    LabelBackreferences,
)
from discover_xbrl.sbrnl_modules.db.collections.namespaces import Namespaces
from discover_xbrl.sbrnl_modules.db.collections.reference_backreference import (
    ReferenceBackreferences,
)
from discover_xbrl.sbrnl_modules.db.collections.references import References
from discover_xbrl.sbrnl_modules.db.collections.search import Search
from discover_xbrl.sbrnl_modules.db.collections.concepts import Concepts
from discover_xbrl.sbrnl_modules.db.collections.concept_backreferences import (
    ConceptBackReferences,
)
from discover_xbrl.sbrnl_modules.db.collections.dts import DTSes, DTS
from discover_xbrl.sbrnl_modules.db.collections.hypercubes import HyperCubes
from discover_xbrl.sbrnl_modules.db.collections.itemtypes import ItemTypes
from discover_xbrl.sbrnl_modules.db.collections.itemtype_backreference import (
    ItemtypeBackreferences,
)
from discover_xbrl.sbrnl_modules.db.collections.labels import Labels
from discover_xbrl.sbrnl_modules.db.collections.linkroles import Linkroles
from discover_xbrl.sbrnl_modules.db.collections.presentation_hierarchies import (
    PresentationHierarchies,
)
from discover_xbrl.sbrnl_modules.db.collections.tables import Tables
from discover_xbrl.sbrnl_modules.db.collections.helpers import get_dts_nice_name
from discover_xbrl.sbrnl_modules.db.render.table_grid import TableGrid
from discover_xbrl.sbrnl_modules.db.render.table_tree import TableTree

from discover_xbrl.sbrnl_xbrl_toolbox.xbrl_cli import CLI
from discover_xbrl.api.admin import admin

config = Config()

api_server = FastAPI(
    title="NTA - XBRL Taxonomie API",
    version="0.1.0",
    description="NTA - XBRL Taxonomie API. Een API die delen van de NTA in JSON formaat kan antwoorden. "
    "Geschreven door Logius",
    openapi_tags=[
        {"name": "Concepts", "description": "Endpoints returning concept(s)"},
        {"name": "Labels", "description": "Endpoints returning label object(s)"},
        {"name": "Linkroles"},
        {"name": "References"},
        {"name": "Tables"},
        {"name": "Presentations"},
        {"name": "Arcs"},
        {"name": "Table of Contents"},
        {"name": "DTS"},
        {"name": "NT Versions"},
        {"name": "Namespaces"},
        {"name": "Admin"},
        {"name": "AdminUser"},
        {"name": "AdminZip"},
        {"name": "Deprecated"},
    ],
)
origins = ["*"]
api_server.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
# The admin module is located in admin.py
api_server.include_router(admin)


##
# Endpoints
##


@api_server.get(
    "/dts",
    response_model=List[ApiDts],
    tags=["DTS"],
    responses={404: {"model": ErrorMessage}},
)
def all_dts(filter: Optional[str] = None, nt_version: Optional[str] = None):
    """
    Returns a list of all available entrypoints,
    the list can optionally be filtered by the server by adding ?filter=...

    """
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    dts = DTSes(db=db, full=False)
    dts.get_all(filter=filter)
    response = [ApiDts(**dts) for dts in dts.dtses]
    return response


@api_server.get(
    "/dts/{dts_name}",
    response_model=ApiDtsFull,
    tags=["DTS"],
    description="This method returns a complete entrypoint and is rather big. "
    "It will take minutes for the first time to generate the response so be patient. "
    "After that the object will be cached and response time will improve.",
    responses={404: {"model": ErrorMessage}},
)
def get_dts(
    dts_name: str, nt_version: Optional[str] = None, full: Optional[bool] = False
):
    """
    Gets a specific DTS by its entrypoint name.
    You probably want to add **nt_version=NT17** for example, don't rely on the defaults!
    If you want the complete DTS as 1 JSON document you should add **full=true** as
    query parameter _(takes long)_
    """
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    dts = DTS(db=db, full=full)
    try:
        dts.get_dts(name=dts_name)
    except ValueError as err:
        raise HTTPException(status_code=404, detail=f"{err.__str__()} {db.dbname}")

    dts.db = None
    dts.cursor = None
    return dts


@api_server.get(
    "/dts/{dts_name}/features",
    tags=["DTS"],
    description="Returns which features of a DTS are available in the current Entrypoint",
    response_model=DtsFeatures,
)
def dts_features(dts_name: str, nt_version: Optional[str] = None):
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    dts = DTS(db=db, full=False)
    return dts.dts_features(dts_name=dts_name)


@api_server.get(
    "/dts/{dts_name}/arcroles",
    tags=["Arcs"],
    description="Returns a list of available Arcroles which represent a network.",
    responses={404: {"model": ErrorMessage}},
    response_model=List[ApiArcrole],
    response_model_exclude_none=True,
)
def arcroles_of_dts(dts_name: str, nt_version: Optional[str] = None):
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    arcs = Arcs(db=db)
    arcs.get_arcrole_dropdown(dts_name=dts_name)
    return arcs.arcrole_dropdown


@api_server.get(
    "/dts/{dts_name}/arcs/{arcrole}",
    tags=["Arcs"],
    responses={404: {"model": ErrorMessage}},
    response_model=ApiArc,
    response_model_exclude_none=True,
    description="For given Arcrole return all the members of the network. (Arcs of Arcrole)",
)
def arcs_by_arcrole(dts_name: str, arcrole: str, nt_version: Optional[str] = None):
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    arcs = Arcs(db=db)
    arcs.arc_tree(dts_name=dts_name, arcrole=arcrole)
    return arcs.json_tree


@api_server.get(
    "/dts/{dts_name}/assertions",
    response_model=List[ApiAssertion],
    response_model_exclude_none=True,
    tags=["DTS"],
    responses={404: {"model": ErrorMessage}},
    description="Returns a list of all Assertions found in the entrypoint",
)
def assertions_of_dts(
    dts_name: str, nt_version: Optional[str] = None, full: Optional[bool] = True
):
    """
    Gets a list of assertions found in the entrypoint
    """
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    assertions = Assertions(db=db, full=full)
    assertions.assertions_of_dts(dts_name=dts_name)
    return assertions.assertions


@api_server.get(
    "/dts/{dts_name}/assertions/{assertion_id}",
    response_model=List[ApiAssertion],
    response_model_exclude_none=True,
    tags=["DTS"],
    responses={404: {"model": ErrorMessage}},
    description="Returns the complete assertion including the formulas, variable arc, messages and severity.",
)
def assertion_of_dts_by_id(
    dts_name: str,
    assertion_id: str,
    nt_version: Optional[str] = None,
    full: Optional[bool] = True,
):
    """
    Gets a specific assertion
    """
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    assertions = Assertions(db=db, full=full)
    assertions.assertion_of_dts_by_id(dts_name=dts_name, assertion_id=assertion_id)
    return assertions.assertions


@api_server.get(
    "/dts/{dts_name}/concepts/backref/{concept_id}",
    tags=["Concepts"],
    response_model=List[ApiBackreference],
    responses={404: {"model": ErrorMessage}},
    description="Searches inside the entrypoint for usage of a Concept.",
)
def concept_backref(dts_name: str, concept_id: str, nt_version: Optional[str] = None):
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    back_references = ConceptBackReferences(db=db)
    back_references.back_references_of_concept(concept_id=concept_id, dts_name=dts_name)
    return back_references.back_references


@api_server.get(
    "/dts/{dts_name}/concepts",
    response_model=List[ApiConcept],
    tags=["DTS", "Concepts"],
    responses={404: {"model": ErrorMessage}},
    description="Returns a list of concepts associated with the entrypoint",
)
def concepts_of_dts(
    dts_name: str,
    nt_version: Optional[str] = None,
    full: Optional[bool] = True,
    referenced_only: Optional[bool] = True,
):
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    concepts = Concepts(db=db, full=full)
    concepts.concepts_of_dts(dts_name=dts_name, referenced_only=referenced_only)
    return concepts.concepts


@api_server.get(
    "/dts/{dts_name}/concepts/{concept_id}",
    response_model=ApiConcept,
    response_model_exclude_none=True,
    tags=["Concepts"],
    responses={404: {"model": ErrorMessage}},
    description="Returns a complete concept and it's children like labels and references",
)
def concept_by_id(dts_name: str, concept_id: str, nt_version: Optional[str] = None):
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    concepts = Concepts(db=db, full=True)
    concepts.concept_by_id(id=concept_id, dts_name=dts_name)
    if len(concepts.concepts):
        return concepts.concepts[0]
    else:
        raise HTTPException(
            status_code=404, detail=f"Concept {concept_id} not found (/concepts)"
        )


@api_server.get(
    "/dts/{dts_name}/dimensions",
    response_model=List[ApiDimension],
    response_model_exclude_none=True,
    tags=["DTS"],
    responses={404: {"model": ErrorMessage}},
    description="Returns a list of Dimensions of given entrypoint",
)
def dimensions_of_dts(
    dts_name: str, nt_version: Optional[str] = None, full: Optional[bool] = True
):
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    dimensions = Dimensions(db=db, full=full)
    dimensions.dimensions_of_dts(dts_name=dts_name)
    if len(dimensions.dimensions):
        return dimensions.dimensions
    else:
        detail = f"No dimensions for entrypoint: {dts_name} "
        if nt_version:
            detail += f" in NT version: {nt_version}"
        raise HTTPException(status_code=404, detail=detail)


@api_server.get(
    "/dts/{dts_name}/dimensions/{rowid}",
    response_model=List[ApiDimension],
    response_model_exclude_none=True,
    tags=["DTS"],
    responses={404: {"model": ErrorMessage}},
    description="Gets a specific dimension from the entrypoint",
)
def dimension_by_rowid(
    dts_name: str,
    rowid: int,
    nt_version: Optional[str] = None,
    full: Optional[bool] = True,
):
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    dimensions = Dimensions(db=db, full=full)
    dimensions.dimension_by_rowid(rowid=rowid, dts_name=dts_name)
    if len(dimensions.dimensions):
        return dimensions.dimensions
    else:
        detail = f"Dimensions with rowid: {rowid}  not found"
        if nt_version:
            detail += f" in NT version: {nt_version}"
        raise HTTPException(status_code=404, detail=detail)


@api_server.get(
    "/dts/{dts_name}/hypercubes",
    response_model=List[ApiHypercube],
    response_model_exclude_none=True,
    tags=["DTS"],
    responses={404: {"model": ErrorMessage}},
    description="Gets a list of 'hypercubes' (definiton links) with line-items and dimensions",
)
def hypercubes_of_dts(
    dts_name: str, nt_version: Optional[str] = None, full: Optional[bool] = True
):
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    hypercubes = HyperCubes(db=db, full=full)
    hypercubes.hypercubes_of_dts(dts_name=dts_name)
    if len(hypercubes.hypercubes):
        return hypercubes.hypercubes
    else:
        detail = f"No hypercubes for entrypoint: {dts_name} "
        if nt_version:
            detail += f" in NT version: {nt_version}"
        raise HTTPException(status_code=404, detail=detail)


@api_server.get(
    "/dts/{dts_name}/hypercubes/{role_uri}",
    response_model=List[ApiHypercube],
    response_model_exclude_none=True,
    tags=["DTS"],
    responses={404: {"model": ErrorMessage}},
    description="Gets a specific definition link. _Beware: you need to replace a '/' with a '|' for obvious reasons_",
)
def hypercubes_of_dts_of_linkrole(
    dts_name: str,
    role_uri: str,
    nt_version: Optional[str] = None,
    full: Optional[bool] = True,
):
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    hypercubes = HyperCubes(db=db, full=full)
    hypercubes.hypercube_of_linkrole_dts(role_uri=role_uri, dts_name=dts_name)
    if len(hypercubes.hypercubes):
        return hypercubes.hypercubes
    else:
        detail = f"Hypercubes {role_uri} for entrypoint: {dts_name} "
        if nt_version:
            detail += f" in NT version: {nt_version}"
        raise HTTPException(status_code=404, detail=detail)


@api_server.get(
    "/dts/{dts_name}/itemtypes",
    response_model=List[ApiItemtype],
    tags=["DTS"],
    responses={404: {"model": ErrorMessage}},
    description="Gets a list of all itemtypes used in this entrypoint",
)
def itemtypes_of_dts(
    dts_name: str, nt_version: Optional[str] = None, full: Optional[bool] = True
):
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    itemtypes = ItemTypes(db=db, full=full)
    itemtypes.itemtypes_of_dts(dts_name=dts_name)
    if not len(itemtypes.itemtypes):
        raise HTTPException(status_code=404, detail="No itemtypes found")
    return itemtypes.itemtypes


@api_server.get(
    "/dts/{dts_name}/itemtypes/{itemtype_name}",
    response_model=List[ApiItemtype],
    response_model_exclude_none=True,
    tags=["DTS"],
    responses={404: {"model": ErrorMessage}},
    description="Gets a specific Itemtype with all attributes and enumerations resolved",
)
def itemtypes_of_dts_by_name(
    dts_name: str,
    itemtype_name: str,
    nt_version: Optional[str] = None,
    full: Optional[bool] = True,
):
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    itemtypes = ItemTypes(db=db, full=full)
    itemtypes.itemtype_by_name(itemtype_name=itemtype_name, dts_name=dts_name)
    if len(itemtypes.itemtypes):
        return itemtypes.itemtypes
    else:
        raise HTTPException(
            status_code=404,
            detail=f"Itemtype {itemtype_name} not found (/dts/{dts_name}/itemtypes)",
        )


@api_server.get(
    "/dts/{dts_name}/itemtypes/backref/{itemtype_name}",
    tags=["DTS"],
    responses={404: {"model": ErrorMessage}},
    description="Gets a list of al objects which are linked to the reference.",
)
def backref_of_itemtypes_by_id(
    dts_name: str, itemtype_name: str, nt_version: Optional[str] = None
):
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    backref = ItemtypeBackreferences(db=db)
    backref.backreferences_of_itemtype(itemtype_name=itemtype_name, dts_name=dts_name)
    return backref.back_references


@api_server.get(
    "/dts/{dts_name}/labels",
    tags=["DTS", "Labels"],
    responses={404: {"model": ErrorMessage}},
    response_model=List[ApiLabel],
    response_model_exclude_none=True,
    description="Gets a list of labels found in the entrypoint with a maximum of 10000. "
    "Optionally a search query can be added to the url to search within the labels.",
)
def labels_of_dts(
    dts_name: str, nt_version: Optional[str] = None, q: Optional[str] = None
):
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    labels = Labels(db=db)
    labels.labels_of_dts(dts_name=dts_name, q=q)
    if len(labels.labels):
        return labels.labels
    else:
        raise HTTPException(status_code=404, detail="No labels found")


@api_server.get(
    "/concepts/{concept_id}/labels",
    response_model=List[ApiLabel],
    tags=["Concepts", "Labels"],
    responses={404: {"model": ErrorMessage}},
)
def labels_of_concept(concept_id: str, nt_version: Optional[str] = None):
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    labels = Labels(db=db)
    labels.labels_of_concept(concept_id=concept_id)
    return labels.labels


@api_server.get(
    "/dts/{dts_name}/languages",
    response_model=List[str],
    tags=["Labels"],
    responses={404: {"model": ErrorMessage}},
    description="Gets a list of languages in which labels are available",
)
def languages_of_labels(dts_name: str, nt_version: Optional[str] = None):
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    labels = Labels(db=db)
    response = labels.languages_of_dts(dts_name=dts_name)
    if not len(response):
        raise HTTPException(status_code=404, detail="No languages found")
    return response


@api_server.get(
    "/label_roles",
    response_model=List,
    tags=["Labels"],
    responses={404: {"model": ErrorMessage}},
    description="Gets a list of different label-roles available in the NTA",
)
def roles_of_labels(nt_version: Optional[str] = None):
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    labels = Labels(db=db)
    return labels.label_roles()


@api_server.get(
    "/dts/{dts_name}/labels/backref/{label_id}",
    tags=["DTS", "Labels"],
    responses={404: {"model": ErrorMessage}},
    response_model=List[ApiBackreference],
    description="Gets all objects which use label: <em>id</em>",
)
def backref_of_label(
    dts_name: str,
    label_id: str,
    nt_version: Optional[str] = None,
    role: Optional[str] = None,
    lang: Optional[str] = None,
):
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    backrefs = LabelBackreferences(db=db)
    backrefs.back_references_of_label(
        label_id=label_id, dts_name=dts_name, role=role, lang=lang
    )
    return backrefs.back_references


@api_server.get(
    "/dts/{dts_name}/linkroles",
    response_model=List[ApiLinkRole],
    response_model_exclude_none=True,
    tags=["DTS", "Linkroles"],
    responses={404: {"model": ErrorMessage}},
    description="Gets a list of linkroles belonging to the entrypoint.",
)
def linkroles_of_dts(
    dts_name: str, nt_version: Optional[str] = None, full: Optional[bool] = True
):
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    linkroles = Linkroles(db=db, full=full)
    linkroles.linkroles_from_dts(dts_name=dts_name)
    if len(linkroles.linkroles):
        return linkroles.linkroles
    else:
        detail = f"No linkroles found for: entrypoint: {dts_name} "
        if nt_version:
            detail += f" in NT version: {nt_version}"
        raise HTTPException(status_code=404, detail=detail)


@api_server.get(
    "/dts/{dts_name}/linkroles/{linkrole_id}",
    response_model_exclude_none=True,
    response_model=List[ApiLinkRole],
    tags=["DTS", "Linkroles"],
    responses={404: {"model": ErrorMessage}},
    description="Gets a specific linkrole",
)
def linkroles_of_dts(
    dts_name: str,
    linkrole_id: str,
    nt_version: Optional[str] = None,
    roleURI: Optional[str] = None,
    full: Optional[bool] = True,
):
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    linkroles = Linkroles(db=db, full=full)
    linkroles.linkrole_of_dts_by_id(
        dts_name=dts_name, linkrole_id=linkrole_id, roleURI=roleURI
    )
    if len(linkroles.linkroles):
        return linkroles.linkroles
    else:
        detail = f"Linkrole {linkrole_id} not found in entrypoint: {dts_name} "
        if nt_version:
            detail += f" in NT version: {nt_version}"
        raise HTTPException(status_code=404, detail=detail)


@api_server.get(
    "/dts/{dts_name}/presentation",
    response_model=List[ApiPresentationHierarchyNode],
    response_model_exclude_none=True,
    tags=["DTS", "Table of Contents"],
    responses={404: {"model": ErrorMessage}},
    description="Gets the 'Table of Contents' of the entrypoint. "
    "If no Table of Contents is found we return a list of linkroles cast to a presentation",
)
def presentation_of_dts(
    dts_name: str, nt_version: Optional[str] = None, full: Optional[bool] = True
):
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    presentations = PresentationHierarchies(db=db, full=full)
    presentations.table_of_contents_of_dts(dts_name=dts_name)
    if len(presentations.presentation_hierarchies):
        return presentations.presentation_hierarchies
    # This is som sort of a hack
    linkroles = Linkroles(db=db, full=True)
    linkroles.linkroles_of_dts_with_presentation(dts_name=dts_name)
    if len(linkroles.linkroles):
        return linkroles.linkroles
    else:
        detail = f"Presentation for entrypoint: {dts_name} "
        if nt_version:
            detail += f" in NT version: {nt_version}"
        raise HTTPException(status_code=404, detail=detail)


@api_server.get(
    "/dts/{dts_name}/presentations",
    response_model=List[ApiPresentationHierarchyNode],
    tags=["DTS", "Presentations"],
    responses={404: {"model": ErrorMessage}},
    description="Gets a list of presentation hierarchies belonging to the entrypoint.",
)
def presentations_of_dts(
    dts_name: str, nt_version: Optional[str] = None, full: Optional[bool] = True
):
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    presentations = PresentationHierarchies(db=db, full=full)
    presentations.presentation_hierarchies_of_dts(dts_name=dts_name)
    if len(presentations.presentation_hierarchies):
        return presentations.presentation_hierarchies
    else:
        detail = f"No presntations for entrypoint: {dts_name} "
        if nt_version:
            detail += f" in NT version: {nt_version}"
        raise HTTPException(status_code=404, detail=detail)


@api_server.get(
    "/dts/{dts_name}/nice_name",
    response_model=DtsNiceName,
    tags=["DTS"],
    responses={404: {"model": ErrorMessage}},
    description="returns the entrypoint name with '_' and '-' removed",
)
def dts_nice_name(dts_name: str, nt_version: Optional[str] = None):
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    dts = DTS(db=db, full=False)
    try:
        dts.get_dts(name=dts_name)
        return {"dts_nice_name": dts.nice_name}
    except ValueError as err:
        raise HTTPException(status_code=404, detail=str(err))


@api_server.get(
    "/qname",
    response_model=ApiConcept,
    tags=["Concepts"],
    responses={404: {"model": ErrorMessage}},
    description="Find a Concept by QName.",
)
def concept_by_qname(qname: str, nt_version: Optional[str] = None):
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    concepts = Concepts(db=db, full=True)
    concepts.concept_by_qname(qname=qname)
    if len(concepts.concepts):
        return concepts.concepts[0]
    else:
        raise HTTPException(
            status_code=404, detail=f"Concept {qname} not found (/qname)"
        )


@api_server.get(
    "/dts/{dts_name}/references",
    response_model=List[ApiReference],
    tags=["DTS", "References"],
    responses={404: {"model": ErrorMessage}},
    description="Gets a list of references found in the entrypoint",
)
def references_of_dts(dts_name: str, nt_version: Optional[str] = None):
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    references = References(db=db)
    references.references_of_dts(dts_name=dts_name)
    if not len(references.references):
        raise HTTPException(status_code=404, detail="No references found")
    return references.references


@api_server.get(
    "/dts/{dts_name}/references/{reference_id}",
    response_model=List[ApiReference],
    tags=["DTS", "References"],
    responses={404: {"model": ErrorMessage}},
    description="Gets a specific reference",
)
def references_by_id(
    dts_name: str, reference_id: str, nt_version: Optional[str] = None
):
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    references = References(db=db)
    references.reference_by_id(reference_id=reference_id, dts_name=dts_name)
    return references.references


@api_server.get(
    "/dts/{dts_name}/references/backref/{reference_id}",
    tags=["DTS", "References"],
    responses={404: {"model": ErrorMessage}},
    response_model=List[ApiBackreference],
    description="Gets a list of al objects which are linked to the reference.",
)
def backref_of_references_by_id(
    dts_name: str, reference_id: str, nt_version: Optional[str] = None
):
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    backref = ReferenceBackreferences(db=db)
    backref.backreferences_of_reference(reference_id=reference_id, dts_name=dts_name)
    return backref.back_references


@api_server.get(
    "/dts/{dts_name}/tables",
    response_model=List[ApiTable],
    tags=["DTS", "Tables"],
    responses={404: {"model": ErrorMessage}},
    description="Gets a list of tables found in the entrypoint",
)
def tables_of_dts(
    dts_name: str, nt_version: Optional[str] = None, full: Optional[bool] = True
):
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    tables = Tables(db=db, full=full)
    tables.tables_of_dts(dts_name=dts_name)
    if len(tables.tables):
        return tables.tables
    else:
        detail = f"No tables for entrypoint: {dts_name} "
        if nt_version:
            detail += f" in NT version: {nt_version}"
        raise HTTPException(status_code=404, detail=detail)


@api_server.get(
    "/dts/{dts_name}/tables/{table_id}",
    response_model=List[ApiTable],
    tags=["DTS", "Tables"],
    responses={404: {"model": ErrorMessage}},
    description="Gets a specific table definition from entrypoint",
)
def tables_of_dts(
    dts_name: str,
    table_id: str,
    nt_version: Optional[str] = None,
    full: Optional[bool] = True,
):
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    tables = Tables(db=db, full=full)
    tables.get_table_of_dts_by_id(dts_name=dts_name, table_id=table_id)
    if len(tables.tables):
        return tables.tables
    else:
        detail = f"Tables {table_id} for entrypoint: {dts_name} "
        if nt_version:
            detail += f" in NT version: {nt_version}"
        raise HTTPException(status_code=404, detail=detail)


@api_server.get(
    "/concepts",
    response_model=List[ApiConcept],
    tags=["Concepts"],
    responses={404: {"model": ErrorMessage}},
    description="Lists all Concepts available in the database _(takes long)_ "
    "Can optionally be filtered by the servver, add **filter=...**",
)
def all_concepts(
    filter: str = None, nt_version: Optional[str] = None, full: Optional[bool] = True
):
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    concepts = Concepts(db=db, full=full)
    concepts.get_all(filter=filter)
    return concepts.concepts


@api_server.get(
    "/namespaces",
    response_model=List[ApiNamespace],
    tags=["Namespaces"],
    responses={404: {"model": ErrorMessage}},
    description="Returns a list of all available namespaces within the givenNT Version",
)
def all_namespaces(filter: str = None, nt_version: Optional[str] = None):
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    namespaces = Namespaces(db=db)
    namespaces.get_all(filter=filter)
    return namespaces.namespaces


@api_server.get(
    "/presentations",
    response_model=List[ApiPresentationHierarchyNode],
    tags=["Presentations", "Deprecated"],
    responses={404: {"model": ErrorMessage}},
    description="Returns all presentations in the database. deprecated. slow",
    deprecated=True,
)
def all_presentations(filter: str = None):
    try:
        db = get_connection()
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    presentations = PresentationHierarchies(db=db)
    presentations.get_all(filter=filter)
    return presentations.presentation_hierarchies


@api_server.get(
    "/presentations/{presentation_rowid}",
    response_model=ApiPresentationHierarchyNode,
    tags=["Presentations"],
    responses={404: {"model": ErrorMessage}},
    description="Returns one, complete, presentation_hierarchy starting at the node specified by @param: rowid",
)
def presentation_by_rowid(presentation_rowid: int, nt_version: Optional[str] = None):
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    presentations = PresentationHierarchies(db=db)
    presentations.get_by_rowid(rowid=presentation_rowid)
    if len(presentations.presentation_hierarchies):
        return presentations.presentation_hierarchies[0]
    else:
        raise HTTPException(
            status_code=404, detail=f"No presentation found with id: {id}"
        )


@api_server.get(
    "/dts/{dts_name}/grid_table/{linkrole_id}",
    tags=["Deprecated", "Tables", "DTS"],
    # todo pydantic-model
    responses={404: {"model": ErrorMessage}},
    deprecated=True,
)
def grid_table(dts_name: str, linkrole_id: str, nt_version: Optional[str] = None):
    """
    obsolete, see render_table
    Gets a Nessie specific representation of the requested table.
    The returned JSON holds enough information to be transformed to html
    The stylesheet that is returned holds the css-grid definition
    """
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    grid_table = TableGrid(db=db, dts_name=dts_name, linkrole_id=linkrole_id)
    return grid_table.grid.api_response


@api_server.get(
    "/dts/{dts_name}/render_table/{linkrole_id}",
    tags=["Presentations", "Linkroles", "Tables", "DTS"],
    responses={404: {"model": ErrorMessage}},
    response_model=ApiTableGrid,
    description="Gets a Nessie specific representation of the requested table. "
    "The returned JSON holds enough information to be transformed to html. "
    "The stylesheet that is returned holds the css-grid definition",
)
def render_table(
    dts_name: str,
    linkrole_id: str,
    nt_version: Optional[str] = None,
    linkrole_role_uri: Optional[str] = None,
):
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    table_tree = TableTree(db=db)
    if table_tree.build_table_tree(
        dts_name=dts_name, linkrole_id=linkrole_id, linkrole_role_uri=linkrole_role_uri
    ):
        return table_tree.render_html
    else:
        raise HTTPException(
            status_code=404, detail=f"Table of linkrole: {id} could not be rendered"
        )


@api_server.get(
    "/dts/{dts_name}/render_hypercube/{linkrole_rowid}",
    tags=["Presentations", "Linkroles", "Tables", "DTS"],
    responses={404: {"model": ErrorMessage}},
    response_model=ApiTableGrid,
    description="Gets a Nessie specific representation of the requested table. "
    "The returned JSON holds enough information to be transformed to html. "
    "The stylesheet that is returned holds the css-grid definition",
)
def render_hypercube(
    dts_name: str, linkrole_rowid: str, nt_version: Optional[str] = None
):
    try:
        db = get_connection(nt_version=nt_version)
    except ValueError as error:
        raise HTTPException(status_code=404, detail=str(error))
    table_tree = TableTree(db=db)
    if table_tree.build_hypercube(dts_name=dts_name, linkrole_rowid=linkrole_rowid):
        return table_tree.render_html
    else:
        raise HTTPException(
            status_code=404,
            detail=f"Hypercube of linkrole: {linkrole_rowid} could not be rendered",
        )


@api_server.get(
    "/nt-versions",
    tags=["NT Versions"],
    response_model=NTVersions,
    description="Returns all NT Versions available for viewing. This information is stored inside config.yaml",
)
def get_nt_versions():
    versions = []
    # adding a 'z' to the string so NT19z will be sorted above NT19_bz (we sort reversed)
    if config.db["type"] == "postgres":
        for key in config.db.get("dbnames").keys():
            versions.append({"label": key, "value": key, "_sort": f"{key}z"})
    else:
        for key in config.db.get("files").keys():
            versions.append({"label": key, "value": key, "_sort": f"{key}z"})
    versions.sort(key=lambda x: x["_sort"], reverse=True)
    return {"nt_versions": versions}


@api_server.get(
    "/search", response_model=SearchResponse, tags=["Deprecated"], deprecated=True
)
def search(q: str, filter: str = None):
    db = get_connection()
    search = Search(db=db)
    search.search(query=q, filter=filter)
    return {
        "query": q,
        "filter": filter,
        "found": len(search.result),
        "results": search.result,
    }


if __name__ == "__main__":
    import uvicorn

    uvicorn.run(api_server, host="0.0.0.0", port=8000, root_path="/api")
