import requests

from discover_xbrl.api.api_server import get_nt_versions
from discover_xbrl.sbrnl_modules.db.connection import get_connection
from discover_xbrl.sbrnl_modules.db.collections.linkroles import Linkroles
from discover_xbrl.sbrnl_modules.db.collections.tables import Tables
from discover_xbrl.sbrnl_modules.db.collections.dts import DTSes
from discover_xbrl.sbrnl_modules.db.collections.hypercubes import HyperCubes
from discover_xbrl.sbrnl_modules.db.collections.presentation_hierarchies import (
    PresentationHierarchies,
)
from discover_xbrl.sbrnl_modules.db.collections.arcs import Arcs


class ApiCacher:
    def __init__(self, nt_version=None, ep_list=None):
        self.ep_list = ep_list
        self.nt_version = nt_version
        self.bypass_cache = True if ep_list else False
        self.db = None
        self.api_url = "https://kc-xbrl.cooljapan.nl/api"
        self.host_string = "kc-xbrl.cooljapan.nl"
        self.dts_list = []
        self.path_list = [
            "/presentation",
            "/concepts",
            "/tables",
            "/itemtypes",
            "/hypercubes",
            "/dimensions",
            "/linkroles",
            "/assertions",
            # "/arcroles",
        ]
        self.full = ["/presentation", "/itemtypes"]
        if not self.nt_version:
            db_names = get_nt_versions()
            self.nt_versions = (
                [val["value"] for val in db_names["nt_versions"]]
                if db_names.get("nt_versions")
                else [False]
            )
        else:
            self.nt_versions = [self.nt_version]

    def create_url(self):
        yield f"{self.api_url}/nt-versions"
        for nt_version in self.nt_versions:
            yield f"{self.api_url}/dts?nt_version={nt_version}"
            self.db = get_connection(nt_version=nt_version)
            dtses = DTSes(db=self.db)
            dtses.get_all()

            for dts in dtses.dtses:
                cache = True
                if self.ep_list:
                    cache = False
                    for ep in self.ep_list:
                        if hasattr(ep, "href"):
                            parts = ep.href.split("/")
                        else:
                            parts = ep.split("/")
                        parts.reverse()
                        name = parts[0]
                        if len(parts) > 1:
                            ep_nt_version = parts[4]
                            ep_sub_version = parts[2].split(".")
                        else:
                            ep_nt_version = self.nt_version
                            ep_sub_version = ""
                        if len(ep_sub_version) > 1:
                            ep_sub_version = (
                                "alfa" if ep_sub_version[1] == "a" else "beta"
                            )
                            ep_nt_version = f"{ep_nt_version}_{ep_sub_version}"
                        # print(
                        #     f"{ep_nt_version} / {nt_version} : {name} / {dts['name']}"
                        # )
                        if ep_nt_version == nt_version and name == dts["name"]:
                            cache = True
                            break
                if not cache:
                    continue

                for path in self.path_list:
                    if path not in ["/presentation"]:
                        if nt_version:
                            request_uri = f"{self.api_url}/dts/{dts['name']}{path}?nt_version={nt_version}&full=0"
                            yield request_uri
                        else:
                            request_uri = (
                                f"{self.api_url}/dts/{dts['name']}{path}?full=0"
                            )
                            yield request_uri
                    if path in self.full:
                        if nt_version:
                            request_uri = f"{self.api_url}/dts/{dts['name']}{path}?nt_version={nt_version}&full=1"
                            yield request_uri
                        else:
                            request_uri = (
                                f"{self.api_url}/dts/{dts['name']}{path}?full=1"
                            )
                            yield request_uri
                if nt_version:
                    yield f"{self.api_url}/dts/{dts['name']}/references?nt_version={nt_version}"
                    yield f"{self.api_url}/dts/{dts['name']}/presentations?nt_version={nt_version}"
                else:
                    yield f"{self.api_url}/dts/{dts['name']}/references"
                    yield f"{self.api_url}/dts/{dts['name']}/presentations"

                hypercubes = HyperCubes(db=self.db, full=False)
                hypercubes.hypercubes_of_dts(dts_name=dts["name"])
                if len(hypercubes.hypercubes):
                    for hypercube in hypercubes.hypercubes:
                        if nt_version:
                            request_uri = f"{self.api_url}/dts/{dts['name']}/hypercubes/{hypercube['hc_role_uri'].replace('/', '|')}?nt_version={nt_version}&full=1"
                        else:
                            request_uri = f"{self.api_url}/dts/{dts['name']}/hypercubes/{hypercube['hc_role_uri'].replace('/', '|')}?full=1"
                        yield request_uri

                tables = Tables(db=self.db, full=False)
                tables.tables_of_dts(dts_name=dts["name"])
                if len(tables.tables):
                    for table in tables.tables:
                        if nt_version:
                            request_uri = f"{self.api_url}/dts/{dts['name']}/tables/{table['id']}?nt_version={nt_version}&full=1"
                        else:
                            request_uri = f"{self.api_url}/dts/{dts['name']}/tables/{table['id']}?full=1"
                        yield request_uri

                linkroles = Linkroles(db=self.db, full=False)
                linkroles.linkroles_from_dts(dts_name=dts["name"])
                if len(linkroles.linkroles):
                    for linkrole in linkroles.linkroles:
                        if nt_version:
                            request_uri = f"{self.api_url}/dts/{dts['name']}/linkroles/{linkrole['id']}/{linkrole['role_uri']}?nt_version={nt_version}&full=1"
                        else:
                            request_uri = f"{self.api_url}/dts/{dts['name']}/linkroles/{linkrole['id']}?full=1"
                        yield request_uri
                        if nt_version:
                            request_uri = f"{self.api_url}/dts/{dts['name']}/render_table/{linkrole['id']}/{linkrole['role_uri']}?nt_version={nt_version}"
                        else:
                            request_uri = f"{self.api_url}/dts/{dts['name']}/render_table/{linkrole['id']}"
                        yield request_uri

                presentations = PresentationHierarchies(db=self.db, full=False)
                presentations.presentation_hierarchies_of_dts(dts_name=dts["name"])
                if len(presentations.presentation_hierarchies):
                    for presentation in presentations.presentation_hierarchies:
                        if nt_version:
                            request_uri = f"{self.api_url}/presentations/{presentation['rowid']}?nt_version={nt_version}"
                        else:
                            request_uri = (
                                f"{self.api_url}/presentations/{presentation['rowid']}"
                            )
                        yield request_uri
                arcs = Arcs(db=self.db)
                arcs.get_arcrole_dropdown(dts_name=dts["name"])
                for role in arcs.arcrole_dropdown:
                    request_uri = f"{self.api_url}/dts/{dts['name']}/arcs/{role['value']}?nt_version={nt_version}"
                    yield request_uri

    def cache_api(self):
        headers = {
            "Origin": "http://plu.to",
            "Host": self.host_string,
            "accept-encoding": "gzip",
        }
        if self.bypass_cache:
            headers["X-Cache-Refresh"] = "true"

        for url in self.create_url():
            response = requests.get(url, headers=headers)
            if not response.status_code == 200:
                print(f"ERROR {response.status_code}", end=" url: ")
            else:
                print(f"time: {response.elapsed}", end=" url: ")
            print(url)


def main():
    api_cacher = ApiCacher()
    api_cacher.cache_api()


if __name__ == "__main__":
    main()
