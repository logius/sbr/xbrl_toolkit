import yaml
from os.path import isfile
from discover_xbrl.conf.conf import Config


class Configuration:
    """
    This resembles somehow conf.conf but
    it's nota singleton
    """

    def __init__(self, filename=None):
        _filename = filename if filename else None
        self.configuration = Config()  # we need the project_root first
        self.filename = f"{self.configuration.project_root}/conf/{_filename}"
        if isfile(self.filename):
            self.configuration = Config(file=_filename)
        elif _filename and isfile(_filename):
            pass
        elif _filename:
            raise ValueError("Configuration file not found")

    def save(self):
        print(f"Saving config: {self.filename}")
        with open(self.filename, "w") as configfile:
            print(self.filename)
            if isinstance(self.configuration, Config):
                configfile.write(
                    yaml.dump(self.configuration.__dict__)
                )  # No fancy object annotations please.
            else:
                configfile.write(yaml.dump(self.configuration))

    def update(self, new_config=None):
        if not new_config:
            return False


def main():
    cfg = Configuration("config.admin.yaml")
    print(cfg)
    # cfg.configuration.parser["default_ep_list"] = [
    #     "http://www.nltaxonomie.nl/nt16/abm/20220119/entrypoints/abm-rpt-aedes-benchmark.xsd"
    # ]
    print(yaml.dump(cfg.configuration))
    cfg.save()


if __name__ == "__main__":
    main()
