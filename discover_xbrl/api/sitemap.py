import datetime
from lxml import etree

from discover_xbrl.api.api_server import get_nt_versions
from discover_xbrl.conf.conf import Config
from discover_xbrl.sbrnl_modules.db.collections.dts import DTSes
from discover_xbrl.sbrnl_modules.db.connection import get_connection


class Sitemap:
    def __init__(self):
        self.conf = Config()
        self.host_string = "kc-xbrl.cooljapan.nl"
        self.db = None
        db_names = get_nt_versions()
        self.nt_versions = (
            reversed([val["value"] for val in db_names["nt_versions"]])
            if db_names.get("nt_versions")
            else [False]
        )
        self.webroot = self.conf.admin.get("webroot", "/tmp")

    def generate_sitemap(self):
        attr_qname = etree.QName(
            "http://www.w3.org/2001/XMLSchema-instance", "schemaLocation"
        )
        ns_map = {
            "xsi": "http://www.w3.org/2001/XMLSchema-instance",
            None: "http://www.sitemaps.org/schemas/sitemap/0.9",
        }
        root = etree.Element(
            "urlset",
            {attr_qname: "http://www.sitemaps.org/schemas/sitemap/0.9"},
            nsmap=ns_map,
        )
        doc = etree.ElementTree(root)
        for nt_version in self.nt_versions:
            self.db = get_connection(nt_version=nt_version)
            dtses = DTSes(db=self.db)
            dtses.get_all()
            for dts in dtses.dtses:
                url = etree.SubElement(root, "url")
                loc = etree.SubElement(url, "loc")
                loc.text = f"https://{self.host_string}/dts/{nt_version}/{dts['name']}/definition"
                lastmod = etree.SubElement(url, "lastmod")
                lastmod.text = (
                    datetime.datetime.now()
                    .astimezone()
                    .replace(microsecond=0)
                    .isoformat()
                )
        doc.write(
            f"{self.webroot}/sitemap.xml",
            pretty_print=True,
            encoding="utf-8",
            xml_declaration=True,
        )


def main():
    s = Sitemap()
    s.generate_sitemap()


if __name__ == "__main__":
    main()
