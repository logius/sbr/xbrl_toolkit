import os.path as path
import os
import copy
from typing import Union, List
from datetime import datetime, timedelta
from fastapi import APIRouter, HTTPException, UploadFile, File, BackgroundTasks, Depends
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jose import JWTError, jwt

from discover_xbrl.api.admin_schema import (
    ApiConfig,
    ApiCliParams,
    ApiLoadTaxonomies,
    ApiUser,
    ZipUploadResponse,
    ApiCompareTaxonomies,
    Token,
)
from discover_xbrl.api.admin_configuration import Configuration
from discover_xbrl.api.ziphelper import (
    search_entrypoints,
    find_root_dir,
    find_zip_files,
    unpack_zip_from_zip,
    unpack_to_local_taxonomy_dir,
    search_nt_version,
    remapped_entrypoints,
)
from discover_xbrl.api.users import users
from discover_xbrl.sbrnl_modules.db.version_compare.version_compare import (
    VersionCompare,
)
from discover_xbrl.sbrnl_xbrl_parser.parsetools.helpers import version_from_identifier

from discover_xbrl.sbrnl_xbrl_toolbox.xbrl_cli import CLI
from discover_xbrl.sbrnl_xbrl_parser.parsetools.taxonomy_package import (
    read_taxonomy_package,
)
from discover_xbrl.sbrnl_xbrl_toolbox.build_ep_list import NTVersionList

admin = APIRouter(prefix="/admin", tags=["Admin"])
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="admin/login/token")


# todo: vervang die hash door een SECRET!
def create_access_token(data: dict, expires_delta: Union[timedelta, None] = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(hours=2)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(
        to_encode,
        "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7",
        algorithm="HS256",
    )
    return encoded_jwt


def get_current_user(token: str = Depends(oauth2_scheme)):
    try:
        payload = jwt.decode(
            token,
            "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7",
            algorithms=["HS256"],
        )
        username: str = payload.get("username")
        if username is None:
            raise ValueError
        return users.get_user(username=username)
    except JWTError as err:
        raise HTTPException(
            status_code=401, detail=str(err), headers={"WWW-Authenticate": "Bearer"}
        )


@admin.get(
    "/current_user",
    response_model=ApiUser,
    tags=["AdminUser"],
    description="Get the full name and e-mail from <username>",
)
def get_user(username: str = None):
    return users.get_user(username=username)


@admin.post(
    "/login/token",
    response_model=Token,
    tags=["AdminUser"],
    description="Authorized users get a token which they should use in a special request header",
)
def login(form_data: OAuth2PasswordRequestForm = Depends()):
    user = users.get_user(form_data.username)
    if not user:
        raise HTTPException(
            status_code=401,
            detail="Invalid authentication credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )
    if not users.authenticate(form_data.username, form_data.password):
        raise HTTPException(
            status_code=401,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token = create_access_token({"username": user.username}, timedelta(hours=23))
    return {"access_token": access_token, "token_type": "bearer"}


@admin.post(
    "/preview_ep_list",
    response_model=List[str],
    description="Fetches a list of entrypoints for given NT-Version",
)
def preview_ep_list(
    cli_params: ApiCliParams, current_user: ApiUser = Depends(get_current_user)
):
    try:
        xbrl_cli = CLI()
        xbrl_cli.alfa = cli_params.alfa
        xbrl_cli.beta = cli_params.beta
        xbrl_cli.nt_version = cli_params.nt_version
        xbrl_cli.create_ep_list()
        return sorted(xbrl_cli.ep_list)
    except ValueError:
        print("Pass on what you have learned.")
        return {"status": "Error"}


@admin.post(
    "/upload_taxonomy_package",
    # response_model=ZipUploadResponse,
    tags=["AdminZip"],
    description="Special endpoint for taxonomy packages. This only works if the taxonomy package "
    "is completely self contained or all mappings for external files are correct."
    "Use with caution and test with {save: false, validate: false}",
)
def upload_taxonomy_package(
    zipfiles: List[UploadFile] = File(...),
    current_user: ApiUser = Depends(get_current_user),
):
    for file in zipfiles:
        taxonomy_package = path.join("/tmp", file.filename)
        try:
            contents = file.file.read()
            with open(taxonomy_package, "wb") as outfile:
                outfile.write(contents)
            outfile.close()
        except Exception:
            raise HTTPException(
                status_code=415, detail={"error": "Could not save archive"}
            )
        try:
            response = read_taxonomy_package(taxonomy_package)
            if not response.get("entrypoints"):
                raise HTTPException(
                    status_code=415, detail={"error": "Not a valid taxonomy package"}
                )
        except ValueError:
            raise HTTPException(
                status_code=415,
                detail={
                    "error": "Value error on taxonomy package, uploaded zip could not be read"
                },
            )

        nt_version = version_from_identifier(response["metadata"].get("identifier"))

        return {
            "status": "Ok",
            "filename": taxonomy_package,
            "info": response,
            "nt_version": nt_version,
        }


@admin.post(
    "/upload_zip",
    # response_model=ZipUploadResponse,
    tags=["AdminZip"],
    description="Upload Zip-file containing parts of the Dutch Taxonomy. Returns several objects:"
    "the contents of the zipfile, found entrypoints, possible warnings",
)
def upload_zipfile(
    zipfiles: List[UploadFile] = File(...),
    current_user: ApiUser = Depends(get_current_user),
):
    response = {}
    for file in zipfiles:
        try:
            with open(f"/tmp/{file.filename}", "wb") as zipfile:
                zipfile.write(file.file.read())
            zipfile.close()
            # maybe this is a taxonomy-package?
            response = read_taxonomy_package(f"/tmp/{file.filename}")
            if not response.get("entrypoints"):
                response["entrypoints"] = search_entrypoints(
                    filename=f"/tmp/{file.filename}"
                )
                response["directories"] = find_root_dir(
                    filename=f"/tmp/{file.filename}"
                )
                response["zipfiles"] = find_zip_files(filename=f"/tmp/{file.filename}")
                response["filename"] = file.filename
                print("Unpacked zipfile inside /tmp")
                return response
            else:
                response["taxonomy_package"] = True
                response["filename"] = f"/tmp/{file.filename}"
                print("It's a package!")
                return response
        except IOError:
            return {"status": "error", "detail": "Unable to write file"}


@admin.post(
    "/unpack_zip",
    response_model=ZipUploadResponse,
    tags=["AdminZip"],
    description="After uploading a Zipfile you need to unpack it. "
    "It takes the filename of a previously uploaded archive.",
)
def unpack_zipfile(
    payload: ApiLoadTaxonomies,
):
    response = {}
    if payload.zip and payload.zipfiles:
        for archive in payload.zipfiles:
            newfile = unpack_zip_from_zip(filename=f"/tmp/{payload.zip}", zip=archive)
            print(f"got {newfile}")
            response["entrypoints"] = search_entrypoints(filename=f"/tmp/{newfile}")
            response["directories"] = find_root_dir(filename=f"/tmp/{newfile}")
            response["zipfiles"] = find_zip_files(filename=f"/tmp/{newfile}")
            response["filename"] = newfile
            response["nt_version"] = search_nt_version(filename=f"/tmp/{archive}")
            print(response)
    return response


@admin.post("/unpack_to_local", response_model=ZipUploadResponse, tags=["AdminZip"])
def unpack_zipfile(
    payload: ApiLoadTaxonomies,
):
    response = {}
    cfg = Configuration()
    if payload.zipfiles:
        for archive in payload.zipfiles:
            archive = path.basename(archive)
            if unpack_to_local_taxonomy_dir(
                archive, cfg.configuration.parser["local_taxonomy_dir"]
            ):
                response["entrypoints"] = remapped_entrypoints(
                    filename=f"/tmp/{archive}",
                    local_taxonomy_dir=cfg.configuration.parser["local_taxonomy_dir"],
                )
                response["nt_version"], response["sub_version"] = search_nt_version(
                    filename=f"/tmp/{archive}"
                )
            print("Saved to tmp directory")
            print(response)
    return response


@admin.post(
    "/unpack_non_nta_to_local", response_model=ZipUploadResponse, tags=["AdminZip"]
)
def unpack_non_nta_zipfile(
    payload: ApiLoadTaxonomies,
):
    response = {}
    cfg = payload.conf
    if payload.zipfiles:
        for archive in payload.zipfiles:
            if unpack_to_local_taxonomy_dir(
                archive, cfg.configuration.parser["non_nta_taxonomy_dir"]
            ):
                response["entrypoints"] = remapped_entrypoints(
                    filename=f"/tmp/{archive}",
                    local_taxonomy_dir=cfg.configuration.parser["non_nta_taxonomy_dir"],
                )
                response["nt_version"], response["sub_version"] = search_nt_version(
                    filename=f"/tmp/{archive}"
                )
            print("Saved to tmp directory")
            print(response)
    return response


@admin.get(
    "/available_nt_versions",
    description="Returns which NT Versions are available in given directory. ",
)
def available_nt_versions(
    local_taxonomy_dir=None, current_user: ApiUser = Depends(get_current_user)
):
    nt_version = NTVersionList(local_taxonomy_dir=local_taxonomy_dir)
    if len(nt_version.nt_versions) > 0:
        return {"nt_versions": nt_version.nt_versions}
    return {"nt_versions": [f"Not found"]}


@admin.post(
    "/config",
    description="Save the server configuration. You'd better know what you're doing",
)
def save_admin_config(
    new_config: ApiConfig, current_user: ApiUser = Depends(get_current_user)
):
    try:
        cfg = Configuration()
        configuration = new_config.configuration
        cfg.configuration = configuration
        cfg.save()
    except ValueError:
        print("Oh noes!")
        return {"status": "Error"}
    return {"status": "Ok"}


@admin.get(
    "/taxonomy",
    response_model=ApiConfig,
    description="Get the configuration of the server. You'll need this if you want to start a background job.",
)
def admin_config(current_user: ApiUser = Depends(get_current_user)):
    try:
        cfg = Configuration()
    except ValueError:
        print("Stuk?")
        return {"Error"}
    return {"configuration": cfg.configuration.__dict__, "filename": cfg.filename}


@admin.post(
    "/load_tax",
    description="The worker.\n\nTakes a list of entrypoints from a given NT Version (or taxonomy package)"
    "and executes the actions that are in the request. Always parses the original XML Files,"
    "can validate the entrypoints against NTA Rules, can store the result to the database "
    "and can produce a mutation report of given entrypoints.",
)
async def start_cli(
    payload: ApiLoadTaxonomies,
    background_tasks: BackgroundTasks,
    current_user: ApiUser = Depends(get_current_user),
):
    background_tasks.add_task(xbrl_cli_run, payload)
    return {"status": "Ok"}


@admin.post(
    "/compare-versions",
    description="Compare the complete DTS, the domains and each entrypoint to detect changes."
    "Runs in the background.",
)
async def version_compare(
    payload: ApiCompareTaxonomies,
    background_tasks: BackgroundTasks,
    current_user: ApiUser = Depends(get_current_user),
):
    print(payload)
    compare = VersionCompare(
        old_db=payload.old_nt_version,
        new_db=payload.new_nt_version,
        run_id=payload.run_id,
    )
    background_tasks.add_task(compare.create_report)
    background_tasks.add_task(compare.create_domeinen_reports)
    background_tasks.add_task(compare.create_ep_reports)
    return {"status": "Ok"}


@admin.get(
    "/available-compare-reports",
    description="Return a list of available mutation reports.",
)
def get_available_compare_reports():
    compare = VersionCompare(None, None)
    return compare.available_reports


@admin.get("/get_log", description="Returns the log of the current background job")
def get_log(position: int = None):
    cfg = Configuration()
    lines = []
    with open(
        path.join(
            cfg.configuration.logging["maindir"], cfg.configuration.logging["xbrl_cli"]
        ),
        "r",
    ) as logfile:
        if position:
            logfile.seek(position)
        while True:
            line = logfile.readline()
            if not line:
                break
            lines.append(line)
        position = logfile.tell()
    return {"position": position, "loglines": lines}


@admin.get("/get-config", description="Returns the current  (server) configuration")
def get_current_config(current_user: ApiUser = Depends(get_current_user)):
    cfg = Configuration()
    return cfg


@admin.get(
    "/get-config/non-nta", description="Returns the current  (server) configuration"
)
def get_current_config(current_user: ApiUser = Depends(get_current_user)):
    # strips 'www.nltaxonomie.nl from the file-path.
    cfg = Configuration()
    cfg.configuration.parser["non_nta_taxonomy_dir"] = path.dirname(
        cfg.configuration.parser["local_taxonomy_dir"]
    )
    return cfg


@admin.get("/non-nta-dts-disk", description="Returns a list of available DTS on disk")
def get_non_nta_dts_disk(
    local_taxonomy_dir=None, current_user: ApiUser = Depends(get_current_user)
):
    cfg = Configuration()
    new_local_taxonomy_dir = path.dirname(
        cfg.configuration.parser["local_taxonomy_dir"]
    )
    print(new_local_taxonomy_dir)
    non_nta_dts = [
        entry.name
        for entry in os.scandir(new_local_taxonomy_dir)
        if entry.is_dir() and entry.name not in ["www.nltaxonomie.nl"]
    ]
    print(non_nta_dts)
    return {"available_dts": non_nta_dts}


def xbrl_cli_run(payload):
    try:
        xbrl_cli = CLI()
        xbrl_cli.parse = payload.cli.parse
        xbrl_cli.alfa = payload.cli.alfa
        xbrl_cli.beta = payload.cli.beta
        xbrl_cli.build_ep_list = payload.cli.build_ep_list
        xbrl_cli.nt_version = payload.cli.nt_version
        xbrl_cli.store = payload.cli.store
        xbrl_cli.validate_nt = payload.cli.validate_nt
        xbrl_cli.compare = payload.cli.compare
        xbrl_cli.run_id = payload.cli.run_id
        xbrl_cli.notify = payload.cli.notify
        xbrl_cli.ep_list = payload.ep_list
        xbrl_cli.taxonomy_package = payload.cli.taxonomy_package

        xbrl_cli.run()
    except ValueError as e:
        print(payload)
        print("Don't eat yellow snow")
        print(e)
        return {"status": "Error"}


if __name__ == "__main__":
    get_non_nta_dts_disk()
