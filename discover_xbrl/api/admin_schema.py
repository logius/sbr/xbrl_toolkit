from __future__ import annotations
from pydantic import BaseModel, Field
from typing import List, TypedDict, Literal, Union


class ApiConfig(BaseModel):
    configuration: dict
    filename: str = None

    class Config:
        orm_mode = True


class ApiCliParams(BaseModel):
    parse: bool
    store: bool
    validate_nt: bool
    compare: bool
    run_id: str = None
    build_ep_list: bool
    alfa: bool
    beta: bool
    final: bool
    nt_version: str = None
    taxonomy_package: str = None
    notify: str = None
    local_taxonomy_dir: str = None


class ApiLoadTaxonomies(BaseModel):
    conf: ApiConfig
    cli: ApiCliParams
    ep_list: List = None
    zipfiles: List = None
    zip: str = None


class ApiCompareTaxonomies(BaseModel):
    old_nt_version: str
    new_nt_version: str
    run_id: Union[Literal["SBR", "KVK", "BD", "BZK", "OCW"], None] = None


class ApiUser(BaseModel):
    username: str
    email: Union[str, None] = None
    full_name: Union[str, None] = None


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: Union[str, None] = None


class OptionDict(TypedDict):
    label: str
    value: str


class ZipUploadResponse(BaseModel):
    filename: str = None
    directories: List[OptionDict] = None
    entrypoints: List[OptionDict] = None
    zipfiles: List[OptionDict] = None
    nt_version: str = None
    sub_version: str = None
    taxonomy_package: bool = None
