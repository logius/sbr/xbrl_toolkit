from __future__ import annotations
from pydantic import BaseModel
from typing import List, Optional


class DtsFeatures(BaseModel):
    toc: bool = True
    definition: bool = True
    dimensions: bool = False
    presentations: bool = True
    concepts: bool = True
    itemtypes: bool = True
    references: bool = True
    labels: bool = True
    linkroles: bool = True
    tables: bool = False
    networks: bool = True
    formulas: bool = False


class ApiDts(BaseModel):
    name: str
    shortname: str
    nice_name: str = None
    description: str = None
    has_presentation: bool = None
    features: DtsFeatures = None


class ApiNamespace(BaseModel):
    prefix: str
    uri: str


class ApiLabel(BaseModel):
    id: str = None
    lang: str = None
    label_text: str = None
    link_label: str = None
    link_role: str = None


class ApiReference(BaseModel):
    id: str
    label: str
    name: str = None
    number: str = None
    issuedate: str = None
    article: str = None
    chapter: str = None
    note: str = None
    section: str = None
    subsection: str = None
    publisher: str = None
    paragraph: str = None
    subparagraph: str = None
    clause: str = None
    subclause: str = None
    appendix: str = None
    example: str = None
    page: str = None
    exhibit: str = None
    footnote: str = None
    sentence: str = None
    uri: str = None
    uridate: str = None


class ApiEnumerationOptions(BaseModel):
    id: str
    value: str = None
    labels: List[ApiLabel] = None


class ApiItemtype(BaseModel):
    id: str = None
    name: str
    domain: str = None
    base: str = None
    inheritance_type: str = None
    restriction_type: str = None
    restriction_length: int = None
    restriction_pattern: str = None
    restriction_max_length: str = None
    restriction_min_length: int = None
    total_digits: int = None
    fraction_digits: int = None
    min_inclusive: str = None
    max_inclusive: str = None
    enumeration: List[ApiEnumerationOptions] = None
    number_of_concepts: int = None


class ApiConcept(BaseModel):
    id: str
    name: str = None
    balance: str = None
    nillable: bool = None
    abstract: bool = None
    periodtype: str = None
    ns_prefix: str = None
    namespace: str = None
    substitution_group: str = None
    typed_domainref: str = None
    itemtype_name: str = None
    labels: List[ApiLabel] = None
    references: List[ApiReference] = None
    itemtype: ApiItemtype = None
    p_link: bool = None
    d_link: bool = None
    t_link: bool = None


class ApiLineItem(BaseModel):
    order: str = None
    concept_id: str
    concept: ApiConcept = None
    parent_concept_id: str = None
    line_items: List[ApiLineItem] = None


ApiLineItem.update_forward_refs()


class ApiMember(BaseModel):
    concept_id: str
    order: str = None
    usable: bool = None
    parent_rowid: int
    concept: ApiConcept = None
    preferred_label: str = None
    target_linkrole: str = None
    rowid: int
    members: List[ApiMember] = None
    type: str = None


ApiMember.update_forward_refs()


class ApiDomain(BaseModel):
    rowid: int
    concept_id: str
    target_linkrole: str = None
    usable: bool = None
    dimension_rowid: int
    members: List[ApiMember]
    concept: ApiConcept = None
    type: str = None


class ApiDimension(BaseModel):
    rowid: int
    concept_id: str
    target_linkrole: str = None
    hypercube_linkrole_rowid: str = None
    linkrole_rowid: str = None
    domains: List[ApiDomain] = None
    members: List[ApiMember] = None
    concept: ApiConcept = None


class ApiHypercube(BaseModel):
    hc_role_uri: str
    hc_type: str
    linkrole_rowid: int
    context_element: str = None
    closed: str = None
    definition: str = None
    line_items: List[ApiLineItem] = None
    dimensions: List[ApiDimension] = None
    labels: List[ApiLabel] = None


class ApiFilter(BaseModel):
    id: str = None
    label: str
    type: str
    aspect: str = None
    include_qname: str = None
    exclude_qname: str = None
    qname: str = None
    qname_expression: str = None
    period_type: str = None
    value: str = None
    qname_member: str = None
    qname_dimension: str = None
    linkrole_role_uri: str = None
    arcrole: str = None
    axis: str = None
    balance_type: str = None
    strict: str = None
    type_qname: str = None
    type_qname_expression: str = None
    test: str = None
    boundary: str = None
    date: str = None
    time: str = None
    parent: str = None
    variable: str = None
    arc_complement: str = None
    arc_priority: str = None
    arc_order: str = None
    arc_cover: str = None
    subfilters: List[ApiFilter] = None


ApiFilter.update_forward_refs()


class ApiVariable(BaseModel):
    id: str
    type: str
    label: str
    bind_as_sequence: str = None
    variable_arc_name: str = None
    select: str = None
    matches: str = None
    nils: bool = None
    fallback_value: str = None
    linkrole_rowid: int = None
    order: int = None
    priority: str = None
    filters: List[ApiFilter] = None


class ApiVariableset(BaseModel):
    name: str
    variables: List[ApiVariable] = None


class ApiVariablesetVariable(BaseModel):
    variableset_name: str
    variable_id: str
    order: int = None
    priority: str = None


class ApiParameter(BaseModel):
    id: str
    label: str
    as_datatype: str = None
    select: str = None
    name: str = None
    required: str = None


class ApiAssertion(BaseModel):
    id: str
    aspect_model: str
    implicit_filtering: str
    xlink_label: str
    test: str = None
    assertion_type: str = None
    severity: str = None
    labels: List[ApiLabel] = None
    variablesets: List[ApiVariableset] = None


class ApiAspectNode(BaseModel):
    id: str
    label: str
    parent_child_order: str = None
    tagselector: str = None
    merge: bool = None
    order: str = None
    is_abstract: bool = None
    aspect_type: str = None
    include_unreported_value: bool = None
    value: str = None
    labels: List[ApiLabel] = None


class ApiDimensionRelationshipNode(BaseModel):
    id: str
    label: str
    parent_child_order: str = None
    order: str = None
    dimension_id: str
    linkrole_role_uri: str = None
    formula_axis: str = None
    generations: str = None
    relationship_source: str = None
    tagselector: str = None
    labels: List[ApiLabel] = None


class ApiConceptRelationshipNode(BaseModel):
    id: str
    label: str
    parent_child_order: str = None
    tagselector: str = None
    order: str = None
    relationship_source: str = None
    linkrole_role_uri: str
    formula_axis: str = None
    generations: str = None
    arcrole: str = None
    linkname: str = None
    arcname: str = None
    labels: List[ApiLabel] = None


class ApiFormulaPeriod(BaseModel):
    rowid: int
    period_type: str
    start: str = None
    end: str = None
    value: str = None
    parent_ruleset: str = None
    parent_node: str = None


class ApiFormulaTypedDimension(BaseModel):
    rowid: int
    dimension_id: str
    xpath: str = None
    value: str = None
    parent_ruleset: str = None
    parent_node: str = None


class ApiFormulaExplicitDimension(BaseModel):
    rowid: int
    qname_dimension: str = None
    qname_member: str = None
    type_qname: str = None
    omit: bool = None


class ApiRuleSet(BaseModel):
    rowid: int
    rulenode_id: str = None
    tag: str
    formula_periods: List[ApiFormulaPeriod]
    formula_typed_dimensions: List[ApiFormulaTypedDimension]
    formula_explicit_dimensions: List[ApiFormulaExplicitDimension]


class ApiTableRuleNode(BaseModel):
    id: str
    label: str = None
    parent_child_order: str = None
    merge: bool = None
    order: str = None
    is_abstract: bool = None
    parent: str
    tagselector: str = None
    formula_periods: List[ApiFormulaPeriod] = None
    formula_typed_dimensions: List[ApiFormulaTypedDimension] = None
    formula_explicit_dimensions: List[ApiFormulaExplicitDimension] = None
    rulesets: List[ApiRuleSet] = None
    children: List[ApiTableRuleNode] = None
    aspect_nodes: List[ApiAspectNode] = None
    concept_relationshipnodes: List[ApiConceptRelationshipNode] = None
    dimension_relationshipnodes: List[ApiDimensionRelationshipNode] = None
    labels: List[ApiLabel] = None


ApiTableRuleNode.update_forward_refs()


class ApiTableBreakdown(BaseModel):
    id: str
    label: str
    parent_child_order: str = None
    axis: str
    order: str = None
    concept_relationshipnodes: List[ApiConceptRelationshipNode] = None
    aspect_nodes: List[ApiAspectNode] = None
    dimension_relationshipnodes: List[ApiDimensionRelationshipNode] = None
    table_rulenodes: List[ApiTableRuleNode] = None
    labels: List[ApiLabel] = None


class ApiTable(BaseModel):
    id: str
    label: str
    parent_child_order: str = None
    table_breakdowns: List[ApiTableBreakdown] = None
    labels: List[ApiLabel] = None
    parameters: List[ApiParameter] = None
    variables: List[ApiVariable] = None
    linkrole_id: str = None


class ApiPresentationHierarchyNode(BaseModel):
    rowid: int
    id: str
    node_type: str = None
    order: str = None
    linkrole_role_uri: str = None
    linkrole_id: str = None
    has_presentation: bool = None
    definition: str = None
    preferred_label: str = None
    is_root: bool = None
    parent_rowid: int = None
    dts_name: str = None
    directly_referenced: bool = None
    parent_linkrole_rowid: int = None
    hypercube: ApiHypercube = None
    tables: List[ApiTable] = None
    children: List[ApiPresentationHierarchyNode] = None
    concept: ApiConcept = None
    labels: List[ApiLabel] = None


ApiPresentationHierarchyNode.update_forward_refs()


class ApiLinkRole(BaseModel):
    id: str = None
    role_uri: str
    definition: str = None
    dts_name: str
    rowid: str
    order: str = None
    directly_referenced: int = None
    hypercube: ApiHypercube = None
    presentation_hierarchy: ApiPresentationHierarchyNode = None
    references: List[ApiReference] = None
    variables: List[ApiVariable] = None
    tables: List[ApiTable] = None
    assertions: List[ApiAssertion] = None
    targeted_by: List[ApiDimension] = None
    dimensions: List[ApiDimension] = None
    labels: List[ApiLabel] = None
    used_on: list = None


class ApiArcrole(BaseModel):
    value: str
    arcrole: str
    type: str
    label: str = None


class ApiArc(BaseModel):
    id: str
    children: List[ApiArc] = None
    linkrole: ApiLinkRole = None
    concept: ApiConcept = None


ApiArc.update_forward_refs()


class ApiBackreference(BaseModel):
    type: str = None
    source: str = None
    linkrole_rowid: str = None
    definition: str = None
    linkrole_id: str = None
    concept_name: str = None
    concept_id: str = None
    concept: ApiConcept = None


class ApiDtsFull(ApiDts):
    concepts: List[ApiConcept]
    linkroles: List[ApiLinkRole]
    labels: List[ApiLabel]
    namespaces: List[ApiNamespace]
    presentation_hierarchy: ApiPresentationHierarchyNode = None

    class Config:
        orm_mode = True


class ApiTableCell(BaseModel):
    tag: str
    id: str
    type: str
    preferred_label: str = None
    parent_child_order: str = None
    aspects: List[dict] = None
    concept: ApiConcept = None
    context_heading: str = None
    labels: List[ApiLabel] = None
    children_first_css: str = None


class ApiTableDiv(BaseModel):
    tag: str
    name: str = None
    id: str = None
    rows: int = 0
    width: int = 0
    labels: List[ApiLabel]
    children: List[ApiTableCell]
    children_first_css: str = None


class ApiTableGrid(BaseModel):
    css: str = None
    divs: ApiTableDiv


class SearchResult(BaseModel):
    text: str
    element_type: str
    element_rowid: str


class SearchResponse(BaseModel):
    query: str
    filter: str = None
    found: int = 0
    results: List[SearchResult]

    class Config:
        orm_mode = True


class DtsNiceName(BaseModel):
    dts_nice_name: str = None


class ErrorMessage(BaseModel):
    detail: str


class Option(BaseModel):
    label: str = None
    value: str = None


class NTVersions(BaseModel):
    nt_versions: List[Option]
