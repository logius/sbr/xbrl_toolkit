from passlib.hash import argon2
from discover_xbrl.api.admin_schema import ApiUser
from discover_xbrl.conf.conf import Config
from os.path import join
import json


class Users:
    def __init__(self):
        self.conf = Config()
        self.all_users = None
        if hasattr(self.conf, "admin") and self.conf.admin.get("user_db"):
            with open(
                join(self.conf.project_root, self.conf.admin.get("user_db"))
            ) as user_db:
                self.all_users = json.load(user_db)

    def get_user(self, username):
        if self.all_users:
            if self.all_users.get(username):
                return ApiUser(**self.all_users.get(username))
            else:
                return None
        else:
            return None

    def authenticate(self, username, password):
        if self.all_users:
            user = self.all_users.get(username)
            if user:
                return argon2.verify(password, user["hashed_password"])


users = Users()
