import zipfile
import os
import shutil
import uuid


def read_zipfile(filename):
    try:
        with zipfile.ZipFile(filename, "r") as validation_zip:
            pass
    except IOError:
        return False


def search_nt_version(filename):
    nt_version = ""
    sub_version = "final"
    try:
        with zipfile.ZipFile(filename, "r") as archive:
            for name in archive.namelist():
                if "www.nltaxonomie.nl/nt" in name:
                    parts = name.split("/")
                    for i, part in enumerate(parts):
                        if part == "www.nltaxonomie.nl":
                            nt_version = parts[i + 1]
                    break
            for name in archive.namelist():
                if "dictionary" in name:
                    version_dir = os.path.dirname(os.path.dirname(name)).split("/")[-1]
                    if version_dir.endswith(".a"):
                        sub_version = "alfa"
                    if version_dir.endswith(".b"):
                        sub_version = "beta"
                    return nt_version, sub_version
    except IOError:
        return [f"Error reading {filename} (nt_version)"]


def search_entrypoints(filename):
    entrypoints = []
    try:
        with zipfile.ZipFile(filename, "r") as validation_zip:
            for name in validation_zip.namelist():
                if "entrypoints" in name and name.endswith(".xsd"):
                    entrypoints.append({"label": name, "value": name})
            return entrypoints
    except IOError:
        return [f"Error reading {filename} (entrypoints)"]


def remapped_entrypoints(filename, local_taxonomy_dir):
    entrypoints = search_entrypoints(filename)
    for ep in entrypoints:
        _, path = ep["value"].split("www.nltaxonomie.nl")
        ep["value"] = f"{local_taxonomy_dir}{path}"
    return entrypoints


def find_root_dir(filename):
    directories = []
    try:
        with zipfile.ZipFile(filename, "r") as validation_zip:
            for info in validation_zip.infolist():
                if info.is_dir():
                    directories.append({"label": info.filename, "value": info.filename})
            return directories
    except IOError:
        return [f"Error reading {filename} (searching directories)"]


def find_zip_files(filename):
    zipfiles = []
    try:
        with zipfile.ZipFile(filename, "r") as validation_zip:
            for info in validation_zip.infolist():
                if not info.is_dir() and info.filename.endswith(".zip"):
                    zipfiles.append({"label": info.filename, "value": info.filename})
            return zipfiles
    except IOError:
        return [f"Error reading {filename} (searching zipfiles)"]


def unpack_zip_from_zip(filename, zip):
    try:
        print(f"fn: {filename} zip: {zip}")
        with zipfile.ZipFile(filename, "r") as archive:
            print("Unpackingk zip from zip")
            for info in archive.infolist():
                if info.filename == zip:
                    filename = info.filename.split("/")[-1:][0]
                    contents = archive.open(info)
                    with open(f"/tmp/{filename}", "wb") as outfile:
                        outfile.write(contents.read())
                    outfile.close()
                    return filename
    except IOError:
        return False


def unpack_to_local_taxonomy_dir(filename, local_taxonomy_dir):
    try:
        with zipfile.ZipFile(f"/tmp/{filename}", "r") as archive:
            print("Unpacking 'normal' zip")
            tempdir = f"/tmp/_xbrl_{uuid.uuid4().hex[:6].lower()}"
            os.mkdir(tempdir)
            archive.extractall(path=tempdir)
            #  match local_taxonomydir on extracted archive
            match_name = local_taxonomy_dir.split("/")[-1:][0]
            for info in archive.infolist():
                # this is dangerous ^_^, it works on www.nltaxonomies.nl which is fine, but ...
                if info.is_dir() and info.filename.endswith(f"{match_name}/"):
                    print(f"Moving: {info.filename} into {local_taxonomy_dir}")
                    shutil.copytree(
                        f"{tempdir}/{info.filename}",
                        local_taxonomy_dir,
                        dirs_exist_ok=True,
                    )
                    shutil.rmtree(tempdir)
                    return True
    except IOError:
        print(f"File not found errorT {filename}")
        return False


def main():
    print(search_nt_version("/tmp/NT17_SBR_20220301.zip"))


if __name__ == "__main__":
    main()
