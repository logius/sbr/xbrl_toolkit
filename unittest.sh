#!/bin/bash
# Find out where this script resides on disk,
# that is the project root,
# add that to the PYTHON_PATH
PROJECT_ROOT=$(dirname "$0")
export PYTHON_PATH=$PROJECT_ROOT:$PYTHON_PATH
export PYTHON_UNBUFFERED=True
python -m unittest discover -s ${PROJECT_ROOT}/test/unittests/ -t  ${PROJECT_ROOT}

