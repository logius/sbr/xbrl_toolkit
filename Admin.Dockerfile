FROM python:3.9
WORKDIR /app
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY ./*.sh /app/
RUN mkdir discover_xbrl
COPY ./discover_xbrl discover_xbrl

RUN mkdir -p /tmp/reports
RUN mkdir -p /tmp/log
RUN mkdir -p /tmp/db
RUN mkdir -p /tmp/xbrl/instances/output
RUN mkdir -p /tmp/xbrl/www.nltaxonomie.nl
COPY www.nltaxonomie.nl/nt16 /tmp/xbrl/www.nltaxonomie.nl/nt16
RUN cd /tmp/reports && git clone https://gitlab.com/xiffy/xbrl-validation-reports.git
COPY ./discover_xbrl/conf/config.admin.yaml discover_xbrl/conf/config.yaml
EXPOSE 8000
CMD ./SP.admin_server.sh

